﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class inventoryCardVm
    {
       
    }
    public class getInventoryitems
    {
        public Int32 companyId { get; set; }
        public bool isCompany { get; set; }
        public string reportName { get; set; }
        public Int32 inventoryId { get; set; }

    }

    public class getInventoryitemsMoves
    {
        public Int32 companyId { get; set; }
        public bool isCompany { get; set; }
        public string reportName { get; set; }
        public Int32 inventoryId { get; set; }
        public Int32 subAccountId { get; set; }
        public DateTime MinDate { get; set; }
        public DateTime MaxDate { get; set; }
        public Int32 pageSize { get; set; }
        public Int32 pageNumber { get; set; }
        public string orderby { get; set; }
        public bool orderType { get; set; }

    }

    public class itemsMoveWithFirsMove
    {
       

        public itemMoves firsMove { get; set; }
        public List<itemMoves> itemMoves { get; set; }
        public decimal turnoverRate { get; set; }
        public int StockRate { get; set; }
        public decimal totalIn { get; set; }
        public decimal totalOut { get; set; }
        public decimal total { get; set; }
        public int TotalCount { get; set; }
        public double totalPages { get; set; }
    }

    public class itemsMoveGroupWithFirsMove
    {
        public List<itemMovesGroup> itemMoves { get; set; }
        public decimal turnoverRate { get; set; }
        public int StockRate { get; set; }
        public decimal totalIn { get; set; }
        public decimal totalOut { get; set; }
        public decimal total { get; set; }
        public int TotalCount { get; set; }
        public double totalPages { get; set; }
    }
    public class itemMoves
    {
        public DateTime moveDate { get; set; }
        public string note { get; set; }
        public decimal qtyin { get; set; }
        public decimal pricein { get; set; }
        public decimal totalPricein { get; set; }
        public decimal qtyout { get; set; }
        public decimal priceout { get; set; }
        public decimal totalPriceout { get; set; }
        public decimal totalstockqty { get; set; }
        public decimal stockprice { get; set; }
        public decimal totalstockPrice { get; set; }
        public Int32 indexNo { get; set; }
    }
    public class itemMovesGroup
    {
        public decimal turnOVer { get; set; }
        public int StockRate { get; set; }
        public string itemName { get; set; }
        public decimal qty { get; set; }
        public decimal qtyin { get; set; }
        public decimal qtyout { get; set; }
        public decimal totalstockqty { get; set; }
        public decimal stockprice { get; set; }
        public decimal totalstockPrice { get; set; }
        public Int32 indexNo { get; set; }
    }

    public class getInventoryAccounts
    {
        public Int32 companyId { get; set; }
        public bool isCompany { get; set; }
      

    }

    public class dashbordVm
    {
        public decimal turnOverRate { get; set; }
        public decimal revnue { get; set; }
        public decimal sales { get; set; }
        public Int32 stockqty { get; set; }

    }
    public class inventoryAccounts
    {
        public Int32 AccountId { get; set; }
        public string AccountName { get; set; }
    }

   

}