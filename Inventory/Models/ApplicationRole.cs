﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole()
        {

        }

        public ApplicationRole(string name) : base(name)
        {

        }
        //public Int32 UserRoleGroupId { get; set; }
        //public virtual UserRoleGroup UserRoleGroup { get; set; }

        
    }
    //public class UserRoleGroup
    //{
    //    public Int32 UserRoleGroupId { get; set; }
    //    public string RoleGroupName { get; set; }
    //    public virtual ICollection<ApplicationRole> ApplicationRoles { get; set; }

    //}
}