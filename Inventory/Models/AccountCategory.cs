﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class AccountCategory
    {
        public Int32 AccountCategoryId { get; set; }
        public string AccountCategoryName { get; set; }
        public string Code { get; set; }
        public Int32 BalanceSheetTypeId { get; set; }
        public virtual BalanceSheetType BalanceSheetType { get; set; }
        public virtual List<Account> Accounts { get; set; }
        public Int32 AccountTypeId { get; set; }
        public virtual AccountType AccountType { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
     
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }
        public Int32? AccountsDataTypesId { get; set; }
        public virtual AccountsDataTypes AccountsDataTypes { get; set; }
       
    }
}