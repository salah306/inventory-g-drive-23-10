﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class PurchaseOrder
    {
        public Int32 PurchaseOrderId { get; set; }
        public int orderNo { get; set; }
        public DateTime orderDate { get; set; }
        public DateTime orderDateSecond { get; set; }
        public DateTime orderDateThird { get; set; }
        public DateTime RecivedDate { get; set; }
        public string userFirstId { get; set; }
        [ForeignKey("userFirstId")]
        public virtual ApplicationUser userFirst { get; set; }
        public string userSecondId { get; set; }
        [ForeignKey("userSecondId")]
        public virtual ApplicationUser userSecond { get; set; }
        public string userThirdId { get; set; }
        [ForeignKey("userThirdId")]
        public virtual ApplicationUser userThird { get; set; }
        public bool isDoneFirst { get; set; }
        public bool isDoneSecond { get; set; }
        public bool isDoneThird { get; set; }
        public bool cancel { get; set; }
        public bool isNewOrder { get; set; }
        public Int32 TransferToId { get; set; }
        [ForeignKey("TransferToId")]
        public virtual Account TransferTo { get; set; }
        public virtual List<ItemsRequest> itemsRequest { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public DateTime? cancelDate { get; set; }
        public string userCancelId { get; set; }
        [ForeignKey("userCancelId")]
        public virtual ApplicationUser userCancel { get; set; }
        public Int32? orderONo { get; set; }
        public string OType { get; set; }
        public string OrderType { get; set; }
    }
    public class ItemsRequest
    {
        public Int32 ItemsRequestId { get; set; }
        public Int32 PurchaseOrderId { get; set; }
        public virtual PurchaseOrder PurchaseOrder { get; set; }
        public Int32 SubAccountId { get; set; }
        [ForeignKey("SubAccountId")]
        public virtual SubAccount SubAccount { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public decimal LastCostPrice { get; set; }
        public decimal CurrentCostPrice { get; set; }
        public string note { get; set; }
        public string noteSecond { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string Realted { get; set; }
    }

    public class InventoryRequestOrderVm
    {
        public Int32 orderId { get; set; }
        public int orderNo { get; set; }
        public int ONo { get; set; }
        public Int32 OrderOId { get; set; }

        public DateTime orderDate { get; set; }
        public DateTime recivedDate { get; set; }
        public DateTime orderDateSecond { get; set; }
        public DateTime orderDateThird { get; set; }
        public string userName { get; set; }
        public string userNameSecond { get; set; }
        public string userNameThird { get; set; }
        public bool isDoneFirst { get; set; }
        public bool isDoneSecond { get; set; }
        public bool isDoneThird { get; set; }
        public bool cancel { get; set; }
        public bool isNewOrder { get; set; }

        public purOrderRequestTo TransferTo { get; set; }
        public IEnumerable<ItemsRequestVm> itemsRequest { get; set; }

        public Int32 CompanyId { get; set; }
        public DateTime cancelDate { get; set; }
        public string userNameCancel { get; set; }
        public string OrderType { get; set; }
    }

    public class PurchaseOrderVm
    {
        public Int32 PurchaseOrderId { get; set; }
        public int orderNo { get; set; }
        public DateTime orderDate { get; set; }
        public DateTime recivedDate { get; set; }
        public DateTime orderDateSecond { get; set; }
        public DateTime orderDateThird { get; set; }
        public string userName { get; set; }

        public string userNameSecond { get; set; }

        public string userNameThird { get; set; }

        public bool isDoneFirst { get; set; }
        public bool isDoneSecond { get; set; }
        public bool isDoneThird { get; set; }
        public bool cancel { get; set; }
        public bool isNewOrder { get; set; }

        public purOrderRequestTo TransferTo { get; set; }
        public List<ItemsRequestVm> itemsRequest { get; set; }
     
        public Int32 CompanyId { get; set; }
        public DateTime cancelDate { get; set; }
        public string userNameCancel { get; set; }
   
    }

    public class purOrderRequestTo
    {
        public Int32 inventoryId { get; set; }
        public string inventoryName { get; set; }
        public string code { get; set; }
    }

    public class ItemsRequestVm
    {
        public Int32 ItemsRequestId { get; set; }
        public Int32 itemId { get; set; }
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public string note { get; set; }
        public string noteSecond { get; set; }
        public string itemUnitName { get; set; }
        public string itemUnitType { get; set; }
        public string itemGroupName { get; set; }
        public decimal maxQ { get; set; }

    }

    public class getPurRequest
    {
        public Int32 orderNo { get; set; }
    }

    public class getorderO
    {
        public Int32 orderNo { get; set; }
        public string orderType { get; set; }
        public string type { get; set; }
    }


    public class getInventoryRequestes
    {
        public string orderType { get; set; }
        public Int32 inventoryId { get; set; }
        public string type { get; set; }
        public Int32 OrderNo { get; set; }
        public Int32 orderId { get; set; }
    }
    public class getwaitinPurRequest
    {
        public DateTime MinDate { get; set; }
        public DateTime MaxDate { get; set; }
        public Int32 pageSize { get; set; }
        public Int32 pageNumber { get; set; }
        public string orderby { get; set; }
        public Int32 orderNo { get; set; }
        public bool isDoneFirst { get; set; }
        public bool isDoneSecond { get; set; }
        public bool isDoneThird { get; set; }
        public bool cancel { get; set; }
        public bool all { get; set; }
        public string OrderType { get; set; }
        public string OType { get; set; }

    }

    #region InventoryRequest
    public class InventoryRequest 
    {
        public Int32 InventoryRequestId { get; set; }
        public int orderNo { get; set; }
        public DateTime orderDate { get; set; }
        public DateTime orderDateSecond { get; set; }
        public DateTime orderDateThird { get; set; }
        public DateTime RecivedDate { get; set; }
        public string userFirstId { get; set; }
        [ForeignKey("userFirstId")]
        public virtual ApplicationUser userFirst { get; set; }
        public string userSecondId { get; set; }
        [ForeignKey("userSecondId")]
        public virtual ApplicationUser userSecond { get; set; }
        public string userThirdId { get; set; }
        [ForeignKey("userThirdId")]
        public virtual ApplicationUser userThird { get; set; }
        public bool isDoneFirst { get; set; }
        public bool isDoneSecond { get; set; }
        public bool isDoneThird { get; set; }
        public bool cancel { get; set; }
        public bool isNewOrder { get; set; }
        [ValidationAccountbyCompany(ErrorMessage = "بعض الحسابات لا يمكن الدخول اليها تاكد من اختيارك للحساب الصحيح او انك تملك الصلاحيات اللازمة : اخيار خاطيء للمخزن")]
        public Int32 TransferToId { get; set; }
        [ForeignKey("TransferToId")]
        public virtual Account TransferTo { get; set; }
        [EnsureOneElement(ErrorMessage = "يجب ادخال صنف واحد علي الاقل")]
        [Display(Name = "الاصناف")]
        public virtual List<InventoryItemsRequest> itemsRequest { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public DateTime? cancelDate { get; set; }
        public string userCancelId { get; set; }
        [ForeignKey("userCancelId")]
        public virtual ApplicationUser userCancel { get; set; }
        public Int32? orderONo { get; set; }
        public string OType { get; set; }
        public string OrderType { get; set; }
      
    }
    public class InventoryItemsRequest
    {
        public Int32 InventoryItemsRequestId { get; set; }
        public Int32 InventoryRequestId { get; set; }
        public virtual InventoryRequest InventoryRequest { get; set; }
        [ValidationSubAccountbyCompany(ErrorMessage = "بعض الحسابات لا يمكن الدخول اليها تاكد من اختيارك للحساب الصحيح او انك تملك الصلاحيات اللازمة : اختار خاطيء للصنف")]

        public Int32 SubAccountId { get; set; }
        [ForeignKey("SubAccountId")]
        public virtual SubAccount SubAccount { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public decimal LastCostPrice { get; set; }
        public decimal CurrentCostPrice { get; set; }
        public string note { get; set; }
        public string noteSecond { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string Realted { get; set; }
    }



    public class InventoryRequestVm
    {
        public Int32 InventoryRequestId { get; set; }
        public int orderNo { get; set; }
        public DateTime orderDate { get; set; }
        public DateTime recivedDate { get; set; }
        public DateTime orderDateSecond { get; set; }
        public DateTime orderDateThird { get; set; }
        public string userName { get; set; }

        public string userNameSecond { get; set; }

        public string userNameThird { get; set; }

        public bool isDoneFirst { get; set; }
        public bool isDoneSecond { get; set; }
        public bool isDoneThird { get; set; }
        public bool cancel { get; set; }
        public bool isNewOrder { get; set; }

        public InventoryRequestTo TransferTo { get; set; }
        public List<InventoryItemsRequestVm> itemsRequest { get; set; }

        public Int32 CompanyId { get; set; }
        public DateTime cancelDate { get; set; }
        public string userNameCancel { get; set; }

    }

    public class InventoryRequestTo
    {
        public Int32 inventoryId { get; set; }
        public string inventoryName { get; set; }
        public string code { get; set; }
    }

    public class InventoryItemsRequestVm
    {
        public Int32 InventoryItemsRequestId { get; set; }
        public Int32 itemId { get; set; }
        public string itemCode { get; set; }
        public string itemName { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public string note { get; set; }
        public string noteSecond { get; set; }
        public string itemUnitName { get; set; }
        public string itemUnitType { get; set; }
        public string itemGroupName { get; set; }
        public string realted { get; set; }

    }

    public class getInventoryRequest
    {
        public Int32 orderNo { get; set; }
    }
    public class getwaitinInventoryRequest
    {
        public DateTime MinDate { get; set; }
        public DateTime MaxDate { get; set; }
        public Int32 pageSize { get; set; }
        public Int32 pageNumber { get; set; }
        public string orderby { get; set; }
        public Int32 orderNo { get; set; }
        public bool isDoneFirst { get; set; }
        public bool isDoneSecond { get; set; }
        public bool isDoneThird { get; set; }
        public bool cancel { get; set; }
        public bool all { get; set; }
    }
    #endregion
}