﻿using Inventory.DataLayer;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;


namespace Inventory.Models
{


    #region SalesOrdersConfiguration
    public class SalesOrdersConfiguration
    {
        public Int32 SalesOrdersConfigurationId { get; set; }
        [Required]
        public string SalesOrderName { get; set; }
        [Required]
        public string SalesName { get; set; }
        [Required]
        public string SalesReturnName { get; set; }
        public bool IsActiveOrder { get; set; }
        public bool OnlyInventory { get; set; }
        public bool DirectGlTransaction { get; set; }
        public string TitleAccountNickName { get; set; }
        public Int32 TitleAccountCategoryId { get; set; }
        [ForeignKey("TitleAccountCategoryId")]
        public virtual AccountCategory TitleAccountCategory { get; set; }
        public Int32? TitleBalanceSheetTypeId { get; set; }
        [ForeignKey("TitleBalanceSheetTypeId")]
        public virtual BalanceSheetType TitleBalanceSheetType { get; set; }

        public Int32? DefaultPayAccountCategoryId { get; set; }
        [ForeignKey("DefaultPayAccountCategoryId")]
        public virtual AccountCategory DefaultPayAccountCategory { get; set; }

        [EnsureOneElement(ErrorMessage = "يجب ادخال طريقة دفع واحدة علي الاقل")]
        public virtual List<SalessOrdersConfigurationPayMethod> PayMethod { get; set; }
        public virtual List<SalessOrdersConfigurationTerm> Terms { get; set; }
        public virtual List<SalessOrdersConfigurationTableAccount> TableAccounts { get; set; }
        public virtual List<SalessOrdersConfigurationTotalAccount> TotalAccounts { get; set; }

        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

    }

    public class SalessOrdersConfigurationPayMethod
    {
        public Int32 SalessOrdersConfigurationPayMethodId { get; set; }
        public Int32 SalesOrdersConfigurationId { get; set; }
        [ForeignKey("SalesOrdersConfigurationId")]
        public virtual SalesOrdersConfiguration SalesOrdersConfiguration { get; set; }
        public string PayName { get; set; }
        public Int32 AccountCategoryId { get; set; }
        [ForeignKey("AccountCategoryId")]
        public virtual AccountCategory AccountCategory { get; set; }
        public Int32? DefaultChildAccountId { get; set; }
        [ForeignKey("DefaultChildAccountId")]
        public virtual Account DefaultChildAccount { get; set; }
        public byte[] RowVersion { get; set; }

        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }

    }
    public class SalessOrdersConfigurationTerm
    {
        public Int32 SalessOrdersConfigurationTermId { get; set; }
        public string Note { get; set; }
        public Int32 SalesOrdersConfigurationId { get; set; }
        [ForeignKey("SalesOrdersConfigurationId")]
        public virtual SalesOrdersConfiguration SalesOrdersConfiguration { get; set; }
        public byte[] RowVersion { get; set; }

        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
    }
    public class SalessOrdersConfigurationTableAccount
    {
        public Int32 SalessOrdersConfigurationTableAccountId { get; set; }
        public virtual Int32 AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
        public string AccountNickName { get; set; }
        public Int32 AccountTypeId { get; set; }
        [ForeignKey("AccountTypeId")]
        public virtual SalessOrdersConfigurationAccountType AccountType { get; set; }
        public Int32 SalesOrdersConfigurationId { get; set; }
        [ForeignKey("SalesOrdersConfigurationId")]
        public virtual SalesOrdersConfiguration SalesOrdersConfiguration { get; set; }
        public string CurrentWorkerTableAccountId { get; set; }
        [ForeignKey("CurrentWorkerTableAccountId")]
        public virtual ApplicationUser CurrentWorkerTableAccount { get; set; }
        #region This part is for requestes and inventories and others uses in table header
        public Int32? itemId { get; set; }
        public string itemName { get; set; }
        public string itemCode { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public decimal itemcostPrice { get; set; }
        public string realted { get; set; }
        public Int32? ItemGroupId { get; set; }
        public virtual ItemGroup ItemGroup { get; set; }
        public bool isInventory { get; set; }
        public bool isQty { get; set; }
        public string serialNo { get; set; }
        public DateTime? expireDate { get; set; }
        public bool attchedRequest { get; set; }
        public bool isPercent { get; set; }
        public bool IsAccepted { get; set; }
        public bool IsCanceled { get; set; }
        public string firstItemNote { get; set; }
        public string SecondItemNote { get; set; }
        public string thirdItemNote { get; set; }
        public Int32? firstItemNoteId { get; set; }
        public Int32? SecondItemNoteId { get; set; }
        public Int32? thirdItemNoteId { get; set; }
        public Int32? firstItemAccountNoteId { get; set; }
        [ForeignKey("firstItemAccountNoteId")]
        public virtual Account firstItemAccountNote { get; set; }
        public Int32? secondItemAccountNoteId { get; set; }
        [ForeignKey("secondItemAccountNoteId")]
        public virtual Account secondItemAccountNote { get; set; }
        public Int32? thirdItemAccountNoteId { get; set; }
        [ForeignKey("thirdItemAccountNoteId")]
        public virtual Account thirdItemAccountNote { get; set; }
        public DateTime? firstItemNoteDate { get; set; }
        public DateTime? secondItemNoteDate { get; set; }
        public DateTime? thirdItemNoteDate { get; set; }
        public Int32? firstReferenceId { get; set; }
        public Int32? firstReferenceType { get; set; }
        public Int32? secondReferenceId { get; set; }
        public Int32? secondReferenceType { get; set; }
        public Int32? thirdReferenceId { get; set; }
        public Int32? thirdReferenceType { get; set; }
        public DateTime? cancelDate { get; set; }
        public bool doneFirst { get; set; }
        public bool doneSceond { get; set; }
        public bool doneThird { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string CurrentWorkerSecondId { get; set; }
        [ForeignKey("CurrentWorkerSecondId")]
        public virtual ApplicationUser CurrentWorkerSecond { get; set; }
        public string CurrentWorkerCancelId { get; set; }
        [ForeignKey("CurrentWorkerCancelId")]
        public virtual ApplicationUser CurrentWorkerCancel { get; set; }
        #endregion

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }                 

    public class SalessOrdersConfigurationTotalAccount
    {
        public Int32 SalessOrdersConfigurationTotalAccountId { get; set; }
        public virtual Int32 AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
        public string AccountNickName { get; set; }
        public Int32 AccountTypeId { get; set; }
        [ForeignKey("AccountTypeId")]
        public virtual SalessOrdersConfigurationAccountType AccountType{ get; set; }
        public Int32 SalesOrdersConfigurationId { get; set; }
        [ForeignKey("SalesOrdersConfigurationId")]
        public virtual SalesOrdersConfiguration SalesOrdersConfiguration { get; set; }
        public byte[] RowVersion { get; set; }

        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
    }

    public class SalessOrdersConfigurationAccountType
    {
        public Int32 SalessOrdersConfigurationAccountTypeId { get; set; }
        public string AccountTypeName { get; set; }

    }

    #region SalesOrdersConfigurationVm
    public class SalesOrdersConfigurationVm
    {
        public Int32 salesId { get; set; }
        [Required(ErrorMessage ="يجب ادخال اسم النوع للاستمرار")]
        [Index(IsUnique = true)]
        public string salesOrderName { get; set; }
        [Required(ErrorMessage ="يجب ادخال عنوان امر البيع")]
        public string salesName { get; set; }
        [Required(ErrorMessage = "يجب ادخال عنوان امر رد المبيعات")]
        public string salesReturnName { get; set; }
        public bool salesOrderIsNew { get; set; }
        [EnsureOneElement(ErrorMessage = "يجب ادخال طريقة دفع واحدة علي الاقل")]

        public List<SalesPayMethodVm> payMethod { get; set; }
        
        public Int32 defaultPayName { get; set; }
        public List<SalesTermVm> terms { get; set; }
        public List<SalesTableAccountVm> tableAccounts { get; set; }
        public List<SalesTotalAccountVm> totalAccounts { get; set; }
    }
    public class SalesPayMethodVm
    {
        public Int32 payMethodId { get; set; }
        [Required(ErrorMessage ="يجب ادخال اسم طريقة الدفع")]
        public string payName { get; set; }
        [Required]
        public string accountName { get; set; }
        [Index(IsUnique =true)]
        [ValidationAccountCatogerybyCompany(ErrorMessage = "حسابات طرق الدفع غير مسجلة")]
        public int accountId { get; set; }
        public bool attchedRequest { get; set; }
        public Int32? defaultChildAccountId { get; set; }
    }

    public class SalesTermVm
    {
        public Int32 termId { get; set; }
        [Required(ErrorMessage ="يجب ادخال الشروط والاحكام")]
        public string name { get; set; }
    }

    public class SalesTableAccountVm
    {
        public Int32 tableAccountId { get; set; }
        public string nickName { get; set; }
        [Required]
        [Index(IsUnique = true)]
        public string accountName { get; set; }
        [Required]
        [Index(IsUnique = true)]
        [ValidationAccountbyCompany(ErrorMessage = "بعض الحسابات في حسابات الاجمالي غير مسجلة")]

        public Int32 accountId { get; set; }
        [Required]
        public string accountType { get; set; }
    }

    public class SalesTotalAccountVm
    {
        public Int32 totalAccountId { get; set; }
        public string nickName { get; set; }
        [Required]
        [Index(IsUnique = true)]
        public string accountName { get; set; }
        [Required]
        [Index(IsUnique = true)]
        [ValidationAccountbyCompany(ErrorMessage = "بعض الحسابات في حسابات الاجمالي غير مسجلة")]
        public Int32 accountId { get; set; }
        public string accountType { get; set; }
    }
    #endregion



    #endregion

    #region PurchaseOrdersConfiguration
    public class PurchaseOrdersConfiguration
    {
        public Int32 PurchaseOrdersConfigurationId { get; set; }
        [Required]
        public string PurchaseOrderName { get; set; }
        [Required]
        public string PurchaseName { get; set; }
        [Required]
        public string PurchaseReturnName { get; set; }
        public bool IsActiveOrder { get; set; }
        public bool OnlyInventory { get; set; }
        public bool DirectGlTransaction { get; set; }
        public string TitleAccountNickName { get; set; }
        public Int32 TitleAccountCategoryId { get; set; }
        [ForeignKey("TitleAccountCategoryId")]
        public virtual AccountCategory TitleAccountCategory { get; set; }
        public Int32? TitleBalanceSheetTypeId { get; set; }
        [ForeignKey("TitleBalanceSheetTypeId")]
        public virtual BalanceSheetType TitleBalanceSheetType { get; set; }

        public Int32? DefaultPayAccountCategoryId { get; set; }
        [ForeignKey("DefaultPayAccountCategoryId")]
        public virtual AccountCategory DefaultPayAccountCategory { get; set; }
        public virtual List<PurchasesOrdersConfigurationPayMethod> PayMethod { get; set; }
        public virtual List<PurchasesOrdersConfigurationTerm> Terms { get; set; }
        public virtual List<PurchasesOrdersConfigurationTableAccount> TableAccounts { get; set; }
        public virtual List<PurchasesOrdersConfigurationTotalAccount> TotalAccounts { get; set; }

        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }



    }
    public class PurchasesOrdersConfigurationPayMethod
    {
        public Int32 PurchasesOrdersConfigurationPayMethodId { get; set; }
        public Int32 PurchaseOrdersConfigurationId { get; set; }
        [ForeignKey("PurchaseOrdersConfigurationId")]
        public virtual PurchaseOrdersConfiguration PurchaseOrdersConfiguration { get; set; }
        public string PayName { get; set; }
        public Int32 AccountCategoryId { get; set; }
        [ForeignKey("AccountCategoryId")]
        public virtual AccountCategory AccountCategory { get; set; }
        public Int32? DefaultChildAccountId { get; set; }
        [ForeignKey("DefaultChildAccountId")]
        public virtual Account DefaultChildAccount { get; set; }
        public byte[] RowVersion { get; set; }

        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }

    }
    public class PurchasesOrdersConfigurationTerm
    {
        public Int32 PurchasesOrdersConfigurationTermId { get; set; }
        public string Note { get; set; }
        public Int32 PurchaseOrdersConfigurationId { get; set; }
        [ForeignKey("PurchaseOrdersConfigurationId")]
        public virtual PurchaseOrdersConfiguration PurchaseOrdersConfiguration { get; set; }
        public byte[] RowVersion { get; set; }

        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
    }
    public class PurchasesOrdersConfigurationTableAccount
    {
        public Int32 PurchasesOrdersConfigurationTableAccountId { get; set; }
        public virtual Int32 AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
        public string AccountNickName { get; set; }
        public Int32 AccountTypeId { get; set; }
        [ForeignKey("AccountTypeId")]
        public virtual PurchasesOrdersConfigurationAccountType AccountType { get; set; }
        public Int32 PurchaseOrdersConfigurationId { get; set; }
        [ForeignKey("PurchaseOrdersConfigurationId")]
        public virtual PurchaseOrdersConfiguration PurchaseOrdersConfiguration { get; set; }
        public string CurrentWorkerTableAccountId { get; set; }
        [ForeignKey("CurrentWorkerTableAccountId")]
        public virtual ApplicationUser CurrentWorkerTableAccount { get; set; }
        #region This part is for requestes and inventories and others uses in table header
        public Int32? itemId { get; set; }
        public string itemName { get; set; }
        public string itemCode { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public decimal itemcostPrice { get; set; }
        public string realted { get; set; }
        public Int32? ItemGroupId { get; set; }
        public virtual ItemGroup ItemGroup { get; set; }
        public bool isInventory { get; set; }
        public bool isQty { get; set; }
        public string serialNo { get; set; }
        public DateTime? expireDate { get; set; }
        public bool attchedRequest { get; set; }
        public bool isPercent { get; set; }
        public bool IsAccepted { get; set; }
        public bool IsCanceled { get; set; }
        public string firstItemNote { get; set; }
        public string SecondItemNote { get; set; }
        public string thirdItemNote { get; set; }
        public Int32? firstItemNoteId { get; set; }
        public Int32? SecondItemNoteId { get; set; }
        public Int32? thirdItemNoteId { get; set; }
        public Int32? firstItemAccountNoteId { get; set; }
        [ForeignKey("firstItemAccountNoteId")]
        public virtual Account firstItemAccountNote { get; set; }
        public Int32? secondItemAccountNoteId { get; set; }
        [ForeignKey("secondItemAccountNoteId")]
        public virtual Account secondItemAccountNote { get; set; }
        public Int32? thirdItemAccountNoteId { get; set; }
        [ForeignKey("thirdItemAccountNoteId")]
        public virtual Account thirdItemAccountNote { get; set; }
        public DateTime? firstItemNoteDate { get; set; }
        public DateTime? secondItemNoteDate { get; set; }
        public DateTime? thirdItemNoteDate { get; set; }
        public Int32? firstReferenceId { get; set; }
        public Int32? firstReferenceType { get; set; }
        public Int32? secondReferenceId { get; set; }
        public Int32? secondReferenceType { get; set; }
        public Int32? thirdReferenceId { get; set; }
        public Int32? thirdReferenceType { get; set; }
        public DateTime? cancelDate { get; set; }
        public bool doneFirst { get; set; }
        public bool doneSceond { get; set; }
        public bool doneThird { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string CurrentWorkerSecondId { get; set; }
        [ForeignKey("CurrentWorkerSecondId")]
        public virtual ApplicationUser CurrentWorkerSecond { get; set; }
        public string CurrentWorkerCancelId { get; set; }
        [ForeignKey("CurrentWorkerCancelId")]
        public virtual ApplicationUser CurrentWorkerCancel { get; set; }
        #endregion

        [Timestamp]
        public byte[] RowVersion { get; set; }
    }
    public class PurchasesOrdersConfigurationTotalAccount
    {
        public Int32 PurchasesOrdersConfigurationTotalAccountId { get; set; }
        public virtual Int32 AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
        public string AccountNickName { get; set; }
        public Int32 AccountTypeId { get; set; }
        [ForeignKey("AccountTypeId")]
        public virtual PurchasesOrdersConfigurationAccountType AccountType { get; set; }
        public Int32 PurchaseOrdersConfigurationId { get; set; }
        [ForeignKey("PurchaseOrdersConfigurationId")]
        public virtual PurchaseOrdersConfiguration PurchaseOrdersConfiguration { get; set; }
        public byte[] RowVersion { get; set; }

        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
    }
    public class PurchasesOrdersConfigurationAccountType
    {
        public Int32 PurchasesOrdersConfigurationAccountTypeId { get; set; }
        public string AccountTypeName { get; set; }

    }

    #region PurchaseOrdersConfigurationVm
    public class PurchaseOrdersConfigurationVm
    {
        public Int32 PurId { get; set; }
        [Required(ErrorMessage = "يجب ادخال اسم النوع للاستمرار")]
        [Index(IsUnique = true)]
        public string PurOrderName { get; set; }
        [Required(ErrorMessage = "يجب ادخال عنوان امر الشراء")]
        public string PurName { get; set; }
        [Required(ErrorMessage = "يجب ادخال عنوان امر رد المشتريات")]
        public string PurReturnName { get; set; }
        public bool PurOrderIsNew { get; set; }
        [EnsureOneElement(ErrorMessage = "يجب ادخال طريقة دفع واحدة علي الاقل")]

        public List<PurchasePayMethodVm> payMethod { get; set; }
        public Int32 defaultPayName { get; set; }
        public List<PurchaseTermVm> terms { get; set; }
        public List<PurchaseTableAccountVm> tableAccounts { get; set; }
        public List<PurchaseTotalAccountVm> totalAccounts { get; set; }
    }
    public class PurchasePayMethodVm
    {
        public Int32 payMethodId { get; set; }
        [Required(ErrorMessage = "يجب ادخال اسم طريقة الدفع")]
        public string payName { get; set; }
        [Required]
        public string accountName { get; set; }
        [Index(IsUnique = true)]
        [ValidationAccountCatogerybyCompany(ErrorMessage ="بعض الحسابات في طرق الدفع غير مسجلة")]
        public int accountId { get; set; }
        public bool attchedRequest { get; set; }
        public Int32? defaultChildAccountId { get; set; }
    }

    public class PurchaseTermVm
    {
        public Int32 termId { get; set; }
        [Required(ErrorMessage = "يجب ادخال الشروط والاحكام")]
        public string name { get; set; }
    }

    public class PurchaseTableAccountVm
    {
        public Int32 tableAccountId { get; set; }
        public string nickName { get; set; }
        [Required]
        [Index(IsUnique = true)]
        public string accountName { get; set; }
        [Required]
        [Index(IsUnique = true)]
        [ValidationAccountbyCompany(ErrorMessage = "بعض الحسابات في حسابات الجدول غير مسجلة")]

        public Int32 accountId { get; set; }
        [Required]
        public string accountType { get; set; }
    }

    public class PurchaseTotalAccountVm
    {
        public Int32 totalAccountId { get; set; }
        public string nickName { get; set; }
        [Required]
        [Index(IsUnique = true)]
        public string accountName { get; set; }
        [Required]
        [Index(IsUnique = true)]
        [ValidationAccountbyCompany(ErrorMessage = "بعض الحسابات في حسابات الاجمالي غير مسجلة")]

        public Int32 accountId { get; set; }
        public string accountType { get; set; }
    }
    #endregion



    #endregion


    #region Inventory

    public class InventoriesConfigurtion
    {
        public Int32 InventoriesConfigurtionId { get; set; }
        public Int32 InventoryId { get; set; }
        [ForeignKey("InventoryId")]
        public virtual Account Inventory { get; set; }
        public Int32 SalesCostAccountId { get; set; }
        [ForeignKey("SalesCostAccountId")]
        public virtual Account SalesCostAccount { get; set; }

        public Int32 SalesAccountId { get; set; }
        [ForeignKey("SalesAccountId")]
        public virtual Account SalesAccount { get; set; }
     
        public Int32 SalesReturnId { get; set; }
        [ForeignKey("SalesReturnId")]
        public virtual Account SalesReturn { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }


    }

    public class inventoriesConfigurtionVm
    {
        public Int32 inventoryId { get; set; }
        public string inventoryName { get; set; }
        public Int32 salesCostAccountId { get; set; }
        public string salesCostAccountName { get; set; }
        public Int32 salesAccountId { get; set; }
        public string salesAccountName { get; set; }
        public Int32 salesReturnId { get; set; }
        public string salesReturnName { get; set; }

    }
    #endregion

    #region globl order config class
    public class SetOrdersConfigurations
    {
        public Int32 orderId{ get; set; }
        public string type { get; set; }
    }
    public class SetOrdersConfigurationsO
    {
        public Int32 orderId { get; set; }
        public string type { get; set; }
        public string orderType { get; set; }
    }
    #endregion


    #region order pur sales etc...
    #region OrderO

    public class AccountO
    {
        public Int32 AccountOId { get; set; }
        [ValidationAccountbyCompany(ErrorMessage = "بعض الحسابات لا يمكن الدخول اليها تاكد من اختيارك للحساب الصحيح او انك تملك الصلاحيات اللازمة : اختيار خاطي لحسابات جدول الاصناف")]

        public Int32 AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
        public decimal amount { get; set; }
        public string amountType { get; set; }
        public Int32 OrderOId { get; set; }
        [ForeignKey("OrderOId")]
        public virtual OrderO OrderO { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string name { get; set; }
    }

    public class ItemsRequestO
    {
        public Int32 ItemsRequestOId { get; set; }
        [ValidationSubAccountbyCompany(ErrorMessage = "بعض الاصناف لا يمكن الدخول اليها تاكد من اختيارك للاصناف الصحيحة او انك تملك الصلاحيات اللازمة : اختيار خاطيء للصنف")]

        public Int32 SubAccountId { get; set; }
        [ForeignKey("SubAccountId")]
        public virtual SubAccount SubAccount { get; set; }
        public int itemId { get; set; }
        public string itemName { get; set; }
        public string itemCode { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public decimal costPrice { get; set; }
        public string note { get; set; }
        public string realted { get; set; }
        public virtual List<AccountO> accounts { get; set; }
        public string itemUnitType { get; set; }
        public string itemUnitName { get; set; }
        public decimal? maxQ { get; set; }
        public Int32 OrderOId { get; set; }
        [ForeignKey("OrderOId")]
        public virtual OrderO OrderO { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }

    public class PayO
    {
        public Int32 PayOId { get; set; }
        public Int32 PayId { get; set; }
        public string PayName { get; set; }
        [ValidationAccountbyCompany(ErrorMessage = "بعض الحسابات لا يمكن الدخول اليها تاكد من اختيارك للحساب الصحيح او انك تملك الصلاحيات اللازمة : اختيار خاطي لحساب طريقة الدفع والسداد")]
        public Int32 AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
        public decimal amount { get; set; }
        public DateTime dueDate { get; set; }
        public string note { get; set; }
        public string type { get; set; }
        public Int32 OrderOId { get; set; }
        [ForeignKey("OrderOId")]
        public virtual OrderO OrderO { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
      
    }

    public class TotalAccountO
    {
        public Int32 TotalAccountOId { get; set; }
        public Int32 AccountId { get; set; }
        [ForeignKey("AccountId")]
        public virtual Account Account { get; set; }
        public decimal amount { get; set; }
        public string type { get; set; }
        public string amountType { get; set; }
        public string name { get; set; }
    }

    public class TermO
    {
        public Int32 TermOId { get; set; }
        [Required(ErrorMessage ="يجب ادخال نص للشروط والاحكام")]
        public string termName { get; set; }
        public Int32 OrderOId { get; set; }
        [ForeignKey("OrderOId")]
        public virtual OrderO OrderO { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

    }

    public class OrderO 
    {
        public Int32 OrderOId { get; set; }
        public Int32 orderNo { get; set; }
        [ValidationAccountbyCompany(ErrorMessage = "بعض الحسابات لا يمكن الدخول اليها تاكد من اختيارك للحساب الصحيح او انك تملك الصلاحيات اللازمة : احتيار خاطيء للحساب الرئيسي (العميل ,المورد ...)")]

        public Int32 SubjectAccountId { get; set; }
        [ForeignKey("SubjectAccountId")]
        public virtual Account SubjectAccount { get; set; }
        public Int32 InventoryRequestsId { get; set; }
        [ForeignKey("InventoryRequestsId")]
        public virtual InventoryRequest InventoryRequests { get; set; }
        public string OrderTitleName { get; set; }
        public string OrderConfigName { get; set; }
        public DateTime orderDate { get; set; }
        public DateTime orderDateSecond { get; set; }
        public DateTime orderDateThird { get; set; }
        public DateTime recivedDate { get; set; }
        public string userId { get; set; }
        [ForeignKey("userId")]
        public virtual ApplicationUser user { get; set; }
        public string userSecondId { get; set; }
        [ForeignKey("userSecondId")]
        public virtual ApplicationUser userSecond { get; set; }
        public string userNameThird { get; set; }
        public string userThirdId { get; set; }
        [ForeignKey("userThirdId")]
        public virtual ApplicationUser userThird { get; set; }
        public string cancelById { get; set; }
        [ForeignKey("cancelById")]
        public virtual ApplicationUser cancelBy { get; set; }
        public bool isDoneFirst { get; set; }
        public bool isDoneSecond { get; set; }
        public bool isDoneThird { get; set; }
        public bool cancel { get; set; }
        public bool isNewOrder { get; set; }
        [ValidationAccountbyCompany(ErrorMessage = "بعض الحسابات لا يمكن الدخول اليها تاكد من اختيارك للحساب الصحيح او انك تملك الصلاحيات اللازمة : اختيار خاطيء للمخزن")]
        public Int32 TransferToOId { get; set; }
        [ForeignKey("TransferToOId")]
        public virtual Account TransferToO { get; set; }
        public virtual List<ItemsRequestO> itemsRequest { get; set; }
        public virtual List<PayO> pay { get; set; }
        public virtual List<TotalAccountO> totalAccount { get; set; }
        public virtual List<TermO> terms { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public Int32? PurchaseOrderId { get; set; }
        [ForeignKey("PurchaseOrderId")]
        public virtual PurchaseOrder PurchaseOrder { get; set; }
        public bool requestOrderAttched { get; set; }
        public Int32? RefrenceRequestId { get; set; }
        public string RefrenceRequestName { get; set; }
        public Int32? RefrenceInventoryOrderId { get; set; }
        public string RefrenceInventoryOrderName { get; set; }
        public Int32? RefrenceQueryId { get; set; }
        public string RefrenceQueryName { get; set; }
        public Int32? RefrenceBillId { get; set; }
        public string RefrenceBillName { get; set; }
        public Int32? RefrenceOrderConfigId { get; set; }
        [ForeignKey("RefrenceOrderConfigId")]
        public PurchaseOrdersConfiguration RefrenceOrderConfig { get; set; }

        public string orderType { get; set; }
        public string type { get; set; }

    }
    #endregion

    #region orderOVm
    public class TransferToOVm
    {
        public int inventoryId { get; set; }
        public string inventoryName { get; set; }
        public string code { get; set; }
    }
    public class SubjectAccountOVm
    {
        public int SubjectAccountId { get; set; }
        public string SubjectAccountName { get; set; }
        public string code { get; set; }
    }
    public class AccountOVm
    {
        public string name { get; set; }
        public int id { get; set; }
        public decimal amount { get; set; }
        public string type { get; set; }
        public string amountType { get; set; }
    }

    public class ItemsRequestOVm
    {
        [Required(ErrorMessage = "يجب تحديد الصنف")]
        public int itemId { get; set; }
        [Required (ErrorMessage = "اسم الصنف مطلوب لاتمام عملية الحفظ")]
        [Display(Name = "اسم الصنف")]
        public string itemName { get; set; }
        [Required(ErrorMessage = "كود الصنف مطلوب لاتمام عملية الحفظ")]
        [Display(Name = "الكود")]
        public string itemCode { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public string note { get; set; }
        public string realted { get; set; }
        public List<AccountOVm> accounts { get; set; }
        public string itemUnitType { get; set; }
        public string itemUnitName { get; set; }
        public decimal? maxQ { get; set; }
    }

    public class PayOVm
    {
        public int payId { get; set; }
        [Required(ErrorMessage ="يجب تحديد طريقة الدفع")]
        [Display(Name ="طريقة الدفع")]
        public string payName { get; set; }
        public Int32 accountId { get; set; }
        public string accountName { get; set; }
        public string code { get; set; }
        public decimal amount { get; set; }
        [Required(ErrorMessage ="يجب تحديد تاريخ الاستحقاق ")]
        [Display(Name = "التاريخ")]
        public string dueDate { get; set; }
        public string note { get; set; }
        public string type { get; set; }
        public Int32? defaultChildAccountId { get; set; }
        public List<payvmAccounts> accounts { get; set; }
    }
    public class payvmAccounts
    {
        public Int32 accountId { get; set; }
        public string accountName { get; set; }
    }
    public class TotalAccountOVm
    {
        public int id { get; set; }
        public string name { get; set; }
        public decimal amount { get; set; }
        public string type { get; set; }
        public string amountType { get; set; }
    }

    public class TermOVm
    {
        public int termId { get; set; }
       
        [Display(Name = "الشروط والاحكام")]
        public string termName { get; set; }
    }

    public class OrderOVm : IValidatableObject
    {
        public Int32 OrderId { get; set; }
        public Int32 orderrequestId { get; set; }
        public bool requestOrderAttched { get; set; }
        public Int32 orderrequestNo { get; set; }
        public int orderNo { get; set; }
        public string orderName { get; set; }
        public Int32 orderConfigId { get; set; }
        public string orderType { get; set; }
        public string oType { get; set; }

        public string orderDate { get; set; }
        public string orderDateSecond { get; set; }
        public string orderDateThird { get; set; }
        public string recivedDate { get; set; }
        public string userName { get; set; }
        public string userNameSecond { get; set; }
        public string userNameThird { get; set; }
        public bool isDoneFirst { get; set; }
        public bool isDoneSecond { get; set; }
        public bool isDoneThird { get; set; }
        public bool cancel { get; set; }
        public bool isNewOrder { get; set; }
        public TransferToOVm transferTo { get; set; }
        public SubjectAccountOVm SubjectAccount { get; set; }
        [EnsureOneElement(ErrorMessage = "يجب ادخال صنف واحد علي الاقل")]
        [Display(Name = "الاصناف")]
        public List<ItemsRequestOVm> itemsRequest { get; set; }
       
        [Display(Name = "طرق الدفع")]
        public List<PayOVm> pay { get; set; }
        public List<TotalAccountOVm> totalAccount { get; set; }
        public List<TermOVm> terms { get; set; }
        public int companyId { get; set; }
        public bool isCompany { get; set; }
        public string cancelDate { get; set; }
        public string userNameCancel { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {

            #region checkTotal
            var grandTotal = 0.0m;
            if(SubjectAccount == null || SubjectAccount.SubjectAccountId == 0)
            {
                string typ = oType == "pur" ? "المورد" : "العميل";
                string ms = string.Format("يجب تحديد {0}", typ);
                yield return new ValidationResult(ms, new[] { "الحساب الرئيسي" });

            }

            if (transferTo == null || transferTo.inventoryId == 0)
            {
                string typ = "المخزن";
                string ms = string.Format("يجب تحديد {0}", typ);
                yield return new ValidationResult(ms, new[] { "المخزن" });

            }
            if (Convert.ToDateTime(orderDate) < Convert.ToDateTime(recivedDate) && isNewOrder)
            {
                yield return new ValidationResult("يجب ان يكون تاريخ الاستلام اكبر من او يساوي تاريخ الاصدار.", new[] {"التواريخ" });

            }
            foreach (var item in itemsRequest)
            {
                if(item.qty <= 0){
                    yield return new ValidationResult( "يجب ان تكون الكمية اكبر من صفر.", new[] { item.itemName + " : " +item.qty });
                }
                if (item.price < 0)
                {
                    yield return new ValidationResult("سعر الصنف لا يجب ان يقل عن صفر.", new[] { item.itemName + " : " + item.price });
                }
                var itemTotal = item.price * item.qty;
                var totalamounts = 0.0m;
                if(item.accounts !=null && item.accounts.Count > 0)
                {
                    foreach (var acc in item.accounts)
                    {
                        if (acc.amount < 0)
                        {
                            yield return new ValidationResult("لا يسمح باستخدام قيمة سالبة.", new[] { item.itemName + " , " + acc.name });
                        }
                        if (acc.amountType == "%")
                        {
                            var transFerToamount = 0.0m;
                            if (acc.type == "crdit")
                            {
                                transFerToamount = (itemTotal + totalamounts) / 100 * acc.amount;
                                totalamounts -= transFerToamount;
                            }
                            if (acc.type == "debit")
                            {
                                transFerToamount = (itemTotal + totalamounts) / 100 * acc.amount;
                                totalamounts += transFerToamount;
                            }
                        }
                        else
                        {
                            if (acc.type == "crdit")
                            {


                                totalamounts -= acc.amount;
                            }
                            if (acc.type == "debit")
                            {
                                totalamounts += acc.amount;
                            }
                        }
                    }
                }

                grandTotal += (itemTotal + totalamounts);
            }
            var totaltable = 0.0m;
            foreach (var acc in totalAccount)
            {
               
                if (acc.amount < 0)
                {
                    yield return new ValidationResult("لا يسمح باستخدام قيم سالبة.", new[] { acc.name + " : " + acc.amount });
                }
                if (acc.amountType == "%")
                {
                    var transFerToamount = 0.0m;
                    if (acc.type == "crdit")
                    {
                        transFerToamount = (grandTotal + acc.amount) / 100 * acc.amount;
                        totaltable -= transFerToamount;
                    }
                    if (acc.type == "debit")
                    {
                        transFerToamount = (grandTotal + totaltable) / 100 * acc.amount;
                        totaltable += transFerToamount;
                    }
                }
                else
                {
                    if (acc.type == "crdit")
                    {


                        totaltable -= acc.amount;
                    }
                    if (acc.type == "debit")
                    {
                        totaltable += acc.amount;
                    }
                }
                
            }
            grandTotal += totaltable;
            if (pay.Count() > 0)
            {
                foreach (var p in pay)
                {
                    if (Convert.ToDateTime(orderDate) < Convert.ToDateTime(p.dueDate) && isNewOrder)
                    {
                        yield return new ValidationResult("يجب ان يكون التاريخ في طرق الدفع اكبر من او يساوي تاريخ الاصدار.", new[] { "التواريخ" });

                    }
                    if (p.accountId == 0)
                    {
                        yield return new ValidationResult("يرجي اختيار طريقة دفع من الطرق المتاحة.", new[] { "طرق الدفع" });

                    }
                    if (string.IsNullOrEmpty(p.dueDate))
                    {
                        yield return new ValidationResult("يجب ادخال تاريخ الاستحقاق المفترض لانشاء طلب للحساب المستهدف.", new[] { "طرق الدفع" });

                    }
                    if (p.amount <= 0)
                    {
                        yield return new ValidationResult("لا يسمح باستخدام قيمة اقل من 1.", new[] { "طرق الدفع" });

                    }
                    if (p.accountId == SubjectAccount.SubjectAccountId)
                    {
                        yield return new ValidationResult("حسابات طرق الدفع يجب ان تكون مختلفه عن الحساب الرئيسي للامر.", new[] { "طرق الدفع" });
                    }
                }
            }
          
          
            #endregion
          if(orderType == "purReturn" || orderType == "sales")
            {
                ApplicationDbContext db = new ApplicationDbContext();
                foreach (var item in itemsRequest)
                {
                    var lastStocktotalQty = db.SubAccountMovements.Where(a => a.SubAccountId == item.itemId && a.SubAccountMovementDate == (db.SubAccountMovements.Where(s => s.SubAccountId == item.itemId).Select(d => d.SubAccountMovementDate).Max())).Select(a => a.ItemStockQuantity).DefaultIfEmpty(0).FirstOrDefault();
                    if (lastStocktotalQty - item.qty < 0)
                    {
                        yield return new ValidationResult(string.Format("كمية غير متاحة للصرف من الصنف {0}", item.itemName), new[] { "صنف غير متاح" });
                    }
                }
                
            }
        }
    }
    #endregion



    public class ordertypeVM
    {
        public string orderType { get; set; }
        public string type { get; set; }
    }
    #endregion

}