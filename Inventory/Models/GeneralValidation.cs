﻿using Inventory.DataLayer;
using Inventory.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class EnsureOneElementAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var list = value as IList;
            if (list != null)
            {
                return list.Count > 0;
            }
            return false;
        }
    }
    public class ValidationAccountCatogerybyCompany : ValidationAttribute
    {

        public override bool IsValid(object value)
        {

            var httpContext = new HttpContextWrapper(HttpContext.Current);
            string uid = httpContext.User.Identity.GetUserId();
            ApplicationDbContext db = new ApplicationDbContext();
            var accId = value as int?;
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var acc = db.AccountCategories.Where(a => a.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.AccountCategoryId == accId).SingleOrDefault();
            if (acc == null)
            {
                return false;
            }


            return true;
        }

    }
    public class ValidationAccountbyCompany : ValidationAttribute
    {

        public override bool IsValid(object value)
        {

            var httpContext = new HttpContextWrapper(HttpContext.Current);
            string uid = httpContext.User.Identity.GetUserId();
            ApplicationDbContext db = new ApplicationDbContext();
            var accId = value as int?;
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var acc = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.AccountId == accId && a.BranchId == (company.isCompany ? (int?)null : company.oId)).SingleOrDefault();
            if (acc == null)
            {
                return false;
            }


            return true;
        }

    }

    public class ValidationSubAccountbyCompany : ValidationAttribute
    {

        public override bool IsValid(object value)
        {

            var httpContext = new HttpContextWrapper(HttpContext.Current);
            string uid = httpContext.User.Identity.GetUserId();
            ApplicationDbContext db = new ApplicationDbContext();
            var accId = value as int?;
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var acc = db.SubAccounts.Where(a => a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.SubAccountId == accId && a.Account.BranchId == (company.isCompany ? (int?)null : company.oId)).SingleOrDefault();
            if (acc == null)
            {
                return false;
            }


            return true;
        }

    }

    public class IsAllPropertyDTOUnique : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var list = value as IList<PropertyDTO>;
            var AllUnique = list.GroupBy(x => x.propertyName).All(g => g.Count() == 1);
            return AllUnique;
        }
    }
}