﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class AccountCategoryProperties
    {
        public Int32 AccountCategoryPropertiesId { get; set; }
        public string AccountCategoryPropertiesName { get; set; }
       
        //public Int32 AccountCategoryId { get; set; }
        //[ForeignKey("AccountCategoryId")]
        //public virtual AccountCategory AccountCategory { get; set; }
        public virtual List<AccountCategoryPropertiesValue> AccountCategoryPropertiesValues { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }

        public Int32 AccountsTypesAcccountsId { get; set; }
        [ForeignKey("AccountsTypesAcccountsId")]
        public virtual AccountsCatogeryTypesAcccounts AccountsCatogeryTypesAcccounts { get; set; }

        public Int32 AccountsDataTypesId { get; set; }
        [ForeignKey("AccountsDataTypesId")]
        public virtual AccountsDataTypes AccountsDataTypes { get; set; }
        public Int32 CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public Company Company { get; set; }
    }

    public class AccountsCatogeryTypesAcccounts
    {
        public Int32 AccountsCatogeryTypesAcccountsId { get; set; }
        public string TypeName { get; set; }
        public virtual List<AccountCategoryProperties> AccountCategoryProperties { get; set; }

    }
    public class AccountsDataTypes
    {
        public Int32 Id { get; set; }
        public string TypeName { get; set; }
        public string aliasName { get; set; }
        public List<OrderRequest> OrderRequests { get; set; }
        public virtual List<AccountCategoryProperties> AccountCategoryProperties { get; set; }

    }
    public class AccountsDataTypesVm
    {
        public Int32 Id { get; set; }
        public string TypeName { get; set; }
        public bool IsTrue { get; set; }

    }

    public class AccountsCatogeryDataTypes
    {
        public Int32 Id { get; set; }
        public string CatogeryName { get; set; }
        public List<AccountsDataTypesVm> AccountsDataTypes { get; set; }

    }

    public class SetAccountsCatogeryDataTypes
    {
        public Int32 accountId { get; set; }
        public string BalanceSheetTypeId{ get; set; }

    }
}