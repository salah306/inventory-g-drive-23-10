﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class InventoryTransferEntity
    {
        public Int32 InventoryTransferEntityId { get; set; }
        public Int32 OrderNo { get; set; }
        public Int32 OrderNoIn { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime OrderDateSecond { get; set; }
        public Int32 MainAccountId { get; set; }
        [ForeignKey("MainAccountId")]
        public virtual Account MainAccount { get; set; }
        public Int32 TitleAccountId { get; set; }
        [ForeignKey("TitleAccountId")]
        public virtual Account TitleAccount { get; set; }
        public bool Cancel { get; set; }
        public DateTime? CancelDate { get; set; }
        public bool DoneFirst { get; set; }
        public bool DoneSceond { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerSecondId { get; set; }
        [ForeignKey("CurrentWorkerSecondId")]
        public virtual ApplicationUser CurrentWorkerSecond { get; set; }
        public string CurrentWorkerCancelId { get; set; }
        [ForeignKey("CurrentWorkerCancelId")]
        public virtual ApplicationUser CurrentWorkerCancel { get; set; }
        public Int32? BranchId { get; set; }
        [ForeignKey("BranchId")]
        public Branch Branch { get; set; }
        public Int32? CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public Company Company { get; set; }
        public bool IsCompany { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual List<InventoryItems> Items { get; set; }
    }

    public class InventoryItems
    {
        //itemId: 47, itemCode: "19823787", itemName: "مروحة توشيبا", qty: 3, realted: "2", groupName: "مراواح"
        public Int32 InventoryItemsId { get; set; }
        public Int32 SubAccountFromId { get; set; }
        [ForeignKey("SubAccountFromId")]
        public virtual SubAccount SubAccountFrom { get; set; }
        public Int32 SubAccountToId { get; set; }
        [ForeignKey("SubAccountToId")]
        public virtual SubAccount SubAccountTo { get; set; }
        public Int32 SubAccountTransitId { get; set; }
        [ForeignKey("SubAccountTransitId")]
        public virtual SubAccount SubAccountTransit { get; set; }
        public string itemName { get; set; }
        public string itemCode { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public decimal costPrice { get; set; }
        public string note { get; set; }
        public string noteSecond { get; set; }
        public Int32 ItemGroupId { get; set; }
        public virtual ItemGroup ItemGroup { get; set; }
        public Int32 InventoryTransferEntityId { get; set; }
        [ForeignKey("InventoryTransferEntityId")]
        public virtual InventoryTransferEntity InventoryTransferEntity { get; set; }
        public string CurrentWorkerId { get; set; }
        [ForeignKey("CurrentWorkerId")]
        public virtual ApplicationUser CurrentWorker { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
    }

    public class InventoryTransferOrderVM
    {
        public Int32 InventoryTransferId { get; set; }
        public Int32 OrderNo { get; set; }
        public Int32 OrderNoIn { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime OrderDateSecond { get; set; }
        public string userNameFirst { get; set; }
        public string userNameSecond { get; set; }
        public bool isDoneFirst { get; set; }
        public bool isDoneSecond { get; set; }
        public bool cancel { get; set; }
        public bool IsNewOrder { get; set; }
        //$scope.transferFrom = { inventoryId: 0, inventoryName: null };
        public inventorytransferAccount transferFrom { get; set; }
        public inventorytransferAccount transferTo { get; set; }
        public List<InventoryItemsVm> transferItems { get; set; }
        public Int32? CompanyId { get; set; }
        public bool IsCompany { get; set; }
        public DateTime CancelDate { get; set; }
        public string userNameCancel { get; set; }
    }
    public class SaveInventoryTransferin
    {
        public Int32 OrderNo { get; set; }
        public DateTime OrderDate { get; set; }
        public Int32 transferFromId { get; set; }
        public List<InventoryItemsInVm> items { get; set; }
    }
    public class InventoryItemsInVm
    {
        public Int32 itemId { get; set; }
        public string noteSecond { get; set; }
    }
    public class InventoryItemsVm
    {
        public Int32 itemId { get; set; }
        public string itemName { get; set; }
        public string itemCode { get; set; }
        public decimal qty { get; set; }
        public decimal price { get; set; }
        public string note { get; set; }
        public string noteSecond { get; set; }
        public string ItemGroupName { get; set; }
        public decimal maxQ { get; set; }
    }

    public class inventorytransferAccount
    {
        public Int32 inventoryId { get; set; }
        public string inventoryName { get; set; }
        public string code { get; set; }
    }
}