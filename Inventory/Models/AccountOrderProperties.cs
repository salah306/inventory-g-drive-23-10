﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class AccountOrderProperties
    {
        public Int32 AccountOrderPropertiesId { get; set; }
        public string AccountOrderPropertiesName { get; set; }
        public string AccountOrderPropertiesCode { get; set; }
        public virtual List<AccountOrder> AccountOrders { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }

        // Consider this one is like types of account order such as Customer order , sales tax order

        //        In reference to accounting :

        //every transaction in certain account affect at least in another account, so when you create order to implement movement on account movement table it will affect others accounts
        //therefore we need to define at least 2 account , so we can get balance sheet correct

        //e.g.customer has got $ 10000 merchandise and he Deposited $ 10000 .
        //to implement this transaction it will effect in these accounts

        //1- sales account will be credited by $10000

        //2- customer account will be debited by $ 10000

        //3 -treasury will be debited by $ 10000

        //4 - customer will be credited by $10000

        //e.g.customer has got $ 10000 merchandise and he Deposited $ 9000 .

        //1- sales account will be credited by 10000

        //2- customer account will be debited by $ 10000

        //3 -treasury will be debited by $ 9000

        //4 - customer will be credited by $9000

    }
}