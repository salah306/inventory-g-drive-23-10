﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Inventory.Models
{
    public class Branch
    {
        public Int32 BranchId { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }

        public Int32 CompanyId { get; set; }
        public virtual Company Company { get; set; }
        public virtual List<AccountMovement> AccountMovements { get; set; }
        public virtual List<ApplicationUserComapny> ApplicationUsersComapnies { get; set; }
        public virtual List<BalanceSheet> BalanceSheets { get; set; }
        
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreatedDate { get; set; }
        public virtual List<BranchProperties> BranchProperties { get; set; }

    }

    public class BranchProperties
    {
        public Int32 BranchPropertiesId { get; set; }
        public Int32 BranchId { get; set; }
        public virtual Branch Branch { get; set; }
        public string propertyName { get; set; }
        public Int32 AccountsCatogeryTypesAcccountsId { get; set; }
        public AccountsCatogeryTypesAcccounts AccountsCatogeryTypesAcccounts { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }
        public string CurrentWorkerId { get; set; }
        public virtual List<BranchPropertyValues> BranchPropertyValues { get; set; }
    }
    public class BranchPropertyValues
    {
        public Int32 BranchPropertyValuesId { get; set; }
        public Int32 BranchPropertiesId { get; set; }
        public virtual BranchProperties BranchProperties { get; set; }
        public string propertyValue { get; set; }
        [Timestamp]
        public byte[] RowVersion { get; set; }

        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
    }

}