﻿using Inventory.DataLayer;
using Inventory.Managers;
using Inventory.Models;
using Inventory.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Inventory.Controllers
{
    [RoutePrefix("api/OrderReqest")]
    [Authorize]
    public class OrderReqestController : BaseApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private OrderReqestManager OrderReqestManager;
        public OrderReqestController()
        {
            this.OrderReqestManager = new OrderReqestManager(this.db);
        }


        #region OrderRequest
        [Route("getInventory")]
        [HttpPost]
        public IHttpActionResult getInventory()
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            if (company == null)
            {
                return BadRequest("الشركة غير متاحة - اختار الشركة او قم باعادة تسجيل الدخول");

            }
            var findInv = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.AccountCategory.AccountsDataTypes.aliasName == "inventory").ToList();
            if (findInv == null)
            {
                return BadRequest("لا يوجد مخازن متاحة للعمليات في هذه الشركة");

            }
            var returninv = findInv.Select(a => new InventoryTransfer()
            {
                inventoryId = a.AccountId,
                inventoryName = a.AccountName,
                code = a.Code
            }).ToList();
            var doneFirst = db.PurchaseOrder.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.cancel == false && a.isDoneSecond == false).Count();
            var donesecond = db.PurchaseOrder.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.cancel == false && a.isDoneSecond == true).Count();
            var returnd = new { waitingcount = doneFirst, workinprogress = donesecond, inventory = returninv };

            return Ok(returnd);
        }
        [Route("getInventoryItems")]
        [HttpPost]
        public IHttpActionResult getInventoryItems(purOrderRequestTo inventory)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            if (company == null)
            {
                return BadRequest("الشركة غير متاحة - اختار الشركة او قم باعادة تسجيل الدخول");

            }
            var it = OrderReqestManager.getItemsByinventoryId(inventory.inventoryId, company);
            if (it == null)
            {
                return BadRequest("لا يوجد اصناف متاحة في هذا المخزن");
            }
            var items = it
                .Select(i => new ItemsRequestVm
                {
                    itemId = i.subAccountId,
                    itemName = i.name,
                    itemCode = i.code,
                    itemUnitName = i.UnitName,
                    itemUnitType = i.UnitType,
                    itemGroupName = i.ItemGroupName,
                    maxQ = i.avQty,
                    price = i.price


                });

            return Ok(items);
        }

        [Route("savePurOrderRequest")]
        [HttpPost]
        public IHttpActionResult savePurOrderRequest(PurchaseOrderVm order)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            if (company == null)
            {
                return BadRequest("الشركة غير متاحة - اختار الشركة او قم باعادة تسجيل الدخول");

            }

            if (order.isNewOrder)
            {
                #region MyRegion
                #region checkingOrder
                var inventory = db.Accounts.SingleOrDefault(a => a.AccountId == order.TransferTo.inventoryId && a.AccountCategory.AccountsDataTypes.aliasName == "inventory");
                if (inventory == null)
                {
                    return BadRequest("المخزن الذي تم اختيارة غير موجود , او غير متاح حاليا");

                }
                foreach (var item in order.itemsRequest)
                {
                    try
                    {
                        var findItem = inventory.SubAccounts.SingleOrDefault(a => a.SubAccountId == item.itemId && a.SubAccountName == item.itemName);
                        if (findItem == null)
                        {
                            return BadRequest("بعض الاصناف غير مسجلة بالمخزن مكان الاستلام - يرجي التاكد من الاصناف");
                        }
                    }
                    catch
                    {
                        return BadRequest("تاكد من عدم تكرار الاصناف");
                    }
                    if (!this.AppCompanyUserManager.CheckDataType(item.qty.ToString(), item.itemUnitType))
                    {
                        return BadRequest("كمية الصنف" + " " + item.itemName + " غير متوافقة مع وحدة قياس الصنق" + " -" + item.itemUnitName);
                    }

                    if (item.qty <= 0)
                    {
                        return BadRequest("تاكد من ان كمية الصنف اكبر صفر");
                    }


                }
                #endregion
                var orderNo = db.PurchaseOrder.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId)).Select(a => a.orderNo).DefaultIfEmpty(0).Max() + 1;

                var orderrequest = new PurchaseOrder()
                {
                    orderNo = orderNo,
                    orderDate = order.orderDate,
                    orderDateSecond = order.orderDate,
                    orderDateThird = order.orderDate,
                    BranchId = company.isCompany ? (int?)null : company.oId,
                    CompanyId = company.companyId,
                    IsCompany = company.isCompany,
                    cancel = false,
                    isDoneFirst = true,
                    isDoneSecond = false,
                    isDoneThird = false,
                    isNewOrder = false,
                    TransferToId = inventory.AccountId,
                    RecivedDate = order.recivedDate,
                    userFirstId = uid,
                    itemsRequest = order.itemsRequest.Select(a => new ItemsRequest()
                    {
                        SubAccountId = a.itemId,
                        qty = a.qty,
                        note = a.note,
                        price = a.price,
                    }).ToList()
                };
                db.PurchaseOrder.Add(orderrequest);
                db.SaveChanges();
                var ordertoReturn = OrderReqestManager.getPurRequestById(orderrequest.orderNo, company);
                return Ok(ordertoReturn);
                #endregion

            }
            else
            {
                #region NotNewOrder
                if (order.cancel)
                {
                    var orderc = db.PurchaseOrder.SingleOrDefault(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.orderNo == order.orderNo && a.isDoneFirst && a.isDoneSecond == false && a.cancel == false);
                    if (orderc == null)
                    {
                        return BadRequest("طلب الشراء غير متاح للالغاء");
                    }
                    orderc.cancel = true;
                    orderc.cancelDate = order.orderDate;
                    orderc.userCancelId = uid;
                    db.Entry(orderc).State = EntityState.Modified;

                    db.SaveChanges();
                }
                return Ok();
                #endregion
            }

        }


        [Route("getOrderRequestById")]
        [HttpPost]
        public IHttpActionResult getOrderRequestById(getPurRequest o)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var order = OrderReqestManager.getPurRequestById(o.orderNo, company);
            if (order == null)
            {
                return BadRequest("طلب الشراء غير مسجل او غير متاح لهذة العملية");
            }
            return Ok(order);
        }


        [Route("getAllOrderpurRequest")]
        [HttpPost]
        public IHttpActionResult getwaitingOrder(getwaitinPurRequest o)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();

            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);

            if (o.orderNo != 0)
            {
                var sorder = db.PurchaseOrder.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.orderNo == o.orderNo)
                    .ToList()
                    .Select(a => new
                    {

                        orderNo = a.orderNo,
                        inventoryto = a.TransferTo.AccountName + " - " + a.TransferTo.Code,
                        date = a.orderDate,
                        Status = a.isDoneSecond && !a.cancel ? "جاري التنفيذ" : (a.cancel ? "الغي بتاريخ " + a.cancelDate.GetValueOrDefault().ToString("yyyy-MM-dd") : (!a.cancel && !a.isDoneSecond ? "لم بتم التاكيد" : (a.isDoneThird && !a.cancel ? "تم الاستلام" : ""))),
                        user = a.userFirst.FirstName + " " + a.userFirst.LastName
                    }).ToList();
                if (sorder == null || sorder.Count == 0)
                {
                    return BadRequest("لا يوجد اي طلب مسجل بهذا الرقم");
                }
                var toreturn1 = new
                {
                    orders = sorder,
                    TotalCount = 1,
                    totalPages = 1,
                };
                return Ok(toreturn1);
            }
            if (o.all)
            {
                var totalCount = db.PurchaseOrder.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId)).Count();
                if (totalCount == 0)
                {
                    return BadRequest("لا توجد اي طلبات متاحة في الوقت الحالي");
                }
                var totalPages = Math.Ceiling((double)totalCount / o.pageSize);
                var order = db.PurchaseOrder.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId))
                     .ToList()
                    .Select(a => new
                    {

                        orderNo = a.orderNo,
                        inventoryto = a.TransferTo.AccountName + " - " + a.TransferTo.Code,
                        date = a.orderDate,
                        Status = a.isDoneSecond && !a.cancel ? "جاري التنفيذ" : (a.cancel ? "الغي بتاريخ " + a.cancelDate.GetValueOrDefault().ToString("yyyy-MM-dd") : (!a.cancel && !a.isDoneSecond ? "لم بتم التاكيد" : (a.isDoneThird && !a.cancel ? "تم الاستلام" : ""))),
                        user = a.userFirst.FirstName + " " + a.userFirst.LastName
                    }).ToList().Skip((o.pageNumber - 1) * o.pageSize)
                                 .Take(o.pageSize)
                                 .ToList();

                var toreturn = new
                {
                    orders = order,
                    TotalCount = totalCount,
                    totalPages = totalPages,
                };
                return Ok(toreturn);
            }

            else if (o.isDoneFirst)
            {
                var totalCount = db.PurchaseOrder.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.isDoneFirst == o.isDoneFirst && a.cancel == false && a.isDoneSecond == false).Count();
                if (totalCount == 0)
                {
                    return BadRequest("لا توجد اي طلبات متاحة في الوقت الحالي");
                }
                var totalPages = Math.Ceiling((double)totalCount / o.pageSize);
                var order = db.PurchaseOrder.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.isDoneFirst == a.isDoneFirst == o.isDoneFirst && a.cancel == false && a.isDoneSecond == false)
                     .ToList()
                    .Select(a => new
                    {

                        orderNo = a.orderNo,
                        inventoryto = a.TransferTo.AccountName + " - " + a.TransferTo.Code,
                        date = a.orderDate,
                        Status = a.isDoneSecond && !a.cancel ? "جاري التنفيذ" : (a.cancel ? "الغي بتاريخ " + a.cancelDate.GetValueOrDefault().ToString("yyyy-MM-dd") : (!a.cancel && !a.isDoneSecond ? "لم بتم التاكيد" : (a.isDoneThird && !a.cancel ? "تم الاستلام" : ""))),
                        user = a.userFirst.FirstName + " " + a.userFirst.LastName
                    }).ToList().Skip((o.pageNumber - 1) * o.pageSize)
                                 .Take(o.pageSize)
                                 .ToList();

                var toreturn = new
                {
                    orders = order,
                    TotalCount = totalCount,
                    totalPages = totalPages,
                };
                return Ok(toreturn);
            }
            else if (o.cancel)
            {
                var totalCount = db.PurchaseOrder.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.cancel == o.cancel).Count();
                if (totalCount == 0)
                {
                    return BadRequest("لا توجد اي طلبات متاحة في الوقت الحالي");
                }
                var totalPages = Math.Ceiling((double)totalCount / o.pageSize);
                var order = db.PurchaseOrder.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.cancel == o.cancel)
                     .ToList()
                    .Select(a => new
                    {

                        orderNo = a.orderNo,
                        inventoryto = a.TransferTo.AccountName + " - " + a.TransferTo.Code,
                        date = a.orderDate,
                        Status = a.isDoneSecond && !a.cancel ? "جاري التنفيذ" : (a.cancel ? "الغي بتاريخ " + a.cancelDate.GetValueOrDefault().ToString("yyyy-MM-dd") : (!a.cancel && !a.isDoneSecond ? "لم بتم التاكيد" : (a.isDoneThird && !a.cancel ? "تم الاستلام" : ""))),
                        user = a.userFirst.FirstName + " " + a.userFirst.LastName
                    }).ToList().Skip((o.pageNumber - 1) * o.pageSize)
                                 .Take(o.pageSize)
                                 .ToList();

                var toreturn = new
                {
                    orders = order,
                    TotalCount = totalCount,
                    totalPages = totalPages,
                };
                return Ok(toreturn);
            }
            else if (o.isDoneSecond)
            {
                var totalCount = db.PurchaseOrder.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.isDoneSecond == true && a.cancel == false).Count();
                if (totalCount == 0)
                {
                    return BadRequest("لا توجد اي طلبات متاحة في الوقت الحالي");
                }
                var totalPages = Math.Ceiling((double)totalCount / o.pageSize);
                var order = db.PurchaseOrder.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.isDoneSecond == true && a.cancel == false)
                     .ToList()
                    .Select(a => new
                    {

                        orderNo = a.orderNo,
                        inventoryto = a.TransferTo.AccountName + " - " + a.TransferTo.Code,
                        date = a.orderDate,
                        Status = a.isDoneSecond && !a.cancel ? "جاري التنفيذ" : (a.cancel ? "الغي بتاريخ " + a.cancelDate.GetValueOrDefault().ToString("yyyy-MM-dd") : (!a.cancel && !a.isDoneSecond ? "لم بتم التاكيد" : (a.isDoneThird && !a.cancel ? "تم الاستلام" : ""))),
                        user = a.userFirst.FirstName + " " + a.userFirst.LastName
                    }).ToList().Skip((o.pageNumber - 1) * o.pageSize)
                                 .Take(o.pageSize)
                                 .ToList();

                var toreturn = new
                {
                    orders = order,
                    TotalCount = totalCount,
                    totalPages = totalPages,
                };
                return Ok(toreturn);
            }
            else
            {
                var totalCount = db.PurchaseOrder.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId)).Count();
                if (totalCount == 0)
                {
                    return BadRequest("لا توجد اي طلبات متاحة في الوقت الحالي");
                }
                var totalPages = Math.Ceiling((double)totalCount / o.pageSize);
                var order = db.PurchaseOrder.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId))
                     .ToList()
                    .Select(a => new
                    {

                        orderNo = a.orderNo,
                        inventoryto = a.TransferTo.AccountName + " - " + a.TransferTo.Code,
                        date = a.orderDate,
                        Status = a.isDoneSecond && !a.cancel ? "جاري التنفيذ" : (a.cancel ? "الغي بتاريخ " + a.cancelDate.GetValueOrDefault().ToString("yyyy-MM-dd") : (!a.cancel && !a.isDoneSecond ? "لم بتم التاكيد" : (a.isDoneThird && !a.cancel ? "تم الاستلام" : ""))),
                        user = a.userFirst.FirstName + " " + a.userFirst.LastName
                    }).ToList().Skip((o.pageNumber - 1) * o.pageSize)
                                 .Take(o.pageSize)
                                 .ToList();

                var toreturn = new
                {
                    orders = order,
                    TotalCount = totalCount,
                    totalPages = totalPages,
                };
                return Ok(toreturn);
            }

        }


        [Route("getwaitingOrderCounts")]
        [HttpPost]
        public IHttpActionResult getwaitingOrder()
        {
            string uid = RequestContext.Principal.Identity.GetUserId();

            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var doneFirst = db.PurchaseOrder.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.cancel == false && a.isDoneSecond == false).Count();
            var donesecond = db.PurchaseOrder.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.cancel == false && a.isDoneSecond == true).Count();
            var returnd = new { waitingcount = doneFirst, workinprogress = donesecond };
            return Ok(returnd);
        }
        #endregion


        /// <summary>
        /// sssssssssssssss
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        #region inVentoryRequestes


        [Route("getInventoryOrder")]
        [HttpPost]
        public IHttpActionResult getInventoryOrder(getInventoryRequestes o)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            if (company == null)
            {
                return BadRequest("الشركة غير متاحة - اختار الشركة او قم باعادة تسجيل الدخول");

            }
            string orderType1 = null;
            string orderType2 = null;
            if (o.orderType == "in")
            {
                orderType1 = "pur";
                orderType2 = "salesReturn";

            }
            if (o.orderType == "out")
            {
                orderType1 = "purReturn";
                orderType2 = "sales";

            }

            var findInv = db.InventoryRequests.Where(a => a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.cancel == false && a.isDoneSecond == false && (a.OrderType == orderType1 || a.OrderType == orderType2)).ToList();
            if (findInv == null)
            {
                return BadRequest("لا يوجد اذون متاحة في الوقت الحالي");

            }
            var returninv = findInv.Select(a => new InventoryTransfer()
            {
                inventoryId = a.TransferToId,
                inventoryName = a.TransferTo.AccountName,
                code = a.TransferTo.Code
            }).ToList();
            //var returnd = new { waitingcount = doneFirst, workinprogress = donesecond, inventory = returninv };
            var doneFirst = db.InventoryRequests.Where(a => a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.cancel == false && a.isDoneSecond == false && (a.OrderType == orderType1 || a.OrderType == orderType2)).Count();

            var returnd = new { waitingcount = doneFirst, inventory = returninv };
            return Ok(returnd);
        }

        [Route("getInventoryByIdOrder")]
        [HttpPost]
        public IHttpActionResult getInventoryByIdOrder(getInventoryRequestes o)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            if (company == null)
            {
                return BadRequest("الشركة غير متاحة - اختار الشركة او قم باعادة تسجيل الدخول");

            }
            string orderType1 = null;
            string orderType2 = null;
            if (o.orderType == "in")
            {
                orderType1 = "pur";
                orderType2 = "salesReturn";

            }
            if (o.orderType == "out")
            {
                orderType1 = "purReturn";
                orderType2 = "sales";
            }

            var findInv = db.InventoryRequests.Where(a => a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.TransferToId == o.inventoryId && a.cancel == false && a.isDoneSecond == false && (a.OrderType == orderType1 || a.OrderType == orderType2)).ToList();
            if (findInv == null)
            {
                return BadRequest("لا يوجد اذون متاحة في الوقت الحالي");

            }

            var doneFirst = db.InventoryRequests.Where(a => a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.TransferToId == o.inventoryId && a.cancel == false && a.isDoneSecond == false && (a.OrderType == orderType1 || a.OrderType == orderType2)).Count();



            return Ok(doneFirst);
        }


        [Route("getOrderRequestByIdw")]
        [HttpPost]
        public IHttpActionResult getInventoryOrderRequestById(getInventoryRequestes o)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var order = OrderReqestManager.getInventoryRequestById(o.OrderNo, o.orderType, o.orderId, company);
            if (order == null)
            {
                return BadRequest("الاذن غير مسجل او غير متاح لهذة العملية");
            }
            return Ok(order);
        }


        [Route("getwaitingOrderInventory")]
        [HttpPost]
        public IHttpActionResult getwaitingOrderInventory(getwaitinPurRequest o)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();

            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            string orderType1 = null;
            string orderType2 = null;
            if (o.OrderType == "in")
            {
                orderType1 = "pur";
                orderType2 = "salesReturn";

            }
            if (o.OrderType == "out")
            {
                orderType1 = "purReturn";
                orderType2 = "sales";
            }
            if (o.orderNo != 0)
            {

                var sorder = db.InventoryRequests.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.orderNo == o.orderNo && (a.OrderType == orderType1 || a.OrderType == orderType2))
                    .ToList()
                    .Select(a => new
                    {   orderId = a.InventoryRequestId,
                        ONo = a.orderONo,
                        orderNo = a.orderNo,
                        inventoryto = a.TransferTo.AccountName + " - " + a.TransferTo.Code,
                        date = a.orderDate,
                        Status = a.isDoneSecond && !a.cancel ? "تم التنفيذ" : (a.cancel ? "الغي بتاريخ " + a.cancelDate.GetValueOrDefault().ToString("yyyy-MM-dd") : (!a.cancel && !a.isDoneSecond ? "لم بتم التاكيد" : (a.isDoneThird && !a.cancel ? "تم الاستلام" : ""))),
                        user = a.userFirst.FirstName + " " + a.userFirst.LastName
                    }).ToList();
                if (sorder == null || sorder.Count == 0)
                {
                    return BadRequest("لا يوجد اي طلب مسجل بهذا الرقم");
                }
                var toreturn1 = new
                {
                    orders = sorder,
                    TotalCount = 1,
                    totalPages = 1,
                };
                return Ok(toreturn1);
            }
            if (o.all)
            {
                var totalCount = db.InventoryRequests.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && (a.OrderType == orderType1 || a.OrderType == orderType2)).Count();
                if (totalCount == 0)
                {
                    return BadRequest("لا توجد اي طلبات متاحة في الوقت الحالي");
                }
                var totalPages = Math.Ceiling((double)totalCount / o.pageSize);
                var order = db.InventoryRequests.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && (a.OrderType == orderType1 || a.OrderType == orderType2))
                     .ToList()
                    .Select(a => new
                    {
                        orderId = a.InventoryRequestId,
                        ONo = a.orderONo,
                        orderNo = a.orderNo,
                        inventoryto = a.TransferTo.AccountName + " - " + a.TransferTo.Code,
                        date = a.orderDate,
                        Status = a.isDoneSecond && !a.cancel ? "تم التنفيذ" : (a.cancel ? "الغي بتاريخ " + a.cancelDate.GetValueOrDefault().ToString("yyyy-MM-dd") : (!a.cancel && !a.isDoneSecond ? "لم بتم التاكيد" : (a.isDoneThird && !a.cancel ? "تم الاستلام" : ""))),
                        user = a.userFirst.FirstName + " " + a.userFirst.LastName
                    }).ToList().Skip((o.pageNumber - 1) * o.pageSize)
                                 .Take(o.pageSize)
                                 .ToList();

                var toreturn = new
                {
                    orders = order,
                    TotalCount = totalCount,
                    totalPages = totalPages,
                };
                return Ok(toreturn);
            }

            else if (o.isDoneFirst)
            {
                var totalCount = db.InventoryRequests.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.isDoneFirst == o.isDoneFirst && a.cancel == false && a.isDoneSecond == false && (a.OrderType == orderType1 || a.OrderType == orderType2)).Count();
                if (totalCount == 0)
                {
                    return BadRequest("لا توجد اي طلبات متاحة في الوقت الحالي");
                }
                var totalPages = Math.Ceiling((double)totalCount / o.pageSize);
                var order = db.InventoryRequests.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.isDoneFirst == a.isDoneFirst == o.isDoneFirst && a.cancel == false && a.isDoneSecond == false && (a.OrderType == orderType1 || a.OrderType == orderType2))
                     .ToList()
                    .Select(a => new
                    {
                        orderId = a.InventoryRequestId,
                        ONo = a.orderONo,
                        orderNo = a.orderNo,
                        inventoryto = a.TransferTo.AccountName + " - " + a.TransferTo.Code,
                        date = a.orderDate,
                        Status = a.isDoneSecond && !a.cancel ? "تم التنفيذ" : (a.cancel ? "الغي بتاريخ " + a.cancelDate.GetValueOrDefault().ToString("yyyy-MM-dd") : (!a.cancel && !a.isDoneSecond ? "لم بتم التاكيد" : (a.isDoneThird && !a.cancel ? "تم الاستلام" : ""))),
                        user = a.userFirst.FirstName + " " + a.userFirst.LastName
                    }).ToList().Skip((o.pageNumber - 1) * o.pageSize)
                                 .Take(o.pageSize)
                                 .ToList();

                var toreturn = new
                {
                    orders = order,
                    TotalCount = totalCount,
                    totalPages = totalPages,
                };
                return Ok(toreturn);
            }
            else if (o.cancel)
            {
                var totalCount = db.InventoryRequests.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.cancel == o.cancel && (a.OrderType == orderType1 || a.OrderType == orderType2)).Count();
                if (totalCount == 0)
                {
                    return BadRequest("لا توجد اي طلبات متاحة في الوقت الحالي");
                }
                var totalPages = Math.Ceiling((double)totalCount / o.pageSize);
                var order = db.InventoryRequests.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.cancel == o.cancel && (a.OrderType == orderType1 || a.OrderType == orderType2))
                     .ToList()
                    .Select(a => new
                    {
                        orderId = a.InventoryRequestId,
                        ONo = a.orderONo,
                        orderNo = a.orderNo,
                        inventoryto = a.TransferTo.AccountName + " - " + a.TransferTo.Code,
                        date = a.orderDate,
                        Status = a.isDoneSecond && !a.cancel ? "تم التنفيذ" : (a.cancel ? "الغي بتاريخ " + a.cancelDate.GetValueOrDefault().ToString("yyyy-MM-dd") : (!a.cancel && !a.isDoneSecond ? "لم بتم التاكيد" : (a.isDoneThird && !a.cancel ? "تم الاستلام" : ""))),
                        user = a.userFirst.FirstName + " " + a.userFirst.LastName
                    }).ToList().Skip((o.pageNumber - 1) * o.pageSize)
                                 .Take(o.pageSize)
                                 .ToList();

                var toreturn = new
                {
                    orders = order,
                    TotalCount = totalCount,
                    totalPages = totalPages,
                };
                return Ok(toreturn);
            }
            else if (o.isDoneSecond)
            {
                var totalCount = db.InventoryRequests.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.isDoneSecond == true && a.cancel == false && (a.OrderType == orderType1 || a.OrderType == orderType2)).Count();
                if (totalCount == 0)
                {
                    return BadRequest("لا توجد اي طلبات متاحة في الوقت الحالي");
                }
                var totalPages = Math.Ceiling((double)totalCount / o.pageSize);
                var order = db.InventoryRequests.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.isDoneSecond == true && a.cancel == false && (a.OrderType == orderType1 || a.OrderType == orderType2))
                     .ToList()
                    .Select(a => new
                    {
                        orderId = a.InventoryRequestId,
                        ONo = a.orderONo,
                        orderNo = a.orderNo,
                        inventoryto = a.TransferTo.AccountName + " - " + a.TransferTo.Code,
                        date = a.orderDate,
                        Status = a.isDoneSecond && !a.cancel ? "تم التنفيذ" : (a.cancel ? "الغي بتاريخ " + a.cancelDate.GetValueOrDefault().ToString("yyyy-MM-dd") : (!a.cancel && !a.isDoneSecond ? "لم بتم التاكيد" : (a.isDoneThird && !a.cancel ? "تم الاستلام" : ""))),
                        user = a.userFirst.FirstName + " " + a.userFirst.LastName
                    }).ToList().Skip((o.pageNumber - 1) * o.pageSize)
                                 .Take(o.pageSize)
                                 .ToList();

                var toreturn = new
                {
                    orders = order,
                    TotalCount = totalCount,
                    totalPages = totalPages,
                };
                return Ok(toreturn);
            }
            else
            {
                var totalCount = db.InventoryRequests.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && (a.OrderType == orderType1 || a.OrderType == orderType2)).Count();
                if (totalCount == 0)
                {
                    return BadRequest("لا توجد اي طلبات متاحة في الوقت الحالي");
                }
                var totalPages = Math.Ceiling((double)totalCount / o.pageSize);
                var order = db.InventoryRequests.Where(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && (a.OrderType == orderType1 || a.OrderType == orderType2))
                     .ToList()
                    .Select(a => new
                    {
                        orderId = a.InventoryRequestId,
                        ONo = a.orderONo,
                        orderNo = a.orderNo,
                        inventoryto = a.TransferTo.AccountName + " - " + a.TransferTo.Code,
                        date = a.orderDate,
                        Status = a.isDoneSecond && !a.cancel ? "تم التنفيذ" : (a.cancel ? "الغي بتاريخ " + a.cancelDate.GetValueOrDefault().ToString("yyyy-MM-dd") : (!a.cancel && !a.isDoneSecond ? "لم بتم التاكيد" : (a.isDoneThird && !a.cancel ? "تم الاستلام" : ""))),
                        user = a.userFirst.FirstName + " " + a.userFirst.LastName
                    }).ToList().Skip((o.pageNumber - 1) * o.pageSize)
                                 .Take(o.pageSize)
                                 .ToList();

                var toreturn = new
                {
                    orders = order,
                    TotalCount = totalCount,
                    totalPages = totalPages,
                };
                return Ok(toreturn);
            }

        }
        [Route("saveinventoryrequest")]
        [HttpPost]
        public IHttpActionResult saveinventoryrequest(InventoryRequestOrderVm o)
        {
            #region save 
            string uid = RequestContext.Principal.Identity.GetUserId();
            
            //I must Edit Date Now this in the future to meet our customers localDate
            DateTime nowDate = DateTime.Now;

            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            string orderType1 = null;
            string orderType2 = null;
            
            if (o.OrderType == "in")
            {
                orderType1 = "pur";
                orderType2 = "salesReturn";
                var order = db.InventoryRequests.SingleOrDefault(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.InventoryRequestId == o.orderId && (a.OrderType == orderType1 || a.OrderType == orderType2) && a.cancel == false);
                if(order == null)
                {
                    return BadRequest("الاذن غير مسجل او غير متاح لهذة العملية");

                }
                var oorder = db.OrderOs.SingleOrDefault(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.orderNo == o.ONo && a.orderType == order.OrderType && a.type == order.OType && a.cancel == false);
                if (order == null)
                {
                    return BadRequest("الامر غير مسجل او غير متاح لهذة العملية");

                }

                order.isDoneSecond = true;
                order.userSecondId = uid;
                order.orderDateSecond = nowDate;
                var itemsInfo =OrderReqestManager.getTransferInfo(order.TransferToId, company, true);

                List<SubAccountMovement> itemMove = new List<SubAccountMovement>();
              
                foreach (var item in order.itemsRequest)
                {
                    item.note = o.itemsRequest.Where(oi => oi.itemId == item.SubAccountId).Select(inote => inote.note).SingleOrDefault();

                    var selectedItem = itemsInfo.items.SingleOrDefault(n => n.itemId == item.SubAccountId);
                    var subAccountsTo = new SubAccountMovement()
                    {
                        SubAccountId = item.SubAccountId,
                        CurrentWorkerId = uid,
                        SubAccountMovementDate = nowDate,
                        OrderNote = string.Format("اذن استلام رقم {0}" ,order.orderNo ),
                        QuantityIn = item.qty,
                        QuantityOut = 0,
                        ItemPricein =oorder.orderType == "pur" ? item.price : selectedItem.costPrice,
                        ItemTotalPriceIn = oorder.orderType == "pur" ? item.price * item.qty : selectedItem.costPrice * item.qty,
                        ItemStockQuantity = selectedItem.Qty + item.qty,
                        ItemTotalStockPrice = oorder.orderType == "pur" ? selectedItem.totalStockprice + (item.price * item.qty) : selectedItem.totalStockprice + (selectedItem.costPrice * item.qty),
                        ItemStockPrice = oorder.orderType == "pur" ? (selectedItem.totalStockprice + (item.price * item.qty)) / (selectedItem.Qty + item.qty) : (selectedItem.totalStockprice + (selectedItem.costPrice * item.qty)) / (selectedItem.Qty + item.qty),
                        OrderId = order.orderNo
                    };
                   
                    itemMove.Add(subAccountsTo);
                }
                string note = order.OrderType == "pur" ? "شراء" : "رد مبيعات";
                var suborderfrom = new SubAccountOrder()
                {
                    OrderDate = nowDate,
                    CurrentWorkerId = uid,
                    OrderNote = string.Format("اذن استلام رقم {0} من امر {1} رقم {2}" , order.orderNo , note,order.orderONo ),
                    CustomSubAccountsOrderId = order.orderNo,
                    isDone = true,
                    SubAccountMovements = itemMove,
                    
                };


                #region AccountMove Order
                

            

                var accorders = new List<AccountOrder>();

                if (oorder.orderType == "pur")
                {
                    List<AccountMovement> accountMove = new List<AccountMovement>();
                    var groupAmount = itemMove.Select(a => a.QuantityIn * a.ItemPricein).Sum();
                    var accorderNo = db.AccountOrders.Where(a => a.CompanyId == (company.isCompany ? company.companyId : (int?)null) && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.IsCompany == company.isCompany).Select(oc => oc.OrderNo).DefaultIfEmpty(0).Max() + 1;

                    var accnote = string.Format("استلام بضاعة - من {0} - {1} الي {2} - {3} - من امر {4} رقم {5} - رقم اذن {6} {7} - بقيمة {8}", oorder.SubjectAccount.AccountName, oorder.SubjectAccount.Code, oorder.TransferToO.AccountName, oorder.TransferToO.Code, note, oorder.orderNo, "الاستلام", order.orderNo, groupAmount);

                    var accmoveFrom = new AccountMovement()
                    {
                        AccountId = order.TransferToId,
                        Crdit = 0,
                        Debit = groupAmount,
                        CurrentWorkerId = uid,
                        AccountMovementDate = nowDate,
                        AccountMovementNote = "استلام بضاعه",

                    };
                    var accmoveTrnsit = new AccountMovement()
                    {
                        AccountId = oorder.SubjectAccountId,
                        Crdit = groupAmount,
                        Debit = 0,
                        CurrentWorkerId = uid,
                        AccountMovementDate = nowDate,
                        AccountMovementNote = "استلام بضاعه",
                    };
                    accountMove.Add(accmoveFrom);
                    accountMove.Add(accmoveTrnsit);

                     Int32 refId = Convert.ToInt32((oorder.SubjectAccount.Code + order.orderNo));
                    var newAccountOrder = new AccountOrder()
                    {
                        OrderDate = nowDate,
                        CompanyId = company.isCompany ? company.companyId : (int?)null,
                        BranchId = company.isCompany ? (int?)null : company.oId,
                        IsCompany = company.isCompany,
                        HasDone = true,
                        OrderIdRefrence = refId,
                        OrderTypeRefrence = "استلام",
                        AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId,
                        OrderNo = accorderNo,
                        OrderNote = accnote,
                        CurrentWorkerId = uid,
                        AccountMovements = accountMove
                    };
                    accorders.Add(newAccountOrder);
                }
                if (oorder.orderType == "salesReturn")
                {
                    var invConfig = db.InventoriesConfigurtion.SingleOrDefault(a => a.InventoryId == oorder.TransferToOId && a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId));
                    if (invConfig == null)
                    {
                        return BadRequest("تاكد من ان المخزن معد لعمليات البيع - تم الغاء العملية");
                    }
                    List<AccountMovement> accountMove = new List<AccountMovement>();
                    List<AccountMovement> accountMove2 = new List<AccountMovement>();

                    var groupAmount = itemMove.Select(a => a.QuantityIn * a.ItemPricein).Sum();
                    var groupAmount2 = oorder.itemsRequest.Select(a => a.qty * a.price).Sum();

                    var accmoveFrom = new AccountMovement()
                    {
                        AccountId = order.TransferToId,
                        Crdit = 0,
                        Debit = groupAmount,
                        CurrentWorkerId = uid,
                        AccountMovementDate = nowDate,
                        AccountMovementNote = "استلام بضاعه",

                    };

                    var accmoveTrnsit = new AccountMovement()
                    {
                        AccountId = invConfig.SalesCostAccountId,
                        Crdit = groupAmount,
                        Debit = 0,
                        CurrentWorkerId = uid,
                        AccountMovementDate = nowDate,
                        AccountMovementNote = "استلام بضاعه",
                    };
                    accountMove.Add(accmoveFrom);
                    accountMove.Add(accmoveTrnsit);
                    var accorderNo = db.AccountOrders.Where(a => a.CompanyId == (company.isCompany ? company.companyId : (int?)null) && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.IsCompany == company.isCompany).Select(oc => oc.OrderNo).DefaultIfEmpty(0).Max() + 1;
                    var accorderNo2 = db.AccountOrders.Where(a => a.CompanyId == (company.isCompany ? company.companyId : (int?)null) && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.IsCompany == company.isCompany).Select(oc => oc.OrderNo).DefaultIfEmpty(0).Max() + 2;

                    var accnote = string.Format("استلام بضاعة بسعر التكلفة - من {0} - {1}  - امر {2} رقم {3} - رقم اذن {4} {5} - بقيمة {6}", oorder.SubjectAccount.AccountName, oorder.SubjectAccount.Code, note, oorder.orderNo, "الصرف", order.orderNo, groupAmount);
                    var accnote2 = string.Format("استلام بضاعة - من {0} - {1} الي {2} - {3} - امر {4} رقم {5} - رقم اذن {6} {7} - بقيمة {8}",  oorder.SubjectAccount.AccountName, oorder.SubjectAccount.Code, oorder.TransferToO.AccountName, oorder.TransferToO.Code, note, oorder.orderNo, "الصرف", order.orderNo, groupAmount2);


                    var newAccountOrder = new AccountOrder()
                    {
                        OrderDate = nowDate,
                        CompanyId = company.isCompany ? company.companyId : (int?)null,
                        BranchId = company.isCompany ? (int?)null : company.oId,
                        IsCompany = company.isCompany,
                        HasDone = true,
                        OrderTypeRefrence = "استلام",
                        AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId,
                        OrderNo = accorderNo,
                        OrderNote = accnote,
                        CurrentWorkerId = uid,
                        AccountMovements = accountMove
                    };
                    if (groupAmount <= 0)
                    {
                        ModelState.AddModelError("", "قيمة الامر تساوي 0 - تاكد من اعداد الاصناف بطريقة صحيحة");
                    }

                    var accmoveFrom2 = new AccountMovement()
                    {
                        AccountId = invConfig.SalesReturnId,
                        Crdit = 0,
                        Debit = groupAmount2,
                        CurrentWorkerId = uid,
                        AccountMovementDate = nowDate,
                        AccountMovementNote = "استلام بضاعه",

                    };

                    var accmoveTrnsit2 = new AccountMovement()
                    {
                        AccountId = oorder.SubjectAccountId,
                        Crdit = groupAmount2,
                        Debit = 0,
                        CurrentWorkerId = uid,
                        AccountMovementDate = nowDate,
                        AccountMovementNote = "استلام بضاعه",


                    };
                    accountMove2.Add(accmoveFrom2);
                    accountMove2.Add(accmoveTrnsit2);
                    Int32 refId = Convert.ToInt32((oorder.SubjectAccount.Code + order.orderNo));
                    var newAccountOrder2 = new AccountOrder()
                    {
                        OrderDate = nowDate,
                        CompanyId = company.isCompany ? company.companyId : (int?)null,
                        BranchId = company.isCompany ? (int?)null : company.oId,
                        IsCompany = company.isCompany,
                        HasDone = true,
                        OrderIdRefrence = refId,
                        OrderTypeRefrence = "استلام",
                        AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId,
                        OrderNo = accorderNo2,
                        OrderNote = accnote2,
                        CurrentWorkerId = uid,
                        AccountMovements = accountMove2
                    };

                    accorders.Add(newAccountOrder);
                    accorders.Add(newAccountOrder2);
                }



                #endregion
                db.Entry(order).State = EntityState.Modified;
                db.SubAccountOrders.Add(suborderfrom);
                db.AccountOrders.AddRange(accorders);
                db.SaveChanges();
                return Ok("تم الحفظ");
            }
            if(o.OrderType == "out")
            {
                orderType1 = "purReturn";
                orderType2 = "sales";
                var order = db.InventoryRequests.SingleOrDefault(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.InventoryRequestId == o.orderId && (a.OrderType == orderType1 || a.OrderType == orderType2) && a.cancel == false);
                if (order == null)
                {
                    return BadRequest("الاذن غير مسجل او غير متاح لهذة العملية");

                }
                order.isDoneSecond = true;
                order.userSecondId = uid;
                order.orderDateSecond =nowDate;
                var itemsInfo = OrderReqestManager.getTransferInfo(order.TransferToId, company, true);

                List<SubAccountMovement> itemMove = new List<SubAccountMovement>();
              
                foreach (var item in order.itemsRequest)
                {
                    item.note = o.itemsRequest.Where(oi => oi.itemId == item.SubAccountId).Select(inote => inote.note).SingleOrDefault();
                    var selectedItem = itemsInfo.items.SingleOrDefault(n => n.itemId == item.SubAccountId);
                    if (item.qty > selectedItem.Qty)
                    {
                        string ni = string.Format("الكمية التي تحاول صرفها من الصنف {0} غير متاحة - الكمية المتاحة هي {1}", selectedItem.itemName, selectedItem.Qty);
                        ModelState.AddModelError("كمية غير متاحة", ni);

                    }
                    var subAccountsTo = new SubAccountMovement()
                    {
                        SubAccountId = item.SubAccountId,
                        CurrentWorkerId = uid,
                        SubAccountMovementDate = nowDate,
                        OrderNote = string.Format("اذن صرف رقم {0}", order.orderNo),
                        QuantityIn = 0,
                        QuantityOut = item.qty,
                        ItemPriceOut = selectedItem.costPrice,
                        ItemTotalPriceOut = selectedItem.costPrice * item.qty,
                        ItemStockQuantity = selectedItem.Qty - item.qty,
                        ItemTotalStockPrice = (selectedItem.Qty - item.qty) * selectedItem.costPrice,
                        ItemStockPrice = selectedItem.Qty - item.qty != 0 ? ((selectedItem.Qty - item.qty) * selectedItem.costPrice) / (selectedItem.Qty - item.qty) : selectedItem.costPrice,
                        OrderId = order.orderNo
                    };
                    itemMove.Add(subAccountsTo);
                }
                string note = order.OrderType == "purReturn" ? "رد مشتريات" : "بيع";
                var suborderfrom = new SubAccountOrder()
                {
                    OrderDate = nowDate,
                    CurrentWorkerId = uid,
                    OrderNote = string.Format("اذن صرف رقم {0} من امر {1} رقم {2}", order.orderNo, note, order.orderONo),
                    CustomSubAccountsOrderId = order.orderNo,
                    isDone = true,
                    SubAccountMovements = itemMove
                };
                var oorder = db.OrderOs.SingleOrDefault(a => a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.orderNo == o.ONo && a.orderType == order.OrderType && a.type == order.OType && a.cancel == false);
                if (order == null)
                {
                    return BadRequest("الامر غير مسجل او غير متاح لهذة العملية");

                }
                #region AccountMove Order NOT SAles
                var accorders = new List<AccountOrder>();
                if(oorder.orderType == "purReturn")
                {
                    List<AccountMovement> accountMove = new List<AccountMovement>();
                    var groupAmount = itemMove.Select(a => a.QuantityOut * a.ItemPriceOut).Sum();
                    var accmoveFrom = new AccountMovement()
                    {
                        AccountId = order.TransferToId,
                        Crdit = groupAmount,
                        Debit = 0,
                        CurrentWorkerId = uid,
                        AccountMovementDate = nowDate,
                        AccountMovementNote = "صرف بضاعه",

                    };

                    var accmoveTrnsit = new AccountMovement()
                    {
                        AccountId = oorder.SubjectAccountId,
                        Crdit = 0,
                        Debit = groupAmount,
                        CurrentWorkerId = uid,
                        AccountMovementDate = nowDate,
                        AccountMovementNote = "صرف بضاعه",


                    };
                    accountMove.Add(accmoveFrom);
                    accountMove.Add(accmoveTrnsit);
                    var accorderNo = db.AccountOrders.Where(a => a.CompanyId == (company.isCompany ? company.companyId : (int?)null) && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.IsCompany == company.isCompany).Select(oc => oc.OrderNo).DefaultIfEmpty(0).Max() + 1;

                    var accnote = string.Format("صرف بضاعة - من {0} - {1} الي {2} - {3} - من امر {4} رقم {5} - رقم اذن {6} {7} - بقيمة {8}", oorder.TransferToO.AccountName, oorder.TransferToO.Code, oorder.SubjectAccount.AccountName, oorder.SubjectAccount.Code, note, oorder.orderNo, "الصرف", order.orderNo, groupAmount);
                    Int32 refId = Convert.ToInt32((oorder.SubjectAccount.Code + order.orderNo));
                    var newAccountOrder = new AccountOrder()
                    {
                        OrderDate = nowDate,
                        CompanyId = company.isCompany ? company.companyId : (int?)null,
                        BranchId = company.isCompany ? (int?)null : company.oId,
                        IsCompany = company.isCompany,
                        HasDone = true,
                        OrderIdRefrence = refId,
                        OrderTypeRefrence = "صرف",
                        AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId,
                        OrderNo = accorderNo,
                        OrderNote = accnote,
                        CurrentWorkerId = uid,
                        AccountMovements = accountMove
                    };
                    if (groupAmount <= 0)
                    {
                        ModelState.AddModelError("", "قيمة الامر تساوي 0 - تاكد من اعداد الاصناف بطريقة صحيحة");
                    }
                    accorders.Add(newAccountOrder);
                }

                #endregion

                #region AccountMove Order Sales

                if (oorder.orderType == "sales")
                {
                    var invConfig = db.InventoriesConfigurtion.SingleOrDefault(a => a.InventoryId == oorder.TransferToOId && a.CompanyId == company.companyId && a.IsCompany == company.isCompany && a.BranchId == (company.isCompany ? (int?)null : company.oId));
                    if(invConfig == null)
                    {
                        return BadRequest("تاكد من ان المخزن معد لعمليات البيع - تم الغاء العملية");
                    }
                    List<AccountMovement> accountMove = new List<AccountMovement>();
                    List<AccountMovement> accountMove2 = new List<AccountMovement>();

                    var groupAmount = itemMove.Select(a => a.QuantityOut * a.ItemPriceOut).Sum();
                    var groupAmount2 = oorder.itemsRequest.Select(a => a.qty * a.price).Sum();

                    var accmoveFrom = new AccountMovement()
                    {
                        AccountId = order.TransferToId,
                        Crdit = groupAmount,
                        Debit = 0,
                        CurrentWorkerId = uid,
                        AccountMovementDate = nowDate,
                        AccountMovementNote = "صرف بضاعه",

                    };

                    var accmoveTrnsit = new AccountMovement()
                    {
                        AccountId = invConfig.SalesCostAccountId,
                        Crdit = 0,
                        Debit = groupAmount,
                        CurrentWorkerId = uid,
                        AccountMovementDate = nowDate,
                        AccountMovementNote = "صرف بضاعه",


                    };
                    accountMove.Add(accmoveFrom);
                    accountMove.Add(accmoveTrnsit);
                    var accorderNo = db.AccountOrders.Where(a => a.CompanyId == (company.isCompany ? company.companyId : (int?)null) && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.IsCompany == company.isCompany).Select(oc => oc.OrderNo).DefaultIfEmpty(0).Max() + 1;
                    var accorderNo2 = db.AccountOrders.Where(a => a.CompanyId == (company.isCompany ? company.companyId : (int?)null) && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.IsCompany == company.isCompany).Select(oc => oc.OrderNo).DefaultIfEmpty(0).Max() + 2;

                    var accnote = string.Format("صرف بضاعة بسعر التكلفة - من {0} - {1}  - امر {2} رقم {3} - رقم اذن {4} {5} - بقيمة {6}", oorder.TransferToO.AccountName, oorder.TransferToO.Code,note, oorder.orderNo, "الصرف", order.orderNo, groupAmount);
                    var accnote2 = string.Format("صرف بضاعة - من {0} - {1} الي {2} - {3} - امر {4} رقم {5} - رقم اذن {6} {7} - بقيمة {8}", oorder.TransferToO.AccountName, oorder.TransferToO.Code, oorder.SubjectAccount.AccountName, oorder.SubjectAccount.Code, note, oorder.orderNo, "الصرف", order.orderNo, groupAmount2);

                  
                    var newAccountOrder = new AccountOrder()
                    {
                        OrderDate = nowDate,
                        CompanyId = company.isCompany ? company.companyId : (int?)null,
                        BranchId = company.isCompany ? (int?)null : company.oId,
                        IsCompany = company.isCompany,
                        HasDone = true,
                        OrderTypeRefrence = "صرف",
                        AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId,
                        OrderNo = accorderNo,
                        OrderNote = accnote,
                        CurrentWorkerId = uid,
                        AccountMovements = accountMove
                    };
                    if (groupAmount <= 0)
                    {
                        ModelState.AddModelError("", "قيمة الامر تساوي 0 - تاكد من اعداد الاصناف بطريقة صحيحة");
                    }

                    var accmoveFrom2 = new AccountMovement()
                    {
                        AccountId =invConfig.SalesAccountId,
                        Crdit = groupAmount2,
                        Debit = 0,
                        CurrentWorkerId = uid,
                        AccountMovementDate = nowDate,
                        AccountMovementNote = "صرف بضاعه",

                    };

                    var accmoveTrnsit2 = new AccountMovement()
                    {
                        AccountId = oorder.SubjectAccountId,
                        Crdit = 0,
                        Debit = groupAmount2,
                        CurrentWorkerId = uid,
                        AccountMovementDate = nowDate,
                        AccountMovementNote = "صرف بضاعه",


                    };
                    accountMove2.Add(accmoveFrom2);
                    accountMove2.Add(accmoveTrnsit2);
                    Int32 refId = Convert.ToInt32((oorder.SubjectAccount.Code + order.orderNo));
                    var newAccountOrder2 = new AccountOrder()
                    {
                        OrderDate = nowDate,
                        CompanyId = company.isCompany ? company.companyId : (int?)null,
                        BranchId = company.isCompany ? (int?)null : company.oId,
                        IsCompany = company.isCompany,
                        HasDone = true,
                        OrderIdRefrence = refId,
                        OrderTypeRefrence = "صرف",
                        AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId,
                        OrderNo = accorderNo2,
                        OrderNote = accnote2,
                        CurrentWorkerId = uid,
                        AccountMovements = accountMove2
                    };

                    accorders.Add(newAccountOrder);
                    accorders.Add(newAccountOrder2);
                }

                if(oorder.orderType != "purReturn" && oorder.orderType != "sales")
                {
                    return BadRequest("الاذن غير متاح لهذة العملية , تاكد من اتاحة الاذن وانك تملك الصلاحيات الكافية");
                }
                #endregion
                if (!ModelState.IsValid)
                {
                    var err = ModelState.Values.SelectMany(m => m.Errors)
                                     .Select(e => e.ErrorMessage)
                                     .ToList();
                    string tocoorect = "لا يمكن الحفظ لعدم اكتمال البيانات التالية :" + "<br>";
                    foreach (var er in err)
                    {
                        tocoorect = tocoorect + "<br>" + er;
                    }
                    return BadRequest(tocoorect);
                }
                db.Entry(order).State = EntityState.Modified;

                db.SubAccountOrders.Add(suborderfrom);
                db.AccountOrders.AddRange(accorders);

                db.SaveChanges();
                return Ok("تم الحفظ");
            }
            return BadRequest();
            #endregion
        } 
        #endregion

    }
}
