﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Inventory.DataLayer;
using Inventory.Models;
using Inventory.ViewModels;
using Inventory.Utility;
using Microsoft.AspNet.Identity;

namespace Inventory.Controllers
{
    [RoutePrefix("api/AccountMovements")]
    public class AccountMovementsController : BaseApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationRepository repository = new ApplicationRepository();


        [Route("GetOrder")]
        [HttpPost]
        public IHttpActionResult GetOrder(GetSetOrder or)
        {

            return Ok(this.AppCompanyUserManager.GetOrder(or));
        }


        [Route("GetAccountMovements/{id:int}")]
        [HttpGet]
        public IHttpActionResult GetAccountMovements(int id)
        {
            DateTime sD = DateTime.Now.AddDays(-29);
            DateTime eD = DateTime.Now;
            return Ok(this.AppCompanyUserManager.GetAccountmoves(id  , sD, eD).OrderByDescending(a => a.orderNo));
        }

        // GET: api/AccountMovements/5
        [Route("GetAccountMovementsDates")]
        [HttpPost]
        public IHttpActionResult Get(getAccountMovements or)
        {
            //GetAccountMovementsCatogeryDates
            DateTime sD = Convert.ToDateTime(or.startDate);
            DateTime eD = Convert.ToDateTime(or.endDate);
            var totalCount = this.AppCompanyUserManager.GetAccountmoves(or.id , sD , eD).Where(a => a.date >= sD && a.date <= eD).Count();
            var totalPages = Math.Ceiling((double)totalCount / or.pageSize);

            var Orders = this.AppCompanyUserManager.GetAccountmoves(or.id, sD, eD).Where(a => a.date >= sD && a.date <= eD);
            if (QueryHelper.PropertyExists<AccountsMoves>(or.orderby))
            {

                switch (or.orderby)
                {
                    case "date":
                        if (or.orderType)
                        {
                            Orders = Orders.OrderByDescending(a => a.date).ToList();
                        }
                        else
                        {
                            Orders = Orders.OrderBy(a => a.date).ToList();

                        }

                        break;
                    case "note":
                        if (or.orderType)
                        {
                            Orders = Orders.OrderByDescending(a => a.note).ToList();
                        }
                        else
                        {
                            Orders = Orders.OrderBy(a => a.note).ToList();

                        }

                        break;

                    case "debit":

                        if (or.orderType)
                        {
                            Orders = Orders.OrderByDescending(a => a.debit).ToList();
                        }
                        else
                        {
                            Orders = Orders.OrderBy(a => a.debit).ToList();

                        }
                        break;
                    case "crdit":


                        if (or.orderType)
                        {
                            Orders = Orders.OrderByDescending(a => a.crdit).ToList();
                        }
                        else
                        {
                            Orders = Orders.OrderBy(a => a.crdit).ToList();

                        }
                        break;

                    default:
                        Orders = Orders.OrderByDescending(a => a.date).ToList();
                        break;


                }
            }
            if (Orders.Count() == 0)
            {
                //sD = DateTime.Now.AddDays(-29);
                //eD = DateTime.Now;
                //Orders = this.AppCompanyUserManager.GetAccountmoves(or.id, sD, eD).Where(a => a.date >= sD && a.date <= eD).OrderByDescending(a => a.orderNo);
            }

            var orders = Orders.Skip((or.pageNumber - 1) * or.pageSize)
                                    .Take(or.pageSize)
                                    .ToList();

            if (!string.IsNullOrEmpty(or.search))
            {


                var ordersse = Orders.Where(a => a.orderNo == this.AppCompanyUserManager.ConvertToId(or.search)).Skip((or.pageNumber - 1) * or.pageSize)
                                      .Take(or.pageSize)
                                      .ToList();
                if (ordersse.Count > 0)
                {
                    orders = ordersse;
                }
            }





            var result = new
            {
                TotalCount = totalCount,
                totalPages = totalPages,
                Orders = orders
            };

            return Ok(result);
        }
        [Route("GetCustomAccountMovementsCatogeryDates")]
        [HttpPost]
        public IHttpActionResult GetCustomCat(getAccountMovements or)
        {
            DateTime sD = Convert.ToDateTime(or.startDate);
            DateTime eD = Convert.ToDateTime(or.endDate);
           
            var Orders = this.AppCompanyUserManager.GetCustomAccountmovesCatogery(or.id, sD, eD);
            return Ok(Orders);
        }

        // GET: api/AccountMovements/5
        [Route("GetAccountMovementsCatogeryDates")]
        [HttpPost]
        public IHttpActionResult GetCat(getAccountMovements or)
        {
            //GetAccountMovementsCatogeryDates
            DateTime sD = Convert.ToDateTime(or.startDate);
            DateTime eD = Convert.ToDateTime(or.endDate);
            var totalCount = this.AppCompanyUserManager.GetAccountmovesCatogery(or.id , sD , eD , or.IsCompany , or.IsCompany == true ? or.AllAccounts : false).Count();
            var totalPages = Math.Ceiling((double)totalCount / or.pageSize);

            var Orders = this.AppCompanyUserManager.GetAccountmovesCatogery(or.id, sD, eD, or.IsCompany, or.IsCompany == true ? or.AllAccounts : false);
            if (QueryHelper.PropertyExists<AccountCatogeryLedger>(or.orderby))
            {

                switch (or.orderby)
                {
                    case "name":
                        if (or.orderType)
                        {
                            Orders = Orders.OrderByDescending(a => a.name).ToList();
                        }
                        else
                        {
                            Orders = Orders.OrderBy(a => a.name).ToList();

                        }

                        break;
               

                    case "debit":

                        if (or.orderType)
                        {
                            Orders = Orders.OrderByDescending(a => a.debit).ToList();
                        }
                        else
                        {
                            Orders = Orders.OrderBy(a => a.debit).ToList();

                        }
                        break;
                    case "crdit":


                        if (or.orderType)
                        {
                            Orders = Orders.OrderByDescending(a => a.crdit).ToList();
                        }
                        else
                        {
                            Orders = Orders.OrderBy(a => a.crdit).ToList();

                        }
                        break;

                    default:
                        Orders = Orders.OrderByDescending(a => a.name).ToList();
                        break;


                }
            }
            if (Orders.Count() == 0)
            {
                sD = DateTime.Now.AddDays(-29);
                eD = DateTime.Now;
                Orders = this.AppCompanyUserManager.GetAccountmovesCatogery(or.id, sD, eD , or.IsCompany, or.IsCompany == true ? or.AllAccounts : false).OrderByDescending(a =>a.name).ToList();
            }

            var orders = Orders.Skip((or.pageNumber - 1) * or.pageSize)
                                    .Take(or.pageSize)
                                    .ToList();

            if (!string.IsNullOrEmpty(or.search))
            {


                var ordersse = Orders.Where(a => a.name == or.search).Skip((or.pageNumber - 1) * or.pageSize)
                                      .Take(or.pageSize)
                                      .ToList();
                if (ordersse.Count > 0)
                {
                    orders = ordersse;
                }
            }





            var result = new
            {
                TotalCount = totalCount,
                totalPages = totalPages,
                Orders = orders
            };

            return Ok(result);
        }



      
        [Route("GetfinancialStatements")]
        [HttpPost]
        public IHttpActionResult GetfinancialStatements(getfinancialStatements f)
        {
            var recevedList = db.FinancialLists.Find(this.AppCompanyUserManager.ConvertToId(f.id));
            if(recevedList == null)
            {
                return NotFound();
            }

            DateTime sD = Convert.ToDateTime(f.startDate);
            DateTime eD = Convert.ToDateTime(f.endDate);
            var days = (eD - sD).TotalDays;
            DateTime PsD = sD.AddDays(-days);

            var allbranchListes =  this.AppCompanyUserManager.getfinancialStatement((int)recevedList.CompanyId);
            if (f.IsCompany)
            {
                if (f.AllAccounts)
                {
                    var sendList = allbranchListes.Where(a => a.ListId == f.id + "F").FirstOrDefault();
                    foreach (var l in sendList.Listes)
                    {
                        decimal debit = 0;
                        decimal crdit = 0;
                        decimal Pdebit = 0;
                        decimal Pcrdit = 0;
                        if (l.Level == 5)
                        {
                            var cId = this.AppCompanyUserManager.ConvertToId(l.Id);
                            var all = (from ac in db.CustomAccounts
                                       where ac.CustomAccountId == cId
                                       from acm in ac.Account.AccountsMovements
                                       where (acm.AccountMovementDate >= sD && acm.AccountMovementDate <= eD)
                                       select acm);
                            foreach (var i in all)
                            {
                                i.Debit = this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true);
                                i.Crdit = this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false);
                            }
                            debit = (from ac in all
                                     select ac.Debit).DefaultIfEmpty(0).Sum();
                            crdit = (from ac in all
                                     select ac.Crdit).DefaultIfEmpty(0).Sum();

                            var allP = (from ac in db.CustomAccounts
                                        where ac.CustomAccountId == cId
                                        from acm in ac.Account.AccountsMovements
                                        where (acm.AccountMovementDate >= PsD && acm.AccountMovementDate <= sD)
                                        select acm);
                            foreach (var i in allP)
                            {
                                i.Debit = this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true);
                                i.Crdit = this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false);
                            }
                            Pdebit = (from ac in allP
                                      select ac.Debit).DefaultIfEmpty(0).Sum();
                            Pcrdit = (from ac in allP
                                      select ac.Crdit).DefaultIfEmpty(0).Sum();
                        }
                        if (l.Level == 4)
                        {
                            var cId = this.AppCompanyUserManager.ConvertToId(l.Id);
                            var all = (from ac in db.CustomAccounts
                                       where ac.CustomAccountCategoryId == cId
                                       from acm in ac.Account.AccountsMovements
                                       where (acm.AccountMovementDate >= sD && acm.AccountMovementDate <= eD)
                                       select acm);
                            debit = 0;
                            crdit = 0;
                            foreach (var i in all)
                            {


                                debit = debit + (i.Debit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true));
                                crdit = crdit + (i.Crdit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false));
                            }


                            var allP = (from ac in db.CustomAccounts
                                        where ac.CustomAccountCategoryId == cId
                                        from acm in ac.Account.AccountsMovements
                                        where (acm.AccountMovementDate >= PsD && acm.AccountMovementDate <= sD)
                                        select acm);
                            Pdebit = 0;
                            Pcrdit = 0;
                            foreach (var i in allP)
                            {

                                Pdebit = Pdebit + (i.Debit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true));
                                Pcrdit = Pcrdit + (i.Crdit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false));
                            }


                        }

                        if (l.Level == 3)
                        {
                            var cId = this.AppCompanyUserManager.ConvertToId(l.Id);
                            var all = (from acc in db.CustomAccountCategories
                                       where acc.CustomBalanceSheetTypeId == cId
                                       from ac in acc.CustomAccount
                                       from acm in ac.Account.AccountsMovements
                                       where (acm.AccountMovementDate >= sD && acm.AccountMovementDate <= eD)
                                       select acm);
                            debit = 0;
                            crdit = 0;
                            foreach (var i in all)
                            {
                                debit = debit + (i.Debit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true));
                                crdit = crdit + (i.Crdit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false));
                            }

                            var allP = (from acc in db.CustomAccountCategories
                                        where acc.CustomBalanceSheetTypeId == cId
                                        from ac in acc.CustomAccount
                                        from acm in ac.Account.AccountsMovements
                                        where (acm.AccountMovementDate >= PsD && acm.AccountMovementDate <= sD)
                                        select acm);
                            Pdebit = 0;
                            Pcrdit = 0;
                            foreach (var i in allP)
                            {
                                Pdebit = Pdebit + (i.Debit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true));
                                Pcrdit = Pcrdit + (i.Crdit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false));
                            }

                        }

                        if (l.Level == 2)
                        {
                            var cId = this.AppCompanyUserManager.ConvertToId(l.Id);
                            var all = (from bst in db.CustomBalanceSheetTypes
                                       where bst.CustomBalanceSheetId == cId
                                       from acc in bst.CustomAccountCategories
                                       from ac in acc.CustomAccount
                                       from acm in ac.Account.AccountsMovements
                                       where (acm.AccountMovementDate >= sD && acm.AccountMovementDate <= eD)
                                       select acm);
                            debit = 0;
                            crdit = 0;
                            foreach (var i in all)
                            {
                                debit = debit + (i.Debit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true));
                                crdit = crdit + (i.Crdit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false));
                            }
                            var allP = (from bst in db.CustomBalanceSheetTypes
                                        where bst.CustomBalanceSheetId == cId
                                        from acc in bst.CustomAccountCategories
                                        from ac in acc.CustomAccount
                                        from acm in ac.Account.AccountsMovements
                                        where (acm.AccountMovementDate >= PsD && acm.AccountMovementDate <= sD)
                                        select acm);
                            Pdebit = 0;
                            Pcrdit = 0;
                            foreach (var i in allP)
                            {
                                Pdebit = Pdebit + (i.Debit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true));
                                Pcrdit = Pcrdit + (i.Crdit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false));
                            }


                        }

                        l.amount = debit + crdit;
                        if (l.Level == 2)
                        {
                            var lId = this.AppCompanyUserManager.ConvertToId(l.Id);
                            l.amount = l.amount * this.AppCompanyUserManager.GetCustomAccountamountType(lId);
                        }


                        l.PreviousAmount = Pdebit + Pcrdit;
                        if (l.Level == 2)
                        {
                            var lId = this.AppCompanyUserManager.ConvertToId(l.Id);
                            l.PreviousAmount = l.PreviousAmount * this.AppCompanyUserManager.GetCustomAccountamountType(lId);

                        }

                    }

                    sendList.amount = sendList.Listes.Where(b => b.Level == 2).Sum(a => a.amount);
                    sendList.Pamount = sendList.Listes.Where(b => b.Level == 2).Sum(a => a.PreviousAmount);
                    sendList.pstartDate = PsD;
                    sendList.pendDate = sD;
                    return Ok(sendList);
                }
                else
                {
                    var sendList = allbranchListes.Where(a => a.ListId == f.id + "F").FirstOrDefault();
                    foreach (var l in sendList.Listes)
                    {
                        decimal debit = 0;
                        decimal crdit = 0;
                        decimal Pdebit = 0;
                        decimal Pcrdit = 0;
                        if (l.Level == 5)
                        {
                            var cId = this.AppCompanyUserManager.ConvertToId(l.Id);
                            var all = (from ac in db.CustomAccounts
                                       where ac.CustomAccountId == cId
                                       && ac.Account.BranchId == null
                                       from acm in ac.Account.AccountsMovements
                                       where (acm.AccountMovementDate >= sD && acm.AccountMovementDate <= eD)
                                       select acm);
                            foreach (var i in all)
                            {
                                i.Debit = this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true);
                                i.Crdit = this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false);
                            }
                            debit = (from ac in all
                                     select ac.Debit).DefaultIfEmpty(0).Sum();
                            crdit = (from ac in all
                                     select ac.Crdit).DefaultIfEmpty(0).Sum();

                            var allP = (from ac in db.CustomAccounts
                                        where ac.CustomAccountId == cId
                                         && ac.Account.BranchId == null
                                        from acm in ac.Account.AccountsMovements
                                        where (acm.AccountMovementDate >= PsD && acm.AccountMovementDate <= sD)
                                        select acm);
                            foreach (var i in allP)
                            {
                                i.Debit = this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true);
                                i.Crdit = this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false);
                            }
                            Pdebit = (from ac in allP
                                      select ac.Debit).DefaultIfEmpty(0).Sum();
                            Pcrdit = (from ac in allP
                                      select ac.Crdit).DefaultIfEmpty(0).Sum();
                        }
                        if (l.Level == 4)
                        {
                            var cId = this.AppCompanyUserManager.ConvertToId(l.Id);
                            var all = (from ac in db.CustomAccounts
                                       where ac.CustomAccountCategoryId == cId
                                        && ac.Account.BranchId == null
                                       from acm in ac.Account.AccountsMovements
                                       where (acm.AccountMovementDate >= sD && acm.AccountMovementDate <= eD)
                                       select acm);
                            debit = 0;
                            crdit = 0;
                            foreach (var i in all)
                            {


                                debit = debit + (i.Debit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true));
                                crdit = crdit + (i.Crdit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false));
                            }


                            var allP = (from ac in db.CustomAccounts
                                        where ac.CustomAccountCategoryId == cId
                                         && ac.Account.BranchId == null
                                        from acm in ac.Account.AccountsMovements
                                        where (acm.AccountMovementDate >= PsD && acm.AccountMovementDate <= sD)
                                        select acm);
                            Pdebit = 0;
                            Pcrdit = 0;
                            foreach (var i in allP)
                            {

                                Pdebit = Pdebit + (i.Debit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true));
                                Pcrdit = Pcrdit + (i.Crdit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false));
                            }


                        }

                        if (l.Level == 3)
                        {
                            var cId = this.AppCompanyUserManager.ConvertToId(l.Id);
                            var all = (from acc in db.CustomAccountCategories
                                       where acc.CustomBalanceSheetTypeId == cId
                                       from ac in acc.CustomAccount
                                        where ac.Account.BranchId == null
                                       from acm in ac.Account.AccountsMovements
                                       where (acm.AccountMovementDate >= sD && acm.AccountMovementDate <= eD)
                                       select acm);
                            debit = 0;
                            crdit = 0;
                            foreach (var i in all)
                            {
                                debit = debit + (i.Debit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true));
                                crdit = crdit + (i.Crdit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false));
                            }

                            var allP = (from acc in db.CustomAccountCategories
                                        where acc.CustomBalanceSheetTypeId == cId
                                        from ac in acc.CustomAccount
                                        from acm in ac.Account.AccountsMovements
                                        where (acm.AccountMovementDate >= PsD && acm.AccountMovementDate <= sD)
                                        select acm);
                            Pdebit = 0;
                            Pcrdit = 0;
                            foreach (var i in allP)
                            {
                                Pdebit = Pdebit + (i.Debit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true));
                                Pcrdit = Pcrdit + (i.Crdit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false));
                            }

                        }

                        if (l.Level == 2)
                        {
                            var cId = this.AppCompanyUserManager.ConvertToId(l.Id);
                            var all = (from bst in db.CustomBalanceSheetTypes
                                       where bst.CustomBalanceSheetId == cId
                                       from acc in bst.CustomAccountCategories
                                       from ac in acc.CustomAccount
                                       where  ac.Account.BranchId == null
                                       from acm in ac.Account.AccountsMovements
                                       where (acm.AccountMovementDate >= sD && acm.AccountMovementDate <= eD)
                                       select acm);
                            debit = 0;
                            crdit = 0;
                            foreach (var i in all)
                            {
                                debit = debit + (i.Debit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true));
                                crdit = crdit + (i.Crdit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false));
                            }
                            var allP = (from bst in db.CustomBalanceSheetTypes
                                        where bst.CustomBalanceSheetId == cId
                                        from acc in bst.CustomAccountCategories
                                        from ac in acc.CustomAccount
                                        where ac.Account.BranchId == null
                                        from acm in ac.Account.AccountsMovements
                                        where (acm.AccountMovementDate >= PsD && acm.AccountMovementDate <= sD)
                                        select acm);
                            Pdebit = 0;
                            Pcrdit = 0;
                            foreach (var i in allP)
                            {
                                Pdebit = Pdebit + (i.Debit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true));
                                Pcrdit = Pcrdit + (i.Crdit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false));
                            }


                        }

                        l.amount = debit + crdit;
                        if (l.Level == 2)
                        {
                            var lId = this.AppCompanyUserManager.ConvertToId(l.Id);
                            l.amount = l.amount * this.AppCompanyUserManager.GetCustomAccountamountType(lId);
                        }


                        l.PreviousAmount = Pdebit + Pcrdit;
                        if (l.Level == 2)
                        {
                            var lId = this.AppCompanyUserManager.ConvertToId(l.Id);
                            l.PreviousAmount = l.PreviousAmount * this.AppCompanyUserManager.GetCustomAccountamountType(lId);

                        }

                    }

                    sendList.amount = sendList.Listes.Where(b => b.Level == 2).Sum(a => a.amount);
                    sendList.Pamount = sendList.Listes.Where(b => b.Level == 2).Sum(a => a.PreviousAmount);
                    sendList.pstartDate = PsD;
                    sendList.pendDate = sD;
                    return Ok(sendList);
                }
            }
            else
            {
                var sendList = allbranchListes.Where(a => a.ListId == f.id + "F").FirstOrDefault();
                foreach (var l in sendList.Listes)
                {
                    decimal debit = 0;
                    decimal crdit = 0;
                    decimal Pdebit = 0;
                    decimal Pcrdit = 0;
                    if (l.Level == 5)
                    {
                        var cId = this.AppCompanyUserManager.ConvertToId(l.Id);
                        var all = (from ac in db.CustomAccounts
                                   where ac.CustomAccountId == cId
                                    && ac.Account.BranchId != null
                                   from acm in ac.Account.AccountsMovements
                                   where (acm.AccountMovementDate >= sD && acm.AccountMovementDate <= eD)
                                   select acm);
                        foreach (var i in all)
                        {
                            i.Debit = this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true);
                            i.Crdit = this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false);
                        }
                        debit = (from ac in all
                                 select ac.Debit).DefaultIfEmpty(0).Sum();
                        crdit = (from ac in all
                                 select ac.Crdit).DefaultIfEmpty(0).Sum();

                        var allP = (from ac in db.CustomAccounts
                                    where ac.CustomAccountId == cId
                                    from acm in ac.Account.AccountsMovements
                                    where (acm.AccountMovementDate >= PsD && acm.AccountMovementDate <= sD)
                                    select acm);
                        foreach (var i in allP)
                        {
                            i.Debit = this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true);
                            i.Crdit = this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false);
                        }
                        Pdebit = (from ac in allP
                                  select ac.Debit).DefaultIfEmpty(0).Sum();
                        Pcrdit = (from ac in allP
                                  select ac.Crdit).DefaultIfEmpty(0).Sum();
                    }
                    if (l.Level == 4)
                    {
                        var cId = this.AppCompanyUserManager.ConvertToId(l.Id);
                        var all = (from ac in db.CustomAccounts
                                   where ac.CustomAccountCategoryId == cId
                                   && ac.Account.BranchId != null
                                   from acm in ac.Account.AccountsMovements
                                   where (acm.AccountMovementDate >= sD && acm.AccountMovementDate <= eD)
                                   select acm);
                        debit = 0;
                        crdit = 0;
                        foreach (var i in all)
                        {


                            debit = debit + (i.Debit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true));
                            crdit = crdit + (i.Crdit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false));
                        }


                        var allP = (from ac in db.CustomAccounts
                                    where ac.CustomAccountCategoryId == cId
                                    && ac.Account.BranchId != null
                                    from acm in ac.Account.AccountsMovements
                                    where (acm.AccountMovementDate >= PsD && acm.AccountMovementDate <= sD)
                                    select acm);
                        Pdebit = 0;
                        Pcrdit = 0;
                        foreach (var i in allP)
                        {

                            Pdebit = Pdebit + (i.Debit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true));
                            Pcrdit = Pcrdit + (i.Crdit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false));
                        }


                    }

                    if (l.Level == 3)
                    {
                        var cId = this.AppCompanyUserManager.ConvertToId(l.Id);
                        var all = (from acc in db.CustomAccountCategories
                                   where acc.CustomBalanceSheetTypeId == cId
                                   from ac in acc.CustomAccount
                                   where ac.Account.BranchId != null
                                   from acm in ac.Account.AccountsMovements
                                   where (acm.AccountMovementDate >= sD && acm.AccountMovementDate <= eD)
                                   select acm);
                        debit = 0;
                        crdit = 0;
                        foreach (var i in all)
                        {
                            debit = debit + (i.Debit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true));
                            crdit = crdit + (i.Crdit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false));
                        }

                        var allP = (from acc in db.CustomAccountCategories
                                    where acc.CustomBalanceSheetTypeId == cId
                                    from ac in acc.CustomAccount
                                    where ac.Account.BranchId != null
                                    from acm in ac.Account.AccountsMovements
                                    where (acm.AccountMovementDate >= PsD && acm.AccountMovementDate <= sD)
                                    select acm);
                        Pdebit = 0;
                        Pcrdit = 0;
                        foreach (var i in allP)
                        {
                            Pdebit = Pdebit + (i.Debit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true));
                            Pcrdit = Pcrdit + (i.Crdit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false));
                        }

                    }

                    if (l.Level == 2)
                    {
                        var cId = this.AppCompanyUserManager.ConvertToId(l.Id);
                        var all = (from bst in db.CustomBalanceSheetTypes
                                   where bst.CustomBalanceSheetId == cId
                                   from acc in bst.CustomAccountCategories
                                   from ac in acc.CustomAccount
                                   where ac.Account.BranchId != null
                                   from acm in ac.Account.AccountsMovements
                                   where (acm.AccountMovementDate >= sD && acm.AccountMovementDate <= eD)
                                   select acm);
                        debit = 0;
                        crdit = 0;
                        foreach (var i in all)
                        {
                            debit = debit + (i.Debit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true));
                            crdit = crdit + (i.Crdit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false));
                        }
                        var allP = (from bst in db.CustomBalanceSheetTypes
                                    where bst.CustomBalanceSheetId == cId
                                    from acc in bst.CustomAccountCategories
                                    from ac in acc.CustomAccount
                                    where ac.Account.BranchId != null
                                    from acm in ac.Account.AccountsMovements
                                    where (acm.AccountMovementDate >= PsD && acm.AccountMovementDate <= sD)
                                    select acm);
                        Pdebit = 0;
                        Pcrdit = 0;
                        foreach (var i in allP)
                        {
                            Pdebit = Pdebit + (i.Debit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, true));
                            Pcrdit = Pcrdit + (i.Crdit * this.AppCompanyUserManager.GetAccountamountType(i.AccountId, false));
                        }


                    }

                    l.amount = debit + crdit;
                    if (l.Level == 2)
                    {
                        var lId = this.AppCompanyUserManager.ConvertToId(l.Id);
                        l.amount = l.amount * this.AppCompanyUserManager.GetCustomAccountamountType(lId);
                    }


                    l.PreviousAmount = Pdebit + Pcrdit;
                    if (l.Level == 2)
                    {
                        var lId = this.AppCompanyUserManager.ConvertToId(l.Id);
                        l.PreviousAmount = l.PreviousAmount * this.AppCompanyUserManager.GetCustomAccountamountType(lId);

                    }

                }

                sendList.amount = sendList.Listes.Where(b => b.Level == 2).Sum(a => a.amount);
                sendList.Pamount = sendList.Listes.Where(b => b.Level == 2).Sum(a => a.PreviousAmount);
                sendList.pstartDate = PsD;
                sendList.pendDate = sD;
                return Ok(sendList);
            }
        }

        // GET: api/AccountMovements/5
        [Route("GetAllAccountMovementsCatogeryDates")]
        [HttpPost]
        public IHttpActionResult GetAllCat(getAccountMovements or)
        {
            //GetAccountMovementsCatogeryDates
            DateTime sD = Convert.ToDateTime(or.startDate);
            DateTime eD = Convert.ToDateTime(or.endDate);
            
            var Orders = this.AppCompanyUserManager.GetAllAccountmovesCatogery(or.id, sD, eD ,or.IsCompany , or.IsCompany == true ? or.AllAccounts : false);
            foreach (var i in Orders)
            {
                i.Code = this.AppCompanyUserManager.GetAccountCode(or.id, i.id + "Acc");
            }
            if (QueryHelper.PropertyExists<AccountCatogeryLedger>(or.orderby))
            {

                switch (or.orderby)
                {
                    case "name":
                        if (or.orderType)
                        {
                            Orders = Orders.OrderByDescending(a => a.name).ToList();
                        }
                        else
                        {
                            Orders = Orders.OrderBy(a => a.name).ToList();

                        }

                        break;


                    case "debit":

                        if (or.orderType)
                        {
                            Orders = Orders.OrderByDescending(a => a.debit).ToList();
                        }
                        else
                        {
                            Orders = Orders.OrderBy(a => a.debit).ToList();

                        }
                        break;
                    case "crdit":


                        if (or.orderType)
                        {
                            Orders = Orders.OrderByDescending(a => a.crdit).ToList();
                        }
                        else
                        {
                            Orders = Orders.OrderBy(a => a.crdit).ToList();

                        }
                        break;
                    case "code":


                        if (or.orderType)
                        {
                            Orders = Orders.OrderByDescending(a => a.Code).ToList();
                        }
                        else
                        {
                            Orders = Orders.OrderBy(a => a.Code).ToList();

                        }
                        break;

                    default:
                        Orders = Orders.OrderByDescending(a => a.Code).ToList();
                        break;


                }
            }
            if (Orders.Count() == 0)
            {
                sD = DateTime.Now.AddDays(-365);
                eD = DateTime.Now;
                Orders = this.AppCompanyUserManager.GetAllAccountmovesCatogery(or.id, sD, eD, or.IsCompany, or.IsCompany == true ? or.AllAccounts : false).OrderByDescending(a => a.name).ToList();
            }

            var orders = Orders.ToList();

            if (!string.IsNullOrEmpty(or.search))
            {


                var ordersse = Orders.Where(a => a.name == or.search).ToList();
                if (ordersse.Count > 0)
                {
                    orders = ordersse;
                }
            }


          


            var result = new
            {
                TotalCount = 0,
                totalPages = 0,
                Orders = orders
            };

            return Ok(result);
        }

        // PUT: api/AccountMovements/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAccountMovement(int id, AccountMovement accountMovement)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != accountMovement.AccountMovementId)
            {
                return BadRequest();
            }

            db.Entry(accountMovement).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountMovementExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AccountMovements
        [ResponseType(typeof(AccountsMoves))]
        public IHttpActionResult PostAccountMovement(GetSetOrder setOrder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var accountM = new AccountsMoves();
            var db = new ApplicationDbContext();
            var order = db.AccountOrders.Find(setOrder.orderId);
            string userId = RequestContext.Principal.Identity.GetUserId();
            DateTime sD = DateTime.Now.AddDays(-29);
            DateTime eD = DateTime.Now;
            if (order != null)
            {
                var checkcom = this.AppCompanyUserManager.GetAccountOrders(setOrder.branchId , setOrder.IsCompany).Where(a => a.orderId == setOrder.orderId).FirstOrDefault();

                if (checkcom.crditorAccounts.Count > 1 && checkcom.debitorAccounts.Count > 1)
                {
                    return BadRequest("غير مسموح بتعديل قيود مركبة من الاستاذ المساعد , الرجاء استخدام دفتر اليومية للتعديل");
                }
                else if (checkcom.crditorAccounts.Count > 1 && checkcom.debitorAccounts.Count == 1)
                {
                    var checkfirstacc = checkcom.debitorAccounts.Where(a => a.account.id == setOrder.accountId).FirstOrDefault();
                    if (checkfirstacc == null)
                    {
                        return BadRequest("غير مسموح بتعديل قيود مركبة من الاستاذ المساعد , الرجاء استخدام دفتر اليومية للتعديل");
                    }
                    else
                    {
                        try
                        {
                            accountM = this.AppCompanyUserManager.SetOrder(setOrder, userId  , sD, eD);
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                    }
                }
                else if (checkcom.crditorAccounts.Count == 1 && checkcom.debitorAccounts.Count > 1)
                {
                    var checkfirstacc = checkcom.crditorAccounts.Where(a => a.account.id == setOrder.accountId).FirstOrDefault();
                    if (checkfirstacc == null)
                    {
                        return BadRequest("غير مسموح بتعديل قيود مركبة من الاستاذ المساعد , الرجاء استخدام دفتر اليومية للتعديل");
                    }
                    else
                    {
                        try
                        {
                            accountM = this.AppCompanyUserManager.SetOrder(setOrder, userId, sD, eD);
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                        //o.iscomplex = false;
                        
                    }
                }
                else if (checkcom.crditorAccounts.Count == 1 && checkcom.debitorAccounts.Count == 1)
                {
                    try
                    {
                        accountM = this.AppCompanyUserManager.SetOrder(setOrder, userId, sD, eD);
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }

            }
            else
            {
                try
                {
                     accountM = this.AppCompanyUserManager.SetOrder(setOrder, userId, sD, eD);
                }
                catch (Exception)
                {

                    throw;
                }
            }

            return Ok(accountM);
        
           
        }

        // DELETE: api/AccountMovements/5
        [ResponseType(typeof(AccountMovement))]
        public async Task<IHttpActionResult> DeleteAccountMovement(int id)
        {
            AccountMovement accountMovement = await db.AccountMovements.FindAsync(id);
            if (accountMovement == null)
            {
                return NotFound();
            }

            db.AccountMovements.Remove(accountMovement);
            await db.SaveChangesAsync();

            return Ok(accountMovement);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AccountMovementExists(int id)
        {
            return db.AccountMovements.Count(e => e.AccountMovementId == id) > 0;
        }
    }
}