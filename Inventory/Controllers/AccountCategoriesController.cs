﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Inventory.DataLayer;
using Inventory.Models;
using Inventory.ViewModels;

namespace Inventory.Controllers
{
   
    public class AccountCategoriesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationRepository repository = new ApplicationRepository();

        // GET: api/AccountCategories
        public IQueryable<AccountCategoriesVm> GetAccountCategories()
        {
            return repository.GetAllAccountCategories();
        }

        // GET: api/AccountCategories/5
        [ResponseType(typeof(AccountCategory))]
        public async Task<IHttpActionResult> GetAccountCategory(int id)
        {
            AccountCategory accountCategory = await db.AccountCategories.FindAsync(id);
            if (accountCategory == null)
            {
                return NotFound();
            }

            return Ok(accountCategory);
        }

        // PUT: api/AccountCategories/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAccountCategory(int id, AccountCategory accountCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != accountCategory.AccountCategoryId)
            {
                return BadRequest();
            }

            db.Entry(accountCategory).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountCategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AccountCategories
        [ResponseType(typeof(AccountCategory))]
        public async Task<IHttpActionResult> PostAccountCategory(AccountCategory accountCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.AccountCategories.Add(accountCategory);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = accountCategory.AccountCategoryId }, accountCategory);
        }

        // DELETE: api/AccountCategories/5
        [ResponseType(typeof(AccountCategory))]
        public async Task<IHttpActionResult> DeleteAccountCategory(int id)
        {
            AccountCategory accountCategory = await db.AccountCategories.FindAsync(id);
            if (accountCategory == null)
            {
                return NotFound();
            }

            db.AccountCategories.Remove(accountCategory);
            await db.SaveChangesAsync();

            return Ok(accountCategory);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AccountCategoryExists(int id)
        {
            return db.AccountCategories.Count(e => e.AccountCategoryId == id) > 0;
        }
    }
}