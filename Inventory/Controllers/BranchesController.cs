﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Inventory.DataLayer;
using Inventory.Models;
using Inventory.ViewModels;
using Microsoft.AspNet.Identity;

namespace Inventory.Controllers
{
    [RoutePrefix("api/Branches")]
    public class BranchesController : BaseApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationRepository repository = new ApplicationRepository();
        // GET: api/Branches
        [Route("GetAll")]
        public IQueryable<BranchesVm> GetBranchs()
        {
            return repository.GetAllBranches();
        }

        [Route("GetAll2/{id:int}")]
        public IQueryable<BranchesVm> GetBranchsBycompany(Int32 id)
        {
            //Only SuperAdmin or Admin can delete users (Later when implement roles)
           
                return  repository.GetAllBranches().Where(c => c.CompaniesId == id);
        }

        [Route("branchesById/{id:int}")]
        // GET: api/Companies/5
        [ResponseType(typeof(BranchesVm))]
        public async Task<IHttpActionResult> Getbranch(int id)
        {
            Branch branch = await db.Branchs.FindAsync(id);
            if (branch == null)
            {
                return NotFound();
            }
            var comVM = this.AppCompanyUserManager.GetAllBranchs(branch.CompanyId).Where(c => c.PublicId == branch.BranchId).SingleOrDefault();
            return Ok(comVM);
        }

       

        // POST: api/Branches
        [ResponseType(typeof(BranchesVm))]
        public async Task<IHttpActionResult> PostBranch(BranchesVm branchesVm)
        {
            string id;
            id = User.Identity.GetUserId();
            id = RequestContext.Principal.Identity.GetUserId();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Branch branch =await db.Branchs.FindAsync(branchesVm.PublicId);
            if(branch == null)
            {
                branch = new Branch()
                {
                    BranchName = branchesVm.name,
                    CompanyId = branchesVm.CompaniesId,
                    CurrentWorkerId = id

                };
                db.Branchs.Add(branch);

            }
            await db.SaveChangesAsync();

            BranchesVm banchesVm = this.AppCompanyUserManager.GetAllBranchs(branch.CompanyId).Where(c => c.PublicId == branch.BranchId).SingleOrDefault();
            return CreatedAtRoute("DefaultApi", new { id = banchesVm.PublicId }, banchesVm);
        }

        // DELETE: api/Branches/5
        [ResponseType(typeof(Branch))]
        public async Task<IHttpActionResult> DeleteBranch(int id)
        {
            Branch branch = await db.Branchs.FindAsync(id);
            if (branch == null)
            {
                return NotFound();
            }

            db.Branchs.Remove(branch);
            await db.SaveChangesAsync();

            return Ok(branch);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BranchExists(int id)
        {
            return db.Branchs.Count(e => e.BranchId == id) > 0;
        }
    }
}