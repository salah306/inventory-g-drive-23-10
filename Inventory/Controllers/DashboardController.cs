﻿using Inventory.DataLayer;
using Inventory.Models;
using Inventory.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Inventory.Controllers
{
    [RoutePrefix("api/dashboard")]
    [Authorize]
    public class DashboardController : BaseApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        [Route("Getdashitems")]
        [HttpPost]
        public IHttpActionResult Getdashitems(getInventoryAccounts company)
        {
            int year = DateTime.Now.Year;
            DateTime firstDay = new DateTime(year, 1, 1);
            DateTime lastDay = new DateTime(year, 12, 31);
            dashbordVm dash = new dashbordVm();
            if (company.isCompany)
            {
                var flist = db.FinancialLists.Where(a => a.CompanyId == company.companyId && a.FinancialListName == "الدخل"
                                                || a.FinancialListName == "دخل"
                                                || a.FinancialListName == "قائمة الدخل"
                                                || a.FinancialListName == "قائمة دخل").SingleOrDefault();
                var debitAccounts = db.CustomAccounts.Where(a => a.CustomAccountCategory.CustomBalanceSheetTypes.CustomBalanceSheet.AccountType.AccountTypeName == "مدين"
                                     && a.CustomAccountCategory.CustomBalanceSheetTypes.CustomBalanceSheet.FinancialListId == flist.FinancialListId).ToList();
                var crditAccounts = db.CustomAccounts.Where(a => a.CustomAccountCategory.CustomBalanceSheetTypes.CustomBalanceSheet.AccountType.AccountTypeName == "دائن"
                                     && a.CustomAccountCategory.CustomBalanceSheetTypes.CustomBalanceSheet.FinancialListId == flist.FinancialListId).ToList();
                decimal debitAmount = 0;
                decimal crditAmount = 0;
                foreach (var item in debitAccounts)
                {
                    var debit = item.Account.AccountsMovements.Where(a => a.Debit > 0 && a.Crdit == 0 && a.AccountMovementDate >= firstDay && a.AccountMovementDate <= lastDay).Select(a => a.Debit).DefaultIfEmpty(0).Sum();
                    var crdit = item.Account.AccountsMovements.Where(a => a.Crdit > 0 && a.Debit == 0 && a.AccountMovementDate >= firstDay && a.AccountMovementDate <= lastDay).Select(a => a.Crdit).DefaultIfEmpty(0).Sum();
                    debitAmount = debitAmount + debit - crdit;
                    if(debit > 0 || crdit > 0)
                    {

                    }
                }
                foreach (var item in crditAccounts)
                {
                    var debit = item.Account.AccountsMovements.Where(a => a.Debit > 0 && a.Crdit == 0 && a.AccountMovementDate > firstDay && a.AccountMovementDate <= lastDay).Select(a => a.Debit).DefaultIfEmpty(0).Sum();
                    var crdit = item.Account.AccountsMovements.Where(a => a.Crdit > 0 && a.Debit == 0 && a.AccountMovementDate > firstDay && a.AccountMovementDate <= lastDay).Select(a => a.Crdit).DefaultIfEmpty(0).Sum();
                    crditAmount = crditAmount +  + crdit - debit;
                }
               decimal totalRevnue =Math.Round((crditAmount - debitAmount) , 2);

                var turnoverRatePricein = db.SubAccountMovements.Where(a => a.SubAccount.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                && a.SubAccount.Account.AccountCategory.AccountsDataTypes.TypeName == "مخازن"
                && a.SubAccountMovementDate >= firstDay
                && a.SubAccountMovementDate <= lastDay && a.OrderNote == "مبيعات").Select(a => a.ItemTotalPriceOut).DefaultIfEmpty(0).Sum() -
            db.SubAccountMovements.Where(a => a.SubAccount.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                && a.SubAccount.Account.AccountCategory.AccountsDataTypes.TypeName == "مخازن"
                  && a.SubAccountMovementDate >= firstDay
                && a.SubAccountMovementDate <= lastDay && a.OrderNote == "رد مبيعات").Select(a => a.ItemTotalPriceIn).DefaultIfEmpty(0).Sum();

                var AvgCost = db.SubAccountMovements.Where(a => a.SubAccount.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                && a.SubAccount.Account.AccountCategory.AccountsDataTypes.TypeName == "مخازن"
                && a.SubAccountMovementDate >= firstDay
                 && a.SubAccountMovementDate <= lastDay).OrderByDescending(a => a.SubAccountMovementDate).FirstOrDefault().ItemTotalStockPrice;


                var turnoverRate = 0m;
                double stockRate = 0;


                if (turnoverRatePricein == 0 || AvgCost == 0)
                {
                    turnoverRate = 0;
                }
                else
                {
                    turnoverRate = Math.Round((turnoverRatePricein / AvgCost), 1);
                    stockRate = Math.Round(360 / (double)turnoverRate);
                    // stockRate =Math.Round( (stockRate / 360) * (report.MaxDate - report.MinDate).TotalDays);
                }
                DateTime dateToday = DateTime.Today;

                var salesT = db.AccountOrders.Where(a => a.IsCompany == true && a.CompanyId == company.companyId
                                                         && a.IsSales == true && a.OrderTypeRefrence == "مبيعات"
                                                         && DbFunctions.TruncateTime(a.OrderDate) == dateToday).ToList();
                var returnSalesT = db.AccountOrders.Where(a => a.IsCompany == true && a.CompanyId == company.companyId
                                                         && a.IsSales == true && a.OrderTypeRefrence == "رد مبيعات"
                                                       && DbFunctions.TruncateTime(a.OrderDate) == dateToday).ToList();
                var sumDebit = 0m;
                var sumCrdit = 0m;
                foreach (var acc in salesT)
                {
                    sumDebit += acc.AccountMovements.Select(a => a.Debit).DefaultIfEmpty(0).Sum();
                }
                foreach (var acc in returnSalesT)
                {
                    sumCrdit += acc.AccountMovements.Select(a => a.Debit).DefaultIfEmpty(0).Sum();
                }
                var SalesToday = sumDebit - sumCrdit;

                dash.revnue =(decimal)totalRevnue;
                dash.turnOverRate = turnoverRate;
                dash.sales = SalesToday;
                return Ok(dash);
            }
            else
            {
               
                return Ok();
            }
        }
    }
}
