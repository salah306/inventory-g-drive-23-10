﻿using Inventory.DataLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Inventory.Models;
using Inventory.Utility;

namespace Inventory.Controllers
{
    [RoutePrefix("api/inventoryCard")]
    public class inventoryCardController : BaseApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        [Route("GetinventoryAccounts")]
        [HttpPost]
        public IHttpActionResult GetinventoryAccounts(getInventoryAccounts company)
        {
            if (company.isCompany)
            {
                var accounts = db.Accounts.Where(a => a.AccountCategory.AccountsDataTypes.TypeName == "مخازن" && a.BranchId == null && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId)
                    .Select(a => new inventoryAccounts()
                    {
                        AccountId = a.AccountId,
                        AccountName = a.AccountName
                    }).ToList();
                return Ok(accounts);
            }
            else
            {
                var accounts = db.Accounts.Where(a => a.AccountCategory.AccountsDataTypes.TypeName == "مخازن" && a.BranchId == company.companyId)
                     .Select(a => new inventoryAccounts()
                     {
                         AccountId = a.AccountId,
                         AccountName = a.AccountName
                     }).ToList(); ;
                return Ok(accounts);
            }
        }

        [Route("getInventoryitems")]
        [HttpPost]
        public IHttpActionResult getInventoryitems(getInventoryitems report)
        {

            if (report.isCompany)
            {
                if(report.reportName == "item")
                {
                    var getitemqty = (from i in db.SubAccounts
                                      where i.AccountId == report.inventoryId
                                      select new billitemsQty()
                                      {
                                          itemPropertyValueId = i.SubAccountId,
                                          itemPropertyValueName = i.SubAccountName,
                                          Code = i.ItemInfo.code,

                                      }).ToList();

                    return Ok(getitemqty);
                }
                if (report.reportName == "itemGroup")
                {
                    var getitemqty = (from i in db.ItemGroups
                                      where i.CompanyId == report.companyId
                                      select new billitemsQty()
                                      {
                                          itemPropertyValueId = i.ItemGroupId,
                                          itemPropertyValueName = i.ItemGroupName,
                                          Code = i.Unit.UnitName

                                      }).ToList();

                    
                    return Ok(getitemqty);
                }

                return Ok();
            }
            else
            {
              
                return Ok();
            }


        }

        [Route("getItemMoves")]
        [HttpPost]
        public IHttpActionResult getItemMoves(getInventoryitemsMoves report)
        {

            DateTime sD = Convert.ToDateTime(report.MinDate);
            DateTime eD = Convert.ToDateTime(report.MaxDate);
            var days = (eD - sD).TotalDays;
            DateTime PsD = sD.AddDays(-days);
            #region report Name = item
            if (report.reportName == "item")
            {
                var turnoverRatePricein = db.SubAccountMovements.Where(a => a.SubAccountId == report.subAccountId
                            && a.SubAccount.AccountId == report.inventoryId
                            && a.SubAccountMovementDate <= report.MaxDate && a.OrderNote == "مبيعات").Select(a => a.ItemTotalPriceOut).DefaultIfEmpty(0).Sum() -
                            db.SubAccountMovements.Where(a => a.SubAccountId == report.subAccountId
                            && a.SubAccount.AccountId == report.inventoryId
                            && a.SubAccountMovementDate <= report.MaxDate && a.OrderNote == "رد مبيعات").Select(a => a.ItemTotalPriceIn).DefaultIfEmpty(0).Sum();
                var AvgCost = 0m;
                if (db.SubAccountMovements.Where(a => a.SubAccountId == report.subAccountId
                 && a.SubAccount.AccountId == report.inventoryId
                 && a.SubAccountMovementDate <= report.MaxDate).OrderByDescending(a => a.SubAccountMovementDate).FirstOrDefault() != null)
                {
                    AvgCost = db.SubAccountMovements.Where(a => a.SubAccountId == report.subAccountId
                && a.SubAccount.AccountId == report.inventoryId
                && a.SubAccountMovementDate <= report.MaxDate).OrderByDescending(a => a.SubAccountMovementDate).FirstOrDefault().ItemTotalStockPrice;
                }

               


                var turnoverRate = 0m;
                double stockRate = 0;


                if (turnoverRatePricein == 0 || AvgCost == 0)
                {
                    turnoverRate = 0;
                }
                else
                {
                    turnoverRate = Math.Round((turnoverRatePricein / AvgCost), 1);
                    stockRate = Math.Round(360 / (double)turnoverRate);
                    // stockRate =Math.Round( (stockRate / 360) * (report.MaxDate - report.MinDate).TotalDays);
                }

                var firstMove = db.SubAccountMovements.Where(a => a.SubAccountId == report.subAccountId
                && a.SubAccount.AccountId == report.inventoryId
                && a.SubAccountMovementDate < report.MinDate)
                          .Select(a => new itemMoves()
                          {
                              moveDate = a.SubAccountMovementDate,
                              note = a.OrderNote,
                              qtyin = 0,
                              pricein = 0,
                              totalPricein = 0,
                              qtyout = 0,
                              priceout = 0,
                              totalPriceout = a.ItemTotalPriceOut,
                              totalstockqty = a.ItemStockQuantity,
                              stockprice = a.ItemStockPrice,
                              totalstockPrice = a.ItemTotalStockPrice
                          }).OrderByDescending(a => a.moveDate).FirstOrDefault() ?? new itemMoves()
                          {
                              moveDate = DateTime.Now,
                              note = "",
                              qtyin = 0,
                              pricein = 0,
                              totalPricein = 0,
                              qtyout = 0,
                              priceout = 0,
                              totalPriceout = 0,
                              totalstockqty = 0,
                              stockprice = 0,
                              totalstockPrice = 0
                          };

                var totalCount = db.SubAccountMovements.Where(a => a.SubAccountId == report.subAccountId
              && a.SubAccount.AccountId == report.inventoryId && a.SubAccountMovementDate >= report.MinDate && a.SubAccountMovementDate <= report.MaxDate)
                          .Select(a => new itemMoves()
                          {
                              moveDate = a.SubAccountMovementDate,
                              note = a.OrderNote,
                              qtyin = a.QuantityIn,
                              pricein = a.ItemPricein,
                              totalPricein = a.ItemTotalPriceIn,
                              qtyout = a.QuantityOut,
                              priceout = a.ItemPriceOut,
                              totalPriceout = a.ItemTotalPriceOut,
                              totalstockqty = a.ItemStockQuantity,
                              stockprice = a.ItemStockPrice,
                              totalstockPrice = a.ItemTotalStockPrice
                          }).Count();
                var totalPages = Math.Ceiling((double)totalCount / report.pageSize);

                var stock = db.SubAccountMovements.Where(a => a.SubAccountId == report.subAccountId
                && a.SubAccount.AccountId == report.inventoryId && a.SubAccountMovementDate >= report.MinDate && a.SubAccountMovementDate <= report.MaxDate)
                            .Select(a => new itemMoves()
                            {
                                moveDate = a.SubAccountMovementDate,
                                note = a.OrderNote,
                                qtyin = a.QuantityIn,
                                pricein = a.ItemPricein,
                                totalPricein = a.ItemTotalPriceIn,
                                qtyout = a.QuantityOut,
                                priceout = a.ItemPriceOut,
                                totalPriceout = a.ItemTotalPriceOut,
                                totalstockqty = a.ItemStockQuantity,
                                stockprice = a.ItemStockPrice,
                                totalstockPrice = a.ItemTotalStockPrice
                            }).ToList();
             

                if (QueryHelper.PropertyExists<itemMoves>(report.orderby))
                {

                    switch (report.orderby)
                    {
                      

                        case "moveDate":

                            if (report.orderType)
                            {
                                stock = stock.OrderByDescending(a => a.moveDate).ToList();
                            }
                            else
                            {
                                stock = stock.OrderBy(a => a.moveDate).ToList();

                            }
                            break;
                       

                        default:
                            stock = stock.OrderByDescending(a => a.moveDate).ToList();
                            break;


                    }
                }
                var num = 0;
                foreach (var item in stock)
                {
                    num += 1;
                    item.indexNo = num;

                }
                var orders = stock.Skip((report.pageNumber - 1) * report.pageSize)
                                .Take(report.pageSize)
                                .ToList();
                var totalIn = db.SubAccountMovements.Where(a => a.SubAccountId == report.subAccountId
                && a.SubAccountMovementDate >= report.MinDate && a.SubAccountMovementDate <= report.MaxDate).Select(a => a.ItemTotalPriceIn).DefaultIfEmpty(0).Sum();
                var totalOut = db.SubAccountMovements.Where(a => a.SubAccountId == report.subAccountId
                && a.SubAccountMovementDate >= report.MinDate && a.SubAccountMovementDate <= report.MaxDate).Select(a => a.ItemTotalPriceOut).DefaultIfEmpty(0).Sum();
                if(orders.Count == 0)
                {
                    orders = stock.Take(50).ToList();

                }
                itemsMoveWithFirsMove moves = new itemsMoveWithFirsMove()
                {
                    firsMove = firstMove,
                    itemMoves = orders,
                    StockRate = (int)stockRate,
                    turnoverRate = turnoverRate,
                    totalIn = totalIn,
                    totalOut = totalOut,
                    total = stock.Count > 0 ? stock[stock.Count - 1].totalstockPrice : 0,
                    TotalCount = totalCount,
                    totalPages = totalPages,
                };
                return Ok(moves);
            }
            #endregion

            #region report Name = itemGroup
            if (report.reportName == "itemGroup")
            {
                var turnoverRatePricein = db.SubAccountMovements.Where(a => a.SubAccount.ItemInfo.ItemGroupId == report.subAccountId
                            && a.SubAccount.AccountId == report.inventoryId
                            && a.SubAccountMovementDate <= report.MaxDate && a.OrderNote == "مبيعات").Select(a => a.ItemTotalPriceOut).DefaultIfEmpty(0).Sum() -
                            db.SubAccountMovements.Where(a => a.SubAccount.ItemInfo.ItemGroupId == report.subAccountId
                            && a.SubAccount.AccountId == report.inventoryId
                            && a.SubAccountMovementDate <= report.MaxDate && a.OrderNote == "رد مبيعات").Select(a => a.ItemTotalPriceIn).DefaultIfEmpty(0).Sum();

                var AvgCost = db.SubAccountMovements.Where(a => a.SubAccount.ItemInfo.ItemGroupId == report.subAccountId
                 && a.SubAccount.AccountId == report.inventoryId
                 && a.SubAccountMovementDate <= report.MaxDate).OrderByDescending(a => a.SubAccountMovementDate).FirstOrDefault().ItemTotalStockPrice;


                var turnoverRate = 0m;
                double stockRate = 0;


                if (turnoverRatePricein == 0 || AvgCost == 0)
                {
                    turnoverRate = 0;
                }
                else
                {
                    turnoverRate = Math.Round((turnoverRatePricein / AvgCost), 1);
                    stockRate = Math.Round(360 / (double)turnoverRate);
                    // stockRate =Math.Round( (stockRate / 360) * (report.MaxDate - report.MinDate).TotalDays);
                }

 





                var stock = db.SubAccountMovements.Where(a => a.SubAccount.AccountId == report.inventoryId
                && a.SubAccount.ItemInfo.ItemGroupId == report.subAccountId
                && a.SubAccountMovementDate >= report.MinDate && a.SubAccountMovementDate <= report.MaxDate)
                            .GroupBy(a => a.SubAccount.SubAccountName)
                            .Select(a => new
                            {
                                itemName = a.Key
                                
                            }).ToList();
                var itemMovesGroupList = new List<itemMovesGroup>();
                foreach (var item in stock)
                {


                    var turnoverRatePriceinitem = db.SubAccountMovements.Where(a => a.SubAccount.SubAccountName == item.itemName
             && a.SubAccount.AccountId == report.inventoryId
             && a.SubAccountMovementDate <= report.MaxDate && a.OrderNote == "مبيعات").Select(a => a.ItemTotalPriceOut).DefaultIfEmpty(0).Sum() -
             db.SubAccountMovements.Where(a => a.SubAccount.SubAccountName == item.itemName
             && a.SubAccount.AccountId == report.inventoryId
             && a.SubAccountMovementDate <= report.MaxDate && a.OrderNote == "رد مبيعات").Select(a => a.ItemTotalPriceIn).DefaultIfEmpty(0).Sum();

                    var AvgCostitem = db.SubAccountMovements.Where(a => a.SubAccount.SubAccountName == item.itemName
                     && a.SubAccount.AccountId == report.inventoryId
                     && a.SubAccountMovementDate <= report.MaxDate).OrderByDescending(a => a.SubAccountMovementDate).FirstOrDefault().ItemTotalStockPrice;


                    var turnoverRateitem = 0m;
                    double stockRateitem = 0;


                    if (turnoverRatePriceinitem == 0 || AvgCostitem == 0)
                    {
                        turnoverRateitem = 0;
                    }
                    else
                    {
                        turnoverRateitem = Math.Round((turnoverRatePriceinitem / AvgCostitem), 1);
                        stockRateitem = Math.Round(360 / (double)turnoverRateitem);
                        // stockRate =Math.Round( (stockRate / 360) * (report.MaxDate - report.MinDate).TotalDays);
                    }

                    var move = new itemMovesGroup()
                    {
                        itemName = item.itemName,
                        turnOVer = turnoverRateitem,
                        StockRate =(int) stockRateitem,
                        qty = db.SubAccountMovements.Where(a => a.SubAccount.SubAccountName == item.itemName 
                                                           && a.SubAccountMovementDate == (db.SubAccountMovements.Where(s => s.SubAccount.SubAccountName == item.itemName
                                                           && s.SubAccountMovementDate < report.MinDate).Select(d => d.SubAccountMovementDate).Max()))
                                     .Select(a => a.ItemStockQuantity).SingleOrDefault(),
                        qtyin = db.SubAccountMovements.Where(a => a.SubAccount.SubAccountName == item.itemName
                                                           && a.SubAccountMovementDate >= report.MinDate 
                                                           && a.SubAccountMovementDate <= report.MaxDate)
                                 .Select(a => a.QuantityIn).DefaultIfEmpty(0).Sum(),
                        qtyout = db.SubAccountMovements.Where(a => a.SubAccount.SubAccountName == item.itemName
                                                           && a.SubAccountMovementDate >= report.MinDate
                                                           && a.SubAccountMovementDate <= report.MaxDate)
                                .Select(a => a.QuantityOut).DefaultIfEmpty(0).Sum(),

                        stockprice = db.SubAccountMovements.Where(a => a.SubAccount.SubAccountName == item.itemName
                                                           && a.SubAccountMovementDate ==
                                                           (db.SubAccountMovements.Where(s => s.SubAccount.SubAccountName == item.itemName
                                                           && a.SubAccountMovementDate >= report.MinDate
                                                           && a.SubAccountMovementDate <= report.MaxDate).Select(d => d.SubAccountMovementDate).Max())).OrderBy(a => a.SubAccountMovementDate)
                                     .Select(a => a.ItemStockPrice).DefaultIfEmpty(0).FirstOrDefault(),

                        totalstockPrice = db.SubAccountMovements.Where(a => a.SubAccount.SubAccountName == item.itemName
                                                           && a.SubAccountMovementDate ==
                                                           (db.SubAccountMovements.Where(s => s.SubAccount.SubAccountName == item.itemName 
                                                           && a.SubAccountMovementDate >= report.MinDate
                                                           && a.SubAccountMovementDate <= report.MaxDate).Select(d => d.SubAccountMovementDate).Max())).OrderBy(a => a.SubAccountMovementDate)
                                          .Select(a => a.ItemTotalStockPrice).DefaultIfEmpty(0).FirstOrDefault(),

                        totalstockqty = db.SubAccountMovements.Where(a => a.SubAccount.SubAccountName == item.itemName
                                                           && a.SubAccountMovementDate ==
                                                           (db.SubAccountMovements.Where(s => s.SubAccount.SubAccountName == item.itemName 
                                                           && a.SubAccountMovementDate >= report.MinDate
                                                           && a.SubAccountMovementDate <= report.MaxDate).Select(d => d.SubAccountMovementDate).Max())).OrderBy(a => a.SubAccountMovementDate)
                                      .Select(a => a.ItemStockQuantity).DefaultIfEmpty(0).FirstOrDefault(),

                    };
                    itemMovesGroupList.Add(move);
                }
               

               
                var totalIn = db.SubAccountMovements.Where(a => a.SubAccountId == report.subAccountId
                && a.SubAccountMovementDate >= report.MinDate && a.SubAccountMovementDate <= report.MaxDate).Select(a => a.ItemTotalPriceIn).DefaultIfEmpty(0).Sum();
                var totalOut = db.SubAccountMovements.Where(a => a.SubAccountId == report.subAccountId
                && a.SubAccountMovementDate >= report.MinDate && a.SubAccountMovementDate <= report.MaxDate).Select(a => a.ItemTotalPriceOut).DefaultIfEmpty(0).Sum();

                var totalCount =itemMovesGroupList.Count();
                var totalPages = Math.Ceiling((double)totalCount / report.pageSize);
              
                if (QueryHelper.PropertyExists<itemMovesGroup>(report.orderby))
                {

                    switch (report.orderby)
                    {
                        case "qty":

                            if (report.orderType)
                            {
                                // itemMovesGroupList = itemMovesGroupList.OrderByDescending(b => b.qty).ToList();
                                itemMovesGroupList = itemMovesGroupList.OrderByDescending(b => b.qty).ToList();
                            }
                            else
                            {
                                // itemMovesGroupList = itemMovesGroupList.OrderBy(b => b.qty).ToList();
                                itemMovesGroupList = itemMovesGroupList.OrderBy(b => b.qty).OrderBy(b => b.qty).ToList();
                            }
                            break;

                        case "totalstockqty":

                            if (report.orderType)
                            {
                               // itemMovesGroupList = itemMovesGroupList.OrderByDescending(b => b.qty).ToList();
                                itemMovesGroupList = itemMovesGroupList.OrderByDescending(b => b.totalstockqty).ToList();
                            }
                            else
                            {
                               // itemMovesGroupList = itemMovesGroupList.OrderBy(b => b.qty).ToList();
                                itemMovesGroupList = itemMovesGroupList.OrderBy(b =>b.qty).OrderBy(b => b.totalstockqty).ToList();
                            }
                            break;

                        case "totalstockPrice":

                            if (report.orderType)
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderByDescending(a => a.totalstockPrice).ToList();
                            }
                            else
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderBy(a => a.totalstockPrice).ToList();

                            }
                            break;
                        case "stockprice":

                            if (report.orderType)
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderByDescending(a => a.stockprice).ToList();
                            }
                            else
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderBy(a => a.stockprice).ToList();

                            }
                            break;
                        case "turnOVer":

                            if (report.orderType)
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderByDescending(a => a.turnOVer).ToList();
                            }
                            else
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderBy(a => a.turnOVer).ToList();

                            }
                            break;
                        case "StockRate":

                            if (report.orderType)
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderByDescending(a => a.StockRate).ToList();
                            }
                            else
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderBy(a => a.StockRate).ToList();

                            }
                            break;
                        case "qtyin":

                            if (report.orderType)
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderByDescending(a => a.qtyin).ToList();
                            }
                            else
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderBy(a => a.qtyin).ToList();

                            }
                            break;
                        case "qtyout":

                            if (report.orderType)
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderByDescending(a => a.qtyout).ToList();
                            }
                            else
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderBy(a => a.qtyout).ToList();

                            }
                            break;
                        case "itemName":

                            if (report.orderType)
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderByDescending(a => a.itemName).ToList();
                            }
                            else
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderBy(a => a.itemName).ToList();

                            }
                            break;
                        default:
                            itemMovesGroupList = itemMovesGroupList.OrderByDescending(a => a.itemName).ToList();
                            break;


                    }
                }

                var num = 0;
                foreach (var item in itemMovesGroupList)
                {
                    num += 1;
                    item.indexNo = num;

                }
                var orders = itemMovesGroupList.Skip((report.pageNumber - 1) * report.pageSize)
                              .Take(report.pageSize)
                              .ToList();
                itemsMoveGroupWithFirsMove moves = new itemsMoveGroupWithFirsMove()
                {
                    itemMoves = orders,
                    StockRate = (int)stockRate,
                    turnoverRate = turnoverRate,
                    totalIn = totalIn,
                    totalOut = totalOut,
                    total = itemMovesGroupList.Select(a => a.totalstockPrice).DefaultIfEmpty(0).Sum(),
                    TotalCount = totalCount,
                    totalPages = totalPages,
                };
                return Ok(moves);
            }
            #endregion

            #region report Name = itemGroup
            if (report.reportName == "allItem")
            {
                var turnoverRatePricein = db.SubAccountMovements.Where(a=> a.SubAccount.AccountId == report.inventoryId
                            && a.SubAccountMovementDate <= report.MaxDate && a.OrderNote == "مبيعات").Select(a => a.ItemTotalPriceOut).DefaultIfEmpty(0).Sum() -
                            db.SubAccountMovements.Where(a => a.SubAccount.AccountId == report.inventoryId
                            && a.SubAccountMovementDate <= report.MaxDate && a.OrderNote == "رد مبيعات").Select(a => a.ItemTotalPriceIn).DefaultIfEmpty(0).Sum();

                var AvgCost = db.SubAccountMovements.Where(a => a.SubAccount.AccountId == report.inventoryId
                 && a.SubAccountMovementDate <= report.MaxDate).OrderByDescending(a => a.SubAccountMovementDate).FirstOrDefault().ItemTotalStockPrice;


                var turnoverRate = 0m;
                double stockRate = 0;


                if (turnoverRatePricein == 0 || AvgCost == 0)
                {
                    turnoverRate = 0;
                }
                else
                {
                    turnoverRate = Math.Round((turnoverRatePricein / AvgCost), 1);
                    stockRate = Math.Round(360 / (double)turnoverRate);
                    // stockRate =Math.Round( (stockRate / 360) * (report.MaxDate - report.MinDate).TotalDays);
                }







                var stock = db.SubAccountMovements.Where(a => a.SubAccount.AccountId == report.inventoryId
                && a.SubAccountMovementDate >= report.MinDate && a.SubAccountMovementDate <= report.MaxDate)
                            .GroupBy(a => a.SubAccount.SubAccountName)
                            .Select(a => new
                            {
                                itemName = a.Key

                            }).ToList();
                var itemMovesGroupList = new List<itemMovesGroup>();
                foreach (var item in stock)
                {


                    var turnoverRatePriceinitem = db.SubAccountMovements.Where(a => a.SubAccount.SubAccountName == item.itemName
             && a.SubAccount.AccountId == report.inventoryId
             && a.SubAccountMovementDate <= report.MaxDate && a.OrderNote == "مبيعات").Select(a => a.ItemTotalPriceOut).DefaultIfEmpty(0).Sum() -
             db.SubAccountMovements.Where(a => a.SubAccount.SubAccountName == item.itemName
             && a.SubAccount.AccountId == report.inventoryId
             && a.SubAccountMovementDate <= report.MaxDate && a.OrderNote == "رد مبيعات").Select(a => a.ItemTotalPriceIn).DefaultIfEmpty(0).Sum();

                    var AvgCostitem = db.SubAccountMovements.Where(a => a.SubAccount.SubAccountName == item.itemName
                     && a.SubAccount.AccountId == report.inventoryId
                     && a.SubAccountMovementDate <= report.MaxDate).OrderByDescending(a => a.SubAccountMovementDate).FirstOrDefault().ItemTotalStockPrice;


                    var turnoverRateitem = 0m;
                    double stockRateitem = 0;


                    if (turnoverRatePriceinitem == 0 || AvgCostitem == 0)
                    {
                        turnoverRateitem = 0;
                    }
                    else
                    {
                        turnoverRateitem = Math.Round((turnoverRatePriceinitem / AvgCostitem), 1);
                        stockRateitem = Math.Round(360 / (double)turnoverRateitem);
                        // stockRate =Math.Round( (stockRate / 360) * (report.MaxDate - report.MinDate).TotalDays);
                    }

                    var move = new itemMovesGroup()
                    {
                        itemName = item.itemName,
                        turnOVer = turnoverRateitem,
                        StockRate = (int)stockRateitem,
                        qty = db.SubAccountMovements.Where(a => a.SubAccount.SubAccountName == item.itemName
                                                           && a.SubAccountMovementDate == (db.SubAccountMovements.Where(s => s.SubAccount.SubAccountName == item.itemName
                                                           && s.SubAccountMovementDate < report.MinDate).Select(d => d.SubAccountMovementDate).Max()))
                                     .Select(a => a.ItemStockQuantity).SingleOrDefault(),
                        qtyin = db.SubAccountMovements.Where(a => a.SubAccount.SubAccountName == item.itemName
                                                           && a.SubAccountMovementDate >= report.MinDate
                                                           && a.SubAccountMovementDate <= report.MaxDate)
                                 .Select(a => a.QuantityIn).DefaultIfEmpty(0).Sum(),
                        qtyout = db.SubAccountMovements.Where(a => a.SubAccount.SubAccountName == item.itemName
                                                           && a.SubAccountMovementDate >= report.MinDate
                                                           && a.SubAccountMovementDate <= report.MaxDate)
                                .Select(a => a.QuantityOut).DefaultIfEmpty(0).Sum(),

                        stockprice = db.SubAccountMovements.Where(a => a.SubAccount.SubAccountName == item.itemName
                                                           && a.SubAccountMovementDate ==
                                                           (db.SubAccountMovements.Where(s => s.SubAccount.SubAccountName == item.itemName
                                                           && a.SubAccountMovementDate >= report.MinDate
                                                           && a.SubAccountMovementDate <= report.MaxDate).Select(d => d.SubAccountMovementDate).Max())).OrderBy(a => a.SubAccountMovementDate)
                                     .Select(a => a.ItemStockPrice).DefaultIfEmpty(0).FirstOrDefault(),

                        totalstockPrice = db.SubAccountMovements.Where(a => a.SubAccount.SubAccountName == item.itemName
                                                           && a.SubAccountMovementDate ==
                                                           (db.SubAccountMovements.Where(s => s.SubAccount.SubAccountName == item.itemName
                                                           && a.SubAccountMovementDate >= report.MinDate
                                                           && a.SubAccountMovementDate <= report.MaxDate).Select(d => d.SubAccountMovementDate).Max())).OrderBy(a => a.SubAccountMovementDate)
                                          .Select(a => a.ItemTotalStockPrice).DefaultIfEmpty(0).FirstOrDefault(),

                        totalstockqty = db.SubAccountMovements.Where(a => a.SubAccount.SubAccountName == item.itemName
                                                           && a.SubAccountMovementDate ==
                                                           (db.SubAccountMovements.Where(s => s.SubAccount.SubAccountName == item.itemName
                                                           && a.SubAccountMovementDate >= report.MinDate
                                                           && a.SubAccountMovementDate <= report.MaxDate).Select(d => d.SubAccountMovementDate).Max())).OrderBy(a => a.SubAccountMovementDate)
                                      .Select(a => a.ItemStockQuantity).DefaultIfEmpty(0).FirstOrDefault(),

                    };
                    itemMovesGroupList.Add(move);
                }



                var totalIn = db.SubAccountMovements.Where(a => a.SubAccountId == report.subAccountId
                && a.SubAccountMovementDate >= report.MinDate && a.SubAccountMovementDate <= report.MaxDate).Select(a => a.ItemTotalPriceIn).DefaultIfEmpty(0).Sum();
                var totalOut = db.SubAccountMovements.Where(a => a.SubAccountId == report.subAccountId
                && a.SubAccountMovementDate >= report.MinDate && a.SubAccountMovementDate <= report.MaxDate).Select(a => a.ItemTotalPriceOut).DefaultIfEmpty(0).Sum();

                var totalCount = itemMovesGroupList.Count();
                var totalPages = Math.Ceiling((double)totalCount / report.pageSize);

                if (QueryHelper.PropertyExists<itemMovesGroup>(report.orderby))
                {

                    switch (report.orderby)
                    {
                        case "qty":

                            if (report.orderType)
                            {
                                // itemMovesGroupList = itemMovesGroupList.OrderByDescending(b => b.qty).ToList();
                                itemMovesGroupList = itemMovesGroupList.OrderByDescending(b => b.qty).ToList();
                            }
                            else
                            {
                                // itemMovesGroupList = itemMovesGroupList.OrderBy(b => b.qty).ToList();
                                itemMovesGroupList = itemMovesGroupList.OrderBy(b => b.qty).OrderBy(b => b.qty).ToList();
                            }
                            break;

                        case "totalstockqty":

                            if (report.orderType)
                            {
                                // itemMovesGroupList = itemMovesGroupList.OrderByDescending(b => b.qty).ToList();
                                itemMovesGroupList = itemMovesGroupList.OrderByDescending(b => b.totalstockqty).ToList();
                            }
                            else
                            {
                                // itemMovesGroupList = itemMovesGroupList.OrderBy(b => b.qty).ToList();
                                itemMovesGroupList = itemMovesGroupList.OrderBy(b => b.qty).OrderBy(b => b.totalstockqty).ToList();
                            }
                            break;

                        case "totalstockPrice":

                            if (report.orderType)
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderByDescending(a => a.totalstockPrice).ToList();
                            }
                            else
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderBy(a => a.totalstockPrice).ToList();

                            }
                            break;
                        case "stockprice":

                            if (report.orderType)
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderByDescending(a => a.stockprice).ToList();
                            }
                            else
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderBy(a => a.stockprice).ToList();

                            }
                            break;
                        case "turnOVer":

                            if (report.orderType)
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderByDescending(a => a.turnOVer).ToList();
                            }
                            else
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderBy(a => a.turnOVer).ToList();

                            }
                            break;
                        case "StockRate":

                            if (report.orderType)
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderByDescending(a => a.StockRate).ToList();
                            }
                            else
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderBy(a => a.StockRate).ToList();

                            }
                            break;
                        case "qtyin":

                            if (report.orderType)
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderByDescending(a => a.qtyin).ToList();
                            }
                            else
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderBy(a => a.qtyin).ToList();

                            }
                            break;
                        case "qtyout":

                            if (report.orderType)
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderByDescending(a => a.qtyout).ToList();
                            }
                            else
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderBy(a => a.qtyout).ToList();

                            }
                            break;
                        case "itemName":

                            if (report.orderType)
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderByDescending(a => a.itemName).ToList();
                            }
                            else
                            {
                                itemMovesGroupList = itemMovesGroupList.OrderBy(a => a.itemName).ToList();

                            }
                            break;
                        default:
                            itemMovesGroupList = itemMovesGroupList.OrderByDescending(a => a.itemName).ToList();
                            break;


                    }
                }

                var num = 0;
                foreach (var item in itemMovesGroupList)
                {
                    num += 1;
                    item.indexNo = num;

                }
                var orders = itemMovesGroupList.Skip((report.pageNumber - 1) * report.pageSize)
                              .Take(report.pageSize)
                              .ToList();
                itemsMoveGroupWithFirsMove moves = new itemsMoveGroupWithFirsMove()
                {
                    itemMoves = orders,
                    StockRate = (int)stockRate,
                    turnoverRate = turnoverRate,
                    totalIn = totalIn,
                    totalOut = totalOut,
                    total = itemMovesGroupList.Select(a => a.totalstockPrice).DefaultIfEmpty(0).Sum(),
                    TotalCount = totalCount,
                    totalPages = totalPages,
                };
                return Ok(moves);
            }
            #endregion
            return BadRequest();

        }

    }
}
