﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Inventory.DataLayer;
using Inventory.Models;
using Inventory.ViewModels;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;
using Inventory.Managers;

namespace Inventory.Controllers
{
    [RoutePrefix("api/BalanceSheets")]
    public class BalanceSheetsController : BaseApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private BalanceSheetsManager BalanceSheetsManager;
        public BalanceSheetsController()
        {
            this.BalanceSheetsManager = new BalanceSheetsManager(this.db);
        }
       // private ApplicationRepository repository = new ApplicationRepository();
        // GET: api/BalanceSheets
        public IQueryable<BranchTree> GetBalanceSheets()
        {
            return BalanceSheetsManager.GetAllBalanceSheets();
        }

  

        [Route("accountCategoryProperties/{id:int}")]
        public IQueryable<AccountCategoryPropertiesVM> GetAccountCategoryProperties(Int32 id)
        {
            return BalanceSheetsManager.GetAccountCatogeryProperties(id);
        }

        [Route("getAccountsDataTypes")]
        [HttpPost]
        public IQueryable<AccountsCatogeryDataTypes> GetDataTypes(SetAccountsCatogeryDataTypes ac)
        {
            return BalanceSheetsManager.GetAllDataTypes(ac);

        }

        //[Route("GetAllAccounts/{id:int}")]
        //public IQueryable<AccountCategoryPropertiesVM> GetAllAccounta(Int32 id)
        //{
        //    return this.AppCompanyUserManager.GetAccountCatogeryProperties(id);
        //}

        [HttpGet]
        [Route("getaccvalue/{id}")]
        public IHttpActionResult GetAccCatogeryPropertiesValue(string id)
        {
            return Ok(BalanceSheetsManager.GetAccountCatogeryPropertiesValue(BalanceSheetsManager.ConvertToId(id)).OrderByDescending(a => a.Values.Count()));
           
        }
        // POST: api/BalanceSheets
        [ResponseType(typeof(List<AccountCategoryPropertiesValuesVM>))]
        [Route("setaccvalue")]
        [HttpPost]
        public async Task<IHttpActionResult> SetAccCatogeryPropertiesValue(SetAccPropertiesValuesVM val)
        {
            AccountCategoryPropertiesValue accv = db.AccountCategoryPropertiesValues.Where(a => a.AccountCategoryPropertiesValueName == val.value && a.AccountCategoryPropertiesId == val.AccountPropertyId).FirstOrDefault();
            string uid = RequestContext.Principal.Identity.GetUserId();

            if (BalanceSheetsManager.CheckDataType(val.value, val.DataTypeName))
            {
                
                accv = new AccountCategoryPropertiesValue()
                {

                    AccountCategoryPropertiesValueName = val.value,
                    AccountCategoryPropertiesId = val.AccountPropertyId,
                    AccountId = val.SubAccountId,
                    CurrentWorkerId =uid
                };

                db.AccountCategoryPropertiesValues.Add(accv);
            }
            else
            {
                return BadRequest("  من فضلك تاكد من صحة البيانات وعدم تكرارها"); 
            }

            try
            {
                await db.SaveChangesAsync();

                Int32 accId = db.AccountCategoryPropertiesValues.Where(a => a.AccountCategoryPropertiesValueName == val.value && a.AccountCategoryPropertiesId == val.AccountPropertyId).FirstOrDefault().AccountId;
                return Ok(BalanceSheetsManager.GetAccountCatogeryPropertiesValue(BalanceSheetsManager.ConvertToId(accId.ToString())));
            }
            catch (Exception e)
            {

                return BadRequest(e.InnerException.ToString());
            }
           
        }

        [ResponseType(typeof(List<AccountCategoryPropertiesValuesVM>))]
        [Route("updateaccvalue")]
        [HttpPost]
        public async Task<IHttpActionResult> UpdateAccCatogeryPropertiesValue(UpdateAccPropertiesValuesVM val)
        {
            AccountCategoryPropertiesValue accv = db.AccountCategoryPropertiesValues.Where(a => a.AccountCategoryPropertiesValueId == val.AccountPropertyvalId).FirstOrDefault();
            string uid = RequestContext.Principal.Identity.GetUserId();
            if(accv != null)
            {
                var datatype = db.AccountsDataTypes.Find(accv.AccountCategoryProperties);

                if (BalanceSheetsManager.CheckDataType(val.value, datatype.TypeName))
                {



                    accv.AccountCategoryPropertiesValueName = val.value;
                    accv.CurrentWorkerId = uid;

                    db.Entry(accv).State = EntityState.Modified;

                }
                else
                {
                    throw new System.ArgumentException(" حدث خطاء من فضلك تاكد من صحة البيانات وعدم تكرارها");
                }
            }
            else
            {
                return NotFound();
            }
         

            try
            {
                await db.SaveChangesAsync();

                Int32 accId = db.AccountCategoryPropertiesValues.Where(a => a.AccountCategoryPropertiesValueId == accv.AccountCategoryPropertiesValueId).FirstOrDefault().AccountId;
                return Ok(BalanceSheetsManager.GetAccountCatogeryPropertiesValue(BalanceSheetsManager.ConvertToId(accId.ToString())));
            }
            catch (Exception e)
            {

               return BadRequest(e.InnerException.ToString());
            }

        }

        [Route("removeaccvalue/{id:int}")]
        [HttpGet]
        [ResponseType(typeof(List<AccountCategoryPropertiesValuesVM>))]

        public async Task<IHttpActionResult> removeAccountValue(int id)
        {
            AccountCategoryPropertiesValue accountCategoryProperties = db.AccountCategoryPropertiesValues.Find(id);
            Int32 accId = db.AccountCategoryPropertiesValues.Where(a => a.AccountCategoryPropertiesValueId == id).FirstOrDefault().AccountId;

            if (accountCategoryProperties == null)
            {
                return NotFound();
            }

            db.AccountCategoryPropertiesValues.Remove(accountCategoryProperties);
            await db.SaveChangesAsync();
            return Ok(BalanceSheetsManager.GetAccountCatogeryPropertiesValue(accId));

        }

        [ResponseType(typeof(List<AccountCategoryPropertiesVM>))]
        [Route("saveAccountCategoryProperties")]
        [HttpPost]
        public async Task<IHttpActionResult> SaveAccountCategoryProperties(AccountCategoryPropertiesVM accPropList)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            string uid = RequestContext.Principal.Identity.GetUserId();

            var acp = db.AccountCategoryProperties.Where(a =>  a.AccountCategoryPropertiesName == accPropList.Name).FirstOrDefault();
            if (acp == null)
            {
                acp = new AccountCategoryProperties()
                {
                    AccountCategoryPropertiesName = accPropList.Name,
                    //AccountCategoryId = accPropList.AccountCategoryId,
                    //DataTypeId = accPropList.DataTypeId,
                    CurrentWorkerId = uid
                };

                db.AccountCategoryProperties.Add(acp);
            }

            try
            {
                await db.SaveChangesAsync();

                var prop = BalanceSheetsManager.GetAccountCatogeryProperties(accPropList.AccountCategoryId);
                return Ok(prop);
            }
            catch (Exception)
            {

                throw;
            }

        
        }



         [Route("branchTreeById")]
        [HttpPost]
        public async Task<IHttpActionResult> Getbranch(getAccountList accList)
        {
            if (accList.Iscompany)
            {
               
               Company company = await db.Companies.FindAsync(accList.Id);

                if (company == null)
                {
                    return NotFound();

                }
             //   var bb = new List<List<AccountsTableFlatten>>();
             //   bb.Add(BalanceSheetsManager.bsTree(company.CompanyId).ToList());
             //return Ok(bb);
                var Tbs = (from bs in db.BalanceSheets
                           where bs.CompanyId == accList.Id
                           orderby bs.CreatedDate
                           select new AccountsTable
                           {
                               Id = bs.BalanceSheetId + "Bs",
                               AccountLevel = "رئيسي",
                               AccountName = bs.BalanceSheetName,
                               TitleForAdd = "اضافة حـ عام",
                               Code = db.BalanceSheets.Where(a => a.CompanyId == accList.Id).OrderBy(a => a.CreatedDate).Count(x => x.BalanceSheetId < bs.BalanceSheetId) + 1,
                               ParentId = bs.CompanyId.ToString(),
                               AccountType = bs.AccountType.AccountTypeName

                           }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);
               
              
                var Tbst = (from bst in db.BalanceSheetTypes
                            where bst.BalanceSheet.CompanyId == accList.Id
                            orderby bst.CreatedDate
                            select new AccountsTable
                            {
                                Id = bst.BalanceSheetTypeId + "Bst",
                                AccountLevel = "عام",
                                AccountName = bst.BalanceSheetTypeName,
                                TitleForAdd = "اضافة حـ مساعد",
                                ParentId = bst.BalanceSheetId + "Bs",
                                AccountType = bst.AccountType.AccountTypeName,
                                Code = db.BalanceSheetTypes.Where(a => a.BalanceSheetId == bst.BalanceSheetId).OrderBy(a => a.CreatedDate).Count(x => x.BalanceSheetTypeId < bst.BalanceSheetTypeId) + 1,
                            }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


                List<AccountsTable> Taccounts = new List<AccountsTable>();


                var TAcc = (from Acc in db.AccountCategories
                            where Acc.BalanceSheetType.BalanceSheet.CompanyId == accList.Id
                            orderby Acc.CreatedDate
                            select new AccountsTable
                            {
                                Id = Acc.AccountCategoryId + "Acc",
                                AccountLevel = "مساعد",
                                AccountName = Acc.AccountCategoryName,
                                AccountType = Acc.AccountType.AccountTypeName,
                                TitleForAdd = "اضافة حـ فرعي",
                                Code = db.AccountCategories.Where(a => a.BalanceSheetTypeId == Acc.BalanceSheetTypeId).OrderBy(a => a.CreatedDate).Count(x => x.AccountCategoryId < Acc.AccountCategoryId) + 1,
                                ParentId = Acc.BalanceSheetTypeId + "Bst",
                                AccountsDataTypesId = (Int32)Acc.AccountsDataTypesId,
                                AccountsDataTypesName = Acc.AccountsDataTypes.TypeName,
                            }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);




                var TAc = (from Ac in db.Accounts
                           where Ac.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == accList.Id
                           orderby Ac.CreatedDate
                           select new AccountsTable
                           {
                               Id = Ac.AccountId + "Ac",
                               AccountLevel = "فرعي",
                               TitleForAdd = "اضافة حـ جزئي",
                               AccountName = Ac.AccountName,
                               Code = db.Accounts.Where(a => a.AccountCategoryId == Ac.AccountCategoryId).OrderBy(a => a.CreatedDate).Count(x => x.AccountId < Ac.AccountId) + 1,
                               ParentId = Ac.AccountCategoryId + "Acc",
                               linkedAccountId = Ac.Branch == null ? Ac.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId : Ac.BranchId,
                               linkedAccountName = Ac.Branch == null ? Ac.AccountCategory.BalanceSheetType.BalanceSheet.Company.CompanyName : Ac.Branch.BranchName,
                               isCompany = Ac.Branch == null ? true : false, 
                             
                               Activated = Ac.Activated
                           }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


                var TSac = (from Ac in db.SubAccounts
                            where Ac.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == accList.Id
                            orderby Ac.CreatedDate
                            select new AccountsTable
                            {
                                Id = Ac.SubAccountId + "Sac",
                                AccountLevel = "جزئي",
                                AccountName = Ac.SubAccountName,
                                Code = db.SubAccounts.Where(a => a.AccountId == Ac.AccountId).OrderBy(a => a.CreatedDate).Count(x => x.SubAccountId < Ac.SubAccountId) + 1,
                                ParentId = Ac.AccountId + "Ac"
                            }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


                foreach (var acc in TAc)
                {
                    foreach (var ac in TSac)
                    {

                        if (ac[ac.Count - 1].ParentId == acc[0].Id)
                        {
                            ac.Where(a => a.ParentId == acc[0].Id).ToList().ForEach(c => c.fullCode = acc[0].Code.ToString() + " - " + c.Code.ToString());
                            acc.AddRange(ac);
                        }
                    }
                }

                foreach (var acc in TAcc)
                {
                    foreach (var ac in TAc)
                    {

                        if (ac[0].ParentId == acc[0].Id)
                        {

                            acc.AddRange(ac);
                        }
                    }
                }

                foreach (var bst in Tbst)
                {
                    foreach (var acc in TAcc)
                    {

                        if (acc[0].ParentId == bst[0].Id)
                        {
                            bst.AddRange(acc);
                        }
                    }
                }

                foreach (var bs in Tbs)
                {
                    foreach (var bst in Tbst)
                    {

                        if (bst[0].ParentId == bs[0].Id)
                        {


                            bs.AddRange(bst);

                        }
                    }
                }

                foreach (var i in Tbs)
                {
                    foreach (var a in i)
                    {
                        i.Where(w => w.ParentId == a.Id).ToList().ForEach(c => c.Code = int.Parse(a.Code.ToString() + c.Code.ToString()));

                    }
                }
                Tbs.OrderBy(a => a[0].Code);

                return Ok(Tbs.OrderBy(a => a[0].fullCode));

            }
            else
            {
                Branch branch = await db.Branchs.FindAsync(accList.Id);

                if (branch == null)
                {
                    return NotFound();

                }
                var Tbs = (from bs in db.BalanceSheets
                           where bs.CompanyId == branch.CompanyId
                           orderby bs.CreatedDate
                           select new AccountsTable
                           {
                               Id = bs.BalanceSheetId + "Bs",
                               AccountLevel = "رئيسي",
                               AccountName = bs.BalanceSheetName,
                               TitleForAdd = "اضافة حـ عام",
                               Code = db.BalanceSheets.Where(a => a.CompanyId == accList.Id).OrderBy(a => a.CreatedDate).Count(x => x.BalanceSheetId < bs.BalanceSheetId) + 1,
                               ParentId = bs.CompanyId.ToString(),
                               AccountType = bs.AccountType.AccountTypeName

                           }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


                var Tbst = (from bst in db.BalanceSheetTypes
                            where bst.BalanceSheet.CompanyId == branch.CompanyId
                            orderby bst.CreatedDate
                            select new AccountsTable
                            {
                                Id = bst.BalanceSheetTypeId + "Bst",
                                AccountLevel = "عام",
                                AccountName = bst.BalanceSheetTypeName,
                                TitleForAdd = "اضافة حـ مساعد",
                                ParentId = bst.BalanceSheetId + "Bs",
                                AccountType = bst.AccountType.AccountTypeName,
                                Code = db.BalanceSheetTypes.Where(a => a.BalanceSheetId == bst.BalanceSheetId).OrderBy(a => a.CreatedDate).Count(x => x.BalanceSheetTypeId < bst.BalanceSheetTypeId) + 1,
                            }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


                List<AccountsTable> Taccounts = new List<AccountsTable>();


                var TAcc = (from Acc in db.AccountCategories
                            where Acc.BalanceSheetType.BalanceSheet.CompanyId == branch.CompanyId
                            orderby Acc.CreatedDate
                            select new AccountsTable
                            {
                                Id = Acc.AccountCategoryId + "Acc",
                                AccountLevel = "مساعد",
                                AccountName = Acc.AccountCategoryName,
                                AccountType = Acc.AccountType.AccountTypeName,
                                TitleForAdd = "اضافة حـ فرعي",
                                Code = db.AccountCategories.Where(a => a.BalanceSheetTypeId == Acc.BalanceSheetTypeId).OrderBy(a => a.CreatedDate).Count(x => x.AccountCategoryId < Acc.AccountCategoryId) + 1,
                                ParentId = Acc.BalanceSheetTypeId + "Bst"
                            }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);




                var TAc = (from Ac in db.Accounts
                           where Ac.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId ==branch.CompanyId
                           && Ac.BranchId  == branch.BranchId
                           orderby Ac.CreatedDate
                           select new AccountsTable
                           {
                               Id = Ac.AccountId + "Ac",
                               AccountLevel = "فرعي",
                               TitleForAdd = "اضافة حـ جزئي",
                               AccountName = Ac.AccountName,
                               Code = db.Accounts.Where(a => a.AccountCategoryId == Ac.AccountCategoryId).OrderBy(a => a.CreatedDate).Count(x => x.AccountId < Ac.AccountId) + 1,
                               ParentId = Ac.AccountCategoryId + "Acc",
                               linkedAccountId = Ac.Branch == null ? Ac.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId : Ac.BranchId,
                               linkedAccountName = Ac.Branch == null ? Ac.AccountCategory.BalanceSheetType.BalanceSheet.Company.CompanyName : Ac.Branch.BranchName,
                               isCompany = Ac.Branch == null ? true : false
                           }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


                var TSac = (from Ac in db.SubAccounts
                            where Ac.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == branch.CompanyId
                            && Ac.Account.BranchId == branch.BranchId
                            orderby Ac.CreatedDate
                            select new AccountsTable
                            {
                                Id = Ac.SubAccountId + "Sac",
                                AccountLevel = "جزئي",
                                AccountName = Ac.SubAccountName,
                                Code = db.SubAccounts.Where(a => a.AccountId == Ac.AccountId).OrderBy(a => a.CreatedDate).Count(x => x.SubAccountId < Ac.SubAccountId) + 1,
                                ParentId = Ac.AccountId + "Ac"
                            }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


                foreach (var acc in TAc)
                {
                    foreach (var ac in TSac)
                    {

                        if (ac[ac.Count - 1].ParentId == acc[0].Id)
                        {
                            ac.Where(a => a.ParentId == acc[0].Id).ToList().ForEach(c => c.fullCode = acc[0].Code.ToString() + " - " + c.Code.ToString());
                            acc.AddRange(ac);
                        }
                    }
                }

                foreach (var acc in TAcc)
                {
                    foreach (var ac in TAc)
                    {

                        if (ac[0].ParentId == acc[0].Id)
                        {

                            acc.AddRange(ac);
                        }
                    }
                }

                foreach (var bst in Tbst)
                {
                    foreach (var acc in TAcc)
                    {

                        if (acc[0].ParentId == bst[0].Id)
                        {
                            bst.AddRange(acc);
                        }
                    }
                }

                foreach (var bs in Tbs)
                {
                    foreach (var bst in Tbst)
                    {

                        if (bst[0].ParentId == bs[0].Id)
                        {


                            bs.AddRange(bst);

                        }
                    }
                }

                foreach (var i in Tbs)
                {
                    foreach (var a in i)
                    {
                        i.Where(w => w.ParentId == a.Id).ToList().ForEach(c => c.Code = int.Parse(a.Code.ToString() + c.Code.ToString()));

                    }
                }
                Tbs.OrderBy(a => a[0].Code);

                return Ok(Tbs.OrderBy(a => a[0].fullCode));
            }
            
        }

        [Route("financialListTreeById/{id:int}")]
        [ResponseType(typeof(BranchTree))]
        public IHttpActionResult GetFinancialList(int id)
        {
            FinancialList financialList =db.FinancialLists.Find(id);
            if (financialList == null)
            {
                return NotFound();
            }
            var comVM = this.AppCompanyUserManager.GetAllFinancialListTree(financialList.FinancialListId).ToList();
            
            return Ok(comVM);
        }

        [Route("GetAllFinancialList")]
        [HttpPost]
       // [ResponseType(typeof(BranchTree))]
        public IHttpActionResult GetAllFinancialList(isCompanyBranch set)
        {
            Int32 id = 0;
            if (set.IsCompany)
            {
                Company financialList = db.Companies.Find(set.Id);
                id = financialList.CompanyId;
                if (financialList == null)
                {
                    return NotFound();
                }
            }
            else
            {
                Branch financialList = db.Branchs.Find(set.Id);
                id = financialList.CompanyId;
                if (financialList == null)
                {
                    return NotFound();
                }
            }

            
            List<GetFinancialList> ftree = new List<Models.GetFinancialList>();

            var cac = (from c in db.CustomAccounts where c.CustomAccountCategory.CustomBalanceSheetTypes.CustomBalanceSheet.FinancialList.CompanyId == id  select new FListes {Id = c.CustomAccountId + "Ac" ,Show = false ,Name = c.Account.AccountName  , Level = 5  , ParentId = c.CustomAccountCategoryId  +"Acc",LevelName="فرعي", ParentName = c.CustomAccountCategory.AccountCategoryName}).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList(); 
            var cacc = (from c in db.CustomAccountCategories select c).OrderBy(r => r.CreatedDate).Where(c => c.CustomBalanceSheetTypes.CustomBalanceSheet.FinancialList.CompanyId == id).Select(c => new FListes { Id = c.CustomAccountCategoryId + "Acc", Show = false,childern = c.CustomAccount.Count() ,Name = c.AccountCategoryName, Level = 4, AddTitle = "اضافة حسابات فرعية", Addlink = "ربط مع حساب مساعد", IsLinked = false, LinkedAccountId = c.LinkedAccountId, LinkedAccountName = c.LinkedAccountName, LevelName = "مساعد", ParentId = c.CustomBalanceSheetTypeId  +"Bst", ParentName = c.CustomBalanceSheetTypes.BalanceSheetTypeName }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();
            var bst = ((from c in db.CustomBalanceSheetTypes select c).OrderBy(r => r.CreatedDate).Where(c=> c.CustomBalanceSheet.FinancialList.CompanyId == id).Select(c => new FListes { Id = c.CustomBalanceSheetTypeId + "Bst", Show = false, childern = c.CustomAccountCategories.Count(),Name = c.BalanceSheetTypeName, Level = 3, AddTitle = "اضافة حساب مساعد", Addlink = "ربط مع حساب عام", LevelName = "عام", IsLinked = false, LinkedAccountId = c.LinkedAccountId, LinkedAccountName = c.LinkedAccountName, ParentId = c.CustomBalanceSheetId + "Bs", ParentName = c.CustomBalanceSheet.BalanceSheetName })).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();
            var bs = (from c in db.CustomBalanceSheets select c).OrderBy(r => r.CreatedDate).Where(c => c.FinancialList.CompanyId == id).Select(c => new FListes { Id = c.CustomBalanceSheetId + "Bs", Name = c.BalanceSheetName, Show = true, accountType = c.AccountType.AccountTypeName ,Level = 2, childern = c.CustomBalanceSheetTypes.Count(), AddTitle="اضافة حساب عام" , Addlink = "ربط مع حساب رئيسي" ,IsLinked = false , LinkedAccountId = c.LinkedAccountId , LinkedAccountName = c.LinkedAccountName ,LevelName = "رئيسي",ParentId = c.FinancialListId + "F", ParentName = c.FinancialList.FinancialListName }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();
            var fl = (from c in db.FinancialLists where c.CompanyId == id orderby c.CreatedDate select new FListes { Id = c.FinancialListId + "F", forLevel = c.FinancialListId + "F", Name = c.FinancialListName, Level = 1, Show = true, ParentId = c.BranchId.ToString(), childern = c.CustomBalanceSheets.Count(), ParentName = c.Branch.BranchName }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList();
          
            List<FListes> listes = new List<FListes>();
            
            foreach(var mainList in fl)
            {
                foreach(var f in mainList)
                {
                    listes.Add(f);
                    foreach (var balancesheet in bs)
                    {
                        foreach(var customBS in balancesheet)
                        {
                            if(customBS.ParentId == f.Id)
                            {
                                customBS.forLevel = f.Id;
                                listes.Insert(listes.IndexOf(f)  , customBS);
                                foreach (var Bstype in bst)
                                {
                                    foreach (var CustomBst in Bstype)
                                    {
                                        if(CustomBst.ParentId == customBS.Id)
                                        {
                                            CustomBst.forLevel = f.Id;
                                            listes.Insert(listes.IndexOf(customBS), CustomBst);
                                            foreach(var CustomAccountc in cacc)
                                            {
                                                foreach(var CustomAcc in CustomAccountc)
                                                {
                                                    if (CustomAcc.ParentId == CustomBst.Id)
                                                    {
                                                        CustomAcc.forLevel = f.Id;
                                                        listes.Insert(listes.IndexOf(CustomBst), CustomAcc);
                                                        foreach(var Customacco in cac)
                                                        {
                                                            foreach(var CustomAc in Customacco)
                                                            {
                                                                if (CustomAc.ParentId == CustomAcc.Id)
                                                                {
                                                                    CustomAc.forLevel = f.Id;
                                                                    listes.Insert(listes.IndexOf(CustomAcc), CustomAc);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                            }

                          

                        }
                    }
                }
            }
           var g = listes.GroupBy(u => u.forLevel).Select(grp => grp.ToList()).ToList();
            List<Alllists> listes5 = new List<Alllists>();

            foreach (var l in g)
            {

                listes5.Add(new Alllists { ListId = l[l.Count - 1].Id, ListName = l[l.Count - 1].Name, forWidget = false, Listes = l });
            }

            foreach (var l in listes5)
            {
                l.Listes.RemoveAll(a => a.Id == l.ListId && a.Name == l.ListName);
            }

            return Ok(listes5);
        }


        // GET: api/BalanceSheets/5
        [ResponseType(typeof(BalanceSheet))]
        public async Task<IHttpActionResult> GetBalanceSheet(int id)
        {
            BalanceSheet balanceSheet = await db.BalanceSheets.FindAsync(id);
            if (balanceSheet == null)
            {
                return NotFound();
            }

            return Ok(balanceSheet);
        }

        [ResponseType(typeof(BranchTree))]

        [Route("updateBalanceSheet")]
        [HttpPost]
        public async Task<IHttpActionResult> UpdateBalanceSheet(PostAccountsTable t)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            string uid = RequestContext.Principal.Identity.GetUserId();

           

            if(t.old.AccountLevel == "رئيسي")
            {
                var exsit = db.BalanceSheets.Find(this.AppCompanyUserManager.ConvertToId(t.old.Id));
                if (exsit == null)
                {
                    return BadRequest(ModelState);

                }
                else
                {
                    exsit.BalanceSheetName = t.edited.AccountName;
                    exsit.AccountTypeId = db.AccountTypes.Where(a => a.AccountTypeName == t.edited.AccountType).SingleOrDefault().AccountTypeId;

                    exsit.CurrentWorkerId = uid;

                    db.Entry(exsit).State = EntityState.Modified;


                }
            }
           else if (t.old.AccountLevel == "عام")
            {
                var exsit = db.BalanceSheetTypes.Find(this.AppCompanyUserManager.ConvertToId(t.old.Id));
                if (exsit == null)
                {
                    return BadRequest();

                }
                else
                {
                    exsit.BalanceSheetTypeName = t.edited.AccountName;
                    exsit.CurrentWorkerId = uid;
                    exsit.AccountTypeId = db.AccountTypes.Where(a => a.AccountTypeName == t.edited.AccountType).SingleOrDefault().AccountTypeId;

                    db.Entry(exsit).State = EntityState.Modified;


                }
            }
           else if (t.old.AccountLevel == "مساعد")
            {
                var exsit = db.AccountCategories.Find(this.AppCompanyUserManager.ConvertToId(t.old.Id));
                if (exsit == null)
                {
                    return BadRequest();

                }
                else
                {
                    exsit.AccountCategoryName = t.edited.AccountName;
                    exsit.AccountTypeId = db.AccountTypes.Where(a => a.AccountTypeName == t.edited.AccountType).SingleOrDefault().AccountTypeId;
                    exsit.CurrentWorkerId = uid;
                    exsit.AccountsDataTypesId = t.edited.AccountsDataTypesId;
                    db.Entry(exsit).State = EntityState.Modified;

                }
            }
           else if (t.old.AccountLevel == "فرعي")
            {
                var exsit = db.Accounts.Find(this.AppCompanyUserManager.ConvertToId(t.old.Id));
                if (exsit == null)
                {
                    return BadRequest();

                }
                else
                {
                    exsit.AccountName = t.edited.AccountName;
                    exsit.CurrentWorkerId = uid;
                    exsit.BranchId = t.edited.isCompany == false ? t.edited.linkedAccountId : null;
                    if (t.edited.isCompany)
                    {
                        exsit.Branch = null;
                    }
                    exsit.Activated = t.edited.Activated;
                   
                  
                    db.Entry(exsit).State = EntityState.Modified;

                }
            }
            else if (t.old.AccountLevel == "جزئي")
            {
                var exsit = db.SubAccounts.Find(this.AppCompanyUserManager.ConvertToId(t.old.Id));
                if (exsit == null)
                {
                    return BadRequest();

                }
                else
                {
                    exsit.SubAccountName = t.edited.AccountName;
                    exsit.CurrentWorkerId = uid;
                    db.Entry(exsit).State = EntityState.Modified;

                }
            }
            else
            {
                return BadRequest();

            }


            try
            {
                await db.SaveChangesAsync();
                return Ok();

            }
            catch (Exception)
            {

                throw;
            }
            

        }



        [HttpPost]
        [Route("getAllAccounts")]
        public IHttpActionResult GetAllAccounts(GetAccountsForList id)
        {
           

            var caccount = (from ca in db.Accounts
                            where ca.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == id.companyId
                            select new AccountToSave()
                            {
                                AccountId = ca.AccountId,
                                AccountName = ca.AccountName
                            }).ToList();

            return Ok(caccount);
        }

        [Route("getCustomesAccounts")]
        [HttpPost]
        public async Task<IHttpActionResult> GetCustomesAccounts(GetAccountsForList getCusomAccounts)
        {
            var cc = this.AppCompanyUserManager.ConvertToId(getCusomAccounts.accountCatogeryId);
            CustomAccountCategory financialList = await db.CustomAccountCategories.FindAsync(cc);
            if (financialList == null)
            {
                return NotFound();
            }

            var caccount = (from acc in db.CustomAccounts
                            where acc.CustomAccountCategoryId == cc
                            select new AccountToSave()
                            {
                                AccountId = acc.AccountId,
                                AccountName = acc.Account.AccountName
                            }).ToList();
            return Ok(caccount);
        }

        [Route("saveCustomAccounts")]
        [HttpPost]
        public IHttpActionResult saveCustomAccounts(saveGetCusomAccounts CustomAccounts)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            
            try
            {
                string uid = RequestContext.Principal.Identity.GetUserId();
               
                List<CustomAccount> accountsList = new List<CustomAccount>();
                var customAcc = db.CustomAccountCategories.Find(CustomAccounts.CustomAccountCategoryId);
                if(customAcc != null)
                {
                    var accountTodelete = db.CustomAccounts.Where(a => a.CustomAccountCategoryId == CustomAccounts.CustomAccountCategoryId);
                    if(accountTodelete != null)
                    {
                        db.CustomAccounts.RemoveRange(accountTodelete);

                    }

                    List<CustomAccount> accountList = new List<CustomAccount>();
                    foreach(var ac in CustomAccounts.AccountsToSave)
                    {
                        var getaccount = db.Accounts.Find(ac.AccountId);
                        if(getaccount != null)
                        {
                           
                            CustomAccount addAccount = new CustomAccount()
                            {
                                AccountId = getaccount.AccountId,
                                CustomAccountCategoryId = customAcc.CustomAccountCategoryId,
                                CurrentWorkerId = uid
                            };
                            accountList.Add(addAccount);
                        }
                        else
                        {
                            return BadRequest();
                        }
                    }
                    db.CustomAccounts.AddRange(accountList);

                }
                else
                {
                    return BadRequest();
                }

                db.SaveChanges();
                var getlistId = customAcc.CustomBalanceSheetTypes.CustomBalanceSheet.FinancialList;
                //CustomAccounts.BranchId
                var returnedlist = (from c in this.AppCompanyUserManager.fListes(new isCompanyBranch {Id = CustomAccounts.BranchId ,  IsCompany = CustomAccounts.isCompany})
                                    where c.ListId == getlistId.FinancialListId + "F"
                                    select c.Listes).SingleOrDefault();

                return Ok(returnedlist);
            }


            catch 
            {

                throw;
            }

        }

        [ResponseType(typeof(FListes))]
        [Route("updateCustomBalanceSheet")]
        [HttpPost]
        public IHttpActionResult UpdateCustomBalanceSheet(FListes recivdeList)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            string uid = RequestContext.Principal.Identity.GetUserId();

            var listId = this.AppCompanyUserManager.ConvertToId(recivdeList.Id);
            List<CustomAccount> removeac = new List<CustomAccount>();
            List<CustomAccountCategory> removeacc = new List<CustomAccountCategory>();
            List<CustomBalanceSheetType> removebst = new List<CustomBalanceSheetType>();
            List<CustomBalanceSheet> removebs = new List<CustomBalanceSheet>();
            List<CustomAccount> linkedac = new List<CustomAccount>();
            List<CustomAccountCategory> linkedacc = new List<CustomAccountCategory>();
            List<CustomBalanceSheetType> linkedbst = new List<CustomBalanceSheetType>();
            List<CustomBalanceSheet> linkedbs = new List<CustomBalanceSheet>();

            if (recivdeList.Level == 1)
            {
                var fl = db.FinancialLists.Find(listId);

                if (fl != null)
                {
                    fl.FinancialListName = recivdeList.Name;
                    fl.CurrentWorkerId = uid;
                    db.Entry(fl).State = EntityState.Modified;

                }
            }
            if (recivdeList.Level == 2)
            {
                var bs = db.CustomBalanceSheets.Find(listId);
                if(bs != null)
                {

                    if (recivdeList.IsLinked != null && (bool)recivdeList.IsLinked)
                    {
                        var chartBs = db.BalanceSheets.Find(recivdeList.LinkedAccountId);
                        
                        var delbs = bs;
                        removebs.Add(delbs);
                        foreach (var cbst in delbs.CustomBalanceSheetTypes)
                        {
                            removebst.Add(cbst);
                            foreach (var cacc in cbst.CustomAccountCategories)
                            {
                                removeacc.Add(cacc);
                                foreach (var cac in cacc.CustomAccount)
                                {
                                    removeac.Add(cac);
                                }
                            }
                        }
                        
                        db.CustomAccounts.RemoveRange(removeac);
                        db.CustomAccountCategories.RemoveRange(removeacc);
                        db.CustomBalanceSheetTypes.RemoveRange(removebst);
                        db.CustomBalanceSheets.RemoveRange(removebs);
                        db.SaveChanges();

                        var addCbs = new CustomBalanceSheet() { BalanceSheetName = chartBs.BalanceSheetName, CurrentWorkerId = uid, FinancialListId = this.AppCompanyUserManager.ConvertToId(recivdeList.ParentId) , AccountTypeId = db.AccountTypes.SingleOrDefault(a => a.AccountTypeName == recivdeList.accountType).AccountTypeId};
                      
                        db.CustomBalanceSheets.Add(addCbs);
                        db.SaveChanges();
                        foreach (var bst in chartBs.BalanceSheetTypes)
                        {
                            CustomBalanceSheetType cbst = new CustomBalanceSheetType()
                            {
                                BalanceSheetTypeName = bst.BalanceSheetTypeName,
                                CustomBalanceSheetId = addCbs.CustomBalanceSheetId,
                                CurrentWorkerId = uid
                            };
                            db.CustomBalanceSheetTypes.Add(cbst);
                            db.SaveChanges();
                            foreach (var acc in bst.AccountCategories)
                            {
                                CustomAccountCategory cacc = new CustomAccountCategory()
                                {
                                    AccountCategoryName = acc.AccountCategoryName,
                                    CustomBalanceSheetTypeId = cbst.CustomBalanceSheetTypeId,
                                    CurrentWorkerId = uid
                                };
                                db.CustomAccountCategories.Add(cacc);
                                db.SaveChanges();

                                foreach (var ac in acc.Accounts)
                                {
                                    CustomAccount cac = new CustomAccount()
                                    {
                                        AccountId = ac.AccountId,
                                        CustomAccountCategoryId = cacc.CustomAccountCategoryId,
                                        CurrentWorkerId = uid
                                    };
                                    db.CustomAccounts.Add(cac);
                                    db.SaveChanges();
                                }
                            }
                        }
                        
                        db.SaveChanges();
                       

                    }
                    else
                    {
                        bs.BalanceSheetName = recivdeList.Name;
                        bs.CurrentWorkerId = uid;
                        bs.AccountTypeId = db.AccountTypes.SingleOrDefault(a => a.AccountTypeName == recivdeList.accountType).AccountTypeId;
                        db.Entry(bs).State = EntityState.Modified;
                    }
                   


                }
            }

            if (recivdeList.Level == 3)
            {
                var bst = db.CustomBalanceSheetTypes.Find(listId);
                if (bst != null)
                {
                    if (recivdeList.IsLinked != null && (bool)recivdeList.IsLinked)
                    {
                        var chartBst = db.BalanceSheetTypes.Find(recivdeList.LinkedAccountId);

                        var delbst = bst;
                        removebst.Add(delbst);
                            foreach (var cacc in bst.CustomAccountCategories)
                            {
                                removeacc.Add(cacc);
                                foreach (var cac in cacc.CustomAccount)
                                {
                                    removeac.Add(cac);
                                }
                            }
                        

                        db.CustomAccounts.RemoveRange(removeac);
                        db.CustomAccountCategories.RemoveRange(removeacc);
                        db.CustomBalanceSheetTypes.RemoveRange(removebst);
                        db.SaveChanges();


                        var addCbs = new CustomBalanceSheetType() { BalanceSheetTypeName = chartBst.BalanceSheetTypeName, CurrentWorkerId = uid, CustomBalanceSheetId = this.AppCompanyUserManager.ConvertToId(recivdeList.ParentId) };

                        db.CustomBalanceSheetTypes.Add(addCbs);
                        db.SaveChanges();
                        
                            foreach (var acc in chartBst.AccountCategories)
                            {
                                CustomAccountCategory cacc = new CustomAccountCategory()
                                {
                                    AccountCategoryName = acc.AccountCategoryName,
                                    CustomBalanceSheetTypeId = addCbs.CustomBalanceSheetTypeId,
                                    CurrentWorkerId = uid
                                };
                                db.CustomAccountCategories.Add(cacc);
                                db.SaveChanges();

                                foreach (var ac in acc.Accounts)
                                {
                                    CustomAccount cac = new CustomAccount()
                                    {
                                        AccountId = ac.AccountId,
                                        CustomAccountCategoryId = cacc.CustomAccountCategoryId,
                                        CurrentWorkerId = uid
                                    };
                                    db.CustomAccounts.Add(cac);
                                    db.SaveChanges();
                                }
                            }

                    }
                    else
                    {
                        bst.BalanceSheetTypeName = recivdeList.Name;
                        bst.CurrentWorkerId = uid;
                        db.Entry(bst).State = EntityState.Modified;
                    }
                   

                }
            }

            if (recivdeList.Level == 4)
            {
                var acc = db.CustomAccountCategories.Find(listId);
                if (acc != null)
                {
                    if (recivdeList.IsLinked != null && (bool)recivdeList.IsLinked)
                    {
                        var chartacc = db.AccountCategories.Find(recivdeList.LinkedAccountId);

                        var delacc = acc;
                        removeacc.Add(delacc);
                        
                            foreach (var cac in delacc.CustomAccount)
                            {
                                removeac.Add(cac);
                            }
                        db.CustomAccounts.RemoveRange(removeac);
                        db.CustomAccountCategories.RemoveRange(removeacc);

                        var addCbs = new CustomAccountCategory() { AccountCategoryName = chartacc.AccountCategoryName, CurrentWorkerId = uid, CustomBalanceSheetTypeId = this.AppCompanyUserManager.ConvertToId(recivdeList.ParentId) };

                        db.CustomAccountCategories.Add(addCbs);
                        db.SaveChanges();
                        
                            foreach (var ac in chartacc.Accounts)
                            {
                                CustomAccount cac = new CustomAccount()
                                {
                                    AccountId = ac.AccountId,
                                    CustomAccountCategoryId = addCbs.CustomAccountCategoryId,
                                    CurrentWorkerId = uid
                                };
                                db.CustomAccounts.Add(cac);
                                db.SaveChanges();
                            }
                        

                    }
                    else
                    {
                        acc.AccountCategoryName = recivdeList.Name;
                        acc.CurrentWorkerId = uid;
                        db.Entry(acc).State = EntityState.Modified;
                    }
                  
                }
            }
            db.SaveChanges();
           
            if (recivdeList.IsLinked != null && recivdeList.IsLinked.GetValueOrDefault())
            {
                var returnedlist = (from c in this.AppCompanyUserManager.fListes(new isCompanyBranch { Id = recivdeList.childern,IsCompany = recivdeList.IsCompany })
                                    where c.ListId == recivdeList.forLevel
                                    select c.Listes).SingleOrDefault();
                return Ok(returnedlist);
            }
            return Ok();
        }

       [ResponseType(typeof(FListes))]
        [Route("saveCustomBalanceSheet")]
        [HttpPost]
        public IHttpActionResult PostCustomBalanceSheet(FListes recivdeList)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            string uid = RequestContext.Principal.Identity.GetUserId();

            FListes returnList = new FListes();
            Int32 acId = this.AppCompanyUserManager.ConvertToId(recivdeList.ParentId);

            if (!recivdeList.IsCompany && recivdeList.Level == 0)
            {
                Branch branch = db.Branchs.Find(acId);
                acId = branch.CompanyId;
            }
            if (recivdeList.Level == 1)
            {
                
                var uniqeKey = "حـ / رئيسي مخصص " + this.AppCompanyUserManager.GetUniqueKey(4);
                do
                {
                    uniqeKey = "حـ / رئيسي مخصص " + this.AppCompanyUserManager.GetUniqueKey(4);
                } while (db.CustomBalanceSheets.Any(b => b.BalanceSheetName == uniqeKey && b.FinancialListId == acId));

                CustomBalanceSheet bs = new CustomBalanceSheet()
                {
                    BalanceSheetName= uniqeKey,
                    FinancialListId = this.AppCompanyUserManager.ConvertToId(recivdeList.ParentId),
                    CurrentWorkerId = uid
                };

                db.CustomBalanceSheets.Add(bs);
                try
                {
                    db.SaveChanges();

                }
                catch 
                {

                    
                }
                returnList.Id = bs.CustomBalanceSheetId + "Bs";
                returnList.Name = bs.BalanceSheetName;
                returnList.Show = true;
                returnList.Level = 2;
                returnList.AddTitle = "اضافة حساب عام";
                returnList.Addlink = "ربط مع حساب رئيسي";
                returnList.forLevel = bs.FinancialListId + "F";
                returnList.ParentId = bs.FinancialListId + "Bs";
                returnList.LevelName = "رئيسي";

            }
            if (recivdeList.Level == 2)
            {
                var uniqeKey = "حـ / عام مخصص" + this.AppCompanyUserManager.GetUniqueKey(4);
                do 
                {
                  uniqeKey = "حـ / عام مخصص" + this.AppCompanyUserManager.GetUniqueKey(4);
                } while(db.CustomBalanceSheetTypes.Any(b => b.BalanceSheetTypeName == uniqeKey && b.CustomBalanceSheetId == acId));

                CustomBalanceSheetType bst = new CustomBalanceSheetType()
                {
                    BalanceSheetTypeName = uniqeKey,
                    CustomBalanceSheetId = this.AppCompanyUserManager.ConvertToId(recivdeList.Id),
                    CurrentWorkerId = uid
                };

                db.CustomBalanceSheetTypes.Add(bst);
                try
                {
                    db.SaveChanges();

                }
                catch 
                {

                    
                }
                returnList.Id = bst.CustomBalanceSheetTypeId + "Bst";
                returnList.Name = bst.BalanceSheetTypeName;
                returnList.Show = recivdeList.Show;
                returnList.Level = 3;
                returnList.AddTitle = "اضافة حساب مساعد";
                returnList.Addlink = "ربط مع حساب عام";
                returnList.forLevel = recivdeList.forLevel;
                returnList.ParentId = bst.CustomBalanceSheetId + "Bs";
                returnList.LevelName = "عام";
                

            }
            if (recivdeList.Level == 3)
            {

               

                var uniqeKey = "حـ / مساعد مخصص " + this.AppCompanyUserManager.GetUniqueKey(4);
                do
                {
                    uniqeKey = "حـ / مساعد مخصص " + this.AppCompanyUserManager.GetUniqueKey(4);
                } while (db.CustomAccountCategories.Any(b => b.AccountCategoryName == uniqeKey && b.CustomBalanceSheetTypeId == acId));

                CustomAccountCategory acc = new CustomAccountCategory()
                {
                    AccountCategoryName =  uniqeKey,
                    CustomBalanceSheetTypeId = this.AppCompanyUserManager.ConvertToId(recivdeList.Id),
                    CurrentWorkerId = uid
                };

                db.CustomAccountCategories.Add(acc);
                try
                {
                    db.SaveChanges();

                }
                catch 
                {

                    
                }
                returnList.Id = acc.CustomAccountCategoryId + "Acc";
                returnList.Name = acc.AccountCategoryName;
                returnList.Show = recivdeList.Show;
                returnList.Level = 4;
                returnList.AddTitle = "اضافة حسابات فرعية";
                returnList.Addlink = "";
                returnList.forLevel = recivdeList.forLevel;
                returnList.ParentId = acc.CustomBalanceSheetTypeId + "Bst";
                returnList.LevelName = "مساعد";
            }

            if(recivdeList.Level == 0)
            {
                var uniqeKey = "قائمة جديدة " + this.AppCompanyUserManager.GetUniqueKey(4);
                do
                {
                    uniqeKey = "قائمة جديدة " + this.AppCompanyUserManager.GetUniqueKey(4);
                } while (db.FinancialLists.Any(b => b.FinancialListName == uniqeKey && b.CompanyId == acId));

                var newFL = new FinancialList()
                {
                    CompanyId = acId,
                    CurrentWorkerId = uid,
                    FinancialListName = uniqeKey
                };
                db.FinancialLists.Add(newFL);
                try
                {
                    db.SaveChanges();
                    Alllists list = new Alllists()
                    {
                        ListId = newFL.FinancialListId + "F",
                        ListName = newFL.FinancialListName,
                        Listes = new List<FListes>(),
                        forWidget= false

                    };
                    return Ok(list);
                }
                catch (Exception)
                {

                    throw;
                }
                
            }

            return Ok(returnList);

        }

        // POST: api/BalanceSheets
        [ResponseType(typeof(AccountsTable))]
        public IHttpActionResult PostBalanceSheet(AccountsTable t)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            string uid = RequestContext.Principal.Identity.GetUserId();
            Int32? id = t.Code;


            AccountsTable returndAccount = new AccountsTable(); 
            if (t.AccountLevel == "")
            {
                Int32 count = this.AppCompanyUserManager.ConvertToId(t.Id);
                if (db.BalanceSheets.Count(a => a.CompanyId == count) >= 99)
                {
                    return BadRequest("الحد الاقصي للحسابات الرئيسية 99 حساب");
                }

                string name = "حـ / رئيسي - " + this.AppCompanyUserManager.GetUniqueKey(2);
                do
                {
                    name = "حـ / رئيسي - " + this.AppCompanyUserManager.GetUniqueKey(2);
                } while (db.BalanceSheets.Any(a => a.BalanceSheetName == name && a.CompanyId == count));

                BalanceSheet exsit = new BalanceSheet()
                    {
                        BalanceSheetName = name,
                       CompanyId = this.AppCompanyUserManager.ConvertToId(t.ParentId),

                    CurrentWorkerId = uid,
                        AccountTypeId = db.AccountTypes.Where(b => b.AccountTypeName == "مدين").SingleOrDefault().AccountTypeId

                    };
                    db.BalanceSheets.Add(exsit);
         
                try
                {
                    db.SaveChanges();
                    id = exsit.CompanyId;
                    this.AppCompanyUserManager.setAccountsCodes(exsit.CompanyId.GetValueOrDefault());

                }
                catch (Exception)
                {

                    throw;
                }
              
            }
            else if (t.AccountLevel == "رئيسي")
            {
                Int32 count = this.AppCompanyUserManager.ConvertToId(t.Id);
                if (db.BalanceSheetTypes.Count(a => a.BalanceSheetId == count) >= 99)
                {
                    return BadRequest("الحد الاقصي للحسابات العامة 99 حساب");
                }
                string name = "حـ / عام - " + this.AppCompanyUserManager.GetUniqueKey(3);
                do
                {
                    name = "حـ / عام - " + this.AppCompanyUserManager.GetUniqueKey(3);
                } while (db.BalanceSheetTypes.Any(a => a.BalanceSheetTypeName == name));

                var accType = db.BalanceSheets.Find(this.AppCompanyUserManager.ConvertToId(t.Id)).AccountType.AccountTypeName;
                BalanceSheetType exsit = new BalanceSheetType()
                {

                    BalanceSheetTypeName = name,
                    BalanceSheetId = this.AppCompanyUserManager.ConvertToId(t.Id),
                    CurrentWorkerId = uid,
                    AccountTypeId = db.AccountTypes.Where(a => a.AccountTypeName == accType).SingleOrDefault().AccountTypeId

                    };
                    db.BalanceSheetTypes.Add(exsit);
                try
                {
                    db.SaveChanges();
                    Int32 companyId = db.BalanceSheetTypes.Find(exsit.BalanceSheetTypeId).BalanceSheet.CompanyId.GetValueOrDefault();

                    this.AppCompanyUserManager.setAccountsCodes(companyId);


                }
                catch (Exception)
                {

                    throw;
                }
     
            }
            else if (t.AccountLevel == "عام")
            {
                Int32 count = this.AppCompanyUserManager.ConvertToId(t.Id);

                if (db.AccountCategories.Count(a => a.BalanceSheetTypeId == count) >= 1000)
                {
                    return BadRequest("الحد الاقصي للحسابات المساعدة 1000 حساب");
                }
                string name = "حـ / مساعد - " + this.AppCompanyUserManager.GetUniqueKey(4);
                do
                {
                    name = "حـ / مساعد - " + this.AppCompanyUserManager.GetUniqueKey(4);
                } while (db.AccountCategories.Any(a => a.AccountCategoryName == name));

                var accType = this.AppCompanyUserManager.GetBstType(this.AppCompanyUserManager.ConvertToId(t.Id));

                AccountCategory exsit = new AccountCategory()
                {
                        AccountCategoryName = name,
                        BalanceSheetTypeId = this.AppCompanyUserManager.ConvertToId(t.Id),
                        CurrentWorkerId = uid,
                        AccountTypeId = db.AccountTypes.Where(b => b.AccountTypeName == accType).SingleOrDefault().AccountTypeId,
                       AccountsDataTypesId = db.AccountsDataTypes.SingleOrDefault(a => a.TypeName == "اخري").Id

                };
                    db.AccountCategories.Add(exsit);
                try
                {
                    db.SaveChanges();
                    Int32 companyId = db.BalanceSheetTypes.Find(this.AppCompanyUserManager.ConvertToId(t.Id)).BalanceSheet.CompanyId.GetValueOrDefault();
                    this.AppCompanyUserManager.setAccountsCodes(companyId);


                }
                catch (Exception)
                {

                    throw;
                }

               
            }
            else if (t.AccountLevel == "مساعد")
            {
               
                Int32 count = this.AppCompanyUserManager.ConvertToId(t.Id);

                if (db.Accounts.Count(a => a.AccountCategoryId == count) >= 5000)
                {
                    return BadRequest("الحد الاقصي للحسابات الفرعية 5000 حساب");
                }
                string name = "حـ / فرعي - " + this.AppCompanyUserManager.GetUniqueKey(4);
                do
                {
                    name = "حـ / فرعي - " + this.AppCompanyUserManager.GetUniqueKey(4);
                } while (db.Accounts.Any(a => a.AccountName == name));

                var accType = this.AppCompanyUserManager.GetAccountCategoryType(this.AppCompanyUserManager.ConvertToId(t.Id));

                Account exsit = new Account()
                    {
                        AccountName = name,
                        AccountCategoryId = this.AppCompanyUserManager.ConvertToId(t.Id),
                        CurrentWorkerId = uid,
                        Activated = true,
                };
                    db.Accounts.Add(exsit);
                try
                {
                    db.SaveChanges();
                    Int32 companyId = db.AccountCategories.Find(this.AppCompanyUserManager.ConvertToId(t.Id)).BalanceSheetType.BalanceSheet.CompanyId.GetValueOrDefault();
                    this.AppCompanyUserManager.setAccountsCodes(companyId);

                }
                catch (Exception)
                {

                    throw;
                }
 
              
            }
            else if (t.AccountLevel == "فرعي")
            {

                Int32 count = this.AppCompanyUserManager.ConvertToId(t.Id);

                if (db.SubAccounts.Count(a => a.AccountId == count) >= 5000)
                {
                    return BadRequest("الحد الاقصي للحسابات الجزئية 5000 حساب");
                }
                string name = "حـ / جزئي - " + this.AppCompanyUserManager.GetUniqueKey(4);
                do
                {
                    name = "حـ / جزئي - " + this.AppCompanyUserManager.GetUniqueKey(4);
                } while (db.SubAccounts.Any(a => a.SubAccountName == name));

                var accType = this.AppCompanyUserManager.GetAccountCategoryType(this.AppCompanyUserManager.ConvertToId(t.Id));

                SubAccount exsit = new SubAccount()
                {
                    SubAccountName = name,
                    AccountId = this.AppCompanyUserManager.ConvertToId(t.Id),
                    CurrentWorkerId = uid,
                   
                    
                };
                db.SubAccounts.Add(exsit);
                try
                {
                    db.SaveChanges();
                    Int32 companyId = db.SubAccounts.Find(this.AppCompanyUserManager.ConvertToId(t.Id)).Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId.GetValueOrDefault();
                    this.AppCompanyUserManager.setAccountsCodes(companyId);
                    

                }
                catch (Exception)
                {

                    throw;
                }

            }
            else
            {
                return BadRequest();

            }

            var Tbs = (from bs in db.BalanceSheets
                       where bs.CompanyId == id
                       orderby bs.CreatedDate
                       select new AccountsTable
                       {
                           Id = bs.BalanceSheetId + "Bs",
                           AccountLevel = "رئيسي",
                           AccountName = bs.BalanceSheetName,
                           TitleForAdd = "اضافة حـ عام",
                           Code = db.BalanceSheets.Where(a => a.CompanyId == id).OrderBy(a => a.CreatedDate).Count(x => x.BalanceSheetId < bs.BalanceSheetId) + 1,
                           ParentId = bs.CompanyId.ToString(),
                           AccountType = bs.AccountType.AccountTypeName

                       }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


            var Tbst = (from bst in db.BalanceSheetTypes
                        where bst.BalanceSheet.CompanyId == id
                        orderby bst.CreatedDate
                        select new AccountsTable
                        {
                            Id = bst.BalanceSheetTypeId + "Bst",
                            AccountLevel = "عام",
                            AccountName = bst.BalanceSheetTypeName,
                            TitleForAdd = "اضافة حـ مساعد",
                            ParentId = bst.BalanceSheetId + "Bs",
                            AccountType = bst.AccountType.AccountTypeName,
                            Code = db.BalanceSheetTypes.Where(a => a.BalanceSheetId == bst.BalanceSheetId).OrderBy(a => a.CreatedDate).Count(x => x.BalanceSheetTypeId < bst.BalanceSheetTypeId) + 1,
                        }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


            List<AccountsTable> Taccounts = new List<AccountsTable>();


            var TAcc = (from Acc in db.AccountCategories
                        where Acc.BalanceSheetType.BalanceSheet.CompanyId == id
                        orderby Acc.CreatedDate
                        select new AccountsTable
                        {
                            Id = Acc.AccountCategoryId + "Acc",
                            AccountLevel = "مساعد",
                            AccountName = Acc.AccountCategoryName,
                            AccountType = Acc.AccountType.AccountTypeName,
                            TitleForAdd = "اضافة حـ فرعي",
                            Code = db.AccountCategories.Where(a => a.BalanceSheetTypeId == Acc.BalanceSheetTypeId).OrderBy(a => a.CreatedDate).Count(x => x.AccountCategoryId < Acc.AccountCategoryId) + 1,
                            ParentId = Acc.BalanceSheetTypeId + "Bst",
                            AccountsDataTypesId = (Int32)Acc.AccountsDataTypesId,
                            AccountsDataTypesName = Acc.AccountsDataTypes.TypeName
                        }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);




            var TAc = (from Ac in db.Accounts
                       where Ac.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == id
                       orderby Ac.CreatedDate
                       select new AccountsTable
                       {
                           Id = Ac.AccountId + "Ac",
                           AccountLevel = "فرعي",
                           TitleForAdd = "اضافة حـ جزئي",
                           AccountName = Ac.AccountName,
                           Code = db.Accounts.Where(a => a.AccountCategoryId == Ac.AccountCategoryId).OrderBy(a => a.CreatedDate).Count(x => x.AccountId < Ac.AccountId) + 1,
                           ParentId = Ac.AccountCategoryId + "Acc",
                          
                       }).GroupBy(u => u.Id).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


            var TSac = (from Ac in db.SubAccounts
                        where Ac.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == id
                        orderby Ac.CreatedDate
                        select new AccountsTable
                        {
                            Id = Ac.SubAccountId + "Sac",
                            AccountLevel = "جزئي",
                            AccountName = Ac.SubAccountName,
                            Code = db.SubAccounts.Where(a => a.AccountId == Ac.AccountId).OrderBy(a => a.CreatedDate).Count(x => x.SubAccountId < Ac.SubAccountId) + 1,
                            ParentId = Ac.AccountId + "Ac"
                        }).GroupBy(u => u.ParentId).Select(grp => grp.ToList()).ToList().OrderBy(b => b[0].Code);


            foreach (var acc in TAc)
            {
                foreach (var ac in TSac)
                {

                    if (ac[ac.Count - 1].ParentId == acc[0].Id)
                    {
                        ac.Where(a => a.ParentId == acc[0].Id).ToList().ForEach(c => c.fullCode = acc[0].Code.ToString() + " - " + c.Code.ToString());
                        acc.AddRange(ac);
                    }
                }
            }

            foreach (var acc in TAcc)
            {
                foreach (var ac in TAc)
                {

                    if (ac[0].ParentId == acc[0].Id)
                    {

                        acc.AddRange(ac);
                    }
                }
            }

            foreach (var bst in Tbst)
            {
                foreach (var acc in TAcc)
                {

                    if (acc[0].ParentId == bst[0].Id)
                    {
                        bst.AddRange(acc);
                    }
                }
            }

            foreach (var bs in Tbs)
            {
                foreach (var bst in Tbst)
                {

                    if (bst[0].ParentId == bs[0].Id)
                    {


                        bs.AddRange(bst);

                    }
                }
            }

            foreach (var i in Tbs)
            {
                foreach (var a in i)
                {
                    i.Where(w => w.ParentId == a.Id).ToList().ForEach(c => c.Code = int.Parse(a.Code.ToString() + c.Code.ToString()));

                }
            }
            Tbs.OrderBy(a => a[0].Code);
            
            return Ok(Tbs);


        }

        // DELETE: api/BalanceSheets/5
        [Route("delBalanceSheet")]
        [HttpPost]
        public IHttpActionResult delBalanceSheet(AccountsTable t)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(t.AccountLevel == "فرعي")
            {
                var id = this.AppCompanyUserManager.ConvertToId(t.Id);
                var acc = db.Accounts.Find(id);
                Int32 companyid = acc.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId.GetValueOrDefault();
                if (acc != null)
                {
                    db.Accounts.Remove(acc);
                    try
                    {
                       
                        db.SaveChanges();
                        
                        this.AppCompanyUserManager.setAccountsCodes(companyid);

                        return Ok();
                    }
                    catch 
                    {

                        return BadRequest("تاكد من ان الحساب غير مرتبط باي حركة او اتصل بالدعم");
                    }
                }
            }
            if (t.AccountLevel == "مساعد")
            {
                var id = this.AppCompanyUserManager.ConvertToId(t.Id);
                var acc = db.AccountCategories.Find(id);

                Int32 companyid = acc.BalanceSheetType.BalanceSheet.CompanyId.GetValueOrDefault();
                if (acc != null)
                {
                    if(acc.Accounts != null && acc.Accounts.Count() > 0)
                    {
                        db.Accounts.RemoveRange(acc.Accounts);
                    }
                  
                    db.AccountCategories.Remove(acc);
                    try
                    {
                        
                        db.SaveChanges();
                        this.AppCompanyUserManager.setAccountsCodes(companyid);
                        return Ok();
                    }
                    catch 
                    {

                        return BadRequest("تاكد من ان الحساب غير مرتبط باي حركة او اتصل بالدعم");
                    }
                }
            }

            if (t.AccountLevel == "عام")
            {
                var id = this.AppCompanyUserManager.ConvertToId(t.Id);
                var bst = db.BalanceSheetTypes.Find(id);
                Int32 companyid = bst.BalanceSheet.CompanyId.GetValueOrDefault();

                if (bst != null)
                {
                    if(bst.AccountCategories != null && bst.AccountCategories.Count() > 0)
                    {
                        foreach (var i in bst.AccountCategories)
                        {
                            db.Accounts.RemoveRange(i.Accounts);
                        }
                        db.AccountCategories.RemoveRange(bst.AccountCategories);
                    }
                  
                    db.BalanceSheetTypes.Remove(bst);
                    try
                    {
                       
                        db.SaveChanges();
                        this.AppCompanyUserManager.setAccountsCodes(companyid);
                        return Ok();
                    }
                    catch 
                    {

                        return BadRequest("تاكد من ان الحساب غير مرتبط باي حركة او اتصل بالدعم");
                    }
                }
            }


            if (t.AccountLevel == "رئيسي")
            {
                var id = this.AppCompanyUserManager.ConvertToId(t.Id);
                var bst = db.BalanceSheets.Where(a=> a.BalanceSheetId == id).SingleOrDefault();
                Int32 companyid = bst.CompanyId.GetValueOrDefault();

                if (bst != null)
                {

                    if (bst.BalanceSheetTypes != null && bst.BalanceSheetTypes.Count() > 0)
                    {
                        foreach (var x in bst.BalanceSheetTypes)
                        {
                            foreach (var i in x.AccountCategories)
                            {
                                db.Accounts.RemoveRange(i.Accounts);

                            }
                            db.AccountCategories.RemoveRange(x.AccountCategories);

                        }
                        db.BalanceSheetTypes.RemoveRange(bst.BalanceSheetTypes);
                    }
                   
                    db.BalanceSheets.Remove(bst);
                    try
                    {

                       
                        db.SaveChanges();
                        this.AppCompanyUserManager.setAccountsCodes(companyid);
                        return Ok();
                    }
                    catch 
                    {

                        return BadRequest("تاكد من ان الحساب غير مرتبط باي حركة او اتصل بالدعم");
                    }
                }
            }

            return Ok();

        }

        [Route("dBalanceSheetf")]
        [HttpPost]
        public IHttpActionResult DBalanceSheetf(FListes delList)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Int32 accId = this.AppCompanyUserManager.ConvertToId(delList.Id);
            List<CustomAccount> ac = new List<CustomAccount>();
            List<CustomAccountCategory> acc = new List<CustomAccountCategory>();
            List<CustomBalanceSheetType> bst = new List<CustomBalanceSheetType>();
            List<CustomBalanceSheet> bs = new List<CustomBalanceSheet>();
            FinancialList ftodel = new FinancialList();
            if (delList.Level == 1)
            {
                var delbs = db.FinancialLists.Find(accId);
                ftodel = delbs;
                foreach (var cbs in delbs.CustomBalanceSheets)
                {
                    bs.Add(cbs);
                    foreach (var cbst in cbs.CustomBalanceSheetTypes)
                    {
                        bst.Add(cbst);
                        foreach (var cacc in cbst.CustomAccountCategories)
                        {
                            acc.Add(cacc);
                            foreach (var cac in cacc.CustomAccount)
                            {
                                ac.Add(cac);
                            }
                        }
                    }
                }
               
             

            }

            if (delList.Level == 2)
            {
                var delbs = db.CustomBalanceSheets.Find(accId);
                bs.Add(delbs);
                foreach(var cbst in delbs.CustomBalanceSheetTypes)
                {
                    bst.Add(cbst);
                    foreach(var cacc in cbst.CustomAccountCategories)
                    {
                        acc.Add(cacc);
                        foreach(var cac in cacc.CustomAccount)
                        {
                            ac.Add(cac);
                        }
                    }
                }
             
            }

            if (delList.Level == 3)
            {
                var del = db.CustomBalanceSheetTypes.Find(accId);
                bst.Add(del);
                foreach (var cacc in del.CustomAccountCategories)
                {
                    acc.Add(cacc);
                    foreach (var cac in cacc.CustomAccount)
                    {
                        ac.Add(cac);
                    }
                }
            }


            if (delList.Level == 4)
            {
                var del = db.CustomAccountCategories.Find(accId);
                acc.Add(del);
                foreach (var cac in del.CustomAccount)
                {
                    ac.Add(cac);
                }
            }

            if (delList.Level == 5)
            {
                var del = db.CustomAccounts.Find(accId);
                ac.Add(del);

            }

            db.CustomAccounts.RemoveRange(ac);
            db.CustomAccountCategories.RemoveRange(acc);
            db.CustomBalanceSheetTypes.RemoveRange(bst);
            db.CustomBalanceSheets.RemoveRange(bs);
            if(ftodel.FinancialListId != 0)
               db.FinancialLists.Remove(ftodel);

            try
            {
                db.SaveChanges();
            }
            catch (Exception)
            {

                throw;
            }

            var returnedlist = (from c in this.AppCompanyUserManager.fListes(new isCompanyBranch {Id = delList.childern , IsCompany = delList.IsCompany })
                               where c.ListId == delList.forLevel
                               select c.Listes).SingleOrDefault();
           
            return Ok(returnedlist);

        }


        [Route("removeAccProperty/{id:int}")]
        [HttpGet]
        [ResponseType(typeof(List<AccountCategoryPropertiesVM>))]

        public async Task<IHttpActionResult> removeAccountCatogeryProp(int id)
        {
            AccountCategoryProperties accountCategoryProperties = db.AccountCategoryProperties.Find(id);
            if (accountCategoryProperties == null)
            {
                return NotFound();
            }

            db.AccountCategoryProperties.Remove(accountCategoryProperties);
            await db.SaveChangesAsync();
            //var prop = this.AppCompanyUserManager.GetAccountCatogeryProperties(accountCategoryProperties.AccountCategoryId);
            return Ok();//prop);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BalanceSheetExists(int id)
        {
            return db.BalanceSheets.Count(e => e.BalanceSheetId == id) > 0;
        }
    }
}