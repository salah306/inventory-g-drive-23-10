﻿using Inventory.DataLayer;
using Inventory.Managers;
using Inventory.Models;
using Inventory.Utility;
using Inventory.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Inventory.Controllers
{
    [RoutePrefix("api/accountProperty")]
    public class AccountPropertyController : BaseApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private AccountPropertyManager AccountPropertyManager;
        public AccountPropertyController()
        {
            this.AccountPropertyManager = new AccountPropertyManager(this.db);
        }

        [Route("GetaccountCategoryProperties")]
        [HttpPost]
        public IHttpActionResult GetaccountCategoryProperties(accountPropetyCompanyBranch or)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();

            Int32 companyId = 0;
            if (or.isCompany)
            {
                companyId = or.CompanyId;
            }
            else
            {
                companyId = db.Branchs.Find(or.CompanyId).CompanyId;
            }
            var accType = "";
            switch (or.accountType)
            {
                case "pur":
                    accType = "موردين";
                    break;
                case "cust":
                    accType = "عملاء";
                    break;
                case "bank":
                    accType = "بنوك";
                    break;
                case "inventory":
                    accType = "مخازن";
                    break;
                case "cash":
                    accType = "نقود";
                    break;
                case "notereceive":
                    accType = "اوراق قبض";
                    break;
                case "notepay":
                    accType = "اوراق دفع";
                    break;
                case "other":
                    accType = "";
                    break;
                default:
                    accType = "";
                    break;


            }

            var waitingOrder = db.OrderMoveRequest.Where(a => a.CompanyId == companyId && a.BranchId == (or.isCompany ? (int?)null : or.CompanyId) && a.isToCancel == false && a.isDone == false && a.AccountsDataTypes.TypeName == accType).Count();
            var waitingRequest = db.orderRequest.Where(a => a.CompanyId == companyId && a.BranchId == (or.isCompany ? (int?)null : or.CompanyId) && a.isToCancel == false && a.isDone == false && a.AccountsDataTypes.TypeName == accType).Count();
            var notfy = 0;
            if (or.isCompany)
            {
               
                var acc = db.Accounts
                    .Where(a => a.AccountCategory.AccountsDataTypes.TypeName ==accType && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == or.CompanyId && a.BranchId ==(int?)null && a.Activated == true)
                    .Select(ac => new accountsName() {
                        AccountId = ac.AccountId,
                        AccountName = ac.AccountName,
                        code = ac.Code,
                        AccountCatogeryId = ac.AccountCategoryId
                    }).ToList();
                var props = db.AccountCategoryProperties.Where(a => a.AccountsDataTypes.TypeName == accType && a.CompanyId == or.CompanyId).Select(pr => new accountCategoryPropertiesVm()
                {
                    accountCategoryPropertiesId = pr.AccountCategoryPropertiesId , 
                    accountCategoryPropertiesName = pr.AccountCategoryPropertiesName,
                    typeId = pr.AccountsDataTypesId , 
                    typeName = pr.AccountsCatogeryTypesAcccounts.TypeName
                }).ToList();
                var types = db.AccountsCatogeryTypesAcccounts.Select(a => new accCatogeryTypesAccountVm()
                {
                    typeId = a.AccountsCatogeryTypesAcccountsId,
                    typeName = a.TypeName
                }).ToList();
                ReturnedAccountProperty accprop = new ReturnedAccountProperty()
                {
                    accountsName = acc,
                    properties = props,
                    types = types,
                    waitingOrder = waitingOrder,
                    waitingRequest = waitingRequest,
                    notfy = notfy
                };
                return Ok(accprop);

            }
            else
            {
               
                var acc = db.Accounts
                    .Where(a => a.AccountCategory.AccountsDataTypes.TypeName == accType && a.BranchId == or.CompanyId && a.Activated == true)
                     .Select(ac => new accountsName()
                      {
                          AccountId = ac.AccountId,
                          AccountName = ac.AccountName,
                          code = ac.Code,
                          AccountCatogeryId = ac.AccountCategoryId
                      }).ToList();
                var props = db.AccountCategoryProperties.Where(a => a.AccountsDataTypes.TypeName == accType && a.CompanyId == companyId).Select(pr => new accountCategoryPropertiesVm()
                {
                    accountCategoryPropertiesId = pr.AccountCategoryPropertiesId,
                    accountCategoryPropertiesName = pr.AccountCategoryPropertiesName,
                    typeId = pr.AccountsDataTypesId,
                    typeName = pr.AccountsCatogeryTypesAcccounts.TypeName
                }).ToList();
                var types = db.AccountsCatogeryTypesAcccounts.Select(a => new accCatogeryTypesAccountVm()
                {
                    typeId = a.AccountsCatogeryTypesAcccountsId , 
                    typeName = a.TypeName
                }).ToList();
                ReturnedAccountProperty accprop = new ReturnedAccountProperty()
                {
                    accountsName = acc,
                    properties = props,
                    types = types,
                    waitingOrder = waitingOrder,
                    waitingRequest = waitingRequest,
                    notfy = notfy
                };
                return Ok(accprop);
            }
           
        }

        [Route("addNewMainacc")]
        [HttpPost]
        public IHttpActionResult addNewMainacc(mainAccName accName)
        {
            
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
          
            var accType = "";
            switch (accName.accountType)
            {
                case "pur":
                    accType = "موردين";
                    break;
                case "cust":
                    accType = "عملاء";
                    break;
                case "bank":
                    accType = "بنوك";
                    break;
                case "inventory":
                    accType = "مخازن";
                    break;
                case "cash":
                    accType = "نقود";
                    break;
                case "notereceive":
                    accType = "اوراق قبض";
                    break;
                case "notepay":
                    accType = "اوراق دفع";
                    break;
                case "other":
                    accType = "";
                    break;
                default:
                    accType = "";
                    break;


            }

            var accCatogery = db.AccountCategories.SingleOrDefault(a => a.AccountsDataTypes.TypeName == accType && a.BalanceSheetType.BalanceSheet.CompanyId == company.companyId);
            if(accCatogery == null)
            {
                return BadRequest("لا يمكن اضافة حساب في الوقت الحالي تاكد من وجود تصنيف للخساب في دليل الحسابات او انك تملك الصلاحيات الكافية ");

            }
            if (accName.isNew)
            {
                var isexistes = accCatogery.Accounts.Any(a => a.AccountName == accName.accountName && a.BranchId == (company.isCompany ? (int?)null : company.oId) 
                && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId);
                if (isexistes)
                {
                    return BadRequest("يوجد حساب مسجل بنفس الاسم - تم الغاء الحفظ");

                }
                if (string.IsNullOrEmpty(accName.accountName))
                {
                    return BadRequest("يجب تحديد اسم للحساب الذي تريد اضافته - تم الغاء الحفظ");

                }
                Account acc = new Account()
                {
                    AccountCategoryId = accCatogery.AccountCategoryId,
                    AccountName = accName.accountName,
                    BranchId = company.isCompany ? (int?)null : company.oId,
                    Activated = true,
                    CurrentWorkerId = uid,
                    Code = accCatogery.Code + (accCatogery.Accounts.Count() + 1).ToString()
                };
                db.Accounts.Add(acc);
                db.SaveChanges();
                var returned = new { accountId = acc.AccountId, accountName = acc.AccountName, accountCatogeryId = acc.AccountCategoryId, code = acc.Code };
                return Ok(returned);
            }

            if (!accName.isNew)
            {
                if (string.IsNullOrEmpty(accName.accountName))
                {
                    return BadRequest("يجب تحديد اسم للحساب الذي تريد اضافته - تم الغاء الحفظ");

                }
                var isexistes = accCatogery.Accounts.Any(a => a.AccountName == accName.accountName && a.BranchId == (company.isCompany ? (int?)null : company.oId)
                  && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId && a.AccountId != accName.accountId);
                if (isexistes)
                {
                    return BadRequest("يوجد حساب مسجل بنفس الاسم الذي تريد تحديثة - تم الغاء التحديث");

                }
                var findacc = accCatogery.Accounts.SingleOrDefault(a => a.AccountId == accName.accountId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && 
                a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId);
                if (findacc == null)
                {
                    return BadRequest("الحساب الذي تحاول غير مسجل - تأكد من امتلاك الصلاحيات الكافية - تم الغاء التحديث");

                }
                findacc.AccountName = accName.accountName;
                findacc.Activated = accName.active;
                db.Entry(findacc).State = EntityState.Modified;
                db.SaveChanges();
                // { accountId: 161, accountName: "عميل 1", accountCatogeryId: 135, code: "1511"}
                var returned = new { accountId = findacc.AccountId, accountName = findacc.AccountName, accountCatogeryId = findacc.AccountCategoryId, code = findacc.Code };
                return Ok(returned);
            }
            return BadRequest("لم نتمكن من تحديد خيارات الحفظ والتحديث تاكد من امتلاك الصلاحيات الكافية ");
          
        }

        [Route("getwaiting")]
        [HttpPost]
        public IHttpActionResult getWaiting(accountPropetywaiting wa)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var combanyBranch = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            Int32 companyId = 0;
            if (combanyBranch.isCompany)
            {
                companyId = combanyBranch.companyId;
            }
            else
            {
                companyId = db.Branchs.Find(combanyBranch.companyId).CompanyId;
            }
            var accType = "";
            switch (wa.accountType)
            {
                case "pur":
                    accType = "موردين";
                    break;
                case "cust":
                    accType = "عملاء";
                    break;
                case "bank":
                    accType = "بنوك";
                    break;
                case "inventory":
                    accType = "مخازن";
                    break;
                case "cash":
                    accType = "نقود";
                    break;
                case "notereceive":
                    accType = "اوراق قبض";
                    break;
                case "notepay":
                    accType = "اوراق دفع";
                    break;
                case "other":
                    accType = "";
                    break;
                default:
                    accType = "";
                    break;


            }

            OrdersWaitingVm orders = new OrdersWaitingVm();
            if (wa.waitType == "waitingRequest")
            {
                var waitingRequest = db.orderRequest.Where(a => a.CompanyId == companyId && a.BranchId == (combanyBranch.isCompany ? (int?)null : combanyBranch.companyId) && a.isToCancel == false && a.isDone == false && a.AccountsDataTypes.TypeName == accType);
                orders = new OrdersWaitingVm
                {
                    type = "request",
                    orders = waitingRequest.Select(a => new OrderWaitingVm()
                    {
                        id = a.OrderRequestId,
                        date = a.requestDate,
                        orderNo = a.requestNo,
                        type = a.typeName == "pay" ? "طلب صرف" : "طلب توريد",
                        amount = a.items.Select(i => i.qty * i.price).DefaultIfEmpty(0).Sum(),
                        user = a.CurrentWorker != null ?  a.CurrentWorker.FirstName+ " " + a.CurrentWorker.LastName : "",
                        typeName = a.typeName
                    }).ToList()
                };
            }
            if (wa.waitType == "waitingOrder")
            {
                var waitingOrder = db.OrderMoveRequest.Where(a => a.CompanyId == companyId && a.BranchId == (combanyBranch.isCompany ? (int?)null : combanyBranch.companyId) && a.isToCancel == false && a.isDone == false && a.AccountsDataTypes.TypeName == accType); ;
               
                if (waitingOrder != null)
                {
                    orders = new OrdersWaitingVm
                    {
                        type = "order",
                        orders = waitingOrder.Select(a => new OrderWaitingVm()
                        {
                            id = a.OrderMoveRequestId,
                            date = a.OrderDate,
                            orderNo = a.OrderNo,
                            type = a.typeName == "pay" ? "اذن اضافة" : "اذن صرف",
                            amount = a.amount,
                            user = a.CurrentWorker != null ? string.Format("{0} {1}", a.CurrentWorker.FirstName, a.CurrentWorker.LastName) : "",
                            typeName = a.typeName,
                        }).ToList()
                    };
                }
            }
            if (wa.waitType == "newNotfy")
            {
               // var notfy = 0;
                //var waitingOrder = db.OrderMoveRequest.Where(a => a.CompanyId == companyId && a.BranchId == (combanyBranch.isCompany ? (int?)null : combanyBranch.companyId) && a.isToCancel == false && a.isDone == false && a.AccountsDataTypes.TypeName == accType); ;

            }

            return Ok(orders);

        }
        [Route("AddaccountCategoryPropery")]
        [HttpPost]
        public IHttpActionResult AddaccountCategoryPropery(AddaccountPropetyCompanyBranch company)
        {
            var accType = "";
            string uid = RequestContext.Principal.Identity.GetUserId();
            switch (company.accountType)
            {
                case "pur":
                    accType = "موردين";
                    break;
                case "cust":
                    accType = "عملاء";
                    break;
                case "bank":
                    accType = "بنوك";
                    break;
                case "inventory":
                    accType = "مخازن";
                    break;
                case "cash":
                    accType = "نقود";
                    break;
                case "notereceive":
                    accType = "اوراق قبض";
                    break;
                case "notepay":
                    accType = "اوراق دفع";
                    break;
                case "other":
                    accType = "";
                    break;
                default:
                    accType = "";
                    break;


            }

            var propertyExistes = db.AccountCategoryProperties.Find(company.accountCategoryPropertiesId);
            var accDataType = db.AccountsDataTypes.SingleOrDefault(d => d.TypeName == accType);
            if(accDataType == null)
            {
                return BadRequest("لا يوجد نوع حساب مسجل بهذا الاسم");
            }

            Int32 companyId = 0;
            if (company.isCompany)
            {
                companyId = company.CompanyId;
            }
            else
            {
                companyId = db.Branchs.Find(company.CompanyId).CompanyId;
            }

            if (propertyExistes == null)
            {
                propertyExistes = new AccountCategoryProperties()
                {
                    AccountCategoryPropertiesName = "خاصية جديدة",
                    AccountsDataTypesId = accDataType.Id,
                    AccountsTypesAcccountsId = db.AccountsCatogeryTypesAcccounts.SingleOrDefault(a => a.TypeName == "ملاحظات").AccountsCatogeryTypesAcccountsId,
                    CurrentWorkerId = uid ,
                    CompanyId = company.CompanyId
                };
                db.AccountCategoryProperties.Add(propertyExistes);
                db.SaveChanges();
                accountCategoryPropertiesVm newProp = new accountCategoryPropertiesVm()
                {
                    accountCategoryPropertiesId = propertyExistes.AccountCategoryPropertiesId,
                    accountCategoryPropertiesName = propertyExistes.AccountCategoryPropertiesName,
                    typeId = propertyExistes.AccountsTypesAcccountsId,
                    typeName = propertyExistes.AccountsCatogeryTypesAcccounts.TypeName 
                };
                return Ok(newProp);
            }
            else
            {
                var propType = db.AccountsCatogeryTypesAcccounts.SingleOrDefault(a => a.TypeName == company.typeName);
                if (propType == null)
                {
                    
                    return BadRequest("لا يوجد نوع بيانات متوفر بهذا الاسم ");
                }
                propertyExistes.AccountCategoryPropertiesName = company.accountCategoryPropertiesName;
                propertyExistes.AccountsTypesAcccountsId = propType.AccountsCatogeryTypesAcccountsId;
                propertyExistes.CurrentWorkerId = uid;
                db.Entry(propertyExistes).State = EntityState.Modified;
                db.SaveChanges();
                accountCategoryPropertiesVm newProp = new accountCategoryPropertiesVm()
                {
                    accountCategoryPropertiesId = propertyExistes.AccountCategoryPropertiesId,
                    accountCategoryPropertiesName = propertyExistes.AccountCategoryPropertiesName,
                    typeId = propertyExistes.AccountsTypesAcccountsId,
                    typeName = propertyExistes.AccountsCatogeryTypesAcccounts.TypeName
                };
                return Ok(newProp);
            }


        }

        [Route("DelaccountCategoryPropery")]
        [HttpPost]
        public IHttpActionResult DelaccountCategoryPropery(AddaccountPropetyCompanyBranch company)
        {
            var accType = "";
            string uid = RequestContext.Principal.Identity.GetUserId();

            switch (company.accountType)
            {
                case "pur":
                    accType = "موردين";
                    break;
                case "cust":
                    accType = "عملاء";
                    break;
                case "bank":
                    accType = "بنوك";
                    break;
                case "inventory":
                    accType = "مخازن";
                    break;
                case "cash":
                    accType = "نقود";
                    break;
                case "notereceive":
                    accType = "اوراق قبض";
                    break;
                case "notepay":
                    accType = "اوراق دفع";
                    break;
                case "other":
                    accType = "";
                    break;
                default:
                    accType = "";
                    break;


            }
            Int32 companyId = 0;
            if (company.isCompany)
            {
                companyId = company.CompanyId;
            }
            else
            {
                companyId = db.Branchs.Find(company.CompanyId).CompanyId;
            }
            var findprop = db.AccountCategoryProperties.SingleOrDefault(a => a.AccountCategoryPropertiesId == company.accountCategoryPropertiesId && a.CompanyId == companyId && a.AccountsDataTypes.TypeName == accType);
            if(findprop != null)
            {
                db.AccountCategoryProperties.Remove(findprop);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest("خاصية غير متاحة للحذف");
            }

        }


        [Route("addPropertyValue")]
        [HttpPost]
        public IHttpActionResult addPropertyValue(AddaccountPropetyValueCompanyBranch company)
        {
            var accType = "";
            string uid = RequestContext.Principal.Identity.GetUserId();

            switch (company.accountType)
            {
                case "pur":
                    accType = "موردين";
                    break;
                case "cust":
                    accType = "عملاء";
                    break;
                case "bank":
                    accType = "بنوك";
                    break;
                case "inventory":
                    accType = "مخازن";
                    break;
                case "cash":
                    accType = "نقود";
                    break;
                case "notereceive":
                    accType = "اوراق قبض";
                    break;
                case "notepay":
                    accType = "اوراق دفع";
                    break;
                case "other":
                    accType = "";
                    break;
                default:
                    accType = "";
                    break;


            }

            Int32 companyId = 0;
            if (company.isCompany)
            {
                companyId = company.CompanyId;
            }
            else
            {
                companyId = db.Branchs.Find(company.CompanyId).CompanyId;
            }

            var pEX = db.AccountCategoryProperties.SingleOrDefault(a => a.CompanyId == companyId && a.AccountCategoryPropertiesId == company.accountCategoryPropertiesId);

            if (pEX != null)
            {
                var findValue = db.AccountCategoryPropertiesValues.SingleOrDefault(a => a.AccountCategoryPropertiesId == company.accountCategoryPropertiesId && a.AccountId == company.accountId && a.Account.AccountCategory.AccountsDataTypes.TypeName == accType && a.AccountCategoryPropertiesValueId == company.valueId);
                if(findValue == null)
                {
                    var tvalue = "";
                        switch (pEX.AccountsCatogeryTypesAcccounts.TypeName)
                        {
                            case "عدد طبيعي":
                                tvalue = "0000000";
                                break;
                            case "عدد عشري":
                                tvalue = "0.00";
                                break;
                            case "بريد الكتروني":
                                tvalue = "example@company.com";
                                break;
                            case "هاتف محمول":
                                tvalue = "01111111";
                                break;
                            case "هاتف ارضي":
                                tvalue = "02222222";
                                break;
                            case "ملاحظات":
                                 tvalue = "يمكنك كتابة الملاحظات هنا";
                                break;
                            case "عنوان":
                                tvalue = "يمكنك كتابة العنوان هنا";
                                break;
                            default:
                               tvalue = "00";
                                break;
                        }
                    findValue = new AccountCategoryPropertiesValue()
                    {
                        AccountId = company.accountId,
                        AccountCategoryPropertiesId = company.accountCategoryPropertiesId,
                        AccountCategoryPropertiesValueName =tvalue,
                        CurrentWorkerId = uid,

                    };
                    db.AccountCategoryPropertiesValues.Add(findValue);
                    db.SaveChanges();
                    accountPropertiesValuesVm returnvalue = new accountPropertiesValuesVm()
                    {
                        valueId = findValue.AccountCategoryPropertiesValueId,
                        valueName = findValue.AccountCategoryPropertiesValueName,
                        accountCategoryPropertiesId = findValue.AccountCategoryPropertiesId
                    };
                    return Ok(returnvalue);
                }
                else
                {

                    var tvalue = false;
                    switch (pEX.AccountsCatogeryTypesAcccounts.TypeName)
                    {
                        case "عدد طبيعي":
                            tvalue = this.AppCompanyUserManager.CheckDataType(company.value , pEX.AccountsCatogeryTypesAcccounts.TypeName);
                            break;
                        case "عدد عشري":
                            tvalue = this.AppCompanyUserManager.CheckDataType(company.value, pEX.AccountsCatogeryTypesAcccounts.TypeName);
                            break;
                        case "بريد الكتروني":
                            tvalue = this.AppCompanyUserManager.CheckDataType(company.value, pEX.AccountsCatogeryTypesAcccounts.TypeName);
                            if (db.AccountCategoryPropertiesValues.Any(a => a.AccountId == company.accountId && a.AccountCategoryProperties.AccountsCatogeryTypesAcccounts.TypeName == "بريد الكتروني" && a.AccountCategoryPropertiesValueName == company.value))
                            {
                                tvalue = false;
                            }
                            break;
                        case "هاتف محمول":

                            tvalue = this.AppCompanyUserManager.CheckDataType(company.value, pEX.AccountsCatogeryTypesAcccounts.TypeName);
                            if(db.AccountCategoryPropertiesValues.Any(a => a.AccountId == company.accountId && a.AccountCategoryProperties.AccountsCatogeryTypesAcccounts.TypeName == "هاتف محمول" && a.AccountCategoryPropertiesValueName == company.value))
                            {
                                tvalue = false;
                            }
                            break;
                        case "هاتف ارضي":
                            tvalue = this.AppCompanyUserManager.CheckDataType(company.value, pEX.AccountsCatogeryTypesAcccounts.TypeName);
                            if (db.AccountCategoryPropertiesValues.Any(a => a.AccountId == company.accountId && a.AccountCategoryProperties.AccountsCatogeryTypesAcccounts.TypeName == "هاتف ارضي" && a.AccountCategoryPropertiesValueName == company.value))
                            {
                                tvalue = false;
                            }
                            break;
                        case "ملاحظات":
                            tvalue = true;
                            break;
                        case "عنوان":
                            tvalue = true;
                            break;
                        default:
                            tvalue = false;
                            break;
                    }
                    if (!tvalue)
                    {
                        return BadRequest("لا يمكن تسجيل هذه القيمة");
                    }
                    findValue.AccountCategoryPropertiesValueName = company.value;
                    findValue.CurrentWorkerId = uid;
                    db.Entry(findValue).State = EntityState.Modified;
                    db.SaveChanges();
                    accountPropertiesValuesVm returnvalue = new accountPropertiesValuesVm()
                    {
                        valueId = findValue.AccountCategoryPropertiesValueId,
                        valueName = findValue.AccountCategoryPropertiesValueName,
                        accountCategoryPropertiesId = findValue.AccountCategoryPropertiesId
                    };
                    return Ok(returnvalue);
                }
            }
            else
            {
                return BadRequest("لا يوجد خاصية مسجلة بعذا الاسم");
            }
        }

        [Route("GetAccountInfo")]
        [HttpPost]
        public IHttpActionResult GetAccountInfo(getAccountInfoAndProp company)
        {
            //accountInfoToreturn
            var accType = "";
            string uid = RequestContext.Principal.Identity.GetUserId();
            var acc = db.Accounts.Find(company.accountId);
            
            switch (company.accountType)
            {
                case "pur":
                    accType = "موردين";
                    break;
                case "cust":
                    accType = "عملاء";
                    break;
                case "bank":
                    accType = "بنوك";
                    break;
                case "inventory":
                    accType = "مخازن";
                    break;
                case "cash":
                    accType = "نقود";
                    break;
                case "notereceive":
                    accType = "اوراق قبض";
                    break;
                case "notepay":
                    accType = "اوراق دفع";
                    break;
                case "other":
                    accType = "";
                    break;
                default:
                    accType = "";
                    break;


            }

            var fc = new Company();
            if (company.isCompany)
            {
                fc = db.Companies.Find(company.CompanyId);
            }
            else
            {
                fc = db.Branchs.Find(company.CompanyId).Company;
            }

            var findacc = db.Accounts.SingleOrDefault(a => a.AccountId == company.accountId && a.BranchId == (company.isCompany ? (int?)null : company.CompanyId) && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == fc.CompanyId && a.AccountCategory.AccountsDataTypes.TypeName == accType);
            if(findacc != null)
            {
                var propvalues = findacc.AccountCategoryPropertiesValues.Select(a => new accountPropertiesValuesVm()
                {
                    valueId = a.AccountCategoryPropertiesValueId,
                    valueName = a.AccountCategoryPropertiesValueName,
                    accountCategoryPropertiesId = a.AccountCategoryPropertiesId
                }).ToList();
                accountInfoToreturn toreturn = new accountInfoToreturn()
                {
                    accountValues = propvalues
                };
                return Ok(toreturn);
            }
            else
            {
                return BadRequest();
            }
         

        }


        [Route("DelPropertyValue")]
        [HttpPost]
        public IHttpActionResult delPropertyValue(AddaccountPropetyValueCompanyBranch company)
        {
            var accType = "";
            string uid = RequestContext.Principal.Identity.GetUserId();

            switch (company.accountType)
            {
                case "pur":
                    accType = "موردين";
                    break;
                case "cust":
                    accType = "عملاء";
                    break;
                case "bank":
                    accType = "بنوك";
                    break;
                case "inventory":
                    accType = "مخازن";
                    break;
                case "cash":
                    accType = "نقود";
                    break;
                case "notereceive":
                    accType = "اوراق قبض";
                    break;
                case "notepay":
                    accType = "اوراق دفع";
                    break;
                case "other":
                    accType = "";
                    break;
                default:
                    accType = "";
                    break;


            }

            Int32 companyId = 0;
            if (company.isCompany)
            {
                companyId = company.CompanyId;
            }
            else
            {
                companyId = db.Branchs.Find(company.CompanyId).CompanyId;
            }

            var valueex = db.AccountCategoryPropertiesValues.SingleOrDefault(a => a.AccountCategoryPropertiesId == company.accountCategoryPropertiesId && a.AccountCategoryPropertiesValueId == company.valueId && a.AccountId == company.accountId && a.Account.AccountCategory.AccountsDataTypes.TypeName == accType);
            if(valueex != null)
            {
                db.AccountCategoryPropertiesValues.Remove(valueex);
                db.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest("قيمة غير متاحة للحذف");
            }
        }


        [Route("GetAccountMovementsDates")]
        [HttpPost]
        public IHttpActionResult Get(getAccountMovementsproperty or)
        {
            var accType = "";
            string uid = RequestContext.Principal.Identity.GetUserId();

            switch (or.accountType)
            {
                case "pur":
                    accType = "موردين";
                    break;
                case "cust":
                    accType = "عملاء";
                    break;
                case "bank":
                    accType = "بنوك";
                    break;
                case "inventory":
                    accType = "مخازن";
                    break;
                case "cash":
                    accType = "نقود";
                    break;
                case "notereceive":
                    accType = "اوراق قبض";
                    break;
                case "notepay":
                    accType = "اوراق دفع";
                    break;
                case "other":
                    accType = "";
                    break;
                default:
                    accType = "";
                    break;


            }

            Int32 companyId = 0;
            if (or.IsCompany)
            {
                companyId = or.companyId;
            }
            else
            {
                companyId = db.Branchs.Find(or.companyId).CompanyId;
            }
            DateTime sD = Convert.ToDateTime(or.startDate);
            DateTime eD = Convert.ToDateTime(or.endDate);
            //GetAccountMovementsCatogeryDates
            if (or.search == "account")
            {
                var checkAccount = db.Accounts.SingleOrDefault(a => a.AccountId == or.id && a.AccountCategory.AccountsDataTypes.TypeName == accType && a.BranchId == (or.IsCompany ? (int?)null : or.companyId) && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId);
                if(checkAccount == null)
                {
                    return BadRequest();
                }
                #region accountMoveReport
                var firstBalnce = checkAccount.AccountsMovements.Where(a => a.AccountMovementDate < sD).Select(s => s.Debit - s.Crdit).DefaultIfEmpty(0).Sum();
                var currentbalance = checkAccount.AccountsMovements.Select(s => s.Debit - s.Crdit).DefaultIfEmpty(0).Sum();
                var periodbalance = checkAccount.AccountsMovements.Where(a => a.AccountMovementDate >= sD && a.AccountMovementDate <= eD).Select(s => s.Debit - s.Crdit).DefaultIfEmpty(0).Sum();
                //AccountsMovesforProperty
                var Orders = checkAccount.AccountsMovements.Where(a => a.AccountMovementDate >= sD && a.AccountMovementDate <= eD).Select(m => new AccountsMovesforProperty()
                {
                    orderNote = m.AccountOrder.OrderNote,
                    date = m.AccountMovementDate,
                    debit = m.Debit,
                    crdit = m.Crdit,
                    balance = m.Debit - m.Crdit + checkAccount.AccountsMovements.Where(a => a.AccountMovementDate < m.AccountMovementDate).Select(s => s.Debit - s.Crdit).DefaultIfEmpty(0).Sum(),
                    orderNo = m.AccountOrder.OrderNo,
                    refrenceId = m.AccountOrder.OrderIdRefrence,
                    typeRefrence = m.AccountOrder.OrderTypeRefrence
                    
               }).ToList();
                var totalCount = Orders.Count();

                #endregion
                //var totalCount = this.AppCompanyUserManager.GetAccountmoves(or.id, sD, eD).Where(a => a.date >= sD && a.date <= eD).Count();
                var totalPages = Math.Ceiling((double)totalCount / or.pageSize);

                //var Orders = this.AppCompanyUserManager.GetAccountmoves(or.id, sD, eD).Where(a => a.date >= sD && a.date <= eD).ToList();

                if (QueryHelper.PropertyExists<AccountsMoves>(or.orderby))
                {

                    switch (or.orderby)
                    {
                        case "date":
                            if (or.orderType)
                            {
                                Orders = Orders.OrderByDescending(a => a.date).ToList();
                            }
                            else
                            {
                                Orders = Orders.OrderBy(a => a.date).ToList();

                            }

                            break;
                        case "note":
                            if (or.orderType)
                            {
                                Orders = Orders.OrderByDescending(a => a.note).ToList();
                            }
                            else
                            {
                                Orders = Orders.OrderBy(a => a.note).ToList();

                            }

                            break;

                        case "debit":

                            if (or.orderType)
                            {
                                Orders = Orders.OrderByDescending(a => a.debit).ToList();
                            }
                            else
                            {
                                Orders = Orders.OrderBy(a => a.debit).ToList();

                            }
                            break;
                        case "crdit":


                            if (or.orderType)
                            {
                                Orders = Orders.OrderByDescending(a => a.crdit).ToList();
                            }
                            else
                            {
                                Orders = Orders.OrderBy(a => a.crdit).ToList();

                            }
                            break;

                        default:
                            Orders = Orders.OrderByDescending(a => a.date).ToList();
                            break;


                    }
                }
                if (Orders.Count() == 0)
                {
                    //sD = DateTime.Now.AddDays(-29);
                    //eD = DateTime.Now;
                    //Orders = this.AppCompanyUserManager.GetAccountmoves(or.id, sD, eD).Where(a => a.date >= sD && a.date <= eD).OrderByDescending(a => a.orderNo);
                }
                Int32 num = 0;
                foreach (var item in Orders)
                {
                    num += 1;
                    item.indexdId = num;
                }
                var orders = Orders.Skip((or.pageNumber - 1) * or.pageSize)
                                        .Take(or.pageSize)
                                        .ToList();
                var result = new
                {
                    TotalCount = totalCount,
                    totalPages = totalPages,
                    Orders = new {
                        firstBalnce = firstBalnce,
                        currentbalance = currentbalance,
                        periodbalance = periodbalance,
                        orders = orders
                    } 
                };

                return Ok(result);
            }

            if(or.search == "accounts")
            {
                var acc = db.Accounts.Where(a => a.AccountCategory.AccountsDataTypes.TypeName == accType 
                                              && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId
                                              && a.BranchId == (or.IsCompany == true ? (int?)null : companyId))
                    .Select(a => new AccountsMovesProperty()
                    {
                        accountName = a.AccountName,
                        pBalance = a.AccountsMovements.Where(m => m.AccountMovementDate <= sD).Select(d => d.Debit).DefaultIfEmpty(0).Sum() - a.AccountsMovements.Where(m => m.AccountMovementDate <= sD).Select(d => d.Crdit).DefaultIfEmpty(0).Sum(),
                        debit = a.AccountsMovements.Where(m => m.AccountMovementDate >= sD && m.AccountMovementDate <= eD).Select(d => d.Debit).DefaultIfEmpty(0).Sum(),
                        crdit = a.AccountsMovements.Where(m => m.AccountMovementDate >= sD && m.AccountMovementDate <= eD).Select(d => d.Crdit).DefaultIfEmpty(0).Sum(),
                        Balance = a.AccountsMovements.Select(d => d.Debit).DefaultIfEmpty(0).Sum() - a.AccountsMovements.Select(d => d.Crdit).DefaultIfEmpty(0).Sum(),

                    }).ToList();

             acc.ForEach(x => {
                    x.spBalance = x.pBalance < 0 ? string.Format("({0})" ,Math.Abs(Math.Round(x.pBalance , 2))) : Math.Abs(Math.Round(x.pBalance, 2)).ToString();
                    x.sBalance = x.Balance < 0 ? string.Format("({0})", Math.Abs(Math.Round(x.Balance, 2))) : Math.Abs(Math.Round(x.Balance, 2)).ToString();
                    x.debit = Math.Abs(Math.Round(x.debit, 2));
                    x.crdit = Math.Abs(Math.Round(x.crdit, 2));
               
                });
                var totalCount = acc.Count();
                var totalPages = Math.Ceiling((double)totalCount / or.pageSize);

                var Orders = acc.ToList();

                if (QueryHelper.PropertyExists<AccountsMoves>(or.orderby))
                {

                    switch (or.orderby)
                    {
                        case "accountName":
                            if (or.orderType)
                            {
                                Orders = Orders.OrderByDescending(a => a.accountName).ToList();
                            }
                            else
                            {
                                Orders = Orders.OrderBy(a => a.accountName).ToList();

                            }

                            break;
                        case "note":
                            if (or.orderType)
                            {
                                Orders = Orders.OrderByDescending(a => a.note).ToList();
                            }
                            else
                            {
                                Orders = Orders.OrderBy(a => a.note).ToList();

                            }

                            break;

                        case "debit":

                            if (or.orderType)
                            {
                                Orders = Orders.OrderByDescending(a => a.debit).ToList();
                            }
                            else
                            {
                                Orders = Orders.OrderBy(a => a.debit).ToList();

                            }
                            break;
                        case "crdit":


                            if (or.orderType)
                            {
                                Orders = Orders.OrderByDescending(a => a.crdit).ToList();
                            }
                            else
                            {
                                Orders = Orders.OrderBy(a => a.crdit).ToList();

                            }
                            break;

                        default:
                            Orders = Orders.OrderByDescending(a => a.accountName).ToList();
                            break;


                    }
                }
                if (Orders.Count() == 0)
                {
                    //sD = DateTime.Now.AddDays(-29);
                    //eD = DateTime.Now;
                    //Orders = this.AppCompanyUserManager.GetAccountmoves(or.id, sD, eD).Where(a => a.date >= sD && a.date <= eD).OrderByDescending(a => a.orderNo);
                }
                Int32 num = 0;
                foreach (var item in Orders)
                {
                    num += 1;
                    item.indexdId = num;
                }
                var orders = Orders.Skip((or.pageNumber - 1) * or.pageSize)
                                        .Take(or.pageSize)
                                        .ToList();
                var result = new
                {
                    TotalCount = totalCount,
                    totalPages = totalPages,
                    Orders = orders, 
                    SumPBalance = acc.Select(b => b.pBalance).DefaultIfEmpty(0).Sum() < 0 ? string.Format("({0})", Math.Abs(Math.Round(acc.Select(b => b.pBalance).DefaultIfEmpty(0).Sum(), 2))) : Math.Abs(Math.Round(acc.Select(b => b.pBalance).DefaultIfEmpty(0).Sum())).ToString() , 
                    SumBalance = acc.Select(b => b.pBalance).DefaultIfEmpty(0).Sum() < 0 ? string.Format("({0})", Math.Abs(Math.Round(acc.Select(b => b.Balance).DefaultIfEmpty(0).Sum(), 2))) : Math.Abs(Math.Round(acc.Select(b => b.Balance).DefaultIfEmpty(0).Sum())).ToString(),
                    SumDebit = acc.Select(b => b.debit).DefaultIfEmpty(0).Sum(),
                    SumCrdit = acc.Select(b => b.crdit).DefaultIfEmpty(0).Sum()

                };

                return Ok(result);
                
            }
            return BadRequest();

        }

        //getacNames
        [Route("getacNames")]
        [HttpPost]
        public IHttpActionResult getacNames(AddaccountPropetyValueCompanyBranch company)
        {
            var accType = "";
            string uid = RequestContext.Principal.Identity.GetUserId();

            switch (company.accountType)
            {
                case "pur":
                    accType = "موردين";
                    break;
                case "cust":
                    accType = "عملاء";
                    break;
                case "bank":
                    accType = "بنوك";
                    break;
                case "inventory":
                    accType = "مخازن";
                    break;
                case "cash":
                    accType = "نقود";
                    break;
                case "notereceive":
                    accType = "اوراق قبض";
                    break;
                case "notepay":
                    accType = "اوراق دفع";
                    break;
                case "other":
                    accType = "";
                    break;
                default:
                    accType = "";
                    break;


            }

            Int32 companyId = 0;
            if (company.isCompany)
            {
                companyId = company.CompanyId;
                var acc = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && a.BranchId == null && a.AccountCategory.AccountsDataTypes.TypeName != accType)
                    .Select(c => new
                    {
                        accountId = c.AccountId,
                        accountName = c.AccountName,
                        Code = c.Code
                    }).ToList();
                return Ok(acc);
            }
            else
            {
                companyId = db.Branchs.Find(company.CompanyId).CompanyId;
                var acc = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && a.BranchId == company.CompanyId && a.AccountCategory.AccountsDataTypes.TypeName != accType)
                  .Select(c => new
                  {
                      accountId = c.AccountId,
                      accountName = c.AccountName,
                      Code = c.Code
                  }).ToList();
                return Ok(acc);
            }

            
        }
        //OrderRequesVm
        [Route("setOrderRequest")]
        [HttpPost]
        public IHttpActionResult setOrderRequest(OrderRequesVm or)
        {
            var accType = "";
            string uid = RequestContext.Principal.Identity.GetUserId();

            switch (or.accountType)
            {
                case "pur":
                    accType = "موردين";
                    break;
                case "cust":
                    accType = "عملاء";
                    break;
                case "bank":
                    accType = "بنوك";
                    break;
                case "inventory":
                    accType = "مخازن";
                    break;
                case "cash":
                    accType = "نقود";
                    break;
                case "notereceive":
                    accType = "اوراق قبض";
                    break;
                case "notepay":
                    accType = "اوراق دفع";
                    break;
                case "other":
                    accType = "";
                    break;
                default:
                    accType = "";
                    break;


            }

            Int32 companyId = 0;
        
            if (or.isCompany)
            {
                companyId = or.CompanyId;
               
            }
            else
            {
                companyId = db.Branchs.Find(or.CompanyId).CompanyId;
            }
            if (or.isNewRequest)
            {
                //var billNo = db.orderRequest.Where(a => a.AccountsDataTypes.TypeName == accType && a.CompanyId == companyId && a.BranchId == (or.isCompany == true ? (int?)null : or.CompanyId)).Count() + 1;

                var newRequest = new OrderRequest()
                {
                    CompanyId = companyId,
                    AccountsDataTypesId = db.AccountsDataTypes.SingleOrDefault(a => a.TypeName == accType).Id,
                    BranchId = or.isCompany == true ? (int?)null : or.CompanyId,
                    isQty = or.isQty,
                    mainAccountId = or.mainAccount.accountId,
                    tilteAccountId = or.tilteAccount.accountId,
                    requestNo = db.orderRequest.Where(a => a.AccountsDataTypes.TypeName == accType && a.CompanyId == companyId && a.BranchId == (or.isCompany == true ? (int?)null : or.CompanyId) && a.typeName == or.typeName).Count()+ 1,
                    CurrentWorkerId = uid,
                    dueDate = or.dueDate,
                    typeName = or.typeName,
                    requestDate = or.requestDate,
                    items = or.items.Select(i => new OrderRequestItems()
                    {
                        CurrentWorkerId = uid,
                        note = i.note,
                        price = i.price,
                        qty = i.qty,
                        refrenceType = i.refrenceType,
                        refrenceTypeId = i.refrenceTypeId,
                        
                    }).ToList()
                };

                db.orderRequest.Add(newRequest);
                db.SaveChanges();
                var user = db.Users.Find(uid);
                return Ok(new { requestNo = newRequest.requestNo, userName = string.Format("{0} {1}", user.FirstName, user.LastName) });

            }
            else
            {
                //update
                var findR = db.orderRequest.SingleOrDefault(a => a.AccountsDataTypes.TypeName == accType && a.CompanyId == companyId && a.BranchId == (or.isCompany == true ? (int?)null : or.CompanyId) && a.requestNo == or.requestNo && a.typeName == or.typeName);
                if(findR == null)
                {
                    return BadRequest("لا يوجد طلب مسجل بهذا الاسم");
                }

                if (findR.isDone)
                {
                    return BadRequest("تم الموافقة علي الطلب لا يمكنك التعديل");
                }
                if (findR.isToCancel)
                {
                    return BadRequest("طلب ملغي لا يمكنك التعديل");
                }


                findR.isQty = or.isQty;
                findR.mainAccountId = or.mainAccount.accountId;
                findR.tilteAccountId = or.tilteAccount.accountId;
                findR.CurrentWorkerId = uid;
                findR.dueDate = or.dueDate;
                findR.isToCancel = or.isToCancel;
               
                var items = or.items.Select(i => new OrderRequestItems()
                {
                    OrderRequestId = findR.OrderRequestId,
                    CurrentWorkerId = uid,
                    note = i.note,
                    price = i.price,
                    qty = i.qty,
                    refrenceType = i.refrenceType,
                    refrenceTypeId = i.refrenceTypeId,
                   
                }).ToList();
               
              
                db.Entry(findR).State = EntityState.Modified;
                db.OrderRequestItems.RemoveRange(findR.items);
                db.OrderRequestItems.AddRange(items);
                db.SaveChanges();
                var user = db.Users.Find(uid);
                return Ok(new { requestNo = findR.requestNo, userName = string.Format("{0} {1}", user.FirstName, user.LastName) });

            }

           
        }

        [Route("getOrderRequestById")]
        [HttpPost]
        public IHttpActionResult getOrderRequestById(getOrderRequest or)
        {
            var accType = "";
            string uid = RequestContext.Principal.Identity.GetUserId();

            switch (or.accountType)
            {
                case "pur":
                    accType = "موردين";
                    break;
                case "cust":
                    accType = "عملاء";
                    break;
                case "bank":
                    accType = "بنوك";
                    break;
                case "inventory":
                    accType = "مخازن";
                    break;
                case "cash":
                    accType = "نقود";
                    break;
                case "notereceive":
                    accType = "اوراق قبض";
                    break;
                case "notepay":
                    accType = "اوراق دفع";
                    break;
                case "other":
                    accType = "";
                    break;
                default:
                    accType = "";
                    break;


            }

            Int32 companyId = 0;

            if (or.isCompany)
            {
                companyId = or.CompanyId;

            }
            else
            {
                companyId = db.Branchs.Find(or.CompanyId).CompanyId;
            }

            var findR = db.orderRequest.SingleOrDefault(a => a.AccountsDataTypes.TypeName == accType && a.CompanyId == companyId && a.BranchId == (or.isCompany == true ? (int?)null : or.CompanyId) && a.requestNo == or.requestNo && a.typeName == or.typeName);
            string requestTypeName = "";
            if (findR == null)
            {
                if (!string.IsNullOrEmpty(or.RequestTypeName))
                {
                    var findtype = db.Accounts.SingleOrDefault(a => a.Code == or.RequestTypeName && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && a.BranchId == (or.isCompany == true ? (int?)null : or.CompanyId));
                    if(findtype != null)
                    {
                        findR = db.orderRequest.SingleOrDefault(a => a.AccountsDataTypes.TypeName == findtype.AccountCategory.AccountsDataTypes.TypeName && a.CompanyId == companyId && a.BranchId == (or.isCompany == true ? (int?)null : or.CompanyId) && a.requestNo == or.requestNo && a.typeName != or.typeName);
                        requestTypeName = findtype.AccountCategory.AccountsDataTypes.TypeName;
                    }
                    else
                    {
                        return BadRequest("لا يوجد طلب مسجل بهذا الرقم");
                    }
                }
                else
                {
                    return BadRequest("لا يوجد طلب مسجل بهذا الرقم");
                }
               
                
            }
            OrderRequesVm order = new OrderRequesVm()
            {
                CompanyId = findR.CompanyId,
                dueDate = findR.dueDate,
                RequestTypeName = requestTypeName,
                isCompany = or.isCompany,
                isDone = findR.isDone,
                isNewRequest = false,
                isQty = findR.isQty,
                isToCancel = findR.isToCancel,
                mainAccount = new accountforRequestVm() { accountId = findR.mainAccountId , accountName = findR.mainAccount.AccountName , Code = findR.mainAccount.Code},
                tilteAccount = new accountforRequestVm() { accountId = findR.tilteAccountId, accountName = findR.tilteAccount.AccountName , Code = findR.tilteAccount.Code},
                requestNo = findR.requestNo,
                orderRequestId = findR.OrderRequestId,
                requestDate = findR.requestDate,
                typeName = findR.typeName,
                accountType = or.accountType,
               
                items = findR.items.Select(a => new OrderRequestitemsVm()
                {
                    amount = a.price * a.qty,
                    price = a.price,
                    note = a.note ,
                    qty = a.qty,
                    refrenceTypeId = a.refrenceTypeId == (int?)null ? 0 :(int) a.refrenceTypeId,
                    refrenceType = a.refrenceType,
                    OrderRequestitemsId = a.OrderRequestItemsId
                }).ToList()
               
            };
            var user = db.Users.Find(findR.CurrentWorkerId);
            var userName = "";
            if (user != null)
            {
                userName = string.Format("{0} {1}", user.FirstName, user.LastName);
                order.userName = userName;
            };
            
            return Ok(order);
        }

        //OrderRequesVm
        [Route("setOrderMoveRequest")]
        [HttpPost]
        public IHttpActionResult setOrderMoveRequest(OrderRequestMoveVm or)
        {
            var accType = "";
            string uid = RequestContext.Principal.Identity.GetUserId();
            if (string.IsNullOrEmpty(or.note))
            {
                return BadRequest("يجب ادخال البيان");
               
            }
            if (or.attchedRequest)
            {
                if(or.orderRequest.items.Count() <= 0)
                {
                    return BadRequest("الطلب المرفق لا يحتوي علي اي بيانات ");
                }
                foreach (var a in or.orderRequest.items)
                {
                    if (string.IsNullOrEmpty(a.note))
                    {
                        return BadRequest("بيان خالي في الطلب المرفق");
                    }
                    if (a.price <= 0)
                    {
                        return BadRequest("السعر يجب ان يكون اكبر من صفر");
                    }
                    if (a.qty <= 0)
                    {
                        return BadRequest("الكمية يجب ان تكون اكبر من صفر");
                    }

                }
            }

            switch (or.accountType)
            {
                case "pur":
                    accType = "موردين";
                    break;
                case "cust":
                    accType = "عملاء";
                    break;
                case "bank":
                    accType = "بنوك";
                    break;
                case "inventory":
                    accType = "مخازن";
                    break;
                case "cash":
                    accType = "نقود";
                    break;
                case "notereceive":
                    accType = "اوراق قبض";
                    break;
                case "notepay":
                    accType = "اوراق دفع";
                    break;
                case "other":
                    accType = "اخري";
                    break;
                default:
                    accType = "";
                    break;
            }



            Int32 companyId = 0;
            var titleAc = new Account();
            var mainAc = new Account();

            if (or.isCompany)
            {
                companyId = or.CompanyId;
                var cc = db.Companies.Find(companyId);

                if(cc == null)
                {
                    return BadRequest("الشركة غير مسجلة");
                }

                titleAc = db.Accounts.SingleOrDefault(a => a.AccountId == or.tilteAccount.accountId
                          && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && a.BranchId == (int?)null);

                mainAc = db.Accounts.SingleOrDefault(a => a.AccountId == or.mainAccount.accountId
                         && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && a.BranchId ==(int?) null);
                if(titleAc == null || mainAc == null)
                {
                    return BadRequest("خطاء في اختيار الحسابات");
                }
            }
            else
            {
                companyId = db.Branchs.Find(or.CompanyId).CompanyId;
                titleAc = db.Accounts.SingleOrDefault(a => a.AccountId == or.tilteAccount.accountId
                       && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && a.BranchId == or.CompanyId);

                mainAc = db.Accounts.SingleOrDefault(a => a.AccountId == or.mainAccount.accountId
                         && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == companyId && a.BranchId == or.CompanyId);
                if (titleAc == null || mainAc == null)
                {
                    return BadRequest("خطاء في اختيار الحسابات");
                }
            }
            if (or.isNewRequest)
            {
               var requestNo = db.OrderMoveRequest.Where(a => a.CompanyId == companyId && a.BranchId == (or.isCompany ? (int?)null : or.CompanyId) && a.mainAccountId == or.mainAccount.accountId && a.AccountsDataTypes.TypeName == accType && a.typeName == or.typeName).Count() != 0 ? db.OrderMoveRequest.Where(a => a.CompanyId == or.CompanyId && a.mainAccountId == or.mainAccount.accountId && a.AccountsDataTypes.TypeName == accType && a.typeName == or.typeName).Count() + 1 : 1;
                var orderrequest = new OrderMoveRequest()
                {
                    CompanyId = companyId,
                    BranchId = or.isCompany ? (int?)null : or.CompanyId,
                    AccountsDataTypesId = db.AccountsDataTypes.SingleOrDefault(a => a.TypeName == accType).Id,
                    CurrentWorkerId = uid,
                    isDone = or.isDone,
                    tilteAccountId = titleAc.AccountId,
                    mainAccountId = mainAc.AccountId,
                    OrderNo = requestNo,
                    isToCancel = false,
                    typeName = or.typeName,
                    OrderDate = or.OrderDate,
                    note = or.note,
                    amount = or.amount,
                    attchedRequest = or.attchedRequest

                };

                #region orderRequest
                var requestuserId = "";
                if (or.attchedRequest)
                {
                    var checkrequestAmount = or.orderRequest.items.Select(a => a.price * a.qty).DefaultIfEmpty(0).Sum();
                    if(checkrequestAmount != or.amount)
                    {
                        return BadRequest("مبالغ مالية غير صحيحة");

                    }
                   

                    if (or.orderRequest.isNewRequest)
                    {
                        requestuserId = uid;
                        var newRequest = new OrderRequest()
                        {
                            CompanyId = companyId,
                            AccountsDataTypesId = db.AccountsDataTypes.SingleOrDefault(a => a.TypeName == accType).Id,
                            BranchId = or.isCompany == true ? (int?)null : or.CompanyId,
                            isQty = or.orderRequest.isQty,
                            mainAccountId = or.mainAccount.accountId,
                            tilteAccountId = or.tilteAccount.accountId,
                            requestNo = db.orderRequest.Where(a => a.AccountsDataTypes.TypeName == accType && a.CompanyId == companyId && a.BranchId == (or.isCompany == true ? (int?)null : or.CompanyId) && a.typeName == or.typeName).Count() + 1,
                            CurrentWorkerId = uid,
                            dueDate = or.OrderDate,
                            typeName = or.typeName,
                            requestDate = or.OrderDate,
                            isDone = true,
                            items = or.orderRequest.items.Select(i => new OrderRequestItems()
                            {
                                CurrentWorkerId = uid,
                                note = i.note,
                                price = i.price,
                                qty = i.qty,
                                refrenceType = i.refrenceType,
                                refrenceTypeId = i.refrenceTypeId,

                            }).ToList()
                        };

                        if (orderrequest.orderRequest == null)
                        {
                            orderrequest.orderRequest = new OrderRequest();
                        }
                        orderrequest.orderRequest = newRequest;
                    }
                    else
                    {
                        var findR = db.orderRequest.SingleOrDefault(a => a.AccountsDataTypes.TypeName == accType && a.CompanyId == companyId && a.BranchId == (or.isCompany == true ? (int?)null : or.CompanyId) && a.requestNo == or.orderRequest.requestNo && a.typeName == or.typeName);
                        if (findR == null)
                        {
                            return BadRequest("لا يوجد طلب مسجل بهذا الاسم");
                        }

                        if (findR.isDone)
                        {
                            return BadRequest("تم الموافقة علي الطلب لا يمكنك التعديل");
                        }
                        if (findR.isToCancel)
                        {
                            return BadRequest("طلب ملغي لا يمكنك الارفاق");
                        }
                        requestuserId = findR.CurrentWorkerId;
                        findR.dueDate = or.OrderDate;
                        findR.isDone = true;
                        db.Entry(findR).State = EntityState.Modified;
                        orderrequest.orderRequestId = findR.OrderRequestId;
                    }
                }


                #endregion
              
                if (or.isDone && !or.isNewRequest)
                {
                    #region AccountMovement

                    var orderNo = 1;
                    if (or.isCompany)
                    {
                        orderNo = db.AccountOrders.Where(a => a.CompanyId == or.CompanyId).Count() != 0 ? db.AccountOrders.Where(a => a.CompanyId == or.CompanyId).Max(a => a.OrderNo) + 1 : 1;
                    }
                    else
                    {
                        orderNo = db.AccountOrders.Where(a => a.BranchId == or.CompanyId).Count() != 0 ? db.AccountOrders.Where(a => a.BranchId == or.CompanyId).Max(a => a.OrderNo) + 1 : 1;
                    }
                    string ot = or.typeName == "pay" ? "صرف" : "اضافة";
                    var accOrder = new AccountOrder()
                    {
                        OrderDate = or.OrderDate,
                        OrderNote = or.note + string.Format(" - اذن {0} رقم {1}", ot, orderrequest.OrderNo),
                        BranchId = or.isCompany ? (int?)null : or.CompanyId,
                        CompanyId = or.isCompany ? or.CompanyId : (int?)null,
                        AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId,
                        CurrentWorkerId = uid,
                        OrderNo = orderNo,
                        IsCompany = or.isCompany

                    };
                    if (or.amount <= 0)
                    {
                        return BadRequest("يجب ان تكون القيمة اكبر من صفر");
                    }
                    var acMoves = new List<AccountMovement>();
                    if (or.typeName == "pay")
                    {

                        var acMove = new AccountMovement()
                        {
                            AccountId = or.mainAccount.accountId,
                            AccountMovementDate = or.OrderDate,
                            Debit = mainAc.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? 0 : or.amount,
                            Crdit = mainAc.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? or.amount : 0,
                            CurrentWorkerId = uid,
                            AccountMovementNote = accType,
                            typeName = or.typeName

                        };
                        var acMove2 = new AccountMovement()
                        {
                            AccountId = or.tilteAccount.accountId,
                            AccountMovementDate = or.DoneDate,
                            Debit = mainAc.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? or.amount : 0,
                            Crdit = mainAc.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? 0 : or.amount,
                            CurrentWorkerId = uid,
                            AccountMovementNote = accType,
                            typeName = "recive"
                        };

                        acMoves.Add(acMove);
                        acMoves.Add(acMove2);
                        if (accOrder.AccountMovements == null)
                        {
                            accOrder.AccountMovements = new List<AccountMovement>();
                        }
                        accOrder.AccountMovements = acMoves;
                    }
                    else
                    {

                        var acMove = new AccountMovement()
                        {
                            AccountId = or.mainAccount.accountId,
                            AccountMovementDate = or.OrderDate,
                            Debit = mainAc.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? or.amount : 0,
                            Crdit = mainAc.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? 0 : or.amount,
                            CurrentWorkerId = uid,
                            AccountMovementNote = accType,
                            typeName = or.typeName


                        };
                        var acMove2 = new AccountMovement()
                        {
                            AccountId = or.tilteAccount.accountId,
                            AccountMovementDate = or.OrderDate,
                            Debit = mainAc.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? 0 : or.amount,
                            Crdit = mainAc.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? or.amount : 0,
                            CurrentWorkerId = uid,
                            AccountMovementNote = accType,
                            typeName = "pay"
                        };

                        acMoves.Add(acMove);
                        acMoves.Add(acMove2);
                        if (accOrder.AccountMovements == null)
                        {
                            accOrder.AccountMovements = new List<AccountMovement>();
                        }
                        accOrder.AccountMovements.AddRange(acMoves);
                    }
                    #endregion
                    if(accOrder.OrderMoveRequest == null)
                    {
                        accOrder.OrderMoveRequest = new OrderMoveRequest();
                    }
                    accOrder.OrderMoveRequest = orderrequest;
                    db.AccountOrders.Add(accOrder);
                }
                else
                {
                    db.OrderMoveRequest.Add(orderrequest);
                }
                db.SaveChanges();
                var toreturn = AccountPropertyManager.getOrderRequestMoveById(new getOrderRequest { requestNo = orderrequest.OrderNo, accountType = or.accountType, CompanyId = or.CompanyId, isCompany = or.isCompany, typeName = or.typeName });

                return Ok(toreturn);

            }
            else
            {
                var findo = db.OrderMoveRequest.SingleOrDefault(a => a.OrderNo == or.OrderNo && a.CompanyId == companyId && a.BranchId == (or.isCompany ? (int?)null : or.CompanyId) && a.AccountsDataTypes.TypeName == accType && a.typeName == or.typeName);
                if (findo.attchedRequest)
                {
                    var freq = db.orderRequest.Find(findo.orderRequestId);
                    if(findo.orderRequest == null)
                    {
                        findo.orderRequest = new OrderRequest();
                        findo.orderRequest = freq;
                    }
                }
                if(findo != null)
                {
                    var now = DateTime.Now;
                    var Nowdate = now.AddMilliseconds(-now.Millisecond);

                    if (findo.isToCancel)
                    {
                        return BadRequest("لقد تم الغاء الاذن من قبل");
                    }
                    if(or.amount <= 0)
                    {
                        return BadRequest("القيمة يجب ان تكون اكبر من صفر");
                    }
                    if(!findo.isDone && !findo.isToCancel && !or.isToCancel)
                    {
                        findo.CurrentWorkerId = uid;
                        findo.note = or.note;
                        findo.amount = or.amount;
                       // findo.isDone = true;
                        findo.attchedRequest = or.attchedRequest;
                        if(findo.attchedRequest && !or.attchedRequest)
                        {
                           //remove Request
                            findo.orderRequest.isDone = false;
                            findo.attchedRequest = false;
                            findo.orderRequestId =(int?) null;
                            
                        }
                        if(findo.attchedRequest && or.attchedRequest)
                        {
                            //update request
                            findo.orderRequest.isQty = or.orderRequest.isQty;
                            findo.CurrentWorkerId = uid;
                            db.OrderRequestItems.RemoveRange(findo.orderRequest.items);
                            findo.orderRequest.items = or.orderRequest.items.Select(a => new OrderRequestItems()
                            {
                                CurrentWorkerId = uid,
                                note = a.note,
                                price = a.price,
                                qty = a.qty,
                                refrenceType = a.refrenceType,
                                refrenceTypeId = a.refrenceTypeId,
                                
                            }).ToList();
                        }
                        if(!findo.attchedRequest && or.attchedRequest && findo.orderRequestId == (int?)null)
                        {
                            //attch OrderRequest
                            if (or.orderRequest.isNewRequest)
                            {
                                //new request
                                var newrequest = new OrderRequest()
                                {
                                    CurrentWorkerId = uid,
                                    requestDate = Nowdate,
                                    dueDate = findo.OrderDate,
                                    AccountsDataTypesId = findo.AccountsDataTypesId,
                                    isQty = or.orderRequest.isQty,
                                    isDone = true,
                                    mainAccountId = findo.mainAccountId,
                                    tilteAccountId = findo.tilteAccountId,
                                    requestNo = db.orderRequest.Where(a => a.AccountsDataTypes.TypeName == accType && a.CompanyId == companyId && a.BranchId == (or.isCompany == true ? (int?)null : or.CompanyId) && a.typeName == or.typeName).Count() + 1,
                                    CompanyId = findo.CompanyId,
                                    BranchId = findo.BranchId,
                                    typeName = findo.typeName,
                                    items = or.orderRequest.items.Select(a => new OrderRequestItems()
                                    {
                                        CurrentWorkerId = uid,
                                        note = a.note,
                                        price = a.price,
                                        qty = a.qty,
                                        refrenceType = a.refrenceType,
                                        refrenceTypeId = a.refrenceTypeId,

                                    }).ToList(),
                                    
                                };

                                if (findo.orderRequest == null)
                                {
                                    findo.orderRequest = new OrderRequest();
                                }
                                findo.orderRequest = newrequest;
                            }
                            else
                            {
                                //attchRequest && findRequest && update
                                if(or.RequestTypeName == accType)
                                {
                                    var findR = db.orderRequest.SingleOrDefault(a => a.CompanyId == companyId && a.BranchId == (or.isCompany == true ? (int?)null : or.CompanyId) && a.requestNo == or.orderRequest.requestNo && a.AccountsDataTypes.TypeName == accType && a.typeName == or.typeName);
                                    if (findR == null)
                                    {
                                        
                                        return BadRequest("الاذن غير مسجل او غير متاح لهذة العملية");

                                    }
                                    else
                                    {
                                        //attchRequest && update
                                        db.OrderRequestItems.RemoveRange(findR.items);
                                        findR.isQty = or.orderRequest.isQty;
                                        findR.isDone = true;
                                        findR.dueDate = or.OrderDate;
                                        findR.CurrentWorkerId = uid;
                                        findR.items = or.orderRequest.items.Select(a => new OrderRequestItems()
                                        {
                                            CurrentWorkerId = uid,
                                            note = a.note,
                                            price = a.price,
                                            qty = a.qty,
                                            refrenceType = a.refrenceType,
                                            refrenceTypeId = a.refrenceTypeId,

                                        }).ToList();
                                        db.Entry(findR).State = EntityState.Modified;
                                        findo.orderRequestId = findR.OrderRequestId;

                                    }
                                }
                                else
                                {
                                    var rtype = db.AccountsDataTypes.SingleOrDefault(a => a.TypeName == or.RequestTypeName);
                                    if(rtype == null)
                                    {
                                        return BadRequest("فئة حسابات غير مسجلة او غير متاحة لهذة العملية");
                                    }
                                    var findR = db.orderRequest.SingleOrDefault(a => a.CompanyId == companyId && a.BranchId == (or.isCompany == true ? (int?)null : or.CompanyId) && a.requestNo == or.orderRequest.requestNo && a.isDone == false && a.isToCancel == false && a.tilteAccountId == or.mainAccount.accountId &&  a.AccountsDataTypes.TypeName == or.RequestTypeName && a.typeName != or.typeName);
                                    if(findR == null)
                                    {
                                        return  BadRequest("الاذن غير مسجل او غير متاح لهذة العملية");

                                    }
                                    findo.orderRequestId = findR.OrderRequestId;

                                }

                            }
                        }
                    }
                    #region orderDone update
                    if(!findo.isDone && or.isDone && !or.isToCancel) {
                        #region AccountMovement

                        var orderNo = 1;
                        if (or.isCompany)
                        {
                            orderNo = db.AccountOrders.Where(a => a.CompanyId == or.CompanyId).Count() != 0 ? db.AccountOrders.Where(a => a.CompanyId == or.CompanyId).Max(a => a.OrderNo) + 1 : 1;
                        }
                        else
                        {
                            orderNo = db.AccountOrders.Where(a => a.BranchId == or.CompanyId).Count() != 0 ? db.AccountOrders.Where(a => a.BranchId == or.CompanyId).Max(a => a.OrderNo) + 1 : 1;
                        }
                        string ot = or.typeName == "pay" ? "صرف" : "اضافة";
                        var accOrder = new AccountOrder()
                        {
                            OrderDate = Nowdate,
                            OrderNote = or.note + string.Format(" - اذن {0} رقم {1}" , ot , findo.OrderNo),
                            BranchId = or.isCompany ? (int?)null : or.CompanyId,
                            CompanyId = or.isCompany ? or.CompanyId : (int?)null,
                            AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId,
                            CurrentWorkerId = uid,
                            OrderNo = orderNo,
                            OrderMoveRequestId = findo.orderRequestId,
                            IsCompany = or.isCompany

                        };
                        if (or.amount <= 0)
                        {
                            return BadRequest("يجب ان تكون القيمة اكبر من صفر");
                        }
                        var acMoves = new List<AccountMovement>();
                        if (or.typeName == "pay")
                        {

                            var acMove = new AccountMovement()
                            {
                                AccountId = or.mainAccount.accountId,
                                AccountMovementDate = Nowdate,
                                Debit = mainAc.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? 0 : or.amount,
                                Crdit = mainAc.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? or.amount : 0,
                                CurrentWorkerId = uid,
                                AccountMovementNote = accType,
                                typeName = or.typeName

                            };
                            var acMove2 = new AccountMovement()
                            {
                                AccountId = or.tilteAccount.accountId,
                                AccountMovementDate = Nowdate,
                                Debit = mainAc.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? or.amount : 0,
                                Crdit = mainAc.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? 0 : or.amount,
                                CurrentWorkerId = uid,
                                AccountMovementNote = accType,
                                typeName = "recive"
                            };

                            acMoves.Add(acMove);
                            acMoves.Add(acMove2);
                            if (accOrder.AccountMovements == null)
                            {
                                accOrder.AccountMovements = new List<AccountMovement>();
                            }
                            accOrder.AccountMovements = acMoves;
                        }
                        else
                        {

                            var acMove = new AccountMovement()
                            {
                                AccountId = or.mainAccount.accountId,
                                AccountMovementDate = Nowdate,
                                Debit = mainAc.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? or.amount : 0,
                                Crdit = mainAc.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? 0 : or.amount,
                                CurrentWorkerId = uid,
                                AccountMovementNote = accType,
                                typeName = or.typeName


                            };
                            var acMove2 = new AccountMovement()
                            {
                                AccountId = or.tilteAccount.accountId,
                                AccountMovementDate = Nowdate,
                                Debit = mainAc.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? 0 : or.amount,
                                Crdit = mainAc.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? or.amount : 0,
                                CurrentWorkerId = uid,
                                AccountMovementNote = accType,
                                typeName = "pay"
                            };

                            acMoves.Add(acMove);
                            acMoves.Add(acMove2);
                            if (accOrder.AccountMovements == null)
                            {
                                accOrder.AccountMovements = new List<AccountMovement>();
                            }
                            accOrder.AccountMovements.AddRange(acMoves);
                        }
                        #endregion
                        if (accOrder.OrderMoveRequest == null)
                        {
                            accOrder.OrderMoveRequest = new OrderMoveRequest();
                        }
                        accOrder.OrderMoveRequest = findo;
                        findo.isDone = true;
                        if(findo.orderRequest != null)
                        {
                            findo.orderRequest.isDone = true;
                        }
                       
                       // db.Entry(findo).State = EntityState.Modified;
                        db.AccountOrders.Add(accOrder);
                    }
                    #endregion
                    #region toCancel
                    if (or.isToCancel)
                    {

                        
                        if (findo.attchedRequest)
                        {
                            findo.orderRequest.isToCancel = true;
                            findo.orderRequest.CurrentWorkerId = uid;
                        }
                       
                        if (findo.isDone && !findo.isToCancel)
                        {
                            findo.isToCancel = true;
                            findo.CurrentWorkerId = uid;
                            db.Entry(findo).State = EntityState.Modified;
                            var orderNo = 1;
                            if (or.isCompany)
                            {
                                orderNo = db.AccountOrders.Where(a => a.CompanyId == or.CompanyId).Count() != 0 ? db.AccountOrders.Where(a => a.CompanyId == or.CompanyId).Max(a => a.OrderNo) + 1 : 1;
                            }
                            else
                            {
                                orderNo = db.AccountOrders.Where(a => a.BranchId == or.CompanyId).Count() != 0 ? db.AccountOrders.Where(a => a.BranchId == or.CompanyId).Max(a => a.OrderNo) + 1 : 1;
                            }
                            Int32 reId = findo.OrderMoveRequestId;
                            var accOrder = db.AccountOrders.SingleOrDefault(a => a.OrderMoveRequestId == reId);
                            var cancelOrder = new AccountOrder()
                            {
                                OrderDate = Nowdate,
                                OrderNote = string.Format("قيود الالغاء لاذن ({0}) رقم {1}", findo.typeName == "pay" ? "صرف" : "اضافة", findo.OrderNo),
                                BranchId = findo.BranchId,
                                CompanyId = findo.CompanyId,
                                AccountOrderPropertiesId = accOrder.AccountOrderPropertiesId,
                                CurrentWorkerId = uid,
                                OrderTypeRefrence = accType,
                                OrderIdRefrence = findo.OrderNo,
                                OrderNo = orderNo,
                               IsCompany = or.isCompany
                            };
                            var cancelMoves = new List<AccountMovement>();
                            foreach (var a in accOrder.AccountMovements)
                            {
                                var acc = new AccountMovement()
                                {
                                    AccountId = a.AccountId,
                                    AccountMovementDate = Nowdate,
                                    Debit = a.Crdit,
                                    Crdit = a.Debit,
                                    CurrentWorkerId = uid,
                                    AccountMovementNote = "قيد الغاء",
                                    typeName = a.typeName == "pay" ? "recive" : "pay"

                                };
                                cancelMoves.Add(acc);
                            }

                            if (cancelOrder.AccountMovements == null)
                            {
                                cancelOrder.AccountMovements = new List<AccountMovement>();
                            }
                            cancelOrder.AccountMovements = cancelMoves;
                            db.AccountOrders.Add(cancelOrder);
                        }


                    }
                    #endregion


                    db.Entry(findo).State = EntityState.Modified;

                    db.SaveChanges();
                   var toreturn = AccountPropertyManager.getOrderRequestMoveById(new getOrderRequest {requestNo = findo.OrderNo , accountType = or.accountType , CompanyId = or.CompanyId , isCompany = or.isCompany , typeName = or.typeName });

                    return Ok(toreturn);

                }
                else
                {
                    return BadRequest("الاذن غير مسجل او غير متاح لهذة العملية");

                }
            }



        }


        [Route("getOrderMoveById")]
        [HttpPost]
        public IHttpActionResult getOrderMoveById(getOrderRequest or)
        {
            var accType = "";
            string uid = RequestContext.Principal.Identity.GetUserId();

            switch (or.accountType)
            {
                case "pur":
                    accType = "موردين";
                    break;
                case "cust":
                    accType = "عملاء";
                    break;
                case "bank":
                    accType = "بنوك";
                    break;
                case "inventory":
                    accType = "مخازن";
                    break;
                case "cash":
                    accType = "نقود";
                    break;
                case "notereceive":
                    accType = "اوراق قبض";
                    break;
                case "notepay":
                    accType = "اوراق دفع";
                    break;
                case "other":
                    accType = "";
                    break;
                default:
                    accType = "";
                    break;


            }

            Int32 companyId = 0;

            if (or.isCompany)
            {
                companyId = or.CompanyId;

            }
            else
            {
                companyId = db.Branchs.Find(or.CompanyId).CompanyId;
            }
            
            var findR = db.OrderMoveRequest.SingleOrDefault(a => a.AccountsDataTypes.TypeName == accType && a.CompanyId == companyId && a.BranchId == (or.isCompany == true ? (int?)null : or.CompanyId) && a.OrderNo == or.requestNo && a.typeName == or.typeName);
            if(findR == null)
            {
                return BadRequest("الاذن غير مسجل او غير متاح لهذة العملية");
            }

            var om = AccountPropertyManager.getOrderRequestMoveById(or);
           
            return Ok(om);
        }

    }
}
