﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Inventory.DataLayer;
using Inventory.Models;
using Inventory.ViewModels;

namespace Inventory.Controllers
{
    public class AccountDetailsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationRepository repository = new ApplicationRepository();

        // GET: api/AccountDetails
        public IQueryable<AccountMovement> GetAccountMovements()
        {
            return db.AccountMovements;
        }

        //// GET: api/AccountDetails/5
        //[ResponseType(typeof(AccountDetailsVm))]
        //[ActionName("AccountDetailsById")]
        
        //public AccountDetailsVm GetAccountMovement(string id)
        //{
        //    return repository.GetAllAccountDetails(id);
           
        //}

        // PUT: api/AccountDetails/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutAccountMovement(int id, AccountMovement accountMovement)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != accountMovement.AccountMovementId)
            {
                return BadRequest();
            }

            db.Entry(accountMovement).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccountMovementExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AccountDetails
        //[ResponseType(typeof(AccountOrderVm))]
        //public async Task<IHttpActionResult> PostAccountMovement(AccountOrderVm accountOrderVm)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }
        //    AccountOrder accountOrder = db.AccountOrders.First(a => a.OrderDate == accountOrderVm.date);
        //    if (accountOrder.AccountOrderId == 0)
        //    {
        //        accountOrder = new AccountOrder()
        //        {
        //            OrderDate = accountOrderVm.date,
        //            AccountOrderPropertiesId = db.AccountOrderProperties.First(p => p.AccountOrderPropertiesName == accountOrderVm.AccountMovementFrom.AccountOrderPropertiesName).AccountOrderPropertiesId

        //        };

        //        db.AccountOrders.Add(accountOrder);
        //        await db.SaveChangesAsync();
        //        accountOrder = db.AccountOrders.First(a => a.OrderDate == accountOrderVm.date);
        //        AccountMovement accountMovementFrom = new AccountMovement()
        //        {
        //            AccountMovementDate = accountOrderVm.AccountMovementFrom.mdate,
        //            Crdit = Convert.ToDecimal(accountOrderVm.AccountMovementFrom.Crdit),
        //            Debit = Convert.ToDecimal(accountOrderVm.AccountMovementFrom.Debit),
        //            AccountOrderId = accountOrder.AccountOrderId,
        //            BranchId = db.Branchs.First(b => b.BranchName == accountOrderVm.AccountMovementFrom.BranchName).BranchId,
        //            AccountId = db.Accounts.First(a => a.AccountName == accountOrderVm.AccountMovementFrom.name).AccountId,

        //        };
        //        List<AccountMovement> AccountMovements = null;
        //        foreach (var i in accountOrderVm.AccountMovementsTo)
        //        {
        //            AccountMovement accountMovementTo = new AccountMovement()
        //            {
        //                AccountMovementDate = i.mdate,
        //                Crdit = Convert.ToDecimal(i.Crdit),
        //                Debit = Convert.ToDecimal(i.Debit),
        //                AccountOrderId = accountOrder.AccountOrderId,
        //                BranchId = db.Branchs.First(b => b.BranchName == i.BranchName).BranchId,
        //                AccountId = db.Accounts.First(a => a.AccountName == i.name).AccountId,

        //            };
        //            AccountMovements.Add(accountMovementTo);
        //        }

        //        db.AccountMovements.AddRange(AccountMovements);
        //        await db.SaveChangesAsync();

        //    }

        //    return CreatedAtRoute("DefaultApi", new { id = accountOrder.AccountOrderId }, accountOrder);
        //}

        // DELETE: api/AccountDetails/5
        [ResponseType(typeof(AccountMovement))]
        public async Task<IHttpActionResult> DeleteAccountMovement(int id)
        {
            AccountMovement accountMovement = await db.AccountMovements.FindAsync(id);
            if (accountMovement == null)
            {
                return NotFound();
            }

            db.AccountMovements.Remove(accountMovement);
            await db.SaveChangesAsync();

            return Ok(accountMovement);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool AccountMovementExists(int id)
        {
            return db.AccountMovements.Count(e => e.AccountMovementId == id) > 0;
        }
    }
}