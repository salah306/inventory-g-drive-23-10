﻿using Inventory.DataLayer;
using Inventory.Models;
using Inventory.ViewModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Inventory.Controllers
{
    [RoutePrefix("api/ItemGroup")]
    public class ItemGroupController : BaseApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationRepository repository = new ApplicationRepository();

        [Route("getAccountDataTypesForItemGroup/{id:int}")]
        [HttpGet]
        public IHttpActionResult GetAccountDataTypes(Int32 id)
        {
            var itemGroup = db.ItemGroups.Find(id);
            var types = (from a in db.Accounts
                         where a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == itemGroup.CompanyId
                         && a.AccountCategory.AccountsDataTypes.TypeName == "مخازن"
                         && !a.AccountsforItemGroups.Any(b => b.ItemGroupId == itemGroup.ItemGroupId)
                         select new accountsForItemGroup
                         {
                             TypeId = (Int32)a.AccountCategory.AccountsDataTypesId,
                             TypeName = a.AccountCategory.AccountsDataTypes.TypeName,
                             AccountId = a.AccountId,
                             AccountName = a.AccountName,
                             code = 0
                         }).ToList();
            foreach (var i in types)
            {
                i.code = this.AppCompanyUserManager.GetAccountCode((int)id, i.AccountId + "Ac");
            }

            return Ok(types);
        }
        //getinventoryValuatioMethods
        [Route("getinventoryValuatioMethods/{id:int}")]
        [HttpGet]
        public IHttpActionResult getinventoryValuatioMethods(Int32 id)
        {
            var getmethod = (from m in db.InventoryValuationMethods
                            select new GetInventoryValuationMethodVm() {
                                InventoryValuationMethodId = m.InventoryValuationMethodId,
                                InventoryValuationMethodName = m.InventoryValuationMethodName,
                                companyId = id,
                                isSelected = db.Companies.Any(c => c.CompanyId == id && c.InventoryValuationMethodId == m.InventoryValuationMethodId) == true ? "checked" : ""
                            }).ToList();
           
            return Ok(getmethod);
        }
        [Route("GetItemsGroupNames/{id:int}")]
        public IHttpActionResult GetItemsGroupNames(Int32 id)
        {

            #region try to but transit
           
            //var itGroups = db.ItemGroups.Where(a => a.Accounts.Count() > 0).SelectMany(c => c.Accounts).ToList();


            //foreach (var account in itGroups)
            //{
            //    var acc = db.Accounts.Find(account.AccountId);

            //    var trnsitAccountCat = db.AccountCategories.Where(a => a.AccountsDataTypes.aliasName == "goods-in-transit" && a.BalanceSheetType.BalanceSheet.CompanyId == acc.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId).FirstOrDefault();

            //    var transitAccount = trnsitAccountCat.Accounts.Where(a => a.BranchId == acc.BranchId && a.AccountCategory.AccountsDataTypes.aliasName == "goods-in-transit").FirstOrDefault();
            //    if (!db.AccountsforItemGroups.Any(a => a.AccountId == transitAccount.AccountId && a.ItemGroupId == account.ItemGroupId))
            //    {
            //        AccountsforItemGroup itemgroupaccounts = new AccountsforItemGroup
            //        {
            //            AccountId = transitAccount.AccountId,
            //            ItemGroupId = account.ItemGroupId

            //        };
            //        db.AccountsforItemGroups.Add(itemgroupaccounts);
            //        db.SaveChanges();
            //        var itemGroup = db.ItemGroups.Find(account.ItemGroupId);
            //        var prop = itemGroup.Properties.SingleOrDefault(p => p.PropertiesforItemGroupName == "الصنف");
            //        foreach (var value in prop.PropertyValuesforItemGroup)
            //        {

            //            var subaccounttr = new SubAccount
            //            {
            //                AccountId = transitAccount.AccountId,
            //                SubAccountName = value.PropertyValuesforItemGroupName,
            //                Realted = value.Realted
            //            };

            //            db.SubAccounts.Add(subaccounttr);
            //            db.SaveChanges();
            //            var subpropertytr = new subAccountNpropertyValues
            //            {
            //                SubAccountId = subaccounttr.SubAccountId,
            //                PropertyValuesforItemGroupId = value.PropertyValuesforItemGroupId
            //            };
            //            db.subAccountNpropertyValues.Add(subpropertytr);
            //            db.SaveChanges();

            //        }
            //    }

            //}
            #endregion

            string uid = RequestContext.Principal.Identity.GetUserId();
            var fcompany = db.Companies.SingleOrDefault(c => c.CompanyId == id && c.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
            if (fcompany == null)
            {
                return BadRequest("الشركة غير مسجلة او غير متاحة لهذة العملية");
            }
            var names = (from i in db.ItemGroups
                        where i.CompanyId == id
                        select new GetitemsGroupNames
                        {
                            itemGroupId = i.ItemGroupId,
                            itemGroupName = i.ItemGroupName
                        }).ToList();
            var units = (from i in db.Units
                         where i.CompanyId == id
                         select new UnitVM
                         {
                             UnitId = i.UnitId,
                             UnitName = i.UnitName,
                             UnitTypeId = i.UnitTypeId
                         }).ToList();

            if(units.Count() == 0)
            {
                var unitToInsert = new List<Unit>()
                {
                    new Unit() {CompanyId = id , UnitName = "كيلو" , UnitTypeId = db.UnitTypes.SingleOrDefault(t => t.UnitTypeName == "عدد عشري").UnitTypeId },
                    new Unit() {CompanyId = id , UnitName = "لتر" , UnitTypeId = db.UnitTypes.SingleOrDefault(t => t.UnitTypeName == "عدد عشري").UnitTypeId },
                    new Unit() {CompanyId = id , UnitName = "متر" , UnitTypeId = db.UnitTypes.SingleOrDefault(t => t.UnitTypeName == "عدد عشري").UnitTypeId },
                    new Unit() {CompanyId = id , UnitName = "جرام" , UnitTypeId = db.UnitTypes.SingleOrDefault(t => t.UnitTypeName == "عدد عشري").UnitTypeId },
                    new Unit() {CompanyId = id , UnitName = "وحدة" , UnitTypeId = db.UnitTypes.SingleOrDefault(t => t.UnitTypeName == "عدد صحيح").UnitTypeId },
                    new Unit() {CompanyId = id , UnitName = "اوقية" , UnitTypeId = db.UnitTypes.SingleOrDefault(t => t.UnitTypeName == "عدد صحيح").UnitTypeId },

                };
                db.Units.AddRange(unitToInsert);
                db.SaveChanges();
                units = (from i in db.Units
                         where i.CompanyId == id
                         select new UnitVM
                         {
                             UnitId = i.UnitId,
                             UnitName = i.UnitName,
                             UnitTypeId = i.UnitTypeId
                         }).ToList();
            }

            var types = (from i in db.TypeforItemGroups
                         select new TypeforItemGroupVM
                         {
                             typeId = i.TypeforItemGroupId,
                             typeName = i.TypeName,
                             show = i.show,
                             description = i.description,
                             value = i.Value
                         }).ToList();
            var unitNames = new NamesNUnit
            {
                Names = names,
                Units = units,
                propertyTypes = types
            };

            return Ok(unitNames);
        }

        [Route("GetitemsGroup/{id:int}")]
        public IHttpActionResult GetAllItems(Int32 id)
        {

            var returnedItems = this.AppCompanyUserManager.GetAllItems(id);

            return Ok(returnedItems);
        }

        //setInventoryValuationMethod
        [Route("setInventoryValuationMethod")]
        [HttpPost]
        public IHttpActionResult setInventoryValuationMethod(GetInventoryValuationMethodVm InventoryValuation)
        {
            var com = db.Companies.Find(InventoryValuation.companyId);
            if(com != null)
            {
                var checkforSubAccountsMoves = from i in db.SubAccountMovements
                                               where i.SubAccount.Account.AccountCategory.AccountsDataTypes.TypeName == "مخازن"
                                               && i.SubAccount.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == com.CompanyId
                                               select i;
                var counts = checkforSubAccountsMoves.Select(i => i.QuantityOut).DefaultIfEmpty(0).Sum();
                if(counts != 0)
                {
                    return BadRequest("لا يمكن تغير سياسة المخزون نظرا لوجود حركات مسجلة");

                }
                else
                {
                    var value = db.InventoryValuationMethods.Find(InventoryValuation.InventoryValuationMethodId);
                    if(value != null)
                    {
                        com.InventoryValuationMethodId = InventoryValuation.InventoryValuationMethodId;
                        db.Entry(com).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        return BadRequest("لا توجد سياسة مسجلة بهذا الاسم");

                    }
                }

            }else
            {
                return BadRequest();
            }
            return Ok();
        }
        [Route("updateItemPropertyValues")]
        [HttpPost]
        public IHttpActionResult updateItemPropertvalue(List<PropertyValuesforItemGroupVM> values)
        {
            try
            {
                var listofProp = new List<PropertyValuesforItemGroup>();
               
                //string realted = Guid.NewGuid().ToString();
                //do
                //{
                //    if (db.PropertyValuesforItemGroups.Any(a => a.PropertyValuesforItemGroupName == realted))
                //    {
                //        realted = Guid.NewGuid().ToString();
                //    }

                //} while (db.PropertyValuesforItemGroups.Any(a => a.PropertyValuesforItemGroupName == realted));
                foreach (var value in values)
                {
                    var propValue = db.PropertyValuesforItemGroups.Find(value.propertyValueId);
                    if(propValue == null)
                    {
                        if (value.isNew)
                        {
                            propValue = db.PropertyValuesforItemGroups.SingleOrDefault(a => a.uniqueId == value.uniqueId && a.parintType == value.typeName && a.PropertiesforItemGroupId == value.propertyId);
                            if(propValue == null)
                            {
                                return BadRequest("الصنف غير مسجل او غير قابل للتعديل");
                            }
                            value.propertyValueId = propValue.PropertyValuesforItemGroupId;
                        }
                        else
                        {
                            return BadRequest("الصنف غير مسجل او غير قابل للتعديل");
                        }
                    }

                    
                        if (propValue.PropertiesforItemGroup.Type.TypeName == "صنف")
                        {
                            if (string.IsNullOrEmpty(value.propertyValueName))
                            {
                                return BadRequest("يجب ادخال اسم للصنف");

                            }
                            var isExistes = db.PropertyValuesforItemGroups.Any(a => a.PropertyValuesforItemGroupName == value.propertyValueName && a.PropertyValuesforItemGroupId != value.propertyValueId && a.parintType == value.typeName);
                            var cc = db.PropertyValuesforItemGroups.Where(a => a.PropertyValuesforItemGroupName == value.propertyValueName && a.PropertyValuesforItemGroupId != value.propertyValueId);

                        if (!isExistes)
                            {
                               
                                propValue.PropertyValuesforItemGroupName = value.propertyValueName;
                                propValue.parintType = propValue.PropertiesforItemGroup.Type.TypeName;
                                propValue.Properties.SelectMany(c => c.TempItem.SubAccounts).ToList().ForEach(a => a.SubAccountName = value.propertyValueName);
                              
                             }
                            else
                            {
                                return BadRequest(string.Format("الاسم {0} مسجل لصنف اخر", value.propertyValueName));
                            }
                        }
                        else if (propValue.PropertiesforItemGroup.Type.TypeName == "كود")
                        {
                            if (string.IsNullOrWhiteSpace(value.propertyValueName))
                            {
                                return BadRequest("يجب ادخال كود للصنف");

                            }
                            var isExistes = db.PropertyValuesforItemGroups.Any(a => a.PropertyValuesforItemGroupName == value.propertyValueName && a.PropertyValuesforItemGroupId != value.propertyValueId && a.parintType == value.typeName);
                            if (!isExistes)
                            {
                                propValue.parintType = propValue.PropertiesforItemGroup.Type.TypeName;
                                propValue.PropertyValuesforItemGroupName = value.propertyValueName;
                            }
                            else
                            {
                                return BadRequest(string.Format("الكود {0} مسجل لصنف اخر", value.propertyValueName));
                            }
                        }
                        else if (propValue.PropertiesforItemGroup.Type.TypeName == "سعر")
                        {
                            if (string.IsNullOrWhiteSpace(value.propertyValueName) || string.IsNullOrEmpty(value.propertyValueName))
                            {
                                value.propertyValueName = "0";
                                propValue.parintType = propValue.PropertiesforItemGroup.Type.TypeName;
                            }

                            var isinteger =this.AppCompanyUserManager.CheckDataType(value.propertyValueName , "عدد عشري");
                            if (isinteger)
                            {
                               
                                propValue.PropertyValuesforItemGroupName = value.propertyValueName;
                                propValue.parintType = propValue.PropertiesforItemGroup.Type.TypeName;
                            }
                            else
                            {
                                return BadRequest(string.Format("نوع البيان ل {0} يجب ان يكون {1} فقط", propValue.PropertiesforItemGroup.PropertiesforItemGroupName , value.typeName));
                            }
                        }
                    else if (propValue.PropertiesforItemGroup.Type.TypeName == "حد الطلب")
                    {
                        if (string.IsNullOrWhiteSpace(value.propertyValueName) || string.IsNullOrEmpty(value.propertyValueName))
                        {
                            value.propertyValueName = "0";
                            propValue.parintType = propValue.PropertiesforItemGroup.Type.TypeName;
                        }

                        var isinteger = this.AppCompanyUserManager.CheckDataType(value.propertyValueName, "عدد عشري");
                        if (isinteger)
                        {

                            propValue.PropertyValuesforItemGroupName = value.propertyValueName;
                            propValue.parintType = propValue.PropertiesforItemGroup.Type.TypeName;
                        }
                        else
                        {
                            return BadRequest(string.Format("نوع البيان ل {0} يجب ان يكون {1} فقط", propValue.PropertiesforItemGroup.PropertiesforItemGroupName, value.typeName));
                        }
                    }
                    else if (propValue.PropertiesforItemGroup.Type.TypeName == "عدد صحيح" && !string.IsNullOrWhiteSpace(value.propertyValueName))
                        {
                           
                            var isinteger = this.AppCompanyUserManager.CheckDataType(value.propertyValueName , "عدد صحيح");
                            if (isinteger)
                            {
                             
                                propValue.PropertyValuesforItemGroupName =Convert.ToInt32(value.propertyValueName).ToString();
                                propValue.parintType = propValue.PropertiesforItemGroup.Type.TypeName;
                            }
                            else
                            {
                                return BadRequest(string.Format("نوع البيان ل {0} يجب ان يكون {1} فقط", propValue.PropertiesforItemGroup.PropertiesforItemGroupName, value.typeName));
                            }
                        }
                        else if (propValue.PropertiesforItemGroup.Type.TypeName == "عدد عشري" && !string.IsNullOrWhiteSpace(value.propertyValueName))
                        {
                            
                            var isinteger = this.AppCompanyUserManager.CheckDataType(value.propertyValueName , "عدد عشري");
                            if (isinteger)
                            {
                               
                                propValue.PropertyValuesforItemGroupName = (Convert.ToDecimal(value.propertyValueName)).ToString();
                                propValue.parintType = propValue.PropertiesforItemGroup.Type.TypeName;
                            }
                            else
                            {
                                return BadRequest(string.Format("نوع البيان ل {0} يجب ان يكون {1} فقط", propValue.PropertiesforItemGroup.PropertiesforItemGroupName, value.typeName));
                            }
                        }
                        else if (propValue.PropertiesforItemGroup.Type.TypeName == "مدة صلاحية" && !string.IsNullOrWhiteSpace(value.propertyValueName))
                        {

                            var isinteger = this.AppCompanyUserManager.CheckDataType( value.propertyValueName , "عدد صحيح");
                            if (isinteger)
                            {
                                
                                propValue.PropertyValuesforItemGroupName = (Convert.ToInt32(value.propertyValueName)).ToString();
                                propValue.parintType = propValue.PropertiesforItemGroup.Type.TypeName;
                            }
                            else
                            {
                                return BadRequest(string.Format("نوع البيان ل {0} يجب ان يكون {1} فقط", propValue.PropertiesforItemGroup.PropertiesforItemGroupName, value.typeName));
                            }
                        }
                        else
                        {
                            propValue.PropertyValuesforItemGroupName = value.propertyValueName;
                            propValue.PropertyValuesforItemGroupValue = value.value;
                            propValue.parintType = propValue.PropertiesforItemGroup.Type.TypeName;
                        }
                    propValue.uniqueId = null;

                    listofProp.Add(propValue);
                }

                listofProp.ForEach(a => db.Entry(a).State = EntityState.Modified);
                db.SaveChanges();
                var re = this.AppCompanyUserManager.GetAllItems(listofProp[0].PropertiesforItemGroup.ItemGroupId);
                this.AppCompanyUserManager.setTempItems(re.CompanyId , re.ItemGroupId);
                return Ok(re);

            }
            catch 
            {

                return BadRequest("حدث خطاء اثناء حفظ البيانات - لمزيد من المعلومات تواصل مع المدراء");

            }

        }

        [Route("saveItemPropertyValues/{id:int}")]
        [HttpGet]
        public IHttpActionResult SaveItem(Int32 id)
        {
            try
            {
                var itemgroup = db.ItemGroups.Find(id);
                //var listsubNprop = new List<subAccountNpropertyValues>();
                if (itemgroup != null)
                {
                    var listPropertyvalues = new List<PropertyValuesforItemGroup>();
                    string realted = Guid.NewGuid().ToString();
                    do
                    {
                        if(db.PropertyValuesforItemGroups.Any(a => a.PropertyValuesforItemGroupName == realted))
                        {
                            realted = Guid.NewGuid().ToString();
                        }
                     
                    } while (db.PropertyValuesforItemGroups.Any(a => a.PropertyValuesforItemGroupName == realted));

                    foreach (var prop in itemgroup.Properties)
                    {
                        if (prop.Type.TypeName == "صنف")
                        {
                            string itemName = "صنف جديد - " + this.AppCompanyUserManager.GetUniqueKey(4);
                            do
                            {
                                itemName = "صنف جديد - " + this.AppCompanyUserManager.GetUniqueKey(4);
                            } while (db.PropertiesforItemGroups.Any(a => a.PropertiesforItemGroupName == "الصنف" && a.ItemGroup.CompanyId == itemgroup.CompanyId && a.PropertyValuesforItemGroup.Any(b => b.PropertyValuesforItemGroupName == itemName)));
                            var propertValue = new PropertyValuesforItemGroup
                            {

                                PropertiesforItemGroupId = prop.PropertiesforItemGroupId,
                                PropertyValuesforItemGroupName = itemName, Realted = realted,parintType = prop.Type.TypeName
                                
                            };
                           
                            foreach (var ac in itemgroup.Accounts)
                            {
                                //var subAccount = new SubAccount() { SubAccountName = itemName,
                                //                                  AccountId = ac.AccountId,
                                //                                  Activated = true,Realted = realted
                                //                                   };
                           
                            }

                            //propertValue.subAccountNpropertyValues = listsubNprop;

                            listPropertyvalues.Add(propertValue);
                          
                        }
                        else if (prop.Type.TypeName == "كود")
                        {
                            string itemName = this.AppCompanyUserManager.GetUniqueKey(4);
                            do
                            {
                                itemName = this.AppCompanyUserManager.GetUniqueKey(4);
                            } while (db.PropertiesforItemGroups.Any(a => a.PropertiesforItemGroupName == "الكود" && a.ItemGroup.CompanyId == itemgroup.CompanyId && a.PropertyValuesforItemGroup.Any(b => b.PropertyValuesforItemGroupName == itemName)));
                            var propertValue = new PropertyValuesforItemGroup
                            {

                                PropertiesforItemGroupId = prop.PropertiesforItemGroupId,
                                PropertyValuesforItemGroupName = itemName,
                                Realted = realted,
                                parintType = prop.Type.TypeName
                            };
                            listPropertyvalues.Add(propertValue);
                        }
                        else if (prop.Type.TypeName == "سعر")
                        {
                            var propertValue = new PropertyValuesforItemGroup
                            {

                                PropertiesforItemGroupId = prop.PropertiesforItemGroupId,
                                PropertyValuesforItemGroupName = "0.00",
                                Realted = realted,
                                parintType = prop.Type.TypeName
                            };
                            listPropertyvalues.Add(propertValue);
                        }
                        else if (prop.Type.TypeName == "حد الطلب")
                        {
                            var propertValue = new PropertyValuesforItemGroup
                            {

                                PropertiesforItemGroupId = prop.PropertiesforItemGroupId,
                                PropertyValuesforItemGroupName = "0",
                                Realted = realted,
                                parintType = prop.Type.TypeName
                            };
                            listPropertyvalues.Add(propertValue);
                        }
                        else if (prop.Type.TypeName == "مدة صلاحية")
                        {
                            var propertValue = new PropertyValuesforItemGroup
                            {

                                PropertiesforItemGroupId = prop.PropertiesforItemGroupId,
                                PropertyValuesforItemGroupName = "0",
                                Realted = realted,
                                parintType = prop.Type.TypeName
                            };
                            listPropertyvalues.Add(propertValue);
                        }
                        else
                        {
                            var propertValue = new PropertyValuesforItemGroup
                            {

                                PropertiesforItemGroupId = prop.PropertiesforItemGroupId,
                                PropertyValuesforItemGroupName = "",
                                Realted = realted,
                                parintType = prop.Type.TypeName
                            };
                            listPropertyvalues.Add(propertValue);
                        }
                    }
                    db.PropertyValuesforItemGroups.AddRange(listPropertyvalues);
                  
                    db.SaveChanges();
                    this.AppCompanyUserManager.setTempItems(itemgroup.CompanyId , itemgroup.ItemGroupId);
                }
                else
                {
                    return BadRequest("لا توجد فئة مسجلة بهذا الاسم");
                }
                var re = this.AppCompanyUserManager.GetAllItems(itemgroup.ItemGroupId);

                return Ok(re);
            }
            catch 
            {

                return BadRequest("حدث خطاء اثناء حفظ البيانات - لمزيد من المعلومات تواصل مع المدراء");

            }

        }

        [Route("saveItemPropertyValueInputs")]
        [HttpPost]
        public IHttpActionResult SaveItemfromInputs(List<ItemInputVm> items)
        {
            try
            {
                var id = items.FirstOrDefault().itemGroupId;
                var itemgroup = db.ItemGroups.Find(id);
                //var listsubNprop = new List<subAccountNpropertyValues>();
                if(itemgroup == null)
                {
                    return BadRequest("لا توجد فئة مسجلة بهذا الاسم");
                }
               
                string realted = Guid.NewGuid().ToString();
                do
                {
                    if(db.PropertyValuesforItemGroups.Any(a => a.PropertyValuesforItemGroupName == realted))
                    {
                            realted = Guid.NewGuid().ToString();
                    }
                
                } while (db.PropertyValuesforItemGroups.Any(a => a.PropertyValuesforItemGroupName == realted));
                items.ForEach(a => { a.uniqueId = realted; a.isNew = true; });
                if (!string.IsNullOrEmpty(realted))
                {
                    new System.Threading.Tasks.Task(() =>
                    {
                        var listPropertyvalues = new List<PropertyValuesforItemGroup>();


                        foreach (var prop in items)
                        {
                            if (prop.type.TypeName == "صنف")
                            {
                                string itemName = prop.propertyValues;
                                if(string.IsNullOrEmpty(itemName) || string.IsNullOrWhiteSpace(itemName))
                                {
                                    itemName = "صنف جديد";
                                }
                                do
                                {
                                    if (db.PropertiesforItemGroups.Any(a => a.PropertiesforItemGroupName == "الصنف" && a.ItemGroup.CompanyId == itemgroup.CompanyId && a.PropertyValuesforItemGroup.Any(b => b.PropertyValuesforItemGroupName == itemName)))
                                    {
                                        itemName = prop.propertyValues + " " + this.AppCompanyUserManager.GetUniqueKey(3);

                                    }
                                } while (db.PropertiesforItemGroups.Any(a => a.PropertiesforItemGroupName == "الصنف" && a.ItemGroup.CompanyId == itemgroup.CompanyId && a.PropertyValuesforItemGroup.Any(b => b.PropertyValuesforItemGroupName == itemName)));
                                var propertValue = new PropertyValuesforItemGroup
                                {

                                    PropertiesforItemGroupId = prop.propertyId,
                                    PropertyValuesforItemGroupName = itemName,
                                    Realted = realted,
                                    parintType = prop.type.TypeName,
                                    uniqueId = realted,

                                };

                                foreach (var ac in itemgroup.Accounts)
                                {
                                    var subAccount = new SubAccount()
                                    {
                                        SubAccountName = itemName,
                                        AccountId = ac.AccountId,
                                        Activated = true,
                                      
                                        

                                    };
                                    //var subNprop = new subAccountNpropertyValues()
                                    //{
                                    //    PropertyValuesforItemGroupId = propertValue.PropertyValuesforItemGroupId,
                                    //    SubAccount = subAccount,
                                    //};
                                    //listsubNprop.Add(subNprop);
                                }

                                //propertValue.subAccountNpropertyValues = listsubNprop;

                                //listPropertyvalues.Add(propertValue);

                            }
                            else if (prop.type.TypeName == "كود")
                            {
                                string itemName = prop.propertyValues;
                                if (string.IsNullOrEmpty(prop.propertyValues) || string.IsNullOrWhiteSpace(prop.propertyValues))
                                {
                                    itemName = this.AppCompanyUserManager.GetUniqueKey(4);
                                }

                                do
                                {
                                    if (db.PropertiesforItemGroups.Any(a => a.PropertiesforItemGroupName == "الكود" && a.ItemGroup.CompanyId == itemgroup.CompanyId && a.PropertyValuesforItemGroup.Any(b => b.PropertyValuesforItemGroupName == itemName)))
                                    {
                                        itemName = prop.propertyValues + " " + this.AppCompanyUserManager.GetUniqueKey(3);
                                    }

                                } while (db.PropertiesforItemGroups.Any(a => a.PropertiesforItemGroupName == "الكود" && a.ItemGroup.CompanyId == itemgroup.CompanyId && a.PropertyValuesforItemGroup.Any(b => b.PropertyValuesforItemGroupName == itemName)));
                                var propertValue = new PropertyValuesforItemGroup
                                {

                                    PropertiesforItemGroupId = prop.propertyId,
                                    PropertyValuesforItemGroupName = itemName,
                                    Realted = realted,
                                    parintType = prop.type.TypeName,
                                    uniqueId = realted,
                                };
                                listPropertyvalues.Add(propertValue);
                            }
                            else if (prop.type.TypeName == "سعر")
                            {
                                var isinteger = this.AppCompanyUserManager.CheckDataType(prop.propertyValues, "عدد عشري");
                                if (!isinteger)
                                {
                                    prop.propertyValues = "99.9";
                                }
                                var propertValue = new PropertyValuesforItemGroup
                                {

                                    PropertiesforItemGroupId = prop.propertyId,
                                    PropertyValuesforItemGroupName = prop.propertyValues,
                                    Realted = realted,
                                    parintType = prop.type.TypeName,
                                    uniqueId = realted,
                                };
                                listPropertyvalues.Add(propertValue);
                            }
                            else if (prop.type.TypeName == "حد الطلب")
                            {
                                var isinteger = this.AppCompanyUserManager.CheckDataType(prop.propertyValues, "عدد عشري");
                                if (!isinteger)
                                {
                                    prop.propertyValues = "0";
                                }
                                var propertValue = new PropertyValuesforItemGroup
                                {

                                    PropertiesforItemGroupId = prop.propertyId,
                                    PropertyValuesforItemGroupName = prop.propertyValues,
                                    Realted = realted,
                                    parintType = prop.type.TypeName,
                                    uniqueId = realted,
                                };
                                listPropertyvalues.Add(propertValue);
                            }
                            else if (prop.type.TypeName == "مدة صلاحية")
                            {
                                var isinteger = this.AppCompanyUserManager.CheckDataType(prop.propertyValues, "عدد صحيح");
                                if (!isinteger)
                                {
                                    prop.propertyValues = "0";
                                }
                                var propertValue = new PropertyValuesforItemGroup
                                {

                                    PropertiesforItemGroupId = prop.propertyId,
                                    PropertyValuesforItemGroupName = prop.propertyValues,
                                    Realted = realted,
                                    parintType = prop.type.TypeName,
                                    uniqueId = realted,
                                };
                                listPropertyvalues.Add(propertValue);
                            }
                            else if (prop.type.TypeName == "عدد صحيح")
                            {
                                var isinteger = this.AppCompanyUserManager.CheckDataType(prop.propertyValues, "عدد صحيح");
                                if (!isinteger)
                                {
                                    prop.propertyValues = "0";
                                }
                                var propertValue = new PropertyValuesforItemGroup
                                {

                                    PropertiesforItemGroupId = prop.propertyId,
                                    PropertyValuesforItemGroupName = prop.propertyValues,
                                    Realted = realted,
                                    parintType = prop.type.TypeName,
                                    uniqueId = realted,
                                };
                                listPropertyvalues.Add(propertValue);
                            }
                            else if (prop.type.TypeName == "عدد عشري")
                            {
                                var isinteger = this.AppCompanyUserManager.CheckDataType(prop.propertyValues, "عدد عشري");
                                if (!isinteger)
                                {
                                    prop.propertyValues = "0.00";
                                }
                                var propertValue = new PropertyValuesforItemGroup
                                {

                                    PropertiesforItemGroupId = prop.propertyId,
                                    PropertyValuesforItemGroupName = prop.propertyValues,
                                    Realted = realted,
                                    parintType = prop.type.TypeName,
                                    uniqueId = realted,
                                };
                                listPropertyvalues.Add(propertValue);
                            }
                            else
                            {
                                var propertValue = new PropertyValuesforItemGroup
                                {

                                    PropertiesforItemGroupId = prop.propertyId,
                                    PropertyValuesforItemGroupName = "",
                                    Realted = realted,
                                    parintType = prop.type.TypeName,
                                    uniqueId = realted,
                                };
                                listPropertyvalues.Add(propertValue);
                            }
                        }
                        db.PropertyValuesforItemGroups.AddRange(listPropertyvalues);
                        db.SaveChanges();
                        this.AppCompanyUserManager.setTempItems(itemgroup.CompanyId , itemgroup.ItemGroupId);
                    }).Start();
                }
                if (string.IsNullOrEmpty(realted))
                {
                    return BadRequest("لا يمكن اتمام العملية الان");
                }
                return Ok(realted);
            }
            catch
            {

                return BadRequest("حدث خطاء اثناء حفظ البيانات - لمزيد من المعلومات تواصل مع المدراء");

            }

        }

        [Route("deleteFromItemGroup")]
        [HttpPost]
        public IHttpActionResult deleteFromItemGroup(delFromitemGroup del)
        {

            Int32 comId = 0;
            Int32 gid = 0;
            if(del.TypeName == "itemGroup")
            {
                var toDel = db.ItemGroups.Find(del.AccountId);
                if(toDel != null)
                {
                    comId = toDel.CompanyId;
                    gid = toDel.ItemGroupId;
                    var ItemstoRemove = db.TempItems.Where(a => a.CompanyId == comId && a.ItemGroupId == gid);
                    if (ItemstoRemove != null && ItemstoRemove.Count() > 0)
                    {
                        db.TempItems.RemoveRange(ItemstoRemove);

                    }
                    db.ItemGroups.Remove(toDel);
                   
                }

            }
            else if (del.TypeName == "property")
            {
                var toDel = db.PropertiesforItemGroups.Find(del.AccountId);
                comId = toDel.ItemGroup.CompanyId;
                gid = toDel.ItemGroupId;
                if (toDel != null)
                {
                   if(toDel.PropertiesforItemGroupName != "الصنف" || toDel.PropertiesforItemGroupName != "الكود" || toDel.PropertiesforItemGroupName != "السعر" || toDel.PropertiesforItemGroupName != "حد الطلب")
                    {
                      if(!  toDel.PropertyValuesforItemGroup.Any(a => !string.IsNullOrEmpty(a.PropertyValuesforItemGroupName)))
                        {
                            var ItemstoRemove = db.TempItems.Where(a => a.CompanyId == comId && a.ItemGroupId == gid);
                            if (ItemstoRemove != null && ItemstoRemove.Count() > 0)
                            {
                                db.TempItems.RemoveRange(ItemstoRemove);

                            }
                            db.PropertyValuesforItemGroups.RemoveRange(toDel.PropertyValuesforItemGroup);
                            db.PropertiesforItemGroups.Remove(toDel);

                        }
                        else
                        {
                            return BadRequest();
                        }
                       
                    }

                }
            }
            else if (del.TypeName == "accounts")
            {
                var itemgroup = db.ItemGroups.Find(del.ItemGroupId);
                comId = itemgroup.CompanyId;
                gid = itemgroup.ItemGroupId;
                var toDel = db.Accounts.SingleOrDefault(a=> a.AccountId == del.AccountId
                && a.AccountCategory.AccountsDataTypes.aliasName == "inventory" && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == itemgroup.CompanyId);
                if (toDel != null && itemgroup != null)
                {
                    var accforitem = (from c in db.AccountsforItemGroups
                                      where c.AccountId == toDel.AccountId && c.ItemGroupId == itemgroup.ItemGroupId
                                      select c).SingleOrDefault();

                    //var subAccountN = (from a in db.subAccountNpropertyValues
                    //                   where a.SubAccount.Account.AccountId == toDel.AccountId
                    //                   && a.PropertyValuesforItemGroup.PropertiesforItemGroup.ItemGroupId == itemgroup.ItemGroupId
                    //                   select a).ToList();
                    //var subacc = new List<SubAccount>();
                    //foreach (var item in subAccountN)
                    //{
                    //    subacc.Add(item.SubAccount);
                    //}

                    var findtrnstAcc = db.Accounts.Where(a => a.AccountCategory.AccountsDataTypes.aliasName == "goods-in-transit" && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == itemgroup.CompanyId && a.BranchId == toDel.BranchId).FirstOrDefault();

                    //if (findtrnstAcc != null)
                    //{
                    //    var accforitemtr = (from c in db.AccountsforItemGroups
                    //                      where c.AccountId == findtrnstAcc.AccountId && c.ItemGroupId == itemgroup.ItemGroupId
                    //                      select c).SingleOrDefault();

                    //    var subAccountNtr = (from a in db.subAccountNpropertyValues
                    //                       where a.SubAccount.Account.AccountId == findtrnstAcc.AccountId
                    //                       && a.PropertyValuesforItemGroup.PropertiesforItemGroup.ItemGroupId == itemgroup.ItemGroupId
                    //                       select a).ToList();
                    //    var subacctr = new List<SubAccount>();
                    //    foreach (var item in subAccountNtr)
                    //    {
                    //        subacctr.Add(item.SubAccount);
                    //    }
                    //    if(accforitemtr != null && subAccountNtr != null && subacctr != null)
                    //    {
                    //        db.subAccountNpropertyValues.RemoveRange(subAccountNtr);
                    //        db.SubAccounts.RemoveRange(subacctr);
                    //        db.AccountsforItemGroups.Remove(accforitemtr);
                    //    }
                      
                    //}
                    var ItemstoRemove = db.TempItems.Where(a => a.CompanyId == comId && a.ItemGroupId == gid);
                    if (ItemstoRemove != null && ItemstoRemove.Count() > 0)
                    {
                        db.TempItems.RemoveRange(ItemstoRemove);

                    }
                    //db.subAccountNpropertyValues.RemoveRange(subAccountN);
                    //db.SubAccounts.RemoveRange(subacc);
                    //db.AccountsforItemGroups.Remove(accforitem);
                   
                };
            }
            else if (del.TypeName == "propertyValue")
            {
                var listPropValues = new List<PropertyValuesforItemGroup>();
                foreach (var propValue in del.propertyValues)
                {
                    var pV = db.PropertyValuesforItemGroups.Find(propValue.propertyValueId);
                    comId = pV.PropertiesforItemGroup.ItemGroup.CompanyId;
                    gid = pV.PropertiesforItemGroup.ItemGroupId;
                    if (propValue.typeName == "صنف")
                    {
                       
                        //pV.subAccountNpropertyValues.ForEach(a => db.Entry(a.SubAccount).State = EntityState.Deleted);
                       
                    }
                            
                    listPropValues.Add(pV);
                  
                }
                var ItemstoRemove = db.TempItems.Where(a => a.CompanyId == comId && a.ItemGroupId == gid);
                if (ItemstoRemove != null && ItemstoRemove.Count() > 0)
                {
                    db.TempItems.RemoveRange(ItemstoRemove);

                }
                db.PropertyValuesforItemGroups.RemoveRange(listPropValues);
            }

            db.SaveChanges();
            this.AppCompanyUserManager.setTempItems(comId , gid);
            var firstitemgroup = db.ItemGroups.Find(del.ItemGroupId);
            if(firstitemgroup == null)
            {
               firstitemgroup = db.ItemGroups.FirstOrDefault();

                var re = this.AppCompanyUserManager.GetAllItems(firstitemgroup.ItemGroupId);
                return Ok(re);
            }
            else
            {
                var re = this.AppCompanyUserManager.GetAllItems(firstitemgroup.ItemGroupId);
                return Ok(re);
            }
           
        }

        [Route("UpdateItemGroupProperty")]
        [HttpPost]
        public IHttpActionResult UpdateItemGroupProperty(PropertiesforItemGroupVM property)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var prop = db.PropertiesforItemGroups.Find(property.propertyId);

                if (prop != null)
                {
                    if(prop.PropertyValuesforItemGroup.Any(a => a.PropertyValuesforItemGroupName.Length > 1))
                    {
                        return BadRequest("يجب حذف جميع القيم المتعلقة بالخاصية اولا");
                    }
                    if(db.PropertiesforItemGroups.Any(a => a.PropertiesforItemGroupName == property.propertyName && a.ItemGroupId == prop.ItemGroupId && a.PropertiesforItemGroupId != prop.PropertiesforItemGroupId))
                    {
                        return BadRequest("يوجد خاصية بنفس الاسم مسجلة من قبل");
                    }
                    prop.PropertiesforItemGroupName = property.propertyName;
                    prop.TypeforItemGroupId = property.Type.typeId;
                   
                    db.Entry(prop).State = EntityState.Modified;
                    db.SaveChanges();

                }
                else
                {
                    return BadRequest("الخاصية غير مسجلة");
                }
                // var re = this.AppCompanyUserManager.GetAllItems(prop.ItemGroupId);
                this.AppCompanyUserManager.setTempItems(prop.ItemGroup.CompanyId , prop.ItemGroupId);
                return Ok();
            }
            catch 
            {
                return BadRequest("حدث خطاء اثناء حفظ البيانات - لمزيد من المعلومات تواصل مع المدراء");
            }
        }

        [Route("saveItemGroupProperty/{id:int}")]
        [HttpGet]
        public IHttpActionResult saveItemGroupProperty(Int32  id)
        {
            try
            {


                var item = db.ItemGroups.Find(id);
               
                if (item != null )
                {
                    string name = "خاصية جديدة - " + this.AppCompanyUserManager.GetUniqueKey(2);
                    do
                    {
                        name = "خاصية جديدة - " + this.AppCompanyUserManager.GetUniqueKey(2);
                    } while (item.Properties.Any(a => a.PropertiesforItemGroupName == name));

                    var prop = new PropertiesforItemGroup
                    {
                        ItemGroupId = id,
                        PropertiesforItemGroupName = name,
                        Show = true,
                        TypeforItemGroupId = db.TypeforItemGroups.SingleOrDefault(a => a.TypeName == "نص").TypeforItemGroupId,
                        orderNum =id

                    };
                    var listprop = new List<PropertyValuesforItemGroup>();
                    foreach (var value in item.Properties[0].PropertyValuesforItemGroup)
                    {
                        var propToAdd = new PropertyValuesforItemGroup
                        {
                            PropertyValuesforItemGroupName = "",orderNum=0,Realted = item.Properties[0].PropertyValuesforItemGroup[0].Realted,
                            parintType = db.TypeforItemGroups.SingleOrDefault(a => a.TypeName == "نص").TypeName

                        };
                        listprop.Add(propToAdd);
                    }
                    prop.PropertyValuesforItemGroup=listprop;
                    db.PropertiesforItemGroups.Add(prop);
                    db.SaveChanges();

                }
                else
                {
                    return BadRequest("الفئة غير مسجلة");
                }
                var re = this.AppCompanyUserManager.GetAllItems(id);

                return Ok(re);
            }
            catch
            {

                return BadRequest("حدث خطاء اثناء حفظ البيانات - لمزيد من المعلومات تواصل مع المدراء");

            }
        }

        [Route("updateItemGroupUnit")]
        [HttpPost]
        public IHttpActionResult updateItemGroupunit(GetitemsGroupUnit itemGroup)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var item = db.ItemGroups.Find(itemGroup.itemGroupId);
                var unit = db.Units.SingleOrDefault(u => u.UnitId == itemGroup.unitId && u.CompanyId == item.CompanyId);

                if (item != null && unit != null)
                {

                        item.UnitId = itemGroup.unitId;
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();

                }
                else
                {
                    return BadRequest("الفئة او وحدة القياس غير مسجلة");
                }

                return Ok();
            }
            catch 
            {

                return BadRequest("حدث خطاء اثناء حفظ البيانات - لمزيد من المعلومات تواصل مع المدراء");

            }
        }


        [Route("updateItemGroupName")]
        [HttpPost]
        public  IHttpActionResult updateItemGroupName(GetitemsGroupNames itemGroup)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                string uid = RequestContext.Principal.Identity.GetUserId();
                var item = db.ItemGroups.Find(itemGroup.itemGroupId);
                var company = db.Companies.SingleOrDefault(a => a.CompanyId == item.CompanyId && a.ApplicationUsersComapnies.Any(u => u.ApplicationUserId == uid));
                if(company == null)
                {
                    return BadRequest("الشركة غير مسجلة او غير متاحة لهذه العملية");
                }

                if (item != null)
                {
                    var itemExistes = db.ItemGroups.Any(a => a.ItemGroupName == itemGroup.itemGroupName && a.ItemGroupId != item.ItemGroupId && a.CompanyId == company.CompanyId);
                    if (!itemExistes)
                    {
                        item.ItemGroupName = itemGroup.itemGroupName;
                        db.Entry(item).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {
                        return BadRequest("اسم الفئة الذي تحاول تسجيلة مسجل لفئة اخري ");
                    }

                }else
                {
                    return BadRequest("لا يمكن حفظ البيانات الفئة غير مسجلة");

                }

                return Ok();
            }
            catch (Exception)
            {

                return BadRequest("حدث خطاء اثناء حفظ البيانات - لمزيد من المعلومات تواصل مع المدراء");

            }
        }

        [Route("saveItemGroup/{id:int}")]
        [HttpGet]
        public IHttpActionResult saveItemGroup(Int32 id)
        {
            try
            {
                string realted = Guid.NewGuid().ToString();
                do
                {
                    if (db.PropertyValuesforItemGroups.Any(a => a.PropertyValuesforItemGroupName == realted))
                    {
                        realted = Guid.NewGuid().ToString();
                    }

                } while (db.PropertyValuesforItemGroups.Any(a => a.PropertyValuesforItemGroupName == realted));

                string name = "فئة جديدة - " + this.AppCompanyUserManager.GetUniqueKey(2);
                    do
                    {
                        name = "فئة جديدة - " + this.AppCompanyUserManager.GetUniqueKey(2);
                    } while (db.ItemGroups.Any(a => a.ItemGroupName == name && a.CompanyId == id));

                    string itemName = "صنف جديد - " + this.AppCompanyUserManager.GetUniqueKey(4);
                    do
                    {
                        itemName = "صنف جديد - " + this.AppCompanyUserManager.GetUniqueKey(4);
                    } while (db.PropertiesforItemGroups.Any(a => a.PropertiesforItemGroupName == "الصنف" && a.ItemGroup.CompanyId == id && a.PropertyValuesforItemGroup.Any(b=> b.PropertyValuesforItemGroupName == itemName)));

                string itemCode = this.AppCompanyUserManager.GetUniqueKey(4);
                do
                {
                    itemCode = this.AppCompanyUserManager.GetUniqueKey(4); ;
                } while (db.PropertiesforItemGroups.Any(a => a.PropertiesforItemGroupName == "الكود" && a.ItemGroup.CompanyId == id && a.PropertyValuesforItemGroup.Any(b => b.PropertyValuesforItemGroupName == itemCode)));

                var addnewItemGroup = new ItemGroup
                    {
                        ItemGroupName = name,
                        Properties = new List<PropertiesforItemGroup>()
                        {
                            new PropertiesforItemGroup {PropertiesforItemGroupName = "الصنف", orderNum = 1 , TypeforItemGroupId = db.TypeforItemGroups.SingleOrDefault(t => t.TypeName == "صنف").TypeforItemGroupId , Show = false ,
                                PropertyValuesforItemGroup = new List<PropertyValuesforItemGroup> () { new PropertyValuesforItemGroup { PropertyValuesforItemGroupName = itemName , Realted = realted , parintType = "صنف" } } },
                            new PropertiesforItemGroup {PropertiesforItemGroupName = "الكود" , orderNum = 2 ,  TypeforItemGroupId = db.TypeforItemGroups.SingleOrDefault(t => t.TypeName == "كود").TypeforItemGroupId , Show = false ,
                             PropertyValuesforItemGroup = new List<PropertyValuesforItemGroup> () { new PropertyValuesforItemGroup { PropertyValuesforItemGroupName = itemCode, Realted = realted, parintType = "كود" } } },
                            new PropertiesforItemGroup {PropertiesforItemGroupName = "السعر" , orderNum = 3 ,  TypeforItemGroupId = db.TypeforItemGroups.SingleOrDefault(t => t.TypeName == "سعر").TypeforItemGroupId , Show = false,
                             PropertyValuesforItemGroup = new List<PropertyValuesforItemGroup> () { new PropertyValuesforItemGroup { PropertyValuesforItemGroupName = "0.0", Realted = realted, parintType = "سعر" } } },
                             new PropertiesforItemGroup {PropertiesforItemGroupName = "حد الطلب" , orderNum = 4 ,  TypeforItemGroupId = db.TypeforItemGroups.SingleOrDefault(t => t.TypeName == "حد الطلب").TypeforItemGroupId , Show = false,
                             PropertyValuesforItemGroup = new List<PropertyValuesforItemGroup> () { new PropertyValuesforItemGroup { PropertyValuesforItemGroupName = "0", Realted = realted, parintType = "حد الطلب" } } },
                        },
                        UnitId = db.Units.FirstOrDefault().UnitId,
                        CompanyId = id,
                       
                       
                    };
                db.ItemGroups.Add(addnewItemGroup);
                db.SaveChanges();
                
                    var re = this.AppCompanyUserManager.GetAllItems(addnewItemGroup.ItemGroupId);
                    this.AppCompanyUserManager.setTempItems(re.CompanyId , re.ItemGroupId);
                    return Ok(re);
                
            }
            catch (Exception)
            {

                return BadRequest("حدث خطاء اثناء حفظ البيانات - لمزيد من المعلومات تواصل مع المدراء");

            }

        }

        [Route("saveItemGroupAccounts")]
        [HttpPost]
        public IHttpActionResult saveItemGroupAccounts(List<accountsForItemGroup> accounts)
        {
            return Ok();
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                string uid = RequestContext.Principal.Identity.GetUserId();

                foreach (var account in accounts)
                {
                    var acc = db.Accounts.Find(account.AccountId);
                    if(acc == null)
                    {
                        return BadRequest("يوجد خطاء في الحسابات المرتبطة");
                    }
                    var trnsitAccountCat = db.AccountCategories.Where(a => a.AccountsDataTypes.aliasName == "goods-in-transit" && a.BalanceSheetType.BalanceSheet.CompanyId == acc.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId).FirstOrDefault();
                    if(trnsitAccountCat == null)
                    {
                       
                        trnsitAccountCat = new AccountCategory()
                        {
                            AccountCategoryName = "بضاعة بالطريق",
                            BalanceSheetTypeId = acc.AccountCategory.BalanceSheetTypeId,
                            AccountsDataTypesId = db.AccountsDataTypes.SingleOrDefault(a => a.aliasName == "goods-in-transit").Id,
                            AccountTypeId = acc.AccountCategory.AccountTypeId,
                            CurrentWorkerId = uid,
                            
                        };
                        db.AccountCategories.Add(trnsitAccountCat);
                        db.SaveChanges();

                    };
                    var transitAccount = trnsitAccountCat.Accounts.Where(a => a.BranchId == acc.BranchId && a.AccountCategory.AccountsDataTypes.aliasName == "goods-in-transit").FirstOrDefault();
                    if(transitAccount == null)
                    {
                        var catName = "";
                        if (acc.BranchId == null)
                        {
                            catName = "بضاعة بالطريق" + " - " + db.Companies.Find(acc.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId).CompanyName;
                        }
                        else
                        {
                            catName = "بضاعة بالطريق" + " - " + db.Branchs.Find(acc.BranchId).BranchName;
                        }

                        transitAccount = new Account()
                        {
                            AccountCategoryId = trnsitAccountCat.AccountCategoryId,
                            AccountName = catName,
                            BranchId = acc.BranchId,
                            Activated = true
                        };
                        db.Accounts.Add(transitAccount);
                        db.SaveChanges();
                        this.AppCompanyUserManager.setAccountsCodes(transitAccount.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId.GetValueOrDefault());
                    }

                    if (acc.AccountCategory.AccountsDataTypes.TypeName == "مخازن" && account.Value == true)
                    {
                        if(!db.AccountsforItemGroups.Any(a => a.AccountId == account.AccountId && a.ItemGroupId == account.code))
                        {
                            AccountsforItemGroup itemgroupaccounts = new AccountsforItemGroup
                            {
                                AccountId = account.AccountId,
                                ItemGroupId = account.code

                            };
                            db.AccountsforItemGroups.Add(itemgroupaccounts);
                            db.SaveChanges();
                            var itemGroup = db.ItemGroups.Find(account.code);
                            var prop = itemGroup.Properties.SingleOrDefault(p => p.PropertiesforItemGroupName == "الصنف");
                            foreach (var value in prop.PropertyValuesforItemGroup)
                            {
                               
                               var subaccount = new SubAccount
                                {
                                    AccountId = acc.AccountId,
                                    SubAccountName = value.PropertyValuesforItemGroupName,
                                    CurrentWorkerId = uid,
                                  
                                };
                                
                                db.SubAccounts.Add(subaccount);
                                db.SaveChanges();
                                //var subproperty = new subAccountNpropertyValues
                                //{
                                //    SubAccountId = subaccount.SubAccountId,
                                //    PropertyValuesforItemGroupId = value.PropertyValuesforItemGroupId
                                //};
                              
                                //db.subAccountNpropertyValues.Add(subproperty);
                                //db.SaveChanges();

                            }
                        }

                        if (!db.AccountsforItemGroups.Any(a => a.AccountId == transitAccount.AccountId && a.ItemGroupId == account.code))
                        {
                            AccountsforItemGroup itemgroupaccounts = new AccountsforItemGroup
                            {
                                AccountId = transitAccount.AccountId,
                                ItemGroupId = account.code

                            };
                            db.AccountsforItemGroups.Add(itemgroupaccounts);
                            db.SaveChanges();
                            var itemGroup = db.ItemGroups.Find(account.code);
                            var prop = itemGroup.Properties.SingleOrDefault(p => p.PropertiesforItemGroupName == "الصنف");
                            foreach (var value in prop.PropertyValuesforItemGroup)
                            {
                                
                                var subaccounttr = new SubAccount
                                {
                                    AccountId = transitAccount.AccountId,
                                    SubAccountName = value.PropertyValuesforItemGroupName,
                                    CurrentWorkerId = uid,
                                
                                };
                              
                                db.SubAccounts.Add(subaccounttr);
                                db.SaveChanges();
                                //var subpropertytr = new subAccountNpropertyValues
                                //{
                                //    SubAccountId = subaccounttr.SubAccountId,
                                //    PropertyValuesforItemGroupId = value.PropertyValuesforItemGroupId
                                //};
                                //db.subAccountNpropertyValues.Add(subpropertytr);
                                //db.SaveChanges();

                            }
                        }
                    }
                }
                var re = this.AppCompanyUserManager.GetAllItems(accounts[0].code).Accounts;
               
                return Ok(re);
            }
            catch (Exception)
            {

                return BadRequest("حدث خطاء اثناء حفظ البيانات - لمزيد من المعلومات تواصل مع المدراء");

            }
        }


    }

}

