﻿using Inventory.DataLayer;
using Inventory.Managers;
using Inventory.Models;
using Inventory.ViewModels;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Inventory.Controllers
{
    [RoutePrefix("api/OrderGroup")]
    public class OrderGroupController : BaseApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private OrderGroupManger OrderGroupManger;
        public OrderGroupController()
        {
            this.OrderGroupManger = new OrderGroupManger(this.db);
        }
        [Route("getOrderGroupNames")]
        [HttpPost]
        public IHttpActionResult GetOrderGroupNames(isCompanyBranch companyBranch)
        {

            if (companyBranch.IsCompany)
            {
                var group = (from c in db.OrderGroup
                             where c.IsCompany == true
                             && c.CompanyBranchId == companyBranch.Id
                             select new orderGroupNamesVm()
                             {
                                 OrderGroupId = c.OrderGroupId,
                                 OrderGroupName = c.OrderGroupName,

                             }).ToList();
                return Ok(group);
            }
            else
            {
                var group = (from c in db.OrderGroup
                             where c.IsCompany == false
                             && c.CompanyBranchId == companyBranch.Id
                             select new orderGroupNamesVm()
                             {
                                 OrderGroupId = c.OrderGroupId,
                                 OrderGroupName = c.OrderGroupName,

                             }).ToList();
                return Ok(group);
            }

           
        }

        [Route("getOrderConfigureName")]
        [HttpPost]
        public IHttpActionResult GetOrderGroupName()
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);

            var groupSales = db.Sales.Where(c => c.CompanyId == company.companyId
            && c.IsCompany == company.isCompany
            && c.BranchId == (company.isCompany ? (int?)null : company.oId))
                .Select(n => new orderGroupNameVm()
                {
                    Id = n.SalesId,
                    name = n.SalesName,

                }).ToList();

            var groupPurchases = db.Purchases.Where(c => c.CompanyId == company.companyId
            && c.IsCompany == company.isCompany
            && c.BranchId == (company.isCompany ? (int?)null : company.oId))
                .Select(n => new orderGroupNameVm()
                {
                    Id = n.PurchasesId,
                    name = n.PurchasesName,

                }).ToList();
            var returned = new
            {
                sales = groupSales,
                pur = groupPurchases
            };
           
            return Ok(returned);
            


        }

        [Route("getOrderSalePur")]
        [HttpPost]
        public IHttpActionResult GetOrderSalePur(orderSalePurGet order)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            if(company == null)
            {
                return BadRequest("يرجي تسجيل الدخول من جديد او او قم بتحديد شركة اخري");
            }
            if (order.type == "sales")
            {
                var o =OrderGroupManger.getOrderSalebyId(order.id, company);
                if(o == null)
                {
                    return BadRequest("لا يوجد اذن مسجل بهذا النوع");
                }
                return Ok(o);
            }
            else
            {
                var o = OrderGroupManger.getOrderPurbyId(order.id, company);
                if (o == null)
                {
                    return BadRequest("لا يوجد اذن مسجل بهذا النوع");
                }
                return Ok(o);
            }
        }

        [Route("getOrderGroupbyId/{id:int}")]
        [HttpGet]
        public IHttpActionResult GetOrderGroup(int id)
        {

           var group = OrderGroupManger.getOrderbyId(id);

            return Ok(group);


        }


        [Route("getOrderToBillById")]
        [HttpPost]
        public IHttpActionResult GetOrdertoBill(reciveOrderToBill id)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var group = OrderGroupManger.getOrderToBillByType(id.Type , id.orderGroupId , id.InventoryId , company);
           // this.AppCompanyUserManager.getchrt();
            return Ok(group);
        }

        [Route("getBillById")]
        [HttpPost]
        public IHttpActionResult GetBillById(getBill bill)
        {
            if(bill.type == "مشتريات" || bill.type == "purReturns")
            {
                SavebillVm bi = new SavebillVm();
                PurchasesBill bx = new PurchasesBill();
                var group = OrderGroupManger.getOrderbyId(bill.orderGroupId);

                if (bill.billTypeName == "buy")
                {
                    if (bill.isCompny)
                    {
                        bx = db.PurchasesBills.SingleOrDefault(a => a.BillNo == bill.billId && a.IsCompany == true && a.CompanyId == bill.CompanyId && a.OrdersTypePurchasesType == bill.billTypeName);
                    }
                    else
                    {
                        bx = db.PurchasesBills.SingleOrDefault(a => a.BillNo == bill.billId && a.IsCompany == false && a.BranchId == bill.CompanyId && a.OrdersTypePurchasesType == bill.billTypeName);

                    }
                    if(bx == null)
                    {
                        return BadRequest("لا يوجد اذن مسجل بهذا الرقم");
                    }

                    bi.billId = bx.PurchasesBillId;
                    bi.type = "inventory";
                    if(bi.billName == null)
                    {
                        bi.billName = new orderBillNames();
                        bi.billMainAccount = new billMainAccount();
                        bi.billTitleaccount = new tilteAccounts();
                        bi.billOtherTotalAcounts = new List<billOtherAccounts>();
                        bi.billTotalAccount = new BillTotalAccount();
                        bi.billTotalAccount.billPaymethods = new billPaymethodsforSave();
                        bi.billTotalAccount.billPaymethods.billPaymethodProperties = new List<billPaymethodProperties>();
                        bi.billType = new BillType();
                    }
                    bi.billName.billName = bx.PurchasesBillName;
                    bi.billName.typeName = bx.OrdersTypePurchasesType;
                    bi.billName.typeId = bx.OrdersTypePurchasesId;
                    bi.billNo = bx.BillNo;
                    bi.billDate = bx.BillDate;
                    
                    bi.billMainAccount.AccountId = bx.BillMainAccountId;
                    bi.billMainAccount.AccountName = bx.BillMainAccount.AccountName;
                    bi.billTitleaccount.AccountId = bx.BillTitleAccount.AccountId;
                    bi.billOtherTotalAcounts = (from b in bx.BillOtherAccountBills
                                                select new billOtherAccounts()
                                                {
                                                    accountId = b.AccountId,
                                                    accountName = b.Account.AccountName,
                                                    value = b.value,
                                                    type = b.IsDebit == true ? "debit" : "crdit"
                                                }).ToList();
                    bi.billTotalAccount.accountId = bx.BillTotalAccountId;
                    bi.billTotalAccount.accountName = bx.BillTotalAccount.AccountName;
                    bi.billTotalAccount.billPaymethods.payId = bx.BillPaymethodId;
                    bi.billTotalAccount.billPaymethods.payName = bx.BillPaymethod.OrderGroupPayMethodPurchasesName;
                    bi.billTotalAccount.billPaymethods.billPaymethodProperties = (from p in bx.BillPaymentProperties
                                                                                  select new billPaymethodProperties()
                                                                                  {
                                                                                      Id = p.OrderPropertiesPayPurchases.OrderPropertyTypeId,
                                                                                      Name = p.OrderPropertiesPayPurchases.OrderPropertiesPayPurchasesName,
                                                                                      isRequied = p.OrderPropertiesPayPurchases.IsRequired,
                                                                                      toPrint = p.OrderPropertiesPayPurchases.ToPrint,
                                                                                      typeName = p.OrderPropertiesPayPurchases.OrderPropertyType.OrderPropertyTypeName,
                                                                                      value = p.value
                                                                                  }).ToList();
                    bi.billType.typeId = bx.OrdersTypePurchasesId;
                    bi.billType.typeName = bx.OrdersTypePurchases.sale;
                    bi.IsCompany = bill.isCompny;
                    bi.ComapnyId = bill.CompanyId;
                    bi.OrderGroupId = bill.orderGroupId;
                    

                    var groupRow = bx.itemsRows.GroupBy(a => a.value).ToList();
                    foreach (var row in groupRow)
                    {
                        List<itemPropertyValuesbill> rows = new List<itemPropertyValuesbill>();

                        foreach (var item in row)
                        {
                            rows.Add(new itemPropertyValuesbill()
                            {
                                itemPropertyId = item.itemPropertyId,
                                itemPropertyValueId = item.itemPropertyValueId,
                                itemPropertyValueName = item.itemPropertyValueName,
                                type = item.type
                            });
                        }
                        if(bi.billItems == null)
                        {
                            bi.billItems = new List<List<itemPropertyValuesbill>>();
                        }
                       
                        bi.billItems.Add(rows);
                    }
                    bi.worker = bx.CurrentWorker.FirstName + " " + bx.CurrentWorker.LastName;
                    return Ok(bi);

                }
                else if(bill.billTypeName == "returnd")
                {
                    if (bill.isCompny)
                    {
                        bx = db.PurchasesBills.SingleOrDefault(a => a.BillNo == bill.billId && a.IsCompany == true && a.CompanyId == bill.CompanyId && a.OrdersTypePurchasesType == bill.billTypeName);
                    }
                    else
                    {
                        bx = db.PurchasesBills.SingleOrDefault(a => a.BillNo == bill.billId && a.IsCompany == false && a.BranchId == bill.CompanyId && a.OrdersTypePurchasesType == bill.billTypeName);

                    }
                    if (bx == null)
                    {
                        return BadRequest("لا يوجد اذن مسجل بهذا الرقم");
                    }

                    bi.billId = bx.PurchasesBillId;
                    bi.type = "inventory";
                    if (bi.billName == null)
                    {
                        bi.billName = new orderBillNames();
                        bi.billMainAccount = new billMainAccount();
                        bi.billTitleaccount = new tilteAccounts();
                        bi.billOtherTotalAcounts = new List<billOtherAccounts>();
                        bi.billTotalAccount = new BillTotalAccount();
                        bi.billTotalAccount.billPaymethods = new billPaymethodsforSave();
                        bi.billTotalAccount.billPaymethods.billPaymethodProperties = new List<billPaymethodProperties>();
                        bi.billType = new BillType();
                    }
                    bi.billName.billName = bx.PurchasesBillName;
                    bi.billName.typeName = bx.OrdersTypePurchasesType;
                    bi.billName.typeId = bx.OrdersTypePurchasesId;
                    bi.billNo = bx.BillNo;
                    bi.billDate = bx.BillDate;

                    bi.billMainAccount.AccountId = bx.BillMainAccountId;
                    bi.billMainAccount.AccountName = bx.BillMainAccount.AccountName;
                    bi.billTitleaccount.AccountId = bx.BillTitleAccount.AccountId;
                    bi.billOtherTotalAcounts = (from b in bx.BillOtherAccountBills
                                                select new billOtherAccounts()
                                                {
                                                    accountId = b.AccountId,
                                                    accountName = b.Account.AccountName,
                                                    value = b.value,
                                                    type = b.IsDebit == true ? "debit" : "crdit"
                                                }).ToList();
                    bi.billTotalAccount.accountId = bx.BillTotalAccountId;
                    bi.billTotalAccount.accountName = bx.BillTotalAccount.AccountName;
                    bi.billTotalAccount.billPaymethods.payId = bx.BillPaymethodId;
                    bi.billTotalAccount.billPaymethods.payName = bx.BillPaymethod.OrderGroupPayMethodPurchasesName;
                    bi.billTotalAccount.billPaymethods.billPaymethodProperties = (from p in bx.BillPaymentProperties
                                                                                  select new billPaymethodProperties()
                                                                                  {
                                                                                      Id = p.OrderPropertiesPayPurchases.OrderPropertyTypeId,
                                                                                      Name = p.OrderPropertiesPayPurchases.OrderPropertiesPayPurchasesName,
                                                                                      isRequied = p.OrderPropertiesPayPurchases.IsRequired,
                                                                                      toPrint = p.OrderPropertiesPayPurchases.ToPrint,
                                                                                      typeName = p.OrderPropertiesPayPurchases.OrderPropertyType.OrderPropertyTypeName,
                                                                                      value = p.value
                                                                                  }).ToList();
                    bi.billType.typeId = bx.OrdersTypePurchasesId;
                    bi.billType.typeName = bx.OrdersTypePurchases.returnd;
                    bi.IsCompany = bill.isCompny;
                    bi.ComapnyId = bill.CompanyId;
                    bi.OrderGroupId = bill.orderGroupId;


                    var groupRow = bx.itemsRows.GroupBy(a => a.value).ToList();
                    foreach (var row in groupRow)
                    {
                        List<itemPropertyValuesbill> rows = new List<itemPropertyValuesbill>();

                        foreach (var item in row)
                        {
                            rows.Add(new itemPropertyValuesbill()
                            {
                                itemPropertyId = item.itemPropertyId,
                                itemPropertyValueId = item.itemPropertyValueId,
                                itemPropertyValueName = item.itemPropertyValueName,
                                type = item.type
                            });
                        }
                        if (bi.billItems == null)
                        {
                            bi.billItems = new List<List<itemPropertyValuesbill>>();
                        }

                        bi.billItems.Add(rows);
                    }
                    bi.worker = bx.CurrentWorker.FirstName + " " + bx.CurrentWorker.LastName;
                    return Ok(bi);
                }

            }
           else if(bill.type == "مبيعات" ||bill.type == "salesReturn")
            {
                SavebillVm bi = new SavebillVm();
                SalesBill bx = new SalesBill();
                if (bill.billTypeName == "sale")
                {
                    if (bill.isCompny)
                    {
                        bx = db.SalesBills.SingleOrDefault(a => a.BillNo == bill.billId && a.IsCompany == true && a.CompanyId == bill.CompanyId && a.OrdersTypeSalesType == bill.billTypeName);
                    }
                    else
                    {
                        bx = db.SalesBills.SingleOrDefault(a => a.BillNo == bill.billId && a.IsCompany == false && a.BranchId == bill.CompanyId && a.OrdersTypeSalesType == bill.billTypeName);

                    }
                    if (bx == null)
                    {
                        return BadRequest("لا يوجد اذن مسجل بهذا الرقم");
                    }

                    bi.billId = bx.SalesBillId;
                    bi.type = "inventory";
                    if (bi.billName == null)
                    {
                        bi.billName = new orderBillNames();
                        bi.billMainAccount = new billMainAccount();
                        bi.billTitleaccount = new tilteAccounts();
                        bi.billOtherTotalAcounts = new List<billOtherAccounts>();
                        bi.billTotalAccount = new BillTotalAccount();
                        bi.billTotalAccount.billPaymethods = new billPaymethodsforSave();
                        bi.billTotalAccount.billPaymethods.billPaymethodProperties = new List<billPaymethodProperties>();
                        bi.billType = new BillType();
                    }
                    bi.billName.billName = bx.SalesBillName;
                    bi.billName.typeName = bx.OrdersTypeSalesType;
                    bi.billName.typeId = bx.OrdersTypeSalesId;
                    bi.billNo = bx.BillNo;
                    bi.billDate = bx.BillDate;

                    bi.billMainAccount.AccountId = bx.BillMainAccountId;
                    bi.billMainAccount.AccountName = bx.BillMainAccount.AccountName;
                    bi.billTitleaccount.AccountId = bx.BillTitleAccount.AccountId;
                    bi.billOtherTotalAcounts = (from b in bx.BillOtherAccountBills
                                                select new billOtherAccounts()
                                                {
                                                    accountId = b.AccountId,
                                                    accountName = b.Account.AccountName,
                                                    value = b.value,
                                                    type = b.IsDebit == true ? "debit" : "crdit"
                                                }).ToList();
                    bi.billTotalAccount.accountId = bx.BillTotalAccountId;
                    bi.billTotalAccount.accountName = bx.BillTotalAccount.AccountName;
                    bi.billTotalAccount.billPaymethods.payId = bx.BillPaymethodId;
                    bi.billTotalAccount.billPaymethods.payName = bx.BillPaymethod.OrderGroupPayMethodName;
                    bi.billTotalAccount.billPaymethods.billPaymethodProperties = (from p in bx.BillPaymentProperties
                                                                                  select new billPaymethodProperties()
                                                                                  {
                                                                                      Id = p.OrderPropertiesPaySales.OrderPropertyTypeId,
                                                                                      Name = p.OrderPropertiesPaySales.OrderPropertiesPaySalesName,
                                                                                      isRequied = p.OrderPropertiesPaySales.IsRequired,
                                                                                      toPrint = p.OrderPropertiesPaySales.ToPrint,
                                                                                      typeName = p.OrderPropertiesPaySales.OrderPropertyType.OrderPropertyTypeName,
                                                                                      value = p.value
                                                                                  }).ToList();
                    bi.billType.typeId = bx.OrdersTypeSalesId;
                    bi.billType.typeName = bx.OrdersTypeSales.sale;
                    bi.IsCompany = bill.isCompny;
                    bi.ComapnyId = bill.CompanyId;
                    bi.OrderGroupId = bill.orderGroupId;


                    var groupRow = bx.itemsRows.GroupBy(a => a.value).ToList();
                    foreach (var row in groupRow)
                    {
                        List<itemPropertyValuesbill> rows = new List<itemPropertyValuesbill>();

                        foreach (var item in row)
                        {
                            rows.Add(new itemPropertyValuesbill()
                            {
                                itemPropertyId = item.itemPropertyId,
                                itemPropertyValueId = item.itemPropertyValueId,
                                itemPropertyValueName = item.itemPropertyValueName,
                                type = item.type
                            });
                        }
                        if (bi.billItems == null)
                        {
                            bi.billItems = new List<List<itemPropertyValuesbill>>();
                        }

                        bi.billItems.Add(rows);
                    }
                    bi.worker = bx.CurrentWorker.FirstName + " " + bx.CurrentWorker.LastName;
                    return Ok(bi);
                }
                else if (bill.billTypeName == "returnd")
                {
                    if (bill.isCompny)
                    {
                        bx = db.SalesBills.SingleOrDefault(a => a.BillNo == bill.billId && a.IsCompany == true && a.CompanyId == bill.CompanyId && a.OrdersTypeSalesType == bill.billTypeName);
                    }
                    else
                    {
                        bx = db.SalesBills.SingleOrDefault(a => a.BillNo == bill.billId && a.IsCompany == false && a.BranchId == bill.CompanyId && a.OrdersTypeSalesType== bill.billTypeName);

                    }
                    if (bx == null)
                    {
                        return BadRequest("لا يوجد اذن مسجل بهذا الرقم");
                    }

                    bi.billId = bx.SalesBillId;
                    bi.type = "inventory";
                    if (bi.billName == null)
                    {
                        bi.billName = new orderBillNames();
                        bi.billMainAccount = new billMainAccount();
                        bi.billTitleaccount = new tilteAccounts();
                        bi.billOtherTotalAcounts = new List<billOtherAccounts>();
                        bi.billTotalAccount = new BillTotalAccount();
                        bi.billTotalAccount.billPaymethods = new billPaymethodsforSave();
                        bi.billTotalAccount.billPaymethods.billPaymethodProperties = new List<billPaymethodProperties>();
                        bi.billType = new BillType();
                    }
                    bi.billName.billName = bx.SalesBillName;
                    bi.billName.typeName = bx.OrdersTypeSalesType;
                    bi.billName.typeId = bx.OrdersTypeSalesId;
                    bi.billNo = bx.BillNo;
                    bi.billDate = bx.BillDate;

                    bi.billMainAccount.AccountId = bx.BillMainAccountId;
                    bi.billMainAccount.AccountName = bx.BillMainAccount.AccountName;
                    bi.billTitleaccount.AccountId = bx.BillTitleAccount.AccountId;
                    bi.billOtherTotalAcounts = (from b in bx.BillOtherAccountBills
                                                select new billOtherAccounts()
                                                {
                                                    accountId = b.AccountId,
                                                    accountName = b.Account.AccountName,
                                                    value = b.value,
                                                    type = b.IsDebit == true ? "debit" : "crdit"
                                                }).ToList();
                    bi.billTotalAccount.accountId = bx.BillTotalAccountId;
                    bi.billTotalAccount.accountName = bx.BillTotalAccount.AccountName;
                    bi.billTotalAccount.billPaymethods.payId = bx.BillPaymethodId;
                    bi.billTotalAccount.billPaymethods.payName = bx.BillPaymethod.OrderGroupPayMethodName;
                    bi.billTotalAccount.billPaymethods.billPaymethodProperties = (from p in bx.BillPaymentProperties
                                                                                  select new billPaymethodProperties()
                                                                                  {
                                                                                      Id = p.OrderPropertiesPaySales.OrderPropertyTypeId,
                                                                                      Name = p.OrderPropertiesPaySales.OrderPropertiesPaySalesName,
                                                                                      isRequied = p.OrderPropertiesPaySales.IsRequired,
                                                                                      toPrint = p.OrderPropertiesPaySales.ToPrint,
                                                                                      typeName = p.OrderPropertiesPaySales.OrderPropertyType.OrderPropertyTypeName,
                                                                                      value = p.value
                                                                                  }).ToList();
                    bi.billType.typeId = bx.OrdersTypeSalesId;
                    bi.billType.typeName = bx.OrdersTypeSales.returnd;
                    bi.IsCompany = bill.isCompny;
                    bi.ComapnyId = bill.CompanyId;
                    bi.OrderGroupId = bill.orderGroupId;


                    var groupRow = bx.itemsRows.GroupBy(a => a.value).ToList();
                    foreach (var row in groupRow)
                    {
                        List<itemPropertyValuesbill> rows = new List<itemPropertyValuesbill>();

                        foreach (var item in row)
                        {
                            rows.Add(new itemPropertyValuesbill()
                            {
                                itemPropertyId = item.itemPropertyId,
                                itemPropertyValueId = item.itemPropertyValueId,
                                itemPropertyValueName = item.itemPropertyValueName,
                                type = item.type
                            });
                        }
                        if (bi.billItems == null)
                        {
                            bi.billItems = new List<List<itemPropertyValuesbill>>();
                        }

                        bi.billItems.Add(rows);
                    }
                    bi.worker = bx.CurrentWorker.FirstName + " " + bx.CurrentWorker.LastName;
                    return Ok(bi);
                }
            }
            else
            {
                return BadRequest("لا يوجد مسجل بهذا النوع");
            }
           

            return BadRequest();


        }

        [Route("getOrderGroupforBill/{id:int}")]
        [HttpGet]
        public IHttpActionResult GetOrderGroupforBill(int id)
        {
            var group =OrderGroupManger.getOrderforbillbyId(id);
            return Ok(group);
        }


        [Route("getOrderGroupPropType/{id:int}")]
        [HttpGet]
        public IHttpActionResult GetOrderGroupPropRtpe(int id)
        {

            var group = (from o in db.OrderPropertyTypes
                        select new OrderPropertyTypeVm() {
                            OrderPropertyTypeId = o.OrderPropertyTypeId,
                            OrderPropertyTypeName = o.OrderPropertyTypeName
                        }).ToList();

            return Ok(group);


        }

        [Route("saveOrderGroup")]
        [HttpPost]
        public IHttpActionResult SaveOrderGroup(SaveOrderGroupVm orderGroup)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();

            if (orderGroup.IsCompany)
            {
                var checkCompany = db.Companies.Find(orderGroup.CompanyBranchId);
                if(checkCompany != null)
                {   if(orderGroup.OrderGroupId == 0)
                    {
                        var CheckgroupName = db.OrderGroup.SingleOrDefault(o => o.OrderGroupName == orderGroup.OrderGroupName && o.IsCompany == true && o.CompanyBranchId == orderGroup.CompanyBranchId);
                        if (CheckgroupName == null)
                        {
                            var oGroup = new OrderGroup()
                            {
                                IsCompany = true,
                                CompanyBranchId = orderGroup.CompanyBranchId,
                                OrderGroupName = orderGroup.OrderGroupName,
                                Sales = new Sales()
                                {
                                    ordersType = new List<OrdersTypeSales>()
                                    {
                                        new OrdersTypeSales()
                                        {
                                            sale = "فاتورة",
                                            returnd= "رد مبيعات"
                                        }
                                    }
                                },
                                Purchases = new Purchases()
                                {
                                   ordersType = new List<OrdersTypePurchases>()
                                   {
                                       new OrdersTypePurchases()
                                       {
                                           sale = "مشتريات سلع",
                                           returnd = "رد مشتريات"
                                       }
                                   }
                                },
                                CurrentWorkerId = uid
                            };

                            db.OrderGroup.Add(oGroup);
                            db.SaveChanges();
                            //var geto = OrderGroupManger.getOrderbyId(oGroup.OrderGroupId);
                            return Ok();

                        }
                        else
                        {
                            return BadRequest("لا يمكن تكرار اسم المجموعة");
                        }
                    }
                    else
                    {
                        //update
                        var oGroupExists = db.OrderGroup.Find(orderGroup.OrderGroupId);
                        var CheckgroupName = db.OrderGroup.SingleOrDefault(o => o.OrderGroupName == orderGroup.OrderGroupName && o.IsCompany == true && o.CompanyBranchId == orderGroup.CompanyBranchId && o.OrderGroupId != orderGroup.OrderGroupId);
                        if (CheckgroupName == null && oGroupExists != null)
                        {
                            
                            oGroupExists.OrderGroupName = orderGroup.OrderGroupName;
                            oGroupExists.CurrentWorkerId = uid;



                            db.Entry(oGroupExists).State = EntityState.Modified;
                            db.SaveChanges();
                            //var geto = OrderGroupManger.getOrderbyId(oGroupExists.OrderGroupId);

                            return Ok();

                        }
                        else
                        {
                            return BadRequest("لا يمكن تغيير الاسم");
                        }

                    }
                   
                }
                else
                {
                    return BadRequest("الشركة غير مسجلة");
                }
            }
            else
            {

                var checkCompany = db.Branchs.Find(orderGroup.CompanyBranchId);
                if (checkCompany != null)
                {
                    if (orderGroup.OrderGroupId == 0)
                    {
                        var CheckgroupName = db.OrderGroup.SingleOrDefault(o => o.OrderGroupName == orderGroup.OrderGroupName && o.IsCompany == false && o.CompanyBranchId == orderGroup.CompanyBranchId);
                        if (CheckgroupName == null)
                        {
                            var oGroup = new OrderGroup()
                            {
                                IsCompany = false,
                                CompanyBranchId = orderGroup.CompanyBranchId,
                                OrderGroupName = orderGroup.OrderGroupName,
                                CurrentWorkerId = uid
                            };

                            db.OrderGroup.Add(oGroup);
                            db.SaveChanges();
                           //var geto = OrderGroupManger.getOrderbyId(oGroup.OrderGroupId);
                            return Ok();

                        }
                        else
                        {
                            return BadRequest("لا يمكن تكرار اسم المجموعة");
                        }
                    }
                    else
                    {
                        //update
                        var oGroupExists = db.OrderGroup.Find(orderGroup.OrderGroupId);
                        var CheckgroupName = db.OrderGroup.SingleOrDefault(o => o.OrderGroupName == orderGroup.OrderGroupName && o.IsCompany == false && o.CompanyBranchId == orderGroup.CompanyBranchId && o.OrderGroupId != orderGroup.OrderGroupId);
                        if (CheckgroupName == null && oGroupExists != null)
                        {
                            oGroupExists.OrderGroupName = orderGroup.OrderGroupName;
                            oGroupExists.CurrentWorkerId = uid;



                            db.Entry(oGroupExists).State = EntityState.Modified;
                            db.SaveChanges();
                          // var geto = this.AppCompanyUserManager.getOrderbyId(oGroupExists.OrderGroupId);
                            return Ok();

                        }
                        else
                        {
                            return BadRequest("لا يمكن تغيير الاسم");
                        }

                    }

                }
                else
                {
                    return BadRequest("الفرع غير مسجل");
                }
            }


        }

        [Route("saveOrderorderGroupf")]
        [HttpPost]
        public IHttpActionResult SaveOrderName(OrderGroupVm orderTosave)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var order = db.OrderGroup.Find(orderTosave.OrderGroupId);
            if(order != null)
            {
                //update payMethod for Sale
                order.OrderGroupName = orderTosave.OrderGroupName;
                order.Sales.ordersType.SingleOrDefault().sale = orderTosave.sales.ordersType.sale;
                order.Sales.ordersType.FirstOrDefault().returnd = orderTosave.sales.ordersType.returnd;
                foreach (var pay in orderTosave.sales.payMethod)
                {
                    var isexistes = order.Sales.payMethod.FirstOrDefault(p => p.OrderGroupPayMethodName == pay.payName || p.AccountCategoryId == pay.accountId);
                    if (isexistes == null)
                    {
                        var oprop = new OrderGroupPayMethodSales()
                        {
                            AccountCategoryId = pay.accountId,
                            OrderGroupPayMethodName = pay.payName
                        };
                        if(pay.PayMethodProperties != null)
                        {
                            foreach (var pprop in pay.PayMethodProperties)
                            {
                                oprop.OrderPropertiesPaySales.Add(new OrderPropertiesPaySales()
                                {
                                    OrderPropertiesPaySalesName = pprop.OrderPropertiesPayName,
                                    OrderPropertyTypeId = pprop.OrderPropertyTypeId,
                                    IsRequired = pprop.IsRequired,
                                    ToPrint = pprop.ToPrint,
                                    CurrentWorkerId = uid,

                                });
                            }
                        }
                        order.Sales.payMethod.Add(oprop);
                    }
                    else
                    {
                        isexistes.AccountCategoryId = pay.accountId;
                        isexistes.OrderGroupPayMethodName = pay.payName;
                        foreach (var pprop in pay.PayMethodProperties)
                        {
                            var isPropExistes = isexistes.OrderPropertiesPaySales.SingleOrDefault(p => p.OrderPropertiesPaySalesName == pprop.OrderPropertiesPayName);
                            if(isPropExistes == null)
                            {
                                isexistes.OrderPropertiesPaySales.Add(new OrderPropertiesPaySales()
                                {
                                    OrderPropertiesPaySalesName = pprop.OrderPropertiesPayName,
                                    IsRequired = pprop.IsRequired,
                                    ToPrint = pprop.ToPrint,
                                    CurrentWorkerId = uid,
                                    OrderPropertyTypeId = db.OrderPropertyTypes.SingleOrDefault(t => t.OrderPropertyTypeName == pprop.OrderPropertyTypeName).OrderPropertyTypeId
                                });
                            }
                            else
                            {
                                isPropExistes.OrderPropertyTypeId = db.OrderPropertyTypes.SingleOrDefault(t => t.OrderPropertyTypeName == pprop.OrderPropertyTypeName).OrderPropertyTypeId;
                                isPropExistes.IsRequired = pprop.IsRequired;
                                isPropExistes.ToPrint = pprop.ToPrint;

                                isPropExistes.CurrentWorkerId = uid;
                            }
                        }

                    }

                }

                //update payMethod for pur
                order.Purchases.ordersType.FirstOrDefault().sale = orderTosave.purchases.ordersType.buy;
                order.Purchases.ordersType.FirstOrDefault().returnd = orderTosave.purchases.ordersType.returnd;
                foreach (var pay in orderTosave.purchases.payMethod)
                {
                    var isexistes = order.Purchases.payMethod.FirstOrDefault(p => p.OrderGroupPayMethodPurchasesName == pay.payName || p.AccountCategoryId == pay.accountId);
                    if (isexistes == null)
                    {
                        var oprop = new OrderGroupPayMethodPurchases()
                        {
                            AccountCategoryId = pay.accountId,
                            OrderGroupPayMethodPurchasesName = pay.payName
                        };
                        if(pay.PayMethodProperties != null)
                        {
                            foreach (var pprop in pay.PayMethodProperties)
                            {
                                oprop.OrderPropertiesPayPurchases.Add(new OrderPropertiesPayPurchases()
                                {
                                    OrderPropertiesPayPurchasesName = pprop.OrderPropertiesPayName,
                                    OrderPropertyTypeId = db.OrderPropertyTypes.SingleOrDefault(t => t.OrderPropertyTypeName == pprop.OrderPropertyTypeName).OrderPropertyTypeId,
                                    IsRequired = pprop.IsRequired,
                                    ToPrint = pprop.ToPrint,
                                    CurrentWorkerId = uid,

                                });
                            }
                        }
                    
                        order.Purchases.payMethod.Add(oprop);
                    }
                    else
                    {
                        isexistes.AccountCategoryId = pay.accountId;
                        isexistes.OrderGroupPayMethodPurchasesName = pay.payName;
                        foreach (var pprop in pay.PayMethodProperties)
                        {
                            var isPropExistes = isexistes.OrderPropertiesPayPurchases.SingleOrDefault(p => p.OrderPropertiesPayPurchasesName == pprop.OrderPropertiesPayName);
                            if (isPropExistes == null)
                            {
                                isexistes.OrderPropertiesPayPurchases.Add(new OrderPropertiesPayPurchases()
                                {
                                    OrderPropertiesPayPurchasesName = pprop.OrderPropertiesPayName,
                                    IsRequired = pprop.IsRequired,
                                    ToPrint = pprop.ToPrint,
                                    CurrentWorkerId = uid,
                                    OrderPropertyTypeId = db.OrderPropertyTypes.SingleOrDefault(t=>t.OrderPropertyTypeName == pprop.OrderPropertyTypeName).OrderPropertyTypeId
                                });
                            }
                            else
                            {
                                isPropExistes.OrderPropertyTypeId = db.OrderPropertyTypes.SingleOrDefault(t => t.OrderPropertyTypeName == pprop.OrderPropertyTypeName).OrderPropertyTypeId;
                                isPropExistes.IsRequired = pprop.IsRequired;
                                isPropExistes.ToPrint = pprop.ToPrint;

                                isPropExistes.CurrentWorkerId = uid;
                            }
                        }

                    }

                }

                //update OrderAccountsforSubAccountSales
                foreach (var o in orderTosave.sales.orderAccountsforSubAccounts)
                {
                    var isExistes = order.Sales.orderAccountsforSubAccounts.SingleOrDefault(s => s.AccountId == o.AccountId);
                    if(isExistes == null)
                    {
                        order.Sales.orderAccountsforSubAccounts.Add(new OrderAccountsforSubAccountSales()
                        {
                            AccountId = o.AccountId,
                            DebitWhenAdd = o.DebitWhenAdd,
                            CurrentWorkerId = uid,
                            DebitforPlus = o.DebitforPlus,
                            IsNewOrder = o.IsNewOrder,
                            CostEffect = o.CostEffect

                        });
                    }
                    else
                    {
                        isExistes.AccountId = o.AccountId;
                        isExistes.CurrentWorkerId = uid;
                        isExistes.DebitWhenAdd = o.DebitWhenAdd;
                        isExistes.DebitforPlus = o.DebitforPlus;
                        isExistes.IsNewOrder = o.IsNewOrder;
                        isExistes.CostEffect = o.CostEffect;
                    }
                }

                //update OrderAccountsforSubAccountpur
                foreach (var o in orderTosave.purchases.orderAccountsforSubAccounts)
                {
                    var isExistes = order.Purchases.orderAccountsforSubAccounts.SingleOrDefault(s => s.AccountId == o.AccountId);
                    if (isExistes == null)
                    {
                        order.Purchases.orderAccountsforSubAccounts.Add(new OrderAccountsforSubAccountPurchases()
                        {
                            AccountId = o.AccountId,
                            DebitWhenAdd = o.DebitWhenAdd,
                            CurrentWorkerId = uid,
                            DebitforPlus = o.DebitforPlus,
                            IsNewOrder = o.IsNewOrder,
                            CostEffect = o.CostEffect
                        });
                    }
                    else
                    {
                        isExistes.AccountId = o.AccountId;
                        isExistes.CurrentWorkerId = uid;
                        isExistes.DebitWhenAdd = o.DebitWhenAdd;
                        isExistes.DebitforPlus = o.DebitforPlus;
                        isExistes.IsNewOrder = o.IsNewOrder;
                        isExistes.CostEffect = o.CostEffect;
                    }
                }

                //Update OrderOtherTotalAccountSales
                foreach (var o in orderTosave.sales.orderOtherTotalAccounts)
                {
                    var isExistes = order.Sales.orderOtherTotalAccounts.SingleOrDefault(s => s.AccountId == o.AccountId);
                    if (isExistes == null)
                    {
                        order.Sales.orderOtherTotalAccounts.Add(new OrderOtherTotalAccountSales()
                        {
                            AccountId = o.AccountId,
                            DebitWhenAdd = o.DebitWhenAdd,
                            CurrentWorkerId = uid,
                            DebitforPlus = o.DebitforPlus,
                            IsNewOrder = o.IsNewOrder,
                            CostEffect = o.CostEffect

                        });
                    }
                    else
                    {
                        isExistes.AccountId = o.AccountId;
                        isExistes.CurrentWorkerId = uid;
                        isExistes.DebitWhenAdd = o.DebitWhenAdd;
                        isExistes.DebitforPlus = o.DebitforPlus;
                        isExistes.IsNewOrder = o.IsNewOrder;
                        isExistes.IsNewOrder = o.CostEffect;
                    }
                }
                //Update OrderOtherTotalAccountPur
                foreach (var o in orderTosave.purchases.orderOtherTotalAccounts)
                {
                    var isExistes = order.Purchases.orderOtherTotalAccounts.SingleOrDefault(s => s.AccountId == o.AccountId);
                    if (isExistes == null)
                    {
                        order.Purchases.orderOtherTotalAccounts.Add(new OrderOtherTotalAccountPurchases()
                        {
                            AccountId = o.AccountId,
                            DebitWhenAdd = o.DebitWhenAdd,
                            CurrentWorkerId = uid,
                            DebitforPlus = o.DebitforPlus,
                            IsNewOrder = o.IsNewOrder,
                            CostEffect = o.CostEffect

                        });
                    }
                    else
                    {
                        isExistes.AccountId = o.AccountId;
                        isExistes.CurrentWorkerId = uid;
                        isExistes.DebitWhenAdd = o.DebitWhenAdd;
                        isExistes.DebitforPlus = o.DebitforPlus;
                        isExistes.IsNewOrder = o.IsNewOrder;
                        isExistes.CostEffect = o.CostEffect;
                    }
                }
                //Update OrderMainAccount
                foreach (var o in orderTosave.sales.orderMainAccounts)
                {
                    if(o.salesAccount.AccountId != o.salesCostAccount.AccountId && o.salesAccount != o.salesreturnAccount && o.salesreturnAccount.AccountId != o.salesCostAccount.AccountId)
                    {
                        var isExistes = order.Sales.orderMainAccounts.SingleOrDefault(a => a.InventoryAccountId == o.inventoryAccount.AccountId);

                        if(isExistes == null)
                        {
                            var isInventory = db.Accounts.Find(o.inventoryAccount.AccountId);
                            if(isInventory.AccountCategory.AccountsDataTypes.TypeName != "مخازن")
                            {
                                return BadRequest("لم يتم التعرف علي بعض المخازن - يرجي مراجعه البيانات");
                            }
                            order.Sales.orderMainAccounts.Add(new OrderMainAccount()
                            {
                                InventoryAccountId = o.inventoryAccount.AccountId,
                                SalesAccountId = o.salesAccount.AccountId,
                                SalesCostAccountId = o.salesCostAccount.AccountId,
                                SalesReturnAccountId = o.salesreturnAccount.AccountId,
                                CurrentWorkerId =uid
                            });
                        }
                        else
                        {
                            isExistes.SalesAccountId = o.salesAccount.AccountId;
                            isExistes.SalesCostAccountId = o.salesCostAccount.AccountId;
                            isExistes.SalesReturnAccountId = o.salesreturnAccount.AccountId;
                        }
                    }
                }
                db.Entry(order).State = EntityState.Modified;
                db.SaveChanges();

            }
            else
            {
                return BadRequest("لا توجد مجموعة مسجلة");
            }

            return Ok();
        }


        [Route("saveOrderProperty")]
        [HttpPost]
        public IHttpActionResult SaveOrderProperty(SaveOrderPropertyVm orderprop)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var ogroup = db.OrderGroup.Find(orderprop.OrderGroupId);
            if(ogroup != null)
            {
                if(orderprop.OrderPropId == 0) 
                {
                   
                    return Ok();
                }
                else
                {
   
                    return Ok();
                }
            }
            else
            {
                return BadRequest();
            }
        }
        [Route("saveOrderMainAccount")]
        [HttpPost]
        public IHttpActionResult SaveOrderMainAccount(List<AccountsforOrderGroupVm> orderprop)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            foreach (var account in orderprop)
            {
                var accountEx = db.Accounts.Find(account.AccountId);
                //if(accountEx != null)
                //{
                //    var accMain = db.OrderMainAccounts.SingleOrDefault(a => a.AccountId == account.AccountId && a.OrderGroupId == account.OrderGroupId);
                //    if(account.selectedaccount == true && accMain == null)
                //    {
                //        var newMainAccount = new OrderMainAccount()
                //        {
                //            OrderGroupId = (Int32)account.OrderGroupId,
                //            AccountId = account.AccountId,
                //            CurrentWorkerId = uid
                //        };
                //        db.OrderMainAccounts.Add(newMainAccount);
                //        db.SaveChanges();
                //    }
                //    else if(account.selectedaccount == false && accMain != null)
                //    {
                //        db.OrderMainAccounts.Remove(accMain);
                //        db.SaveChanges();
                //    }

                //}
            }
            //var geto = this.AppCompanyUserManager.getOrderbyId((Int32)orderprop.FirstOrDefault().OrderGroupId).OrderMainAccounts;

            return Ok();
        }



        [Route("getOrderAccounts")]
        [HttpPost]
        public IHttpActionResult GetOrderAccounts(isCompanyBranch companyBranch)
        {
            if (companyBranch.IsCompany)
            {
                var company = db.Companies.Find(companyBranch.Id);
                if(company != null)
                {

                    //var accounts = (from a in db.Accounts
                    //                where a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.CompanyId
                    //                && a.BranchId == null
                    //                && a.AccountCategory.AccountsDataTypes.Id == companyBranch.groupId
                    //                && !(from nn in db.OrderOtherTotalAccounts
                    //                        where nn.OrderGroupId == companyBranch.gId
                    //                        select nn.AccountId).Contains(a.AccountId)
                    //                && !(from nn in db.OrderAccountsforSubAccounts
                    //                         where nn.OrderGroupId == companyBranch.gId
                    //                         select nn.AccountId).Contains(a.AccountId)
                    //                select new AccountsforOrderGroupVm()
                    //                {
                    //                    AccountId = a.AccountId,
                    //                    AccountName = a.AccountName,
                    //                    AccountType = a.AccountCategory.AccountsDataTypes.TypeName,
                    //                    selectedaccount = db.OrderMainAccounts.Any(b => b.AccountId == a.AccountId && b.OrderGroupId == companyBranch.gId),
                    //                    OrderGroupId = companyBranch.gId
                                        
                    //                }).ToList().OrderBy(c => c.AccountType);
                   

                   
                    return Ok();
                }
                else
                {
                    return BadRequest("لا توجد شركة مسجلة بهذا الاسم");
                }
            }
            else
            {
                //Branch
                var branch = db.Branchs.Find(companyBranch.Id);
                if(branch != null)
                {
                    //var accounts = (from a in db.Accounts
                    //                where a.BranchId == branch.BranchId
                    //                 && a.AccountCategory.AccountsDataTypes.Id == companyBranch.groupId
                    //                  && !(from nn in db.OrderOtherTotalAccounts
                    //                       where nn.OrderGroupId == companyBranch.gId
                    //                       select nn.AccountId).Contains(a.AccountId)
                    //                && !(from nn in db.OrderAccountsforSubAccounts
                    //                     where nn.OrderGroupId == companyBranch.gId
                    //                     select nn.AccountId).Contains(a.AccountId)
                    //                select new AccountsforOrderGroupVm()
                    //                {
                    //                    AccountId = a.AccountId,
                    //                    AccountName = a.AccountName,
                    //                    AccountType = a.AccountCategory.AccountsDataTypes.TypeName,
                    //                    selectedaccount = db.OrderMainAccounts.Any(b => b.AccountId == a.AccountId && b.OrderGroupId == companyBranch.gId)
                    //                }).ToList().OrderBy(c => c.AccountType);
                    return Ok();
                }
                else
                {
                    return BadRequest("لا يوجد فرع مسجل بهذاالاسم");
                }
            }
          
        }

        [Route("getOrderAccountsForPay")]
        [HttpPost]
        public IHttpActionResult GetOrderAccountsForPay(isCompanyBranch companyBranch)
        {
            if (companyBranch.IsCompany)
            {
                var company = db.Companies.Find(companyBranch.Id);
                if (company != null)
                {

                    var accounts = (from a in db.AccountCategories
                                    where a.BalanceSheetType.BalanceSheet.CompanyId == company.CompanyId
                                   
                                    select new AccountsforOrderGroupVm()
                                    {
                                        AccountId = a.AccountCategoryId,
                                        AccountName = a.AccountCategoryName,
                                      
                                        OrderGroupId = companyBranch.gId

                                    }).ToList().OrderBy(c => c.AccountType);



                    return Ok(accounts);
                }
                else
                {
                    return BadRequest("لا توجد شركة مسجلة بهذا الاسم");
                }
            }
            else
            {
                //Branch
                var branch = db.Branchs.Find(companyBranch.Id);
                if (branch != null)
                {
                    var accounts = (from a in db.AccountCategories
                                    where a.BalanceSheetType.BalanceSheet.CompanyId == branch.CompanyId
                                    
                                    select new AccountsforOrderGroupVm()
                                    {
                                        AccountId = a.AccountCategoryId,
                                        AccountName = a.AccountCategoryName,
                                       
                                        OrderGroupId = companyBranch.gId
                                    }).ToList().OrderBy(c => c.AccountType);
                    return Ok(accounts);
                }
                else
                {
                    return BadRequest("لا يوجد فرع مسجل بهذاالاسم");
                }
            }
        }


        [Route("getCatogory")]
        [HttpPost]
        public IHttpActionResult getCatogoryFor( )
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var getCat = db.AccountCategories.Where(a => a.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
            && a.AccountsDataTypes.aliasName != "inventory")
                .Select(c => new
                {
                    AccountId = c.AccountCategoryId,
                    AccountName = c.AccountCategoryName,
                    Code = c.Code,
                    Childern = c.Accounts.Where(a => a.BranchId == (company.isCompany ? (int?)null : company.oId)).Select(a => new
                    {
                        AccountId = a.AccountId,
                        AccountName = a.AccountName,
                        Code = a.Code
                    }).ToList()
                }).ToList();
            if(getCat == null || getCat.Count == 0)
            {
                return BadRequest("لا يوجد حسابات جاهزة الان - اختيار شركة اخري او قم باعداد الحسابات");
            }
            return Ok(getCat);
        }

        [Route("getAccounts")]
        [HttpPost]
        public IHttpActionResult getAccounts()
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            var company = db.branchGroupSt.SingleOrDefault(a => a.CurrentWorkerId == uid);
            var getCat = db.Accounts.Where(a => a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
            && a.AccountCategory.AccountsDataTypes.aliasName == "other" && a.BranchId == (company.isCompany ? (int?)null : company.oId))
                .Select(c => new
                {
                    AccountId = c.AccountId,
                    AccountName = c.AccountName,
                    Code = c.Code
                }).ToList();
            if (getCat == null || getCat.Count == 0)
            {
                return BadRequest("لا يوجد حسابات جاهزة الان - اختار شركة اخري او قم باعداد الحسابات");
            }
            return Ok(getCat);
        }

        [Route("getOrderAccountsForSub")]
        [HttpPost]
        public IHttpActionResult GetOrderAccountsForSub(isCompanyBranch companyBranch)
        {
            if (companyBranch.IsCompany)
            {
                var company = db.Companies.Find(companyBranch.Id);
                if (company != null)
                {
                    var inventoryAccounts = (from i in db.Accounts
                                             where i.AccountCategory.AccountsDataTypes.TypeName == "مخازن"
                                             && i.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.CompanyId
                                             && i.BranchId == null
                                             select new OrderMainAccountVm()
                                             {
                                                 OrderMainAccountId = i.AccountId,
                                                 inventoryAccount = new accountsforSetSalesOrderAccountsMainAccounts() { AccountId = i.AccountId, AccountName = i.AccountName },
                                                 

                                             }).ToList();
                    var accountlist = (from acc in db.Accounts
                                       where acc.AccountCategory.AccountsDataTypes.TypeName == "اخري"
                                       && acc.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.CompanyId
                                       && acc.BranchId == null
                                       select new accountsforSetSalesOrderAccountsMainAccounts() {
                                           AccountId = acc.AccountId,
                                           AccountName = acc.AccountName

                                       }).ToList();

                    var mainList = new SetSalesOrderAccountsMainAccounts()
                    {
                        orderMainAccount = inventoryAccounts,
                        Account = accountlist

                    };

               
                    return Ok(mainList);
                }
                else
                {
                    return BadRequest("لا توجد شركة مسجلة بهذا الاسم");
                }
            }
            else
            {
                //Branch
                var branch = db.Branchs.Find(companyBranch.Id);
                if (branch != null)
                {
                    var inventoryAccounts = (from i in db.Accounts
                                             where i.AccountCategory.AccountsDataTypes.TypeName == "مخازن"
                                             && i.BranchId== branch.BranchId
                                             select new OrderMainAccountVm()
                                             {
                                                 OrderMainAccountId = i.AccountId,
                                                 inventoryAccount = new accountsforSetSalesOrderAccountsMainAccounts() { AccountId = i.AccountId, AccountName = i.AccountName },


                                             }).ToList();
                    var accountlist = (from acc in db.Accounts
                                       where acc.AccountCategory.AccountsDataTypes.TypeName == "اخري"
                                       && acc.BranchId == branch.BranchId
                                       select new accountsforSetSalesOrderAccountsMainAccounts()
                                       {
                                           AccountId = acc.AccountId,
                                           AccountName = acc.AccountName

                                       }).ToList();

                    var mainList = new SetSalesOrderAccountsMainAccounts()
                    {
                        orderMainAccount = inventoryAccounts,
                        Account = accountlist

                    };


                    return Ok(mainList);
                }
                else
                {
                    return BadRequest("لا يوجد فرع مسجل بهذاالاسم");
                }
            }
        }


        [Route("GetOrderAccountsForSubitems")]
        [HttpPost]
        public IHttpActionResult GetOrderAccountsForSubitems(isCompanyBranch companyBranch)
        {
            if (companyBranch.IsCompany)
            {
                var company = db.Companies.Find(companyBranch.Id);
                if (company != null)
                {
                    var accounts = (from a in db.Accounts
                                    where a.BranchId == null
                                    && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.CompanyId
                                    && a.AccountCategory.AccountsDataTypes.TypeName == "اخري"
                                   select new OrderAccountsforSubAccountVm()
                                   {
                                       AccountId = a.AccountId,
                                       AccountName = a.AccountName,
                                       DebitWhenAdd = false

                                   }).ToList();

                    return Ok(accounts);
                }
                else
                {
                    return BadRequest("لا توجد شركة مسجلة بهذا الاسم");
                }
            }
            else
            {
                //Branch
                var branch = db.Branchs.Find(companyBranch.Id);
                if (branch != null)
                {

                    var accounts = (from a in db.Accounts
                                    where a.BranchId ==branch.BranchId
                                    && a.AccountCategory.AccountsDataTypes.TypeName == "اخري"
                                    select new OrderAccountsforSubAccountVm()
                                    {
                                        AccountId = a.AccountId,
                                        AccountName = a.AccountName,
                                        DebitWhenAdd = false

                                    }).ToList();

                    return Ok(accounts);
                }
                else
                {
                    return BadRequest("لا يوجد فرع مسجل بهذاالاسم");
                }
            }
        }

        [Route("saveBill")]
        [HttpPost]
        public IHttpActionResult savebill(SavebillVm bill)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            DateTime billdate = bill.billDate;

            if(bill.billType.typeName == "مشتريات" || bill.billType.typeName == "purReturns")
            {
                #region مشتريات
                if (bill.IsCompany)
                {
                    var isItemExsiest = db.PurchasesBills.Any(a => a.BillNo == bill.billNo && a.BranchId == null && a.CompanyId == bill.ComapnyId);
                    if (isItemExsiest)
                    {
                        return BadRequest("لا يمكن التعديل او اعادة التسجيل");
                    }
                }
                else
                {
                    var isItemExsiest = db.PurchasesBills.Any(a => a.BillNo == bill.billNo && a.BranchId == bill.ComapnyId);
                    if (!isItemExsiest)
                    {
                        return BadRequest("لا يمكن التعديل او اعادة التسجيل");
                    }
                }


                if (bill.billName.typeName == "buy")
                {
                    #region Purchases
                    var bills = new PurchasesBill();
                    bills.BillDate = billdate;
                    bills.BillMainAccountId = bill.billMainAccount.AccountId;
                    bills.BillTotalAccountId = bill.billTotalAccount.accountId;
                    bills.BillTitleAccountId = bill.billTitleaccount.AccountId;
                    bills.OrdersTypePurchasesId = bill.billName.typeId;
                    bills.OrdersTypePurchasesType = bill.billName.typeName;
                    bills.BillPaymethodId = bill.billTotalAccount.billPaymethods.payId;

                    var orderVm = new OrderVm();
                    var group = OrderGroupManger.getOrderbyId(bill.OrderGroupId);
                    #region items
                    //var itemList = new List<BillSubAccountItemsVm>();
                    List<string> ids = new List<string>();
                    List<ItemsRow> itemrows = new List<ItemsRow>();
                    foreach (var row in bill.billItems)
                    {
                        
                        var subAccountItem = new BillSubAccountItems();
                        var subAccounts= new BillAccountItems();
                        bool empity = false;
                        string name = this.AppCompanyUserManager.GetUniqueKey(4);
                        do
                        {
                            name = this.AppCompanyUserManager.GetUniqueKey(4);
                        } while (ids.Any(a => a == name));
                        ids.Add(name);
                        int num = 0;
                        foreach (var item in row)
                        {
                            num += 1;
                            if (bills.itemsRows == null)
                            {
                                bills.itemsRows = new List<ItemsRow>();
                            }
                            bills.itemsRows.Add(new ItemsRow()
                            {
                                itemPropertyValueId = item.itemPropertyValueId,
                                itemPropertyId = item.itemPropertyId,
                                itemPropertyValueName = item.itemPropertyValueName,
                                type = item.type,
                                value = name,
                                orderid = num 
                            });
                            if (item.type == "item")
                            {
                                if (bill.IsCompany)
                                {
                                    var isItemExsiest = db.SubAccounts.Any(a => a.SubAccountId == item.itemPropertyValueId && a.Account.BranchId == null && a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == bill.ComapnyId);
                                    if (!isItemExsiest)
                                    {
                                        return BadRequest(string.Format("لا يوجد صنف مسجل بهذا الاسم {0}", item.itemPropertyValueName));
                                    }
                                }
                                else
                                {
                                    var isItemExsiest = db.SubAccounts.Any(a => a.SubAccountId == item.itemPropertyValueId && a.Account.BranchId == bill.ComapnyId);
                                    if (!isItemExsiest)
                                    {
                                        return BadRequest(string.Format("لا يوجد صنف مسجل بهذا الاسم {0}", item.itemPropertyValueName));
                                    }
                                }
                                subAccountItem.SubAccountId = item.itemPropertyValueId;
                                subAccountItem.uniqueId = name;
                            }
                            if (item.type == "price")
                            {
                                if (Convert.ToDecimal(item.itemPropertyValueName) <= 0)
                                {
                                    return BadRequest("يجب ان يكون السعر اكبر من صفر");
                                }
                                subAccountItem.price = Convert.ToDecimal(item.itemPropertyValueName);

                            }
                            if (item.type == "qty")
                            {
                                if (Convert.ToDecimal(item.itemPropertyValueName) <= 0)
                                {
                                    return BadRequest("يجب ان تكون الكمية اكبر من صفر");
                                }
                                subAccountItem.qty = Convert.ToDecimal(item.itemPropertyValueName); ;

                            }
                            if (item.type == "crdit" || item.type == "debit")
                            {
                                empity = true;
                                if (Convert.ToDecimal(item.itemPropertyValueName) < 0)
                                {
                                    return BadRequest("لا يمكن تسجيل قيمة سالبة للحساب");
                                }
                                if (bill.IsCompany)
                                {
                                    var isItemExsiest = db.Accounts.Any(a => a.AccountId == item.itemPropertyValueId && a.BranchId == null && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == bill.ComapnyId);
                                    if (!isItemExsiest)
                                    {
                                        return BadRequest("تأكد من ان جميع الحسابات مسجلة");
                                    }
                                }
                                else
                                {
                                    var isItemExsiest = db.Accounts.Any(a => a.AccountId == item.itemPropertyValueId && a.BranchId == bill.ComapnyId);
                                    if (!isItemExsiest)
                                    {
                                        return BadRequest("تأكد من ان جميع الحسابات مسجلة");
                                    }
                                }
                                subAccounts.AccountId = item.itemPropertyValueId;
                                subAccounts.value = Convert.ToDecimal(item.itemPropertyValueName);
                                subAccounts.uniqueId = name;
                            }

                        }
                        
                        if (bills.BillSubAccountItems == null)
                        {
                            bills.BillSubAccountItems = new List<BillSubAccountItems>();
                        }
                        bills.BillSubAccountItems.Add(subAccountItem);
                        if (bills.BillAccountItems == null)
                        {
                            bills.BillAccountItems = new List<BillAccountItems>();
                        }
                      
                        if (empity)
                        {
                            bills.BillAccountItems.Add(subAccounts);
                        }
                        

                    }

                    var itemSum = bills.BillSubAccountItems.GroupBy(a => a.SubAccountId).Select(g => new
                    {
                        SubAccounId = g.Key,
                        Price = g.Sum(a => a.price),
                        qty = g.Sum(a => a.qty),
                        total = g.Sum(a => a.price) * g.Sum(a => a.qty)
                    }).ToList();
                    #endregion
                    #region itemAccounts
                    // var itemAccountList = new List<BillAccountItemsVm>();
                   

                    var itemAccountSum = bills.BillAccountItems.GroupBy(a => a.AccountId).Select(g => new
                    {
                        AccounId = g.Key,
                        DebitWhenAdd = group.purchases.orderAccountsforSubAccounts.SingleOrDefault(a => a.AccountId == g.Key).DebitWhenAdd,
                        DebitForPlus = group.purchases.orderAccountsforSubAccounts.SingleOrDefault(a => a.AccountId == g.Key).DebitforPlus,
                        IsNewOrder = group.purchases.orderAccountsforSubAccounts.SingleOrDefault(a => a.AccountId == g.Key).IsNewOrder,

                        value = g.Sum(a => a.value)
                    }).ToList();
                    #endregion

                    #region other Acounts Total

                    foreach (var item in bill.billOtherTotalAcounts)
                    {
                        if (Convert.ToDecimal(item.value) < 0)
                        {
                            return BadRequest("لا يمكن تسجيل قيمة سالبة لاي حساب");
                        }
                        if (bill.IsCompany)
                        {
                            var isItemExsiest = db.Accounts.Any(a => a.AccountId == item.accountId && a.BranchId == null && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == bill.ComapnyId);
                            if (!isItemExsiest)
                            {
                                return BadRequest("تأكد من ان جميع الحسابات مسجلة");
                            }
                        }
                        else
                        {
                            var isItemExsiest = db.Accounts.Any(a => a.AccountId == item.accountId && a.BranchId == bill.ComapnyId);
                            if (!isItemExsiest)
                            {
                                return BadRequest("تأكد من ان جميع الحسابات مسجلة");
                            }
                        }
                        if (bills.BillOtherAccountBills == null)
                        {
                            bills.BillOtherAccountBills = new List<BillOtherAccountBill>();
                        }

                        bills.BillOtherAccountBills.Add(new BillOtherAccountBill
                        {
                            AccountId = item.accountId,
                            value = item.value,
                            IsDebit = item.type == "debit" ? true : false
                        });
                    }
                    var OtherAccountSum = bill.billOtherTotalAcounts.GroupBy(a => a.accountId).Select(g => new
                    {
                        AccounId = g.Key,
                        DebitWhenAdd = group.purchases.orderOtherTotalAccounts.SingleOrDefault(a => a.AccountId == g.Key).DebitWhenAdd,
                        DebitForPlus = group.purchases.orderOtherTotalAccounts.SingleOrDefault(a => a.AccountId == g.Key).DebitforPlus,
                        IsNewOrder = group.purchases.orderOtherTotalAccounts.SingleOrDefault(a => a.AccountId == g.Key).IsNewOrder,
                        value = g.Sum(a => a.value)
                    }).ToList();
                    #endregion
                    #region subaccountsMoves
                    List<SubAccountMovement> subAccountsList = new List<SubAccountMovement>();

                    foreach (var row in bill.billItems)
                    {
                        var subaccMove = new SubAccountMovement();
                        subaccMove.ItemPricein = Convert.ToDecimal(row.SingleOrDefault(a => a.type == "price").itemPropertyValueName);
                        subaccMove.ItemTotalPriceIn = Convert.ToDecimal(row.SingleOrDefault(a => a.type == "price").itemPropertyValueName)
                            * Convert.ToDecimal(row.SingleOrDefault(a => a.type == "qty").itemPropertyValueName);
                        subaccMove.QuantityIn = Convert.ToDecimal(row.SingleOrDefault(a => a.type == "qty").itemPropertyValueName);

                        foreach (var item in row)
                        {
                            if (item.type == "item")
                            {
                                subaccMove.SubAccountId = item.itemPropertyValueId;
                            }
                            if (item.type == "crdit")
                            {
                                var addtoitem = group.purchases.orderAccountsforSubAccounts.Any(a => a.AccountId == item.itemPropertyId && a.CostEffect == true);
                                if (addtoitem)
                                {
                                    //subaccMove.ItemPricein -= Convert.ToDecimal(item.itemPropertyValueName);

                                }
                            }
                            if (item.type == "debit")
                            {
                                var addtoitem = group.purchases.orderAccountsforSubAccounts.Any(a => a.AccountId == item.itemPropertyId && a.CostEffect == true);
                                if (addtoitem)
                                {
                                    // subaccMove.ItemPricein += Convert.ToDecimal(item.itemPropertyValueName);

                                }
                            }
                        }

                        subAccountsList.Add(subaccMove);
                    }

                    var totalSubAccounts = subAccountsList.Select(a => a.ItemTotalPriceIn).DefaultIfEmpty(0).Sum();
                    foreach (var item in bills.BillOtherAccountBills)
                    {
                        if (group.purchases.orderOtherTotalAccounts.Any(a => a.AccountId == item.AccountId && a.CostEffect == true))
                        {

                            if (item.IsDebit == true)
                            {
                                foreach (var subacc in subAccountsList)
                                {
                                    var precnt = subacc.ItemTotalPriceIn / 100 * item.value;
                                    subacc.ItemPricein = (subacc.ItemTotalPriceIn + precnt) / subacc.QuantityIn;
                                    subacc.ItemTotalPriceIn = subacc.QuantityIn * subacc.ItemPricein;

                                }
                            }
                            if (item.IsDebit == false)
                            {
                                foreach (var subacc in subAccountsList)
                                {
                                    var precnt = subacc.ItemTotalPriceIn / 100 * item.value;
                                    subacc.ItemPricein = (subacc.ItemTotalPriceIn - precnt) / subacc.QuantityIn;
                                    subacc.ItemTotalPriceIn = subacc.QuantityIn * subacc.ItemPricein;

                                }
                            }
                        }
                        else
                        {
                            foreach (var subacc in subAccountsList)
                            {

                                subacc.ItemPricein = (subacc.ItemTotalPriceIn) / subacc.QuantityIn;
                                subacc.ItemTotalPriceIn = subacc.QuantityIn * subacc.ItemPricein;

                            }
                        }

                    }

                    foreach (var item in subAccountsList)
                    {
                        //we will use avg property for Now
                        var lastStocktotalPrice = db.SubAccountMovements.Where(a => a.SubAccountId == item.SubAccountId && a.SubAccountMovementDate == (db.SubAccountMovements.Where(s => s.SubAccountId == item.SubAccountId).Select(d => d.SubAccountMovementDate).Max())).Select(a => a.ItemTotalStockPrice).DefaultIfEmpty(0).FirstOrDefault();
                        var lastStocktotalQty = db.SubAccountMovements.Where(a => a.SubAccountId == item.SubAccountId && a.SubAccountMovementDate == (db.SubAccountMovements.Where(s => s.SubAccountId == item.SubAccountId).Select(d => d.SubAccountMovementDate).Max())).Select(a => a.ItemStockQuantity).DefaultIfEmpty(0).FirstOrDefault();
                        item.ItemStockQuantity = lastStocktotalQty + item.QuantityIn;
                        item.ItemTotalStockPrice = lastStocktotalPrice + item.ItemTotalPriceIn;
                        //useing AVG
                        item.ItemStockPrice = (lastStocktotalPrice + item.ItemTotalPriceIn) / (lastStocktotalQty + item.QuantityIn);
                        item.OrderNote = "مشتريات";
                        item.CurrentWorkerId = uid;
                        item.SubAccountMovementDate = billdate;
                    };
                    SubAccountOrder subaccountOrder = new SubAccountOrder();
                    subaccountOrder.CurrentWorkerId = uid;
                    if (subaccountOrder.SubAccountMovements == null)
                    {
                        subaccountOrder.SubAccountMovements = new List<SubAccountMovement>();
                    }

                    subaccountOrder.SubAccountMovements = subAccountsList;
                    subaccountOrder.OrderNote = "مشتريات";
                    subaccountOrder.OrderDate = billdate;
                    #endregion

                    var InventoryItemsDebit = new AccountMovement();
                    var payableAccountCrdit = new AccountMovement();
                    var billsubaccounts = bills.BillAccountItems.GroupBy(a => a.AccountId).Select(grb => new
                    {
                        AccountId = grb.Key,
                        Value = grb.Sum(a => a.value),
                        isNewOrder = group.purchases.orderAccountsforSubAccounts.Any(a => a.AccountId == grb.Key && a.IsNewOrder == true),
                        DebitWhenAdd = group.purchases.orderAccountsforSubAccounts.Any(a => a.AccountId == grb.Key && a.DebitWhenAdd == true),
                        PlusSing = group.purchases.orderAccountsforSubAccounts.Any(a => a.AccountId == grb.Key && a.DebitforPlus == true)
                    });
                    var orderList = new List<AccountOrder>();
                    var AccountMoves = new List<AccountMovement>();
                    var listCrditorList = new List<AccountMovement>();
                    var accountOrder = new AccountOrder();


                    foreach (var item in billsubaccounts)
                    {
                        if (item.Value != 0 && item.Value > 0)
                        {
                            var accMove = new AccountMovement();
                            accMove.Debit = item.DebitWhenAdd == true ? item.Value : 0;
                            accMove.AccountId = item.AccountId;
                            accMove.AccountMovementDate = billdate;
                            accMove.CurrentWorkerId = uid;
                            accMove.Crdit = item.DebitWhenAdd == true ? 0 : item.Value;
                            AccountMoves.Add(accMove);
                        }

                    }
                    foreach (var item in bill.billOtherTotalAcounts)
                    {
                        if (item.value != 0 && item.value > 0)
                        {
                            var isDebit = group.purchases.orderOtherTotalAccounts.Any(a => a.AccountId == item.accountId && a.DebitWhenAdd == true);
                            var accMove = new AccountMovement();
                            accMove.Debit = isDebit == true ? item.value : 0;
                            accMove.AccountId = item.accountId;
                            accMove.AccountMovementDate = billdate;
                            accMove.CurrentWorkerId = uid;
                            accMove.Crdit = isDebit == true ? 0 : item.value;
                            AccountMoves.Add(accMove);
                        }
                    }
                    bill.billMainAccount.value = bills.BillSubAccountItems.Select(a => a.price * a.qty).DefaultIfEmpty(0).Sum().ToString();
                    decimal mainvalue = bills.BillSubAccountItems.Select(a => a.price * a.qty).DefaultIfEmpty(0).Sum();
                    decimal totalValue = 0;
                    foreach (var item in AccountMoves)
                    {
                        if (item.Debit > 0)
                        {
                            totalValue += item.Debit;
                        }
                        if (item.Crdit > 0)
                        {
                            totalValue -= item.Crdit;
                        }
                    }
                    bill.billTotalAccount.value = bills.BillSubAccountItems.Select(a => a.price * a.qty).DefaultIfEmpty(0).Sum() + totalValue;

                    var totalmove = new AccountMovement()
                    {
                        AccountId = bills.BillTotalAccountId,
                        Crdit = bill.billTotalAccount.value,
                        AccountMovementDate = billdate,
                        CurrentWorkerId = uid,
                        Debit = 0
                    };
                    if (bill.IsCompany)
                    {
                        var isItemExsiest = db.Accounts.Any(a => a.AccountId == bills.BillTotalAccountId && a.BranchId == null && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == bill.ComapnyId);
                        if (!isItemExsiest)
                        {
                            return BadRequest("تأكد من ان جميع الحسابات مسجلة");
                        }
                    }
                    else
                    {
                        var isItemExsiest = db.Accounts.Any(a => a.AccountId == bills.BillTotalAccountId && a.BranchId == bill.ComapnyId);
                        if (!isItemExsiest)
                        {
                            return BadRequest("تأكد من ان جميع الحسابات مسجلة");
                        }
                    }
                    AccountMoves.Add(totalmove);
                    var Mainmove = new AccountMovement()
                    {
                        AccountId = bills.BillMainAccountId,
                        Crdit = 0,
                        AccountMovementDate = billdate,
                        CurrentWorkerId = uid,
                        Debit = mainvalue
                    };
                    if (bill.IsCompany)
                    {
                        var isItemExsiest = db.Accounts.Any(a => a.AccountId == bills.BillMainAccountId && a.BranchId == null && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == bill.ComapnyId);
                        if (!isItemExsiest)
                        {
                            return BadRequest("تأكد من ان جميع الحسابات مسجلة");
                        }
                    }
                    else
                    {
                        var isItemExsiest = db.Accounts.Any(a => a.AccountId == bills.BillMainAccountId && a.BranchId == bill.ComapnyId);
                        if (!isItemExsiest)
                        {
                            return BadRequest("تأكد من ان جميع الحسابات مسجلة");
                        }
                    }
                    AccountMoves.Add(Mainmove);
                    accountOrder.OrderDate = billdate;

                    accountOrder.IsCompany = bill.IsCompany;
                    if (accountOrder.AccountMovements == null)
                    {
                        accountOrder.AccountMovements = new List<AccountMovement>();
                    }
                    accountOrder.AccountMovements = AccountMoves;
                    var orderNodebit = 1;
                    var billId = 1;
                    var comId = 0;
                    if (bill.IsCompany)
                    {
                        comId = db.Companies.Find(bill.ComapnyId).CompanyId;
                        orderNodebit = db.AccountOrders.Where(a => a.CompanyId == bill.ComapnyId).Count() != 0 ? db.AccountOrders.Where(a => a.CompanyId == bill.ComapnyId).Max(a => a.OrderNo) + 1 : 1;
                        billId = db.PurchasesBills.Where(a => a.CompanyId == bill.ComapnyId && a.IsCompany == true && a.OrdersTypePurchasesType == bill.billName.typeName).Select(a => a.BillNo).DefaultIfEmpty(0).Max() + 1;

                    }

                    else
                    {
                        comId = db.Branchs.Find(bill.ComapnyId).CompanyId;
                        orderNodebit = db.AccountOrders.Where(a => a.BranchId == bill.ComapnyId).Count() != 0 ? db.AccountOrders.Where(a => a.BranchId == bill.ComapnyId).Max(a => a.OrderNo) + 1 : 1;
                        billId = db.PurchasesBills.Where(a => a.BranchId == bill.ComapnyId && a.IsCompany == false && a.OrdersTypePurchasesType == bill.billName.typeName).Select(a => a.BillNo).DefaultIfEmpty(0).Max() + 1;

                    }
                    bills.BillNo = billId;
                    bills.IsCompany = bill.IsCompany;
                    bills.PurchasesBillName = bill.billName.billName;
                    bills.type = bill.billName.typeName;
                    accountOrder.OrderNote = string.Format("{0} - رقم {1} - {2}", bill.billName.billName, billId, bill.billTotalAccount.billPaymethods.payName);
                    accountOrder.AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId;

                    accountOrder.OrderNo = orderNodebit;
                    accountOrder.CompanyId = comId;

                    if (bill.IsCompany)
                    {
                        accountOrder.BranchId = null;
                        bills.CompanyId = bill.ComapnyId;
                        bills.BranchId = null;
                    }
                    else
                    {
                        accountOrder.BranchId = bill.ComapnyId;
                        bills.CompanyId = null;
                        bills.BranchId = bill.ComapnyId;

                    }

                    foreach (var property in bill.billTotalAccount.billPaymethods.billPaymethodProperties)
                    {

                        bills.BillPaymentProperties.Add(new BillPaymentProperties
                        {
                            OrderPropertiesPayPurchasesId = property.Id,
                            value = property.value,
                        });

                    }
                    db.SubAccountOrders.Add(subaccountOrder);

                    if (accountOrder.AccountMovements != null && accountOrder.AccountMovements.Count > 0)
                    {
                        bills.CurrentWorkerId = uid;
                        accountOrder.OrderIdRefrence = bills.BillNo;
                        accountOrder.OrderTypeRefrence = "مشتريات";
                        accountOrder.IsSales = false;
                        db.AccountOrders.Add(accountOrder);
                        db.PurchasesBills.Add(bills);
                        db.SaveChanges();
                        return Ok(bills.BillNo);
                    }
                    else
                    {
                        return BadRequest();
                    }
                    #endregion
                }
                else if (bill.billName.typeName == "returnd")
                {
                    #region Return Purchases
                    var bills = new PurchasesBill();
                    bills.BillDate = billdate;
                    bills.BillMainAccountId = bill.billMainAccount.AccountId;
                    bills.BillTotalAccountId = bill.billTotalAccount.accountId;
                    bills.BillTitleAccountId = bill.billTitleaccount.AccountId;
                    bills.OrdersTypePurchasesId = bill.billName.typeId;
                    bills.OrdersTypePurchasesType = bill.billName.typeName;
                    bills.BillPaymethodId = bill.billTotalAccount.billPaymethods.payId;

                    var orderVm = new OrderVm();
                    var group = OrderGroupManger.getOrderbyId(bill.OrderGroupId);
                    #region items
                    //var itemList = new List<BillSubAccountItemsVm>();
                    List<string> ids = new List<string>();
                    foreach (var row in bill.billItems)
                    {
                        var subAccountItem = new BillSubAccountItems();
                        var subAccounts = new BillAccountItems();
                        bool empity = false;
                        string name = this.AppCompanyUserManager.GetUniqueKey(4);
                        do
                        {
                            name = this.AppCompanyUserManager.GetUniqueKey(4);
                        } while (ids.Any(a => a == name));
                        ids.Add(name);
                        int num = 0;
                        foreach (var item in row)
                        {
                            num += 1;
                            if (bills.itemsRows == null)
                            {
                                bills.itemsRows = new List<ItemsRow>();
                            }
                            bills.itemsRows.Add(new ItemsRow()
                            {
                                itemPropertyValueId = item.itemPropertyValueId,
                                itemPropertyId = item.itemPropertyId,
                                itemPropertyValueName = item.itemPropertyValueName,
                                type = item.type,
                                value = name,
                                orderid = num
                            });
                            if (item.type == "item")
                            {
                                if (bill.IsCompany)
                                {
                                    var isItemExsiest = db.SubAccounts.Any(a => a.SubAccountId == item.itemPropertyValueId && a.Account.BranchId == null && a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == bill.ComapnyId);
                                    if (!isItemExsiest)
                                    {
                                        return BadRequest(string.Format("لا يوجد صنف مسجل بهذا الاسم {0}", item.itemPropertyValueName));
                                    }
                                }
                                else
                                {
                                    var isItemExsiest = db.SubAccounts.Any(a => a.SubAccountId == item.itemPropertyValueId && a.Account.BranchId == bill.ComapnyId);
                                    if (!isItemExsiest)
                                    {
                                        return BadRequest(string.Format("لا يوجد صنف مسجل بهذا الاسم {0}", item.itemPropertyValueName));
                                    }
                                }
                                subAccountItem.SubAccountId = item.itemPropertyValueId;
                                subAccountItem.uniqueId = name;
                            }
                            if (item.type == "price")
                            {
                                if (Convert.ToDecimal(item.itemPropertyValueName) <= 0)
                                {
                                    return BadRequest("يجب ان يكون السعر اكبر من صفر");
                                }
                                subAccountItem.price = Convert.ToDecimal(item.itemPropertyValueName);

                            }
                            if (item.type == "qty")
                            {
                                if (Convert.ToDecimal(item.itemPropertyValueName) <= 0)
                                {
                                    return BadRequest("يجب ان تكون الكمية اكبر من صفر");
                                }
                                subAccountItem.qty = Convert.ToDecimal(item.itemPropertyValueName); ;

                            }
                            if (item.type == "crdit" || item.type == "debit")
                            {
                                empity = true;
                                if (Convert.ToDecimal(item.itemPropertyValueName) < 0)
                                {
                                    return BadRequest("لا يمكن تسجيل قيمة سالبة للحساب");
                                }
                                if (bill.IsCompany)
                                {
                                    var isItemExsiest = db.Accounts.Any(a => a.AccountId == item.itemPropertyValueId && a.BranchId == null && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == bill.ComapnyId);
                                    if (!isItemExsiest)
                                    {
                                        return BadRequest("تأكد من ان جميع الحسابات مسجلة");
                                    }
                                }
                                else
                                {
                                    var isItemExsiest = db.Accounts.Any(a => a.AccountId == item.itemPropertyValueId && a.BranchId == bill.ComapnyId);
                                    if (!isItemExsiest)
                                    {
                                        return BadRequest("تأكد من ان جميع الحسابات مسجلة");
                                    }
                                }
                                subAccounts.AccountId = item.itemPropertyValueId;
                                subAccounts.value = Convert.ToDecimal(item.itemPropertyValueName);
                                subAccounts.uniqueId = name;
                            }

                        }

                        if (bills.BillSubAccountItems == null)
                        {
                            bills.BillSubAccountItems = new List<BillSubAccountItems>();
                        }
                        bills.BillSubAccountItems.Add(subAccountItem);
                        if (bills.BillAccountItems == null)
                        {
                            bills.BillAccountItems = new List<BillAccountItems>();
                        }

                        if (empity)
                        {
                            bills.BillAccountItems.Add(subAccounts);
                        }


                    }

                    var itemSum = bills.BillSubAccountItems.GroupBy(a => a.SubAccountId).Select(g => new
                    {
                        SubAccounId = g.Key,
                        Price = g.Sum(a => a.price),
                        qty = g.Sum(a => a.qty),
                        total = g.Sum(a => a.price) * g.Sum(a => a.qty)
                    }).ToList();
                    #endregion
                    #region itemAccounts
                    // var itemAccountList = new List<BillAccountItemsVm>();
              

                    var itemAccountSum = bills.BillAccountItems.GroupBy(a => a.AccountId).Select(g => new
                    {
                        AccounId = g.Key,
                        DebitWhenAdd = group.purchases.orderAccountsforSubAccounts.SingleOrDefault(a => a.AccountId == g.Key).DebitWhenAdd,
                        DebitForPlus = group.purchases.orderAccountsforSubAccounts.SingleOrDefault(a => a.AccountId == g.Key).DebitforPlus,
                        IsNewOrder = group.purchases.orderAccountsforSubAccounts.SingleOrDefault(a => a.AccountId == g.Key).IsNewOrder,

                        value = g.Sum(a => a.value)
                    }).ToList();
                    #endregion

                    #region other Acounts Total

                    foreach (var item in bill.billOtherTotalAcounts)
                    {
                        if (Convert.ToDecimal(item.value) < 0)
                        {
                            return BadRequest("لا يمكن تسجيل قيمة سالبة لاي حساب");
                        }
                        if (bill.IsCompany)
                        {
                            var isItemExsiest = db.Accounts.Any(a => a.AccountId == item.accountId && a.BranchId == null && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == bill.ComapnyId);
                            if (!isItemExsiest)
                            {
                                return BadRequest(string.Format("لا يوجد حساب مسجل بهذا الاسم {0}", item.accountName));
                            }
                        }
                        else
                        {
                            var isItemExsiest = db.Accounts.Any(a => a.AccountId == item.accountId && a.BranchId == bill.ComapnyId);
                            if (!isItemExsiest)
                            {
                                return BadRequest(string.Format("لا يوجد حساب مسجل بهذا الاسم {0}", item.accountName));
                            }
                        }
                        if (bills.BillOtherAccountBills == null)
                        {
                            bills.BillOtherAccountBills = new List<BillOtherAccountBill>();
                        }

                        bills.BillOtherAccountBills.Add(new BillOtherAccountBill
                        {
                            AccountId = item.accountId,
                            value = item.value,
                            IsDebit = item.type == "debit" ? true : false
                        });
                    }
                    var OtherAccountSum = bill.billOtherTotalAcounts.GroupBy(a => a.accountId).Select(g => new
                    {
                        AccounId = g.Key,
                        DebitWhenAdd = group.purchases.orderOtherTotalAccounts.SingleOrDefault(a => a.AccountId == g.Key).DebitWhenAdd,
                        DebitForPlus = group.purchases.orderOtherTotalAccounts.SingleOrDefault(a => a.AccountId == g.Key).DebitforPlus,
                        IsNewOrder = group.purchases.orderOtherTotalAccounts.SingleOrDefault(a => a.AccountId == g.Key).IsNewOrder,
                        value = g.Sum(a => a.value)
                    }).ToList();
                    #endregion
                    #region subaccountsMoves
                    List<SubAccountMovement> subAccountsList = new List<SubAccountMovement>();

                    foreach (var row in bill.billItems)
                    {
                        var subaccMove = new SubAccountMovement();
                        subaccMove.ItemPriceOut = Convert.ToDecimal(row.SingleOrDefault(a => a.type == "price").itemPropertyValueName);
                        subaccMove.ItemTotalPriceOut = Convert.ToDecimal(row.SingleOrDefault(a => a.type == "price").itemPropertyValueName)
                            * Convert.ToDecimal(row.SingleOrDefault(a => a.type == "qty").itemPropertyValueName);
                        subaccMove.QuantityOut = Convert.ToDecimal(row.SingleOrDefault(a => a.type == "qty").itemPropertyValueName);

                        foreach (var item in row)
                        {
                            if (item.type == "item")
                            {
                                if (bill.IsCompany)
                                {
                                    var isItemExsiest = db.SubAccounts.Any(a => a.SubAccountId == item.itemPropertyValueId && a.Account.BranchId == null && a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == bill.ComapnyId);
                                    if (!isItemExsiest)
                                    {
                                        return BadRequest(string.Format("لا يوجد صنف مسجل بهذا الاسم {0}", item.itemPropertyValueName));
                                    }
                                }
                                else
                                {
                                    var isItemExsiest = db.SubAccounts.Any(a => a.SubAccountId == item.itemPropertyValueId && a.Account.BranchId == bill.ComapnyId);
                                    if (!isItemExsiest)
                                    {
                                        return BadRequest(string.Format("لا يوجد صنف مسجل بهذا الاسم {0}", item.itemPropertyValueName));
                                    }
                                }
                                subaccMove.SubAccountId = item.itemPropertyValueId;
                            }
                            if (item.type == "crdit")
                            {
                                var addtoitem = group.purchases.orderAccountsforSubAccounts.Any(a => a.AccountId == item.itemPropertyId && a.CostEffect == true);
                                if (addtoitem)
                                {
                                    //subaccMove.ItemPricein -= Convert.ToDecimal(item.itemPropertyValueName);

                                }
                            }
                            if (item.type == "debit")
                            {
                                var addtoitem = group.purchases.orderAccountsforSubAccounts.Any(a => a.AccountId == item.itemPropertyId && a.CostEffect == true);
                                if (addtoitem)
                                {
                                    // subaccMove.ItemPricein += Convert.ToDecimal(item.itemPropertyValueName);

                                }
                            }
                        }

                        subAccountsList.Add(subaccMove);
                    }

                    // var totalSubAccounts = subAccountsList.Select(a => a.ItemTotalPriceIn).DefaultIfEmpty(0).Sum();
                    foreach (var item in bills.BillOtherAccountBills)
                    {
                        if (group.purchases.orderOtherTotalAccounts.Any(a => a.AccountId == item.AccountId && a.CostEffect == true))
                        {

                            if (item.IsDebit == true)
                            {
                                foreach (var subacc in subAccountsList)
                                {
                                    var precnt = subacc.ItemTotalPriceOut / 100 * item.value;
                                    subacc.ItemPriceOut = (subacc.ItemTotalPriceOut + precnt) / subacc.QuantityOut;
                                    subacc.ItemTotalPriceOut = subacc.QuantityOut * subacc.ItemPriceOut;

                                }
                            }
                            if (item.IsDebit == false)
                            {
                                foreach (var subacc in subAccountsList)
                                {
                                    var precnt = subacc.ItemTotalPriceOut / 100 * item.value;
                                    subacc.ItemPriceOut = (subacc.ItemTotalPriceOut - precnt) / subacc.QuantityOut;
                                    subacc.ItemTotalPriceOut = subacc.QuantityOut * subacc.ItemPriceOut;

                                }
                            }
                        }
                        else
                        {
                            foreach (var subacc in subAccountsList)
                            {

                                subacc.ItemPriceOut = (subacc.ItemTotalPriceOut) / subacc.QuantityOut;
                                subacc.ItemTotalPriceIn = subacc.QuantityOut * subacc.ItemPriceOut;

                            }
                        }

                    }

                    foreach (var item in subAccountsList)
                    {
                        //we will use avg property for Now
                        var lastStocktotalPrice = db.SubAccountMovements.Where(a => a.SubAccountId == item.SubAccountId && a.SubAccountMovementDate == (db.SubAccountMovements.Where(s => s.SubAccountId == item.SubAccountId).Select(d => d.SubAccountMovementDate).Max())).Select(a => a.ItemTotalStockPrice).DefaultIfEmpty(0).FirstOrDefault();
                        var lastStocktotalQty = db.SubAccountMovements.Where(a => a.SubAccountId == item.SubAccountId && a.SubAccountMovementDate == (db.SubAccountMovements.Where(s => s.SubAccountId == item.SubAccountId).Select(d => d.SubAccountMovementDate).Max())).Select(a => a.ItemStockQuantity).DefaultIfEmpty(0).FirstOrDefault();
                        var lastStockPrice = db.SubAccountMovements.Where(a => a.SubAccountId == item.SubAccountId && a.SubAccountMovementDate == (db.SubAccountMovements.Where(s => s.SubAccountId == item.SubAccountId).Select(d => d.SubAccountMovementDate).Max())).Select(a => a.ItemStockPrice).DefaultIfEmpty(0).FirstOrDefault();
                        if (lastStocktotalQty == 0 || lastStocktotalQty - item.QuantityOut < 0)
                        {
                            return BadRequest("كمية غير متاحة");
                        }
                        item.ItemStockQuantity = lastStocktotalQty - item.QuantityOut;
                        item.ItemTotalStockPrice = lastStocktotalPrice - item.ItemTotalPriceOut;
                        //useing AVG
                        item.ItemStockPrice = item.ItemTotalStockPrice / item.ItemStockQuantity;
                        item.OrderNote = "رد مشتريات";
                        item.CurrentWorkerId = uid;
                        item.SubAccountMovementDate = billdate;
                    };
                    SubAccountOrder subaccountOrder = new SubAccountOrder();
                    subaccountOrder.CurrentWorkerId = uid;
                    if (subaccountOrder.SubAccountMovements == null)
                    {
                        subaccountOrder.SubAccountMovements = new List<SubAccountMovement>();
                    }

                    subaccountOrder.SubAccountMovements = subAccountsList;
                    subaccountOrder.OrderNote = "رد مشتريات";
                    subaccountOrder.OrderDate = billdate;
                    #endregion

                    var InventoryItemsDebit = new AccountMovement();
                    var payableAccountCrdit = new AccountMovement();
                    var billsubaccounts = bills.BillAccountItems.GroupBy(a => a.AccountId).Select(grb => new
                    {
                        AccountId = grb.Key,
                        Value = grb.Sum(a => a.value),
                        isNewOrder = group.purchases.orderAccountsforSubAccounts.Any(a => a.AccountId == grb.Key && a.IsNewOrder == true),
                        DebitWhenAdd = group.purchases.orderAccountsforSubAccounts.Any(a => a.AccountId == grb.Key && a.DebitWhenAdd == true),
                        PlusSing = group.purchases.orderAccountsforSubAccounts.Any(a => a.AccountId == grb.Key && a.DebitforPlus == true)
                    });
                    var orderList = new List<AccountOrder>();
                    var AccountMoves = new List<AccountMovement>();
                    var listCrditorList = new List<AccountMovement>();
                    var accountOrder = new AccountOrder();


                    foreach (var item in billsubaccounts)
                    {
                        if (item.Value != 0 && item.Value > 0)
                        {
                            var accMove = new AccountMovement();
                            accMove.Debit = item.DebitWhenAdd == true ? 0 : item.Value;
                            accMove.AccountId = item.AccountId;
                            accMove.AccountMovementDate = billdate;
                            accMove.CurrentWorkerId = uid;
                            accMove.Crdit = item.DebitWhenAdd == true ? item.Value : 0;
                            AccountMoves.Add(accMove);
                        }

                    }
                    foreach (var item in bill.billOtherTotalAcounts)
                    {
                        if (item.value != 0 && item.value > 0)
                        {
                            var isDebit = group.purchases.orderOtherTotalAccounts.Any(a => a.AccountId == item.accountId && a.DebitWhenAdd == true);
                            var accMove = new AccountMovement();
                            accMove.Debit = isDebit == true ? 0 : item.value;
                            accMove.AccountId = item.accountId;
                            accMove.AccountMovementDate = billdate;
                            accMove.CurrentWorkerId = uid;
                            accMove.Crdit = isDebit == true ? item.value : 0;
                            AccountMoves.Add(accMove);
                        }


                    }
                    bill.billMainAccount.value = bills.BillSubAccountItems.Select(a => a.price * a.qty).DefaultIfEmpty(0).Sum().ToString();
                    decimal mainvalue = bills.BillSubAccountItems.Select(a => a.price * a.qty).DefaultIfEmpty(0).Sum();
                    decimal totalValue = 0;
                    foreach (var item in AccountMoves)
                    {
                        if (item.Debit > 0)
                        {
                            totalValue -= item.Debit;
                        }
                        if (item.Crdit > 0)
                        {
                            totalValue += item.Crdit;
                        }
                    }
                    bill.billTotalAccount.value = bills.BillSubAccountItems.Select(a => a.price * a.qty).DefaultIfEmpty(0).Sum() + totalValue;

                    var totalmove = new AccountMovement()
                    {
                        AccountId = bills.BillTotalAccountId,
                        Crdit = 0,
                        AccountMovementDate = billdate,
                        CurrentWorkerId = uid,
                        Debit = bill.billTotalAccount.value
                    };
                    AccountMoves.Add(totalmove);
                    var Mainmove = new AccountMovement()
                    {
                        AccountId = bills.BillMainAccountId,
                        Crdit = mainvalue,
                        AccountMovementDate = billdate,
                        CurrentWorkerId = uid,
                        Debit = 0
                    };
                    AccountMoves.Add(Mainmove);
                    accountOrder.OrderDate = billdate;

                    accountOrder.IsCompany = bill.IsCompany;
                    if (accountOrder.AccountMovements == null)
                    {
                        accountOrder.AccountMovements = new List<AccountMovement>();
                    }
                    accountOrder.AccountMovements = AccountMoves;
                    var orderNodebit = 1;
                    var billId = 1;
                    var comId = 0;
                    if (bill.IsCompany)
                    {
                        comId = db.Companies.Find(bill.ComapnyId).CompanyId;
                        orderNodebit = db.AccountOrders.Where(a => a.CompanyId == bill.ComapnyId).Count() != 0 ? db.AccountOrders.Where(a => a.CompanyId == bill.ComapnyId).Max(a => a.OrderNo) + 1 : 1;
                        billId = db.PurchasesBills.Where(a => a.CompanyId == bill.ComapnyId && a.IsCompany == true && a.OrdersTypePurchasesType == bill.billName.typeName).Select(a => a.BillNo).DefaultIfEmpty(0).Max() + 1;

                    }

                    else
                    {
                        comId = db.Branchs.Find(bill.ComapnyId).CompanyId;
                        orderNodebit = db.AccountOrders.Where(a => a.BranchId == bill.ComapnyId).Count() != 0 ? db.AccountOrders.Where(a => a.BranchId == bill.ComapnyId).Max(a => a.OrderNo) + 1 : 1;
                        billId = db.PurchasesBills.Where(a => a.BranchId == bill.ComapnyId && a.IsCompany == false && a.OrdersTypePurchasesType == bill.billName.typeName).Select(a => a.BillNo).DefaultIfEmpty(0).Max() + 1;

                    }
                    bills.BillNo = billId;
                    bills.IsCompany = bill.IsCompany;
                    bills.PurchasesBillName = bill.billName.billName;
                    bills.type = bill.billName.typeName;
                    accountOrder.OrderNote = string.Format("{0} - رقم {1} - {2}", bill.billName.billName, billId, bill.billTotalAccount.billPaymethods.payName);
                    accountOrder.AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId;

                    accountOrder.OrderNo = orderNodebit;
                    accountOrder.CompanyId = comId;

                    if (bill.IsCompany)
                    {
                        accountOrder.BranchId = null;
                        bills.CompanyId = bill.ComapnyId;
                        bills.BranchId = null;
                    }
                    else
                    {
                        accountOrder.BranchId = bill.ComapnyId;
                        bills.CompanyId = null;
                        bills.BranchId = bill.ComapnyId;

                    }

                    foreach (var property in bill.billTotalAccount.billPaymethods.billPaymethodProperties)
                    {

                        bills.BillPaymentProperties.Add(new BillPaymentProperties
                        {
                            OrderPropertiesPayPurchasesId = property.Id,
                            value = property.value,
                        });

                    }
                    db.SubAccountOrders.Add(subaccountOrder);

                    if (accountOrder.AccountMovements != null && accountOrder.AccountMovements.Count > 0)
                    {
                        bills.CurrentWorkerId = uid;
                        accountOrder.OrderIdRefrence = bills.BillNo;
                        accountOrder.OrderTypeRefrence = "رد مشتريات";
                        accountOrder.IsSales = false;
                        db.AccountOrders.Add(accountOrder);
                        db.PurchasesBills.Add(bills);
                        db.SaveChanges();

                        return Ok(bills.BillNo);
                    }
                    else
                    {
                        return BadRequest();
                    }
                    #endregion
                }
                else
                {
                    return BadRequest("لا يوجد اذن مسجل بهذا النوع");

                }
                #endregion

            }
           else if(bill.billType.typeName == "مبيعات" || bill.billType.typeName == "salesReturn")
            {
                
                #region مبيعات
                if (bill.IsCompany)
                {
                    var isItemExsiest = db.SalesBills.Any(a => a.BillNo == bill.billNo && a.BranchId == null && a.CompanyId == bill.ComapnyId);
                    if (isItemExsiest)
                    {
                        return BadRequest("لا يمكن التعديل او اعادة التسجيل");
                    }
                }
                else
                {
                    var isItemExsiest = db.SalesBills.Any(a => a.BillNo == bill.billNo && a.BranchId == bill.ComapnyId);
                    if (!isItemExsiest)
                    {
                        return BadRequest("لا يمكن التعديل او اعادة التسجيل");
                    }
                }


                if (bill.billName.typeName == "returnd")
                {

                    #region Sales Return 
                    var bills = new SalesBill();
                    bills.BillDate = billdate;
                    bills.BillMainAccountId = bill.billMainAccount.AccountId;
                    bills.BillTotalAccountId = bill.billTotalAccount.accountId;
                    bills.BillTitleAccountId = bill.billTitleaccount.AccountId;
                    bills.OrdersTypeSalesId = bill.billName.typeId;
                    bills.OrdersTypeSalesType = bill.billName.typeName;
                    bills.BillPaymethodId = bill.billTotalAccount.billPaymethods.payId;

                    var group =OrderGroupManger.getOrderbyId(bill.OrderGroupId);
                    #region items
                    //var itemList = new List<BillSubAccountItemsVm>();
                    List<string> ids = new List<string>();
                    foreach (var row in bill.billItems)
                    {
                        var subAccountItem = new BillSubAccountItems();
                        var subAccounts = new BillAccountItems();
                        bool empity = false;
                        string name = this.AppCompanyUserManager.GetUniqueKey(4);
                        do
                        {
                            name = this.AppCompanyUserManager.GetUniqueKey(4);
                        } while (ids.Any(a => a == name));
                        ids.Add(name);
                        int num = 0;
                        foreach (var item in row)
                        {
                            num += 1;
                            if (bills.itemsRows == null)
                            {
                                bills.itemsRows = new List<ItemsRow>();
                            }
                            bills.itemsRows.Add(new ItemsRow()
                            {
                                itemPropertyValueId = item.itemPropertyValueId,
                                itemPropertyId = item.itemPropertyId,
                                itemPropertyValueName = item.itemPropertyValueName,
                                type = item.type,
                                value = name,
                                orderid = num
                            });
                            if (item.type == "item")
                            {
                                if (bill.IsCompany)
                                {
                                    var isItemExsiest = db.SubAccounts.Any(a => a.SubAccountId == item.itemPropertyValueId && a.Account.BranchId == null && a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == bill.ComapnyId);
                                    if (!isItemExsiest)
                                    {
                                        return BadRequest(string.Format("لا يوجد صنف مسجل بهذا الاسم {0}", item.itemPropertyValueName));
                                    }
                                }
                                else
                                {
                                    var isItemExsiest = db.SubAccounts.Any(a => a.SubAccountId == item.itemPropertyValueId && a.Account.BranchId == bill.ComapnyId);
                                    if (!isItemExsiest)
                                    {
                                        return BadRequest(string.Format("لا يوجد صنف مسجل بهذا الاسم {0}", item.itemPropertyValueName));
                                    }
                                }
                                subAccountItem.SubAccountId = item.itemPropertyValueId;
                                subAccountItem.uniqueId = name;
                            }
                            if (item.type == "price")
                            {
                                if (Convert.ToDecimal(item.itemPropertyValueName) <= 0)
                                {
                                    return BadRequest("يجب ان يكون السعر اكبر من صفر");
                                }
                                subAccountItem.price = Convert.ToDecimal(item.itemPropertyValueName);

                            }
                            if (item.type == "qty")
                            {
                                if (Convert.ToDecimal(item.itemPropertyValueName) <= 0)
                                {
                                    return BadRequest("يجب ان تكون الكمية اكبر من صفر");
                                }
                                subAccountItem.qty = Convert.ToDecimal(item.itemPropertyValueName); ;

                            }
                            if (item.type == "crdit" || item.type == "debit")
                            {
                                empity = true;
                                if (Convert.ToDecimal(item.itemPropertyValueName) < 0)
                                {
                                    return BadRequest("لا يمكن تسجيل قيمة سالبة للحساب");
                                }
                                if (bill.IsCompany)
                                {
                                    var isItemExsiest = db.Accounts.Any(a => a.AccountId == item.itemPropertyValueId && a.BranchId == null && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == bill.ComapnyId);
                                    if (!isItemExsiest)
                                    {
                                        return BadRequest("تأكد من ان جميع الحسابات مسجلة");
                                    }
                                }
                                else
                                {
                                    var isItemExsiest = db.Accounts.Any(a => a.AccountId == item.itemPropertyValueId && a.BranchId == bill.ComapnyId);
                                    if (!isItemExsiest)
                                    {
                                        return BadRequest("تأكد من ان جميع الحسابات مسجلة");
                                    }
                                }
                                subAccounts.AccountId = item.itemPropertyValueId;
                                subAccounts.value = Convert.ToDecimal(item.itemPropertyValueName);
                                subAccounts.uniqueId = name;
                            }

                        }

                        if (bills.BillSubAccountItems == null)
                        {
                            bills.BillSubAccountItems = new List<BillSubAccountItems>();
                        }
                        bills.BillSubAccountItems.Add(subAccountItem);
                        if (bills.BillAccountItems == null)
                        {
                            bills.BillAccountItems = new List<BillAccountItems>();
                        }

                        if (empity)
                        {
                            bills.BillAccountItems.Add(subAccounts);
                        }


                    }

                    var itemSum = bills.BillSubAccountItems.GroupBy(a => a.SubAccountId).Select(g => new
                    {
                        SubAccounId = g.Key,
                        Price = g.Sum(a => a.price),
                        qty = g.Sum(a => a.qty),
                        total = g.Sum(a => a.price) * g.Sum(a => a.qty)
                    }).ToList();
                    #endregion
                    var AccountOrderBySales = new AccountOrder();
                    var AccountOrderByCost = new AccountOrder();
                   
                    var inventoryAccounts = group.sales.orderMainAccounts.SingleOrDefault(a => a.inventoryAccount.AccountId == bill.billMainAccount.AccountId);
                    if (inventoryAccounts == null)
                    {
                        return BadRequest("تأكد من اكتمال حسابات المخازن في الاعدادات");
                    }

                    #region القيد بسعر البيع
                    var totalPrice = itemSum.Select(a => a.total).DefaultIfEmpty(0).Sum();
                    var accountReturn = new AccountMovement()
                    {
                        AccountId = inventoryAccounts.salesreturnAccount.AccountId,
                        Debit = totalPrice,
                        Crdit = 0,
                        AccountMovementDate = billdate,
                        AccountMovementNote = "رد مبيعات",
                        CurrentWorkerId = uid
                    };
                    if (AccountOrderBySales.AccountMovements == null)
                    {
                        AccountOrderBySales.AccountMovements = new List<AccountMovement>();
                    }
                    AccountOrderBySales.AccountMovements.Add(accountReturn);
                    #endregion
                    #region القيد بسعر التكلفة
                    var subaccountMoves = new List<SubAccountMovement>();
                    foreach (var item in itemSum)
                    {

                        var subAccountMove = new SubAccountMovement();
                        var lastStocktotalPrice = db.SubAccountMovements.Where(a => a.SubAccountId == item.SubAccounId && a.SubAccountMovementDate == (db.SubAccountMovements.Where(s => s.SubAccountId == item.SubAccounId).Select(d => d.SubAccountMovementDate).Max())).Select(a => a.ItemTotalStockPrice).DefaultIfEmpty(0).FirstOrDefault();
                        var lastStocktotalQty = db.SubAccountMovements.Where(a => a.SubAccountId == item.SubAccounId && a.SubAccountMovementDate == (db.SubAccountMovements.Where(s => s.SubAccountId == item.SubAccounId).Select(d => d.SubAccountMovementDate).Max())).Select(a => a.ItemStockQuantity).DefaultIfEmpty(0).FirstOrDefault();
                        var lastStockPrice = db.SubAccountMovements.Where(a => a.SubAccountId == item.SubAccounId && a.SubAccountMovementDate == (db.SubAccountMovements.Where(s => s.SubAccountId == item.SubAccounId).Select(d => d.SubAccountMovementDate).Max())).Select(a => a.ItemStockPrice).DefaultIfEmpty(0).FirstOrDefault();
                        subAccountMove.SubAccountId = item.SubAccounId;
                        subAccountMove.SubAccountMovementDate = billdate;
                        subAccountMove.QuantityIn = item.qty;
                        subAccountMove.ItemPricein = lastStockPrice;
                        subAccountMove.ItemTotalPriceIn = item.qty * lastStockPrice;
                        subAccountMove.ItemStockQuantity = lastStocktotalQty + item.qty;
                        subAccountMove.ItemTotalStockPrice = lastStocktotalPrice + (item.qty * lastStockPrice);
                        subAccountMove.ItemStockPrice = (lastStocktotalPrice + (item.qty * lastStockPrice)) / (lastStocktotalQty + item.qty);
                        subAccountMove.OrderNote = "رد مبيعات";
                        subaccountMoves.Add(subAccountMove);
                    }
                    var subaccOrder = new SubAccountOrder();
                    if (subaccOrder.SubAccountMovements == null)
                    {
                        subaccOrder.SubAccountMovements = new List<SubAccountMovement>();
                    }
                    subaccOrder.SubAccountMovements = subaccountMoves;
                    subaccOrder.OrderDate = billdate;
                    subaccOrder.CurrentWorkerId = uid;
                    subaccOrder.OrderNote = "رد مبيعات";

                    db.SubAccountOrders.Add(subaccOrder);

                    //حساب التكلفة
                    var totalInventoryCost = subaccountMoves.Select(a => a.ItemTotalPriceIn).DefaultIfEmpty(0).Sum();
                    var Costaccount = new AccountMovement()
                    {
                        AccountId = inventoryAccounts.salesCostAccount.AccountId,
                        AccountMovementDate = billdate,
                        Crdit = totalInventoryCost,
                        Debit = 0,
                        CurrentWorkerId = uid,
                        AccountMovementNote = "رد مبيعات"

                    };
                    var inventoryforCost = new AccountMovement()
                    {
                        AccountId = inventoryAccounts.inventoryAccount.AccountId,
                        AccountMovementDate = billdate,
                        Crdit = 0,
                        Debit = totalInventoryCost,
                        CurrentWorkerId = uid,
                        AccountMovementNote = "رد مبيعات"
                    };
                    if (AccountOrderByCost.AccountMovements == null)
                    {
                        AccountOrderByCost.AccountMovements = new List<AccountMovement>();
                    }

                    var comId = 0;
                    var orderNoCost = 1;
                    var billId = 1;
                    if (bill.IsCompany)
                    {
                        comId = db.Companies.Find(bill.ComapnyId).CompanyId;
                        orderNoCost = db.AccountOrders.Where(a => a.CompanyId == bill.ComapnyId).Count() != 0 ? db.AccountOrders.Where(a => a.CompanyId == bill.ComapnyId).Max(a => a.OrderNo) + 1 : 1;
                        billId = db.SalesBills.Where(a => a.CompanyId == bill.ComapnyId && a.IsCompany == true && a.OrdersTypeSalesType == bill.billName.typeName).Select(a => a.BillNo).DefaultIfEmpty(0).Max() + 1;
                        bills.CompanyId = comId;
                    }

                    else
                    {
                        comId = db.Branchs.Find(bill.ComapnyId).CompanyId;
                        orderNoCost = db.AccountOrders.Where(a => a.BranchId == bill.ComapnyId).Count() != 0 ? db.AccountOrders.Where(a => a.BranchId == bill.ComapnyId).Max(a => a.OrderNo) + 1 : 1;
                        billId = db.SalesBills.Where(a => a.BranchId == bill.ComapnyId && a.IsCompany == false && a.OrdersTypeSalesType == bill.billName.typeName).Select(a => a.BillNo).DefaultIfEmpty(0).Max() + 1;
                        bills.BranchId = comId;

                    }
                    bills.BillNo = billId;
                    bills.IsCompany = bill.IsCompany;
                    AccountOrderByCost.AccountMovements.Add(Costaccount);
                    AccountOrderByCost.AccountMovements.Add(inventoryforCost);
                    AccountOrderByCost.OrderNo = orderNoCost;
                    AccountOrderBySales.OrderNo = orderNoCost + 1;
                    AccountOrderByCost.OrderDate = billdate;
                    AccountOrderByCost.CurrentWorkerId = uid;
                    AccountOrderByCost.AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId;
                    AccountOrderBySales.AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId;
                    AccountOrderBySales.OrderDate = billdate;
                    AccountOrderByCost.OrderNote = string.Format("{0} - رقم {1} - {2}", bill.billName.billName, billId, "بسعر التكلفة");
                    AccountOrderBySales.OrderNote = string.Format("{0} - رقم {1} - {2}", bill.billName.billName, billId, bill.billTotalAccount.billPaymethods.payName);
                    if (bill.IsCompany)
                    {
                        AccountOrderByCost.IsCompany = true;
                        AccountOrderByCost.CompanyId = comId;
                        AccountOrderBySales.IsCompany = true;
                        AccountOrderBySales.CompanyId = comId;
                    }
                    else
                    {
                        AccountOrderByCost.IsCompany = false;
                        AccountOrderByCost.BranchId = comId;
                        AccountOrderBySales.IsCompany = false;
                        AccountOrderBySales.BranchId = comId;
                    }
                    AccountOrderBySales.OrderIdRefrence = bills.BillNo;
                    AccountOrderBySales.IsSales = true;
                    AccountOrderBySales.OrderTypeRefrence = "رد مبيعات";
                    AccountOrderByCost.OrderIdRefrence = bills.BillNo;
                    AccountOrderByCost.OrderTypeRefrence = "رد مبيعات";
                    AccountOrderByCost.IsSales = false;

                    db.AccountOrders.Add(AccountOrderByCost);
                    #endregion
                  
                    #region OtherBillAccounts
                    foreach (var item in bill.billOtherTotalAcounts)
                    {
                        if (bill.IsCompany)
                        {
                            var isItemExsiest = db.Accounts.Any(a => a.AccountId == item.accountId && a.BranchId == null && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == bill.ComapnyId);
                            if (!isItemExsiest)
                            {
                                return BadRequest(string.Format("لا يوجد حساب مسجل بهذا الاسم {0}", item.accountName));
                            }
                        }
                        else
                        {
                            var isItemExsiest = db.Accounts.Any(a => a.AccountId == item.accountId && a.BranchId == bill.ComapnyId);
                            if (!isItemExsiest)
                            {
                                return BadRequest(string.Format("لا يوجد حساب مسجل بهذا الاسم {0}", item.accountName));
                            }
                        }
                        var isDebit = group.sales.orderOtherTotalAccounts.Any(a => a.DebitWhenAdd && a.AccountId == item.accountId);
                        var accountMove = new AccountMovement()
                        {

                            AccountId = item.accountId,
                            Crdit = isDebit == true ? 0 : item.value,
                            Debit = isDebit == true ? item.value : 0,
                            AccountMovementDate = billdate,
                            AccountMovementNote = "رد مبيعات",
                            CurrentWorkerId = uid
                        };
                        AccountOrderBySales.AccountMovements.Add(accountMove);

                        bills.BillOtherAccountBills.Add(new BillOtherAccountBill
                        {
                            AccountId = item.accountId,
                            value = item.value,
                            IsDebit = item.type == "debit" ? true : false
                        });
                    }

                    var toatforRturnAccount = Math.Abs(AccountOrderBySales.AccountMovements.Select(a => a.Debit).DefaultIfEmpty(0).Sum() - AccountOrderBySales.AccountMovements.Select(a => a.Crdit).DefaultIfEmpty(0).Sum());

                    var totalAccount = new AccountMovement()
                    {
                        AccountId = bills.BillTotalAccountId,
                        Crdit = toatforRturnAccount,
                        Debit = 0,
                        AccountMovementDate = billdate,
                        CurrentWorkerId = uid,
                        AccountMovementNote = "رد مبيعات",
                    };
                    AccountOrderBySales.AccountMovements.Add(totalAccount);
                    db.AccountOrders.Add(AccountOrderBySales);
                    #endregion

                    foreach (var property in bill.billTotalAccount.billPaymethods.billPaymethodProperties)
                    {
                        if (bills.BillPaymentProperties == null)
                        {
                            bills.BillPaymentProperties = new List<BillPaymentProperties>();
                        }

                        bills.BillPaymentProperties.Add(new BillPaymentProperties
                        {
                            OrderPropertiesPaySalesId = property.Id,
                            value = property.value,
                        });

                    }
                    bills.SalesBillName = bill.billName.billName;
                    bills.type = bill.billName.typeName;
                    bills.CurrentWorkerId = uid;
                    db.SalesBills.Add(bills);

                    try
                    {

                        db.SaveChanges();
                        return Ok(bills.BillNo);
                    }
                    catch 
                    {

                    }
                    #endregion
                }
                else if (bill.billName.typeName == "sale")
                {
                   
                    #region Sales 
                    var bills = new SalesBill();
                    bills.BillDate = billdate;
                    bills.BillMainAccountId = bill.billMainAccount.AccountId;
                    bills.BillTotalAccountId = bill.billTotalAccount.accountId;
                    bills.BillTitleAccountId = bill.billTitleaccount.AccountId;
                    bills.OrdersTypeSalesId = bill.billName.typeId;
                    bills.OrdersTypeSalesType = bill.billName.typeName;
                    bills.BillPaymethodId = bill.billTotalAccount.billPaymethods.payId;

                    var group = OrderGroupManger.getOrderbyId(bill.OrderGroupId);
                    #region items
                    //var itemList = new List<BillSubAccountItemsVm>();
                    List<string> ids = new List<string>();
                    foreach (var row in bill.billItems)
                    {
                        var subAccountItem = new BillSubAccountItems();
                        var subAccounts = new BillAccountItems();
                        bool empity = false;
                        string name = this.AppCompanyUserManager.GetUniqueKey(4);
                        do
                        {
                            name = this.AppCompanyUserManager.GetUniqueKey(4);
                        } while (ids.Any(a => a == name));
                        ids.Add(name);
                        int num = 0;
                        foreach (var item in row)
                        {
                            num += 1;
                            if (bills.itemsRows == null)
                            {
                                bills.itemsRows = new List<ItemsRow>();
                            }
                            bills.itemsRows.Add(new ItemsRow()
                            {
                                itemPropertyValueId = item.itemPropertyValueId,
                                itemPropertyId = item.itemPropertyId,
                                itemPropertyValueName = item.itemPropertyValueName,
                                type = item.type,
                                value = name,
                                orderid = num
                            });
                            if (item.type == "item")
                            {
                                if (bill.IsCompany)
                                {
                                    var isItemExsiest = db.SubAccounts.Any(a => a.SubAccountId == item.itemPropertyValueId && a.Account.BranchId == null && a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == bill.ComapnyId);
                                    if (!isItemExsiest)
                                    {
                                        return BadRequest(string.Format("لا يوجد صنف مسجل بهذا الاسم {0}", item.itemPropertyValueName));
                                    }
                                }
                                else
                                {
                                    var isItemExsiest = db.SubAccounts.Any(a => a.SubAccountId == item.itemPropertyValueId && a.Account.BranchId == bill.ComapnyId);
                                    if (!isItemExsiest)
                                    {
                                        return BadRequest(string.Format("لا يوجد صنف مسجل بهذا الاسم {0}", item.itemPropertyValueName));
                                    }
                                }
                                subAccountItem.SubAccountId = item.itemPropertyValueId;
                                subAccountItem.uniqueId = name;
                            }
                            if (item.type == "price")
                            {
                                if (Convert.ToDecimal(item.itemPropertyValueName) <= 0)
                                {
                                    return BadRequest("يجب ان يكون السعر اكبر من صفر");
                                }
                                subAccountItem.price = Convert.ToDecimal(item.itemPropertyValueName);

                            }
                            if (item.type == "qty")
                            {
                                if (Convert.ToDecimal(item.itemPropertyValueName) <= 0)
                                {
                                    return BadRequest("يجب ان تكون الكمية اكبر من صفر");
                                }
                                subAccountItem.qty = Convert.ToDecimal(item.itemPropertyValueName); ;

                            }
                            if (item.type == "crdit" || item.type == "debit")
                            {
                                empity = true;
                                if (Convert.ToDecimal(item.itemPropertyValueName) < 0)
                                {
                                    return BadRequest("لا يمكن تسجيل قيمة سالبة للحساب");
                                }
                                if (bill.IsCompany)
                                {
                                    var isItemExsiest = db.Accounts.Any(a => a.AccountId == item.itemPropertyValueId && a.BranchId == null && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == bill.ComapnyId);
                                    if (!isItemExsiest)
                                    {
                                        return BadRequest("تأكد من ان جميع الحسابات مسجلة");
                                    }
                                }
                                else
                                {
                                    var isItemExsiest = db.Accounts.Any(a => a.AccountId == item.itemPropertyValueId && a.BranchId == bill.ComapnyId);
                                    if (!isItemExsiest)
                                    {
                                        return BadRequest("تأكد من ان جميع الحسابات مسجلة");
                                    }
                                }
                                subAccounts.AccountId = item.itemPropertyValueId;
                                subAccounts.value = Convert.ToDecimal(item.itemPropertyValueName);
                                subAccounts.uniqueId = name;
                            }

                        }

                        if (bills.BillSubAccountItems == null)
                        {
                            bills.BillSubAccountItems = new List<BillSubAccountItems>();
                        }
                        bills.BillSubAccountItems.Add(subAccountItem);
                        if (bills.BillAccountItems == null)
                        {
                            bills.BillAccountItems = new List<BillAccountItems>();
                        }

                        if (empity)
                        {
                            bills.BillAccountItems.Add(subAccounts);
                        }


                    }

                    var itemSum = bills.BillSubAccountItems.GroupBy(a => a.SubAccountId).Select(g => new
                    {
                        SubAccounId = g.Key,
                        Price = g.Sum(a => a.price),
                        qty = g.Sum(a => a.qty),
                        total = g.Sum(a => a.price) * g.Sum(a => a.qty)
                    }).ToList();
                    #endregion
                    var AccountOrderBySales = new AccountOrder();
                    var AccountOrderByCost = new AccountOrder();
                    var inventoryAccounts = group.sales.orderMainAccounts.SingleOrDefault(a => a.inventoryAccount.AccountId == bill.billMainAccount.AccountId);
                    if (inventoryAccounts == null)
                    {
                        return BadRequest("تأكد من اكتمال حسابات المخازن في الاعدادات");
                    }

                    #region القيد بسعر البيع
                    var totalPrice = itemSum.Select(a => a.total).DefaultIfEmpty(0).Sum();
                    var accountReturn = new AccountMovement()
                    {
                        AccountId = inventoryAccounts.salesAccount.AccountId,
                        Debit = 0,
                        Crdit = totalPrice,
                        AccountMovementDate = billdate,
                        AccountMovementNote = "مبيعات",
                        CurrentWorkerId = uid
                    };
                    if (AccountOrderBySales.AccountMovements == null)
                    {
                        AccountOrderBySales.AccountMovements = new List<AccountMovement>();
                    }
                    AccountOrderBySales.AccountMovements.Add(accountReturn);
                    #endregion
                    #region القيد بسعر التكلفة
                    var subaccountMoves = new List<SubAccountMovement>();
                    foreach (var item in itemSum)
                    {

                        var subAccountMove = new SubAccountMovement();
                        var lastStocktotalPrice = db.SubAccountMovements.Where(a => a.SubAccountId == item.SubAccounId && a.SubAccountMovementDate == (db.SubAccountMovements.Where(s => s.SubAccountId == item.SubAccounId).Select(d => d.SubAccountMovementDate).Max())).Select(a => a.ItemTotalStockPrice).DefaultIfEmpty(0).FirstOrDefault();
                        var lastStocktotalQty = db.SubAccountMovements.Where(a => a.SubAccountId == item.SubAccounId && a.SubAccountMovementDate == (db.SubAccountMovements.Where(s => s.SubAccountId == item.SubAccounId).Select(d => d.SubAccountMovementDate).Max())).Select(a => a.ItemStockQuantity).DefaultIfEmpty(0).FirstOrDefault();
                        var lastStockPrice = db.SubAccountMovements.Where(a => a.SubAccountId == item.SubAccounId && a.SubAccountMovementDate == (db.SubAccountMovements.Where(s => s.SubAccountId == item.SubAccounId).Select(d => d.SubAccountMovementDate).Max())).Select(a => a.ItemStockPrice).DefaultIfEmpty(0).FirstOrDefault();
                        if(lastStocktotalQty - item.qty < 0)
                        {
                            return BadRequest(string.Format("كمية غير متاحة للصرف من الصنف {0}", db.SubAccounts.Find(item.SubAccounId).SubAccountName));
                        }
                        subAccountMove.SubAccountId = item.SubAccounId;
                        subAccountMove.SubAccountMovementDate = billdate;
                        subAccountMove.QuantityOut = item.qty;
                        subAccountMove.ItemPriceOut = lastStockPrice;
                        subAccountMove.ItemTotalPriceOut = item.qty * lastStockPrice;
                        subAccountMove.ItemStockQuantity = lastStocktotalQty - item.qty;
                        subAccountMove.ItemTotalStockPrice = lastStocktotalPrice - (item.qty * lastStockPrice);
                        subAccountMove.ItemStockPrice = (lastStocktotalPrice - (item.qty * lastStockPrice)) / (lastStocktotalQty - item.qty);
                        subAccountMove.OrderNote = "مبيعات";
                        subaccountMoves.Add(subAccountMove);
                    }
                    var subaccOrder = new SubAccountOrder();
                    if (subaccOrder.SubAccountMovements == null)
                    {
                        subaccOrder.SubAccountMovements = new List<SubAccountMovement>();
                    }
                    subaccOrder.SubAccountMovements = subaccountMoves;
                    subaccOrder.OrderDate = billdate;
                    subaccOrder.CurrentWorkerId = uid;
                    subaccOrder.OrderNote = "مبيعات";
                 
                    db.SubAccountOrders.Add(subaccOrder);

                    //حساب التكلفة
                    var totalInventoryCost = subaccountMoves.Select(a => a.ItemTotalPriceOut).DefaultIfEmpty(0).Sum();
                    var Costaccount = new AccountMovement()
                    {
                        AccountId = inventoryAccounts.salesCostAccount.AccountId,
                        AccountMovementDate = billdate,
                        Crdit =0,
                        Debit = totalInventoryCost,
                        CurrentWorkerId = uid,

                    };
                    var inventoryforCost = new AccountMovement()
                    {
                        AccountId = inventoryAccounts.inventoryAccount.AccountId,
                        AccountMovementDate = billdate,
                        Crdit = totalInventoryCost,
                        Debit = 0,
                        CurrentWorkerId = uid,
                    };
                    if (AccountOrderByCost.AccountMovements == null)
                    {
                        AccountOrderByCost.AccountMovements = new List<AccountMovement>();
                    }

                    var comId = 0;
                    var orderNoCost = 1;
                    var billId = 1;
                    if (bill.IsCompany)
                    {
                        comId = db.Companies.Find(bill.ComapnyId).CompanyId;
                        orderNoCost = db.AccountOrders.Where(a => a.CompanyId == bill.ComapnyId).Count() != 0 ? db.AccountOrders.Where(a => a.CompanyId == bill.ComapnyId).Max(a => a.OrderNo) + 1 : 1;
                        billId = db.SalesBills.Where(a => a.CompanyId == bill.ComapnyId && a.IsCompany == true && a.OrdersTypeSalesType == bill.billName.typeName).Select(a => a.BillNo).DefaultIfEmpty(0).Max() + 1;
                        bills.CompanyId = comId;
                    }

                    else
                    {
                        comId = db.Branchs.Find(bill.ComapnyId).CompanyId;
                        orderNoCost = db.AccountOrders.Where(a => a.BranchId == bill.ComapnyId).Count() != 0 ? db.AccountOrders.Where(a => a.BranchId == bill.ComapnyId).Max(a => a.OrderNo) + 1 : 1;
                        billId = db.SalesBills.Where(a => a.BranchId == bill.ComapnyId && a.IsCompany == false && a.OrdersTypeSalesType == bill.billName.typeName).Select(a => a.BillNo).DefaultIfEmpty(0).Max() + 1;
                        bills.BranchId = comId;

                    }
                    bills.BillNo = billId;
                    bills.IsCompany = bill.IsCompany;
                    AccountOrderByCost.AccountMovements.Add(Costaccount);
                    AccountOrderByCost.AccountMovements.Add(inventoryforCost);
                    AccountOrderByCost.OrderNo = orderNoCost;
                    AccountOrderBySales.OrderNo = orderNoCost + 1;
                    AccountOrderByCost.OrderDate = billdate;
                    AccountOrderByCost.CurrentWorkerId = uid;
                    AccountOrderByCost.AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId;
                    AccountOrderBySales.AccountOrderPropertiesId = db.AccountOrderProperties.FirstOrDefault().AccountOrderPropertiesId;
                    AccountOrderBySales.OrderDate = billdate;
                    AccountOrderByCost.OrderNote = string.Format("{0} - رقم {1} - {2}", bill.billName.billName, billId ,  "بسعر التكلفة");
                    AccountOrderBySales.OrderNote = string.Format("{0} - رقم {1} - {2}", bill.billName.billName, billId, bill.billTotalAccount.billPaymethods.payName);
                    if (bill.IsCompany)
                    {
                        AccountOrderByCost.IsCompany = true;
                        AccountOrderByCost.CompanyId = comId;
                        AccountOrderBySales.IsCompany = true;
                        AccountOrderBySales.CompanyId = comId;
                    }
                    else
                    {
                        AccountOrderByCost.IsCompany = false;
                        AccountOrderByCost.BranchId = comId;
                        AccountOrderBySales.IsCompany = false;
                        AccountOrderBySales.BranchId = comId;
                    }
                    AccountOrderBySales.OrderIdRefrence = bills.BillNo;
                    AccountOrderBySales.OrderTypeRefrence = "مبيعات";
                    AccountOrderBySales.IsSales = true;
                    AccountOrderByCost.OrderIdRefrence = bills.BillNo;
                    AccountOrderByCost.OrderTypeRefrence = "مبيعات";
                    AccountOrderByCost.IsSales = false;
                    db.AccountOrders.Add(AccountOrderByCost);
                    #endregion
                    #region ItemAccounts
                

                    var GroupItemAccount = bills.BillAccountItems.GroupBy(a => a.AccountId).Select(g => new
                    {
                        AccounId = g.Key,
                        DebitWhenAdd = group.sales.orderAccountsforSubAccounts.SingleOrDefault(a => a.AccountId == g.Key).DebitWhenAdd,
                        DebitForPlus = group.sales.orderAccountsforSubAccounts.SingleOrDefault(a => a.AccountId == g.Key).DebitforPlus,
                        IsNewOrder = group.sales.orderAccountsforSubAccounts.SingleOrDefault(a => a.AccountId == g.Key).IsNewOrder,

                        value = g.Sum(a => a.value)
                    }).ToList();

                    foreach (var acc in GroupItemAccount)
                    {
                        var accountMove = new AccountMovement()
                        {

                            AccountId = acc.AccounId,
                            Crdit = acc.DebitWhenAdd == true ? acc.value : 0,
                            Debit = acc.DebitWhenAdd == true ? 0 : acc.value,
                            AccountMovementDate = billdate,
                            AccountMovementNote = "مبيعات",
                            CurrentWorkerId = uid
                        };
                        AccountOrderBySales.AccountMovements.Add(accountMove);
                    }
                    #endregion
                    #region OtherBillAccounts
                    foreach (var item in bill.billOtherTotalAcounts)
                    {
                        if (bill.IsCompany)
                        {
                            var isItemExsiest = db.Accounts.Any(a => a.AccountId == item.accountId && a.BranchId == null && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == bill.ComapnyId);
                            if (!isItemExsiest)
                            {
                                return BadRequest(string.Format("لا يوجد حساب مسجل بهذا الاسم {0}", item.accountName));
                            }
                        }
                        else
                        {
                            var isItemExsiest = db.Accounts.Any(a => a.AccountId == item.accountId && a.BranchId == bill.ComapnyId);
                            if (!isItemExsiest)
                            {
                                return BadRequest(string.Format("لا يوجد حساب مسجل بهذا الاسم {0}", item.accountName));
                            }
                        }
                        var isDebit = group.sales.orderOtherTotalAccounts.Any(a => a.DebitWhenAdd && a.AccountId == item.accountId);
                        var accountMove = new AccountMovement()
                        {

                            AccountId = item.accountId,
                            Crdit = isDebit == true ? item.value : 0,
                            Debit = isDebit == true ? 0 : item.value,
                            AccountMovementDate = billdate,
                            AccountMovementNote = "مبيعات",
                            CurrentWorkerId = uid
                        };
                        AccountOrderBySales.AccountMovements.Add(accountMove);

                        bills.BillOtherAccountBills.Add(new BillOtherAccountBill
                        {
                            AccountId = item.accountId,
                            value = item.value,
                            IsDebit = item.type == "debit" ? true : false
                        });
                    }

                    var toatforRturnAccount = Math.Abs(AccountOrderBySales.AccountMovements.Select(a => a.Debit).DefaultIfEmpty(0).Sum() - AccountOrderBySales.AccountMovements.Select(a => a.Crdit).DefaultIfEmpty(0).Sum());

                    var totalAccount = new AccountMovement()
                    {
                        AccountId = bills.BillTotalAccountId,
                        Crdit = 0,
                        Debit = toatforRturnAccount,
                        AccountMovementDate = billdate,
                        CurrentWorkerId = uid,
                        AccountMovementNote = "مبيعات",
                    };
                    AccountOrderBySales.AccountMovements.Add(totalAccount);
                    db.AccountOrders.Add(AccountOrderBySales);
                    #endregion

                    foreach (var property in bill.billTotalAccount.billPaymethods.billPaymethodProperties)
                    {
                        if(bills.BillPaymentProperties == null)
                        {
                            bills.BillPaymentProperties = new List<BillPaymentProperties>();
                        }

                        bills.BillPaymentProperties.Add(new BillPaymentProperties
                        {
                            OrderPropertiesPaySalesId = property.Id,
                            value = property.value,
                        });

                    }
                    bills.SalesBillName = bill.billName.billName;
                    bills.type = bill.billName.typeName;
                    bills.CurrentWorkerId = uid;
                    db.SalesBills.Add(bills);
                   
                    try
                    {
                        db.SaveChanges();
                        return Ok(bills.BillNo);
                    }
                    catch 
                    {
                        
                    }
                    #endregion

                }
                else
                {
                    return BadRequest("لا يوجد اذن مسجل بهذا النوع");

                }
                #endregion


            }
            else
            {
                return BadRequest();
            }
            return Ok();
        }


        [Route("getOrderAccountsForOther")]
        [HttpPost]
        public IHttpActionResult GetOrderAccountsForOther(isCompanyBranch companyBranch)
        {
            if (companyBranch.IsCompany)
            {
                var company = db.Companies.Find(companyBranch.Id);
                if (company != null)
                {

                    //OrderOtherTotalAccountVm
                    var accounts = (from a in db.Accounts
                                    where a.BranchId == null
                                    && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.CompanyId
                                    && a.AccountCategory.AccountsDataTypes.TypeName == "اخري"
                                    select new OrderOtherTotalAccountVm()
                                    {
                                        AccountId = a.AccountId,
                                        AccountName = a.AccountName,
                                        DebitWhenAdd = false

                                    }).ToList();

                    return Ok(accounts);
                  
                }
                else
                {
                    return BadRequest("لا توجد شركة مسجلة بهذا الاسم");
                }
            }
            else
            {
                //Branch
                var branch = db.Branchs.Find(companyBranch.Id);
                if (branch != null)
                {
                    //OrderOtherTotalAccountVm
                    var accounts = (from a in db.Accounts
                                    where a.BranchId == branch.BranchId
                                    && a.AccountCategory.AccountsDataTypes.TypeName == "اخري"
                                    select new OrderOtherTotalAccountVm()
                                    {
                                        AccountId = a.AccountId,
                                        AccountName = a.AccountName,
                                        DebitWhenAdd = false

                                    }).ToList();

                    return Ok(accounts);
                }
                else
                {
                    return BadRequest("لا يوجد فرع مسجل بهذاالاسم");
                }
            }
        }

        [Route("GetOrderAccountsForTotal")]
        [HttpPost]
        public IHttpActionResult GetOrderAccountsForTotal(isCompanyBranch companyBranch)
        {
            if (companyBranch.IsCompany)
            {
                var company = db.Companies.Find(companyBranch.Id);
                if (company != null)
                {

                    //var accounts = (from a in db.AccountCategories
                    //                where a.BalanceSheetType.BalanceSheet.CompanyId == company.CompanyId
                    //                select new AccountsforOrderGroupTotalVm()
                    //                {
                    //                    AccountCategoryId = a.AccountCategoryId,
                    //                    AccountCategoryName = a.AccountCategoryName,
                    //                    AccountType = a.AccountsDataTypes.TypeName,
                    //                    selectedaccount = db.OrderTotalAccounts.Any(b => b.AccountCategoryId == a.AccountCategoryId && b.OrderGroupId == companyBranch.gId),
                    //                    OrderGroupId = companyBranch.gId

                    //                }).ToList().OrderBy(c => c.AccountType);



                    return Ok();
                }
                else
                {
                    return BadRequest("لا توجد شركة مسجلة بهذا الاسم");
                }
            }
            else
            {
                //Branch
                var branch = db.Branchs.Find(companyBranch.Id);
                if (branch != null)
                {
                    //var accounts = (from a in db.AccountCategories
                    //                where a.BalanceSheetType.BalanceSheet.CompanyId == branch.CompanyId
                    //                select new AccountsforOrderGroupTotalVm()
                    //                {
                    //                    AccountCategoryId = a.AccountCategoryId,
                    //                    AccountCategoryName = a.AccountCategoryName,
                    //                    AccountType = a.AccountsDataTypes.TypeName,
                    //                    selectedaccount = db.OrderTotalAccounts.Any(b => b.AccountCategoryId == a.AccountCategoryId && b.OrderGroupId == companyBranch.gId),
                    //                    OrderGroupId = companyBranch.gId

                    //                }).ToList().OrderBy(c => c.AccountType);



                    return Ok();
                }
                else
                {
                    return BadRequest("لا يوجد فرع مسجل بهذاالاسم");
                }
            }
        }

        [Route("saveOrdersubAccount")]
        [HttpPost]
        public IHttpActionResult SaveOrdersubAccount(List<AccountsforOrderGroupVm> orderprop)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            foreach (var account in orderprop)
            {
                var accountEx = db.Accounts.Find(account.AccountId);
                //if (accountEx != null)
                //{
                //    var accMain = db.OrderAccountsforSubAccounts.SingleOrDefault(a => a.AccountId == account.AccountId && a.OrderGroupId == account.OrderGroupId);
                //    if (account.selectedaccount == true && accMain == null)
                //    {
                //        var newMainAccount = new OrderAccountsforSubAccount()
                //        {
                //            OrderGroupId = (Int32)account.OrderGroupId,
                //            AccountId = account.AccountId,
                //            DebitWhenAdd = (bool)account.isDebit,
                //            CurrentWorkerId = uid
                //        };
                //        db.OrderAccountsforSubAccounts.Add(newMainAccount);
                //        db.SaveChanges();
                //    }
                //    else if (account.selectedaccount == false && accMain != null)
                //    {
                //        db.OrderAccountsforSubAccounts.Remove(accMain);
                //        db.SaveChanges();
                //    }
                //    else if (account.selectedaccount == true && accMain != null)
                //    {
                //        accMain.DebitWhenAdd = (bool)account.isDebit;
                //        db.Entry(accMain).State = EntityState.Modified;
                //        db.SaveChanges();
                //    }

                //}
            }
           // var geto = this.AppCompanyUserManager.getOrderbyId((Int32)orderprop.FirstOrDefault().OrderGroupId).OrderAccountsforSubAccounts;

            return Ok();
        }

        [Route("saveOrderOtherAccount")]
        [HttpPost]
        public IHttpActionResult SaveOrderOtherAccount(List<AccountsforOrderGroupVm> orderprop)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            foreach (var account in orderprop)
            {
                var accountEx = db.Accounts.Find(account.AccountId);
                //if (accountEx != null)
                //{
                //    var accMain = db.OrderOtherTotalAccounts.SingleOrDefault(a => a.AccountId == account.AccountId && a.OrderGroupId == account.OrderGroupId);
                //    if (account.selectedaccount == true && accMain == null)
                //    {
                //        var newMainAccount = new OrderOtherTotalAccount()
                //        {
                //            OrderGroupId = (Int32)account.OrderGroupId,
                //            AccountId = account.AccountId,
                //            DebitWhenAdd = (bool)account.isDebit,
                //            CurrentWorkerId = uid
                //        };
                //        db.OrderOtherTotalAccounts.Add(newMainAccount);
                //        db.SaveChanges();
                //    }
                //    else if (account.selectedaccount == false && accMain != null)
                //    {
                //        db.OrderOtherTotalAccounts.Remove(accMain);
                //        db.SaveChanges();
                //    }
                //    else if (account.selectedaccount == true && accMain != null)
                //    {
                //        accMain.DebitWhenAdd = (bool)account.isDebit;
                //        db.Entry(accMain).State = EntityState.Modified;
                //        db.SaveChanges();
                //    }

                //}
            }
           // var geto = this.AppCompanyUserManager.getOrderbyId((Int32)orderprop.FirstOrDefault().OrderGroupId).OrderOtherTotalAccounts;

            return Ok();
        }

        [Route("saveOrderTotalAccount")]
        [HttpPost]
        public IHttpActionResult SaveOrderTotalAccount(List<AccountsforOrderGroupTotalVm> orderprop)
        {
            string uid = RequestContext.Principal.Identity.GetUserId();
            foreach (var account in orderprop)
            {
                var accountEx = db.AccountCategories.Find(account.AccountCategoryId);
                //if (accountEx != null)
                //{
                //    var accMain = db.OrderTotalAccounts.SingleOrDefault(a => a.AccountCategoryId == account.AccountCategoryId && a.OrderGroupId == account.OrderGroupId);
                //    if (account.selectedaccount == true && accMain == null)
                //    {
                //        var newMainAccount = new OrderTotalAccount()
                //        {
                //            OrderGroupId = (Int32)account.OrderGroupId,
                //            AccountCategoryId = account.AccountCategoryId,
                //            CurrentWorkerId = uid
                //        };
                //        db.OrderTotalAccounts.Add(newMainAccount);
                //        db.SaveChanges();
                //    }
                //    else if (account.selectedaccount == false && accMain != null)
                //    {
                //        db.OrderTotalAccounts.Remove(accMain);
                //        db.SaveChanges();
                //    }

                //}
            }
            //var geto = this.AppCompanyUserManager.getOrderbyId((Int32)orderprop.FirstOrDefault().OrderGroupId).OrderTotalAccounts;

            return Ok();
        }

        //Begin Ordering & billing systems
        [Route("GetBilling/{id:int}")]
        [HttpGet]
        public IHttpActionResult GetBilling(int id)
        {

            return Ok();
        }

        [Route("GetTotalaccounts/{id:int}")]
        [HttpGet]
        public IHttpActionResult GetTotalAccounts(int id)
        {
            var accounts = (from a in db.OrderGroup
                           where a.OrderGroupId == id
                           select a).SingleOrDefault();
            var returnedaccount =new  List<billAccountVm>();
            //foreach (var ac in accounts.OrderTotalAccounts)
            //{
            //    var acc = (from b in ac.AccountCategory.Accounts
            //              select new billAccountVm()
            //              {
            //                  accountId = b.AccountId , 
            //                  accountName = b.AccountName,
                             
            //              }).ToList();
            //    returnedaccount.AddRange(acc);
            //}
            var aliesName = "الحساب";
            //if(accounts.OrderTotalAccounts.Count != 0)
            //{
            //    if (accounts.OrderTotalAccounts.FirstOrDefault().AccountCategory.AccountsDataTypes.TypeName == "عملاء")
            //    {
            //        aliesName = "العميل";

            //    }
            //    else if (accounts.OrderTotalAccounts.FirstOrDefault().AccountCategory.AccountsDataTypes.TypeName == "مخازن")
            //    {
            //        aliesName = "المخزن";

            //    }
            //    else
            //    {
            //        aliesName = "الحساب";
            //    }
            //}
          
            var totalAccounts = new getTotalAccounts()
            {
                accountAliesName = aliesName,
                accounts = returnedaccount
            };
            return Ok(totalAccounts);
        }


    }
}
