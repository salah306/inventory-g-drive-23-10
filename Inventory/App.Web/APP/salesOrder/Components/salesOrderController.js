﻿
(function () {



    angular.module('inventoryModule').controller("salesOrder", function ($scope, $timeout, _, $localStorage, AccountspersistenceService, $rootScope, settings, $filter,
        AccountOrderspersistenceService, OrdersConfigurationpersistenceService, Offline, EasyStoreUserspersistenceService, $location, branchesremotePersistenceStrategy, $sce, globalService, $q, $log, DTOptionsBuilder, DTColumnDefBuilder) {
        var bId;
        $scope.itemsforBill = [];
        $scope.billPaymethods = [];
        $scope.titleAccounts = [];
        $scope.branchesGroub = [];
        $scope.billNames = [];
        $scope.orderGroupNames = [];
        $scope.subjectAccounts = [];
        $scope.orderTemp = null;
        $scope.selectedOrderGroupName = { orderGroupId: 0, orderGroupName: null };

        $scope.waiting = 0;
        $scope.workinprogress = 0;
        jQuery('input[name="notfydate"]').daterangepicker({
            "singleDatePicker": true,
            "startDate": moment().format('YYYY-MM-DD'),
            "minDate": moment().format('YYYY-MM-DD'),
            "locale": {
                "format": "YYYY-MM-DD"
            }
        });

        var vm = this;
        vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(3);
        var start = moment().startOf('year');
        var end = moment().endOf('year');
        $scope.nowDate = moment().format('YYYY-MM-DD');
        $scope.startDate = moment().startOf('year').toDate();
        $scope.endDate = moment().endOf('year').toDate();
        function cb(start, end) {
            $('#reportrange span').html('عن الفترة من ' + start.format('YYYY-MM-DD') + ' الي ' + end.format('YYYY-MM-DD'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'اليوم': [moment(), moment()],
                'امس': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'اخر 7 ايام': [moment().subtract(6, 'days'), moment()],
                'اخر 30 يوم': [moment().subtract(29, 'days'), moment()],
                'هذا الشهر': [moment().startOf('month'), moment().endOf('month')],
                'الشهر الماضي': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'السنة الحالية': [moment().startOf('year'), moment().endOf('year')]
            }
        }, cb);

        cb(start, end);



        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            $scope.startDate = picker.startDate.toDate();
            $scope.endDate = picker.endDate.toDate();
        });

        $scope.branchGroub = $rootScope.mainbranchGroub;
        $scope.oType = "sales";
        $scope.orderType = "sales";
        $scope.labelAccount = "العميل"
        $scope.defualtOrderTile = "امر بيع"
        $scope.catName = "مبيعات";
        $scope.defualtWatingName = "طلبات البيع";
        $scope.defualtWatingDoneName = "جاري العمل";
        $scope.defualtOrderRequest = "رقم طلب البيع"
        $scope.showMaxQ = true;
        $scope.$watch('$root.mainbranchGroub.id', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal !== '0') {
                var findCompanybr = angular.copy($filter('filter')($rootScope.mainbranchesGroub, { id: newVal }, true));
                if (findCompanybr) {
                    $scope.branchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    $rootScope.mainbranchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    if (findCompanybr.length) {

                        $scope.order = {
                            "OrderId": 0,
                            "orderNo": 0,
                            "orderName": $scope.defualtOrderTile,
                            "orderDate": moment().format('YYYY-MM-DD'),
                            "orderDateSecond": "",
                            "orderDateThird": "",
                            "orderType": $scope.orderType,
                            "oType": $scope.oType,
                            "subjectAccount": { "subjectAccountId": 0, "subjectAccountName": null },
                            "recivedDate": moment().format('YYYY-MM-DD'),
                            "userName": "",
                            "userNameSecond": "",
                            "userNameThird": "",
                            "isDoneFirst": false,
                            "isDoneSecond": false,
                            "isDoneThird": false,
                            "cancel": false,
                            "isNewOrder": true,
                            "transferTo": {
                                "inventoryId": 0,
                                "inventoryName": "",
                                "code": ""
                            },
                            "itemsRequest": [
                                {
                                    "itemId": 0,
                                    "itemName": "",
                                    "itemCode": "",
                                    "qty": 0,
                                    "price": 0,
                                    "note": "",
                                    "realted": "",
                                    "accounts": []
                                }
                            ],
                            "pay": [],
                            "totalAccount": [],
                            "terms": [],
                            "companyId": 0,
                            "isCompany": true,
                            "cancelDate": "",
                            "userNameCancel": ""
                        };
                        bId = parseInt(angular.copy(parseInt(findCompanybr[0].id)));

                        EasyStoreUserspersistenceService.action.branchGroupSt(JSON.stringify($scope.branchGroub)).then(function () { }, function () { });
                        $scope.getInventory();
                    }
                }

            }
        });



        $scope.ordertemp = angular.copy($scope.order);
        $scope.items = [];
        $scope.$watch('order.transferTo.inventoryId', function (newVal, oldVal) {
            if (newVal !== oldVal) {

                var findinventory = angular.copy($filter('filter')($scope.inventory, { inventoryId: newVal }, true));
                if (!findinventory) {
                    $scope.order.transferTo = { inventoryId: 0, inventoryName: null };
                }
                $scope.order.transferTo = findinventory.length ? findinventory[0] : { inventoryId: 0, inventoryName: null };

                if ($scope.inventory && findinventory.length) {
                    $localStorage.invrntorytransfertosalesRequest = $scope.order.transferTo;
                    AccountOrderspersistenceService.action.orderReqestPurItems(JSON.stringify({ inventoryId: $scope.order.transferTo.inventoryId })).then(
                        function (result) {
                            $scope.items = result.data;
                        },
                        function (error) {
                            if (error.data) {
                                if (error.data.message) {
                                    toastr.error(error.data.message);
                                }

                            } else {
                                toastr.error("لا يمكن اتمام العملية الان");
                            }

                        });
                }
            }
        });

        $scope.tableAccounts = [];
        $scope.payMethods = [];

        $scope.totalAccount = [];
        $scope.terms = [];
        //{ termId: 0, termName: "التسليم محل المشتري" },
        //{ termId: 0, termName: "الاستبدال والاسترجاع خلال 14 يوم من تاريخ الشراء" },
        //{ termId: 0, termName: "جميع الاصناف الموضحة اعلاه خالصه الضرائب" }
        $scope.$watch('selectedOrderGroupName.orderGroupId', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                var findinventory = angular.copy($filter('filter')($scope.orderGroupNames, { orderGroupId: newVal }, true));

                if (findinventory.length) {
                    $scope.selectedOrderGroupName = findinventory[0];
                    $localStorage.salesselectedOrderGroupName = $scope.selectedOrderGroupName;
                    OrdersConfigurationpersistenceService.action.GetOrderTemplatebyId(JSON.stringify({ orderId: $scope.selectedOrderGroupName.orderGroupId, type: $scope.oType, orderType: $scope.orderType })).then(
                        function (result) {
                            $scope.order = result.data.ordertemp;
                            $scope.orderTemp = angular.copy(result.data.ordertemp);
                            $scope.tableAccounts = result.data.tableAccounts;
                            $scope.payMethods = result.data.payMethods;
                            $scope.totalAccount = result.data.totalAccount;
                            $scope.terms = result.data.terms

                        },
                        function (error) {
                            if (error.data) {
                                if (error.data.message) {
                                    toastr.error(error.data.message);
                                }

                            } else {
                                toastr.error("لا يمكن اتمام العملية الان");
                            }

                        });
                }
            }
        });
        $scope.$watch('order.subjectAccount.subjectAccountId', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                var findinventory = angular.copy($filter('filter')($scope.subjectAccounts, { subjectAccountId: newVal }, true));

                if (findinventory.length) {
                    $scope.order.subjectAccount.subjectAccountName = findinventory[0].subjectAccountName

                }
            }
        });


        $scope.getInventory = function () {

            OrdersConfigurationpersistenceService.action.GetOrderGroupNames(JSON.stringify({ type: $scope.oType, orderType: $scope.orderType })).then(
                function (result) {
                    $scope.inventory = result.data.returnd.inventory;
                    $scope.waiting = result.data.returnd.waitingcount;
                    $scope.workinprogress = result.data.returnd.workinprogress;
                    $scope.subjectAccounts = result.data.subjectAccount;
                    $scope.orderGroupNames = result.data.ordersNames
                    if ($localStorage.salesselectedOrderGroupName) {
                        var findorderName = angular.copy($filter('filter')($scope.orderGroupNames, { orderGroupId: $localStorage.salesselectedOrderGroupName.orderGroupId }, true));
                        if (findorderName.length) {
                            $scope.selectedOrderGroupName = $localStorage.salesselectedOrderGroupName;
                        }

                    }

                },
                function (error) {
                    if (error.data) {
                        if (error.data.message) {
                            toastr.error(error.data.message);
                        }

                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });
        };

        $scope.getremainPay = function () {

            if (!$scope.order || !$scope.order.itemsRequest || !$scope.order.itemsRequest.length) {
                return;
            }
            var tAmount = 0.0;
            if ($scope.order.pay && $scope.order.pay.length && $scope.order.pay.length > 0) {
                for (var i = 0; i < $scope.order.pay.length; i++) {
                    if (!isNaN(parseFloat($scope.order.pay[i].amount)) && isFinite($scope.order.pay[i].amount)) {
                        if (parseFloat($scope.order.pay[i].amount) < 0) {
                            toastr.warning("غير مسموح بقيم سالبة");
                            acc.amount = 0.0;
                        }
                        tAmount += parseFloat($scope.order.pay[i].amount);
                    } else {
                        $scope.order.pay[i].amount = 0;
                    }
                }
            }

            return parseFloat($scope.getGrandTotal()) - parseFloat(tAmount);
        };
        $scope.checkTotalPay = function (acc) {
            toastr.clear();
            if (!$scope.order || !$scope.order.itemsRequest || !$scope.order.itemsRequest.length || !$scope.order.pay || !$scope.order.pay.length) {
                return;
            }
            var tAmount = 0;
            for (var i = 0; i < $scope.order.pay.length; i++) {
                if (!isNaN(parseFloat($scope.order.pay[i].amount)) && isFinite($scope.order.pay[i].amount)) {
                    if (parseFloat($scope.order.pay[i].amount) < 0) {
                        toastr.warning("غير مسموح بقيم سالبة");
                        acc.amount = 0.0;
                    }
                    tAmount += parseFloat($scope.order.pay[i].amount);
                } else {
                    $scope.order.pay[i].amount = 0;
                }
            }
            if (parseFloat(tAmount) > parseFloat($scope.getGrandTotal())) {

                toastr.warning("غير مسموح بتجاوز القيمة الاجمالية للامر");
                console.log("deffreince", parseFloat(tAmount) - parseFloat($scope.getGrandTotal()));
                console.log("acc", acc);
                acc.amount = 0;

            }

        };
        $scope.chengeTAccAmountType = function (item, acc) {
            if ($scope.order.itemsRequest[item].accounts[acc].amountType === "%") {
                $scope.order.itemsRequest[item].accounts[acc].amountType = "جـ"
            } else {
                $scope.order.itemsRequest[item].accounts[acc].amountType = "%"
            }
        };
        $scope.chengeTotalAccAmountType = function (acc) {
            if ($scope.order.totalAccount[acc].amountType === "%") {
                $scope.order.totalAccount[acc].amountType = "جـ"
            } else {
                $scope.order.totalAccount[acc].amountType = "%"
            }
        };
        $scope.getItembyCode = function (data, index) {
            toastr.clear();
           
            for (var i = 0; i < $scope.order.itemsRequest.length; i++) {
                if ($scope.order.itemsRequest.length > 1 && $scope.order.itemsRequest[i].itemCode === data.itemCode) {
                    $scope.order.itemsRequest[index].itemName = null;
                    var id1 = '#qty' + (i + 1);
                    setTimeout(function () { $(id1).focus() }, 200);
                    return;
                }
            };
            $scope.order.itemsRequest[index].itemCode = data.itemCode;
            $scope.order.itemsRequest[index].itemName = data.itemName;
            $scope.order.itemsRequest[index].itemId = data.itemId;
            $scope.order.itemsRequest[index].qty = 1;
            $scope.order.itemsRequest[index].note = null;
            $scope.order.itemsRequest[index].itemUnitType = data.itemUnitType;
            $scope.order.itemsRequest[index].itemUnitName = data.itemUnitName;
            $scope.order.itemsRequest[index].realted = data.realted;
            $scope.order.itemsRequest[index].price = data.price;
            if (data.maxQ === 0) {
               
                $scope.order.itemsRequest[index].qty= 0;
               
            }
            var id = '#qty' + $scope.order.itemsRequest.length;
            setTimeout(function () { $(id).focus() }, 200);
        }
        $scope.checkqty = function (data) {
            console.log("selected qty", data);
            toastr.clear();
            if (!isNaN(parseFloat(data.qty)) && isFinite(data.qty)) {
                if (+data.qty > +data.selected.maxQ) {
                    toastr.warning("الكمية غير متوفرة");
                    data.qty = parseFloat(data.selected.maxQ);
                    return;
                }
                
                if (data.qty <= 0 && data.selected.maxQ > 0) {
                    data.qty = 1;
                    toastr.warning("الكمية يجب ان تكون اكبر من صفر");
                    return;
                }
                if (data.itemUnitType === "عدد صحيح") {

                    data.qty = parseInt(data.qty);
                    return;
                }
              

            } else {
                data.qty = 1;
                toastr.warning("وحدة قياس الصنف هي " + data.itemUnitName);
            }
          

        }



        $scope.subTotal = function (index) {
            if (!$scope.order || !$scope.order.itemsRequest || !$scope.order.itemsRequest.length) {
                return;
            }
            if (!isNaN(parseFloat($scope.order.itemsRequest[index].price)) && isFinite($scope.order.itemsRequest[index].price)) {
                if ($scope.order.itemsRequest[index].price < 0) {
                    $scope.order.itemsRequest[index].price = 0;

                }

            } else {
                $scope.order.itemsRequest[index].price = 0;
            }
            var totalprice = $scope.order.itemsRequest[index].price * $scope.order.itemsRequest[index].qty;
            var totalamounts = 0.0;
            for (var i = 0; i < $scope.order.itemsRequest[index].accounts.length; i++) {
                if (!isNaN(parseFloat($scope.order.itemsRequest[index].accounts[i].amount)) && isFinite($scope.order.itemsRequest[index].accounts[i].amount)) {
                    if ($scope.order.itemsRequest[index].accounts[i].amount < 0) {
                        $scope.order.itemsRequest[index].accounts[i].amount = 0;

                    }

                } else {
                    $scope.order.itemsRequest[index].accounts[i].amount = 0;
                };
                //Start calc
                if ($scope.order.itemsRequest[index].accounts[i].type === "crdit") {
                    if (!isNaN(parseFloat($scope.order.itemsRequest[index].accounts[i].amount))) {
                        if ($scope.order.itemsRequest[index].accounts[i].amountType === "جـ") {
                            totalamounts -= parseFloat($scope.order.itemsRequest[index].accounts[i].amount);
                        } else {

                            var transferToamount = (parseFloat(totalprice) + parseFloat(totalamounts)) / 100 * parseFloat($scope.order.itemsRequest[index].accounts[i].amount);
                            totalamounts -= parseFloat(transferToamount);
                        }
                    }

                }
                else if ($scope.order.itemsRequest[index].accounts[i].type === "debit") {
                    if (!isNaN(parseFloat($scope.order.itemsRequest[index].accounts[i].amount))) {
                        if ($scope.order.itemsRequest[index].accounts[i].amountType === "جـ") {
                            totalamounts += parseFloat($scope.order.itemsRequest[index].accounts[i].amount);

                        } else {

                            var transferToamount = (parseFloat(totalprice) + parseFloat(totalamounts)) / 100 * parseFloat($scope.order.itemsRequest[index].accounts[i].amount);
                            totalamounts += parseFloat(transferToamount);
                        }
                    }
                }
            }

            return parseFloat(totalprice) + parseFloat(totalamounts);
        };

        $scope.getGrandTotal = function () {
            if (!$scope.order || !$scope.order.itemsRequest || !$scope.order.itemsRequest.length) {

                return;
            }
            var gTotal = 0;
            for (var item = 0; item < $scope.order.itemsRequest.length; item++) {
                gTotal += parseFloat($scope.subTotal(item));
            }
            var sumTotalAccouts = 0;
            if ($scope.order.totalAccount || $scope.order.totalAccount.length && $scope.order.totalAccount.length > 0) {
                for (var i = 0; i < $scope.order.totalAccount.length; i++) {
                    if ($scope.order.totalAccount[i].type === "crdit") {
                        if (!isNaN(parseFloat($scope.order.totalAccount[i].amount)) && isFinite($scope.order.totalAccount[i].amount)) {
                            if (parseFloat($scope.order.totalAccount[i].amount) < 0) {
                                $scope.order.totalAccount[i].amount = 0;
                            };
                            if ($scope.order.totalAccount[i].amountType === "جـ") {
                                sumTotalAccouts -= parseFloat($scope.order.totalAccount[i].amount);

                            } else {
                                var transferToamount = (parseFloat(gTotal) + parseFloat(sumTotalAccouts)) / 100 * parseFloat($scope.order.totalAccount[i].amount);
                                sumTotalAccouts -= parseFloat(transferToamount);
                            }
                        } else {
                            $scope.order.totalAccount[i].amount = 0;
                        }

                    }
                    else if ($scope.order.totalAccount[i].type === "debit") {
                        if (!isNaN(parseFloat($scope.order.totalAccount[i].amount)) && isFinite($scope.order.totalAccount[i].amount)) {
                            if (parseFloat($scope.order.totalAccount[i].amount) < 0) {
                                $scope.order.totalAccount[i].amount = 0;
                            }
                            if ($scope.order.totalAccount[i].amountType === "جـ") {
                                sumTotalAccouts += parseFloat($scope.order.totalAccount[i].amount);

                            } else {
                                var transferToamount = (parseFloat(gTotal) + parseFloat(sumTotalAccouts)) / 100 * parseFloat($scope.order.totalAccount[i].amount);
                                sumTotalAccouts += parseFloat(transferToamount);
                            }
                        } else {
                            $scope.order.totalAccount[i].amount = 0;
                        }
                    }
                }
            }


            return parseFloat(gTotal) + parseFloat(sumTotalAccouts);
        };

        $scope.keys = function (event) {

            console.log("$event", event);
            if (event.altKey) {
                switch (event.which) {
                    case 37:
                        //alert("shift + left arrow");
                        break;
                    case 38:
                        //alert("shift + up arrow");
                        break;
                    case 39:
                        //alert("shift + right arrow");
                        break;
                    case 78:
                        //$scope.addNewitemPropertyValue();
                        break;
                    default:
                        break;
                }
            }
        }


        $scope.addNewTransferitem = function () {

            for (var i = 0; i < $scope.order.itemsRequest.length; i++) {
                if ($scope.order.itemsRequest[i].qty <= 0 || !$scope.order.itemsRequest[i].itemCode || !$scope.order.itemsRequest[i].itemName) {
                    return;
                }
            }
            var copyAccounts = angular.copy($scope.tableAccounts);
            $scope.order.itemsRequest.push({ itemId: 0, itemCode: null, itemName: null, qty: 0.0, price: 0.0, maxQ: 0.0, realted: null, accounts: copyAccounts });
            console.log("check item with acc", $scope.order.itemsRequest);
            var id = '#ui' + $scope.order.itemsRequest.length;
            setTimeout(function () { $(id).focus() }, 200);


        }

        $scope.removeTransfer = function (item) {
            console.log("Item : ", angular.toJson(item));
            if ($scope.order.itemsRequest && $scope.order.itemsRequest.length > 1) {
                $scope.order.itemsRequest.splice(item, 1);
            }

        };
        $scope.setPay = function (setpay, pay) {
            console.log("pay ", pay)
            console.log("pay SET ", setpay)
            pay.payId = setpay.payId;
            pay.type = setpay.type;
            pay.payName = setpay.payName;
        };
        $scope.addNewPay = function () {
            toastr.clear();
            if (!$scope.order.pay) {
                $scope.order.pay = [];
            }
            if (!$scope.payMethods || $scope.payMethods.length == 0) {
                toastr.warning("لا يوجد طرق دفع متاحة تأكد من اختيار نموذج الامر المصحيح");
                return;
            }
            if (!$scope.order.isNewOrder) {
                return;
            }
            for (var i = 0; i < $scope.order.pay.length; i++) {
                if ($scope.order.pay[i].amount <= 0 || !$scope.order.pay[i].dueDate || $scope.order.pay[i].payId <= 0) {
                    return;
                }
            }
            $scope.order.pay.push({ "payId": 0, "payName": null, "amount": 0.0, "dueDate": null, "note": null, "type": null });
            console.log("check item with acc", $scope.order.itemsRequest);
            var id = '#paymet' + $scope.order.pay.length;
            setTimeout(function () { $(id).focus() }, 200);
        }
        $scope.removepay = function (item) {
            console.log("Item : ", angular.toJson(item));
            if ($scope.order.pay && $scope.order.pay.length && $scope.order.pay.length !== 0) {
                $scope.order.pay.splice(item, 1);
            }

        };

        //order.orderNo

        $scope.newOrder = function () {
            $scope.order = angular.copy($scope.orderTemp);
        };
        $scope.findOrderById = function (data) {
            //GetTransferOrderById
            OrdersConfigurationpersistenceService.action.getOrderObyId(angular.toJson({ orderNo: parseInt(data), orderType: $scope.orderType, type: $scope.oType })).then(
                function (result) {
                    $scope.tableAccounts = result.data.tableAccounts;
                    $scope.payMethods = result.data.payMethods;
                    $scope.totalAccount = result.data.totalAccount;
                    $scope.terms = result.data.terms
                    $scope.order = result.data.order;

                },
                function (error) {
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });
        };
        $scope.findOrderByIdModal = function (data) {
            //GetTransferOrderById
            AccountOrderspersistenceService.action.getOrderRequestById(angular.toJson({ orderNo: parseInt(data) })).then(
                function (result) {
                    $scope.order.transferTo.inventoryId = result.data.inventoryId;
                    $scope.order.transferTo.inventoryId = result.data.inventoryName;
                    $scope.order.orderrequestId = result.data.purchaseOrderId;
                    $scope.order.orderrequestNo = result.data.orderNo;
                    $scope.order.itemsRequest = [];
                    $scope.order.orderDate = moment(result.data.orderDate).format('YYYY-MM-DD');
                    $scope.order.cancelDate = moment(result.data.cancelDate).format('YYYY-MM-DD');
                    $scope.order.recivedDate = moment(result.data.recivedDate).format('YYYY-MM-DD');
                    $scope.order.orderDateSecond = moment(result.data.orderDateSecond).format('YYYY-MM-DD');
                    $scope.order.requestOrderAttched = true;
                    if (!result.data.isDoneSecond && !result.data.cancel) {
                        $scope.order.isNewOrder = true;
                    }
                    var resualtItems = result.data.itemsRequest;
                    for (var i = 0; i < resualtItems.length; i++) {
                        var it = resualtItems[i];


                        var copyAccounts = angular.copy($scope.tableAccounts);
                        $scope.order.itemsRequest.push({ itemId: it.itemId, itemCode: it.itemCode, itemName: it.itemName, qty: it.qty, price: it.price, maxQ: 0.0, realted: it.realted, accounts: copyAccounts, itemUnitName: it.itemUnitName, itemUnitType: it.itemUnitType, note: it.note, noteSecond: it.noteSecond });

                        var findinventory = angular.copy($filter('filter')($scope.items, { itemCode: it.itemCode }, true));
                        console.log("findItem", findinventory[0]);
                        if (findinventory.length && findinventory.length > 1) {
                            $scope.order.itemsRequest[$scope.order.itemsRequest.length - 1].selected = findinventory[0];
                        } else {

                            $scope.order.itemsRequest[$scope.order.itemsRequest.length - 1].selected = {
                                "itemId": it.itemId,
                                "itemCode": it.itemCode,
                                "itemName": it.itemName,
                                "qty": it.qty,
                                "price": it.price,
                                "itemUnitName": it.itemUnitName,
                                "itemUnitType": it.itemUnitType,
                                "itemGroupName": it.itemGroupName,
                                "realted": it.realted
                            };
                        };




                    }
                    var id = '#ui' + $scope.order.itemsRequest.length;
                    // setTimeout(function () { $(id).focus() }, 200);
                    $("#waiting-model").modal("hide");
                },
                function (error) {
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });
        };
        $scope.findOrderByIdModalw = function (data) {
            //GetTransferOrderById
            OrdersConfigurationpersistenceService.action.getOrderObyId(angular.toJson({ orderNo: parseInt(data), orderType: $scope.orderType, type: $scope.oType })).then(
                function (result) {
                    $scope.tableAccounts = result.data.tableAccounts;
                    $scope.payMethods = result.data.payMethods;
                    $scope.totalAccount = result.data.totalAccount;
                    $scope.terms = result.data.terms
                    $scope.order = result.data.order;
                    $("#waitingDone-model").modal("hide");
                },
                function (error) {
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });
        };
        $scope.saveOrder = function () {

            var orderToSave = angular.copy($scope.order);

            if ($scope.order.isNewOrder) {
                console.log(orderToSave.recivedDate);
                orderToSave.orderDate = moment.utc();
            }
            console.log(angular.toJson(orderToSave));


            OrdersConfigurationpersistenceService.action.saveOrder(angular.toJson(orderToSave)).then(
                function (result) {
                    $scope.order = result.data;
                    $scope.order.orderDate = moment($scope.order.orderDate).format('YYYY-MM-DD');
                    $scope.order.cancelDate = moment($scope.order.cancelDate).format('YYYY-MM-DD');
                    $scope.order.recivedDate = moment($scope.order.recivedDate).format('YYYY-MM-DD');
                    $scope.order.orderDateSecond = moment($scope.order.orderDateSecond).format('YYYY-MM-DD');
                },
                function (error) {
                    toastr.clear();
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });


        };

        $scope.cancelOrder = function () {


            var orderToSave = angular.copy($scope.order);
            orderToSave.orderDate = moment.utc();
            orderToSave.cancel = true;
            orderToSave.isNewOrder = false;
            OrdersConfigurationpersistenceService.action.saveOrder(angular.toJson(orderToSave)).then(
                function (result) {

                    $scope.order.userNameCancel = $rootScope.mainUserInfo.userName;
                    $scope.order.cancelDate = moment().format('YYYY-MM-DD');
                    $scope.order.cancel = true;
                    $scope.isNewOrder = false;
                },
                function (error) {
                    toastr.clear();
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    };

                });
        };


        $scope.getWaitingCounts = function () {
            //GetTransferOrderById
            AccountOrderspersistenceService.action.getwaitingPurOrderCounts().then(
                function (result) {
                    $scope.waiting = result.data.waitingcount;
                    $scope.workinprogress = result.data.workinprogress;
                },
                function (error) {
                    toastr.clear();
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });
        }

        $scope.currentPage = 1;
        $scope.ordertoQuery = { orderNo: 0, isDoneSecond: false, cancel: false, all: true, isDoneFirst: false, minDate: '', maxDate: '', pageSize: 15, pageNumber: 0 }
        $scope.ordertoQueryallChenge = function () {
            if ($scope.ordertoQuery.all) {
                $scope.ordertoQuery.isDoneSecond = false;
                $scope.ordertoQuery.isDoneFirst = false;
                $scope.ordertoQuery.cancel = false;
            }


        };
        $scope.ordertoQueryallChengeFirst = function () {
            if ($scope.ordertoQuery.isDoneFirst) {
                $scope.ordertoQuery.isDoneSecond = false;
                $scope.ordertoQuery.all = false;
                $scope.ordertoQuery.cancel = false;
            }


        };

        $scope.ordertoQueryallChengeSecond = function () {
            if ($scope.ordertoQuery.isDoneSecond) {
                $scope.ordertoQuery.isDoneFirst = false;
                $scope.ordertoQuery.all = false;
                $scope.ordertoQuery.cancel = false;
            }

        };
        $scope.resetQuery = function () {
            $scope.ordertoQuery.isDoneFirst = false;
            $scope.ordertoQuery.all = true;
            $scope.ordertoQuery.cancel = false;
            $scope.ordertoQuery.isDoneSecond = false;
        };

        $scope.ordertoQueryallChengecancel = function () {
            if ($scope.ordertoQuery.cancel) {
                $scope.ordertoQuery.isDoneFirst = false;
                $scope.ordertoQuery.all = false;
                $scope.ordertoQuery.isDoneSecond = false;
            }


        };
        $scope.ordertoQueryw = { orderNo: 0, isDoneSecond: false, cancel: false, all: true, isDoneFirst: false, minDate: '', maxDate: '', pageSize: 15, pageNumber: 0 }
        $scope.ordertoQueryallChengew = function () {
            if ($scope.ordertoQueryw.all) {
                $scope.ordertoQueryw.isDoneSecond = false;
                $scope.ordertoQueryw.isDoneFirst = false;
                $scope.ordertoQueryw.cancel = false;
            }


        };
        $scope.ordertoQueryallChengeFirstw = function () {
            if ($scope.ordertoQueryw.isDoneFirst) {
                $scope.ordertoQueryw.isDoneSecond = false;
                $scope.ordertoQueryw.all = false;
                $scope.ordertoQueryw.cancel = false;
            }


        };

        $scope.ordertoQueryallChengeSecondw = function () {
            if ($scope.ordertoQueryw.isDoneSecond) {
                $scope.ordertoQueryw.isDoneFirst = false;
                $scope.ordertoQueryw.all = false;
                $scope.ordertoQueryw.cancel = false;
            }

        };
        $scope.resetQuery = function () {
            $scope.ordertoQueryw.isDoneFirst = false;
            $scope.ordertoQueryw.all = true;
            $scope.ordertoQueryw.cancel = false;
            $scope.ordertoQueryw.isDoneSecond = false;
        };

        $scope.ordertoQueryallChengecancelw = function () {
            if ($scope.ordertoQueryw.cancel) {
                $scope.ordertoQueryw.isDoneFirst = false;
                $scope.ordertoQueryw.all = false;
                $scope.ordertoQueryw.isDoneSecond = false;
            }


        };
        $scope.totalItems = 0;
        $scope.waitingorder = [];
        $scope.waitingorderw = [];
        $scope.getwaitingDonefirst = function () {
            $scope.ordertoQuery.isDoneFirst = true;
            $scope.ordertoQueryallChengeFirst();
            var qu = {
                minDate: $scope.startDate, maxDate: $scope.endDate, pageSize: 15,
                pageNumber: $scope.currentPage, orderNo: parseInt($scope.ordertoQuery.orderNo), isDoneSecond: $scope.ordertoQuery.isDoneSecond, cancel: $scope.ordertoQuery.cancel, all: $scope.ordertoQuery.all, isDoneFirst: $scope.ordertoQuery.isDoneFirst
            }


            AccountOrderspersistenceService.action.getAllOrderpurRequest(JSON.stringify(qu)).then(
                function (result) {
                    $scope.waitingorder = result.data;
                    $("#waiting-model").modal("show");
                },
                function (error) {
                    toastr.clear();
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }
                    $scope.resetQuery();

                });
        }
        $scope.getwaitingDoneOrder = function () {
            $scope.ordertoQueryw.isDoneFirst = true;
            $scope.ordertoQueryallChengeFirstw();
            var qu = {
                minDate: $scope.startDate, maxDate: $scope.endDate, pageSize: 15,
                pageNumber: $scope.currentPage, orderNo: parseInt($scope.ordertoQueryw.orderNo), isDoneSecond: $scope.ordertoQueryw.isDoneSecond, cancel: $scope.ordertoQueryw.cancel, all: $scope.ordertoQueryw.all, isDoneFirst: $scope.ordertoQueryw.isDoneFirst,
                orderType: $scope.orderType, oType: $scope.oType
            }


            OrdersConfigurationpersistenceService.action.getAllOrderwaiting(JSON.stringify(qu)).then(
                function (result) {
                    $scope.waitingorderw = result.data;
                    $("#waitingDone-model").modal("show");
                },
                function (error) {
                    toastr.clear();
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }
                    $scope.resetQueryw();

                });
        };

        $scope.getwaitingDonesecond = function () {
            $scope.ordertoQuery.isDoneSecond = true;
            $scope.ordertoQueryallChengeSecond();
            var qu = {
                minDate: $scope.startDate, maxDate: $scope.endDate, pageSize: 15,
                pageNumber: $scope.currentPage, orderNo: parseInt($scope.ordertoQuery.orderNo), isDoneSecond: $scope.ordertoQuery.isDoneSecond, cancel: $scope.ordertoQuery.cancel, all: $scope.ordertoQuery.all, isDoneFirst: $scope.ordertoQuery.isDoneFirst
            }


            AccountOrderspersistenceService.action.getAllOrderpurRequest(JSON.stringify(qu)).then(
                function (result) {
                    $scope.waitingorder = result.data;
                    $("#waiting-model").modal("show");
                },
                function (error) {
                    toastr.clear();
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }
                    $scope.resetQuery();
                });
        }

        $scope.getQueryRequest = function () {

            var qu = {
                minDate: $scope.startDate, maxDate: $scope.endDate, pageSize: 15,
            }


            AccountOrderspersistenceService.action.getAllOrderpurRequest(JSON.stringify(qu)).then(
                function (result) {
                    $scope.waitingorder = result.data;
                    $("#waiting-model").modal("show");
                },
                function (error) {
                    toastr.clear()
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }
                    $scope.resetQuery();
                });
        }
        $scope.getQueryRequestw = function () {

            var qu = {
                minDate: $scope.startDate, maxDate: $scope.endDate, pageSize: 15, orderType: $scope.orderType, oType: $scope.oType
            }


            OrdersConfigurationpersistenceService.action.getAllOrderwaiting(JSON.stringify(qu)).then(
                function (result) {
                    $scope.waitingorderw = result.data;
                    $("#waitingDone-model").modal("show");
                },
                function (error) {
                    toastr.clear()
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }
                    $scope.resetQuery();
                });
        }

        $scope.vaildOrder = function () {
            if (!$scope.order || !$scope.order.itemsRequest || !$scope.order.itemsRequest.length || $scope.order.subjectAccount.subjectAccountId == 0) {
                return false;
            }
            if ($scope.branchGroub && $scope.branchGroub.id != '0') {
                if (!$scope.order.transferTo || $scope.order.transferTo.inventoryId == 0 || !$scope.order.transferTo.inventoryName) {
                    //console.log("$scope.order.transferTo")
                    return false;
                }
                if ($scope.order.itemsRequest.length < 1) {
                    //console.log("itemsRequest.length")
                    return false;
                }
                for (var i = 0; i < $scope.order.itemsRequest.length; i++) {

                    if (!$scope.order.itemsRequest[i].itemCode || !$scope.order.itemsRequest[i].itemName) {
                        //console.log("itemsRequest[i].itemCode ")
                        return false;
                    }

                    if (!isNaN(parseFloat($scope.order.itemsRequest[i].qty)) && isFinite($scope.order.itemsRequest[i].qty)) {
                        if ($scope.order.itemsRequest[i].qty <= 0) {
                            //console.log("itemsRequest[i].qty <= 0")
                            return false;
                        }


                    } else {
                        //console.log("itemsRequest[i].qty NAN")
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        };
        var checkbrGroup = function () {
            if ($scope.branchGroub && $scope.branchGroub.id != '0') {
                bId = parseInt(angular.copy(parseInt($scope.branchGroub.id)));

                $scope.getInventory();
            }
        }();

    });

}());


