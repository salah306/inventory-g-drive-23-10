﻿(function () {

    'use strict';
    
    angular.module('inventoryModule').factory('purchasesReturnsOrderService', function ($http, $q) {
        var svc = {


     saveSalesOrderconfig: function (or) {
           
            var deferred = $q.defer();
            $http.post('/api/ordersconfiguration/saveSalesOrderconfig', or)
                 .success(deferred.resolve)
                 .error(deferred.reject);
                return deferred.promise;
     },
     savePurchaseOrderconfig: function (or) {

         var deferred = $q.defer();
         $http.post('/api/ordersconfiguration/savePurchaseOrderconfig', or)
              .success(deferred.resolve)
              .error(deferred.reject);
         return deferred.promise;
     },
     saveinventory: function (or) {

         var deferred = $q.defer();
         $http.post('/api/ordersconfiguration/saveinventory', or)
             .success(deferred.resolve)
             .error(deferred.reject);
         return deferred.promise;
     },
     GetOrderGroupNames: function (company) {
       
         var deferred = $q.defer();
          $http.post('/api/POrder/getOrderGroupNames', company)
                 .success(deferred.resolve)
                 .error(deferred.reject);
         return deferred.promise;
     },
     saveOrder: function (company) {

         var deferred = $q.defer();
         $http.post('/api/POrder/saveOrder', company)
             .success(deferred.resolve)
             .error(deferred.reject);
         return deferred.promise;
     },
     getOrderObyId: function (company) {

         var deferred = $q.defer();
          $http.post('/api/POrder/getOrderbyId', company)
             .success(deferred.resolve)
             .error(deferred.reject);
         return deferred.promise;
     },
     GetOrderTemplatebyId: function (o) {

         var deferred = $q.defer();
          $http.post('/api/POrder/GetOrderTemplatebyId', o)
             .success(deferred.resolve)
             .error(deferred.reject);
         return deferred.promise;
     },
     GetAllFinancialList: function () {

         var deferred = $q.defer();
         $http.post('/api/financialStatementsconfig/GetAllFinancialList')
             .success(deferred.resolve)
             .error(deferred.reject);
         return deferred.promise;
     },
     getAllOrderwaiting: function (o) {

         var deferred = $q.defer();
         $http.post('/api/POrder/getAllOrderwaiting', o)
             .success(deferred.resolve)
             .error(deferred.reject);
         return deferred.promise;
     },
     getOrderGroupforBill: function (name) {
         var deferred = $q.defer();

         $http.get('/api/POrder/getOrderGroupforBill/' + name)
             .success(deferred.resolve)
             .error(deferred.reject);

         return deferred.promise;
     },
     GetOrderbyId: function (or) {

         var deferred = $q.defer();

          $http.post('/api/ordersconfiguration/GetOrderbyId', or)
              .success(deferred.resolve)
              .error(deferred.reject);
         return deferred.promise;
     },
     getOrderConfigureName: function () {
           
            var deferred = $q.defer();
             $http.post('/api/ordersconfiguration/getOrdersName')
                    .success(deferred.resolve)
                    .error(deferred.reject);
            return deferred.promise;
        }};
        return svc;
    });

}());