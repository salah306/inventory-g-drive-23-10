﻿
var inventoryModule = angular.module("inventoryModule", [
    "ui.router",
    'dndLists',
    "ui.bootstrap",
    "oc.lazyLoad",
    "ngSanitize",
    "LocalStorageModule",
    "AxelSoft",
    'ngAnimate',
    'xeditable',
    'angucomplete-alt',
    'datatables',
    'ngRoute',
    'ngTable',
    'mwl.confirm',
    'ngStorage',
    'angular-loading-bar',
    'colorpicker.module'
]);

inventoryModule.config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeBar = true;
    cfpLoadingBarProvider.includeSpinner = true;

}]);


/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
inventoryModule.config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);

inventoryModule.config(['$controllerProvider', function ($controllerProvider) {

    $controllerProvider.allowGlobals();
}]);



/* Setup global settings */
inventoryModule.factory('settings', ['$rootScope', function ($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        assetsPath: '../assets',
        globalPath: '../assets/global',
        layoutPath: '../assets/layouts/layout',
    };

    $rootScope.settings = settings;
    return settings;
}]);

/* Setup App Main Controller */
inventoryModule.controller('AppController', ['$scope', '$rootScope', function ($scope, $rootScope) {
    $scope.$on('$viewContentLoaded', function () {
        $rootScope.hello = 'hello from main';
        console.log($rootScope.hello)
        App.initComponents(); // init core components
        //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive 
    });
}]);


/* Setup Layout Part - Header */
inventoryModule.controller('HeaderController', function ($scope, $rootScope) {
    $scope.$on('$includeContentLoaded', function () {
        //$scope.user = $rootScope.mainUserInfo;
        //$scope.logout = $rootScope.mLogout;
        Layout.initHeader(); // init header
    }); 
});

/* Setup Layout Part - Sidebar */
inventoryModule.controller('SidebarController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initSidebar(); // init sidebar
    });
}]);

/* Setup Layout Part - Quick Sidebar */
inventoryModule.controller('QuickSidebarController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        setTimeout(function () {
            QuickSidebar.init(); // init quick sidebar        
        }, 2000)
    });
}]);

/* Setup Layout Part - Theme Panel */
inventoryModule.controller('ThemePanelController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Demo.init(); // init theme panel
    });
}]);

/* Setup Layout Part - Footer */
inventoryModule.controller('FooterController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initFooter(); // init footer
    });
}]);

/* Setup Rounting For All Pages */
inventoryModule.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    // Redirect any unmatched url
    $urlRouterProvider.otherwise("/dashboard");

    $stateProvider

        // Dashboard
        .state('dashboard', {
            url: "/dashboard",
            templateUrl: "App.Web/APP/Dashboard/Views/dashboard.html",
            data: { pageTitle: 'الرئيسية' },
            controller: "DashboardController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../App.Web/assets/global/plugins/morris/morris.css',
                            '../App.Web/assets/global/plugins/morris/morris.min.js',
                            '../App.Web/assets/global/plugins/morris/raphael-min.js',
                            '../App.Web/assets/global/plugins/jquery.sparkline.min.js',
                            '../App.Web/assets/pages/scripts/dashboard.min.js',
                            'App.Web/APP/Dashboard/Components/DashboardController.js',
                            
                        ]
                    });
                }]
            }
        })


        //generaljournal
        .state('general-journal', {
            url: "/general-journal",
            templateUrl: "App.Web/GeneralJournal/Views/generaljournal.html",
            data: { pageTitle: 'دفتر اليومية' },
            controller: "GeneralJournalController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../App.Web/assets/global/plugins/angular-ui-select/dist/select.min.css',
                            '../App.Web/assets/global/plugins/angular-ui-select/dist/select.min.js',
                            'App.Web/APP/GeneralPage/Components/GeneralJournalController.js',
                        ]
                    });
                }]
            }
        })
        //  Ordeers
        .state('purchases-order', {
            url: "/purchases-order",
            templateUrl: "App.Web/APP/purchasesOrder/Views/purchasesOrder.html",
            data: { pageTitle: 'امر شراء' },
            controller: "purchasesOrderController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'App.Web/APP/purchasesOrder/Services/purchasesOrderService.js',
                            'App.Web/APP/purchasesOrder/Components/purchasesOrderController.js',
                        ]
                    });
                }]
            }
        })

        .state('purchases-returns-order', {
            url: "/purchases-returns-order",
            templateUrl: "App.Web/APP/purchasesReturnsOrder/Views/purchasesReturnsOrder.html",
            data: { pageTitle: 'امر رد مشتريات' },
            controller: "purchasesReturnsOrderController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'App.Web/APP/purchasesReturnsOrder/Services/purchasesReturnsOrderService.js',
                            'App.Web/APP/purchasesReturnsOrder/Components/purchasesReturnsOrderController.js',
                        ]
                    });
                }]
            }
        })

        .state('sales-order', {
            url: "/sales-order",
            templateUrl: "App.Web/APP/salesOrder/Views/salesOrderController.html",
            data: { pageTitle: 'امر بيع' },
            controller: "salesOrderController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                           
                            'App.Web/APP/salesOrder/Services/salesOrderService.js',
                            'App.Web/APP/salesOrder/Components/salesOrderController.js',
                        ]
                    });
                }]
            }
        })

        .state('sales-returns-order', {
            url: "/sales-returns-order",
            templateUrl: "App.Web/APP/salesReturnsOrder/Views/salesReturnsOrder.html",
            data: { pageTitle: 'امر رد مبيعات' },
            controller: "salesReturnsController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'App.Web/APP/salesReturnsOrder/Services/salesReturnsOrderService.js',
                            'App.Web/APP/saleReturnsOrder/Components/salesReturnsOrderController.js',
                        ]
                    });
                }]
            }
        })
          //users
        .state('EasyStoreUsers', {
            url: "/easystoreusers",
            templateUrl: "templates/easyStoreUsers.html",
            data: { pageTitle: 'مستخدمين' },
            controller: "EasyStoreUsersController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/EasyStoreUsersController.js',
                              '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ]
                    });
                }]
            }
        })

        .state('ordersconfiguration', {
            url: "/ordersconfiguration",
            templateUrl: "templates/configureOrders.html",
            data: { pageTitle: 'اعداد الفواتير' },
            controller: "configureOrdersController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/services/servicesOrdersConfiguration/OrdersConfigurationStrategies.js',
                            'Scripts/App/services/servicesOrdersConfiguration/OrdersConfigurationService.js',
                            'Scripts/App/controllers/configureOrdersController.js',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ]
                    });
                }]
            }
        })


        .state('purchaserequest', {
            url: "/purchaserequest",
            templateUrl: "templates/purchaseRequest.html",
            data: { pageTitle: 'طلب شراء' },
            controller: "purchaseRequestController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/purchaseRequestController.js',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ]
                    });
                }]
            }
        })
        .state('inorder', {
            url: "/inorder",
            templateUrl: "templates/inventoryRequest.html",
            data: { pageTitle: 'اذن اضافة' },
            controller: "inventoryRequestInController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/inventoryRequestInController.js',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ]
                    });
                }]
            }
        })

        .state('outorder', {
            url: "/outorder",
            templateUrl: "templates/inventoryRequest.html",
            data: { pageTitle: 'اذن صرف' },
            controller: "inventoryRequestOutController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/inventoryRequestOutController.js',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ]
                    });
                }]
            }
        })
                  //inventoryTransfer
        .state('inventorytransfer', {
            url: "/inventorytransfer",
            templateUrl: "templates/inventoryTransfer.html",
            data: { pageTitle: 'نحويل صادر' },
            controller: "inventoryTransferControlle",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/inventoryTransferControlle.js',
                              '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ]
                    });
                }]
            }
        })

        .state('inventorytransferin', {
            url: "/inventorytransferin",
            templateUrl: "templates/inventoryTransferin.html",
            data: { pageTitle: 'تحويل وارد' },
            controller: "inventoryTransferInControlle",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/inventoryTransferInControlle.js',
                                '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ]
                    });
                }]
            }
        })

                        //subLedgerController
        .state('subLedger', {
            url: "/subLedger?acId&bId",
            templateUrl: "templates/subLedger.html",
            data: { pageTitle: 'الاستاذ المساعد' },
            controller: "subLedgerController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/subLedgerController.js',
                              '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ]
                    });
                }]
            }
        })

  
          //  generalLedgerController
        .state('generalLedger', {
            url: "/generalLedger",
            templateUrl: "templates/generalLedger.html",
            data: { pageTitle: 'الاستاذ العام' },
            controller: "generalLedgerController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/generalLedgerController.js',
                              '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ]
                    });
                }]
            }
        })

        //  Ordeers
        .state('orders', {
            url: "/orders",
            templateUrl: "templates/orders.html",
            data: { pageTitle: 'مشتريات' },
            controller: "ordersController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/ordersController.js',
                              '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ]
                    });
                }]
            }
        })
                .state('purchasereturns', {
                    url: "/purchasereturns",
                    templateUrl: "templates/orders.html",
                    data: { pageTitle: 'رد مشتريات' },
                    controller: "PurReturnsController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'inventoryModule',
                                insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                                files: [
                                    '../assets/global/plugins/morris/morris.css',
                                    '../assets/global/plugins/morris/morris.min.js',
                                    '../assets/global/plugins/morris/raphael-min.js',
                                    '../assets/global/plugins/jquery.sparkline.min.js',
                                    'Scripts/App/controllers/PurReturnsController.js',
                                      '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                    '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                                ]
                            });
                        }]
                    }
                })

                .state('salesreturns', {
                    url: "/salesreturns",
                    templateUrl: "templates/orders.html",
                    data: { pageTitle: 'رد مبيعات' },
                    controller: "SalesReturnsController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'inventoryModule',
                                insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                                files: [
                                    '../assets/global/plugins/morris/morris.css',
                                    '../assets/global/plugins/morris/morris.min.js',
                                    '../assets/global/plugins/morris/raphael-min.js',
                                    '../assets/global/plugins/jquery.sparkline.min.js',
                                    'Scripts/App/controllers/SalesReturnsController.js',
                                      '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                    '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                                ]
                            });
                        }]
                    }
                })

            //  Ordeers
        .state('salesorders', {
            url: "/salesorders",
            templateUrl: "templates/orders.html",
            data: { pageTitle: 'مبيعات' },
            controller: "SalesordersController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/SalesordersController.js',
                              '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ]
                    });
                }]
            }
        })


       //  Cusomer
        .state('customers', {
            url: "/customers",
            templateUrl: "templates/accountProperty.html",
            data: { pageTitle: 'عملاء' },
            controller: "CustomersController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/CustomersController.js',
                              'bower_components/angular-ui-select/dist/select.js',
                            'bower_components/angular-ui-select/dist/select.js'
                        ]
                    });
                }]
            }
        })

         // Suppliers
        .state('suppliers', {
            url: "/suppliers",
            templateUrl: "templates/accountProperty.html",
            data: { pageTitle: 'موردين' },
            controller: "SuppliersController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/SuppliersController.js',
                            'bower_components/angular-ui-select/dist/select.js',
                            'bower_components/angular-ui-select/dist/select.js'
                        ]
                    });
                }]
            }
        })

        .state('cash', {
            url: "/cash",
            templateUrl: "templates/accountProperty.html",
            data: { pageTitle: 'نقود' },
            controller: "cashController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/cashController.js',
                            'bower_components/angular-ui-select/dist/select.js',
                            'bower_components/angular-ui-select/dist/select.js'
                        ]
                    });
                }]
            }
        })


        .state('banks', {
            url: "/banks",
            templateUrl: "templates/accountProperty.html",
            data: { pageTitle: 'بنوك' },
            controller: "bankController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/bankController.js',
                            'bower_components/angular-ui-select/dist/select.js',
                            'bower_components/angular-ui-select/dist/select.js'
                        ]
                    });
                }]
            }
        })

        .state('notespayable', {
            url: "/notespayable",
            templateUrl: "templates/accountProperty.html",
            data: { pageTitle: 'أوراق الدفع' },
            controller: "notesPayableController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/notesPayableController.js',
                            'bower_components/angular-ui-select/dist/select.js',
                            'bower_components/angular-ui-select/dist/select.js'
                        ]
                    });
                }]
            }
        })

    .state('notesreceivable', {
        url: "/notesreceivable",
        templateUrl: "templates/accountProperty.html",
        data: { pageTitle: 'أوراق قبض' },
        controller: "notesReceivableController",
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                return $ocLazyLoad.load({
                    name: 'inventoryModule',
                    insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                    files: [
                        '../assets/global/plugins/morris/morris.css',
                        '../assets/global/plugins/morris/morris.min.js',
                        '../assets/global/plugins/morris/raphael-min.js',
                        '../assets/global/plugins/jquery.sparkline.min.js',
                        'Scripts/App/controllers/notesReceivableController.js',
                        'bower_components/angular-ui-select/dist/select.js',
                        'bower_components/angular-ui-select/dist/select.js'
                    ]
                });
            }]
        }
    })
        .state('items', {
            url: "/items",
            templateUrl: "templates/items.html",
            data: { pageTitle: 'اصناف' },
            controller: "ItemController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/ItemController.js',
                            'bower_components/angular-ui-select/dist/select.js',
                            'bower_components/angular-ui-select/dist/select.js'
                        ]
                    });
                }]
            }
        })
              //  Ordeers
        .state('inventorycards', {
            url: "/inventorycards",
            templateUrl: "templates/inventoryCard.html",
            data: { pageTitle: 'مواد وسلع' },
            controller: "inventoryCardController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/ordersController.js',
                            'bower_components/angular-ui-select/dist/select.js',
                            'bower_components/angular-ui-select/dist/select.js'
                        ]
                    });
                }]
            }
        })

                  //  financialStatements
        .state('financialStatements', {
            url: "/financialStatements",
            templateUrl: "templates/financialStatements.html",
            data: { pageTitle: 'قوائم مالية' },
            controller: "financialStatementsController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/financialStatementsController.js',
                            'bower_components/angular-ui-select/dist/select.js',
                            'bower_components/angular-ui-select/dist/select.js'
                        ]
                    });
                }]
            }
        })

        //trialBalanceController
        .state('trialBalance', {
            url: "/trialBalance",
            templateUrl: "templates/trialBalance.html",
            data: { pageTitle: 'ميزان المراجعة' },
            controller: "trialBalanceController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            'Scripts/App/controllers/trialBalanceController.js',
                              '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ]
                    });
                }]
            }
        })

        // User Profile Account
        .state("configuration", {
            url: "/configuration",
            templateUrl: "templates/configuration.html",
            data: { pageTitle: 'اعداد الشركات' },
            controller: "accountController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/select2/js/select2.full.min.js',
                            '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
                            '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
                            '../assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
                            '../assets/pages/scripts/form-wizard.js',
                            'Scripts/angular-ui-tree/angular-ui-tree.css',
                            'Scripts/App/controllers/accountController.js',
                              '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'

                        ]
                    });
                }]
            }
        })

        .state("configurationlist", {
            url: "/configurationlist",
            templateUrl: "templates/configFinancialstatement.html",
            data: { pageTitle: 'اعداد القوائم' },
            controller: "configFinancialstatementController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'inventoryModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/select2/js/select2.full.min.js',
                            '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
                            '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
                            '../assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js',
                            '../assets/pages/scripts/form-wizard.js',
                            'Scripts/App/services/servicesOrdersConfiguration/OrdersConfigurationStrategies.js',
                            'Scripts/App/services/servicesOrdersConfiguration/OrdersConfigurationService.js',
                            'Scripts/angular-ui-tree/angular-ui-tree.css',
                            'Scripts/App/controllers/configFinancialstatementController.js',
                            'bower_components/angular-ui-select/dist/select.js',
                            'bower_components/angular-ui-select/dist/select.js'

                        ]
                    });
                }]
            }
        })


      .state('login', {
          url: "/login",
          templateUrl: "templates/login.html",
          data: { pageTitle: 'Log In' },
          controller: "loginController",
          resolve: {
              deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                  return $ocLazyLoad.load([{
                      name: 'inventoryModule',
                      insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                      files: [
                          
                           '../assets/pages/css/login-3-rtl.min.css',
                          '../assets/global/plugins/select2/css/select2.css',
                            '../assets/global/plugins/select2/css/select2-bootstrap.min.css',
                            '../assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
                            '../assets/global/plugins/jquery-validation/js/additional-methods.min.js',
                            '../assets/global/plugins/select2/js/select2.full.min.js',
                          '../assets/pages/scripts/login.js',
                           'Scripts/App/controllers/loginController.js',
                           '/Scripts/angular-local-storage.js',
                           '/Scripts/App/services/authService.js',
                           '/Scripts/App/controllers/signupController.js',
                          '/Scripts/App/services/authInterceptorService.js',
                         '/Scripts/App/controllers/indexController.js',
                          ]
                  }]);
              }]
          }
      })

}]);

/* Init global settings and run the app */
inventoryModule.run(function ($rootScope, settings, $state, editableOptions) {
  
    $rootScope.$state = $state; // state to be accessed from view
    $rootScope.$settings = settings; // state to be accessed from view
    editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
});



inventoryModule.run(['authService', function (authService) {
    authService.fillAuthData();
}]);
inventoryModule.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});


inventoryModule.config(function ($locationProvider, $provide) {

    //  $routeProvider.when('/Inventory/Items', { templateUrl: '/templates/items.html', controller: 'ItemsController' });


    //$locationProvider.html5Mode({
    //    enabled: true,
    //    requireBase: false
    //});



    $provide.constant('indexedDB', window.indexedDB);

    $provide.constant('_', window._);

    $provide.constant('localStorage', window.localStorage);

    $provide.constant('Offline', window.Offline);

    $provide.value('nullAccountCategory', {
        id: '',
        insertDate: new Date(-8640000000000000),
        modifiedDate: new Date(-8640000000000000)
    });

    $provide.value('nullAccount', {
        id: '',
        insertDate: new Date(-8640000000000000),
        modifiedDate: new Date(-8640000000000000)
    });
    $provide.value('nullAccountDetail', {
        id: '',
        insertDate: new Date(-8640000000000000),
        modifiedDate: new Date(-8640000000000000)
    });
    $provide.value('nullAccountOrder', {
        id: '',
        insertDate: new Date(-8640000000000000),
        modifiedDate: new Date(-8640000000000000)
    });
    $provide.value('nullAccountMovement', {
        id: '',
        insertDate: new Date(-8640000000000000),
        modifiedDate: new Date(-8640000000000000)
    });
    $provide.value('nullBalanceSheet', {
        id: '',
        insertDate: new Date(-8640000000000000),
        modifiedDate: new Date(-8640000000000000)
    });

    $provide.value('nullBalanceSheetType', {
        id: '',
        insertDate: new Date(-8640000000000000),
        modifiedDate: new Date(-8640000000000000)
    });
    $provide.value('nullbranch', {
        id: '',
        insertDate: new Date(-8640000000000000),
        modifiedDate: new Date(-8640000000000000)
    });

    $provide.value('nullcompany', {
        id: '',
        insertDate: new Date(-8640000000000000),
        modifiedDate: new Date(-8640000000000000)
    });

    $provide.value('dbModel', {
        name: 'inventory',
        version: '1',
        instance: null,
        objectStoreName: 'AccountCategories',
        keyName: 'id',
        upgrade: function (e) {

            var db = e.target.result;
            if (!db.objectStoreNames.contains('AccountCategories')) {
                db.createObjectStore(('AccountCategories'), {
                    keyPath: 'id'
                });
            }

        }
    });

});

