﻿using Inventory.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Inventory.DataLayer
{
    public class AccountCategoryPropertiesValuesConfiguration : EntityTypeConfiguration<AccountCategoryPropertiesValue>
    {
        public AccountCategoryPropertiesValuesConfiguration()
        {
            Property(a => a.AccountCategoryPropertiesValueName)
                .HasMaxLength(80)
                 .IsOptional();

        }
    }
}