﻿using Inventory.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Inventory.DataLayer
{
    public class AccountMovementConfiguration : EntityTypeConfiguration<AccountMovement>
    {
        public AccountMovementConfiguration()
        {
            Property(a => a.AccountMovementNote)
              .HasMaxLength(200)
              .IsOptional();

          

            Property(a => a.Debit).HasPrecision(18, 2);
            Property(a => a.Crdit).HasPrecision(18, 2);

        }
    }
}