﻿using Inventory.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Inventory.DataLayer
{
    public class AccountCategoryPropertiesConfiguration : EntityTypeConfiguration<AccountCategoryProperties>
    {
        public AccountCategoryPropertiesConfiguration()
        {
            Property(a => a.AccountCategoryPropertiesName)
                .HasMaxLength(80)
                .IsRequired();

        }
    }
}