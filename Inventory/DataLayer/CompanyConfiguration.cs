﻿using Inventory.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Inventory.DataLayer
{
    public class CompanyConfiguration : EntityTypeConfiguration<Company>
    {
        public CompanyConfiguration()
        {
            Property(co => co.CompanyName)
                .HasMaxLength(200)
                .IsRequired()
                .HasColumnAnnotation("Index",
                new IndexAnnotation(new IndexAttribute("AK_Company_CompanyName") { IsUnique = true }));

            

        }
    }

    public class ApplicationUserComapnyConfiguration : EntityTypeConfiguration<ApplicationUserComapny>
    {
        public ApplicationUserComapnyConfiguration()
        {

            Property(Ap => Ap.CompanyId)
                .IsOptional();
            Property(Ap => Ap.ApplicationUserId)
               .IsOptional();

        }
    }

    //CompanyRole
    public class CompanyRoleConfiguration : EntityTypeConfiguration<CompanyRole>
    {
        public CompanyRoleConfiguration()
        {
            Property(r => r.Name)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnAnnotation("Index",
                new IndexAnnotation(new IndexAttribute("AK_CompanyRole_Name") { IsUnique = true }));

        }
    }

    public class VocabularyConfiguration : EntityTypeConfiguration<Vocabulary>
    {
        public VocabularyConfiguration()
        {
            Property(r => r.Name)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnAnnotation("Index",
                new IndexAnnotation(new IndexAttribute("AK_Vocabulary_Name") { IsUnique = true }));

        }
    }


    public class AppUserCompanyRoleConfiguration : EntityTypeConfiguration<AppUserCompanyRole>
    {
        public AppUserCompanyRoleConfiguration()
        {
           

        }
    }

}