﻿/**
* When you want to use this gulpfile.js, make sure you execute the gulp.ps1 PowerShell script first, so everything is installed correctly.
*/
(function (require) {
"use strict";
    // Gulp.
var gulp = require("gulp");
// Gulp plugins.
var plumber = require("gulp-plumber");
var livereload = require("gulp-livereload");
    // Gulp plumber error handler
var onError = function (err) {
console.log(err);
};
/**
* The following file will be reloaded, when one of the "watched" *.html or 8.js files has changed.
*/
gulp.task('reloadHtml', function () {
    gulp.src([
    './templates/**/*.html'
    ])
.on('error', onError)
.pipe(gulp.dest('dist'))
.pipe(livereload());
});

gulp.task('reload', function () {
    gulp.src([
    "./Scripts/App/**/*.js",
    ])
.on('error', onError)
.pipe(gulp.dest('dist'))
.pipe(livereload());
});


/**
* All *.css files will be reloaded, when one of the "watched" *.css files has changed.
*/
gulp.task('reloadCss', function () {
gulp.src([
'./Content/**/*.css',
])
.on('error', onError)
.pipe(gulp.dest('dist'))
.pipe(livereload());
});
/**
Watch *.css files, but only reload css, not the entire page.
Watch *.html and *.js files, when a change is detected, reload the whole page.
*/
gulp.task("watch", function () {
livereload.listen();
gulp.watch([
'./Content/appcss/**/*.css',

], ["reloadCss"]);

gulp.watch([
"./Scripts/App/**/*.js"
], ["reload"]);

gulp.watch([
'./templates/**/*.html'
], ["reloadHtml"]);

});
/**
When the user enters "gulp" on the command line, the default task will automatically be called.
This default task below, will run all other task automatically.
So when the user enters "gulp" on the command line all task are run.
*/
gulp.task("default", ["watch", "reload"]);
}(require));