﻿(function () {

    'use strict';
    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("AccountCategoryPropertiesValuesController", function ($scope, _, AccountCategoryPropertiesValuespersistenceService, Offline, $location ,  $routeParams) {

        var AccountCategoryPropertiesValueName = $routeParams.name;
        $scope.catName = $routeParams.name;
        console.log(AccountCategoryPropertiesValueName);
        $scope.showList = false;
        $scope.AccountCategoryPropertiesValues = [];
        $scope.AccountCategoryPropertiesValuesNames = [];
        $scope.AccountCategoryPropertiesValuepropertyValues = [];
        $scope.PropertyValues = [];
        var getData = function () {

            AccountCategoryPropertiesValuespersistenceService.action.getById(AccountCategoryPropertiesValueName).then(
                function (AccountCategoryPropertiesValues) {
                    $scope.AccountCategoryPropertiesValues = AccountCategoryPropertiesValues;
                   
                    
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (AccountCategoryPropertiesValues.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };
        var lazyGetData = _.debounce(getData, 2);

        Offline.on('confirmed-down', lazyGetData);
        Offline.on('confirmed-up', lazyGetData);

        lazyGetData();

        var getAccountCategoryPropertiesValueNameData = function () {

            AccountCategoryPropertiesValuespersistenceService.action.getAll().then(
                function (AccountCategoryPropertiesValues) {
                    $scope.AccountCategoryPropertiesValuesNames = AccountCategoryPropertiesValues;

                    console.log(AccountCategoryPropertiesValues);
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (AccountCategoryPropertiesValues.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };

       
        var lazyGetAccountCategoryPropertiesValueNameData = _.debounce(getAccountCategoryPropertiesValueNameData, 2);

        Offline.on('confirmed-down', lazyGetAccountCategoryPropertiesValueNameData);
        Offline.on('confirmed-up', lazyGetAccountCategoryPropertiesValueNameData);

        lazyGetAccountCategoryPropertiesValueNameData();
       

        function getValueByKey(key, data) {
            var i, len = data.length;

            for (i = 0; i < len; i++) {
                if (data[i] && data[i].hasOwnProperty(key)) {
                    return data[i][key];
                }
            }

            return -1;
        }

        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;   //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        // $scope.catogeries = AccountCategoryPropertiesValueRepository.getcatogery();

        $('#loading').hide();


        // Save 

        $scope.showSuccessMessage = false;
        $scope.showFillOutFormMessage = false;
        $scope.isOnline = true;
        $scope.addAccountCategoryPropertiesValue = {};







        var hasAccountCategoryPropertiesValueToSave = function () {



            console.log($scope.addAccountCategoryPropertiesValue)

            var hasValue = function (value) {

                if (typeof value === 'string') {
                    return value.length > 0;
                }

                return value > 0;
            };

            var returnValue =
                       hasValue($scope.addAccountCategoryPropertiesValue.name)
                    && hasValue($scope.addAccountCategoryPropertiesValue.price)
                    && hasValue($scope.addAccountCategoryPropertiesValue.catogery);

            return returnValue;
        };

        $scope.save = function () {
            if ($scope.addAccountCategoryPropertiesValue.id === null || $scope.addAccountCategoryPropertiesValue.id === undefined) {
                $scope.addAccountCategoryPropertiesValue.id = $scope.addAccountCategoryPropertiesValue.name;
            }

            var saveAccountCategoryPropertiesValue = hasAccountCategoryPropertiesValueToSave();


            var AccountCategoryPropertiesValue = $scope.addAccountCategoryPropertiesValue;


            //;

            AccountCategoryPropertiesValuespersistenceService.action.save(AccountCategoryPropertiesValue).then(
                function (result) {
                    $scope.showSuccessMessage = true;
                    $scope.showErrorMessage = false;
                    $scope.addAccountCategoryPropertiesValue = {};
                },
                function (error) {
                    $scope.showSuccessMessage = false;
                    $scope.showErrorMessage = true;
                });



        };

        Offline.on('confirmed-down', function () {
            $scope.$apply(function () {
                $scope.isOnline = false;
            });
        });

        Offline.on('confirmed-up', function () {
            $scope.$apply(function () {
                $scope.isOnline = true;
            });
        });

        //Edit and update
        var AccountCategoryPropertiesValueToEdit = [];

        $scope.editingAccountCategoryPropertiesValue = {};


        $scope.modify = function (AccountCategoryPropertiesValue) {
            for (var i = 0, length = $scope.AccountCategoryPropertiesValues.length; i < length; i++) {
                $scope.editingAccountCategoryPropertiesValue[$scope.AccountCategoryPropertiesValues[i].id] = false;
            }
            AccountCategoryPropertiesValueToEdit = angular.copy(AccountCategoryPropertiesValue);
            $scope.addAccountCategoryPropertiesValue = AccountCategoryPropertiesValue;
            $scope.editingAccountCategoryPropertiesValue[AccountCategoryPropertiesValue.id] = true;

        };


        $scope.update = function (AccountCategoryPropertiesValue) {
            $scope.editingAccountCategoryPropertiesValue[AccountCategoryPropertiesValue.id] = false;
        };

        $scope.cancel = function (AccountCategoryPropertiesValue) {

            AccountCategoryPropertiesValue.name = AccountCategoryPropertiesValueToEdit.name;
            AccountCategoryPropertiesValue.price = AccountCategoryPropertiesValueToEdit.price;
            AccountCategoryPropertiesValue.catogery = AccountCategoryPropertiesValueToEdit.catogery;

            $scope.editingAccountCategoryPropertiesValue[AccountCategoryPropertiesValue.id] = false;

        };

        $scope.reset = function () {
            for (var i = 0, length = $scope.AccountCategoryPropertiesValues.length; i < length; i++) {
                $scope.editingAccountCategoryPropertiesValue[$scope.AccountCategoryPropertiesValues[i].id] = false;
            }
            $scope.addAccountCategoryPropertiesValue = {};
        };


        //delte
        var delteAccountCategoryPropertiesValue = {};
        $scope.AccountCategoryPropertiesValueToDelte = function (AccountCategoryPropertiesValue) {

            delteAccountCategoryPropertiesValue = AccountCategoryPropertiesValue;
        };


        $scope.delete = function () {


            AccountCategoryPropertiesValuespersistenceService.action.Delete(delteAccountCategoryPropertiesValue.id).then(
                function (result) {
                    $scope.AccountCategoryPropertiesValues.splice($scope.AccountCategoryPropertiesValues.indexOf(delteAccountCategoryPropertiesValue), 1);
                    delteAccountCategoryPropertiesValue = {};
                },
                function (error) {
                    $scope.error = error;
                });
        };


        AccountCategoryPropertiesValuespersistenceService.getById(AccountCategoryPropertiesValueName).then(
            function (AccountCategoryPropertiesValues) {
                $scope.AccountCategoryPropertiesValues = AccountCategoryPropertiesValues;
            },
            function (error) {
                $scope.error = error;
            });

    });

}());
