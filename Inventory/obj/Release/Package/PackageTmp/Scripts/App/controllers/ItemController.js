﻿
(function () {
  

    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("ItemController", function ($scope, $timeout, _, $localStorage, AccountspersistenceService, $rootScope, settings, $filter,
        AccountOrderspersistenceService, Offline, EasyStoreUserspersistenceService, $location, branchesremotePersistenceStrategy, $sce, globalService, $q, $log) {


        
        var bId;
        $scope.selectedinventories = [];
        $scope.branchesGroub = [];
        $scope.itemGroups = [];
        $scope.inventories =[];
        $scope.items =[];
        $scope.totalPages = 0;
        $scope.totalItems = 0;
        $scope.units = [];
        $scope.types = [];
        $scope.newObjects = {};
        $scope.unitTypes = [];
        $scope.itemGroupsOrginal = [];
        $scope.showUnitWarning = false;
        $scope.showGroupWarning = false;
        $scope.cancelGroupEdit = false;
        $scope.selectedGroup = { selected: {}};
        $scope.iteminfo = { pageNumber: 1, search: null, orderby: "qty", inventoryId: 0, groupId: 0, orderType: true, pageSize: 10 };
        $scope.selectConfig = {
            requiredMin: 0
        };
        $scope.branchGroub = $rootScope.mainbranchGroub;

        $scope.$watch('$root.mainbranchGroub.id', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal !== '0') {
                var findCompanybr = angular.copy($filter('filter')($rootScope.mainbranchesGroub, { id: newVal }, true));
                if (findCompanybr) {
                    $scope.branchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    $rootScope.mainbranchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    console.log('$rootScope.mainbranchGroub', $rootScope.mainbranchGroub);
                    if (findCompanybr.length) {
                  
                        bId = parseInt(angular.copy(parseInt(findCompanybr[0].id)));

                        EasyStoreUserspersistenceService.action.branchGroupSt(JSON.stringify($scope.branchGroub)).then(function () { }, function () { });
                        $scope.loadItems();
                    }
                }

            }
        });

   

        $scope.loadItems = function () {
            EasyStoreUserspersistenceService.action.loaditemsInfoVM(JSON.stringify($scope.branchGroub)).then(
                function (result) {
                    $scope.itemGroups = result.data.itemGroups;
                    $scope.itemGroupsOrginal =angular.copy( result.data.itemGroups);
                    $scope.inventories = result.data.inventories;
                    $scope.inventoriesforMulti = angular.copy(result.data.inventories);
                    $scope.selectedGroup = { selected: angular.copy(result.data.itemGroups[0]) };
                    $scope.items = result.data.items;
                    $scope.totalItems = result.data.count
                    $scope.totalPages = result.data.totalPages;
                    $scope.iteminfo.pageSize = result.data.pageSize;
                    $scope.units = result.data.units;
                    $scope.types = result.data.types;
                    $scope.newObjects = result.data.newObjctes;
                    $scope.unitTypes = result.data.unitTypes;
                },
                function (error) {
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });
        };
        $scope.getItemInfo = function () {
            if (!$scope.iteminfo.inventoryId) {
                $scope.iteminfo.inventoryId = 0;
            }
            if (!$scope.iteminfo.groupId) {
                $scope.iteminfo.groupId = 0;
            }
            EasyStoreUserspersistenceService.action.getitemsInfoVM(JSON.stringify($scope.iteminfo)).then(
                function (result) {
                   
                    $scope.items = result.data.items;
                    $scope.totalItems = result.data.count;
                    $scope.totalPages = result.data.totalPages;
                    $scope.iteminfo.pageSize = result.data.pageSize;
                },
                function (error) {
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });
        };
        $scope.getgroupInfo = function (data) {
            $scope.inventoriesforMulti = angular.copy($scope.inventories);
        };


        $scope.addGroup = function (name) {
            var newGroup = angular.copy($scope.newObjects.itemsGroupDTO);
            $scope.oldGroup = angular.copy($scope.selectedGroup.selected);
            newGroup.groupName = name;
            newGroup.unit = angular.copy($scope.units[0]);
            $scope.selectedGroup.selected = newGroup;
            $scope.selectedGroup.selected.inventoryies = [];
            $scope.inventoriesforMulti = angular.copy($scope.inventories);
            $scope.cancelGroupEdit = true;

        };
        $scope.addUnit = function (name) {
            var newunit = angular.copy($scope.newObjects.unitDTO);
            $scope.oldUnit = angular.copy($scope.selectedGroup.selected.unit);
            newunit.unitName = name;
            if (!$scope.selectedGroup.selected.isNew) {
                $scope.selectedGroup.selected.isToEdit = true;
                $scope.cancelGroupEdit = true;
            }
            $scope.selectedGroup.selected.unit = newunit;
           

        };
        $scope.editGroup = function () {
            $scope.selectedGroup.selected.isToEdit = true;
            $scope.cancelGroupEdit = true;
        };
        $scope.editUnit = function () {
            if (!$scope.selectedGroup.selected.isNew) {
                $scope.selectedGroup.selected.isToEdit = true;
                $scope.cancelGroupEdit = true;
            }
            $scope.cancelGroupEdit = true;

        };
        $scope.getUnitnfo = function (data) {
            console.log(data);
            if (!$scope.selectedGroup.selected.isNew) {
                var find = angular.copy($filter('filter')($scope.itemGroupsOrginal, { groupId: $scope.selectedGroup.selected.groupId }, true)[0]);

                if (data.unitId !== find.unit.unitId) {
                    if (!$scope.selectedGroup.selected.isNew) {
                        $scope.selectedGroup.selected.isToEdit = true;
                        $scope.cancelGroupEdit = true;
                    }
                }
            }
        
        };
        $scope.$watch('selectedGroup.selected.inventoryies', function (newVal, oldVal) {
            if ($scope.selectedGroup.selected.inventoryies && $scope.selectedGroup.selected.inventoryies.length && $scope.selectedGroup.selected.groupId !== 0 && $scope.itemGroupsOrginal && $scope.itemGroupsOrginal.length) {
               
                var findgroup = angular.copy($filter('filter')($scope.itemGroupsOrginal, { groupId: $scope.selectedGroup.selected.groupId }, true)[0]);
                var groupsDiff = _.isEqual(findgroup.inventoryies.sort(), angular.copy($scope.selectedGroup.selected.inventoryies).sort());
                if (!groupsDiff) {
                    if (!$scope.selectedGroup.selected.isNew) {
                        $scope.selectedGroup.selected.isToEdit = true;
                        $scope.cancelGroupEdit = true;
                    }
                }

            }
        });

        $scope.dismissGroup = function () {
            if ($scope.selectedGroup.selected.groupId === 0) {
                $scope.selectedGroup.selected = angular.copy($scope.oldGroup);
            }
           
            $scope.itemGroups.forEach(function (group, i) {
              
                if (group.groupId === $scope.selectedGroup.selected.groupId) {
                    var find = angular.copy($filter('filter')($scope.itemGroupsOrginal, { groupId: $scope.selectedGroup.selected.groupId }, true)[0]);
                    $scope.itemGroups[i] = angular.copy(find);
                    $scope.selectedGroup = angular.copy(find);
                    $scope.selectedGroup.selected = angular.copy(find);
                } 
            });
        };
      
        $scope.checkUnitName = function (name)
        {
            var findunit = angular.copy($filter('filter')($scope.units, { unitName: name }, true));
            if (findunit.length) {
                for (var i = 0; i < findunit.length; i++) {
                    if (findunit[i].unitName === $scope.selectedGroup.selected.unit.unitName && findunit[i].unitId !== $scope.selectedGroup.selected.unit.unitId) {
                        $scope.showUnitWarning = true;
                        return;
                    }
                }
                $scope.showUnitWarning = false;
            }

        };

        $scope.checkGroupName = function (name) {
            var findGroup = angular.copy($filter('filter')($scope.itemGroups, { groupName: name }, true));
            if (findGroup.length) {
                for (var i = 0; i < findGroup.length; i++) {
                    if (findGroup[i].groupName === $scope.selectedGroup.selected.groupName && findGroup[i].groupId !== $scope.selectedGroup.selected.groupId) {
                        $scope.showGroupWarning = true;
                        return;
                    } 
                }
                $scope.showGroupWarning = false;
            }

        };

        $scope.addGroupProperty = function () {
            if (!$scope.selectedGroup.selected.isToEdit && !$scope.selectedGroup.selected.isNew) {
                $scope.selectedGroup.selected.isToEdit = true;
                $scope.cancelGroupEdit = true;

            };

            if (!$scope.selectedGroup.selected.properties) {
                $scope.selectedGroup.selected.properties = angular.copy($scope.newObjects.itemsGroupDTO.properties);
                return;

            }
            $scope.selectedGroup.selected.properties.push(angular.copy($scope.newObjects.itemsGroupDTO.properties[0]));

        };
        $scope.removeGroupProperty = function (index) {
            if (!$scope.selectedGroup.selected.isNew) {
                $scope.selectedGroup.selected.isToEdit = true;
                $scope.cancelGroupEdit = true;
            }
            if (index > -1) {
                $scope.selectedGroup.selected.properties.splice(index, 1);
            }
        };

        $scope.save = function () {
            EasyStoreUserspersistenceService.action.saveGroupVM(angular.toJson($scope.selectedGroup.selected)).then(
                function (result) {
                    $filter('filter')($scope.itemGroupsOrginal, { groupId: result.data.groupId }, true)[0] = result.data ;
                    $filter('filter')($scope.itemGroups, { groupId: result.data.groupId }, true)[0]  = angular.copy(result.data );
                    $scope.selectedGroup = angular.copy(result.data);
                    $scope.selectedGroup.selected = angular.copy(result.data);
                },
                function (error) {
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });
        };
        var checkbrGroup = function () {
            if ($scope.branchGroub && $scope.branchGroub.id !== '0') {
                bId = parseInt(angular.copy(parseInt($scope.branchGroub.id)));
                $scope.loadItems();
            }
        }();

    });

}());
