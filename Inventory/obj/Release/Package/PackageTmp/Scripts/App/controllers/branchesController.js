﻿(function () {

    'use strict';
    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("branchesController", function ($scope, _, branchespersistenceService, Offline, $location ,  $routeParams) {

        var branchName = $routeParams.name;
        $scope.catName = $routeParams.name;
        console.log(branchName);
        $scope.showList = false;
        $scope.branches = [];
        $scope.branchesNames = [];
        $scope.branchpropertyValues = [];
        $scope.PropertyValues = [];
        var getData = function () {

            branchespersistenceService.action.getById(branchName).then(
                function (branches) {
                    $scope.branches = branches;
                   
                    
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (branches.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };
        var lazyGetData = _.debounce(getData, 2);

        Offline.on('confirmed-down', lazyGetData);
        Offline.on('confirmed-up', lazyGetData);

        lazyGetData();

        var getbranchNameData = function () {

            branchespersistenceService.action.getAll().then(
                function (branches) {
                    $scope.branchesNames = branches;

                    console.log(branches);
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (branches.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };

       
        var lazyGetbranchNameData = _.debounce(getbranchNameData, 2);

        Offline.on('confirmed-down', lazyGetbranchNameData);
        Offline.on('confirmed-up', lazyGetbranchNameData);

        lazyGetbranchNameData();
       

        function getValueByKey(key, data) {
            var i, len = data.length;

            for (i = 0; i < len; i++) {
                if (data[i] && data[i].hasOwnProperty(key)) {
                    return data[i][key];
                }
            }

            return -1;
        }

        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;   //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        // $scope.catogeries = branchRepository.getcatogery();

        $('#loading').hide();


        // Save 

        $scope.showSuccessMessage = false;
        $scope.showFillOutFormMessage = false;
        $scope.isOnline = true;
        $scope.addbranch = {};







        var hasbranchToSave = function () {



            console.log($scope.addbranch)

            var hasValue = function (value) {

                if (typeof value === 'string') {
                    return value.length > 0;
                }

                return value > 0;
            };

            var returnValue =
                       hasValue($scope.addbranch.name)
                    && hasValue($scope.addbranch.price)
                    && hasValue($scope.addbranch.catogery);

            return returnValue;
        };

        $scope.save = function () {
            if ($scope.addbranch.id === null || $scope.addbranch.id === undefined) {
                $scope.addbranch.id = $scope.addbranch.name;
            }

            var savebranch = hasbranchToSave();


            var branch = $scope.addbranch;


            //;

            branchespersistenceService.action.save(branch).then(
                function (result) {
                    $scope.showSuccessMessage = true;
                    $scope.showErrorMessage = false;
                    $scope.addbranch = {};
                },
                function (error) {
                    $scope.showSuccessMessage = false;
                    $scope.showErrorMessage = true;
                });



        };

        Offline.on('confirmed-down', function () {
            $scope.$apply(function () {
                $scope.isOnline = false;
            });
        });

        Offline.on('confirmed-up', function () {
            $scope.$apply(function () {
                $scope.isOnline = true;
            });
        });

        //Edit and update
        var branchToEdit = [];

        $scope.editingbranch = {};


        $scope.modify = function (branch) {
            for (var i = 0, length = $scope.branches.length; i < length; i++) {
                $scope.editingbranch[$scope.branches[i].id] = false;
            }
            branchToEdit = angular.copy(branch);
            $scope.addbranch = branch;
            $scope.editingbranch[branch.id] = true;

        };


        $scope.update = function (branch) {
            $scope.editingbranch[branch.id] = false;
        };

        $scope.cancel = function (branch) {

            branch.name = branchToEdit.name;
            branch.price = branchToEdit.price;
            branch.catogery = branchToEdit.catogery;

            $scope.editingbranch[branch.id] = false;

        };

        $scope.reset = function () {
            for (var i = 0, length = $scope.branches.length; i < length; i++) {
                $scope.editingbranch[$scope.branches[i].id] = false;
            }
            $scope.addbranch = {};
        };


        //delte
        var deltebranch = {};
        $scope.branchToDelte = function (branch) {

            deltebranch = branch;
        };


        $scope.delete = function () {


            branchespersistenceService.action.Delete(deltebranch.id).then(
                function (result) {
                    $scope.branches.splice($scope.branches.indexOf(deltebranch), 1);
                    deltebranch = {};
                },
                function (error) {
                    $scope.error = error;
                });
        };


        branchespersistenceService.getById(branchName).then(
            function (branches) {
                $scope.branches = branches;
            },
            function (error) {
                $scope.error = error;
            });

    });

}());
