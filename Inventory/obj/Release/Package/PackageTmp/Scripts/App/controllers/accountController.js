﻿'use strict';
var getTree;

(function () {


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("accountController", function ($scope, $timeout, _, AccountspersistenceService, $rootScope, settings, $filter,NgTableParams,
                                                           AccountCategoriespersistenceService, BalanceSheetspersistenceService,
                                                           BalanceSheetTypespersistenceService, branchespersistenceService,
                                                           companiespersistenceService, companiesremotePersistenceStrategy, Offline, $location,
                                                           branchesremotePersistenceStrategy, $sce, authService, globalService, $q, $log) {

        $scope.showList = false;
        $scope.AccountsNames = [];
        $scope.AccountpropertyValues = [];
        $scope.PropertyValues = [];
        $scope.Companies = [];
        $scope.Branches = [];
        $scope.BalanceSheets = [];
        $scope.BalanceSheetTypes = [];
        $scope.AccountCategories = [];
        $scope.Accounts = [];
        $scope.colorValue = [];
        $scope.tree = [];
        $scope.GetBranches = [];
        $scope.EditCompany = [];
        $scope.AddCompany = [];
        $scope.AddBranch = [];
        $scope.accountCatPropvalue = [];
        $scope.RoleCompanyBranch = {};
        $scope.setRoleCompanyBranch = {};
        $scope.people = [];
        $scope.invitePerson = {};
        $scope.setUserComanpy = {};
        $scope.type = {};
        $scope.nodetext = "";
        $scope.models = {};
        $scope.dataTypes = [];
        $scope.customAccountCatogeryId = 0;
        $scope.reveal = null;
        $scope.Accountlist = [];
        $scope.whichOrderType = '';
        $scope.financialLists = [];
        $scope.CustomAccountList = [];
        $scope.person = {'email' : ''};
        var bId;

        $scope.SetselectedType = {};
        $scope.selectedType = {};
        $scope.redo = [];
        $scope.BranchForTree = {};
        $scope.showCompanyproperty = false;
        $scope.showStatus = function () {
            var selected = $filter('filter')($scope.statuses, { value: $scope.user1.status });
            return ($scope.user1.status && selected.length) ? selected[0].text : 'Not set';
        };



        $scope.SetselectedType = function (p) {
            $scope.selectedType = p;

            dlg();
        };

        var dlg = function () {
            $("#addpropertyModel").modal("toggle");

        };

        $scope.updatePropertyValue = function (c) {

            var cjson = JSON.stringify({ AccountPropertyvalId: c.id, value: c.value });
            BalanceSheetspersistenceService.action.Updateaccountvalue(cjson).then(
                    function (result) {
                        $scope.showErrorMessage = false;
                        $scope.showSuccessMessage = true;
                        toastr.info('تم تحديث البيانات')
                        result.data.sort(function (a, b) {
                            // ASC  -> a.length - b.length
                            // DESC -> b.length - a.length
                            return b.values.length - a.values.length;
                        });
                        $scope.redo = angular.copy(result.data);
                        return true;

                    },
                    function (error) {
                        $scope.showSuccessMessage = false;
                        $scope.showErrorMessage = true;
                        toastr.error("حدث خطاء - تاكد من ان نوع البيانات المدخل صحيح وغير مكرر");
                        $scope.message = " حدث خطاء - تاكد من ان نوع البيانات المدخل صحيح وغير مكرر"

                        $scope.accountCatPropvalue = $scope.redo;
                        return false;
                    });
        };

        $scope.deletePropertyValue = function (c) {

            BalanceSheetspersistenceService.action.RemoveAccValue(c.id).then(
                function (result) {
                    $scope.showErrorMessage = false;
                    $scope.showSuccessMessage = true;

                    result.sort(function (a, b) {
                        // ASC  -> a.length - b.length
                        // DESC -> b.length - a.length
                        return b.values.length - a.values.length;
                    });
                    $scope.accountCatPropvalue = result

                    toastr.info('تم الحذف')

                    $scope.redo = angular.copy(result);
                    return true;

                },
                function (error) {
                    $scope.showSuccessMessage = false;
                    $scope.showErrorMessage = true;
                    toastr.error("حدث خطاء - تاكد من ان البيان المراد حذفة غير مرتبط ببيانات فرعية اخري");
                    $scope.message = " حدث خطاء"
                    $scope.accountCatPropvalue = $scope.redo;
                    return false;
                });
        };

        $scope.addnewPropertyValue = function () {

            var accid = document.getElementById("form_control_2").value;

            BalanceSheetspersistenceService.action.setaccountvalue(JSON.stringify({ AccountPropertyId: $scope.selectedType.id, value: accid, DataTypeName: $scope.selectedType.dataTypeName, AccountId: $scope.selectedType.accountId })).then(
                        function (result) {

                            result.data.sort(function (a, b) {

                                return b.values.length - a.values.length;
                            });
                            $scope.accountCatPropvalue = result.data;

                        },
                        function (error) {
                            $scope.showSuccessMessage = false;
                            $scope.showErrorMessage = true;
                            $scope.message = " حدث خطاء - تاكد من ان نوع البيانات المدخل صحيح وغير مكرر"

                            dlg();

                        });
        };

        $scope.$on('$viewContentLoaded', function () {
            // initialize core components
            App.initAjax();

            // set default layout mode
            $rootScope.settings.layout.pageContentWhite = true;
            $rootScope.settings.layout.pageBodySolid = false;
            $rootScope.settings.layout.pageSidebarClosed = true;
        });


        //start Of Company Branch Property
        $scope.companyBranchpropertyTypes = [];
        $scope.getCompanyBranchTypesInfo = function () {
            companiespersistenceService.action.getTypesInfo().then(
                function (result) {

                    $scope.companyBranchpropertyTypes = result.data;
                },
                function (error) {
                    $scope.showSuccessMessage = false;
                    $scope.showErrorMessage = true;

                });
        };

        $scope.getCompanyBranchTypesInfo();


        $scope.selectedcompanyBranchProperty = {};
        $scope.getcompanyProperty = function (data , isCompany) {
            companiespersistenceService.action.getCompanyInfo(JSON.stringify({ comapnyId: data.publicId, iscompany: isCompany })).then(
              function (result) {

                  $scope.selectedcompanyBranchProperty = result.data;
              },
              function (error) {
                  $scope.showSuccessMessage = false;
                  $scope.showErrorMessage = true;

              });
 
            console.log(JSON.stringify($scope.selectedcompanyBranchProperty));
            $scope.showCompanyproperty = true;

        };
        $scope.closeCompanyProperty = function () {
            $scope.showCompanyproperty = false;
        };

        $scope.addNewcompanyProperty = function () {
            var compropId =0;
            if($scope.selectedcompanyBranchProperty.iscompany){
                compropId = $scope.selectedcompanyBranchProperty.comapnyId;
            }
            if(!$scope.selectedcompanyBranchProperty.iscompany){
                compropId = $scope.selectedcompanyBranchProperty.branchId;
            }
            companiespersistenceService.action.addCompanyBranchProperty(JSON.stringify({ comapnyId: compropId, iscompany: $scope.selectedcompanyBranchProperty.iscompany })).then(
             function (result) {

                 $scope.selectedcompanyBranchProperty.properties.push(result.data);
             },
             function (error) {
                 $scope.showSuccessMessage = false;
                 $scope.showErrorMessage = true;

             });
        }

        $scope.copycompanyproperty = {};
        $scope.beforeupdateCompanyProperty = function (prop) {
            $scope.copycompanyproperty = angular.copy(prop);
        };

        $scope.updatecompanyProperty = function (property , index) {
            var compropId = 0;
            if ($scope.selectedcompanyBranchProperty.iscompany) {
                compropId = $scope.selectedcompanyBranchProperty.comapnyId;
            }
            if (!$scope.selectedcompanyBranchProperty.iscompany) {
                compropId = $scope.selectedcompanyBranchProperty.branchId;
            }
            companiespersistenceService.action.updateCompanyBranchProperty(JSON.stringify({
                comapnyId: compropId,
                iscompany: $scope.selectedcompanyBranchProperty.iscompany,
                propertiesId: property.propertiesId,
                propertiesName: property.propertiesName,
                typeName: property.typeName
            })).then(
             function (result) {
                 console.log(result);
             },
             function (error) {
                 $scope.selectedcompanyBranchProperty.properties[index] = $scope.copycompanyproperty;
                 toastr.error("تعذر تعديل البيانات في الوقت الحالي");

             });
        };
        $scope.delteCompanyprop = function (property, index) {
            var compropId = 0;
            if ($scope.selectedcompanyBranchProperty.iscompany) {
                compropId = $scope.selectedcompanyBranchProperty.comapnyId;
            }
            if (!$scope.selectedcompanyBranchProperty.iscompany) {
                compropId = $scope.selectedcompanyBranchProperty.branchId;
            }
            companiespersistenceService.action.delCompanyBranchProperty(JSON.stringify({
                comapnyId: compropId,
                iscompany: $scope.selectedcompanyBranchProperty.iscompany,
                propertiesId: property.propertiesId,
                propertiesName: property.propertiesName,
                typeName: property.typeName
            })).then(
             function (result) {
                 $scope.selectedcompanyBranchProperty.properties.splice(index, 1);
             },
             function (error) {
               
                 toastr.error("لا يمكن حذف البيانات في الوقت");

             });
        };

        $scope.addNewcompanyPropertyValue = function (property , index) {
            var compropId = 0;
            if ($scope.selectedcompanyBranchProperty.iscompany) {
                compropId = $scope.selectedcompanyBranchProperty.comapnyId;
            }
            if (!$scope.selectedcompanyBranchProperty.iscompany) {
                compropId = $scope.selectedcompanyBranchProperty.branchId;
            }
            companiespersistenceService.action.addCompanyBranchPropertyValue(JSON.stringify({ comapnyId: compropId, iscompany: $scope.selectedcompanyBranchProperty.iscompany, propertiesId: property.propertiesId, valueId: 0, value: '' })).then(
             function (result) {

                 property.propertyValue.push(result.data);
             },
             function (error) {
                 $scope.showSuccessMessage = false;
                 $scope.showErrorMessage = true;

             });
        }

        $scope.copycompanypropertyValue = {};
        $scope.beforeupdateCompanyPropertyValue = function (prop) {
            $scope.copycompanypropertyValue = angular.copy(prop);
        };

        $scope.updatecompanyPropertyValue = function (property, propertyValue) {
            var compropId = 0;
            if ($scope.selectedcompanyBranchProperty.iscompany) {
                compropId = $scope.selectedcompanyBranchProperty.comapnyId;
            }
            if (!$scope.selectedcompanyBranchProperty.iscompany) {
                compropId = $scope.selectedcompanyBranchProperty.branchId;
            }
            companiespersistenceService.action.updateCompanyBranchPropertyValue(JSON.stringify({ comapnyId: compropId, iscompany: $scope.selectedcompanyBranchProperty.iscompany, propertiesId: property.propertiesId, valueId: propertyValue.valueId, value: propertyValue.value })).then(

             function (result) {
                 console.log(result);
             },
             function (error) {
                 propertyValue = $scope.copycompanypropertyValue;
                 toastr.error("تعذر تعديل البيانات في الوقت الحالي");

             });
        };
        $scope.delteCompanypropValue = function (property , propertyValue, index) {
            var compropId = 0;
            if ($scope.selectedcompanyBranchProperty.iscompany) {
                compropId = $scope.selectedcompanyBranchProperty.comapnyId;
            }
            if (!$scope.selectedcompanyBranchProperty.iscompany) {
                compropId = $scope.selectedcompanyBranchProperty.branchId;
            }
            companiespersistenceService.action.delCompanyBranchPropertyValue(JSON.stringify({ comapnyId: compropId, iscompany: $scope.selectedcompanyBranchProperty.iscompany, propertiesId: property.propertiesId, valueId: propertyValue.valueId, value: propertyValue.value })).then(

             function (result) {
                 property.propertyValue.splice(index, 1);
             },
             function (error) {

                 toastr.error("لا يمكن حذف البيانات");

             });
        };
        //End of Company Branch peoperty


        //Strat Of Company And Beanches

        var company = function () {

            companiespersistenceService.action.getallCompany().then(
                 function (result) {
                     console.log("hi there" , result)
                     $scope.companies = result;
                 },
                 function (error) {
                     $scope.showsuccessmessage = false;
                     $scope.showerrormessage = true;

                 });

           
        };

        company();
     
     
        $scope.addcompany = function () {

            companiespersistenceService.action.save().then(
                   function (result) {
                       $scope.showsuccessmessage = true;
                       $scope.showerrormessage = false;
                       $scope.companies.push(result.data);
                   },
                   function (error) {
                       $scope.showsuccessmessage = false;
                       $scope.showerrormessage = true;

                   });
        };

        $scope.copycompany = '';
        $scope.beforeupdatecompany = function (editcompany) {
            $scope.copycompany = angular.copy(editcompany.name);
            console.log("before update Company")
        };

        $scope.updatecompany = function (editcompany) {
            console.log('S company updated');
            companiespersistenceService.action.update(JSON.stringify(editcompany)).then(
                     function (result) {
                         console.log('company updated', result);

                     },
                     function (error) {
                         editcompany.name = $scope.copycompany;
                         toastr.error("لا يمكن تحديث بيانات الشركة في الوقت الحالي");

                     })

        };
        $scope.addbranch = function (editcompany) {
            companiespersistenceService.action.saveBranch(JSON.stringify(editcompany)).then(
          function (result) {
              console.log('company updated', result);
              editcompany.branches.push(result.data);
          },
          function (error) {
          
              toastr.error("لا يمكن اضافة فرع في الوقت الحالي");

          })
        };

        $scope.updatebranch = function (editcompany) {
            companiespersistenceService.action.updateBranch(JSON.stringify(editcompany)).then(
                   function (result) {
                       console.log('company updated', result);

                   },
                   function (error) {
                       editcompany.name = $scope.copycompany;
                       toastr.error("لا يمكن تحديث بيانات الفرع في الوقت الحالي");

                   })
        };
        //End Of Company And Beanches

        //strat of chart of accounts

        $scope.branchesGroub = [];

        $scope.branchGroub = {
            id: 0,
            name: 'حدد الشركة',
            fullName: 'حدد الشركة- الفرع'
        };

        $scope.loadCompanies = function () {
            AccountspersistenceService.action.Getbranchgroup(bId).then(
                function (Companies) {
                    $scope.branchesGroub = Companies;
                    $scope.branchGroub = Companies[0];
                    setAccountListtree();
                },
                function (error) {
                    $scope.error = error;
                });
        };
        $scope.bstree = [];
        $scope.addmainaccount = function () {
            var data = { id: bId, accountLevel: '', accountName: '', accountType: '', parentId: bId, titleForAdd: '', userRoles: null };
            data.accountLevel = "";
            $scope.saveBs(data, 0, 0);

        };

        $scope.updateBs = function (data, index1, index2) {



            if ($scope.selectedBranchTolink != null) {
                //$scope.selectedBranchTolink = $scope.branchesGroub.filter(function (el) { return el.id == $scope.selectedBranchTolink.id })[0];
                //data.linkedAccountId = $scope.selectedBranchTolink.isCompany == false ? parseInt($scope.selectedBranchTolink.id) : 0;
                //data.linkedAccountName = $scope.selectedBranchTolink.realName;
                //data.isCompany = $scope.selectedBranchTolink.isCompany;
            }

            BalanceSheetspersistenceService.action.update(JSON.stringify({ old: $scope.beforeupdateBs, edited: data })).then(
             function (Companies) {
                 return true;
             },
             function (error) {
                 $scope.error = error;
                 if (index1 == -1 && index2 == -1) {
                     $scope.selectedAccountfroProperty.accountsDataTypesId = $scope.beforeupdateBs.accountsDataTypesId;

                 }
                 else {
                     $scope.bstree[index1][index2].accountName = $scope.beforeupdateBs.accountName;
                     $scope.bstree[index1][index2].accountType = $scope.beforeupdateBs.accountType;
                 }


                 return false;
             });


        };
        $scope.saveBs = function (data, index1, index2) {
            $scope.reveal = angular.copy(data);
            data.code = bId;
            BalanceSheetspersistenceService.action.save(JSON.stringify(data)).then(
             function (Companies) {
                 $scope.bstree = Companies.data;

                 return true;
             },
             function (error) {
                 $scope.error = error;

                 return false;
             });


        };
        $scope.beforeupdateBs = {};
        $scope.fnbeforeupdateBs = function (oldData) {
            $scope.beforeupdateBs = angular.copy(oldData);

        };
        $scope.delnode = function (data) {
            $scope.nodetext = data.accountName;
            $scope.nodetodel = data;
            $('#confirmdel-modal').modal('show');
        };

        $scope.deleteNodeF = function (confirm) {
            if (confirm == true) {
                BalanceSheetspersistenceService.action.delnode(JSON.stringify($scope.nodetodel)).then(
                         function (result) {

                             $("#tree_table").treetable("removeNode", $scope.nodetodel.id);

                             toastr.info("تم الحذف");

                         },
                         function (error) {


                             toastr.error("لم نتمكن من الحذف , تاكد من ان البيانات غير مستخدمة");



                         });
            }
            if (confirm == false) {

                $scope.nodetext = data.node.text;
                $('#jstree_account_div').jstree(true).refresh();
            }
        };

        $scope.balanceSheetTree = function getAccount(id) {
            console.log('branchGroub.id hhhhhhh', $scope.branchGroub.id)
            BalanceSheetspersistenceService.action.getTreeById(JSON.stringify({ id: parseInt($scope.branchGroub.id), iscompany: $scope.branchGroub.isCompany })).then(
                function (Companies) {
                    $scope.balanceSheet = JSON.stringify(Companies.data);
                    $scope.bstree = Companies.data;
                },
                function (error) {
                    $scope.error = error;
                });
        };
        
        $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {


            $("#tree_table").treetable(
                {
                    expandable: true,
                    onInitialized: function () {

                        if ($scope.reveal != null) {
                            $scope.checkdetails = false;
                            $("#tree_table").treetable("collapseAll");

                            if ($scope.reveal.accountLevel == "رئيسي") {

                                $("#tree_table").treetable("expandNode", $scope.reveal.id);
                                $scope.reveal == null;

                            };
                            if ($scope.reveal.accountLevel == "عام") {

                                $("#tree_table").treetable("expandNode", $scope.reveal.parentId);
                                $("#tree_table").treetable("expandNode", $scope.reveal.id);
                                var selectrow = $('[data-tt-id="' + $scope.reveal.id + '"]')
                                selectrow.not(selectrow).removeClass("selected");
                                $(selectrow).toggleClass("selected");

                                $scope.reveal == null;


                            };
                            if ($scope.reveal.accountLevel == "مساعد") {
                                var node = $("#tree_table").treetable("node", $scope.reveal.parentId);

                                $("#tree_table").treetable("expandNode", node.parentId);
                                $("#tree_table").treetable("expandNode", $scope.reveal.parentId);
                                $("#tree_table").treetable("expandNode", $scope.reveal.id);
                                $scope.reveal == null;


                            };
                            if ($scope.reveal.accountLevel == "فرعي") {

                                var node2 = $("#tree_table").treetable("node", $scope.reveal.parentId);
                                var node = $("#tree_table").treetable("node", node2.parentId);

                                $("#tree_table").treetable("expandNode", node.parentId);
                                $("#tree_table").treetable("expandNode", node2.parentId)
                                $("#tree_table").treetable("expandNode", $scope.reveal.parentId);
                                $("#tree_table").treetable("expandNode", $scope.reveal.id);
                                $scope.reveal == null;


                            };


                        }
                    }
                }
                , true);



            $("#tree_table td div").removeClass('checker');

            // Highlight selected row
            $("#tree_table tbody").on("mousedown", "tr", function () {
                $(".selected").not(this).removeClass("selected");
                $(this).toggleClass("selected");
            });




        });
        //end of chart of accounts

        $scope.removeProperty = function (id) {

            BalanceSheetspersistenceService.action.RemoveAccProperty(id).then(
     function (result) {


         $scope.accountCatProp = result;
     },
     function (error) {
         $scope.showSuccessMessage = false;
         $scope.showErrorMessage = true;
     });
        };

        $scope.removeBranch = function () {
            var lastItem = $scope.EditCompany.branches.length - 1;
            if (lastItem > 0)
                $scope.EditCompany.branches.splice(lastItem);
        };

        $scope.handleOptions = {
            emptySearchResultText: 'لا يوجد اي مستخدم يمكنك ارسال' + '<a>دعوة</a>' + 'وسبتم تفعيلة لاحقا',
        };

        $scope.changedValue = function (tid) {
            //$scope.selected =  $scope.dataTypes.accountsDataTypes[tid - 1];
            $scope.selectedAccountfroProperty.accountsDataTypesId = tid.id;
            BalanceSheetspersistenceService.action.update(JSON.stringify({ old: $scope.beforeupdateBs, edited: $scope.selectedAccountfroProperty })).then(
                         function (Companies) {
                             toastr.info('تم التحديث')
                             return true;
                         },
                         function (error) {
                             $scope.error = error;
                             $scope.selectedAccountfroProperty.accountsDataTypesId = $scope.beforeupdateBs.accountsDataTypesId;


                             return false;
                         });
          
            console.log(tid);
        };


        $scope.$watch('selected', function (newVal, oldVal) {
            if (newVal !== oldVal) {
            }
        });
        
      
        $scope.saveAccountProp = function () {
            BalanceSheetspersistenceService.action.saveAccountProp(JSON.stringify(
                   {
                       id: -1,
                       name: $('#propertyName').val(),
                       DataTypeName: $scope.selected.typeName,
                       DataTypeId: $scope.selected.id,
                       AccountCatogeryName: $scope.dataTypes.catogeryName,
                       AccountCatogeryId: $scope.dataTypes.id
                   }

                )).then(
          function (result) {


              $scope.accountCatProp = result.data;
          },
          function (error) {
              $scope.showSuccessMessage = false;
              $scope.showErrorMessage = true;
          });
        };

    


        $scope.checkdetails = false;

        $scope.CompaniesTree = [];
     
        
        $scope.selectedAccountfroProperty = {};

        $scope.accountsproperty = function (ac , keyacc , keych) {
            if (ac.accountLevel == 'فرعي') {
                BalanceSheetspersistenceService.action.getAccPropertiesValue(ac.id).then(
                     function (respone) {

                         $scope.accountCatPropvalue = respone;
                         $scope.redo = angular.copy($scope.accountCatPropvalue);
                         $('#propertyModel').modal('show');

                     },
                     function (error) { });
            }
            else if (ac.accountLevel == 'مساعد') {
                console.log('$scope.selectedAccountfroProperty', ac);
                  
                $scope.selectedAccountfroProperty = $scope.bstree[keyacc][keych];
                $scope.fnbeforeupdateBs($scope.selectedAccountfroProperty);
                console.log('$scope.selectedAccountfroProperty', $scope.selectedAccountfroProperty);
                BalanceSheetspersistenceService.action.getAccountsDataTypes(JSON.stringify({ accountId: parseInt(ac.id), BalanceSheetTypeId: ac.parentId })).then(
                           function (respone) {
                               $scope.dataTypes = respone.data[0];
                               //$scope.selected = $scope.dataTypes.accountsDataTypes[0];
                               $scope.accountCatProp = function () {
                                   BalanceSheetspersistenceService.action.getaccountCategoryProperties(respone.data[0].id).then(
                                      function (dresult) {

                                          $scope.accountCatProp = dresult;
                                          $('#accountmodel').modal('show');
                                      },
                                      function (error) {

                                          $scope.showSuccessMessage = false;
                                          $scope.showErrorMessage = true;
                                      });
                               }();


                               $('#accountmodel').modal('show');

                           });
            }
        };
        $scope.iscompany = true;


        $scope.refreshNode = function () {
            $('#jstree_account_div').jstree(true).refresh();

        }

        $scope.nodetodel = {};
        $scope.ischeckDetials = function (check) {
            if (check) {
                $("#tree_table").treetable("expandAll");

            }
            else {
                $("#tree_table").treetable("collapseAll");

            }
        };

       
        $scope.toggoletablerows = function (keyf , open) {
           
            //var toggleKey = angular.copy(key);
            var showHideList = $scope.financialLists[keyf];
            console.log("List", showHideList.listes);
            if (open) {
                var key = $scope.financialLists[keyf].listes.length
                while (key > 0) {
                    key--;
                    $scope.financialLists[keyf].listes[key].show = true;
                    $("i#td" + keyf + '' + key).removeClass("arrowup2t");
                    $("i#td" + keyf + '' + key).removeClass("arrow2t");
                    $("i#td" + keyf + '' + key).addClass("arrowup2t");

                }
            }
            else {
                var key = $scope.financialLists[keyf].listes.length
                while (key >0) {
                    key--;
                    if ($scope.financialLists[keyf].listes[key].level != 2) {
                        $scope.financialLists[keyf].listes[key].show = false;
                        $("i#td" + keyf + '' + key).removeClass("arrowup2t");
                        $("i#td" + keyf + '' + key).removeClass("arrow2t");
                        $("i#td" + keyf + '' + key).addClass("arrow2t");
                    }
                    else {
                        $("i#td" + keyf + '' + key).removeClass("arrowup2t");
                        $("i#td" + keyf + '' + key).removeClass("arrow2t");
                        $("i#td" + keyf + '' + key).addClass("arrow2t");
                    }

                }

            }
           
            $(".level4t > i").removeClass("arrowup2t");
            $(".level4t > i").removeClass("arrow2t");
          
        };

        $scope.toggoletablerow = function (row ,keyf , key) {
            var openClose = false;
            var toggleKey = angular.copy(key);
            if (key != 0) {
                openClose = !$scope.financialLists[keyf].listes[key - 1].show;
            }
            if (openClose) {
                while (key > 0) {
                    key--;
                    if ($scope.financialLists[keyf].listes[key].parentId == row.id) {
                        $scope.financialLists[keyf].listes[key].show = true;
                      
                        $("i#td" + keyf + '' + key).addClass("arrow2t");

                    }
                }
                $("i#td" + keyf + '' + toggleKey).removeClass("arrowup2t");
                $("i#td" + keyf + '' + toggleKey).removeClass("arrow2t");

                $("i#td" + keyf + '' + toggleKey).addClass("arrowup2t");
            }
            else {
                while (key > 0) {
                    key--;
                    if ($scope.financialLists[keyf].listes[key].level > row.level) {
                        $("i#td" + keyf + '' + key).removeClass("arrowup2t");
                        $("i#td" + keyf + '' + key).removeClass("arrow2t");
                        $scope.financialLists[keyf].listes[key].show = false;
                      
                    }
                    else {
                   

                        key = 0;
                    }
                   
                }
               
                $("i#td" + keyf + '' + toggleKey).removeClass("arrow2t");
                $("i#td" + keyf + '' + toggleKey).addClass("arrow2t");

            };
            
            $(".level4t > i").removeClass("arrowup2t");
            $(".level4t > i").removeClass("arrow2t");
        };
        $scope.deleteNode = function (data, confirm) {
            if (confirm == true) {
                BalanceSheetspersistenceService.action.delnode(JSON.stringify({ text: data.node.text, old: "", parents: data.node.parents })).then(
                         function (result) {

                             $scope.nodetext = data.node.text;
                             toastr.info("تم الحذف");

                         },
                         function (error) {

                             toastr.error("لم نتمكن من الحذف , تاكد من ان البيانات غير مستخدمة");
                             $scope.balanceSheetTree($scope.BranchForTree);
                             $scope.nodetext = data.node.text;
                             $('#jstree_account_div').jstree(true).refresh();

                         });
            }
            if (confirm == false) {
                $scope.balanceSheetTree($scope.BranchForTree);
                $scope.nodetext = data.node.text;
                $('#jstree_account_div').jstree(true).refresh();
            }
        };

       

       
       



        $scope.createAccountTree = function () {

            BalanceSheetspersistenceService.action.saveCustomBalanceSheet(JSON.stringify({ text: 'قائمة جديدة', old: 'قائمة جديدة', parents: [bId] })).then(
                     function (result) {
                       
                         $scope.setAccountTreeDate(result.data);
                     },
                     function (error) {
                         toastr.error("حدث خطاء - اثناء انشاء قائمة جديدة");
                         $('#jstree_accountsList_div').jstree(true).refresh();

                     });

        };

        $scope.setAccountTreeDate = function (id) {
            BalanceSheetspersistenceService.action.getCustomTreeById( parseInt(id)).then(
                     function (result) {

                         $('#jstree_accountsList_div').jstree(true).settings.core.data = result;


                         $('#jstree_accountsList_div').jstree(true).refresh();
                     },
                     function (error) {
                         toastr.error("حدث خطاء - تاكد من اتصالك وان نوع البيانات المدخل صحيح وغير مكرر");
                         $('jstree_accountsList_div').jstree(true).refresh();

                     });
        };

     
        //accounttreee list
      var setAccountListtree = function () {


            $(function () {
                var to = false;
                $('#accountSearch').keyup(function () {
                    if (to) { clearTimeout(to); }
                    to = setTimeout(function () {
                        var v = $('#accountSearch').val();
                        $('#jstree_accountsList_div').jstree(true).search(v);
                    }, 250);
                });


                $('#jstree_accountsList_div').on('rename_node.jstree', function (e, data) {
                  
                    if (data.node.parents.length == 1) {
                        var parnts = bId;
                        if (parnts != 0) {
                            BalanceSheetspersistenceService.action.saveCustomBalanceSheet(JSON.stringify({ text: data.node.text, old: data.old, parents: [parnts] })).then(
                                      function (result) {
                                          $scope.setAccountTreeDate(result.data);
                                          toastr.info("تم تحديث البيانات بنجاح");
                                      },
                                      function (error) {
                                          toastr.error("حدث خطاء - اثناء انشاء قائمة جديدة");
                                          $('#jstree_accountsList_div').jstree(true).refresh();
                                      });
                        }
                 
                    }
                    else if (data.node.parents.length != 1 && data.node.parents.length != 5) {
                        BalanceSheetspersistenceService.action.saveCustomBalanceSheet(JSON.stringify({ text: data.node.text, old: data.old, parents: data.node.parents })).then(
                           function (result) {

                               //data.node.id = result.data;
                               data.instance.set_id(data.node, result.data);
                               //data.instance.edit(id);
                               //$('#jstree_account_div').jstree(true).refresh();

                               toastr.info("تم تحديث البيانات بنجاح");
                           },
                           function (error) {
                               toastr.error("حدث خطاء - تاكد من اتصالك وان نوع البيانات المدخل صحيح وغير مكرر");
                              
                               $('#jstree_accountsList_div').jstree(true).refresh();

                           });
                    }
                    else if (data.node.parents.length == 5) {

                        var catogeryId = data.node.parents[0];
                        $scope.customAccountCatogeryId = parseInt( catogeryId);
                        BalanceSheetspersistenceService.action.getCustomesAccounts(JSON.stringify({ branchId: parseInt(data.node.parents[0]), accountCatogeryId: catogeryId })).then(
                            function (result) {


                                if (result.data.length != 0) {

                                  

                                     
                                  

                                    var A = result.data;
                                    var B = angular.copy($scope.Accountlist);

                                    var myArray = angular.copy($scope.Accountlist);
                                    var toRemove = A;

                                    for (var i = myArray.length - 1; i >= 0; i--) {
                                        for (var j = 0; j < toRemove.length; j++) {
                                            if (myArray[i] && (myArray[i].accountId === toRemove[j].accountId)) {
                                                myArray.splice(i, 1);
                                            }
                                        }
                                    }


                                    $scope.models.lists.accounts.Accountlist = myArray;
                                    $scope.models.lists.customAccounts.Accountlist = result.data;
                                }
                                else {
                                    $scope.models.lists.accounts.Accountlist = angular.copy($scope.Accountlist);
                                    $scope.models.lists.customAccounts.Accountlist = [];

                                }
                          
                                
                                $('#custom-accounts-modal').modal('show');

                            },
                            function (error) {
                                toastr.error("حدث خطاء - تاكد من اتصالك وان نوع البيانات المدخل صحيح وغير مكرر");
                                //$scope.balanceSheetTree($scope.BranchForTree);
                                $('#jstree_accountsList_div').jstree(true).refresh();

                            });
                    }
                  
                    
                }).on('delete_node.jstree', function (e, data) {
                    $scope.nodetodel = data;
                    if (data.node.parent != "#") {
                        $('#confirmdel-modal-f').modal('show');
                        $scope.nodetext = data.node.text;

                    }
                    else if (data.node.parent == "#") {
                        toastr.error("لا يمكن حذف القائمة من هنا");
                       
                        $scope.nodetext = "";
                        $('#jstree_accountsList_div').jstree(true).refresh();
                    }



                }).on('select_node.jstree', function (e, data) {


                }).jstree({
                    "core": {
                        "animation": 0,
                        "check_callback": true,
                        "themes": { "stripes": true, "name": "default" },
                        'data': []
                    },
                    "types": {
                        "#": { "max_children": 1, "max_depth": 5, "valid_children": ["root"] },
                        "root": { "icon": "./assets/images/tree_icon.png", "valid_children": ["default"] },
                        "default": { "valid_children": ["default", "file"] },
                        "file": { "icon": "glyphicon glyphicon-file", "valid_children": [] }
                    },
                    "contextmenu": {
                        'items': function (o, cb) { // Could be an object directly
                            return {
                                "create": {
                                    "separator_before": false,
                                    "separator_after": true,
                                    "_disabled": false, //(this.check("create_node", data.reference, {}, "last")),
                                    "label": "اضافة",
                                    "action": function (data) {
                                        

                                        var inst = $.jstree.reference(data.reference),
                                            obj = inst.get_node(data.reference);
                                        var nnode = {};
                                        inst.create_node(obj, {}, "last", function (new_node) {
                                            nnode = new_node; 
                                            setTimeout(function () { inst.edit(new_node); }, 0);
                                        });

                                    }
                                },
                                "rename": {
                                    "separator_before": false,
                                    "separator_after": false,
                                    "_disabled": false, //(this.check("rename_node", data.reference, this.get_parent(data.reference), "")),
                                    "label": "تسمية",
                                    /*!
                                    "shortcut"			: 113,
                                    "shortcut_label"	: 'F2',
                                    "icon"				: "glyphicon glyphicon-leaf",
                                    */
                                    "action": function (data) {

                                        var inst = $.jstree.reference(data.reference),
                                            obj = inst.get_node(data.reference);
                                        inst.edit(obj);

                                    }
                                },
                                "remove": {
                                    "separator_before": false,
                                    "icon": false,
                                    "separator_after": false,
                                    "_disabled": false, //(this.check("delete_node", data.reference, this.get_parent(data.reference), "")),
                                    "label": "حذف",
                                    "action": function (data) {
                                        var inst = $.jstree.reference(data.reference),
                                            obj = inst.get_node(data.reference);
                                        if (inst.is_selected(obj)) {
                                            inst.delete_node(inst.get_selected());
                                        }
                                        else {
                                            inst.delete_node(obj);
                                        }
                                    }
                                },
                                "ccp": {
                                    "separator_before": true,
                                    "icon": false,
                                    "separator_after": false,
                                    "label": "تعديل",
                                    "action": false,
                                    "submenu": {
                                        "cut": {
                                            "separator_before": false,
                                            "separator_after": false,
                                            "label": "قص",
                                            "action": function (data) {
                                                var inst = $.jstree.reference(data.reference),
                                                    obj = inst.get_node(data.reference);
                                                if (inst.is_selected(obj)) {
                                                    inst.cut(inst.get_top_selected());
                                                }
                                                else {
                                                    inst.cut(obj);
                                                }
                                            }
                                        },
                                        "copy": {
                                            "separator_before": false,
                                            "icon": false,
                                            "separator_after": false,
                                            "label": "تسخ",
                                            "action": function (data) {
                                                var inst = $.jstree.reference(data.reference),
                                                    obj = inst.get_node(data.reference);
                                                if (inst.is_selected(obj)) {
                                                    inst.copy(inst.get_top_selected());
                                                }
                                                else {
                                                    inst.copy(obj);
                                                }
                                            }
                                        },
                                        "paste": {
                                            "separator_before": false,
                                            "icon": false,
                                            "_disabled": function (data) {
                                                return !$.jstree.reference(data.reference).can_paste();
                                            },
                                            "separator_after": false,
                                            "label": "لصق",
                                            "action": function (data) {
                                                var inst = $.jstree.reference(data.reference),
                                                    obj = inst.get_node(data.reference);
                                                inst.paste(obj);
                                            }
                                        }
                                    }
                                },
                                "users": {
                                    "separator_before": true,
                                    "icon": false,
                                    "separator_after": false,
                                    "label": "مستخدمين",
                                    "action": function (data) {
                                    }
                                }

                            };
                        }
                    },
                    "plugins": ["contextmenu", "dnd", "search", "state", "types", "wholerow", 'unique']
                });

            });

           // $('#jstree_accountsList_div').jstree(true).refresh();


            //$("#jstree_accountsList_div").jstree("create_node", null, null, "last", function (node) {
            //    createRootNode.call(this);
            //    this.edit(node);
            //});

        };
        //End Of Account tree

        //start dndlists




       $scope.branchtoLink = [];

  
       $scope.accountTypes = [{ id: 1, accountType: 'مدين' }, { id: 2, accountType: 'دائن' }];
       $scope.orderTypes = [{ id: 1, orderType: 'اضافة' }, { id: 2, orderType: 'صرف' }];

       $scope.beforeupdateflistName = {};
       $scope.fnbeforeupdateflistName = function (oldData) {
           $scope.beforeupdateflistName = angular.copy(oldData);
           console.log('beforeupdateflistName', oldData);

       }

       $scope.updateflistName = function (data, keyf, key) {
           console.log('updateflistName', data);
           data.iscompany = $scope.branchGroub.iscompany;
           BalanceSheetspersistenceService.action.updateCustomBS(JSON.stringify(data)).then(
           function (result) {

               toastr.info("تم التحديث");

           },
           function (error) {
               console.log(error);
               $scope.financialLists[keyf].listes[key].name = $scope.beforeupdateflistName.name;
               toastr.error("حدث خطاء - التخديث");
           });
       };

       $scope.beforeupdatelistName = {};
       $scope.fnbeforeupdatelistName = function (oldData) {
           $scope.beforeupdatelistName = angular.copy(oldData);
           console.log('beforeupdatelistName', oldData);

       }

       $scope.updatelistName = function (data, key) {
           console.log('updatelistName', data);
         

           BalanceSheetspersistenceService.action.updateCustomBS(JSON.stringify({ id: data.listId, name: data.listName, level: 1, parentId: data.listId, isCompany: $scope.branchGroub.iscompany })).then(
           function (result) {

               toastr.info("تم التحديث");

           },
           function (error) {
               console.log(error);
               $scope.financialLists[keyf].listName = $scope.beforeupdatelistName.listName;
               toastr.error("حدث خطاء - الاضافة");
           });

       };

       $scope.addSubflistaccount = function (data, keyf, key) {
           console.log('addSubflistaccount', data);
           data.isCompany = $scope.branchGroub.iscompany;

           BalanceSheetspersistenceService.action.saveCustomBalanceSheet(JSON.stringify(data)).then(
                 function (result) {
                     
                    
                     $scope.financialLists[keyf].listes.splice(key, 0, result.data);
                     $scope.financialLists[keyf].listes[key].show = $scope.financialLists[keyf].listes.some(function isBiggerThan10(element, index, array) {
                         return element.show == true && element.parentId == data.id;
                     });
                     console.log("resualt new cusom account", result)
                     toastr.info("تم الاضافة");

                 },
                 function (error) {
                     toastr.error("حدث خطاء - الاضافة");
                    

                 });

       };
       $scope.addflistlevel1account = function (data , keyf) {
           console.log('addflistlevel1account', data);
           BalanceSheetspersistenceService.action.saveCustomBalanceSheet(JSON.stringify({ id: data.listId, level: 1, parentId: data.listId, isCompany: $scope.branchGroub.iscompany })).then(
              function (result) {


                  $scope.financialLists[keyf].listes.push(result.data);
                     
                  console.log("resualt new cusom account", result)
                  toastr.info("تم الاضافة");

              },
              function (error) {
                  toastr.error("حدث خطاء - الاضافة");


              });

       };

       $scope.addnewflist = function () {
           console.log('click add new list');
           BalanceSheetspersistenceService.action.saveCustomBalanceSheet(JSON.stringify({ id: bId, level: 0, childern: bId, parentId: bId, isCompany :$scope.branchGroub.iscompany })).then(
          function (result) {

              $scope.financialLists.push(result.data);
              $scope.setSelectedList(result.data);
              console.log("resualt new cusom account", result)
              toastr.info("تم الاضافة");

          },
          function (error) {
              toastr.error("حدث خطاء - الاضافة");


          });
       };

       $scope.linkAccounts = [];
       $scope.dataTolink = {};
       $scope.keyTolink = 0;
       $scope.linkflistaccount = function (data , keyf) {
           $scope.dataTolink = data;
           $scope.keyTolink = keyf;
           console.log('linkflistaccount', data);
           if (data.level == 2) {
               $scope.linkAccounts = [];
               for (var l = 0 ; l <= $scope.bstree.length - 1; l++) {
                   var listtoLink = $filter('filter')($scope.bstree[l], { accountLevel: "رئيسي" });
                   $scope.linkAccounts.push(listtoLink[0]);
                   
               };
               console.log('List To Lonk ', $scope.linkAccounts);
               $("#link-Modal").modal('show');
               
           }
           if (data.level == 3) {
               $scope.linkAccounts = [];
               for (var l = 0 ; l <= $scope.bstree.length - 1; l++) {
                   var listtoLink = $filter('filter')($scope.bstree[l], { accountLevel: "عام" });
                   $scope.linkAccounts.push(listtoLink[0]);

               };
               console.log('List To Lonk ', $scope.linkAccounts);
               $("#link-Modal").modal('show');

           }
           if (data.level == 4) {
               $scope.linkAccounts = [];
               for (var l = 0 ; l <= $scope.bstree.length - 1; l++) {
                   var listtoLink = $filter('filter')($scope.bstree[l], { accountLevel: "مساعد" });
                   $scope.linkAccounts.push(listtoLink[0]);

               };
               console.log('List To Lonk ', $scope.linkAccounts);
               $("#link-Modal").modal('show');

           }

       };
       $scope.acctoLink = { accountId: "", accountName: "", accountType : "" };
       $scope.linkAcccountClicked = function (data) {
         
           $scope.acctoLink.accountId = data.id;
           $scope.acctoLink.accountName = data.accountName;
           if (data.accountLevel == "رئيسي") {
               $scope.acctoLink.accountType = data.accountType;
           };
         
       };
       $scope.linkAcccountConfirm = function () {
           var data = $scope.dataTolink;
           data.childern = bId;

           data.isCompany = $scope.branchGroub.isCompany;
           data.linkedAccountId = parseInt($scope.acctoLink.accountId);
           data.linkedAccountName = $scope.acctoLink.accountName;
           data.isLinked = true;
           data.accountType = $scope.acctoLink.accountType;
           console.log('linked acc', $scope.acctoLink)
           $scope.acctoLink = { accountId: "", accountName: "", accountType: "" };
        

           BalanceSheetspersistenceService.action.updateCustomBS(JSON.stringify(data)).then(
           function (result) {
               $scope.financialLists[$scope.keyTolink].listes= result.data;
               toastr.info("تم التحديث");

           },
           function (error) {
               console.log(error);
               //$scope.financialLists[keyf].listes[key].name = $scope.beforeupdateflistName.name;
               toastr.error("حدث خطاء - التحديث");
           });
       };

       $scope.confirmkey = 0;
       $scope.delflistaccount = function (data, keyf, key) {
           $scope.nodetodel = data;
           $scope.confirmkey = keyf;
           $scope.nodetext = data.name;
           $("#confirmdel-modal-custom-f").modal('show');
           console.log('delflist', data);

       };

       $scope.delflist = function (rdata , keyf) {
           var data = { id: rdata.listId, level: 1, childern: bId};
           $scope.nodetodel = data;
           $scope.confirmkey = keyf;
           $scope.nodetext = rdata.listName;
           $("#confirmdel-modal-custom-f").modal('show');
           console.log('delflist', data);

       };

       $scope.confirmcustom = function (data) {
           
           data.childern = bId;

           console.log('delflistaccount', data);
           BalanceSheetspersistenceService.action.delnodef(JSON.stringify(data)).then(
         function (resualt) {
             console.log('delted Done', resualt);
             if (data.level != 1) {
                 $scope.financialLists[$scope.confirmkey].listes = resualt.data;

             }
             else if(data.level == 1) {
                 $scope.financialLists.splice($scope.confirmkey, 1);
             };

             toastr.info("تم الحذف");

         },
         function (error) {

             toastr.error("حدث خطاء اثناء الحذف");
             return false;
         });
       }
      
     
       $scope.selectedBranchTolink = {};


       $scope.configurelistAccount = function (data , keyf , key) {
           var catogeryId = data.id;
           $scope.keys.keyf = keyf;
           $scope.keys.key = key;
           $scope.keys.data = data;
           console.log('Keys', $scope.keys);
           $scope.customAccountCatogeryId = parseInt(catogeryId);
           BalanceSheetspersistenceService.action.getCustomesAccounts(JSON.stringify({ branchId: bId, accountCatogeryId: catogeryId })).then(
               function (result) {


                   if (result.data.length != 0) {

                       var A = result.data;
                       var B = angular.copy($scope.Accountlist);

                       var myArray = angular.copy($scope.Accountlist);
                       var toRemove = A;

                       for (var i = myArray.length - 1; i >= 0; i--) {
                           for (var j = 0; j < toRemove.length; j++) {
                               if (myArray[i] && (myArray[i].accountId === toRemove[j].accountId)) {
                                   myArray.splice(i, 1);
                               }
                           }
                       }


                       $scope.models.lists.accounts.Accountlist = myArray;
                       $scope.models.lists.customAccounts.Accountlist = result.data;
                   }
                   else {
                       $scope.models.lists.accounts.Accountlist = angular.copy($scope.Accountlist);
                       $scope.models.lists.customAccounts.Accountlist = [];

                   }


                   $('#custom-accounts-modal').modal('show');

               },
               function (error) {
                   toastr.error("حدث خطاء - تاكد من اتصالك وان نوع البيانات المدخل صحيح وغير مكرر");
                 

               });
       };



       $scope.loadCompanies = function () {
           AccountspersistenceService.action.Getbranchgroup(1).then(
               function (Companies) {
                   $scope.branchesGroub = Companies;
                   setAccountListtree();
               },
               function (error) {
                   $scope.error = error;
               });
       };

      // $scope.loadCompanies();
       $scope.branchidforlist = 0;
       $scope.$watch('branchGroub.id', function (newVal, oldVal) {
              console.log('selected branchhhhhh' , newVal + '- old:' + oldVal)
           if (newVal !== oldVal) {
               var debitorSelected =angular.copy( $filter('filter')($scope.branchesGroub, { id: $scope.branchGroub.id }));
               $scope.branchGroub = debitorSelected.length ? debitorSelected[0] : null;
              
               $scope.branchtoLink = $scope.branchesGroub.filter(function (el) { return el.companyId == $scope.branchGroub.companyId });
               if (debitorSelected.length) {

                   var id =parseInt(debitorSelected[0].id);
                   
                   $scope.balanceSheetTree(id);
                   $scope.branchidforlist =parseInt(angular.copy(id));
                   bId =parseInt(angular.copy(id));
                   $scope.GetOrderGroupNames();
                   $scope.getAccountList();
                   $scope.GetAllFinancialList();
                  
               }
           }
       });


       $scope.getAccountList = function () {

           BalanceSheetspersistenceService.action.getAllAccounts(JSON.stringify({ companyId:parseInt($scope.branchGroub.id) })).then(
                                 function (result) {
                                     $scope.Accountlist =result.data;
                                     $scope.models.lists.accounts.Accountlist = result.data;
                                     $scope.branchidforlist =parseInt( angular.copy($scope.branchGroub.id));

                                 },
                                 function (error) {
                                     toastr.error("حدث خطاء - اثناء انشاء قائمة للحسابات");
                                     
                                 });

           
       };

       $scope.GetAllFinancialList = function () {
           var id = $scope.branchGroub.id; 
           BalanceSheetspersistenceService.action.GetAllFinancialList(JSON.stringify({id :$scope.branchidforlist , isCompany : $scope.branchGroub.isCompany})).then(
                                 function (result) {

                                     $scope.financialLists = result.data;
                                    console.log('f Lists' , result)

                                 },
                                 function (error) {

                                     toastr.error("حدث خطاء - اثناء انشاء قائمة للحسابات");
                                   
                                 });


       };
       
       $scope.selectedList = {listId :"0" , listName : "حدد القائمة"};
       $scope.selectedListId = "0";
       $scope.setSelectedList = function (item) {
           $scope.selectedList = item;
           $scope.selectedListId = item.listId;
       };
     


       $scope.refreshAccountstreelist = function () {
           $('#jstree_accountsList_div').jstree(true).refresh();

       };

       $scope.setCustomAccountList = function () {
           $scope.CustomAccountList = [];
       };
       $scope.keys = { key: 0, keyf: 0, data: {}};
       $scope.saveCustomAccounts = function () {
           var accounttoSave =JSON.parse( angular.toJson($scope.models.lists.customAccounts.Accountlist, true));
           var data = $scope.keys.data;
           var obj = { CustomAccountCategoryId: $scope.customAccountCatogeryId, CustomAccountCatogeryName: '', BranchId: $scope.branchGroub.id, isCompany: $scope.branchGroub.isCompany, AccountsToSave: accounttoSave };



           BalanceSheetspersistenceService.action.saveCustomAccounts(JSON.stringify(obj)).then(
                         function (result) {
                             var key = $scope.financialLists[$scope.keys.keyf].listes.length
                             var keyf = $scope.keys.keyf;
                             var accountL =  accounttoSave.length - 1;
                             $scope.financialLists[$scope.keys.keyf].listes = result.data;
                           
                         },
                         function (error) {
                             toastr.error("حدث خطاء - اثناء انشاء قائمة للحسابات");
                         });

       };

       $scope.models = {
           selected: null,
           lists: {
              
               "accounts": { name: "دليل الحسابات", Accountlist:[]},
               "customAccounts": { name: "حسابات مختاره", Accountlist:[] }
               }
       };



        // Generate initial model
       for (var i = 1; i <= 3; ++i) {
           //$scope.Accountlist.push({ accountId: i, accountName: "Item A" + i });
           //$scope.CustomAccountList.push({ accountId: 1 + i, accountName: "Item B" + i });
           $scope.models.lists.accounts.Accountlist.push({ accountId: i, accountName: "Item A" + i });
           $scope.models.lists.customAccounts.Accountlist.push({ accountId: 1 + i, accountName: "Item B" + i });
       }

        // Model to JSON for demo purpose
       $scope.$watch('models', function (model) {
           $scope.modelAsJson = angular.toJson(model, true);
       }, true);

        //end of dndlistes

       $scope.merchandiseCompany = { publicId: 0, name: 'حدد الشركة' };

       $scope.itemGroupNames = [];
       $scope.orderGroupNames = [];
       $scope.selectedOrderGroupName = { orderGroupId: 0, orderGroupName : 'حدد المجموعة'};
       $scope.addEditOrderGroup = { orderGroupId: 0, orderGroupName: '', companyBranchId: $scope.branchGroub.companyId, isCompany: $scope.branchGroub.isCompany };
       $scope.saveUpdateOrderGroup = function (isNew) {
           if (isNew) {
               $scope.addEditOrderGroup.orderGroupId = 0;
               $scope.addEditOrderGroup.orderGroupName = '';
               $scope.addEditOrderGroup.companyBranchId = $scope.branchGroub.companyId;
               $scope.addEditOrderGroup.isCompany = $scope.branchGroub.isCompany;
               console.log('branchGroub branchGroub', $scope.branchGroub)
           }
           else {
               $scope.addEditOrderGroup.orderGroupId = $scope.selectedOrderGroupName.orderGroupId;
               $scope.addEditOrderGroup.orderGroupName = $scope.selectedOrderGroupName.orderGroupName;
               $scope.addEditOrderGroup.companyBranchId = $scope.branchGroub.companyId;
               $scope.addEditOrderGroup.isCompany = $scope.branchGroub.isCompany;

           }


           $('#addEditorderGroup-Modal').modal('show');
       };

       $scope.inventoryValuatioMethods = [];
       $scope.getinventoryValuatioMethods = function () {
           BalanceSheetspersistenceService.action.getinventoryValuatioMethods($scope.merchandiseCompany.publicId).then(
           function (result) {
               $scope.inventoryValuatioMethods = result;

           },
           function (error) {
               toastr.error("حدث خطاء اثناء الحفظ ");
           });
       };

       $scope.setInventoryValuationMethod = function (inventoryValuatioMethods) {
           console.log('inventoryValuatioMethods', inventoryValuatioMethods);
           BalanceSheetspersistenceService.action.setInventoryValuationMethod(angular.toJson(inventoryValuatioMethods)).then(
                function (result) {
                    toastr.info("تم الحفظ");

                },
                function (error) {
                    toastr.error("حدث خطاء اثناء الحفظ ");
                });
       };

       $scope.saveOrderGroup = function () {

           console.log("orderGrooup To SAve",angular.toJson( $scope.selectedOrderGroupName));

           BalanceSheetspersistenceService.action.saveOrderorderGroupf(angular.toJson($scope.selectedOrderGroupName)).then(
              function (result) {
                  toastr.info("تم الحفظ");

              },
              function (error) {
                  toastr.error("حدث خطاء اثناء الحفظ ");
              });
       }


       $scope.addNewOrderName = function () {
           BalanceSheetspersistenceService.action.SaveOrderName(JSON.stringify({ OrderNameId: 0, orderName: '', OrderGroupId: $scope.selectedOrderGroupName.orderGroupId, isDebit: true, ordertype: '' })).then(
              function (result) {
                  console.log('OrderGroups', result)
                  $scope.selectedOrderGroupName.ordersNames = null;

                  $scope.selectedOrderGroupName.ordersNames = result.data;
                 
              },
              function (error) {
                  toastr.error("حدث خطاء - اثناء طلب البيانات");
              });
       }
       
       $scope.addNewOrderProp = function (key , payMethed) {
           if (payMethed.payMethodProperties.length == null) {
               payMethed.payMethodProperties = [];
           }
           if (undefined !== payMethed.payMethodProperties && payMethed.payMethodProperties.length) {
               var payMethodProperties = { orderPropertiesPayId: payMethed.payMethodProperties.length + 1, orderPropertiesPayName: "خاصية جديدة/ " + (payMethed.payMethodProperties.length + 1), isRequired: true, toPrint: true, orderPropertyTypeName: $scope.orderGroupPropTypes[0].orderPropertyTypeName, OrderPropertyTypeId: $scope.orderGroupPropTypes[0].orderPropertyTypeId }
               payMethed.payMethodProperties.push(payMethodProperties);
           } else {
               var payMethodProperties = { orderPropertiesPayId: 1, orderPropertiesPayName: "خاصية جديدة/ " + (1), isRequired: true, toPrint: true, orderPropertyTypeName: $scope.orderGroupPropTypes[0].orderPropertyTypeName, OrderPropertyTypeId: $scope.orderGroupPropTypes[0].orderPropertyTypeId }
               payMethed.payMethodProperties[0] = payMethodProperties;
           }
           

       }
       $scope.orderGroupPropTypes = [];
       var getorderGroupPropType = function () {
           BalanceSheetspersistenceService.action.GetOrderGroupPropType(0).then(
              function (result) {
                  console.log('OrderProp Type', result)
                  $scope.orderGroupPropTypes = result;

              },
              function (error) {
                  toastr.error("حدث خطاء - اثناء طلب البيانات");
              });
       }();

       $scope.updateOrderProp = function (prop) {
           console.log('Order prop', prop)

           //BalanceSheetspersistenceService.action.SaveOrderProperty(JSON.stringify({ OrderPropId: prop.orderPropertiesId, OrderPropName: prop.orderPropertiesName, OrderGroupId: $scope.selectedOrderGroupName.orderGroupId, isRequird: prop.isRequired, OrderpropTypeName: prop.orderPropertyTypeName })).then(
           //   function (result) {
           //       console.log('OrderGroups', result)
           //       $scope.selectedOrderGroupName.orderProperties = null;

           //       $scope.selectedOrderGroupName.orderProperties = result.data;

           //   },
           //   function (error) {
           //       toastr.error("حدث خطاء - اثناء طلب البيانات");
           //   });
       }
       $scope.updaterderName = function (order) {

           console.log('Order' , order)
           BalanceSheetspersistenceService.action.SaveOrderName(JSON.stringify({ OrderNameId: order.ordersNameId, orderName: order.orderName, OrderGroupId: $scope.selectedOrderGroupName.orderGroupId, isDebit: true, ordertype : order.ordertype })).then(
              function (result) {
                  console.log('OrderGroups', result)
                  $scope.selectedOrderGroupName.ordersNames = null;
                  $scope.selectedOrderGroupName.ordersNames = result.data;

              },
              function (error) {
                  toastr.error("حدث خطاء - اثناء طلب البيانات");
              });
       }
     

       $scope.addpayMethod = {};
       $scope.payMethodfor = "";
       $scope.getAccountsforGroupPay = function (type) {
           // GetaccountsTypes
           $scope.whichOrderType = type;
           BalanceSheetspersistenceService.action.GetOrderAccountsForPay(JSON.stringify({ id: $scope.branchGroub.companyId, isCompany: $scope.branchGroub.isCompany, groupId: 0, gid: $scope.selectedOrderGroupName.orderGroupId })).then(
              function (result) {
                  $scope.payMethodfor = type;
                  console.log('$scope.orderaccountsType', result)
                  $scope.orderGroupSubAccounts = [];
                  $scope.orderGroupSubAccounts = result.data;

                  $scope.addpayMethod = {
                      "payId": 0,
                      "payName": "",
                      "accountId": 0,
                      "accountName": ""
                  };
                  $('#addOrderPaymethod-Modal').modal('show');

              },
              function (error) {
                  toastr.error("حدث خطاء - اثناء طلب البيانات");
              });
       };
       $scope.checkpaymethed = function (type) {
           console.log('paytype', type + '-' + $scope.payMethodfor)

           if ($scope.payMethodfor === 'sales' && type == 'name') {

               
               $scope.filetrpay = $filter('filter')($scope.selectedOrderGroupName.sales.payMethod, { payName: $scope.addpayMethod.payName },true)[0];
               console.log('payName', $scope.filetrpay);
               if ($scope.filetrpay != null) {
                   toastr.warning("تحذير : هذا الاسم مسجل من قبل لن يمكنك الحفظ");
               }
           }
           else if($scope.payMethodfor === 'sales' && type == 'account'){
               $scope.filetrpay = $filter('filter')($scope.selectedOrderGroupName.sales.payMethod, { accountName: $scope.addpayMethod.accountName }, true)[0];
               console.log('payName', $scope.filetrpay);
               if ($scope.filetrpay != null) {
                   toastr.warning("تحذير : هذا الاسم مسجل من قبل لن يمكنك الحفظ");
               }

           }
           if ($scope.payMethodfor === 'pur' && type == 'name') {
               $scope.filetrpay = $filter('filter')($scope.selectedOrderGroupName.purchases.payMethod, { payName: $scope.addpayMethod.payName }, true)[0];
               console.log('payName', $scope.filetrpay);
               if ($scope.filetrpay != null) {
                   toastr.warning("تحذير : هذا الاسم مسجل من قبل لن يمكنك الحفظ");
               }
           }
           else if ($scope.payMethodfor === 'pur' && type == 'account') {
               $scope.filetrpay = $filter('filter')($scope.selectedOrderGroupName.purchases.payMethod, { accountName: $scope.addpayMethod.accountName }, true)[0];
               console.log('payName', $scope.filetrpay);
               if ($scope.filetrpay != null) {
                   toastr.warning("تحذير : هذا الاسم مسجل من قبل لن يمكنك الحفظ");
               }

           }

       };
       $scope.selectedPaymethd = function (account) {
           console.log("paymethed", account);
           $scope.addpayMethod.accountId = account.accountId;
           $scope.addpayMethod.accountName = account.accountName;

       };

       
       $scope.savepaymethd = function () {
           if ($scope.payMethodfor === 'sales') {
               var filetrpay1 =angular.copy( $filter('filter')($scope.selectedOrderGroupName.sales.payMethod, { payName: $scope.addpayMethod.payName }, true)[0]);
               var filetrpay2 =angular.copy( $filter('filter')($scope.selectedOrderGroupName.sales.payMethod, { accountName: $scope.addpayMethod.accountName }, true)[0]);
               console.log('payName', $scope.filetrpay);
               if (filetrpay1 != null || filetrpay2 != null) {
                   toastr.error("لا يمكن الحفظ نظرا للتكرار");
               } else {
                   $scope.selectedOrderGroupName.sales.payMethod.push(angular.copy($scope.addpayMethod));

               }
           }

           if ($scope.payMethodfor === 'pur') {
               var filetrpay1 = $filter('filter')($scope.selectedOrderGroupName.purchases.payMethod, { payName: $scope.addpayMethod.payName }, true)[0];
               var filetrpay2 = $filter('filter')($scope.selectedOrderGroupName.purchases.payMethod, { accountName: $scope.addpayMethod.accountName }, true)[0];
               console.log('payName', $scope.filetrpay);
               if (filetrpay1 != null || filetrpay2 != null) {
                   toastr.error("لا يمكن الحفظ نظرا للتكرار");
               } else {
                   $scope.selectedOrderGroupName.purchases.payMethod.push(angular.copy($scope.addpayMethod));
                   $scope.addpayMethod = {
                       "payId": 0,
                       "payName": "",
                       "accountId": 0,
                       "accountName": ""
                   };
               }
           }

       };
       
       $scope.GetOrderGroupNames = function () {
           BalanceSheetspersistenceService.action.GetOrderGroupNames(JSON.stringify({ id: $scope.branchGroub.companyId, isCompany: $scope.branchGroub.isCompany })).then(
                 function (result) {
                     console.log('OrderGroups', result)
                     $scope.orderGroupNames = result.data;

                 },
                 function (error) {
                     toastr.error("حدث خطاء - اثناء طلب البيانات");
                 });
       };
       $scope.orderGroupMainAccounts = [];
       $scope.orderaccountsType = [];
       $scope.getAccountsDataTypesforGroup = function () {
           // GetaccountsTypes
           BalanceSheetspersistenceService.action.GetaccountsTypes($scope.selectedOrderGroupName.orderGroupId).then(
              function (result) {
                  console.log('$scope.orderaccountsType', result)
                  $scope.orderGroupMainAccounts = [];
                  //

                  $scope.orderaccountsType = result;
                  $('#addOrderMainAccount-Modal').modal('show');
                 
              },
              function (error) {
                  toastr.error("حدث خطاء - اثناء طلب البيانات");
              });
       };
       $scope.setSalesAccounts = function (account) {
           console.log('acccount', account);
           
        
          if (account.salesAccount == null || account.salesCostAccount == null || account.salesreturnAccount == null) {
               toastr.error("يجب اختيار جميع الحسابات - لا يمكن الحفظ");
          }
          else if (account.salesAccount.accountId == account.salesCostAccount.accountId
              || account.salesAccount.accountId == account.salesreturnAccount.accountId
              || account.salesCostAccount.accountId == account.salesreturnAccount.accountId) {
              toastr.error("يوجد حسابات مكررة - لا يمكن الحفظ");

          }
           else {
               account.disableAccount = true;
               $scope.selectedOrderGroupName.sales.orderMainAccounts.push(account);
           };
          
       }
       $scope.removepayMetod = function (index, type) {
           $scope.whichOrderType = type;
           $scope.salesaccountTodel = index;
           $('#confirmdelpayAccount-modal').modal('show');
       }
       $scope.confirmDeltePayAccount = function () {
           if ($scope.whichOrderType != 'pur')
           {
               $scope.selectedOrderGroupName.sales.payMethod.splice($scope.salesaccountTodel, 1);

           } else {
               $scope.selectedOrderGroupName.purchases.payMethod.splice($scope.salesaccountTodel, 1);

           }
       };
       $scope.salesaccountTodel = 0;
       $scope.removeSalesAccounts = function (index, account) {
           $scope.salesaccountTodel = index;
           $('#confirmdelsalesAccount-modal').modal('show');
          
       }

      
       $scope.confirmDelteSalesAccount = function () {
           $scope.selectedOrderGroupName.sales.orderMainAccounts.splice($scope.salesaccountTodel, 1);
       };

       $scope.orderGroupSubAccounts = [];
       $scope.setOrderMainAccounts = {};
       
       $scope.getSubAccountsforGroup = function () {
           // GetaccountsTypes
           BalanceSheetspersistenceService.action.GetOrderAccountsForSub(JSON.stringify({ id: $scope.branchGroub.companyId, isCompany: $scope.branchGroub.isCompany, groupId: 0, gid: $scope.selectedOrderGroupName.orderGroupId })).then(
              function (result) {
                  $scope.setOrderMainAccounts = result.data;
                  console.log('$scope.orderaccountsType', result)
                  for (var i = 0; i < $scope.setOrderMainAccounts.orderMainAccount.length; i++) {
                      var findMatchs = $filter('filter')($scope.selectedOrderGroupName.sales.orderMainAccounts, { inventoryAccount: { accountId: $scope.setOrderMainAccounts.orderMainAccount[i].inventoryAccount.accountId, accountName: $scope.setOrderMainAccounts.orderMainAccount[i].inventoryAccount.accountName } }, true)[0];
                      if (findMatchs != null) {
                          console.log('Matches Found', findMatchs)
                          findMatchs.disableAccount = true;
                          $scope.setOrderMainAccounts.orderMainAccount[i] = findMatchs;
                      }
                  }
                  
                  $('#addOrderSubAccount-Modal').modal('show');

              },
              function (error) {
                  toastr.error("حدث خطاء - اثناء طلب البيانات");
              });
       };
       $scope.subAccountsforGroupitems = [];
       $scope.getSubAccountsforGroupitems = function (type) {
           $scope.whichOrderType = type;
          
           BalanceSheetspersistenceService.action.GetOrderAccountsForSubitems(JSON.stringify({ id: $scope.branchGroub.companyId, isCompany: $scope.branchGroub.isCompany, groupId: 0, gid: $scope.selectedOrderGroupName.orderGroupId })).then(
              function (result) {
                 
                  $scope.subAccountsforGroupitems = result.data;
                  if (type !== "pur") {
                      for (var i = 0; i < $scope.subAccountsforGroupitems.length; i++) {
                          var findMatchs = $filter('filter')($scope.selectedOrderGroupName.sales.orderAccountsforSubAccounts, { accountId: $scope.subAccountsforGroupitems[i].accountId }, true)[0];
                          if (findMatchs != null) {
                              console.log('Matches Found', findMatchs)
                              findMatchs.disableAccount = true;
                              findMatchs.selectedaccount = true;
                              $scope.subAccountsforGroupitems[i] = findMatchs;
                          }
                      }
                  } else {
                      for (var i = 0; i < $scope.subAccountsforGroupitems.length; i++) {
                          var findMatchs = $filter('filter')($scope.selectedOrderGroupName.purchases.orderAccountsforSubAccounts, { accountId: $scope.subAccountsforGroupitems[i].accountId }, true)[0];
                          if (findMatchs != null) {
                              console.log('Matches Found', findMatchs)
                              findMatchs.disableAccount = true;
                              findMatchs.selectedaccount = true;

                              $scope.subAccountsforGroupitems[i] = findMatchs;
                          }
                      }
                  }

                
                  $('#addSubAccountsforGroupitems-Modal').modal('show');

              },
              function (error) {
                  toastr.error("حدث خطاء - اثناء طلب البيانات");
              });
       };
       $scope.saveSubAccountsforItem = function (accounts) {
           var trueSelected = angular.copy($filter('filter')(accounts, { selectedaccount: true }));
           console.log('trueSelected', trueSelected);
           if ($scope.whichOrderType !== 'pur') {
               $scope.selectedOrderGroupName.sales.orderAccountsforSubAccounts = trueSelected;

           } else {
               $scope.selectedOrderGroupName.purchases.orderAccountsforSubAccounts = trueSelected;

           }
       };

       $scope.orderGroupOtherAccounts = [];
     
       $scope.getOtherAccountsforGroup = function (type) {
           // GetaccountsTypes
           $scope.whichOrderType = type;
           BalanceSheetspersistenceService.action.GetOrderAccountsForOther(JSON.stringify({ id: $scope.branchGroub.companyId, isCompany: $scope.branchGroub.isCompany, groupId: 0, gid: $scope.selectedOrderGroupName.orderGroupId })).then(
              function (result) {

                  $scope.orderGroupOtherAccounts = result.data;
                  if (type != "pur") {
                      for (var i = 0; i < $scope.orderGroupOtherAccounts.length; i++) {
                          var findMatchs = $filter('filter')($scope.selectedOrderGroupName.sales.orderOtherTotalAccounts, { accountId: $scope.orderGroupOtherAccounts[i].accountId }, true)[0];
                          if (findMatchs != null) {
                              console.log('Matches Found', findMatchs)
                              findMatchs.selectedaccount = true;

                              $scope.orderGroupOtherAccounts[i] = findMatchs;
                          }
                      }
                  } else {
                      for (var i = 0; i < $scope.orderGroupOtherAccounts.length; i++) {
                          var findMatchs = $filter('filter')($scope.selectedOrderGroupName.purchases.orderOtherTotalAccounts, { accountId: $scope.orderGroupOtherAccounts[i].accountId }, true)[0];
                          if (findMatchs != null) {
                              console.log('Matches Found', findMatchs)
                              findMatchs.selectedaccount = true;

                              $scope.orderGroupOtherAccounts[i] = findMatchs;
                          }
                      }
                  }
               

                  
                 
                  $('#addOrderOtherAccount-Modal').modal('show');

              },
              function (error) {
                  toastr.error("حدث خطاء - اثناء طلب البيانات");
              });
       };
       $scope.orderGroupTotalAccounts = [];
       $scope.getTotalAccountsforGroup = function () {
           // GetaccountsTypes
           BalanceSheetspersistenceService.action.GetOrderAccountsForTotal(JSON.stringify({ id: $scope.branchGroub.companyId, isCompany: $scope.branchGroub.isCompany, groupId: 0, gid: $scope.selectedOrderGroupName.orderGroupId })).then(
              function (result) {
                  console.log('$scope.orderaccountsType', result)
                  $scope.orderGroupTotalAccounts = [];
                  $scope.orderGroupTotalAccounts = result.data;
                  $('#addOrdertotalAccount-Modal').modal('show');

              },
              function (error) {
                  toastr.error("حدث خطاء - اثناء طلب البيانات");
              });
       };

       $scope.addOrderMainAccount = function (id) {
           //addOrderMainAccount - Modal
           
           BalanceSheetspersistenceService.action.GetOrderAccounts(JSON.stringify({ id: $scope.branchGroub.companyId, isCompany: $scope.branchGroub.isCompany, groupId: id, gid: $scope.selectedOrderGroupName.orderGroupId})).then(
                function (result) {
                    console.log('$scope.OrderGroupMainAccounts', result)
                    $scope.orderGroupMainAccounts = result.data;
                   
                },
                function (error) {
                    toastr.error("حدث خطاء - اثناء طلب البيانات");
                });

       };
       $scope.selectedOrderSubAccounts = function (accounts) {
           //addOrderMainAccount - Modal
           console.log('accounts Main', accounts);
           var trueSelected = angular.copy($filter('filter')(accounts, { selectedaccount: true}));
           console.log('trueSelected', trueSelected);
           BalanceSheetspersistenceService.action.SaveOrdersubAccount(JSON.stringify(accounts)).then(
                 function (result) {
                     console.log('$scope.OrderGroupMainAccounts', result)

                     $scope.selectedOrderGroupName.orderAccountsforSubAccounts = result.data;

                 },
                 function (error) {
                     toastr.error("حدث خطاء - اثناء طلب البيانات");
                 });
           
       };
       $scope.selectedOrderOtherAccounts = function (accounts) {
           //addOrderMainAccount - Modal
          
           var trueSelected = angular.copy($filter('filter')(accounts, { selectedaccount: true }));
           console.log('trueSelected', trueSelected);
           if ($scope.whichOrderType != 'pur') {
               $scope.selectedOrderGroupName.sales.orderOtherTotalAccounts = trueSelected;

           } else {
               $scope.selectedOrderGroupName.purchases.orderOtherTotalAccounts = trueSelected;

           }
          

       };

       $scope.selectedOrderTotalAccounts = function (accounts) {
           //addOrderMainAccount - Modal
           console.log('accounts Main', accounts);
           var trueSelected = angular.copy($filter('filter')(accounts, { selectedaccount: true }));
           console.log('trueSelected', trueSelected);
           BalanceSheetspersistenceService.action.SaveOrderTotalAccount(JSON.stringify(accounts)).then(
                 function (result) {
                     console.log('$scope.OrderTotalAccounts', result)

                     $scope.selectedOrderGroupName.orderTotalAccounts = result.data;

                 },
                 function (error) {
                     toastr.error("حدث خطاء - اثناء طلب البيانات");
                 });

       };
       $scope.selectedMainAccounts = function (accounts) {
           //addOrderMainAccount - Modal
           console.log('accounts Main', accounts);
           BalanceSheetspersistenceService.action.SaveOrderMainAccount(JSON.stringify(accounts)).then(
                function (result) {
                    console.log('$scope.OrderGroupMainAccounts', result)

                    $scope.selectedOrderGroupName.orderMainAccounts = result.data;

                },
                function (error) {
                    toastr.error("حدث خطاء - اثناء طلب البيانات");
                });

       };

       $scope.$watch('merchandiseCompany.publicId', function (newVal, oldVal) {
           if (newVal !== oldVal) {
               var merchandiseCompany = angular.copy($filter('filter')($scope.companies, { publicId: $scope.merchandiseCompany.publicId }));
               if(merchandiseCompany != null){
                   //merchandiseCompany.id = merchandiseCompany.publicId;
               }
               $scope.merchandiseCompany = merchandiseCompany[0];
               BalanceSheetspersistenceService.action.getItemsGroupNames($scope.merchandiseCompany.publicId).then(
                function (result) {
                    console.log('item group names', result)
                    $scope.itemGroupNames = result.names;
                    $scope.units = result.units;

                    $scope.propertyTypes = result.propertyTypes;
                    console.log('$scope.units', $scope.units)
                    $scope.getinventoryValuatioMethods();
                },
                function (error) {
                    toastr.error("حدث خطاء - اثناء طلب البيانات");
                });
           };
       });



       $scope.itemGroups =[];

       $scope.itemaccounts = [];

       $scope.selectedItemGroup = { itemGroupId: 0, itemGroupName: "حدد الفئة", unit: { unitId: 0, unitName: "حدد وحدة القياس" } };
       $scope.selectedOrderGroup = { orderGroupId: 0, orderGroupName: "حدد المجموعة"};
       $scope.selectedGroup = function (data) {
           $scope.selectedItemGroup = data;
       };
       $scope.units = [];
       $scope.$watch('selectedItemGroup.itemGroupId', function (newVal, oldVal) {
           console.log('selected selectedItemGroup.itemGroupId', newVal + '- old:' + oldVal)
           if (newVal !== oldVal) {
               
              /// var groups = angular.copy($filter('filter')($scope.itemGroups, { itemGroupId: newVal }));
              
               console.log('newVal[0]', newVal)

               BalanceSheetspersistenceService.action.GetitemsGroup(parseInt(newVal)).then(
                             function (result) {
                                 console.log('groups[0]', result)
                                 $scope.selectedItemGroup = result;
                                
                                 $scope.selectedUnit = $scope.selectedItemGroup.unit;
                                
                                 $scope.rowColumns = [];

                                 $scope.re();
                             },
                             function (error) {
                                 toastr.error('لا يمكن طلب البيانات الان')
                             });
           };
       });

       $scope.refreshItemGroup = function () {

           BalanceSheetspersistenceService.action.GetitemsGroup($scope.selectedItemGroup.itemGroupId).then(
                         function (result) {
                             console.log('groups[0]', result)
                             $scope.selectedItemGroup = result;

                             $scope.selectedUnit = $scope.selectedItemGroup.unit;

                             $scope.rowColumns = [];

                             $scope.re();
                         },
                         function (error) {
                             toastr.error('لا يمكن طلب البيانات الان')
                         });
       };
       $scope.$watch('selectedOrderGroupName.orderGroupId', function (newVal, oldVal) {
           console.log('selected selectedItemGroup.itemGroupId', newVal + '- old:' + oldVal)
           if (newVal !== oldVal) {

               console.log('newVal[0]', newVal)
               
               BalanceSheetspersistenceService.action.GetOrderGroupbyId(newVal).then(
                       function (result) {
                           $scope.selectedOrderGroupName = result;
                       },
                       function (error) {
                           toastr.error("حدث خطاء - اثناء طلب البيانات");
                       });
              

           };
       });

      
       $scope.orders = [{ orderId: 1, orderName: "اذن اضافة" }];
       $scope.order = $scope.orders[0];
       
       $scope.confirmDelItemGroup = null;
     
       $scope.deleitemgroup = function(accountId, type){
           console.log('to DElte', accountId + ' - ' + type);
           $scope.confirmDelItemGroup = { accountId: accountId, typeName: type, itemGroupId: $scope.selectedItemGroup.itemGroupId };
           $('#delItemGroup-Modal').modal('show');
       };
       $scope.confirmDelItemGroupDone = function () {
           if ($scope.confirmDelItemGroup != null) {
               BalanceSheetspersistenceService.action.deleteFromItemGroup(angular.toJson($scope.confirmDelItemGroup)).then(
                    function (result) {
                        console.log("new item group", result)
                        $scope.selectedItemGroup = result.data;

                        $scope.selectedUnit = $scope.selectedItemGroup.unit;
                        $scope.rowColumns = [];

                        $scope.re();
                        toastr.info('تم الحذف')


                    },
                    function (error) {
                        toastr.error('لا يمكن الحذف الان')
                    });
               $scope.confirmDelItemGroup = null;
           }

       };

       $scope.delepropertyValues = function (row) {
           console.log('row', angular.toJson({ accountId: 0, typeName: 'propertyValue', itemGroupId: $scope.selectedItemGroup.itemGroupId, propertyValues: row }));
           $scope.confirmDelItemGroup = { accountId: 0, typeName: 'propertyValue', itemGroupId: $scope.selectedItemGroup.itemGroupId, propertyValues: row };
           $('#delItemGroup-Modal').modal('show');
       };
     $scope.colorValueChanged = function (key0 , key , data) {
           
           $scope.rowColumns[0][0].value = data;
           console.log("data", $scope.rowColumns[0][0].value)
         
     }

       $scope.getColorValue = function(propertyValueId, value){
           $('#propertyValue' + propertyValueId).colorpicker({
               color: value,
               format: 'rgba',
           });

           $('#propertyValue' + propertyValueId).colorpicker().on('changeColor',
            function (ev) {
                var input = $('#txtp' + propertyValueId);

                input.trigger('input');
                //if ($filter('filter')($scope.colorValue, { propertyValueId: propertyValueId })[0] == 'undefined' || $filter('filter')($scope.colorValue, { propertyValueId: propertyValueId })[0] == null) {
                //    $scope.colorValue.push({ propertyValueId: propertyValueId, value: $('#propertyValue' + propertyValueId).data('colorpicker').color.toString() });
                    
                //    //angular.element(jQuery('#txtp' + propertyValueId)).triggerHandler('#txtp' + propertyValueId)
                //}

            });

           $('#txtp' + propertyValueId).focus(function () {
               $('#propertyValue' + propertyValueId).colorpicker('show')
               //angular.element(jQuery('#txtp' + propertyValueId)).triggerHandler('#txtp' + propertyValueId)
           });

         

      $('#propertyValue' + propertyValueId).colorpicker().on('hidePicker',
         function () {
             if ($filter('filter')($scope.colorValue, { propertyValueId: propertyValueId })[0] == 'undefined' ||$filter('filter')($scope.colorValue, { propertyValueId: propertyValueId })[0] ==null) {
                 $scope.colorValue.push({ propertyValueId: propertyValueId, value: $('#propertyValue' + propertyValueId).data('colorpicker').color.toString() });
               
             }

         });

       
           return value
       }

       $scope.restColor = function () {
           $scope.colorValue = [];
       };

     


      $scope.selectedpropertyType = { typeId: 0 };


       $scope.beforeupdateproperty = function (data) {
           $scope.selectedpropertyType
       };
       $scope.dataprperty = {};
  
       $scope.showPropertyType = function (data) {
           console.log('data to show on select', data)
           $scope.dataprperty = data;
           $scope.selectedpropertyType = data.type;
       }
       $scope.afterupdateproperty = function (data) {
           //var property = $filter('filter')($scope.selectedItemGroup.properties, { propertyId: data.propertyId });
           $scope.dataprperty = data;
          // var id = angular.copy($scope.selectedpropertyType.typeId);
           console.log('1')

           $timeout(function () {
             
           })
          
       };

       $scope.$watch('selectedpropertyType.typeId', function (newVal, oldVal) {
           if ($scope.selectedpropertyType.typeId != 0) {
               console.log('selected selectedItemGroup.itemGroupId', newVal + '- old:' + oldVal)
               console.log('2')
               var type = angular.copy($filter('filter')($scope.propertyTypes, { typeId: newVal }));
               console.log('type[000]', type[0])
               $scope.selectedpropertyType = type[0];

               var property = $filter('filter')($scope.selectedItemGroup.properties, { propertyId: $scope.dataprperty.propertyId });
               console.log('ffffff', property);
               if (property != null) {
                   if (property[0].type != null) {
                       property[0].type = $scope.selectedpropertyType;
                       property[0].type.propertyId = property[0].propertyId;
                       property[0].type.propertyName = $scope.dataprperty.propertyName;
                       for (var i = 0; i < property[0].propertyValues.length; i++) {
                           property[0].propertyValues[i].typeName = $scope.selectedpropertyType.typeName;
                       }
                       $scope.rowColumns = [];
                       $scope.re();
                   }
                   else {
                       property[0].type = $scope.selectedpropertyType;
                       property[0].type.propertyId = property[0].propertyId;
                       property[0].type.propertyName = $scope.dataprperty.propertyName;
                       for (var i = 0; i < property[0].propertyValues.length; i++) {
                           property[0].propertyValues[i].typeName = $scope.selectedpropertyType.typeName;
                       }
                       $scope.rowColumns = [];
                       $scope.re();

                   }

               }
           }

           

       });

       $scope.rowColumns = [];
       $scope.itemFrominput = [];
       $scope.itemsInputTosave = function (property, items) {
           console.log('$scope.itemFrominput ', angular.toJson($scope.itemFrominput));
           BalanceSheetspersistenceService.action.saveItemPropertyValueInputs(angular.toJson($scope.itemFrominput)).then(
                   function (result) {
                       console.log('item group property', result);
                       for (var i = 0; i < $scope.itemFrominput.length; i++) {
                           var nn = $filter('filter')($scope.selectedItemGroup.properties, { propertyId: $scope.itemFrominput[i].propertyId }, true);

                           if ($scope.itemFrominput[i].type.typeName !== "لون") {
                               nn[0].propertyValues.push({ propertyId: $scope.itemFrominput[i].propertyId, typeName: $scope.itemFrominput[i].type.typeName, propertyValueId: 0, propertyValueName: $scope.itemFrominput[i].propertyValues, value: null, uniqueId: result.data, isNew : true})

                           } else {
                               nn[0].propertyValues.push({ propertyId: $scope.itemFrominput[i].propertyId, typeName: $scope.itemFrominput[i].type.typeName, propertyValueId: 0, propertyValueName: '--', value: $scope.itemFrominput[i].propertyValues, uniqueId: result.data, isNew:true});

                           }

                       }
                       $scope.rowColumns = [];
                       $scope.re();
                       //itemfname
                       for (var i = 0; i < $scope.itemFrominput.length; i++) {
                           $scope.itemFrominput[i].propertyValues = null;

                       }
                       setTimeout(function () { $('#itemfname').focus() }, 200);


                   },
                   function (error) {
                       toastr.error('لا يمكن الاضافة الان')
                   });
         
         
       };

       $scope.re = function () {
           $scope.itemFrominput = angular.copy($scope.selectedItemGroup.properties);
           for (var i = 0; i < $scope.itemFrominput.length; i++) {
               $scope.itemFrominput[i].propertyValues = null;
               $scope.itemFrominput[i].itemGroupId = angular.copy($scope.selectedItemGroup.itemGroupId);
               $scope.itemFrominput[i].companyId = angular.copy($scope.selectedItemGroup.companyId);
           }
           $scope.selectedItemGroup.properties.forEach(function (column, columnIndex) {
               column.propertyValues.forEach(function (card, rowIndex) {
                   if (!$scope.rowColumns[rowIndex])
                       $scope.rowColumns[rowIndex] = [];
                   $scope.rowColumns[rowIndex][columnIndex] = card;
               });
           });
        

       };

       $scope.AccounttoItemGroup = [];
       $scope.selectedAccounttoItemGroup = {};
       $scope.setSelectedAccounts = function () {

           console.log('selected accounts 1111111', $scope.AccounttoItemGroup)
           var acc = angular.copy($filter('filter')($scope.AccounttoItemGroup, { value: true }));

           for (var i = 0; i < acc.length; i++) {
               acc[i].code = $scope.selectedItemGroup.itemGroupId;
           }
           console.log('selected accounts 2222222', acc)


           BalanceSheetspersistenceService.action.saveItemGroupAccounts(JSON.stringify(acc)).then(
         function (result) {
             $scope.selectedItemGroup.accounts = result.data;
             toastr.error('تم تحديث الحسابات')


         },
         function (error) {
             toastr.error('لا يمكن حفظ البيانات الان')
         });
       };
       $scope.addNewItemGroup = function () {
           BalanceSheetspersistenceService.action.saveItemGroup($scope.merchandiseCompany.publicId).then(
         function (result) {
             console.log( "new item group", result)
             $scope.selectedItemGroup = result;

             $scope.selectedUnit = $scope.selectedItemGroup.unit;
             $scope.rowColumns = [];

             $scope.re();
             toastr.info('تم اضافة فئة')


         },
         function (error) {
             toastr.error('لا يمكن اضافة فئة جدية الان الان')
         });
       };

       $scope.updateItemGroupName = function () {
           BalanceSheetspersistenceService.action.updateItemGroupName(JSON.stringify({ itemGroupId: $scope.selectedItemGroup.itemGroupId, itemGroupName: $scope.selectedItemGroup.itemGroupName })).then(
         function (result) {
           
             toastr.info('تم تحديث البيانات')
         },
         function (error) {
             toastr.error(error.data.message)
         });
       };



       $scope.updateItemGroupUnit = function () {
          
               BalanceSheetspersistenceService.action.updateItemGroupunit(JSON.stringify({ itemGroupId: $scope.selectedItemGroup.itemGroupId, unitId: $scope.selectedItemGroup.unit.unitId })).then(
                     function (result) {
                         var unit = angular.copy($filter('filter')($scope.units, { unitId: $scope.selectedItemGroup.unit.unitId }));
                         console.log('unit[0]', unit[0])

                         $scope.selectedUnit = unit[0];
                         $scope.selectedItemGroup.unit = $scope.selectedUnit;
                         toastr.info('تم تحديث البيانات')
                     },
                     function (error) {
                         toastr.error(error.data.message)
                     });
          
       
       };


       $scope.saveItemGroupProperty = function () {

           BalanceSheetspersistenceService.action.saveItemGroupProperty($scope.selectedItemGroup.itemGroupId).then(
                 function (result) {
                     $scope.selectedItemGroup = result;
                     $scope.rowColumns = [];

                     $scope.re();
                     toastr.info('تم تحديث البيانات')
                 },
                 function (error) {
                     toastr.error(error.data.message)
                 });
       };

       $scope.updateItemGroupProperty = function (data) {

           BalanceSheetspersistenceService.action.UpdateItemGroupProperty(JSON.stringify(data)).then(
                 function (result) {
                     $scope.afterupdatePropertyValue(data);
                     toastr.info('تم تحديث البيانات')
                 },
                 function (error) {
                     toastr.error(error.data.message)
                 });
       };


       $scope.updateItemPropertyValues = function (data) {
           if ($scope.colorValue.length > 0) {

               //var propertycolor = $filter('filter')(data, { typeName: 'لون' });
               //console.log('colloction of color', propertycolor);
               //console.log('colloction of color values', $scope.colorValue);

               //if (propertycolor != null) {
               //    //var color = $filter('filter')($scope.colorValue, { propertyValueId: 'لون' })
               //    for (var i = 0; i < propertycolor.length; i++) {
               //        for (var b = 0; b < $scope.colorValue.length; b++) {
               //            if (propertycolor[i].propertyValueId == $scope.colorValue[b].propertyValueId) {
               //                propertycolor[i].value = angular.copy($scope.colorValue[b].value);
               //                console.log('PPPPPP ', propertycolor[i].value);

               //            }

               //        }
               //    }
               //}
               //$scope.colorValue = [];
               //console.log('property to compare', data);
               //console.log('scolorValue', $scope.colorValue);
           }
           BalanceSheetspersistenceService.action.updateItemPropertyValues(JSON.stringify(data)).then(
                 function (result) {
                     $scope.selectedItemGroup = result.data;
                     $scope.selectedUnit = $scope.selectedItemGroup.unit;

                     $scope.rowColumns = [];

                     $scope.re();
                     toastr.info('تم تحديث البيانات')
                 },
                 function (error) {
                     toastr.error(error.data.message)
                 });
       };


       $scope.saveItemPropertyValues = function () {

           BalanceSheetspersistenceService.action.saveItemPropertyValues($scope.selectedItemGroup.itemGroupId).then(
                 function (result) {
                     $scope.selectedItemGroup = result;
                     $scope.selectedUnit = $scope.selectedItemGroup.unit;

                     $scope.rowColumns = [];

                     $scope.re();
                     toastr.info('تم تحديث البيانات')
                 },
                 function (error) {
                     toastr.error(error.data.message)
                 });
       };


       $scope.linkNewAccounttoItemGroup = function () {
      
           BalanceSheetspersistenceService.action.getAccountDataTypesForItemGroup($scope.selectedItemGroup.itemGroupId).then(
                function (result) {
                    $scope.AccounttoItemGroup = result;
                    console.log('مخازن', $scope.AccounttoItemGroup);
                 
                   
                    $('#itemGroup-modal').modal('show');
                    
                },
                function (error) {
                    toastr.error('لا يمكن اظهار قائمة بحسابات المخازن الان')
                });

           
          
       };

       $scope.propertyValueIdUnique =120;
       $scope.propertyuniqeId = 120;
       $scope.addNewProperty = function () {
           //"property in selectedItemGroup.properties
           var propertyLength = $scope.selectedItemGroup.properties[0].propertyValues.length;
           var propertyvalues = [];
           for (var i = 0; i < propertyLength; i++) {
               propertyvalues.push({ propertyValueId: $scope.propertyValueIdUnique, propertyValueName: "", isBarCode: false, propertyId: $scope.propertyuniqeId, typeName: 'نص', value: "" });
               $scope.propertyValueIdUnique++;

           };
           $scope.selectedItemGroup.properties.push({
               propertyId: $scope.propertyuniqeId, propertyName: "خاصية جديدة", type: { typeId: 6, typeName: 'نص', show: true, description: "", propertyId: $scope.propertyuniqeId },
               show: true, propertyValues: propertyvalues
           });
           $scope.propertyuniqeId++;
           $scope.selectedItemGroup.properties
           $scope.rowColumns = [];
           $scope.re();
        
       };

      

       $scope.beforeupdatePropertyValue = function (data) {
           console.log('data property value before ubdated', $scope.colorValue);
          
       };
       $scope.afterupdatePropertyValue = function (data) {
          
           var ibjetedss = [];
          
           if ($scope.colorValue.length > 0) {

               var propertycolor = $filter('filter')(data, { typeName: 'لون' });
               
               
               if (propertycolor != null) {
                   //var color = $filter('filter')($scope.colorValue, { propertyValueId: 'لون' })
                   for (var i = 0; i < propertycolor.length; i++) {
                       for (var b = 0; b < $scope.colorValue.length; b++) {
                           if (propertycolor[i].propertyValueId == $scope.colorValue[b].propertyValueId)
                           {
                               propertycolor[i].value = angular.copy($scope.colorValue[b].value);
                             

                           }

                       }
                   }
               }
               $scope.colorValue = [];
              

           }
           
       };
     
       $scope.getbarCode = function (id , data) {
           if (data) {
               $('#barcode' + id).JsBarcode(data, {
                   height: 25
               });
           }
      

           
       };
       
  

       $scope.selectedUnit = $scope.selectedItemGroup.unit;

       $scope.$watch('selectedItemGroup.unit.unitId', function (newVal, oldVal) {
           console.log('selectedUnit.UnitId', newVal + '- old:' + oldVal)
           if (newVal !== oldVal) {

               //var unit = angular.copy($filter('filter')($scope.units, { unitId: newVal }));
               //console.log('unit[0]', unit[0])

               //$scope.selectedUnit = unit[0];
               //$scope.selectedItemGroup.unit = $scope.selectedUnit;
           };
       });

    });

}());


