﻿(function () {

    'use strict';
    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("AccountCategoryPropertiesController", function ($scope, _, AccountCategoryPropertiespersistenceService, Offline, $location ,  $routeParams) {

        var AccountCategoryPropertyName = $routeParams.name;
        $scope.catName = $routeParams.name;
        console.log(AccountCategoryPropertyName);
        $scope.showList = false;
        $scope.AccountCategoryProperties = [];
        $scope.AccountCategoryPropertiesNames = [];
        $scope.AccountCategoryPropertypropertyValues = [];
        $scope.PropertyValues = [];
        var getData = function () {

            AccountCategoryPropertiespersistenceService.action.getById(AccountCategoryPropertyName).then(
                function (AccountCategoryProperties) {
                    $scope.AccountCategoryProperties = AccountCategoryProperties;
                   
                    
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (AccountCategoryProperties.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };
        var lazyGetData = _.debounce(getData, 2);

        Offline.on('confirmed-down', lazyGetData);
        Offline.on('confirmed-up', lazyGetData);

        lazyGetData();

        var getAccountCategoryPropertyNameData = function () {

            AccountCategoryPropertiespersistenceService.action.getAll().then(
                function (AccountCategoryProperties) {
                    $scope.AccountCategoryPropertiesNames = AccountCategoryProperties;

                    console.log(AccountCategoryProperties);
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (AccountCategoryProperties.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };

       
        var lazyGetAccountCategoryPropertyNameData = _.debounce(getAccountCategoryPropertyNameData, 2);

        Offline.on('confirmed-down', lazyGetAccountCategoryPropertyNameData);
        Offline.on('confirmed-up', lazyGetAccountCategoryPropertyNameData);

        lazyGetAccountCategoryPropertyNameData();
       

        function getValueByKey(key, data) {
            var i, len = data.length;

            for (i = 0; i < len; i++) {
                if (data[i] && data[i].hasOwnProperty(key)) {
                    return data[i][key];
                }
            }

            return -1;
        }

        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;   //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        // $scope.catogeries = AccountCategoryPropertyRepository.getcatogery();

        $('#loading').hide();


        // Save 

        $scope.showSuccessMessage = false;
        $scope.showFillOutFormMessage = false;
        $scope.isOnline = true;
        $scope.addAccountCategoryProperty = {};







        var hasAccountCategoryPropertyToSave = function () {



            console.log($scope.addAccountCategoryProperty)

            var hasValue = function (value) {

                if (typeof value === 'string') {
                    return value.length > 0;
                }

                return value > 0;
            };

            var returnValue =
                       hasValue($scope.addAccountCategoryProperty.name)
                    && hasValue($scope.addAccountCategoryProperty.price)
                    && hasValue($scope.addAccountCategoryProperty.catogery);

            return returnValue;
        };

        $scope.save = function () {
            if ($scope.addAccountCategoryProperty.id === null || $scope.addAccountCategoryProperty.id === undefined) {
                $scope.addAccountCategoryProperty.id = $scope.addAccountCategoryProperty.name;
            }

            var saveAccountCategoryProperty = hasAccountCategoryPropertyToSave();


            var AccountCategoryProperty = $scope.addAccountCategoryProperty;


            //;

            AccountCategoryPropertiespersistenceService.action.save(AccountCategoryProperty).then(
                function (result) {
                    $scope.showSuccessMessage = true;
                    $scope.showErrorMessage = false;
                    $scope.addAccountCategoryProperty = {};
                },
                function (error) {
                    $scope.showSuccessMessage = false;
                    $scope.showErrorMessage = true;
                });



        };

        Offline.on('confirmed-down', function () {
            $scope.$apply(function () {
                $scope.isOnline = false;
            });
        });

        Offline.on('confirmed-up', function () {
            $scope.$apply(function () {
                $scope.isOnline = true;
            });
        });

        //Edit and update
        var AccountCategoryPropertyToEdit = [];

        $scope.editingAccountCategoryProperty = {};


        $scope.modify = function (AccountCategoryProperty) {
            for (var i = 0, length = $scope.AccountCategoryProperties.length; i < length; i++) {
                $scope.editingAccountCategoryProperty[$scope.AccountCategoryProperties[i].id] = false;
            }
            AccountCategoryPropertyToEdit = angular.copy(AccountCategoryProperty);
            $scope.addAccountCategoryProperty = AccountCategoryProperty;
            $scope.editingAccountCategoryProperty[AccountCategoryProperty.id] = true;

        };


        $scope.update = function (AccountCategoryProperty) {
            $scope.editingAccountCategoryProperty[AccountCategoryProperty.id] = false;
        };

        $scope.cancel = function (AccountCategoryProperty) {

            AccountCategoryProperty.name = AccountCategoryPropertyToEdit.name;
            AccountCategoryProperty.price = AccountCategoryPropertyToEdit.price;
            AccountCategoryProperty.catogery = AccountCategoryPropertyToEdit.catogery;

            $scope.editingAccountCategoryProperty[AccountCategoryProperty.id] = false;

        };

        $scope.reset = function () {
            for (var i = 0, length = $scope.AccountCategoryProperties.length; i < length; i++) {
                $scope.editingAccountCategoryProperty[$scope.AccountCategoryProperties[i].id] = false;
            }
            $scope.addAccountCategoryProperty = {};
        };


        //delte
        var delteAccountCategoryProperty = {};
        $scope.AccountCategoryPropertyToDelte = function (AccountCategoryProperty) {

            delteAccountCategoryProperty = AccountCategoryProperty;
        };


        $scope.delete = function () {


            AccountCategoryPropertiespersistenceService.action.Delete(delteAccountCategoryProperty.id).then(
                function (result) {
                    $scope.AccountCategoryProperties.splice($scope.AccountCategoryProperties.indexOf(delteAccountCategoryProperty), 1);
                    delteAccountCategoryProperty = {};
                },
                function (error) {
                    $scope.error = error;
                });
        };


        AccountCategoryPropertiespersistenceService.getById(AccountCategoryPropertyName).then(
            function (AccountCategoryProperties) {
                $scope.AccountCategoryProperties = AccountCategoryProperties;
            },
            function (error) {
                $scope.error = error;
            });

    });

}());
