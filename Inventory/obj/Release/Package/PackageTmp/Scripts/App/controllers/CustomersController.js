﻿
(function () {


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("CustomersController", function ($scope, $timeout, _, AccountspersistenceService, $localStorage, $rootScope, settings, $filter,
                                                                 Offline, $location, $sce, EasyStoreUserspersistenceService, authService, globalService, $q, $log, DTOptionsBuilder, DTColumnDefBuilder) {




        var vm = this;
        vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(2);


        var start = moment().startOf('month');
        var end = moment().endOf('month');
        $scope.nowDate = moment().format('DD/MM/YYYY');
        $scope.startDate = moment().startOf('month').toDate();
        $scope.endDate = moment().endOf('month').toDate();

        function cb(start, end) {
            $('#reportrange span').html('من ' + start.format('DD - MM -YYYY') + ' الي ' + end.format('DD - MM - YYYY'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'اليوم': [moment(), moment()],
                'امس': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'اخر 7 ايام': [moment().subtract(6, 'days'), moment()],
                'اخر 30 يوم': [moment().subtract(29, 'days'), moment()],
                'هذا الشهر': [moment().startOf('month'), moment().endOf('month')],
                'الشهر الماضي': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);



        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            $scope.startDate = picker.startDate.toDate();
            $scope.endDate = picker.endDate.toDate();
            $scope.getaccountMoves();
        });

        $scope.totalItems = 0;
        $scope.currentPage = 0;
        $scope.itemsperpage = 1;
        $scope.reportChange = 'account'


        $scope.$watch('itemsperpage', function (newVal, oldVal) {

            var newvalint = parseInt(newVal)
            if (isNaN(newvalint)) {
                $scope.itemsperpage = 10;
            }
            if (newvalint !== oldVal && newVal > 1 && !$scope.displeWatch) {
                $scope.getaccountMoves($scope.selectedAccountId, parseInt($scope.itemsperpage), $scope.currentPage, $scope.startDate, $scope.endDate);
                $scope.itemsperpage = newvalint;
            }
            if ($scope.displeWatch) {
                $scope.displeWatch = false;
            }

        });
        $scope.account = {};
        $scope.sortType = 'date'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order

        $scope.$watch('sortType', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getaccountMoves();
            }

        });
        $scope.$watch('sortReverse', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getaccountMoves();
            }

        });

        $scope.$watch('reportChange', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                if ($scope.reportChange === 'account') {
                    if ($scope.branchGroub.id !== 0) {
                        //$('#binditem').empty();
                        //$('#binditem').text('0');
                        //document.getElementById("#binditem").innerHTML = "";
                        //undefined
                        $scope.loadAccounts();
                    }
                }

                if ($scope.reportChange === 'accounts') {
                    $scope.accountMoves = {};
                    $scope.account.selected = undefined;
                    $scope.accountsName = [];
                }

            }
        });


        var bId;

        $scope.branchesGroub = [];
        $scope.branchGroub = $rootScope.mainbranchGroub;




        $scope.loadCompanies = function () {
            $scope.branchesGroub = $rootScope.mainbranchesGroub;
        };


        $scope.$watch('$root.mainbranchGroub.id', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal !== '0') {
                var findCompanybr = angular.copy($filter('filter')($rootScope.mainbranchesGroub, { id: newVal }, true));
                if (findCompanybr) {
                    $scope.branchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    $rootScope.mainbranchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    console.log('$rootScope.mainbranchGroub', $rootScope.mainbranchGroub);
                    if (findCompanybr.length) {

                        bId = parseInt(angular.copy(parseInt(findCompanybr[0].id)));

                        EasyStoreUserspersistenceService.action.branchGroupSt(JSON.stringify($scope.branchGroub)).then(function () { }, function () { });
                        if ($scope.reportChange === 'account') {
                            if ($scope.branchGroub.id !== '0') {
                                $scope.loadAccounts();
                            }

                        }
                    }
                }

            }
        });


        $scope.mainAccountName = "عميل";
        $scope.addNewacc = function (data) {
            toastr.clear();
            if (data) {
                AccountspersistenceService.action.addNewMainacc(JSON.stringify({ accountName: data, accountType: $scope.typetorecive, isNew: true, accountId: 0, active : true})).then(
                    function (result) {
                        if (!$scope.accountsNames)
                        {
                            $scope.accountsNames = [];
                        }
                        $scope.accountsNames.push(result.data);
                        toastr.info("تم الحفظ");
                        toastr.clear();
                    },
                    function (error) {
                        if (error.data) {
                            if (error.data.message) {
                                toastr.error(error.data.message);
                            }

                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }
                    });

            }
            if (!data) {
                
                toastr.warning("يجب ادخال اسم لـ " +  $scope.mainAccountName);
            }
        };

        $scope.istoEdit = false;
        $scope.selectedaccountToEdit = {};
        $scope.selectedaccountToEdiOrginal = {};
        $scope.seteditaccName = function (data) {
            if (data) {
                $scope.istoEdit = true;
                $scope.selectedaccountToEdit = angular.copy(data);
                $scope.selectedaccountToEdiOrginal = data;
            }
        };
        $scope.editaccName = function (val) {
            if (val === false) {
                $scope.selectedaccountToEdit = {};
                $scope.istoEdit = false;
                return;
            };

            toastr.clear();
            if ($scope.selectedaccountToEdit) {
                console.log('acc To Edit', $scope.selectedaccountToEdit);
                AccountspersistenceService.action.addNewMainacc(JSON.stringify({ accountName: $scope.selectedaccountToEdit.accountName, accountType: $scope.typetorecive, isNew: false, accountId: $scope.selectedaccountToEdit.accountId, active: true })).then(
                    function (result) {
                        $scope.selectedaccountToEdiOrginal.accountName = $scope.selectedaccountToEdit.accountName;
                        var findacc = $filter('filter')($scope.accountsNames, { accountId: $scope.selectedaccountToEdit.accountId }, true);
                        if (findacc.length) {
                            findacc[0].accountName = $scope.selectedaccountToEdit.accountName;
                            $localStorage.accountselectedcust = $scope.selectedaccountToEdiOrginal;
                        };
                        toastr.info("تم التحديث");
                        $scope.selectedaccountToEdit = {};
                        $scope.istoEdit = false;

                        toastr.clear();
                    },
                    function (error) {
                        if (error.data) {
                            if (error.data.message) {
                                toastr.error(error.data.message);
                            }

                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }
                    });
            }
        };
        $scope.accountPropertyName = "عملاء";
     
        $scope.typetorecive = 'cust';
        $scope.properties = [];
        $scope.accountsNames = [];
        $scope.propertyTypes = [];
        $scope.waitingOrdercount = 0;
        $scope.waitingRequestcount = 0;
        $scope.newNotfy = 0;
        $scope.loadAccounts = function () {
            AccountspersistenceService.action.GetaccountCategoryProperties(JSON.stringify({ companyId: bId, isCompany: $scope.branchGroub.isCompany, accountType: $scope.typetorecive })).then(
                   function (result) {
                       $scope.properties = result.data.properties;
                       $scope.accountsNames = result.data.accountsName;
                       $scope.waitingOrdercount = 0;
                       $scope.waitingRequestcount = 0;
                       $scope.newNotfy = 0;
                       $scope.waitingOrdercount = result.data.waitingOrder;
                       $scope.waitingRequestcount = result.data.waitingRequest;
                       $scope.newNotfy = result.data.notfy;
                       //account.selected
                       $scope.propertyTypes = result.data.types;
                       $scope.account.selected = undefined;

                       if (($localStorage.accountselectedcust)) {
                           var accs = $localStorage.accountselectedcust;
                           var findacc = angular.copy($filter('filter')($scope.accountsNames, { accountId: accs.accountId }, true));
                           if (findacc.length) {
                               $scope.account.selected = $localStorage.accountselectedcust;
                               $scope.getaccountInfo($scope.account.selected);
                           }

                       };
                   },
                   function (error) {
                       toastr.error("حدث خطاء - اثناء طلب البيانات");
                   });
        };

        $scope.waitingnotfy = [];
        $scope.getwaiting = function (type) {

            //getwaiting
            AccountspersistenceService.action.getwaiting(JSON.stringify({ accountType: $scope.typetorecive, waitType: type })).then(
                  function (result) {
                      console.log('accountInfo', result);

                      $scope.waitingnotfy = result.data;
                      console.log(JSON.stringify($scope.waitingnotfy))
                      $('#waiting-model').modal('show');

                  },
                  function (error) {

                      toastr.error("يرجي المحاولة في وقت لاحق");
                  });

        };
        $scope.viewrequest = function (orderNo, typeName) {
            if ($scope.waitingnotfy.type === 'request') {
                $scope.requestOrder(typeName);
                $scope.getRequestById(orderNo);
                $('#waiting-model').modal('hide');
            }

            if ($scope.waitingnotfy.type === 'order') {

                $scope.requestOrderMove(typeName);
                $scope.getRequestMoveByOrderId(orderNo);
                $('#waiting-model').modal('hide');
            }

        };

        $scope.attchedWaitRequest = function (orderNo, typeName) {
            //requestNo: data, companyId: bId, isCompany: $scope.branchGroub.isCompany, accountType: $scope.typetorecive, typeName: $scope.orderRequest.typeName 
            if ($scope.waitingnotfy.type === 'request') {
                $scope.requestOrderMove(typeName);
                $scope.getOrderRequestforOrderById(orderNo.toString());
                $scope.orderMoveRequest.requestNo = orderNo;
                $('#waiting-model').modal('hide');
            }

        };

        $scope.addNewProperty = function () {
            //AddaccountCategoryPropery
            AccountspersistenceService.action.AddaccountCategoryPropery(JSON.stringify({ companyId: bId, isCompany: $scope.branchGroub.isCompany, accountType: $scope.typetorecive, accountCategoryPropertiesName: '', typeName: '', accountCategoryPropertiesId: 0 })).then(
         function (result) {
             $scope.properties.push(result.data);
             toastr.info("تم الحفظ");
         },
         function (error) {
             toastr.error("حدث خطاء - اثناء حفظ البيانات");
         });
        };
        $scope.reproperty = {};
        $scope.beforeupdateProperty = function (property) {
            $scope.reproperty = angular.copy(property);
            return true;
        };
        $scope.updateProperty = function (property) {
            console.log('property', property)
            AccountspersistenceService.action.AddaccountCategoryPropery(JSON.stringify({ companyId: bId, isCompany: $scope.branchGroub.isCompany, accountType: $scope.typetorecive, accountCategoryPropertiesName: property.accountCategoryPropertiesName, typeName: property.typeName, accountCategoryPropertiesId: property.accountCategoryPropertiesId })).then(
                 function (result) {
                     property = result.data;
                     toastr.info("تم التحديث");
                 },
                 function (error) {
                     property = $scope.reproperty;
                     toastr.error("حدث خطاء - اثناء تحديث البيانات");
                 });
        };

        $scope.dlProperty = function (property, key) {
            console.log('property', property)
            AccountspersistenceService.action.DelaccountCategoryPropery(JSON.stringify({ companyId: bId, isCompany: $scope.branchGroub.isCompany, accountType: $scope.typetorecive, accountCategoryPropertiesName: property.accountCategoryPropertiesName, typeName: property.typeName, accountCategoryPropertiesId: property.accountCategoryPropertiesId })).then(
                 function (result) {
                     var index = $scope.properties.indexOf(property);
                     $scope.properties.splice(index, 1);
                     toastr.info("تم الحذف");
                 },
                 function (error) {

                     toastr.error("حدث خطاء - تاكد من ان البيانات متاحة للحذف");
                 });
        };
        $scope.selectedaccount = null;
        $scope.getaccountInfo = function (acc) {
            //GetAccountInfo
            console.log('accSelected', acc);
            $scope.selectedaccount = angular.copy(acc);
            console.log('selectedaccount', $scope.selectedaccount);
            AccountspersistenceService.action.GetAccountInfo(JSON.stringify({ companyId: bId, isCompany: $scope.branchGroub.isCompany, accountType: $scope.typetorecive, accountId: acc.accountId })).then(
                 function (result) {
                     console.log('accountInfo', result);
                     $scope.accountInfo = result.data;
                     $scope.getaccountMoves();
                     $localStorage.accountselectedcust = $scope.account.selected;
                 },
                 function (error) {

                     toastr.error("حدث خطاء - تاكد من ان البيانات متاحة للحذف");
                 });

        };

        $scope.addNewPropertyValue = function (prop) {
            console.log('property', prop);
            AccountspersistenceService.action.addPropertyValue(JSON.stringify({ companyId: bId, isCompany: $scope.branchGroub.isCompany, accountType: $scope.typetorecive, accountCategoryPropertiesId: prop.accountCategoryPropertiesId, accountId: $scope.selectedaccount.accountId, value: '', valueId: 0 })).then(
                function (result) {
                    console.log('accountInfo', result);
                    $scope.accountInfo.accountValues.push(result.data);
                },
                function (error) {

                    toastr.error("حدث خطاء - اثناء الحفظ");
                });
        };
        $scope.propvalueretyrn = {};
        $scope.beforeUpdatePropertyValue = function (prop) {

            $scope.propvalueretyrn = angular.copy(prop);
            console.log('property', $scope.propvalueretyrn);
        };

        $scope.updatePropertyValue = function (prop, key) {
            console.log('property', prop);
            AccountspersistenceService.action.addPropertyValue(JSON.stringify({ companyId: bId, isCompany: $scope.branchGroub.isCompany, accountType: $scope.typetorecive, accountCategoryPropertiesId: prop.accountCategoryPropertiesId, accountId: $scope.selectedaccount.accountId, value: prop.valueName, valueId: prop.valueId })).then(
                function (result) {
                    console.log('accountInfo', result);
                    prop = result.data;
                    toastr.info("تم التحديث");
                },
                function (error) {
                    prop = $scope.propvalueretyrn;
                    prop.valueName = $scope.propvalueretyrn.valueName;
                    $scope.accountInfo.accountValues[key] = $scope.propvalueretyrn;
                    toastr.error("حدث خطاء - اثناء التحديث");
                });
        };

        $scope.delPropertyValue = function (prop) {
            console.log('property', prop);
            AccountspersistenceService.action.delPropertyValue(JSON.stringify({ companyId: bId, isCompany: $scope.branchGroub.isCompany, accountType: $scope.typetorecive, accountCategoryPropertiesId: prop.accountCategoryPropertiesId, accountId: $scope.selectedaccount.accountId, value: prop.valueName, valueId: prop.valueId })).then(
                function (result) {
                    var index = $scope.accountInfo.accountValues.indexOf(prop);
                    $scope.accountInfo.accountValues.splice(index, 1);
                },
                function (error) {

                    toastr.error("حدث خطاء - اثناء الحفظ");
                });
        };


        $scope.accountMoves = {};
        $scope.catogeryMove = {};
        $scope.getaccountMoves = function () {
            var id = 0;
            if ($scope.reportChange == 'account') {
                id = $scope.account.selected.accountId;
            };
            if ($scope.branchGroub.id !== 0) {
                AccountspersistenceService.action.GetAccountOrdersd(JSON.stringify({ id: id, pageSize: $scope.itemsperpage, pageNumber: $scope.currentPage, startDate: $scope.startDate, endDate: $scope.endDate, search: $scope.reportChange, orderby: $scope.sortType, orderType: $scope.sortReverse, accountType: $scope.typetorecive, companyId: bId, isCompany: $scope.branchGroub.isCompany })).then(
                      function (orders) {
                          $scope.totalItems = orders.data.totalCount
                          if ($scope.itemsperpage < 10) {
                              $scope.displeWatch = true;
                              $scope.itemsperpage = 10;

                          }
                          $scope.accountMoves = orders.data.orders;
                          $scope.catogeryMove = { sumCrdit: orders.data.sumCrdit, sumDebit: orders.data.sumDebit, sumPBalance: orders.data.sumPBalance, sumBalance: orders.data.sumBalance };
                      },
                      function (error) {
                          toastr.error("تعذر طلب البيانات في الوقت الحالي");
                      });
            }

        };

        $scope.showreques = false;
        $scope.showorder = false;
        $scope.tilteaccount = {};
        $scope.orderRequest = { orderRequestId: 0, requestNo: 0, mainAccount: { accountId: 0, accountName: '' }, tilteAccount: { accountId: 0, accountName: '' }, requestDate: '1\1\2017', typeName: 'pay', dueDate: '1\1\2017', items: [{ orderRequestitemsId: 0, note: '', qty: 1, price: 1, amount: 1, refrenceType: '', refrenceTypeId: 0 }], isNewRequest: true, isToCancel: false, isDone: false, isQty: true };
        $scope.requestOrder = function (type) {
            $scope.getAllAccountsName();
            $scope.orderRequest.mainAccount = $scope.account.selected;
            $scope.orderRequest.typeName = type;
            $scope.orderRequest.dueDate = moment().format('DD/MM/YYYY');
            $scope.orderRequest.requestDate = moment().format('DD/MM/YYYY');
            $scope.orderRequest.tilteAccount = { accountId: 0, accountName: '' };
            $scope.orderRequest.items = [{ orderRequestitemsId: 0, note: '', qty: 1, price: 1, amount: 1, refrenceType: '', refrenceTypeId: 0 }];
            $scope.tilteaccount.selected = undefined;
            $scope.orderRequest.isNewRequest = true;
            $scope.orderRequest.isToCancel = false
            $scope.orderRequest.isDone = false;
            $scope.orderRequest.isQty = true;
            $scope.orderRequest.requestNo = 0;
            $scope.showreques = true;
        };
        $scope.requestNewOrder = function () {
            $scope.requestNo = 0
            $scope.orderRequest.dueDate = moment().format('DD/MM/YYYY');
            $scope.orderRequest.requestDate = moment().format('DD/MM/YYYY');
            $scope.orderRequest.tilteAccount = { accountId: 0, accountName: '' };
            $scope.orderRequest.items = [{ orderRequestitemsId: 0, note: '', qty: 1, price: 1, amount: 1, refrenceType: '', refrenceTypeId: 0 }];
            $scope.tilteaccount.selected = undefined;
            $scope.orderRequest.isNewRequest = true;
            $scope.orderRequest.isToCancel = false
            $scope.orderRequest.isDone = false;
            $scope.orderRequest.isQty = true;
            $scope.orderRequest.requestNo = 0;
            $scope.showreques = true;
        };
        $scope.isNewRequest = true;
        $scope.addNewitemtoRequest = function () {
            if ($scope.orderRequest !== null && $scope.orderRequest.items.length > 0) {
                for (var i = 0; i < $scope.orderRequest.items.length; i++) {
                    if ($scope.orderRequest.items[i].qty <= 0 || $scope.orderRequest.items[i].price <= 0 || !$scope.orderRequest.items[i].note) {
                        return;
                    }

                }
            }
            $scope.orderRequest.items.push({ orderRequestitemsId: 0, note: '', qty: 1, price: 1, amount: 1, refrenceType: '', refrenceTypeId: 0 });
        };
        $scope.saveRequest = function () {
            console.log(angular.toJson($scope.orderRequest));
            //setOrderRequest
            var orderToSave = angular.copy($scope.orderRequest);
            orderToSave.companyId = bId;
            orderToSave.requestDate = moment.utc();
            orderToSave.isCompany = $scope.branchGroub.isCompany;
            orderToSave.accountType = $scope.typetorecive;

            if ($scope.branchGroub.id !== 0) {
                AccountspersistenceService.action.setOrderRequest(angular.toJson(orderToSave)).then(
                      function (orders) {
                          toastr.info("تم الحفظ");
                          $scope.orderRequest.requestNo = orders.data.requestNo;
                          $scope.orderRequest.userName = orders.data.userName;

                      },
                      function (error) {
                          toastr.error("لا يمكن حفظ الطلب حاليا");
                      });
            }
        };
        $scope.cancelRequest = function () {
            console.log(angular.toJson($scope.orderRequest));
            //setOrderRequest

            var orderToSave = angular.copy($scope.orderRequest);
            orderToSave.companyId = bId;
            orderToSave.isToCancel = true;
            orderToSave.isCompany = $scope.branchGroub.isCompany;
            orderToSave.accountType = $scope.typetorecive;

            if ($scope.branchGroub.id !== 0) {
                AccountspersistenceService.action.setOrderRequest(angular.toJson(orderToSave)).then(
                      function (orders) {
                          $scope.orderRequest.requestNo = orders.data.requestNo;
                          $scope.orderRequest.userName = orders.data.userName;
                          $scope.orderRequest.isNewRequest = false;
                          $scope.orderRequest.isToCancel = true;
                          toastr.info("تم الغاء الطلب");
                      },
                      function (error) {
                          toastr.error("لا يمكن الغاء الطلب حاليا");
                      });
            }
        };

        $scope.getRequestById = function (data) {
            console.log(data);
            if ($scope.branchGroub.id !== 0) {
                AccountspersistenceService.action.getOrderRequestById(JSON.stringify({ requestNo: data, companyId: bId, isCompany: $scope.branchGroub.isCompany, accountType: $scope.typetorecive, typeName: $scope.orderRequest.typeName })).then(
                      function (orders) {
                          console.log(orders);
                          $scope.tilteaccount.selected = orders.data.tilteAccount;
                          $scope.account.selected = orders.data.mainAccount;
                          $scope.orderRequest = orders.data;
                          $scope.orderRequest.dueDate = moment(orders.data.dueDate).format('DD/MM/YYYY');

                      },
                      function (error) {
                          toastr.error("الطلب غير متاح حاليا");
                      });
            }
        };
        $scope.requestTotal = function () {
            if ($scope.showreques) {
                var total = 0;
                if ($scope.orderRequest.isQty) {
                    if ($scope.orderRequest !== null && $scope.orderRequest.items.length > 0) {
                        for (var i = 0; i < $scope.orderRequest.items.length; i++) {
                            total += $scope.orderRequest.items[i].qty * $scope.orderRequest.items[i].price;
                            $scope.orderRequest.items[i].amount = $scope.orderRequest.items[i].qty * $scope.orderRequest.items[i].price;
                        }
                    }
                } else {
                    if ($scope.orderRequest !== null && $scope.orderRequest.items.length > 0) {
                        for (var i = 0; i < $scope.orderRequest.items.length; i++) {
                            total += $scope.orderRequest.items[i].amount;
                            $scope.orderRequest.items[i].price = $scope.orderRequest.items[i].amount;
                            $scope.orderRequest.items[i].qty = 1;
                        }
                    }
                }


                return total;
            }

        };
        $scope.titleAccounts = [];
        $scope.getAllAccountsName = function () {
            if ($scope.branchGroub.id !== 0) {
                AccountspersistenceService.action.getacNames(JSON.stringify({ companyId: bId, isCompany: $scope.branchGroub.isCompany, accountType: $scope.typetorecive })).then(
                      function (result) {
                          $scope.titleAccounts = result.data;
                      },
                      function (error) {

                          toastr.error("حدث خطاء - اثناء طلب البيانات");
                      });
            }
        };

        $scope.gettilteaccount = function (acc) {
            console.log('selected titlt', acc);
            $scope.orderRequest.tilteAccount = acc;
        };

        $scope.requestVaild = function () {
            if ($scope.showreques) {
                for (var i = 0; i < $scope.orderRequest.items.length; i++) {
                    if ($scope.orderRequest.items[i].qty <= 0 || $scope.orderRequest.items[i].price <= 0 || $scope.orderRequest.items[i].amount <= 0 || !$scope.orderRequest.items[i].note) {

                        return false;
                    }

                }
                //$scope.orderRequest = { requestNo: 0, tilteAccount: { accountId: 0, accountName: '' }, requestDate: '1\1\2017', typeName: 'pay', dueDate: '1\1\2017', items: [{ note: '', qty: 1, price: 1, amount: 1 }], isNewRequest: true, isToCancel: true, isDone: false, isQty: true };
                if (!$scope.orderRequest.tilteAccount.accountName || $scope.orderRequest.isDone || !$scope.account.selected.accountName) {
                    return false;
                }
                return true;
            }

            return false;
        }


        //starting OrderRequestMove
        $scope.showrequesMove = false;
        $scope.showorder = false;
        //$scope.orderMoveRequest = { orderMoveRequestId: 0, requestTypeName: '', requestNo: 0, mainAccount: { accountId: 0, accountName: '', code: '' }, tilteAccount: { accountId: 0, accountName: '', code: '' }, orderDate: '1\1\2017', typeName: 'pay', isNewRequest: true, isToCancel: false, isDone: false, OrderNo: 0, attchedRequest: false, orderRequest: {}, note: '', amount: 0 };
        $scope.orderMoveRequest = { // OrderRequestMoveVm
            "orderMoveRequestId": 0,
            "orderNo": 0,
            "mainAccount": null,
            "tilteAccount": null,
            "orderDate": "0001-01-01T00:00:00",
            "typeName": null,
            "isNewRequest": true,
            "isToCancel": false,
            "isDone": false,
            "attchedRequest": false,
            "requestNo": 0,
            "orderRequest": null,
            "note": null,
            "amount": 0.0,
            "companyId": 0,
            "isCompany": false,
            "accountType": null,
            "userName": null,
            "doneDate": "0001-01-01T00:00:00",
            "requestTypeName": null
        }
        $scope.requestOrderMove = function (type) {
            $scope.getAllAccountsName();
            $scope.orderMoveRequest = { // OrderRequestMoveVm
                "orderMoveRequestId": 0,
                "orderNo": 0,
                "mainAccount": null,
                "tilteAccount": null,
                "orderDate": "0001-01-01T00:00:00",
                "typeName": type,
                "isNewRequest": true,
                "isToCancel": false,
                "isDone": false,
                "attchedRequest": false,
                "requestNo": 0,
                "orderRequest": null,
                "note": null,
                "amount": 0.0,
                "companyId": 0,
                "isCompany": false,
                "accountType": null,
                "userName": null,
                "doneDate": "0001-01-01T00:00:00",
                "requestTypeName": null

            }
            $scope.orderMoveRequest.mainAccount = $scope.account.selected;

            $scope.orderMoveRequest.orderDate = moment().format('DD/MM/YYYY');
            $scope.orderMoveRequest.tilteAccount = { accountId: 0, accountName: '', code: '' };


            $scope.tilteaccount.selected = undefined;
            $scope.showrequesMove = true;
        };
        $scope.requestNewOrderMove = function () {
            var type = angular.copy($scope.orderMoveRequest.type);
            $scope.orderMoveRequest = { // OrderRequestMoveVm
                "orderMoveRequestId": 0,
                "orderNo": 0,
                "mainAccount": null,
                "tilteAccount": null,
                "orderDate": "0001-01-01T00:00:00",
                "typeName": type,
                "isNewRequest": true,
                "isToCancel": false,
                "isDone": false,
                "attchedRequest": false,
                "requestNo": 0,
                "orderRequest": null,
                "note": null,
                "amount": 0.0,
                "companyId": 0,
                "isCompany": false,
                "accountType": null,
                "userName": null,
                "doneDate": "0001-01-01T00:00:00",
                "requestTypeName": null

            }
            $scope.orderMoveRequest.mainAccount = $scope.account.selected;

            $scope.orderMoveRequest.orderDate = moment().format('DD/MM/YYYY');
            $scope.orderMoveRequest.tilteAccount = { accountId: 0, accountName: '', code: '' };


            $scope.tilteaccount.selected = undefined;


            $scope.requestNo = 0
            $scope.orderMoveRequest.orderDate = moment().format('DD/MM/YYYY');
            $scope.orderMoveRequest.tilteAccount = { accountId: 0, accountName: '', code: '' };
            $scope.orderMoveRequest.orderRequest = {};
            $scope.tilteaccount.selected = undefined;
            $scope.orderMoveRequest.isNewRequest = true;
            $scope.orderMoveRequest.isToCancel = false
            $scope.orderMoveRequest.isDone = false;
            $scope.orderMoveRequest.requestNo = 0;
        };

        $scope.addNewitemtoRequestMove = function () {
            if ($scope.orderMoveRequest !== null && $scope.orderMoveRequest.orderRequest != null && $scope.orderMoveRequest.orderRequest.items.length > 0) {
                for (var i = 0; i < $scope.orderMoveRequest.orderRequest.items.length; i++) {
                    if ($scope.orderMoveRequest.orderRequest.items[i].qty <= 0 || $scope.orderMoveRequest.orderRequest.items[i].price <= 0 || !$scope.orderMoveRequest.orderRequest.items[i].note) {
                        return;
                    }

                }
            }
            $scope.orderMoveRequest.orderRequest.items.push({ orderMoveRequestitemsId: 0, note: '', qty: 1, price: 1, amount: 1, refrenceType: '', refrenceTypeId: 0 });
        };
        $scope.saveRequestMove = function () {
            console.log(angular.toJson($scope.orderMoveRequest));
            //setorderMoveRequest
            var orderToSave = angular.copy($scope.orderMoveRequest);
            orderToSave.companyId = bId;
            orderToSave.isCompany = $scope.branchGroub.isCompany;
            orderToSave.requestDate = moment.utc();
            orderToSave.accountType = $scope.typetorecive;
            console.log(angular.toJson(orderToSave));
            if ($scope.branchGroub.id !== 0) {
                AccountspersistenceService.action.setOrderMoveRequest(angular.toJson(orderToSave)).then(
                      function (orders) {
                          $scope.orderMoveRequest = orders.data;
                          toastr.info("تم الحفظ");
                      },
                      function (error) {
                          toastr.error("تعذر الحفظ في الوقت الحالي");

                      });
            }
        };
        $scope.cancelRequestMove = function () {
            console.log(angular.toJson($scope.orderMoveRequest));
            //setorderMoveRequest

            var orderToSave = angular.copy($scope.orderMoveRequest);
            orderToSave.companyId = bId;
            orderToSave.isToCancel = true;
            orderToSave.isCompany = $scope.branchGroub.isCompany;
            orderToSave.accountType = $scope.typetorecive;

            if ($scope.branchGroub.id !== 0) {
                AccountspersistenceService.action.setOrderMoveRequest(angular.toJson(orderToSave)).then(
                      function (orders) {
                          $scope.orderMoveRequest = orders.data
                          toastr.info("تم الالغاء");
                      },
                      function (error) {
                          toastr.error("لا يمكن الغاء الاذن في الوقت الحالي");
                      });
            }
        };
        $scope.confirmRequestMove = function () {
            console.log(angular.toJson($scope.orderMoveRequest));
            //setorderMoveRequest

            var orderToSave = angular.copy($scope.orderMoveRequest);
            orderToSave.companyId = bId;
            orderToSave.isDone = true;
            orderToSave.isCompany = $scope.branchGroub.isCompany;
            orderToSave.accountType = $scope.typetorecive;

            if ($scope.branchGroub.id !== 0) {
                AccountspersistenceService.action.setOrderMoveRequest(angular.toJson(orderToSave)).then(
                      function (orders) {
                          $scope.orderMoveRequest = orders.data
                          toastr.info("تم التاكيد");
                      },
                      function (error) {
                          toastr.error("تعذز تاكيد البيانات في الوقت الحالي");
                      });
            }
        };

        $scope.getRequestMoveByOrderId = function (data) {
            console.log(data);
            $scope.orderMoveRequest.attchedRequest = true;
            if ($scope.branchGroub.id !== 0) {
                AccountspersistenceService.action.getOrderMoveById(JSON.stringify({ requestNo: data, companyId: bId, isCompany: $scope.branchGroub.isCompany, accountType: $scope.typetorecive, typeName: $scope.orderMoveRequest.typeName })).then(
                      function (orders) {
                          console.log(orders);
                          $scope.tilteaccount.selected = orders.data.tilteAccount;
                          $scope.account.selected = orders.data.mainAccount;
                          $scope.orderMoveRequest = angular.copy(orders.data);
                          $scope.orderMoveRequest.dueDate = moment(orders.data.dueDate).format('DD/MM/YYYY');

                      },
                      function (error) {

                          toastr.error("الاذن غير مسجل او غير متاح لهذة العملية");
                          var type = angular.copy($scope.orderMoveRequest.type);
                          $scope.orderMoveRequest = { // OrderRequestMoveVm
                              "orderMoveRequestId": 0,
                              "orderNo": 0,
                              "mainAccount": null,
                              "tilteAccount": null,
                              "orderDate": "0001-01-01T00:00:00",
                              "typeName": type,
                              "isNewRequest": true,
                              "isToCancel": false,
                              "isDone": false,
                              "attchedRequest": false,
                              "requestNo": 0,
                              "orderRequest": null,
                              "note": null,
                              "amount": 0.0,
                              "companyId": 0,
                              "isCompany": false,
                              "accountType": null,
                              "userName": null,
                              "doneDate": "0001-01-01T00:00:00",
                              "requestTypeName": null

                          }
                          $scope.orderMoveRequest.mainAccount = $scope.account.selected;

                          $scope.orderMoveRequest.orderDate = moment().format('DD/MM/YYYY');
                          $scope.orderMoveRequest.tilteAccount = { accountId: 0, accountName: '', code: '' };


                          $scope.tilteaccount.selected = undefined;


                          $scope.requestNo = 0
                          $scope.orderMoveRequest.orderDate = moment().format('DD/MM/YYYY');
                          $scope.orderMoveRequest.tilteAccount = { accountId: 0, accountName: '', code: '' };
                          $scope.orderMoveRequest.orderRequest = {};
                          $scope.tilteaccount.selected = undefined;
                          $scope.orderMoveRequest.isNewRequest = true;
                          $scope.orderMoveRequest.isToCancel = false
                          $scope.orderMoveRequest.isDone = false;
                          $scope.orderMoveRequest.requestNo = 0;
                      });
            }
        };
        $scope.getOrderRequestforOrderById = function (data) {

            var partsOfStr = data.split(',');
            var requestTypeName = '';
            if (partsOfStr.length > 1) {
                requestTypeName = partsOfStr[0];
                data = parseInt(partsOfStr[1])
            }
            console.log(partsOfStr);
            if ($scope.branchGroub.id !== 0) {
                $scope.orderMoveRequest.attchedRequest = true;
                AccountspersistenceService.action.getOrderRequestById(JSON.stringify({ requestNo: data, companyId: bId, isCompany: $scope.branchGroub.isCompany, accountType: $scope.typetorecive, typeName: $scope.orderMoveRequest.typeName, requestTypeName: requestTypeName })).then(
                      function (orders) {
                          console.log(orders);
                          $scope.orderMoveRequest.tilteAccount = orders.data.tilteAccount;
                          $scope.tilteaccount.selected = orders.data.tilteAccount;
                          $scope.account.selected = orders.data.mainAccount;
                          $scope.orderMoveRequest.orderRequest = angular.copy(orders.data);

                          $scope.orderMoveRequest.orderRequest.requestDate = moment(orders.data.requestDate).format('DD/MM/YYYY');

                      },
                      function (error) {
                          $scope.orderMoveRequest.attchedRequest = false;
                          toastr.error("الطلب غير مسجل او غير متاح لهذه العملية");

                      });
            }
        };
        $scope.requestTotalMove = function () {
            if ($scope.showrequesMove && !_.isEmpty($scope.orderMoveRequest.orderRequest)) {
                var total = 0;
                if ($scope.orderMoveRequest.orderRequest.isQty) {
                    if ($scope.orderMoveRequest.orderRequest !== null && $scope.orderMoveRequest.orderRequest.items.length > 0) {
                        for (var i = 0; i < $scope.orderMoveRequest.orderRequest.items.length; i++) {
                            total += $scope.orderMoveRequest.orderRequest.items[i].qty * $scope.orderMoveRequest.orderRequest.items[i].price;
                            $scope.orderMoveRequest.orderRequest.items[i].amount = $scope.orderMoveRequest.orderRequest.items[i].qty * $scope.orderMoveRequest.orderRequest.items[i].price;
                        }
                    }
                } else {
                    if ($scope.orderMoveRequest !== null && $scope.orderMoveRequest.orderRequest !== null && $scope.orderMoveRequest.orderRequest.items.length > 0) {
                        for (var i = 0; i < $scope.orderMoveRequest.orderRequest.items.length; i++) {
                            total += $scope.orderMoveRequest.orderRequest.items[i].amount;
                            $scope.orderMoveRequest.orderRequest.items[i].price = $scope.orderMoveRequest.orderRequest.items[i].amount;
                            $scope.orderMoveRequest.orderRequest.items[i].qty = 1;
                        }
                    }
                }
                if ($scope.orderMoveRequest.attchedRequest) {
                    $scope.orderMoveRequest.amount = total;
                }

                return total;
            } else {
                return $scope.orderMoveRequest.amount;
            }

        };


        $scope.gettilteaccountMove = function (acc) {

            $scope.orderMoveRequest.tilteAccount = acc;
        };

        $scope.$watch('orderMoveRequest.attchedRequest', function (newVal, oldVal) {

            if (newVal !== oldVal) {
                if (newVal) {

                    $scope.orderMoveRequest.orderRequest = { // OrderRequesVm
                        "orderRequestId": 0,
                        "requestNo": 0,
                        "mainAccount": null,
                        "tilteAccount": null,
                        "requestDate": moment().format('DD/MM/YYYY'),
                        "dueDate": moment().format('DD/MM/YYYY'),
                        "typeName": null,
                        "items": [{ // OrderRequestitemsVm
                            "orderRequestitemsId": 0,
                            "note": null,
                            "qty": 0.0,
                            "price": 0.0,
                            "amount": 0.0,
                            "refrenceType": null,
                            "refrenceTypeId": 0
                        }],
                        "isDone": false,
                        "isToCancel": false,
                        "isQty": false,
                        "isNewRequest": true,
                        "companyId": 0,
                        "isCompany": false,
                        "accountType": null,
                        "userName": null
                    };
                } else {
                    $scope.orderMoveRequest.orderRequest = {};
                    $scope.requestNo = 0;
                }
                console.log('selected attchedRequest', newVal + '- old:' + oldVal)
            }
        });

        $scope.requestMoveVaild = function () {
            if ($scope.showrequesMove) {
                if (!_.isEmpty($scope.orderMoveRequest.orderRequest)) {
                    for (var i = 0; i < $scope.orderMoveRequest.orderRequest.items.length; i++) {
                        if ($scope.orderMoveRequest.orderRequest.items[i].qty <= 0 || $scope.orderMoveRequest.orderRequest.items[i].price <= 0 || $scope.orderMoveRequest.orderRequest.items[i].amount <= 0 || !$scope.orderMoveRequest.orderRequest.items[i].note) {

                            return false;
                        }

                    }
                }

                if (!$scope.orderMoveRequest.tilteAccount.accountName || !$scope.account.selected.accountName || !$scope.orderMoveRequest.note || $scope.orderMoveRequest.amount <= 0) {

                    return false;
                }
                return true;
            }

            return false;
        }
        //End Order Request 
        var checkbrGroup = function () {
            if ($scope.branchGroub && $scope.branchGroub.id != '0') {
                bId = parseInt(angular.copy(parseInt($scope.branchGroub.id)));
                if ($scope.reportChange === 'account') {
                    if ($scope.branchGroub.id !== '0') {
                        $scope.loadAccounts();
                    }

                }
            }
        }();
    });

}());


