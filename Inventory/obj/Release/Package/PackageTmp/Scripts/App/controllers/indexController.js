﻿'use strict';
inventoryModule.controller('indexController', ['$scope', '$location', 'authService' , '$http', '$q', function ($scope, $location, authService, $http, $q) {

    $scope.logOut = function () {
        authService.logOut();
        $location.path('/index.html#/login');
    }

    $scope.authentication = authService.authentication;
    
    
}]);

