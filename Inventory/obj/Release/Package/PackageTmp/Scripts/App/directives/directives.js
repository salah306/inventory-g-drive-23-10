/***
GLobal Directives
***/
inventoryModule.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
});
// Route State Load Spinner(used on page or content load)
inventoryModule.directive('ngSpinnerBar', ['$rootScope',
    function($rootScope) {
        return {
            link: function(scope, element, attrs) {
                // by defult hide the spinner bar
                element.addClass('hide'); // hide spinner bar by default

                // display the spinner bar whenever the route changes(the content part started loading)
                $rootScope.$on('$stateChangeStart', function() {
                    element.removeClass('hide'); // show spinner bar
                });

                // hide the spinner bar on rounte change success(after the content loaded)
                $rootScope.$on('$stateChangeSuccess', function() {
                    element.addClass('hide'); // hide spinner bar
                    $('body').removeClass('page-on-load'); // remove page loading indicator
                    Layout.setSidebarMenuActiveLink('match'); // activate selected link in the sidebar menu
                   
                    // auto scorll to page top
                    setTimeout(function () {
                        App.scrollTop(); // scroll to the top on content load
                    }, $rootScope.settings.layout.pageAutoScrollOnLoad);     
                });

                // handle errors
                $rootScope.$on('$stateNotFound', function() {
                    element.addClass('hide'); // hide spinner bar
                });

                // handle errors
                $rootScope.$on('$stateChangeError', function() {
                    element.addClass('hide'); // hide spinner bar
                });
            }
        };
    }
])
inventoryModule.directive('input', function () {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ngModel) {
            if (attrs.type == 'number') {
                ngModel.$formatters.push(function (value) {
                    return parseFloat(value);
                });
            }
        }
    };
});

  inventoryModule.directive('contenteditable', function () {
      return {
          restrict: 'A', // only activate on element attribute
          require: '?ngModel', // get a hold of NgModelController
          link: function (scope, element, attrs, ngModel) {
              if (!ngModel) return; // do nothing if no ng-model

              // Specify how UI should be updated
              ngModel.$render = function () {
                  element.html(ngModel.$viewValue || '');
              };

              // Listen for change events to enable binding
              element.on('blur keyup change', function () {
                  scope.$apply(readViewText);
              });

              // No need to initialize, AngularJS will initialize the text based on ng-model attribute

              // Write data to the model
              function readViewText() {
                  var html = element.html();
                  // When we clear the content editable the browser leaves a <br> behind
                  // If strip-br attribute is provided then we strip this out
                  if (attrs.stripBr && html == '<br>') {
                      html = '';
                  }
                  ngModel.$setViewValue(html);
              }
          }
      };
  });
inventoryModule.directive('slideable', function () {
    return {
        restrict: 'C',
        compile: function (element, attr) {
            // wrap tag
            var contents = element.html();
            element.html('<div class="slideable_content" style="margin:0 !important; padding:0 !important" >' + contents + '</div>');

            return function postLink(scope, element, attrs) {
                // default properties
                attrs.duration = (!attrs.duration) ? '1s' : attrs.duration;
                attrs.easing = (!attrs.easing) ? 'ease-in-out' : attrs.easing;
                element.css({
                    'overflow': 'hidden',
                    'height': '0px',
                    'transitionProperty': 'height',
                    'transitionDuration': attrs.duration,
                    'transitionTimingFunction': attrs.easing
                });
            };
        }
    };
})
.directive('slideToggle', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var target, content;

            attrs.expanded = false;

            element.bind('click', function () {
                if (!target) target = document.querySelector(attrs.slideToggle);
                if (!content) content = target.querySelector('.slideable_content');

                if (!attrs.expanded) {
                    content.style.border = '1px solid rgba(0,0,0,0)';
                    var y = content.clientHeight;
                    content.style.border = 0;
                    target.style.height = y + 'px';
                } else {
                    target.style.height = '0px';
                }
                attrs.expanded = !attrs.expanded;
            });
        }
    }
});


// Handle global LINK click
inventoryModule.directive('a', function() {
    return {
        restrict: 'E',
        link: function(scope, elem, attrs) {
            if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                elem.on('click', function(e) {
                    e.preventDefault(); // prevent link click for above criteria
                });
            }
        }
    };
});

// Handle Dropdown Hover Plugin Integration
inventoryModule.directive('dropdownMenuHover', function () {
  return {
    link: function (scope, elem) {
      elem.dropdownHover();
    }
  };  
});

//Click to Edit

inventoryModule.directive('clickToEdit', function($timeout) {
    return {
        require: 'ngModel',
        scope: {
            model: '=ngModel',
            type: '@type'
        },
        objectToInject: '=',
        replace: true,
        transclude: false,
        // includes our template
        template:
            '<div class="templateRoot">'+
                '<div class="hover-edit-trigger"  data-toggle="tooltip"  title="click to edit">' +
                    '<div class="hover-text-field" ng-show="!editState" ng-click="toggle()">{{model}}<div class="edit-pencil glyphicon glyphicon-pencil"></div></div>' +
                    '<input class="inputText text-primary" type="text" ng-model="localModel"  ng-enter="save()" ng-show="editState && type == \'inputText\'" />' +
                '</div>'+
                '<div class="edit-button-group pull-left" ng-show="editState">'+
                    '<div class="glyphicon glyphicon-ok"  ng-click="save()"></div>'+
                    '<div class="glyphicon glyphicon-remove" ng-click="cancel()"></div>'+
                '</div>'+
            '</div>',
        link: function (scope, element, attrs) {
            scope.editState = false;

            // make a local ref so we can back out changes, this only happens once and persists
            scope.localModel = scope.model;

            // apply the changes to the real model
            scope.save = function(){
                scope.model = scope.localModel;
                console.log("Hi From Directive save")
                scope.toggle();
            };

            // don't apply changes
            scope.cancel = function(){
                scope.localModel = scope.model;
                scope.toggle();
            }

            /*
             * toggles the editState of our field
             */
            scope.toggle = function () {

                scope.editState = !scope.editState;

                /*
                 * a little hackish - find the "type" by class query
                 *
                 */
                var x1 = element[0].querySelector("."+scope.type);

                /*
                 * could not figure out how to focus on the text field, needed $timout
                 * http://stackoverflow.com/questions/14833326/how-to-set-focus-on-input-field-in-angularjs
                 */
                $timeout(function(){
                    // focus if in edit, blur if not. some IE will leave cursor without the blur
                    scope.editState ? x1.focus() : x1.blur();
                }, 0);
            }

            //$scope.$watch('objectToInject' , function (value) {
            //    /*Checking if the given value is not undefined*/
            //    if (value) {
            //        $scope.Obj = value;
            //        /*Injecting the Method*/
            //        $scope.Obj.invoke = function () {
            //            //Do something
            //           console.log("Hi From Directive")
            //        }
            //    }
            //});

        }
    }
});
/*
 * seriously i would have never thought of this on my own, i don't think in directives yet
 * http://stackoverflow.com/questions/17470790/how-to-use-a-keypress-event-in-angularjs
 */
inventoryModule.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }

        });
    };
});

inventoryModule.directive('errSrc', function () {
    return {
        link: function (scope, element, attrs) {
            element.bind('error', function () {
                if (attrs.src != attrs.errSrc) {
                    attrs.$set('src', attrs.errSrc);
                }
            });
        }
    }
})

inventoryModule.directive('multiSelect', ['$q', '$parse', function ($q, $parse) {

    function appendSelected(entities) {
        var newEntities = [];
        angular.forEach(entities, function (entity) {
            var appended = entity;
            appended.selected = false;
            newEntities.push(appended);
        });
        return newEntities;
    }

    return {
        restrict: 'E',
        require: 'ngModel',
        scope: {
            selectedLabel: "@",
            availableLabel: "@",
            available: "=",
            model: "=ngModel",
            config: "="
        },
        templateUrl: "../Scripts/multi-select/multi-select.html",
        link: function (scope, elm, attrs, controllers) {
            scope.selected = {
                available: [],
                current: []
            };

            /* Filters out items in original that are also in toFilter. Compares by reference. */
            function filterOut(original, toFilter) {
                var filtered = [];
                angular.forEach(original, function (entity) {
                    var match = false;
                    for (var i = 0; i < toFilter.length; i++) {
                        if (scope.renderItem(toFilter[i]) == scope.renderItem(entity)) {
                            match = true;
                            break;
                        }
                    }
                    if (!match) {
                        filtered.push(entity);
                    }
                });
                return filtered;
            }

            function parseExpression(item, expr) {

                if (expr) {
                    var displayComponents = expr.match(/(.+)\s+as\s+(.+)/);
                    var ctx = {};
                    ctx[displayComponents[1]] = item;
                    return $parse(displayComponents[2])(ctx);
                }

            }

            var requiredMin, inputModel;
            function ensureMinSelected() {
                if (requiredMin && scope.model) {
                    scope.numSelected = scope.model.length;
                    inputModel.$setValidity('min', scope.numSelected >= requiredMin);
                }
            }

            scope.refreshAvailable = function () {
                if (scope.model && scope.available) {
                    scope.available = filterOut(scope.available, scope.model);
                    scope.selected.available = appendSelected(scope.available);
                    scope.selected.current = appendSelected(scope.model);
                }
            };

            scope.add = function () {
                if (!scope.model.length)
                    scope.model = [];
                scope.model = scope.model.concat(scope.selected(scope.selected.available));
            };
            scope.remove = function () {
                var selected = scope.selected(scope.selected.current);
                scope.available = scope.available.concat(selected);
                scope.model = filterOut(scope.model, selected);
            };
            scope.selected = function (list) {
                var found = [];
                angular.forEach(list, function (item) { if (item.selected === true) found.push(item); });
                return found;
            };

            //Watching the model, updating if the model is a resolved promise
            scope.watchModel = function () {
                if (scope.model && scope.model.hasOwnProperty('$promise') && !scope.model.$resolved) {
                    scope.model.then(function (results) {
                        scope.$watch('model', scope.watchModel);
                    });
                }
                else {
                    scope.refreshAvailable();
                    scope.$watch('model', scope.refreshAvailable);
                }
            };

            //Watching the list of available items. Updating if it is a resolved promise, and refreshing the 
            //available list if the list has changed
            var _oldAvailable = {};
            scope.watchAvailable = function () {
                if (scope.available && scope.available.hasOwnProperty('$promise') && !scope.available.$resolved) {
                    scope.available.$promise.then(function (results) {
                        scope.$watch('available', scope.watchAvailable);
                    });
                }
                else {
                    //We only want to refresh the list if the list of available items has changed
                    //and the variable is defined
                    if (scope.available && scope.available != _oldAvailable) {
                        scope.refreshAvailable();
                        _oldAvailable = scope.available;
                    }
                }
            };

            scope.$watch("available", scope.watchAvailable);
            scope.$watch("model", scope.watchModel);

            scope.renderItem = function (item) {
                return parseExpression(item, attrs.display);
            };

            scope.renderTitle = function (item) {
                if (attrs.title) {
                    return parseExpression(item, attrs.title);
                }
                return "";
            };

            if (scope.config && angular.isDefined(scope.config.requiredMin)) {
                var inputs = elm.find("input");
                var validationInput = angular.element(inputs[inputs.length - 1]);
                inputModel = validationInput.controller('ngModel');
            }

            scope.$watch('config.requiredMin', function (value) {
                if (angular.isDefined(value)) {
                    requiredMin = parseInt(value, 10);
                    ensureMinSelected();
                }
            });

            scope.$watch('model', function (selected) {
                ensureMinSelected();
            });
        }
    };
}]);




