﻿(function () {

    'use strict';

    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    };


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.factory('AccountCategoryPropertiesValuesremotePersistenceStrategy', function ($http, $q) {
        var svc = {

            save: function (AccountCategoryPropertiesValue) {
                var it = AccountCategoryPropertiesValue;
                var deferred = $q.defer();
                if (isJson(AccountCategoryPropertiesValue))
                {
                    it = JSON.parse(AccountCategoryPropertiesValue);
                    return $q.all(it.map(function (it) {
                        $http.post('/api/AccountCategoryPropertiesValues', it).then(deferred.resolve, deferred.reject);
                    }));
                } else {

                    $http.post('/api/AccountCategoryPropertiesValues', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },


            getAll: function () {
                var deferred = $q.defer();

                $http.get('/api/AccountCategoryPropertiesValues')
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },

            getById: function (name) {
                var deferred = $q.defer();

                $http.get('/api/AccountCategoryPropertiesValues/AccountCategoryPropertiesValuesById/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            exists: function (name) {

                var deferred = $q.defer();

                svc.getById(name).then(function (AccountCategoryPropertiesValue) {
                    deferred.resolve(AccountCategoryPropertiesValue.id === name);
                }, deferred.reject);

                return deferred.promise;
            },

            'Delete': function (name) {
                var deferred = $q.defer();

                $http.delete('/api/AccountCategoryPropertiesValues/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            }
        };
        return svc;
    });

    inventoryModule.factory('AccountCategoryPropertiesValueslocalPersistenceStrategy',
        function ($q, localDBService, nullAccountCategoryPropertiesValue, dbModel) {

            var svc = {

                dbModel: dbModel,

                localDBService: localDBService,

                getById: function (name) {
                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getById(svc.dbModel.objectStoreName, name)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                exists: function (name) {

                    var deferred = $q.defer();

                    svc.getById(name).then(function (AccountCategoryPropertiesValue) {
                        deferred.resolve(AccountCategoryPropertiesValue.id === name);
                    }, deferred.reject);

                    return deferred.promise;
                },

                save: function (AccountCategoryPropertiesValue) {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function (e) {

                        var name = AccountCategoryPropertiesValue.name;

                        if (name === null || name === undefined) {
                            localDBService.insert(svc.dbModel.objectStoreName, AccountCategoryPropertiesValue, 'id')
                                          .then(deferred.resolve,
                                                deferred.reject);
                        } else {
                            svc.exists(name).then(function (doesExist) {

                                if (doesExist) {
                                    localDBService.update(svc.dbModel.objectStoreName, AccountCategoryPropertiesValue, name)
                                                  .then(deferred.resolve,
                                                        deferred.reject);;
                                } else {
                                    localDBService.insert(svc.dbModel.objectStoreName, AccountCategoryPropertiesValue, 'id')
                                                  .then(deferred.resolve,
                                                        deferred.reject);
                                }

                            }, deferred.reject);
                        }

                    }, deferred.reject);

                    return deferred.promise;
                },

                getAll: function () {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getAll(svc.dbModel.objectStoreName)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                'Delete': function (name) {

                    var deferred = $q.defer();

                    localDBService.delete(svc.dbModel.objectStoreName, name)
                                  .then(deferred.resolve,
                                        deferred.reject);

                    return deferred.promise;
                }

            };
           
            return svc;
        });

}());