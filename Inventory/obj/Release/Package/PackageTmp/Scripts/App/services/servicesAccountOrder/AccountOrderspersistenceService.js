﻿(function () {

    'use strict';

    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.service('AccountOrderspersistenceService',
        function ($q, Offline, AccountOrdersremotePersistenceStrategy, AccountOrderslocalPersistenceStrategy) {

            var self = this;

            self.persistenceType = 'remote';

            self.action = AccountOrdersremotePersistenceStrategy;

            Offline.on('confirmed-down', function () {
                self.action = AccountOrderslocalPersistenceStrategy;
                self.persistenceType = 'local';
            });

            Offline.on('confirmed-up', function () {
                self.action = AccountOrdersremotePersistenceStrategy;
                self.persistenceType = 'remote';
            });

            self.getRemoteAccountOrder = function (name) {
                return AccountOrdersremotePersistenceStrategy.getById(name);
            };

            self.getLocalAccountOrder = function (name) {
                return AccountOrderslocalPersistenceStrategy.getById(name);
            };

            self.getById = function (name) {

                var deferred = $q.defer();

                if (Offline.state = 'up') {

                    var
                        remoteAccountOrder = {},
                        localAccountOrder = {};

                    self.getRemoteAccountOrder(name).then(function (rAccountOrder) {

                        remoteAccountOrder = rAccountOrder;

                        self.getLocalAccountOrder(name).then(function (lAccountOrder) {

                            localAccountOrder = lAccountOrder;

                            if (localAccountOrder.modifiedDate > (new Date(remoteAccountOrder.modifiedDate))) {
                                deferred.resolve(localAccountOrder);
                            }
                            else {
                                deferred.resolve(remoteAccountOrder);
                            }

                        }, deferred.reject);

                    }, deferred.reject);

                } else {
                    self.getLocalAccountOrder(name).then(deferred.resolve, deferred.reject);
                }

                return deferred.promise;
            };

            return self;

        });

}());