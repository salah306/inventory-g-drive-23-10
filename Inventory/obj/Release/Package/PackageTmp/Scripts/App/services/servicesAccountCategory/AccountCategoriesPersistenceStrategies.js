﻿(function () {

    'use strict';

    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    };


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.factory('AccountCategoriesremotePersistenceStrategy', function ($http, $q) {
        var svc = {

            save: function (AccountCategory) {
                var it = AccountCategory;
                var deferred = $q.defer();
                if (isJson(AccountCategory))
                {
                    it = JSON.parse(AccountCategory);
                    return $q.all(it.map(function (it) {
                        $http.post('/api/AccountCategories', it).then(deferred.resolve, deferred.reject);
                    }));
                } else {

                    $http.post('/api/AccountCategories', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },


            getAll: function () {
                var deferred = $q.defer();

                $http.get('/api/AccountCategories')
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },

            getById: function (name) {
                var deferred = $q.defer();

                $http.get('/api/AccountCategories/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            exists: function (name) {

                var deferred = $q.defer();

                svc.getById(name).then(function (AccountCategory) {
                    deferred.resolve(AccountCategory.id === name);
                }, deferred.reject);

                return deferred.promise;
            },

            'Delete': function (name) {
                var deferred = $q.defer();

                $http.delete('/api/AccountCategories/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            }
        };
        return svc;
    });

    inventoryModule.factory('AccountCategorieslocalPersistenceStrategy',
        function ($q, localDBService, nullAccountCategory, dbModel) {

            var svc = {

                dbModel: dbModel,

                localDBService: localDBService,

                getById: function (name) {
                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getById(svc.dbModel.objectStoreName, name)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                exists: function (name) {

                    var deferred = $q.defer();

                    svc.getById(name).then(function (AccountCategory) {
                        deferred.resolve(AccountCategory.id === name);
                    }, deferred.reject);

                    return deferred.promise;
                },

                save: function (AccountCategory) {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function (e) {

                        var name = AccountCategory.name;

                        if (name === null || name === undefined) {
                            localDBService.insert(svc.dbModel.objectStoreName, AccountCategory, 'id')
                                          .then(deferred.resolve,
                                                deferred.reject);
                        } else {
                            svc.exists(name).then(function (doesExist) {

                                if (doesExist) {
                                    localDBService.update(svc.dbModel.objectStoreName, AccountCategory, name)
                                                  .then(deferred.resolve,
                                                        deferred.reject);;
                                } else {
                                    localDBService.insert(svc.dbModel.objectStoreName, AccountCategory, 'id')
                                                  .then(deferred.resolve,
                                                        deferred.reject);
                                }

                            }, deferred.reject);
                        }

                    }, deferred.reject);

                    return deferred.promise;
                },

                getAll: function () {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getAll(svc.dbModel.objectStoreName)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                'Delete': function (name) {

                    var deferred = $q.defer();

                    localDBService.delete(svc.dbModel.objectStoreName, name)
                                  .then(deferred.resolve,
                                        deferred.reject);

                    return deferred.promise;
                }

            };
           
            return svc;
        });

}());