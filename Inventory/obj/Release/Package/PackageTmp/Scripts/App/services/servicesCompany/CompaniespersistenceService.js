﻿(function () {

    'use strict';

    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.service('companiespersistenceService',
        function ($q, Offline, companiesremotePersistenceStrategy, companieslocalPersistenceStrategy) {

            var self = this;

            self.persistenceType = 'remote';

            self.action = companiesremotePersistenceStrategy;

            Offline.on('confirmed-down', function () {
                self.action = companieslocalPersistenceStrategy;
                self.persistenceType = 'local';
            });

            Offline.on('confirmed-up', function () {
                self.action = companiesremotePersistenceStrategy;
                self.persistenceType = 'remote';
            });

            self.getRemotecompany = function (name) {
                return companiesremotePersistenceStrategy.getById(name);
            };

            self.getLocalcompany = function (name) {
                return companieslocalPersistenceStrategy.getById(name);
            };

            self.getById = function (name) {

                var deferred = $q.defer();

                if (Offline.state = 'up') {

                    var
                        remotecompany = {},
                        localcompany = {};

                    self.getRemotecompany(name).then(function (rcompany) {

                        remotecompany = rcompany;

                        self.getLocalcompany(name).then(function (lcompany) {

                            localcompany = lcompany;

                            if (localcompany.modifiedDate > (new Date(remotecompany.modifiedDate))) {
                                deferred.resolve(localcompany);
                            }
                            else {
                                deferred.resolve(remotecompany);
                            }

                        }, deferred.reject);

                    }, deferred.reject);

                } else {
                    self.getLocalcompany(name).then(deferred.resolve, deferred.reject);
                }

                return deferred.promise;
            };

            return self;

        });

}());