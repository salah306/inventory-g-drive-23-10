﻿(function () {

    'use strict';

    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    };


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.factory('branchesremotePersistenceStrategy', function ($http, $q) {
        var svc = {

            save: function (branch) {
                var it = branch;
                var deferred = $q.defer();
                if (isJson(branch))
                {
                    it = JSON.parse(branch);
                    return $http.post('/api/branches', it)
                   .success(deferred.resolve)
                   .error(deferred.reject);
                   

                } else {

                    $http.post('/api/branches', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },
            update: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/branches/updateBranch', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/branches/updateBranch', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },


            getAll: function (companyId) {
                var deferred = $q.defer();

                $http.get('/api/branches', companyId)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },

            getById: function (name) {
                var deferred = $q.defer();

                $http.get('/api/branches/branchesById/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            getBycompany: function (id) {
                var deferred = $q.defer();

                $http.get('/api/Branches/getall2/' + id)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            exists: function (name) {

                var deferred = $q.defer();

                svc.getById(name).then(function (branch) {
                    deferred.resolve(branch.id === name);
                }, deferred.reject);

                return deferred.promise;
            },

            'Delete': function (name) {
                var deferred = $q.defer();

                $http.delete('/api/branches/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            }
        };
        return svc;
    });

    inventoryModule.factory('brancheslocalPersistenceStrategy',
        function ($q, localDBService, nullbranch, dbModel) {

            var svc = {

                dbModel: dbModel,

                localDBService: localDBService,

                getById: function (name) {
                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getById(svc.dbModel.objectStoreName, name)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                exists: function (name) {

                    var deferred = $q.defer();

                    svc.getById(name).then(function (branch) {
                        deferred.resolve(branch.id === name);
                    }, deferred.reject);

                    return deferred.promise;
                },

                save: function (branch) {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function (e) {

                        var name = branch.name;

                        if (name === null || name === undefined) {
                            localDBService.insert(svc.dbModel.objectStoreName, branch, 'id')
                                          .then(deferred.resolve,
                                                deferred.reject);
                        } else {
                            svc.exists(name).then(function (doesExist) {

                                if (doesExist) {
                                    localDBService.update(svc.dbModel.objectStoreName, branch, name)
                                                  .then(deferred.resolve,
                                                        deferred.reject);;
                                } else {
                                    localDBService.insert(svc.dbModel.objectStoreName, branch, 'id')
                                                  .then(deferred.resolve,
                                                        deferred.reject);
                                }

                            }, deferred.reject);
                        }

                    }, deferred.reject);

                    return deferred.promise;
                },

                getAll: function () {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getAll(svc.dbModel.objectStoreName)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                'Delete': function (name) {

                    var deferred = $q.defer();

                    localDBService.delete(svc.dbModel.objectStoreName, name)
                                  .then(deferred.resolve,
                                        deferred.reject);

                    return deferred.promise;
                }

            };
           
            return svc;
        });

}());