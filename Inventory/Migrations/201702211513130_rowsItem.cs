namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class rowsItem : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ItemsRows",
                c => new
                    {
                        itemsRowId = c.Int(nullable: false, identity: true),
                        itemPropertyValueId = c.Int(nullable: false),
                        itemPropertyValueName = c.String(),
                        itemPropertyId = c.Int(nullable: false),
                        value = c.String(),
                        type = c.String(),
                        SalesBillId = c.Int(),
                        PurchasesBillId = c.Int(),
                    })
                .PrimaryKey(t => t.itemsRowId)
                .ForeignKey("dbo.PurchasesBills", t => t.PurchasesBillId)
                .ForeignKey("dbo.SalesBills", t => t.SalesBillId)
                .Index(t => t.SalesBillId)
                .Index(t => t.PurchasesBillId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ItemsRows", "SalesBillId", "dbo.SalesBills");
            DropForeignKey("dbo.ItemsRows", "PurchasesBillId", "dbo.PurchasesBills");
            DropIndex("dbo.ItemsRows", new[] { "PurchasesBillId" });
            DropIndex("dbo.ItemsRows", new[] { "SalesBillId" });
            DropTable("dbo.ItemsRows");
        }
    }
}
