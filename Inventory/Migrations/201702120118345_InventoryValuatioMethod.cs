namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InventoryValuatioMethod : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InventoryValuationMethods",
                c => new
                    {
                        InventoryValuationMethodId = c.Int(nullable: false, identity: true),
                        InventoryValuationMethodName = c.String(),
                    })
                .PrimaryKey(t => t.InventoryValuationMethodId);
            
            AddColumn("dbo.Companies", "InventoryValuationMethodId", c => c.Int());
            CreateIndex("dbo.Companies", "InventoryValuationMethodId");
            AddForeignKey("dbo.Companies", "InventoryValuationMethodId", "dbo.InventoryValuationMethods", "InventoryValuationMethodId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Companies", "InventoryValuationMethodId", "dbo.InventoryValuationMethods");
            DropIndex("dbo.Companies", new[] { "InventoryValuationMethodId" });
            DropColumn("dbo.Companies", "InventoryValuationMethodId");
            DropTable("dbo.InventoryValuationMethods");
        }
    }
}
