namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newrableItemSubAccount : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BillSubAccountItems",
                c => new
                    {
                        BillSubAccountItemsId = c.Int(nullable: false, identity: true),
                        SubAccountId = c.Int(nullable: false),
                        qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SubAccountMovementId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BillSubAccountItemsId)
                .ForeignKey("dbo.SubAccounts", t => t.SubAccountId, cascadeDelete: false)
                .ForeignKey("dbo.SubAccountMovements", t => t.SubAccountMovementId, cascadeDelete: false)
                .Index(t => t.SubAccountId)
                .Index(t => t.SubAccountMovementId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BillSubAccountItems", "SubAccountMovementId", "dbo.SubAccountMovements");
            DropForeignKey("dbo.BillSubAccountItems", "SubAccountId", "dbo.SubAccounts");
            DropIndex("dbo.BillSubAccountItems", new[] { "SubAccountMovementId" });
            DropIndex("dbo.BillSubAccountItems", new[] { "SubAccountId" });
            DropTable("dbo.BillSubAccountItems");
        }
    }
}
