namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateCompanyproperty2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CompanyPropertyValues",
                c => new
                    {
                        CompanyPropertyValuesId = c.Int(nullable: false, identity: true),
                        CompanyPropertiesId = c.Int(nullable: false),
                        propertyValue = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.CompanyPropertyValuesId)
                .ForeignKey("dbo.CompanyProperties", t => t.CompanyPropertiesId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CompanyPropertiesId)
                .Index(t => t.CurrentWorkerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CompanyPropertyValues", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CompanyPropertyValues", "CompanyPropertiesId", "dbo.CompanyProperties");
            DropIndex("dbo.CompanyPropertyValues", new[] { "CurrentWorkerId" });
            DropIndex("dbo.CompanyPropertyValues", new[] { "CompanyPropertiesId" });
            DropTable("dbo.CompanyPropertyValues");
        }
    }
}
