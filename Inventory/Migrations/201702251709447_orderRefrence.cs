namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orderRefrence : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountOrders", "OrderTypeRefrence", c => c.String());
            AddColumn("dbo.AccountOrders", "OrderIdRefrence", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AccountOrders", "OrderIdRefrence");
            DropColumn("dbo.AccountOrders", "OrderTypeRefrence");
        }
    }
}
