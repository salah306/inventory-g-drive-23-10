namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inventoryTranfer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InventoryItems",
                c => new
                    {
                        InventoryItemsId = c.Int(nullable: false, identity: true),
                        itemName = c.Int(nullable: false),
                        itemCode = c.Int(nullable: false),
                        qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        note = c.String(),
                        noteSecond = c.String(),
                        realted = c.String(),
                        ItemGroupId = c.Int(nullable: false),
                        InventoryTransferEntityId = c.Int(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.InventoryItemsId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.InventoryTransferEntities", t => t.InventoryTransferEntityId, cascadeDelete: false)
                .ForeignKey("dbo.ItemGroups", t => t.ItemGroupId, cascadeDelete: false)
                .Index(t => t.ItemGroupId)
                .Index(t => t.InventoryTransferEntityId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.InventoryTransferEntities",
                c => new
                    {
                        InventoryTransferEntityId = c.Int(nullable: false, identity: true),
                        OrderNo = c.Int(nullable: false),
                        OrderDate = c.DateTime(nullable: false),
                        OrderDateSecond = c.DateTime(nullable: false),
                        MainAccountId = c.Int(nullable: false),
                        TitleAccountId = c.Int(nullable: false),
                        DoneFirst = c.Boolean(nullable: false),
                        DoneSceond = c.Boolean(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                        CurrentWorkerSecondId = c.String(maxLength: 128),
                        BranchId = c.Int(),
                        CompanyId = c.Int(),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                    })
                .PrimaryKey(t => t.InventoryTransferEntityId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerSecondId)
                .ForeignKey("dbo.Accounts", t => t.MainAccountId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.TitleAccountId, cascadeDelete: false)
                .Index(t => t.MainAccountId)
                .Index(t => t.TitleAccountId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerSecondId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InventoryItems", "ItemGroupId", "dbo.ItemGroups");
            DropForeignKey("dbo.InventoryTransferEntities", "TitleAccountId", "dbo.Accounts");
            DropForeignKey("dbo.InventoryTransferEntities", "MainAccountId", "dbo.Accounts");
            DropForeignKey("dbo.InventoryItems", "InventoryTransferEntityId", "dbo.InventoryTransferEntities");
            DropForeignKey("dbo.InventoryTransferEntities", "CurrentWorkerSecondId", "dbo.AspNetUsers");
            DropForeignKey("dbo.InventoryTransferEntities", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.InventoryTransferEntities", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.InventoryTransferEntities", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.InventoryItems", "CurrentWorkerId", "dbo.AspNetUsers");
            DropIndex("dbo.InventoryTransferEntities", new[] { "CompanyId" });
            DropIndex("dbo.InventoryTransferEntities", new[] { "BranchId" });
            DropIndex("dbo.InventoryTransferEntities", new[] { "CurrentWorkerSecondId" });
            DropIndex("dbo.InventoryTransferEntities", new[] { "CurrentWorkerId" });
            DropIndex("dbo.InventoryTransferEntities", new[] { "TitleAccountId" });
            DropIndex("dbo.InventoryTransferEntities", new[] { "MainAccountId" });
            DropIndex("dbo.InventoryItems", new[] { "CurrentWorkerId" });
            DropIndex("dbo.InventoryItems", new[] { "InventoryTransferEntityId" });
            DropIndex("dbo.InventoryItems", new[] { "ItemGroupId" });
            DropTable("dbo.InventoryTransferEntities");
            DropTable("dbo.InventoryItems");
        }
    }
}
