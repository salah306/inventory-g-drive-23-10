namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class accountTypeCBS : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CustomBalanceSheetTypes", "AccountTypeId", "dbo.AccountTypes");
            DropIndex("dbo.CustomBalanceSheetTypes", new[] { "AccountTypeId" });
            AddColumn("dbo.CustomBalanceSheets", "AccountTypeId", c => c.Int());
            CreateIndex("dbo.CustomBalanceSheets", "AccountTypeId");
            AddForeignKey("dbo.CustomBalanceSheets", "AccountTypeId", "dbo.AccountTypes", "AccountTypeId");
            DropColumn("dbo.CustomBalanceSheetTypes", "AccountTypeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CustomBalanceSheetTypes", "AccountTypeId", c => c.Int());
            DropForeignKey("dbo.CustomBalanceSheets", "AccountTypeId", "dbo.AccountTypes");
            DropIndex("dbo.CustomBalanceSheets", new[] { "AccountTypeId" });
            DropColumn("dbo.CustomBalanceSheets", "AccountTypeId");
            CreateIndex("dbo.CustomBalanceSheetTypes", "AccountTypeId");
            AddForeignKey("dbo.CustomBalanceSheetTypes", "AccountTypeId", "dbo.AccountTypes", "AccountTypeId");
        }
    }
}
