namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrdersConfiguration4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PurchasesOrdersConfigurationPayMethods", "AccountCategoryId", c => c.Int(nullable: false));
            AddColumn("dbo.SalessOrdersConfigurationPayMethods", "AccountCategoryId", c => c.Int(nullable: false));
            CreateIndex("dbo.PurchasesOrdersConfigurationPayMethods", "AccountCategoryId");
            CreateIndex("dbo.SalessOrdersConfigurationPayMethods", "AccountCategoryId");
            AddForeignKey("dbo.PurchasesOrdersConfigurationPayMethods", "AccountCategoryId", "dbo.AccountCategories", "AccountCategoryId", cascadeDelete: false);
            AddForeignKey("dbo.SalessOrdersConfigurationPayMethods", "AccountCategoryId", "dbo.AccountCategories", "AccountCategoryId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SalessOrdersConfigurationPayMethods", "AccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.PurchasesOrdersConfigurationPayMethods", "AccountCategoryId", "dbo.AccountCategories");
            DropIndex("dbo.SalessOrdersConfigurationPayMethods", new[] { "AccountCategoryId" });
            DropIndex("dbo.PurchasesOrdersConfigurationPayMethods", new[] { "AccountCategoryId" });
            DropColumn("dbo.SalessOrdersConfigurationPayMethods", "AccountCategoryId");
            DropColumn("dbo.PurchasesOrdersConfigurationPayMethods", "AccountCategoryId");
        }
    }
}
