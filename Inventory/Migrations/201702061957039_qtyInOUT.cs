namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class qtyInOUT : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SubAccountMovements", "QuantityIn", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SubAccountMovements", "QuantityOut", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.SubAccountMovements", "Quantity");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SubAccountMovements", "Quantity", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.SubAccountMovements", "QuantityOut");
            DropColumn("dbo.SubAccountMovements", "QuantityIn");
        }
    }
}
