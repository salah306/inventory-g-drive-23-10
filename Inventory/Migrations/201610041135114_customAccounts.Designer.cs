// <auto-generated />
namespace Inventory.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class customAccounts : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(customAccounts));
        
        string IMigrationMetadata.Id
        {
            get { return "201610041135114_customAccounts"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
