namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class transferorderNo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InventoryTransferEntities", "OrderNoIn", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.InventoryTransferEntities", "OrderNoIn");
        }
    }
}
