namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class billProperty2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BillPaymentProperties", "OrderPropertiesPayPurchasesId", c => c.Int());
            CreateIndex("dbo.BillPaymentProperties", "OrderPropertiesPayPurchasesId");
            AddForeignKey("dbo.BillPaymentProperties", "OrderPropertiesPayPurchasesId", "dbo.OrderPropertiesPayPurchases", "OrderPropertiesPayPurchasesId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BillPaymentProperties", "OrderPropertiesPayPurchasesId", "dbo.OrderPropertiesPayPurchases");
            DropIndex("dbo.BillPaymentProperties", new[] { "OrderPropertiesPayPurchasesId" });
            DropColumn("dbo.BillPaymentProperties", "OrderPropertiesPayPurchasesId");
        }
    }
}
