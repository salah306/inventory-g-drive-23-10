namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class deltepayAsaccount : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PayOes", "PayId", "dbo.Accounts");
            DropIndex("dbo.PayOes", new[] { "PayId" });
            DropColumn("dbo.PayOes", "PayId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PayOes", "PayId", c => c.Int(nullable: false));
            CreateIndex("dbo.PayOes", "PayId");
            AddForeignKey("dbo.PayOes", "PayId", "dbo.Accounts", "AccountId", cascadeDelete: true);
        }
    }
}
