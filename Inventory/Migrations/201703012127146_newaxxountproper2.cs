namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newaxxountproper2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountsCatogeryTypesAcccounts",
                c => new
                    {
                        AccountsCatogeryTypesAcccountsId = c.Int(nullable: false, identity: true),
                        TypeName = c.String(),
                    })
                .PrimaryKey(t => t.AccountsCatogeryTypesAcccountsId);
            
            AddColumn("dbo.AccountCategoryProperties", "AccountsCatogeryTypesAcccounts_AccountsCatogeryTypesAcccountsId", c => c.Int());
            CreateIndex("dbo.AccountCategoryProperties", "AccountsCatogeryTypesAcccounts_AccountsCatogeryTypesAcccountsId");
            AddForeignKey("dbo.AccountCategoryProperties", "AccountsCatogeryTypesAcccounts_AccountsCatogeryTypesAcccountsId", "dbo.AccountsCatogeryTypesAcccounts", "AccountsCatogeryTypesAcccountsId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccountCategoryProperties", "AccountsCatogeryTypesAcccounts_AccountsCatogeryTypesAcccountsId", "dbo.AccountsCatogeryTypesAcccounts");
            DropIndex("dbo.AccountCategoryProperties", new[] { "AccountsCatogeryTypesAcccounts_AccountsCatogeryTypesAcccountsId" });
            DropColumn("dbo.AccountCategoryProperties", "AccountsCatogeryTypesAcccounts_AccountsCatogeryTypesAcccountsId");
            DropTable("dbo.AccountsCatogeryTypesAcccounts");
        }
    }
}
