namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class uniquaccounts2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AccountCategoryProperties", "AccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.AccountCategoryPropertiesValues", "AccountId", "dbo.Accounts");
            DropIndex("dbo.AccountCategoryProperties", new[] { "AccountCategoryId" });
            DropIndex("dbo.AccountCategoryPropertiesValues", new[] { "AccountId" });
            AddColumn("dbo.AccountCategoryProperties", "AccountId", c => c.Int(nullable: false));
            AddColumn("dbo.AccountCategoryPropertiesValues", "SubAccountId", c => c.Int(nullable: false));
            AddColumn("dbo.SubAccounts", "Activated", c => c.Boolean(nullable: false));
            CreateIndex("dbo.AccountCategoryProperties", "AccountId");
            CreateIndex("dbo.AccountCategoryPropertiesValues", "SubAccountId");
            AddForeignKey("dbo.AccountCategoryProperties", "AccountId", "dbo.Accounts", "AccountId", cascadeDelete: false);
            AddForeignKey("dbo.AccountCategoryPropertiesValues", "SubAccountId", "dbo.SubAccounts", "SubAccountId", cascadeDelete: false);
            DropColumn("dbo.AccountCategoryProperties", "AccountCategoryId");
            DropColumn("dbo.AccountCategoryPropertiesValues", "AccountId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AccountCategoryPropertiesValues", "AccountId", c => c.Int(nullable: false));
            AddColumn("dbo.AccountCategoryProperties", "AccountCategoryId", c => c.Int(nullable: false));
            DropForeignKey("dbo.AccountCategoryPropertiesValues", "SubAccountId", "dbo.SubAccounts");
            DropForeignKey("dbo.AccountCategoryProperties", "AccountId", "dbo.Accounts");
            DropIndex("dbo.AccountCategoryPropertiesValues", new[] { "SubAccountId" });
            DropIndex("dbo.AccountCategoryProperties", new[] { "AccountId" });
            DropColumn("dbo.SubAccounts", "Activated");
            DropColumn("dbo.AccountCategoryPropertiesValues", "SubAccountId");
            DropColumn("dbo.AccountCategoryProperties", "AccountId");
            CreateIndex("dbo.AccountCategoryPropertiesValues", "AccountId");
            CreateIndex("dbo.AccountCategoryProperties", "AccountCategoryId");
            AddForeignKey("dbo.AccountCategoryPropertiesValues", "AccountId", "dbo.Accounts", "AccountId", cascadeDelete: true);
            AddForeignKey("dbo.AccountCategoryProperties", "AccountCategoryId", "dbo.AccountCategories", "AccountCategoryId", cascadeDelete: true);
        }
    }
}
