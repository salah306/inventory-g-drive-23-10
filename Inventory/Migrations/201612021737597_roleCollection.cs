namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class roleCollection : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RoleCollections",
                c => new
                    {
                        RoleCollectionId = c.Int(nullable: false, identity: true),
                        RoleCollectionName = c.String(),
                        CompanyRoleId = c.Int(nullable: false),
                        ApplicationUserComapnyId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RoleCollectionId)
                .ForeignKey("dbo.ApplicationUserComapnies", t => t.ApplicationUserComapnyId, cascadeDelete: false)
                .ForeignKey("dbo.CompanyRoles", t => t.CompanyRoleId, cascadeDelete: false)
                .Index(t => t.CompanyRoleId)
                .Index(t => t.ApplicationUserComapnyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RoleCollections", "CompanyRoleId", "dbo.CompanyRoles");
            DropForeignKey("dbo.RoleCollections", "ApplicationUserComapnyId", "dbo.ApplicationUserComapnies");
            DropIndex("dbo.RoleCollections", new[] { "ApplicationUserComapnyId" });
            DropIndex("dbo.RoleCollections", new[] { "CompanyRoleId" });
            DropTable("dbo.RoleCollections");
        }
    }
}
