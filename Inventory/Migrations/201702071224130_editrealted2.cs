namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editrealted2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PropertyValuesforItemGroups", "parintType", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PropertyValuesforItemGroups", "parintType");
        }
    }
}
