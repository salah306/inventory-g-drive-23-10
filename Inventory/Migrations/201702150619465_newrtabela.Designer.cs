// <auto-generated />
namespace Inventory.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class newrtabela : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(newrtabela));
        
        string IMigrationMetadata.Id
        {
            get { return "201702150619465_newrtabela"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
