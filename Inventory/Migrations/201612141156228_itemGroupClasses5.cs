namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class itemGroupClasses5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PropertiesforItemGroups", "ItemGroup_ItemGroupId", "dbo.ItemGroups");
            DropForeignKey("dbo.PropertiesforItemGroups", "Type_TypeforItemGroupId", "dbo.TypeforItemGroups");
            DropIndex("dbo.PropertiesforItemGroups", new[] { "Type_TypeforItemGroupId" });
            DropIndex("dbo.PropertiesforItemGroups", new[] { "ItemGroup_ItemGroupId" });
            RenameColumn(table: "dbo.PropertiesforItemGroups", name: "ItemGroup_ItemGroupId", newName: "ItemGroupId");
            RenameColumn(table: "dbo.PropertiesforItemGroups", name: "Type_TypeforItemGroupId", newName: "TypeforItemGroupId");
            AlterColumn("dbo.PropertiesforItemGroups", "TypeforItemGroupId", c => c.Int(nullable: false));
            AlterColumn("dbo.PropertiesforItemGroups", "ItemGroupId", c => c.Int(nullable: false));
            CreateIndex("dbo.PropertiesforItemGroups", "TypeforItemGroupId");
            CreateIndex("dbo.PropertiesforItemGroups", "ItemGroupId");
            AddForeignKey("dbo.PropertiesforItemGroups", "ItemGroupId", "dbo.ItemGroups", "ItemGroupId", cascadeDelete: true);
            AddForeignKey("dbo.PropertiesforItemGroups", "TypeforItemGroupId", "dbo.TypeforItemGroups", "TypeforItemGroupId", cascadeDelete: true);
            DropColumn("dbo.PropertiesforItemGroups", "TypeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PropertiesforItemGroups", "TypeId", c => c.Int(nullable: false));
            DropForeignKey("dbo.PropertiesforItemGroups", "TypeforItemGroupId", "dbo.TypeforItemGroups");
            DropForeignKey("dbo.PropertiesforItemGroups", "ItemGroupId", "dbo.ItemGroups");
            DropIndex("dbo.PropertiesforItemGroups", new[] { "ItemGroupId" });
            DropIndex("dbo.PropertiesforItemGroups", new[] { "TypeforItemGroupId" });
            AlterColumn("dbo.PropertiesforItemGroups", "ItemGroupId", c => c.Int());
            AlterColumn("dbo.PropertiesforItemGroups", "TypeforItemGroupId", c => c.Int());
            RenameColumn(table: "dbo.PropertiesforItemGroups", name: "TypeforItemGroupId", newName: "Type_TypeforItemGroupId");
            RenameColumn(table: "dbo.PropertiesforItemGroups", name: "ItemGroupId", newName: "ItemGroup_ItemGroupId");
            CreateIndex("dbo.PropertiesforItemGroups", "ItemGroup_ItemGroupId");
            CreateIndex("dbo.PropertiesforItemGroups", "Type_TypeforItemGroupId");
            AddForeignKey("dbo.PropertiesforItemGroups", "Type_TypeforItemGroupId", "dbo.TypeforItemGroups", "TypeforItemGroupId");
            AddForeignKey("dbo.PropertiesforItemGroups", "ItemGroup_ItemGroupId", "dbo.ItemGroups", "ItemGroupId");
        }
    }
}
