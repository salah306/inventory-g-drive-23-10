namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class appstorge : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.billMainAccountPurSts",
                c => new
                    {
                        billMainAccountPurStId = c.Int(nullable: false, identity: true),
                        accountId = c.Int(nullable: false),
                        accountName = c.String(),
                        value = c.Int(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.billMainAccountPurStId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.billMainAccountSalesSts",
                c => new
                    {
                        billMainAccountSalesStId = c.Int(nullable: false, identity: true),
                        accountId = c.Int(nullable: false),
                        accountName = c.String(),
                        value = c.Int(nullable: false),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.billMainAccountSalesStId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.branchGroupSts",
                c => new
                    {
                        id = c.String(nullable: false, maxLength: 128),
                        branchGroupStId = c.Int(nullable: false),
                        name = c.String(),
                        companyName = c.String(),
                        fullName = c.String(),
                        isCompany = c.Boolean(nullable: false),
                        companyId = c.Int(nullable: false),
                        realName = c.String(),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.inventorySts",
                c => new
                    {
                        inventoryStId = c.Int(nullable: false, identity: true),
                        accountId = c.Int(nullable: false),
                        accountName = c.String(),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.inventoryStId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.selectedOrderGroupNameSts",
                c => new
                    {
                        selectedOrderGroupNameStId = c.Int(nullable: false, identity: true),
                        orderGroupId = c.Int(nullable: false),
                        orderGroupName = c.String(),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.selectedOrderGroupNameStId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.selectedOrderGroupNameSts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.inventorySts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.branchGroupSts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.billMainAccountSalesSts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.billMainAccountPurSts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropIndex("dbo.selectedOrderGroupNameSts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.inventorySts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.branchGroupSts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.billMainAccountSalesSts", new[] { "CurrentWorkerId" });
            DropIndex("dbo.billMainAccountPurSts", new[] { "CurrentWorkerId" });
            DropTable("dbo.selectedOrderGroupNameSts");
            DropTable("dbo.inventorySts");
            DropTable("dbo.branchGroupSts");
            DropTable("dbo.billMainAccountSalesSts");
            DropTable("dbo.billMainAccountPurSts");
        }
    }
}
