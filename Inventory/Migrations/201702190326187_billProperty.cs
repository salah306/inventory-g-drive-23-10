namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class billProperty : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BillPaymentProperties",
                c => new
                    {
                        BillPaymentPropertiesId = c.Int(nullable: false, identity: true),
                        OrderPropertiesPaySalesId = c.Int(),
                        PurchasesBillId = c.Int(),
                        SalesBillId = c.Int(),
                        value = c.String(),
                    })
                .PrimaryKey(t => t.BillPaymentPropertiesId)
                .ForeignKey("dbo.OrderPropertiesPaySales", t => t.OrderPropertiesPaySalesId)
                .ForeignKey("dbo.PurchasesBills", t => t.PurchasesBillId)
                .ForeignKey("dbo.SalesBills", t => t.SalesBillId)
                .Index(t => t.OrderPropertiesPaySalesId)
                .Index(t => t.PurchasesBillId)
                .Index(t => t.SalesBillId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BillPaymentProperties", "SalesBillId", "dbo.SalesBills");
            DropForeignKey("dbo.BillPaymentProperties", "PurchasesBillId", "dbo.PurchasesBills");
            DropForeignKey("dbo.BillPaymentProperties", "OrderPropertiesPaySalesId", "dbo.OrderPropertiesPaySales");
            DropIndex("dbo.BillPaymentProperties", new[] { "SalesBillId" });
            DropIndex("dbo.BillPaymentProperties", new[] { "PurchasesBillId" });
            DropIndex("dbo.BillPaymentProperties", new[] { "OrderPropertiesPaySalesId" });
            DropTable("dbo.BillPaymentProperties");
        }
    }
}
