namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class itemGroupClasses2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Units", "CompanyId", c => c.Int(nullable: false));
            CreateIndex("dbo.Units", "CompanyId");
            AddForeignKey("dbo.Units", "CompanyId", "dbo.Companies", "CompanyId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Units", "CompanyId", "dbo.Companies");
            DropIndex("dbo.Units", new[] { "CompanyId" });
            DropColumn("dbo.Units", "CompanyId");
        }
    }
}
