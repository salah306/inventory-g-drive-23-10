namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class billother : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BillOtherAccountBills",
                c => new
                    {
                        BillOtherAccountBillId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDebit = c.Boolean(nullable: false),
                        SalesBillId = c.Int(),
                        PurchasesBillId = c.Int(),
                    })
                .PrimaryKey(t => t.BillOtherAccountBillId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.PurchasesBills", t => t.PurchasesBillId)
                .ForeignKey("dbo.SalesBills", t => t.SalesBillId)
                .Index(t => t.AccountId)
                .Index(t => t.SalesBillId)
                .Index(t => t.PurchasesBillId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BillOtherAccountBills", "SalesBillId", "dbo.SalesBills");
            DropForeignKey("dbo.BillOtherAccountBills", "PurchasesBillId", "dbo.PurchasesBills");
            DropForeignKey("dbo.BillOtherAccountBills", "AccountId", "dbo.Accounts");
            DropIndex("dbo.BillOtherAccountBills", new[] { "PurchasesBillId" });
            DropIndex("dbo.BillOtherAccountBills", new[] { "SalesBillId" });
            DropIndex("dbo.BillOtherAccountBills", new[] { "AccountId" });
            DropTable("dbo.BillOtherAccountBills");
        }
    }
}
