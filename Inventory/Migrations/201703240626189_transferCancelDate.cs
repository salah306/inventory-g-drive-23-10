namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class transferCancelDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InventoryTransferEntities", "CancelDate", c => c.DateTime());
            AddColumn("dbo.InventoryTransferEntities", "CurrentWorkerCancelId", c => c.String(maxLength: 128));
            CreateIndex("dbo.InventoryTransferEntities", "CurrentWorkerCancelId");
            AddForeignKey("dbo.InventoryTransferEntities", "CurrentWorkerCancelId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InventoryTransferEntities", "CurrentWorkerCancelId", "dbo.AspNetUsers");
            DropIndex("dbo.InventoryTransferEntities", new[] { "CurrentWorkerCancelId" });
            DropColumn("dbo.InventoryTransferEntities", "CurrentWorkerCancelId");
            DropColumn("dbo.InventoryTransferEntities", "CancelDate");
        }
    }
}
