namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nnnn : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.OrderProperties", "OrderPropertiesName", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.OrderProperties", "OrderPropertiesName", c => c.Int(nullable: false));
        }
    }
}
