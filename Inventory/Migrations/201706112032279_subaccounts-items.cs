namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class subaccountsitems : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.subAccountNpropertyValues", "PropertyValuesforItemGroupId", "dbo.PropertyValuesforItemGroups");
            DropForeignKey("dbo.subAccountNpropertyValues", "SubAccountId", "dbo.SubAccounts");
            DropIndex("dbo.subAccountNpropertyValues", new[] { "PropertyValuesforItemGroupId" });
            DropIndex("dbo.subAccountNpropertyValues", new[] { "SubAccountId" });
            DropColumn("dbo.SubAccounts", "Realted");
            DropColumn("dbo.InventoryItems", "realted");
            DropTable("dbo.subAccountNpropertyValues");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.subAccountNpropertyValues",
                c => new
                    {
                        subAccountNpropertyValuesId = c.Int(nullable: false, identity: true),
                        PropertyValuesforItemGroupId = c.Int(nullable: false),
                        SubAccountId = c.Int(),
                    })
                .PrimaryKey(t => t.subAccountNpropertyValuesId);
            
            AddColumn("dbo.InventoryItems", "realted", c => c.String());
            AddColumn("dbo.SubAccounts", "Realted", c => c.String());
            CreateIndex("dbo.subAccountNpropertyValues", "SubAccountId");
            CreateIndex("dbo.subAccountNpropertyValues", "PropertyValuesforItemGroupId");
            AddForeignKey("dbo.subAccountNpropertyValues", "SubAccountId", "dbo.SubAccounts", "SubAccountId");
            AddForeignKey("dbo.subAccountNpropertyValues", "PropertyValuesforItemGroupId", "dbo.PropertyValuesforItemGroups", "PropertyValuesforItemGroupId", cascadeDelete: true);
        }
    }
}
