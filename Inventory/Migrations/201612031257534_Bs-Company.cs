namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BsCompany : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BalanceSheets", "CompanyId", c => c.Int());
            CreateIndex("dbo.BalanceSheets", "CompanyId");
            AddForeignKey("dbo.BalanceSheets", "CompanyId", "dbo.Companies", "CompanyId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BalanceSheets", "CompanyId", "dbo.Companies");
            DropIndex("dbo.BalanceSheets", new[] { "CompanyId" });
            DropColumn("dbo.BalanceSheets", "CompanyId");
        }
    }
}
