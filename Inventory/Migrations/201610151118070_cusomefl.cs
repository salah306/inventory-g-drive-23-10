namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cusomefl : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CustomAccountCategories", "BalanceSheetType_CustomBalanceSheetTypeId", "dbo.CustomBalanceSheetTypes");
            DropForeignKey("dbo.CustomBalanceSheetTypes", "BalanceSheet_CustomBalanceSheetId", "dbo.CustomBalanceSheets");
            DropIndex("dbo.CustomAccountCategories", new[] { "BalanceSheetType_CustomBalanceSheetTypeId" });
            DropIndex("dbo.CustomBalanceSheetTypes", new[] { "BalanceSheet_CustomBalanceSheetId" });
            RenameColumn(table: "dbo.CustomAccountCategories", name: "BalanceSheetType_CustomBalanceSheetTypeId", newName: "CustomBalanceSheetTypeId");
            RenameColumn(table: "dbo.CustomBalanceSheetTypes", name: "BalanceSheet_CustomBalanceSheetId", newName: "CustomBalanceSheetId");
            AlterColumn("dbo.CustomAccountCategories", "CustomBalanceSheetTypeId", c => c.Int(nullable: false));
            AlterColumn("dbo.CustomBalanceSheetTypes", "CustomBalanceSheetId", c => c.Int(nullable: false));
            CreateIndex("dbo.CustomAccountCategories", "CustomBalanceSheetTypeId");
            CreateIndex("dbo.CustomBalanceSheetTypes", "CustomBalanceSheetId");
            AddForeignKey("dbo.CustomAccountCategories", "CustomBalanceSheetTypeId", "dbo.CustomBalanceSheetTypes", "CustomBalanceSheetTypeId", cascadeDelete: true);
            AddForeignKey("dbo.CustomBalanceSheetTypes", "CustomBalanceSheetId", "dbo.CustomBalanceSheets", "CustomBalanceSheetId", cascadeDelete: true);
            DropColumn("dbo.CustomAccountCategories", "BalanceSheetTypeId");
            DropColumn("dbo.CustomBalanceSheetTypes", "BalanceSheetId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CustomBalanceSheetTypes", "BalanceSheetId", c => c.Int(nullable: false));
            AddColumn("dbo.CustomAccountCategories", "BalanceSheetTypeId", c => c.Int(nullable: false));
            DropForeignKey("dbo.CustomBalanceSheetTypes", "CustomBalanceSheetId", "dbo.CustomBalanceSheets");
            DropForeignKey("dbo.CustomAccountCategories", "CustomBalanceSheetTypeId", "dbo.CustomBalanceSheetTypes");
            DropIndex("dbo.CustomBalanceSheetTypes", new[] { "CustomBalanceSheetId" });
            DropIndex("dbo.CustomAccountCategories", new[] { "CustomBalanceSheetTypeId" });
            AlterColumn("dbo.CustomBalanceSheetTypes", "CustomBalanceSheetId", c => c.Int());
            AlterColumn("dbo.CustomAccountCategories", "CustomBalanceSheetTypeId", c => c.Int());
            RenameColumn(table: "dbo.CustomBalanceSheetTypes", name: "CustomBalanceSheetId", newName: "BalanceSheet_CustomBalanceSheetId");
            RenameColumn(table: "dbo.CustomAccountCategories", name: "CustomBalanceSheetTypeId", newName: "BalanceSheetType_CustomBalanceSheetTypeId");
            CreateIndex("dbo.CustomBalanceSheetTypes", "BalanceSheet_CustomBalanceSheetId");
            CreateIndex("dbo.CustomAccountCategories", "BalanceSheetType_CustomBalanceSheetTypeId");
            AddForeignKey("dbo.CustomBalanceSheetTypes", "BalanceSheet_CustomBalanceSheetId", "dbo.CustomBalanceSheets", "CustomBalanceSheetId");
            AddForeignKey("dbo.CustomAccountCategories", "BalanceSheetType_CustomBalanceSheetTypeId", "dbo.CustomBalanceSheetTypes", "CustomBalanceSheetTypeId");
        }
    }
}
