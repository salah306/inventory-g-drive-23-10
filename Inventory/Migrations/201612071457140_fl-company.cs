namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class flcompany : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.FinancialLists", "BranchId", "dbo.Branches");
            DropIndex("dbo.FinancialLists", new[] { "BranchId" });
            AddColumn("dbo.CustomAccountCategories", "CreatedDate", n => n.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AddColumn("dbo.CustomAccounts", "CreatedDate", n => n.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AddColumn("dbo.CustomBalanceSheetTypes", "CreatedDate", n => n.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AddColumn("dbo.CustomBalanceSheets", "CreatedDate", n => n.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AddColumn("dbo.FinancialLists", "CompanyId", c => c.Int());
            AddColumn("dbo.FinancialLists", "CreatedDate", n => n.DateTime(nullable: false, defaultValueSql: "GETUTCDATE()"));
            AlterColumn("dbo.FinancialLists", "BranchId", c => c.Int());
            CreateIndex("dbo.FinancialLists", "BranchId");
            CreateIndex("dbo.FinancialLists", "CompanyId");
            AddForeignKey("dbo.FinancialLists", "CompanyId", "dbo.Companies", "CompanyId");
            AddForeignKey("dbo.FinancialLists", "BranchId", "dbo.Branches", "BranchId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.FinancialLists", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.FinancialLists", "CompanyId", "dbo.Companies");
            DropIndex("dbo.FinancialLists", new[] { "CompanyId" });
            DropIndex("dbo.FinancialLists", new[] { "BranchId" });
            AlterColumn("dbo.FinancialLists", "BranchId", c => c.Int(nullable: false));
            DropColumn("dbo.FinancialLists", "CreatedDate");
            DropColumn("dbo.FinancialLists", "CompanyId");
            DropColumn("dbo.CustomBalanceSheets", "CreatedDate");
            DropColumn("dbo.CustomBalanceSheetTypes", "CreatedDate");
            DropColumn("dbo.CustomAccounts", "CreatedDate");
            DropColumn("dbo.CustomAccountCategories", "CreatedDate");
            CreateIndex("dbo.FinancialLists", "BranchId");
            AddForeignKey("dbo.FinancialLists", "BranchId", "dbo.Branches", "BranchId", cascadeDelete: true);
        }
    }
}
