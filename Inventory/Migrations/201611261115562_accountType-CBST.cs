namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class accountTypeCBST : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomBalanceSheetTypes", "AccountTypeId", c => c.Int());
            CreateIndex("dbo.CustomBalanceSheetTypes", "AccountTypeId");
            AddForeignKey("dbo.CustomBalanceSheetTypes", "AccountTypeId", "dbo.AccountTypes", "AccountTypeId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CustomBalanceSheetTypes", "AccountTypeId", "dbo.AccountTypes");
            DropIndex("dbo.CustomBalanceSheetTypes", new[] { "AccountTypeId" });
            DropColumn("dbo.CustomBalanceSheetTypes", "AccountTypeId");
        }
    }
}
