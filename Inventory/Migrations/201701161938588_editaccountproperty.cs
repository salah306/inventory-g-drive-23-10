namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editaccountproperty : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AccountCategoryProperties", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.Accounts", "AccountsDataTypesId", "dbo.AccountsDataTypes");
            DropForeignKey("dbo.AccountCategoryPropertiesValues", "SubAccountId", "dbo.SubAccounts");
            DropIndex("dbo.Accounts", new[] { "AccountsDataTypesId" });
            DropIndex("dbo.AccountCategoryProperties", new[] { "AccountId" });
            DropIndex("dbo.AccountCategoryPropertiesValues", new[] { "SubAccountId" });
            RenameColumn(table: "dbo.AccountCategoryPropertiesValues", name: "SubAccountId", newName: "SubAccount_SubAccountId");
            AddColumn("dbo.AccountCategories", "AccountsDataTypesId", c => c.Int());
            AddColumn("dbo.AccountCategoryProperties", "AccountCategoryId", c => c.Int(nullable: false));
            AddColumn("dbo.AccountCategoryPropertiesValues", "AccountId", c => c.Int(nullable: false));
            AlterColumn("dbo.AccountCategoryPropertiesValues", "SubAccount_SubAccountId", c => c.Int());
            CreateIndex("dbo.AccountCategories", "AccountsDataTypesId");
            CreateIndex("dbo.AccountCategoryProperties", "AccountCategoryId");
            CreateIndex("dbo.AccountCategoryPropertiesValues", "AccountId");
            CreateIndex("dbo.AccountCategoryPropertiesValues", "SubAccount_SubAccountId");
            AddForeignKey("dbo.AccountCategoryProperties", "AccountCategoryId", "dbo.AccountCategories", "AccountCategoryId", cascadeDelete: false);
            AddForeignKey("dbo.AccountCategoryPropertiesValues", "AccountId", "dbo.Accounts", "AccountId", cascadeDelete: false);
            AddForeignKey("dbo.AccountCategories", "AccountsDataTypesId", "dbo.AccountsDataTypes", "Id");
            AddForeignKey("dbo.AccountCategoryPropertiesValues", "SubAccount_SubAccountId", "dbo.SubAccounts", "SubAccountId");
            DropColumn("dbo.Accounts", "AccountsDataTypesId");
            DropColumn("dbo.AccountCategoryProperties", "AccountId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AccountCategoryProperties", "AccountId", c => c.Int(nullable: false));
            AddColumn("dbo.Accounts", "AccountsDataTypesId", c => c.Int());
            DropForeignKey("dbo.AccountCategoryPropertiesValues", "SubAccount_SubAccountId", "dbo.SubAccounts");
            DropForeignKey("dbo.AccountCategories", "AccountsDataTypesId", "dbo.AccountsDataTypes");
            DropForeignKey("dbo.AccountCategoryPropertiesValues", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.AccountCategoryProperties", "AccountCategoryId", "dbo.AccountCategories");
            DropIndex("dbo.AccountCategoryPropertiesValues", new[] { "SubAccount_SubAccountId" });
            DropIndex("dbo.AccountCategoryPropertiesValues", new[] { "AccountId" });
            DropIndex("dbo.AccountCategoryProperties", new[] { "AccountCategoryId" });
            DropIndex("dbo.AccountCategories", new[] { "AccountsDataTypesId" });
            AlterColumn("dbo.AccountCategoryPropertiesValues", "SubAccount_SubAccountId", c => c.Int(nullable: false));
            DropColumn("dbo.AccountCategoryPropertiesValues", "AccountId");
            DropColumn("dbo.AccountCategoryProperties", "AccountCategoryId");
            DropColumn("dbo.AccountCategories", "AccountsDataTypesId");
            RenameColumn(table: "dbo.AccountCategoryPropertiesValues", name: "SubAccount_SubAccountId", newName: "SubAccountId");
            CreateIndex("dbo.AccountCategoryPropertiesValues", "SubAccountId");
            CreateIndex("dbo.AccountCategoryProperties", "AccountId");
            CreateIndex("dbo.Accounts", "AccountsDataTypesId");
            AddForeignKey("dbo.AccountCategoryPropertiesValues", "SubAccountId", "dbo.SubAccounts", "SubAccountId", cascadeDelete: false);
            AddForeignKey("dbo.Accounts", "AccountsDataTypesId", "dbo.AccountsDataTypes", "Id");
            AddForeignKey("dbo.AccountCategoryProperties", "AccountId", "dbo.Accounts", "AccountId", cascadeDelete: false);
        }
    }
}
