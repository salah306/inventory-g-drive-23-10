namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inventoryTranfer2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.InventoryItems", "itemName", c => c.String());
            AlterColumn("dbo.InventoryItems", "itemCode", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.InventoryItems", "itemCode", c => c.Int(nullable: false));
            AlterColumn("dbo.InventoryItems", "itemName", c => c.Int(nullable: false));
        }
    }
}
