namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addorderproperty : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderPropertiesPurchases", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderPropertiesPurchases", "OrderPropertyTypeId", "dbo.OrderPropertyTypes");
            DropForeignKey("dbo.OrderPropertiesPurchases", "PurchasesId", "dbo.Purchases");
            DropForeignKey("dbo.OrderPropertiesSales", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderPropertiesSales", "OrderPropertyTypeId", "dbo.OrderPropertyTypes");
            DropForeignKey("dbo.OrderPropertiesSales", "SalesId", "dbo.Sales");
            DropIndex("dbo.OrderPropertiesPurchases", new[] { "PurchasesId" });
            DropIndex("dbo.OrderPropertiesPurchases", new[] { "OrderPropertyTypeId" });
            DropIndex("dbo.OrderPropertiesPurchases", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderPropertiesSales", new[] { "SalesId" });
            DropIndex("dbo.OrderPropertiesSales", new[] { "OrderPropertyTypeId" });
            DropIndex("dbo.OrderPropertiesSales", new[] { "CurrentWorkerId" });
            CreateTable(
                "dbo.OrderPropertiesPayPurchases",
                c => new
                    {
                        OrderPropertiesPayPurchasesId = c.Int(nullable: false, identity: true),
                        OrderPropertiesPayPurchasesName = c.String(),
                        OrderGroupPayMethodPurchasesId = c.Int(nullable: false),
                        IsRequired = c.Boolean(nullable: false),
                        OrderPropertyTypeId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderPropertiesPayPurchasesId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.OrderGroupPayMethodPurchases", t => t.OrderGroupPayMethodPurchasesId,cascadeDelete: false)
                .ForeignKey("dbo.OrderPropertyTypes", t => t.OrderPropertyTypeId,cascadeDelete: false)
                .Index(t => t.OrderGroupPayMethodPurchasesId)
                .Index(t => t.OrderPropertyTypeId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.OrderPropertiesPaySales",
                c => new
                    {
                        OrderPropertiesPaySalesId = c.Int(nullable: false, identity: true),
                        OrderPropertiesPaySalesName = c.String(),
                        OrderGroupPayMethodSalesId = c.Int(nullable: false),
                        IsRequired = c.Boolean(nullable: false),
                        OrderPropertyTypeId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderPropertiesPaySalesId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.OrderGroupPayMethodSales", t => t.OrderGroupPayMethodSalesId,cascadeDelete: false)
                .ForeignKey("dbo.OrderPropertyTypes", t => t.OrderPropertyTypeId,cascadeDelete: false)
                .Index(t => t.OrderGroupPayMethodSalesId)
                .Index(t => t.OrderPropertyTypeId)
                .Index(t => t.CurrentWorkerId);
            
            AddColumn("dbo.OrderGroupPayMethodPurchases", "OrderGroupPayMethodPurchasesName", c => c.String(nullable: false));
            DropColumn("dbo.OrderGroupPayMethodPurchases", "OrderGroupPayMethodName");
            DropTable("dbo.OrderPropertiesPurchases");
            DropTable("dbo.OrderPropertiesSales");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.OrderPropertiesSales",
                c => new
                    {
                        OrderPropertiesSalesId = c.Int(nullable: false, identity: true),
                        OrderPropertiesName = c.String(),
                        SalesId = c.Int(nullable: false),
                        IsRequired = c.Boolean(nullable: false),
                        OrderPropertyTypeId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderPropertiesSalesId);
            
            CreateTable(
                "dbo.OrderPropertiesPurchases",
                c => new
                    {
                        OrderPropertiesPurchasesId = c.Int(nullable: false, identity: true),
                        OrderPropertiesName = c.String(),
                        PurchasesId = c.Int(nullable: false),
                        IsRequired = c.Boolean(nullable: false),
                        OrderPropertyTypeId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.OrderPropertiesPurchasesId);
            
            AddColumn("dbo.OrderGroupPayMethodPurchases", "OrderGroupPayMethodName", c => c.String(nullable: false));
            DropForeignKey("dbo.OrderPropertiesPaySales", "OrderPropertyTypeId", "dbo.OrderPropertyTypes");
            DropForeignKey("dbo.OrderPropertiesPaySales", "OrderGroupPayMethodSalesId", "dbo.OrderGroupPayMethodSales");
            DropForeignKey("dbo.OrderPropertiesPaySales", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderPropertiesPayPurchases", "OrderPropertyTypeId", "dbo.OrderPropertyTypes");
            DropForeignKey("dbo.OrderPropertiesPayPurchases", "OrderGroupPayMethodPurchasesId", "dbo.OrderGroupPayMethodPurchases");
            DropForeignKey("dbo.OrderPropertiesPayPurchases", "CurrentWorkerId", "dbo.AspNetUsers");
            DropIndex("dbo.OrderPropertiesPaySales", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderPropertiesPaySales", new[] { "OrderPropertyTypeId" });
            DropIndex("dbo.OrderPropertiesPaySales", new[] { "OrderGroupPayMethodSalesId" });
            DropIndex("dbo.OrderPropertiesPayPurchases", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderPropertiesPayPurchases", new[] { "OrderPropertyTypeId" });
            DropIndex("dbo.OrderPropertiesPayPurchases", new[] { "OrderGroupPayMethodPurchasesId" });
            DropColumn("dbo.OrderGroupPayMethodPurchases", "OrderGroupPayMethodPurchasesName");
            DropTable("dbo.OrderPropertiesPaySales");
            DropTable("dbo.OrderPropertiesPayPurchases");
            CreateIndex("dbo.OrderPropertiesSales", "CurrentWorkerId");
            CreateIndex("dbo.OrderPropertiesSales", "OrderPropertyTypeId");
            CreateIndex("dbo.OrderPropertiesSales", "SalesId");
            CreateIndex("dbo.OrderPropertiesPurchases", "CurrentWorkerId");
            CreateIndex("dbo.OrderPropertiesPurchases", "OrderPropertyTypeId");
            CreateIndex("dbo.OrderPropertiesPurchases", "PurchasesId");
            AddForeignKey("dbo.OrderPropertiesSales", "SalesId", "dbo.Sales", "SalesId",cascadeDelete: false);
            AddForeignKey("dbo.OrderPropertiesSales", "OrderPropertyTypeId", "dbo.OrderPropertyTypes", "OrderPropertyTypeId",cascadeDelete: false);
            AddForeignKey("dbo.OrderPropertiesSales", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.OrderPropertiesPurchases", "PurchasesId", "dbo.Purchases", "PurchasesId",cascadeDelete: false);
            AddForeignKey("dbo.OrderPropertiesPurchases", "OrderPropertyTypeId", "dbo.OrderPropertyTypes", "OrderPropertyTypeId",cascadeDelete: false);
            AddForeignKey("dbo.OrderPropertiesPurchases", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
        }
    }
}
