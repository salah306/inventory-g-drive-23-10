namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ordergroupEdited : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.OrderMainAccounts", "AccountId", "dbo.Accounts");
            DropIndex("dbo.OrderMainAccounts", new[] { "AccountId" });
            CreateTable(
                "dbo.OrderGroupPayMethods",
                c => new
                    {
                        OrderGroupPayMethodId = c.Int(nullable: false, identity: true),
                        OrderGroupPayMethodName = c.String(nullable: false),
                        AccountId = c.Int(nullable: false),
                        OrderGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderGroupPayMethodId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.OrderGroups", t => t.OrderGroupId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.OrderGroupId);
            
            CreateTable(
                "dbo.OrderGroupTypes",
                c => new
                    {
                        OrderGroupTypeId = c.Int(nullable: false, identity: true),
                        OrderGroupTypeName = c.String(),
                        OrderGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderGroupTypeId)
                .ForeignKey("dbo.OrderGroups", t => t.OrderGroupId, cascadeDelete: false)
                .Index(t => t.OrderGroupId);
            
            CreateTable(
                "dbo.OrderGroupTypeSaleAndPurchese",
                c => new
                    {
                        OrderGroupTypeSaleAndPurcheseId = c.Int(nullable: false, identity: true),
                        CaseDebit = c.String(nullable: false),
                        CaseCrdit = c.String(nullable: false),
                        OrderGroupTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderGroupTypeSaleAndPurcheseId)
                .ForeignKey("dbo.OrderGroupTypes", t => t.OrderGroupTypeId, cascadeDelete: false)
                .Index(t => t.OrderGroupTypeId);
            
            AddColumn("dbo.OrderMainAccounts", "SalesCostAccountId", c => c.Int(nullable: false));
            AddColumn("dbo.OrderMainAccounts", "SalesAccountId", c => c.Int(nullable: false));
            AddColumn("dbo.OrderMainAccounts", "SalesReturnAccountId", c => c.Int(nullable: false));
            AddColumn("dbo.OrderMainAccounts", "Account_AccountId", c => c.Int());
            AddColumn("dbo.OrderMainAccounts", "SalesAccount_AccountId", c => c.Int());
            AddColumn("dbo.OrderMainAccounts", "SalesCostAccount_AccountId", c => c.Int());
            AddColumn("dbo.OrderMainAccounts", "SalesReturnAccount_AccountId", c => c.Int());
            CreateIndex("dbo.OrderMainAccounts", "Account_AccountId");
            CreateIndex("dbo.OrderMainAccounts", "SalesAccount_AccountId");
            CreateIndex("dbo.OrderMainAccounts", "SalesCostAccount_AccountId");
            CreateIndex("dbo.OrderMainAccounts", "SalesReturnAccount_AccountId");
            AddForeignKey("dbo.OrderMainAccounts", "SalesAccount_AccountId", "dbo.Accounts", "AccountId");
            AddForeignKey("dbo.OrderMainAccounts", "SalesCostAccount_AccountId", "dbo.Accounts", "AccountId");
            AddForeignKey("dbo.OrderMainAccounts", "SalesReturnAccount_AccountId", "dbo.Accounts", "AccountId");
            AddForeignKey("dbo.OrderMainAccounts", "Account_AccountId", "dbo.Accounts", "AccountId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderMainAccounts", "Account_AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMainAccounts", "SalesReturnAccount_AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMainAccounts", "SalesCostAccount_AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMainAccounts", "SalesAccount_AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderGroupTypeSaleAndPurchese", "OrderGroupTypeId", "dbo.OrderGroupTypes");
            DropForeignKey("dbo.OrderGroupTypes", "OrderGroupId", "dbo.OrderGroups");
            DropForeignKey("dbo.OrderGroupPayMethods", "OrderGroupId", "dbo.OrderGroups");
            DropForeignKey("dbo.OrderGroupPayMethods", "AccountId", "dbo.Accounts");
            DropIndex("dbo.OrderMainAccounts", new[] { "SalesReturnAccount_AccountId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "SalesCostAccount_AccountId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "SalesAccount_AccountId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "Account_AccountId" });
            DropIndex("dbo.OrderGroupTypeSaleAndPurchese", new[] { "OrderGroupTypeId" });
            DropIndex("dbo.OrderGroupTypes", new[] { "OrderGroupId" });
            DropIndex("dbo.OrderGroupPayMethods", new[] { "OrderGroupId" });
            DropIndex("dbo.OrderGroupPayMethods", new[] { "AccountId" });
            DropColumn("dbo.OrderMainAccounts", "SalesReturnAccount_AccountId");
            DropColumn("dbo.OrderMainAccounts", "SalesCostAccount_AccountId");
            DropColumn("dbo.OrderMainAccounts", "SalesAccount_AccountId");
            DropColumn("dbo.OrderMainAccounts", "Account_AccountId");
            DropColumn("dbo.OrderMainAccounts", "SalesReturnAccountId");
            DropColumn("dbo.OrderMainAccounts", "SalesAccountId");
            DropColumn("dbo.OrderMainAccounts", "SalesCostAccountId");
            DropTable("dbo.OrderGroupTypeSaleAndPurchese");
            DropTable("dbo.OrderGroupTypes");
            DropTable("dbo.OrderGroupPayMethods");
            CreateIndex("dbo.OrderMainAccounts", "AccountId");
            AddForeignKey("dbo.OrderMainAccounts", "AccountId", "dbo.Accounts", "AccountId", cascadeDelete: false);
        }
    }
}
