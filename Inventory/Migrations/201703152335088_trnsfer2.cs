namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class trnsfer2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SubAccountOrders", "isDone", c => c.Boolean());
            AddColumn("dbo.InventoryItems", "SubAccountFromId", c => c.Int(nullable: false));
            AddColumn("dbo.InventoryItems", "SubAccountToId", c => c.Int(nullable: false));
            AddColumn("dbo.InventoryItems", "SubAccountTransitId", c => c.Int(nullable: false));
            AddColumn("dbo.InventoryItems", "costPrice", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.InventoryTransferEntities", "Cancel", c => c.Boolean(nullable: false));
            CreateIndex("dbo.InventoryItems", "SubAccountFromId");
            CreateIndex("dbo.InventoryItems", "SubAccountToId");
            CreateIndex("dbo.InventoryItems", "SubAccountTransitId");
            AddForeignKey("dbo.InventoryItems", "SubAccountFromId", "dbo.SubAccounts", "SubAccountId", cascadeDelete: false);
            AddForeignKey("dbo.InventoryItems", "SubAccountToId", "dbo.SubAccounts", "SubAccountId", cascadeDelete: false);
            AddForeignKey("dbo.InventoryItems", "SubAccountTransitId", "dbo.SubAccounts", "SubAccountId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InventoryItems", "SubAccountTransitId", "dbo.SubAccounts");
            DropForeignKey("dbo.InventoryItems", "SubAccountToId", "dbo.SubAccounts");
            DropForeignKey("dbo.InventoryItems", "SubAccountFromId", "dbo.SubAccounts");
            DropIndex("dbo.InventoryItems", new[] { "SubAccountTransitId" });
            DropIndex("dbo.InventoryItems", new[] { "SubAccountToId" });
            DropIndex("dbo.InventoryItems", new[] { "SubAccountFromId" });
            DropColumn("dbo.InventoryTransferEntities", "Cancel");
            DropColumn("dbo.InventoryItems", "costPrice");
            DropColumn("dbo.InventoryItems", "SubAccountTransitId");
            DropColumn("dbo.InventoryItems", "SubAccountToId");
            DropColumn("dbo.InventoryItems", "SubAccountFromId");
            DropColumn("dbo.SubAccountOrders", "isDone");
        }
    }
}
