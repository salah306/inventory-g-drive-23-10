namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class accountssumdebit3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SubAccounts", "ItemInfoId", "dbo.TempItems");
            DropIndex("dbo.SubAccounts", new[] { "ItemInfoId" });
            AlterColumn("dbo.SubAccounts", "ItemInfoId", c => c.Int(nullable: false));
            CreateIndex("dbo.SubAccounts", "ItemInfoId");
            AddForeignKey("dbo.SubAccounts", "ItemInfoId", "dbo.TempItems", "TempItemId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SubAccounts", "ItemInfoId", "dbo.TempItems");
            DropIndex("dbo.SubAccounts", new[] { "ItemInfoId" });
            AlterColumn("dbo.SubAccounts", "ItemInfoId", c => c.Int());
            CreateIndex("dbo.SubAccounts", "ItemInfoId");
            AddForeignKey("dbo.SubAccounts", "ItemInfoId", "dbo.TempItems", "TempItemId");
        }
    }
}
