namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class appUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ApplicationUserComapnies", "BranchId", c => c.Int());
            AddColumn("dbo.ApplicationUserComapnies", "BalanceSheetId", c => c.Int());
            AddColumn("dbo.ApplicationUserComapnies", "FinancialListId", c => c.Int());
            AddColumn("dbo.ApplicationUserComapnies", "ItemGroupId", c => c.Int());
            CreateIndex("dbo.ApplicationUserComapnies", "BranchId");
            CreateIndex("dbo.ApplicationUserComapnies", "BalanceSheetId");
            CreateIndex("dbo.ApplicationUserComapnies", "FinancialListId");
            CreateIndex("dbo.ApplicationUserComapnies", "ItemGroupId");
            AddForeignKey("dbo.ApplicationUserComapnies", "BalanceSheetId", "dbo.BalanceSheets", "BalanceSheetId");
            AddForeignKey("dbo.ApplicationUserComapnies", "BranchId", "dbo.Branches", "BranchId");
            AddForeignKey("dbo.ApplicationUserComapnies", "FinancialListId", "dbo.FinancialLists", "FinancialListId");
            AddForeignKey("dbo.ApplicationUserComapnies", "ItemGroupId", "dbo.ItemGroups", "ItemGroupId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ApplicationUserComapnies", "ItemGroupId", "dbo.ItemGroups");
            DropForeignKey("dbo.ApplicationUserComapnies", "FinancialListId", "dbo.FinancialLists");
            DropForeignKey("dbo.ApplicationUserComapnies", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.ApplicationUserComapnies", "BalanceSheetId", "dbo.BalanceSheets");
            DropIndex("dbo.ApplicationUserComapnies", new[] { "ItemGroupId" });
            DropIndex("dbo.ApplicationUserComapnies", new[] { "FinancialListId" });
            DropIndex("dbo.ApplicationUserComapnies", new[] { "BalanceSheetId" });
            DropIndex("dbo.ApplicationUserComapnies", new[] { "BranchId" });
            DropColumn("dbo.ApplicationUserComapnies", "ItemGroupId");
            DropColumn("dbo.ApplicationUserComapnies", "FinancialListId");
            DropColumn("dbo.ApplicationUserComapnies", "BalanceSheetId");
            DropColumn("dbo.ApplicationUserComapnies", "BranchId");
        }
    }
}
