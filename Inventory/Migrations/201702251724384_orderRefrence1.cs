namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orderRefrence1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AccountOrders", "IsSales", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AccountOrders", "IsSales");
        }
    }
}
