namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class purupdate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PurchaseOrders", "TransferTo_AccountId", "dbo.Accounts");
            DropIndex("dbo.PurchaseOrders", new[] { "TransferTo_AccountId" });
            DropColumn("dbo.PurchaseOrders", "TransferToId");
            RenameColumn(table: "dbo.PurchaseOrders", name: "TransferTo_AccountId", newName: "TransferToId");
            AlterColumn("dbo.PurchaseOrders", "TransferToId", c => c.Int(nullable: false));
            CreateIndex("dbo.PurchaseOrders", "TransferToId");
            AddForeignKey("dbo.PurchaseOrders", "TransferToId", "dbo.Accounts", "AccountId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PurchaseOrders", "TransferToId", "dbo.Accounts");
            DropIndex("dbo.PurchaseOrders", new[] { "TransferToId" });
            AlterColumn("dbo.PurchaseOrders", "TransferToId", c => c.Int());
            RenameColumn(table: "dbo.PurchaseOrders", name: "TransferToId", newName: "TransferTo_AccountId");
            AddColumn("dbo.PurchaseOrders", "TransferToId", c => c.Int(nullable: false));
            CreateIndex("dbo.PurchaseOrders", "TransferTo_AccountId");
            AddForeignKey("dbo.PurchaseOrders", "TransferTo_AccountId", "dbo.Accounts", "AccountId");
        }
    }
}
