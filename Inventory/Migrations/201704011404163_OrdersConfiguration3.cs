namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OrdersConfiguration3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PurchasesOrdersConfigurationPayMethods", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.SalessOrdersConfigurationPayMethods", "AccountId", "dbo.Accounts");
            DropIndex("dbo.PurchasesOrdersConfigurationPayMethods", new[] { "AccountId" });
            DropIndex("dbo.SalessOrdersConfigurationPayMethods", new[] { "AccountId" });
            DropColumn("dbo.PurchasesOrdersConfigurationPayMethods", "AccountId");
            DropColumn("dbo.SalessOrdersConfigurationPayMethods", "AccountId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SalessOrdersConfigurationPayMethods", "AccountId", c => c.Int(nullable: false));
            AddColumn("dbo.PurchasesOrdersConfigurationPayMethods", "AccountId", c => c.Int(nullable: false));
            CreateIndex("dbo.SalessOrdersConfigurationPayMethods", "AccountId");
            CreateIndex("dbo.PurchasesOrdersConfigurationPayMethods", "AccountId");
            AddForeignKey("dbo.SalessOrdersConfigurationPayMethods", "AccountId", "dbo.Accounts", "AccountId", cascadeDelete: true);
            AddForeignKey("dbo.PurchasesOrdersConfigurationPayMethods", "AccountId", "dbo.Accounts", "AccountId", cascadeDelete: true);
        }
    }
}
