namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class isnewOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderAccountsforSubAccountPurchases", "IsNewOrder", c => c.Boolean(nullable: false));
            AddColumn("dbo.OrderOtherTotalAccountPurchases", "IsNewOrder", c => c.Boolean(nullable: false));
            AddColumn("dbo.OrderAccountsforSubAccountSales", "IsNewOrder", c => c.Boolean(nullable: false));
            AddColumn("dbo.OrderOtherTotalAccountSales", "IsNewOrder", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderOtherTotalAccountSales", "IsNewOrder");
            DropColumn("dbo.OrderAccountsforSubAccountSales", "IsNewOrder");
            DropColumn("dbo.OrderOtherTotalAccountPurchases", "IsNewOrder");
            DropColumn("dbo.OrderAccountsforSubAccountPurchases", "IsNewOrder");
        }
    }
}
