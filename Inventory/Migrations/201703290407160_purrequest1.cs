namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class purrequest1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ItemsRequests",
                c => new
                    {
                        ItemsRequestId = c.Int(nullable: false, identity: true),
                        PurchaseOrderId = c.Int(nullable: false),
                        itemCode = c.String(),
                        itemName = c.String(),
                        qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        note = c.String(),
                        noteSecond = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        ItemGroupId = c.Int(nullable: false),
                        Realted = c.String(),
                    })
                .PrimaryKey(t => t.ItemsRequestId)
                .ForeignKey("dbo.ItemGroups", t => t.ItemGroupId, cascadeDelete: false)
                .ForeignKey("dbo.PurchaseOrders", t => t.PurchaseOrderId, cascadeDelete: false)
                .Index(t => t.PurchaseOrderId)
                .Index(t => t.ItemGroupId);
            
            CreateTable(
                "dbo.PurchaseOrders",
                c => new
                    {
                        PurchaseOrderId = c.Int(nullable: false, identity: true),
                        orderNo = c.Int(nullable: false),
                        orderDate = c.DateTime(nullable: false),
                        orderDateSecond = c.DateTime(nullable: false),
                        orderDateThird = c.DateTime(nullable: false),
                        userFirstId = c.String(maxLength: 128),
                        userSecondId = c.String(maxLength: 128),
                        userThirdId = c.String(maxLength: 128),
                        isDoneFirst = c.Boolean(nullable: false),
                        isDoneSecond = c.Boolean(nullable: false),
                        isDoneThird = c.Boolean(nullable: false),
                        cancel = c.Boolean(nullable: false),
                        isNewOrder = c.Boolean(nullable: false),
                        TransferToId = c.Int(nullable: false),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        cancelDate = c.DateTime(),
                        userCancelId = c.String(maxLength: 128),
                        TransferTo_AccountId = c.Int(),
                    })
                .PrimaryKey(t => t.PurchaseOrderId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.TransferTo_AccountId)
                .ForeignKey("dbo.AspNetUsers", t => t.userCancelId)
                .ForeignKey("dbo.AspNetUsers", t => t.userFirstId)
                .ForeignKey("dbo.AspNetUsers", t => t.userSecondId)
                .ForeignKey("dbo.AspNetUsers", t => t.userThirdId)
                .Index(t => t.userFirstId)
                .Index(t => t.userSecondId)
                .Index(t => t.userThirdId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId)
                .Index(t => t.userCancelId)
                .Index(t => t.TransferTo_AccountId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PurchaseOrders", "userThirdId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchaseOrders", "userSecondId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchaseOrders", "userFirstId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchaseOrders", "userCancelId", "dbo.AspNetUsers");
            DropForeignKey("dbo.PurchaseOrders", "TransferTo_AccountId", "dbo.Accounts");
            DropForeignKey("dbo.ItemsRequests", "PurchaseOrderId", "dbo.PurchaseOrders");
            DropForeignKey("dbo.PurchaseOrders", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.PurchaseOrders", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.ItemsRequests", "ItemGroupId", "dbo.ItemGroups");
            DropIndex("dbo.PurchaseOrders", new[] { "TransferTo_AccountId" });
            DropIndex("dbo.PurchaseOrders", new[] { "userCancelId" });
            DropIndex("dbo.PurchaseOrders", new[] { "CompanyId" });
            DropIndex("dbo.PurchaseOrders", new[] { "BranchId" });
            DropIndex("dbo.PurchaseOrders", new[] { "userThirdId" });
            DropIndex("dbo.PurchaseOrders", new[] { "userSecondId" });
            DropIndex("dbo.PurchaseOrders", new[] { "userFirstId" });
            DropIndex("dbo.ItemsRequests", new[] { "ItemGroupId" });
            DropIndex("dbo.ItemsRequests", new[] { "PurchaseOrderId" });
            DropTable("dbo.PurchaseOrders");
            DropTable("dbo.ItemsRequests");
        }
    }
}
