namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class billpropert : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BillPaymentPropertiesPurchases", "OrderPropertiesPayPurchasesId", "dbo.OrderPropertiesPayPurchases");
            DropForeignKey("dbo.BillPaymentPropertiesPurchases", "PurchasesBillId", "dbo.PurchasesBills");
            DropForeignKey("dbo.BillPaymentPropertiesPurchases", "SalesBill_SalesBillId", "dbo.SalesBills");
            DropIndex("dbo.BillPaymentPropertiesPurchases", new[] { "OrderPropertiesPayPurchasesId" });
            DropIndex("dbo.BillPaymentPropertiesPurchases", new[] { "PurchasesBillId" });
            DropIndex("dbo.BillPaymentPropertiesPurchases", new[] { "SalesBill_SalesBillId" });
            DropTable("dbo.BillPaymentPropertiesPurchases");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.BillPaymentPropertiesPurchases",
                c => new
                    {
                        BillPaymentPropertiesPurchasesId = c.Int(nullable: false, identity: true),
                        OrderPropertiesPayPurchasesId = c.Int(nullable: false),
                        PurchasesBillId = c.Int(nullable: false),
                        value = c.String(),
                        SalesBill_SalesBillId = c.Int(),
                    })
                .PrimaryKey(t => t.BillPaymentPropertiesPurchasesId);
            
            CreateIndex("dbo.BillPaymentPropertiesPurchases", "SalesBill_SalesBillId");
            CreateIndex("dbo.BillPaymentPropertiesPurchases", "PurchasesBillId");
            CreateIndex("dbo.BillPaymentPropertiesPurchases", "OrderPropertiesPayPurchasesId");
            AddForeignKey("dbo.BillPaymentPropertiesPurchases", "SalesBill_SalesBillId", "dbo.SalesBills", "SalesBillId");
            AddForeignKey("dbo.BillPaymentPropertiesPurchases", "PurchasesBillId", "dbo.PurchasesBills", "PurchasesBillId", cascadeDelete: true);
            AddForeignKey("dbo.BillPaymentPropertiesPurchases", "OrderPropertiesPayPurchasesId", "dbo.OrderPropertiesPayPurchases", "OrderPropertiesPayPurchasesId", cascadeDelete: true);
        }
    }
}
