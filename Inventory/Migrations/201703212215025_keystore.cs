namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class keystore : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.branchGroupSts");
            AlterColumn("dbo.branchGroupSts", "id", c => c.String());
            AlterColumn("dbo.branchGroupSts", "branchGroupStId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.branchGroupSts", "branchGroupStId");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.branchGroupSts");
            AlterColumn("dbo.branchGroupSts", "branchGroupStId", c => c.Int(nullable: false));
            AlterColumn("dbo.branchGroupSts", "id", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.branchGroupSts", "id");
        }
    }
}
