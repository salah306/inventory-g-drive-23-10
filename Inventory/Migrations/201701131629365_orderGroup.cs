namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orderGroup : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OrderAccountsforSubAccounts",
                c => new
                    {
                        OrderAccountsforSubAccountId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        OrderGroupId = c.Int(nullable: false),
                        DebitWhenAdd = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.OrderAccountsforSubAccountId)
                .ForeignKey("dbo.Accounts", t => t.AccountId,cascadeDelete: false)
                .ForeignKey("dbo.OrderGroups", t => t.OrderGroupId,cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.OrderGroupId);
            
            CreateTable(
                "dbo.OrderGroups",
                c => new
                    {
                        OrderGroupId = c.Int(nullable: false, identity: true),
                        OrderGroupName = c.String(),
                        IsCompany = c.Boolean(nullable: false),
                        CompanyBranchId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderGroupId);
            
            CreateTable(
                "dbo.OrderMainAccounts",
                c => new
                    {
                        OrderMainAccountId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        OrderGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderMainAccountId)
                .ForeignKey("dbo.Accounts", t => t.AccountId,cascadeDelete: false)
                .ForeignKey("dbo.OrderGroups", t => t.OrderGroupId,cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.OrderGroupId);
            
            CreateTable(
                "dbo.OrdersNames",
                c => new
                    {
                        OrdersNameId = c.Int(nullable: false, identity: true),
                        OrderName = c.String(),
                        OrderGroupId = c.Int(nullable: false),
                        IsDebit = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.OrdersNameId)
                .ForeignKey("dbo.OrderGroups", t => t.OrderGroupId,cascadeDelete: false)
                .Index(t => t.OrderGroupId);
            
            CreateTable(
                "dbo.OrderOtherTotalAccounts",
                c => new
                    {
                        OrderOtherTotalAccountId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        OrderGroupId = c.Int(nullable: false),
                        DebitWhenAdd = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.OrderOtherTotalAccountId)
                .ForeignKey("dbo.Accounts", t => t.AccountId,cascadeDelete: false)
                .ForeignKey("dbo.OrderGroups", t => t.OrderGroupId,cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.OrderGroupId);
            
            CreateTable(
                "dbo.OrderProperties",
                c => new
                    {
                        OrderPropertiesId = c.Int(nullable: false, identity: true),
                        OrderPropertiesName = c.Int(nullable: false),
                        OrderGroupId = c.Int(nullable: false),
                        IsRequired = c.Boolean(nullable: false),
                        OrderPropertyTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderPropertiesId)
                .ForeignKey("dbo.OrderGroups", t => t.OrderGroupId,cascadeDelete: false)
                .ForeignKey("dbo.OrderPropertyTypes", t => t.OrderPropertyTypeId,cascadeDelete: false)
                .Index(t => t.OrderGroupId)
                .Index(t => t.OrderPropertyTypeId);
            
            CreateTable(
                "dbo.OrderPropertyTypes",
                c => new
                    {
                        OrderPropertyTypeId = c.Int(nullable: false, identity: true),
                        OrderPropertyTypeName = c.String(),
                    })
                .PrimaryKey(t => t.OrderPropertyTypeId);
            
            CreateTable(
                "dbo.OrderTotalAccounts",
                c => new
                    {
                        OrderTotalAccountId = c.Int(nullable: false, identity: true),
                        AccountCategoryId = c.Int(nullable: false),
                        OrderGroupId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrderTotalAccountId)
                .ForeignKey("dbo.AccountCategories", t => t.AccountCategoryId,cascadeDelete: false)
                .ForeignKey("dbo.OrderGroups", t => t.OrderGroupId,cascadeDelete: false)
                .Index(t => t.AccountCategoryId)
                .Index(t => t.OrderGroupId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderTotalAccounts", "OrderGroupId", "dbo.OrderGroups");
            DropForeignKey("dbo.OrderTotalAccounts", "AccountCategoryId", "dbo.AccountCategories");
            DropForeignKey("dbo.OrderProperties", "OrderPropertyTypeId", "dbo.OrderPropertyTypes");
            DropForeignKey("dbo.OrderProperties", "OrderGroupId", "dbo.OrderGroups");
            DropForeignKey("dbo.OrderOtherTotalAccounts", "OrderGroupId", "dbo.OrderGroups");
            DropForeignKey("dbo.OrderOtherTotalAccounts", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrdersNames", "OrderGroupId", "dbo.OrderGroups");
            DropForeignKey("dbo.OrderMainAccounts", "OrderGroupId", "dbo.OrderGroups");
            DropForeignKey("dbo.OrderMainAccounts", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderAccountsforSubAccounts", "OrderGroupId", "dbo.OrderGroups");
            DropForeignKey("dbo.OrderAccountsforSubAccounts", "AccountId", "dbo.Accounts");
            DropIndex("dbo.OrderTotalAccounts", new[] { "OrderGroupId" });
            DropIndex("dbo.OrderTotalAccounts", new[] { "AccountCategoryId" });
            DropIndex("dbo.OrderProperties", new[] { "OrderPropertyTypeId" });
            DropIndex("dbo.OrderProperties", new[] { "OrderGroupId" });
            DropIndex("dbo.OrderOtherTotalAccounts", new[] { "OrderGroupId" });
            DropIndex("dbo.OrderOtherTotalAccounts", new[] { "AccountId" });
            DropIndex("dbo.OrdersNames", new[] { "OrderGroupId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "OrderGroupId" });
            DropIndex("dbo.OrderMainAccounts", new[] { "AccountId" });
            DropIndex("dbo.OrderAccountsforSubAccounts", new[] { "OrderGroupId" });
            DropIndex("dbo.OrderAccountsforSubAccounts", new[] { "AccountId" });
            DropTable("dbo.OrderTotalAccounts");
            DropTable("dbo.OrderPropertyTypes");
            DropTable("dbo.OrderProperties");
            DropTable("dbo.OrderOtherTotalAccounts");
            DropTable("dbo.OrdersNames");
            DropTable("dbo.OrderMainAccounts");
            DropTable("dbo.OrderGroups");
            DropTable("dbo.OrderAccountsforSubAccounts");
        }
    }
}
