namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class branchgroupOID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.branchGroupSts", "oId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.branchGroupSts", "oId");
        }
    }
}
