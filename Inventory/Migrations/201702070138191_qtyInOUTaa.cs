namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class qtyInOUTaa : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PropertyValuesforItemGroups", "RealteId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PropertyValuesforItemGroups", "RealteId");
        }
    }
}
