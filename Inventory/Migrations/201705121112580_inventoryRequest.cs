namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inventoryRequest : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InventoryRequests",
                c => new
                    {
                        InventoryRequestId = c.Int(nullable: false, identity: true),
                        orderNo = c.Int(nullable: false),
                        orderDate = c.DateTime(nullable: false),
                        orderDateSecond = c.DateTime(nullable: false),
                        orderDateThird = c.DateTime(nullable: false),
                        RecivedDate = c.DateTime(nullable: false),
                        userFirstId = c.String(maxLength: 128),
                        userSecondId = c.String(maxLength: 128),
                        userThirdId = c.String(maxLength: 128),
                        isDoneFirst = c.Boolean(nullable: false),
                        isDoneSecond = c.Boolean(nullable: false),
                        isDoneThird = c.Boolean(nullable: false),
                        cancel = c.Boolean(nullable: false),
                        isNewOrder = c.Boolean(nullable: false),
                        TransferToId = c.Int(nullable: false),
                        BranchId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        IsCompany = c.Boolean(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        cancelDate = c.DateTime(),
                        userCancelId = c.String(maxLength: 128),
                        orderONo = c.Int(),
                        OType = c.String(),
                        OrderType = c.String(),
                    })
                .PrimaryKey(t => t.InventoryRequestId)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.Accounts", t => t.TransferToId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.userCancelId)
                .ForeignKey("dbo.AspNetUsers", t => t.userFirstId)
                .ForeignKey("dbo.AspNetUsers", t => t.userSecondId)
                .ForeignKey("dbo.AspNetUsers", t => t.userThirdId)
                .Index(t => t.userFirstId)
                .Index(t => t.userSecondId)
                .Index(t => t.userThirdId)
                .Index(t => t.TransferToId)
                .Index(t => t.BranchId)
                .Index(t => t.CompanyId)
                .Index(t => t.userCancelId);
            
            CreateTable(
                "dbo.InventoryItemsRequests",
                c => new
                    {
                        InventoryItemsRequestId = c.Int(nullable: false, identity: true),
                        InventoryRequestId = c.Int(nullable: false),
                        SubAccountId = c.Int(nullable: false),
                        qty = c.Decimal(nullable: false, precision: 18, scale: 2),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        LastCostPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CurrentCostPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        note = c.String(),
                        noteSecond = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        Realted = c.String(),
                    })
                .PrimaryKey(t => t.InventoryItemsRequestId)
                .ForeignKey("dbo.InventoryRequests", t => t.InventoryRequestId, cascadeDelete: false)
                .ForeignKey("dbo.SubAccounts", t => t.SubAccountId, cascadeDelete: false)
                .Index(t => t.InventoryRequestId)
                .Index(t => t.SubAccountId);
            
            AddColumn("dbo.OrderOes", "InventoryRequestsId", c => c.Int(nullable: false));
            CreateIndex("dbo.OrderOes", "InventoryRequestsId");
            AddForeignKey("dbo.OrderOes", "InventoryRequestsId", "dbo.InventoryRequests", "InventoryRequestId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderOes", "InventoryRequestsId", "dbo.InventoryRequests");
            DropForeignKey("dbo.InventoryRequests", "userThirdId", "dbo.AspNetUsers");
            DropForeignKey("dbo.InventoryRequests", "userSecondId", "dbo.AspNetUsers");
            DropForeignKey("dbo.InventoryRequests", "userFirstId", "dbo.AspNetUsers");
            DropForeignKey("dbo.InventoryRequests", "userCancelId", "dbo.AspNetUsers");
            DropForeignKey("dbo.InventoryRequests", "TransferToId", "dbo.Accounts");
            DropForeignKey("dbo.InventoryItemsRequests", "SubAccountId", "dbo.SubAccounts");
            DropForeignKey("dbo.InventoryItemsRequests", "InventoryRequestId", "dbo.InventoryRequests");
            DropForeignKey("dbo.InventoryRequests", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.InventoryRequests", "BranchId", "dbo.Branches");
            DropIndex("dbo.InventoryItemsRequests", new[] { "SubAccountId" });
            DropIndex("dbo.InventoryItemsRequests", new[] { "InventoryRequestId" });
            DropIndex("dbo.InventoryRequests", new[] { "userCancelId" });
            DropIndex("dbo.InventoryRequests", new[] { "CompanyId" });
            DropIndex("dbo.InventoryRequests", new[] { "BranchId" });
            DropIndex("dbo.InventoryRequests", new[] { "TransferToId" });
            DropIndex("dbo.InventoryRequests", new[] { "userThirdId" });
            DropIndex("dbo.InventoryRequests", new[] { "userSecondId" });
            DropIndex("dbo.InventoryRequests", new[] { "userFirstId" });
            DropIndex("dbo.OrderOes", new[] { "InventoryRequestsId" });
            DropColumn("dbo.OrderOes", "InventoryRequestsId");
            DropTable("dbo.InventoryItemsRequests");
            DropTable("dbo.InventoryRequests");
        }
    }
}
