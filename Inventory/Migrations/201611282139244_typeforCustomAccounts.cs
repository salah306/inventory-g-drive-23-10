namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class typeforCustomAccounts : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomAccountCategories", "AccountTypeId", c => c.Int());
            AddColumn("dbo.CustomAccounts", "AccountTypeId", c => c.Int());
            AddColumn("dbo.CustomAccounts", "Code", c => c.Int(nullable: false));
            AddColumn("dbo.CustomBalanceSheetTypes", "AccountTypeId", c => c.Int());
            CreateIndex("dbo.CustomAccountCategories", "AccountTypeId");
            CreateIndex("dbo.CustomAccounts", "AccountTypeId");
            CreateIndex("dbo.CustomBalanceSheetTypes", "AccountTypeId");
            AddForeignKey("dbo.CustomAccountCategories", "AccountTypeId", "dbo.AccountTypes", "AccountTypeId");
            AddForeignKey("dbo.CustomAccounts", "AccountTypeId", "dbo.AccountTypes", "AccountTypeId");
            AddForeignKey("dbo.CustomBalanceSheetTypes", "AccountTypeId", "dbo.AccountTypes", "AccountTypeId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CustomBalanceSheetTypes", "AccountTypeId", "dbo.AccountTypes");
            DropForeignKey("dbo.CustomAccounts", "AccountTypeId", "dbo.AccountTypes");
            DropForeignKey("dbo.CustomAccountCategories", "AccountTypeId", "dbo.AccountTypes");
            DropIndex("dbo.CustomBalanceSheetTypes", new[] { "AccountTypeId" });
            DropIndex("dbo.CustomAccounts", new[] { "AccountTypeId" });
            DropIndex("dbo.CustomAccountCategories", new[] { "AccountTypeId" });
            DropColumn("dbo.CustomBalanceSheetTypes", "AccountTypeId");
            DropColumn("dbo.CustomAccounts", "Code");
            DropColumn("dbo.CustomAccounts", "AccountTypeId");
            DropColumn("dbo.CustomAccountCategories", "AccountTypeId");
        }
    }
}
