namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CutomOrder : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CustomSubAccountsOrders",
                c => new
                    {
                        CustomSubAccountsOrderId = c.Int(nullable: false, identity: true),
                        OrderTitle = c.String(),
                        CustomAccountName = c.String(),
                        CustomAccountId = c.Int(nullable: false),
                        IsPricePerUnit = c.Boolean(nullable: false),
                        AddQuantity = c.Boolean(nullable: false),
                        IsDebit = c.Boolean(nullable: false),
                        IsPercent = c.Boolean(nullable: false),
                        TotalName = c.String(),
                        TotalAccountId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        CustomAccount_AccountId = c.Int(),
                        TotalAccount_AccountId = c.Int(),
                    })
                .PrimaryKey(t => t.CustomSubAccountsOrderId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Accounts", t => t.CustomAccount_AccountId)
                .ForeignKey("dbo.Accounts", t => t.TotalAccount_AccountId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.CustomAccount_AccountId)
                .Index(t => t.TotalAccount_AccountId);
            
            CreateTable(
                "dbo.AccountsSubAccountMovements",
                c => new
                    {
                        AccountsSubAccountMovementId = c.Int(nullable: false, identity: true),
                        SubAccountMovementDate = c.DateTime(nullable: false),
                        AccountId = c.Int(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDebit = c.Boolean(nullable: false),
                        IsRow = c.Boolean(nullable: false),
                        IsPercent = c.Boolean(nullable: false),
                        SubAccountMovementId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.AccountsSubAccountMovementId)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.SubAccountMovements", t => t.SubAccountMovementId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.SubAccountMovementId)
                .Index(t => t.CurrentWorkerId);
            
            AddColumn("dbo.SubAccountOrders", "CustomSubAccountsOrderId", c => c.Int(nullable: false));
            CreateIndex("dbo.SubAccountOrders", "CustomSubAccountsOrderId");
            AddForeignKey("dbo.SubAccountOrders", "CustomSubAccountsOrderId", "dbo.CustomSubAccountsOrders", "CustomSubAccountsOrderId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccountsSubAccountMovements", "SubAccountMovementId", "dbo.SubAccountMovements");
            DropForeignKey("dbo.AccountsSubAccountMovements", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountsSubAccountMovements", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.CustomSubAccountsOrders", "TotalAccount_AccountId", "dbo.Accounts");
            DropForeignKey("dbo.SubAccountOrders", "CustomSubAccountsOrderId", "dbo.CustomSubAccountsOrders");
            DropForeignKey("dbo.CustomSubAccountsOrders", "CustomAccount_AccountId", "dbo.Accounts");
            DropForeignKey("dbo.CustomSubAccountsOrders", "CurrentWorkerId", "dbo.AspNetUsers");
            DropIndex("dbo.AccountsSubAccountMovements", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountsSubAccountMovements", new[] { "SubAccountMovementId" });
            DropIndex("dbo.AccountsSubAccountMovements", new[] { "AccountId" });
            DropIndex("dbo.CustomSubAccountsOrders", new[] { "TotalAccount_AccountId" });
            DropIndex("dbo.CustomSubAccountsOrders", new[] { "CustomAccount_AccountId" });
            DropIndex("dbo.CustomSubAccountsOrders", new[] { "CurrentWorkerId" });
            DropIndex("dbo.SubAccountOrders", new[] { "CustomSubAccountsOrderId" });
            DropColumn("dbo.SubAccountOrders", "CustomSubAccountsOrderId");
            DropTable("dbo.AccountsSubAccountMovements");
            DropTable("dbo.CustomSubAccountsOrders");
        }
    }
}
