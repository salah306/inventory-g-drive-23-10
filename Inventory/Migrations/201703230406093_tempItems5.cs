namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tempItems5 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TempItems", "code", c => c.String(nullable: false));
            AlterColumn("dbo.TempItems", "name", c => c.String(nullable: false));
            AlterColumn("dbo.TempItems", "Realted", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TempItems", "Realted", c => c.String());
            AlterColumn("dbo.TempItems", "name", c => c.String());
            AlterColumn("dbo.TempItems", "code", c => c.String());
        }
    }
}
