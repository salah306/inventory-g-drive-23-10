namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orderOes3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderOes", "PurchaseOrderId", c => c.Int());
            AddColumn("dbo.OrderOes", "requestOrderAttched", c => c.Boolean(nullable: false));
            CreateIndex("dbo.OrderOes", "PurchaseOrderId");
            AddForeignKey("dbo.OrderOes", "PurchaseOrderId", "dbo.PurchaseOrders", "PurchaseOrderId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrderOes", "PurchaseOrderId", "dbo.PurchaseOrders");
            DropIndex("dbo.OrderOes", new[] { "PurchaseOrderId" });
            DropColumn("dbo.OrderOes", "requestOrderAttched");
            DropColumn("dbo.OrderOes", "PurchaseOrderId");
        }
    }
}
