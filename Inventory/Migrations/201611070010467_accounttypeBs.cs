namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class accounttypeBs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BalanceSheets", "AccountTypeId", c => c.Int());
            CreateIndex("dbo.BalanceSheets", "AccountTypeId");
            AddForeignKey("dbo.BalanceSheets", "AccountTypeId", "dbo.AccountTypes", "AccountTypeId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BalanceSheets", "AccountTypeId", "dbo.AccountTypes");
            DropIndex("dbo.BalanceSheets", new[] { "AccountTypeId" });
            DropColumn("dbo.BalanceSheets", "AccountTypeId");
        }
    }
}
