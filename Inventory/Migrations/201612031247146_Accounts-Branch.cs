namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccountsBranch : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Accounts", "BranchId", c => c.Int());
            CreateIndex("dbo.Accounts", "BranchId");
            AddForeignKey("dbo.Accounts", "BranchId", "dbo.Branches", "BranchId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Accounts", "BranchId", "dbo.Branches");
            DropIndex("dbo.Accounts", new[] { "BranchId" });
            DropColumn("dbo.Accounts", "BranchId");
        }
    }
}
