namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newaxxountproperty : DbMigration
    {
        public override void Up()
        {
          
            
            DropForeignKey("dbo.AccountCategoryProperties", "AccountsDataTypesId", "dbo.AccountsDataTypes");
            //DropIndex("dbo.AccountCategoryProperties", new[] { "AccountsCatogeryTypesAcccountsId" });
            //DropIndex("dbo.AccountCategoryProperties", new[] { "AccountsDataTypesId" });
           // DropColumn("dbo.AccountCategoryProperties", "AccountsCatogeryTypesAcccountsId");
            DropColumn("dbo.AccountCategoryProperties", "AccountsDataTypesId");
        }
        
        public override void Down()
        {
           
        }
    }
}
