namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class accountssumdebit2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SubAccounts", "ItemInfoId", c => c.Int());
            CreateIndex("dbo.SubAccounts", "ItemInfoId");
            AddForeignKey("dbo.SubAccounts", "ItemInfoId", "dbo.TempItems", "TempItemId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SubAccounts", "ItemInfoId", "dbo.TempItems");
            DropIndex("dbo.SubAccounts", new[] { "ItemInfoId" });
            DropColumn("dbo.SubAccounts", "ItemInfoId");
        }
    }
}
