namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAccountToPaymethed : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PayOes", "PayId", c => c.Int(nullable: false));
            AddColumn("dbo.PayOes", "AccountId", c => c.Int(nullable: false));
            CreateIndex("dbo.PayOes", "AccountId");
            AddForeignKey("dbo.PayOes", "AccountId", "dbo.Accounts", "AccountId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PayOes", "AccountId", "dbo.Accounts");
            DropIndex("dbo.PayOes", new[] { "AccountId" });
            DropColumn("dbo.PayOes", "AccountId");
            DropColumn("dbo.PayOes", "PayId");
        }
    }
}
