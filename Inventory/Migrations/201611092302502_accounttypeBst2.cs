namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class accounttypeBst2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BalanceSheetTypes", "AccountTypeId", "dbo.AccountTypes");
            DropIndex("dbo.BalanceSheetTypes", new[] { "AccountTypeId" });
            AlterColumn("dbo.BalanceSheetTypes", "AccountTypeId", c => c.Int());
            CreateIndex("dbo.BalanceSheetTypes", "AccountTypeId");
            AddForeignKey("dbo.BalanceSheetTypes", "AccountTypeId", "dbo.AccountTypes", "AccountTypeId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BalanceSheetTypes", "AccountTypeId", "dbo.AccountTypes");
            DropIndex("dbo.BalanceSheetTypes", new[] { "AccountTypeId" });
            AlterColumn("dbo.BalanceSheetTypes", "AccountTypeId", c => c.Int(nullable: false));
            CreateIndex("dbo.BalanceSheetTypes", "AccountTypeId");
            AddForeignKey("dbo.BalanceSheetTypes", "AccountTypeId", "dbo.AccountTypes", "AccountTypeId", cascadeDelete: true);
        }
    }
}
