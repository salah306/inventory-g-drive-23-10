// <auto-generated />
namespace Inventory.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class billProperty : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(billProperty));
        
        string IMigrationMetadata.Id
        {
            get { return "201702190326187_billProperty"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
