namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class sunaccountmove : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SubAccountOrders",
                c => new
                    {
                        SubAccountOrderId = c.Int(nullable: false, identity: true),
                        OrderNote = c.String(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.SubAccountOrderId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CurrentWorkerId);
            
            CreateTable(
                "dbo.SubAccountMovements",
                c => new
                    {
                        SubAccountMovementId = c.Int(nullable: false, identity: true),
                        SubAccountMovementDate = c.DateTime(nullable: false),
                        SubAccountId = c.Int(nullable: false),
                        AccountorderHasDone = c.Boolean(),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        OrderId = c.Int(nullable: false),
                        OrderNote = c.String(),
                        SubAccountOrderId = c.Int(nullable: false),
                        Quantity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IsDebit = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.SubAccountMovementId)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.SubAccounts", t => t.SubAccountId, cascadeDelete: true)
                .ForeignKey("dbo.SubAccountOrders", t => t.SubAccountOrderId, cascadeDelete: true)
                .Index(t => t.SubAccountId)
                .Index(t => t.CurrentWorkerId)
                .Index(t => t.SubAccountOrderId);
            
            AddColumn("dbo.Accounts", "Activated", c => c.Boolean(nullable: false));
            AddColumn("dbo.AccountMovements", "SubAccountOrderId", c => c.Int());
            CreateIndex("dbo.AccountMovements", "SubAccountOrderId");
            AddForeignKey("dbo.AccountMovements", "SubAccountOrderId", "dbo.SubAccountOrders", "SubAccountOrderId");
            DropColumn("dbo.SubAccounts", "Quantity");
            DropColumn("dbo.SubAccounts", "Price");
            DropColumn("dbo.SubAccounts", "IsDebit");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SubAccounts", "IsDebit", c => c.Boolean(nullable: false));
            AddColumn("dbo.SubAccounts", "Price", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.SubAccounts", "Quantity", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropForeignKey("dbo.SubAccountMovements", "SubAccountOrderId", "dbo.SubAccountOrders");
            DropForeignKey("dbo.SubAccountMovements", "SubAccountId", "dbo.SubAccounts");
            DropForeignKey("dbo.SubAccountMovements", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.SubAccountOrders", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AccountMovements", "SubAccountOrderId", "dbo.SubAccountOrders");
            DropIndex("dbo.SubAccountMovements", new[] { "SubAccountOrderId" });
            DropIndex("dbo.SubAccountMovements", new[] { "CurrentWorkerId" });
            DropIndex("dbo.SubAccountMovements", new[] { "SubAccountId" });
            DropIndex("dbo.SubAccountOrders", new[] { "CurrentWorkerId" });
            DropIndex("dbo.AccountMovements", new[] { "SubAccountOrderId" });
            DropColumn("dbo.AccountMovements", "SubAccountOrderId");
            DropColumn("dbo.Accounts", "Activated");
            DropTable("dbo.SubAccountMovements");
            DropTable("dbo.SubAccountOrders");
        }
    }
}
