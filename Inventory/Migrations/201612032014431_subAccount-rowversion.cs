namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class subAccountrowversion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SubAccounts", "RowVersion", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
            AddColumn("dbo.SubAccounts", "CurrentWorkerId", c => c.String(maxLength: 128));
            CreateIndex("dbo.SubAccounts", "CurrentWorkerId");
            AddForeignKey("dbo.SubAccounts", "CurrentWorkerId", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SubAccounts", "CurrentWorkerId", "dbo.AspNetUsers");
            DropIndex("dbo.SubAccounts", new[] { "CurrentWorkerId" });
            DropColumn("dbo.SubAccounts", "CurrentWorkerId");
            DropColumn("dbo.SubAccounts", "RowVersion");
        }
    }
}
