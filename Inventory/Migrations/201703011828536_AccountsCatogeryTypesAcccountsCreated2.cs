namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccountsCatogeryTypesAcccountsCreated2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AccountCategoryProperties", "AccountCategoryId", "dbo.AccountCategories");
            DropIndex("dbo.AccountCategoryProperties", new[] { "AccountCategoryId" });
            RenameColumn(table: "dbo.AccountCategoryProperties", name: "DataTypeId", newName: "AccountsCatogeryTypesAcccountsId");
            RenameIndex(table: "dbo.AccountCategoryProperties", name: "IX_DataTypeId", newName: "IX_AccountsCatogeryTypesAcccountsId");
            AddColumn("dbo.AccountCategoryProperties", "AccountsDataTypesId", c => c.Int(nullable: false));
            CreateIndex("dbo.AccountCategoryProperties", "AccountsDataTypesId");
            AddForeignKey("dbo.AccountCategoryProperties", "AccountsDataTypesId", "dbo.AccountsDataTypes", "Id",  cascadeDelete: false);
            DropColumn("dbo.AccountCategoryProperties", "AccountCategoryId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AccountCategoryProperties", "AccountCategoryId", c => c.Int(nullable: false));
            DropForeignKey("dbo.AccountCategoryProperties", "AccountsDataTypesId", "dbo.AccountsDataTypes");
            DropIndex("dbo.AccountCategoryProperties", new[] { "AccountsDataTypesId" });
            DropColumn("dbo.AccountCategoryProperties", "AccountsDataTypesId");
            RenameIndex(table: "dbo.AccountCategoryProperties", name: "IX_AccountsCatogeryTypesAcccountsId", newName: "IX_DataTypeId");
            RenameColumn(table: "dbo.AccountCategoryProperties", name: "AccountsCatogeryTypesAcccountsId", newName: "DataTypeId");
            CreateIndex("dbo.AccountCategoryProperties", "AccountCategoryId");
            AddForeignKey("dbo.AccountCategoryProperties", "AccountCategoryId", "dbo.AccountCategories", "AccountCategoryId",  cascadeDelete: false);
        }
    }
}
