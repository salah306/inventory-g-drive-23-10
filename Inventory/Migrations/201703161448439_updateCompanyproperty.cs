namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateCompanyproperty : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CompanyProperties",
                c => new
                    {
                        CompanyPropertiesId = c.Int(nullable: false, identity: true),
                        CompanyId = c.Int(nullable: false),
                        propertyName = c.String(),
                        AccountsCatogeryTypesAcccountsId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.CompanyPropertiesId)
                .ForeignKey("dbo.AccountsCatogeryTypesAcccounts", t => t.AccountsCatogeryTypesAcccountsId, cascadeDelete: false)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .Index(t => t.CompanyId)
                .Index(t => t.AccountsCatogeryTypesAcccountsId)
                .Index(t => t.CurrentWorkerId);
            
            AddColumn("dbo.AspNetUsers", "MaxCompany", c => c.Int());
            AddColumn("dbo.Companies", "MaxBranch", c => c.Int());
            AddColumn("dbo.Companies", "startDate", c => c.DateTime());
            AddColumn("dbo.Companies", "endDate", c => c.DateTime());
            AddColumn("dbo.Companies", "closed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CompanyProperties", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.CompanyProperties", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.CompanyProperties", "AccountsCatogeryTypesAcccountsId", "dbo.AccountsCatogeryTypesAcccounts");
            DropIndex("dbo.CompanyProperties", new[] { "CurrentWorkerId" });
            DropIndex("dbo.CompanyProperties", new[] { "AccountsCatogeryTypesAcccountsId" });
            DropIndex("dbo.CompanyProperties", new[] { "CompanyId" });
            DropColumn("dbo.Companies", "closed");
            DropColumn("dbo.Companies", "endDate");
            DropColumn("dbo.Companies", "startDate");
            DropColumn("dbo.Companies", "MaxBranch");
            DropColumn("dbo.AspNetUsers", "MaxCompany");
            DropTable("dbo.CompanyProperties");
        }
    }
}
