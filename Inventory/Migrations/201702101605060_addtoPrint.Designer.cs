// <auto-generated />
namespace Inventory.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addtoPrint : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addtoPrint));
        
        string IMigrationMetadata.Id
        {
            get { return "201702101605060_addtoPrint"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
