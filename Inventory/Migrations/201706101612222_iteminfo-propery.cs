namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class iteminfopropery : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TempItempropertyValues",
                c => new
                    {
                        TempItempropertyValuesId = c.Int(nullable: false, identity: true),
                        TempItemId = c.Int(),
                        PropertyValuesforItemGroupId = c.Int(),
                    })
                .PrimaryKey(t => t.TempItempropertyValuesId)
                .ForeignKey("dbo.PropertyValuesforItemGroups", t => t.PropertyValuesforItemGroupId)
                .ForeignKey("dbo.TempItems", t => t.TempItemId)
                .Index(t => t.TempItemId)
                .Index(t => t.PropertyValuesforItemGroupId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TempItempropertyValues", "TempItemId", "dbo.TempItems");
            DropForeignKey("dbo.TempItempropertyValues", "PropertyValuesforItemGroupId", "dbo.PropertyValuesforItemGroups");
            DropIndex("dbo.TempItempropertyValues", new[] { "PropertyValuesforItemGroupId" });
            DropIndex("dbo.TempItempropertyValues", new[] { "TempItemId" });
            DropTable("dbo.TempItempropertyValues");
        }
    }
}
