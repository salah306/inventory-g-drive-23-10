namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class remvebranch : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BalanceSheets", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.BalanceSheets", "CompanyId", "dbo.Companies");
            DropIndex("dbo.BalanceSheets", "AK_BalanceSheet_BalanceSheetName");
            DropIndex("dbo.BalanceSheets", new[] { "CompanyId" });
            RenameColumn(table: "dbo.BalanceSheets", name: "BranchId", newName: "Branch_BranchId");
            AlterColumn("dbo.BalanceSheets", "Branch_BranchId", c => c.Int());
            AlterColumn("dbo.BalanceSheets", "CompanyId", c => c.Int(nullable: false));
            CreateIndex("dbo.BalanceSheets", new[] { "BalanceSheetName", "CompanyId" }, unique: true, name: "AK_BalanceSheet_BalanceSheetName");
            CreateIndex("dbo.BalanceSheets", "Branch_BranchId");
            AddForeignKey("dbo.BalanceSheets", "Branch_BranchId", "dbo.Branches", "BranchId");
            AddForeignKey("dbo.BalanceSheets", "CompanyId", "dbo.Companies", "CompanyId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BalanceSheets", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.BalanceSheets", "Branch_BranchId", "dbo.Branches");
            DropIndex("dbo.BalanceSheets", new[] { "Branch_BranchId" });
            DropIndex("dbo.BalanceSheets", "AK_BalanceSheet_BalanceSheetName");
            AlterColumn("dbo.BalanceSheets", "CompanyId", c => c.Int());
            AlterColumn("dbo.BalanceSheets", "Branch_BranchId", c => c.Int(nullable: false));
            RenameColumn(table: "dbo.BalanceSheets", name: "Branch_BranchId", newName: "BranchId");
            CreateIndex("dbo.BalanceSheets", "CompanyId");
            CreateIndex("dbo.BalanceSheets", new[] { "BalanceSheetName", "BranchId" }, unique: true, name: "AK_BalanceSheet_BalanceSheetName");
            AddForeignKey("dbo.BalanceSheets", "CompanyId", "dbo.Companies", "CompanyId");
            AddForeignKey("dbo.BalanceSheets", "BranchId", "dbo.Branches", "BranchId", cascadeDelete: true);
        }
    }
}
