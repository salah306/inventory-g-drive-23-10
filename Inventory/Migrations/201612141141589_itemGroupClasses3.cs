namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class itemGroupClasses3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TypeforItemGroups", "Value", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.TypeforItemGroups", "Value");
        }
    }
}
