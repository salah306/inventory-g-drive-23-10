namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removeorderfrombills : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BillAccountItems", "SubAccountMovementId", "dbo.SubAccountMovements");
            DropForeignKey("dbo.BillSubAccountItems", "SubAccountMovementId", "dbo.SubAccountMovements");
            DropForeignKey("dbo.PurchasesBills", "BillOtherAccountOrderId", "dbo.AccountOrders");
            DropForeignKey("dbo.PurchasesBills", "SubAccountOrderId", "dbo.SubAccountOrders");
            DropForeignKey("dbo.SalesBills", "BillOtherAccountOrderId", "dbo.AccountOrders");
            DropForeignKey("dbo.SalesBills", "SubAccountOrderId", "dbo.SubAccountOrders");
            DropIndex("dbo.BillAccountItems", new[] { "SubAccountMovementId" });
            DropIndex("dbo.BillSubAccountItems", new[] { "SubAccountMovementId" });
            DropIndex("dbo.PurchasesBills", new[] { "SubAccountOrderId" });
            DropIndex("dbo.PurchasesBills", new[] { "BillOtherAccountOrderId" });
            DropIndex("dbo.SalesBills", new[] { "SubAccountOrderId" });
            DropIndex("dbo.SalesBills", new[] { "BillOtherAccountOrderId" });
            AddColumn("dbo.BillAccountItems", "SalesBillId", c => c.Int());
            AddColumn("dbo.BillAccountItems", "PurchasesBillId", c => c.Int());
            AddColumn("dbo.BillSubAccountItems", "SalesBillId", c => c.Int());
            AddColumn("dbo.BillSubAccountItems", "PurchasesBillId", c => c.Int());
            CreateIndex("dbo.BillAccountItems", "SalesBillId");
            CreateIndex("dbo.BillAccountItems", "PurchasesBillId");
            CreateIndex("dbo.BillSubAccountItems", "SalesBillId");
            CreateIndex("dbo.BillSubAccountItems", "PurchasesBillId");
            AddForeignKey("dbo.BillAccountItems", "PurchasesBillId", "dbo.PurchasesBills", "PurchasesBillId");
            AddForeignKey("dbo.BillAccountItems", "SalesBillId", "dbo.SalesBills", "SalesBillId");
            AddForeignKey("dbo.BillSubAccountItems", "PurchasesBillId", "dbo.PurchasesBills", "PurchasesBillId");
            AddForeignKey("dbo.BillSubAccountItems", "SalesBillId", "dbo.SalesBills", "SalesBillId");
            DropColumn("dbo.BillAccountItems", "SubAccountMovementId");
            DropColumn("dbo.BillSubAccountItems", "SubAccountMovementId");
            DropColumn("dbo.PurchasesBills", "SubAccountOrderId");
            DropColumn("dbo.PurchasesBills", "BillOtherAccountOrderId");
            DropColumn("dbo.SalesBills", "SubAccountOrderId");
            DropColumn("dbo.SalesBills", "BillOtherAccountOrderId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SalesBills", "BillOtherAccountOrderId", c => c.Int());
            AddColumn("dbo.SalesBills", "SubAccountOrderId", c => c.Int(nullable: false));
            AddColumn("dbo.PurchasesBills", "BillOtherAccountOrderId", c => c.Int());
            AddColumn("dbo.PurchasesBills", "SubAccountOrderId", c => c.Int(nullable: false));
            AddColumn("dbo.BillSubAccountItems", "SubAccountMovementId", c => c.Int(nullable: false));
            AddColumn("dbo.BillAccountItems", "SubAccountMovementId", c => c.Int(nullable: false));
            DropForeignKey("dbo.BillSubAccountItems", "SalesBillId", "dbo.SalesBills");
            DropForeignKey("dbo.BillSubAccountItems", "PurchasesBillId", "dbo.PurchasesBills");
            DropForeignKey("dbo.BillAccountItems", "SalesBillId", "dbo.SalesBills");
            DropForeignKey("dbo.BillAccountItems", "PurchasesBillId", "dbo.PurchasesBills");
            DropIndex("dbo.BillSubAccountItems", new[] { "PurchasesBillId" });
            DropIndex("dbo.BillSubAccountItems", new[] { "SalesBillId" });
            DropIndex("dbo.BillAccountItems", new[] { "PurchasesBillId" });
            DropIndex("dbo.BillAccountItems", new[] { "SalesBillId" });
            DropColumn("dbo.BillSubAccountItems", "PurchasesBillId");
            DropColumn("dbo.BillSubAccountItems", "SalesBillId");
            DropColumn("dbo.BillAccountItems", "PurchasesBillId");
            DropColumn("dbo.BillAccountItems", "SalesBillId");
            CreateIndex("dbo.SalesBills", "BillOtherAccountOrderId");
            CreateIndex("dbo.SalesBills", "SubAccountOrderId");
            CreateIndex("dbo.PurchasesBills", "BillOtherAccountOrderId");
            CreateIndex("dbo.PurchasesBills", "SubAccountOrderId");
            CreateIndex("dbo.BillSubAccountItems", "SubAccountMovementId");
            CreateIndex("dbo.BillAccountItems", "SubAccountMovementId");
            AddForeignKey("dbo.SalesBills", "SubAccountOrderId", "dbo.SubAccountOrders", "SubAccountOrderId", cascadeDelete: false);
            AddForeignKey("dbo.SalesBills", "BillOtherAccountOrderId", "dbo.AccountOrders", "AccountOrderId");
            AddForeignKey("dbo.PurchasesBills", "SubAccountOrderId", "dbo.SubAccountOrders", "SubAccountOrderId", cascadeDelete: false);
            AddForeignKey("dbo.PurchasesBills", "BillOtherAccountOrderId", "dbo.AccountOrders", "AccountOrderId");
            AddForeignKey("dbo.BillSubAccountItems", "SubAccountMovementId", "dbo.SubAccountMovements", "SubAccountMovementId", cascadeDelete: false);
            AddForeignKey("dbo.BillAccountItems", "SubAccountMovementId", "dbo.SubAccountMovements", "SubAccountMovementId", cascadeDelete: false);
        }
    }
}
