namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class orderupdates2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AccountOrders", "AccountsDataTypesId", "dbo.AccountsDataTypes");
            DropForeignKey("dbo.AccountOrders", "AccountsOtherDataTypesId", "dbo.AccountsDataTypes");
            DropForeignKey("dbo.AccountOrders", "OrderRequestId", "dbo.OrderRequests");
            DropIndex("dbo.AccountOrders", new[] { "OrderRequestId" });
            DropIndex("dbo.AccountOrders", new[] { "AccountsDataTypesId" });
            DropIndex("dbo.AccountOrders", new[] { "AccountsOtherDataTypesId" });
            CreateTable(
                "dbo.OrderMoveRequests",
                c => new
                    {
                        OrderMoveRequestId = c.Int(nullable: false, identity: true),
                        OrderNo = c.Int(nullable: false),
                        mainAccountId = c.Int(nullable: false),
                        tilteAccountId = c.Int(nullable: false),
                        OrderDate = c.DateTime(nullable: false),
                        typeName = c.String(),
                        isDone = c.Boolean(nullable: false),
                        isToCancel = c.Boolean(nullable: false),
                        attchedRequest = c.Boolean(nullable: false),
                        orderRequestId = c.Int(),
                        CompanyId = c.Int(nullable: false),
                        BranchId = c.Int(),
                        AccountsDataTypesId = c.Int(nullable: false),
                        RowVersion = c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"),
                        CurrentWorkerId = c.String(maxLength: 128),
                        note = c.String(),
                        amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.OrderMoveRequestId)
                .ForeignKey("dbo.AccountsDataTypes", t => t.AccountsDataTypesId, cascadeDelete: false)
                .ForeignKey("dbo.Branches", t => t.BranchId)
                .ForeignKey("dbo.Companies", t => t.CompanyId, cascadeDelete: false)
                .ForeignKey("dbo.AspNetUsers", t => t.CurrentWorkerId)
                .ForeignKey("dbo.Accounts", t => t.mainAccountId, cascadeDelete: false)
                .ForeignKey("dbo.OrderRequests", t => t.orderRequestId)
                .ForeignKey("dbo.Accounts", t => t.tilteAccountId, cascadeDelete: false)
                .Index(t => t.mainAccountId)
                .Index(t => t.tilteAccountId)
                .Index(t => t.orderRequestId)
                .Index(t => t.CompanyId)
                .Index(t => t.BranchId)
                .Index(t => t.AccountsDataTypesId)
                .Index(t => t.CurrentWorkerId);
            
            AddColumn("dbo.AccountOrders", "OrderMoveRequestId", c => c.Int());
            CreateIndex("dbo.AccountOrders", "OrderMoveRequestId");
            AddForeignKey("dbo.AccountOrders", "OrderMoveRequestId", "dbo.OrderMoveRequests", "OrderMoveRequestId");
            DropColumn("dbo.AccountOrders", "OrderRequestId");
            DropColumn("dbo.AccountOrders", "AccountsDataTypesId");
            DropColumn("dbo.AccountOrders", "AccountsOtherDataTypesId");
            DropColumn("dbo.AccountOrders", "typeName");
            DropColumn("dbo.AccountOrders", "typeNameSecond");
            DropColumn("dbo.AccountOrders", "HasDoneFirst");
            DropColumn("dbo.AccountOrders", "HasDoneSecond");
            DropColumn("dbo.AccountOrders", "HasCanceled");
            DropColumn("dbo.AccountOrders", "attchedRequest");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AccountOrders", "attchedRequest", c => c.Boolean());
            AddColumn("dbo.AccountOrders", "HasCanceled", c => c.Boolean());
            AddColumn("dbo.AccountOrders", "HasDoneSecond", c => c.Boolean());
            AddColumn("dbo.AccountOrders", "HasDoneFirst", c => c.Boolean());
            AddColumn("dbo.AccountOrders", "typeNameSecond", c => c.String());
            AddColumn("dbo.AccountOrders", "typeName", c => c.String());
            AddColumn("dbo.AccountOrders", "AccountsOtherDataTypesId", c => c.Int());
            AddColumn("dbo.AccountOrders", "AccountsDataTypesId", c => c.Int());
            AddColumn("dbo.AccountOrders", "OrderRequestId", c => c.Int());
            DropForeignKey("dbo.AccountOrders", "OrderMoveRequestId", "dbo.OrderMoveRequests");
            DropForeignKey("dbo.OrderMoveRequests", "tilteAccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMoveRequests", "orderRequestId", "dbo.OrderRequests");
            DropForeignKey("dbo.OrderMoveRequests", "mainAccountId", "dbo.Accounts");
            DropForeignKey("dbo.OrderMoveRequests", "CurrentWorkerId", "dbo.AspNetUsers");
            DropForeignKey("dbo.OrderMoveRequests", "CompanyId", "dbo.Companies");
            DropForeignKey("dbo.OrderMoveRequests", "BranchId", "dbo.Branches");
            DropForeignKey("dbo.OrderMoveRequests", "AccountsDataTypesId", "dbo.AccountsDataTypes");
            DropIndex("dbo.OrderMoveRequests", new[] { "CurrentWorkerId" });
            DropIndex("dbo.OrderMoveRequests", new[] { "AccountsDataTypesId" });
            DropIndex("dbo.OrderMoveRequests", new[] { "BranchId" });
            DropIndex("dbo.OrderMoveRequests", new[] { "CompanyId" });
            DropIndex("dbo.OrderMoveRequests", new[] { "orderRequestId" });
            DropIndex("dbo.OrderMoveRequests", new[] { "tilteAccountId" });
            DropIndex("dbo.OrderMoveRequests", new[] { "mainAccountId" });
            DropIndex("dbo.AccountOrders", new[] { "OrderMoveRequestId" });
            DropColumn("dbo.AccountOrders", "OrderMoveRequestId");
            DropTable("dbo.OrderMoveRequests");
            CreateIndex("dbo.AccountOrders", "AccountsOtherDataTypesId");
            CreateIndex("dbo.AccountOrders", "AccountsDataTypesId");
            CreateIndex("dbo.AccountOrders", "OrderRequestId");
            AddForeignKey("dbo.AccountOrders", "OrderRequestId", "dbo.OrderRequests", "OrderRequestId");
            AddForeignKey("dbo.AccountOrders", "AccountsOtherDataTypesId", "dbo.AccountsDataTypes", "Id");
            AddForeignKey("dbo.AccountOrders", "AccountsDataTypesId", "dbo.AccountsDataTypes", "Id");
        }
    }
}
