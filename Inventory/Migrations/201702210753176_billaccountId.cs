namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class billaccountId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BillAccountItems", "BillSubAccountItemsId", c => c.Int(nullable: false));
            CreateIndex("dbo.BillAccountItems", "BillSubAccountItemsId");
            AddForeignKey("dbo.BillAccountItems", "BillSubAccountItemsId", "dbo.BillSubAccountItems", "BillSubAccountItemsId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BillAccountItems", "BillSubAccountItemsId", "dbo.BillSubAccountItems");
            DropIndex("dbo.BillAccountItems", new[] { "BillSubAccountItemsId" });
            DropColumn("dbo.BillAccountItems", "BillSubAccountItemsId");
        }
    }
}
