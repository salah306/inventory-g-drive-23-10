namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class editrealted : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PropertyValuesforItemGroups", "Realted", c => c.String());
            DropColumn("dbo.PropertyValuesforItemGroups", "RealteId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PropertyValuesforItemGroups", "RealteId", c => c.Int());
            DropColumn("dbo.PropertyValuesforItemGroups", "Realted");
        }
    }
}
