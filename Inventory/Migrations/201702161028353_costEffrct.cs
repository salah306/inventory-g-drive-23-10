namespace Inventory.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class costEffrct : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.OrderAccountsforSubAccountPurchases", "CostEffect", c => c.Boolean(nullable: false));
            AddColumn("dbo.OrderOtherTotalAccountPurchases", "CostEffect", c => c.Boolean(nullable: false));
            AddColumn("dbo.OrderAccountsforSubAccountSales", "CostEffect", c => c.Boolean(nullable: false));
            AddColumn("dbo.OrderOtherTotalAccountSales", "CostEffect", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.OrderOtherTotalAccountSales", "CostEffect");
            DropColumn("dbo.OrderAccountsforSubAccountSales", "CostEffect");
            DropColumn("dbo.OrderOtherTotalAccountPurchases", "CostEffect");
            DropColumn("dbo.OrderAccountsforSubAccountPurchases", "CostEffect");
        }
    }
}
