﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.ViewModels
{
    public class AccountOrderVm
    {
        public string id { get; set; }
        public DateTime name { get; set; }
        public DateTime date { get; set; }
        public AccountMovementVm AccountMovementFrom{ get; set; }
        public List<AccountMovementVm> AccountMovementsTo { get; set; }
        public AccountOrderPropertiesVm AccountOrderProperties { get; set; }
        public Int32 PublicId { get; set; }



    }

    public class OrderVm
    {
        public Int32 orderId { get; set; }
        public Int32 orderNo { get; set; }
        public string orderNote { get; set; }
        public Int32? branchId { get; set; }
        public Int32? CompanyId { get; set; }
        public DateTime orderDate { get; set; }
        public List<OrderAccounts> debitorAccounts { get; set; }
        public List<OrderAccounts> crditorAccounts { get; set; }
        public bool IsCompany { get; set; }


    }
    public class isCompanyBranch
    {
        public Int32 Id { get; set; }
        public bool IsCompany { get; set; }
        public Int32? groupId { get; set; }
        public Int32? gId { get; set; }
    }

  

    public class getOrders
    {
        //int id,int pageSize, int pageNumber, string startDate, string endDate
        public Int32 id { get; set; }
        public Int32 pageSize { get; set; }
        public Int32 pageNumber { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public string search { get; set; }
        public bool isCompany { get; set; }


    }

    public class OrderAccounts
    {
        public decimal amount { get; set; }
        public AccountObject account { get; set; }
        public Int32? id { get; set; }

    }

    public class AccountObject
    {
        public Int32 id { get; set; }
        public string value { get; set; }
        public string AccountCatogery { get; set; }
        public bool status { get; set; }
        public string Key { get; set; }
    }

}