﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.ViewModels
{
    public class AccountMovementName
    {
        public string accountOrderPropertiesName { get; set; }
        public string accountName { get; set; }
        public string date { get; set; }
        public Int32 PublicId { get; set; }

    }
}