angular.module('inventoryModule').controller('EasyStoreUsersController', function ($rootScope, $scope, $http, $timeout, _, settings, $filter,
                                                           EasyStoreUserspersistenceService, Offline,
                                                           $location, $sce, authService, globalService, $q, $log, DTOptionsBuilder, DTColumnDefBuilder) {
    $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();
    });

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
    $scope.totalItems = 0;
    $scope.currentPage = 1;
    $scope.itemsperpage = 50;
    $scope.search = "";
    $scope.users = [];

    var vm = this;
    vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(2);

    $scope.$watch('itemsperpage', function (newVal, oldVal) {
        if (newVal !== oldVal && $scope.itemsperpagechange) {
            if (newVal > $scope.totalItems) {
                $scope.itemsperpage = angular.copy($scope.totalItems);
                if ($scope.itemsperpage > 200) {
                    $scope.itemsperpage = 200;
                }
            }
            $scope.getUsers();

        }
        $scope.itemsperpagechange = true;

    });

    $scope.pageChanged = function () {
        $scope.getUsers();
    };

    $scope.setSearch = function () {
        $scope.getUsers();
    };
    

    $scope.getUsers = function () {

        EasyStoreUserspersistenceService.action.getAllUsers(JSON.stringify({
            pageSize: $scope.itemsperpage,
            pageNumber: $scope.currentPage,
            search : $scope.search
        })).then(
           function (result) {
               console.log('Moves', result)
               $scope.totalItems = result.data.totalCount
               $scope.users = result.data.users;
           },
           function (error) {
               toastr.error("حدث خطاء - اثناء طلب البيانات");
           });
    }
    $scope.getUsers();
    $scope.selectedUser = {};

    $scope.showRole = false;

    $scope.getRoles = function (user) {
        $scope.selectedUser = angular.copy(user);
        //REquest It from Server
        $scope.selectedUser.settings = [
            {
                settingsId : 1,
                companyId: 1, companyName: 'ليدر للشحن',
                properties: [
                    {
                        propertiesId: 1, propertyName: "الفروع", roles:
                        [
                            {
                            rolesId: 1, roleName: "فرع 1", active: true,
                            roleproperties: [{ rolepropertiesId: 1, rolepropertyName : "يستطيع التعديل" , isTrue : false, typeRefrence : 'account' , typeRefrenceId: 12}]
                            
                            }
                        ]
                    },
                     {
                         propertiesId: 1, propertyName: "اعداد الحسابات", roles:
                         [
                             {
                                 rolesId: 1, roleName: "الحسابات", active: true,
                                 roleproperties: [
                                     { rolepropertiesId: 1, rolepropertyName: "يستطيع التعديل", isTrue: false, typeRefrence: 'account', typeRefrenceId: 12 },
                                     { rolepropertiesId: 1, rolepropertyName: "يستطيع الحذف", isTrue: false, typeRefrence: 'account', typeRefrenceId: 12 }
                             ]

                             }
                         ]
                     }

                ]
            }
        ]

        //getUserSettings

        EasyStoreUserspersistenceService.action.getUserSettings($scope.selectedUser.userId).then(
               function (result) {
                   console.log('settings', result)
                   $scope.selectedUser.settings = result;
                   $scope.showRole = true;
               },
               function (error) {
                   toastr.error("حدث خطاء - اثناء طلب البيانات");
               });
        
    };
  
    $scope.roles = [];
    $scope.settingInfo = {};
    $scope.propertySetting = function (property , companyId) {
        console.log('prop', property);
        EasyStoreUserspersistenceService.action.getUserRole(JSON.stringify({ userId: $scope.selectedUser.userId, companyId: companyId, propertyId: property.propertiesId })).then(
            function (result) {
                console.log('roles', result)
                property.roles = result.data;
                $scope.roles = property.roles;
                $scope.settingInfo = { userId: $scope.selectedUser.userId, companyId: companyId, propertyId: property.propertiesId };
                $('#members-modal').modal('show');
            },
            function (error) {
                toastr.error("حدث خطاء - اثناء طلب البيانات");
            });

        
        
    };

    $scope.roleProperties = [];
    $scope.roleProperty = function (property) {
        $scope.roleProperties = property;
        $('#roleModal').modal('show');
    };
    $scope.saveAllRoles = function () {
     
        var roleToSave = {};
        var filterRole = $filter('filter')($scope.roles, { active: true}, true);
        roleToSave.roleToAdd = angular.copy(filterRole)
        roleToSave.settingInfo = $scope.settingInfo;
        console.log(angular.toJson(roleToSave))
        EasyStoreUserspersistenceService.action.setUserRole(angular.toJson(roleToSave)).then(
                function (result) {
                    console.log('roles', result)
                   
                },
                function (error) {
                    toastr.error("حدث خطاء - اثناء طلب البيانات");
                });
    };

    $scope.selectedCompany = null;

    $scope.showAddCompany = function () {
        $scope.searchCom = '';
        $scope.companies = [];
        $('#AddCompanyModal').modal('show');
    };

    $scope.setSelectedCompany = function (company) {
        $scope.selectedCompany = company;
    }
    $scope.searchCompany = function (company) {
        console.log('search', company);
        //getCompanyByName
        EasyStoreUserspersistenceService.action.getCompanyByName(JSON.stringify({ name: company, uid: $scope.selectedUser.userId })).then(
                function (Companies) {
                   
                    $scope.companies = Companies.data;
                },
                function (error) {
                    toastr.error("حدث خطاء اثناء محاولة جلب البيانات");
                });
    };

    $scope.addCompanyToUser = function () {
        console.log("Add Company To User", $scope.selectedCompany)
        EasyStoreUserspersistenceService.action.addUserToCompany(angular.toJson($scope.selectedCompany)).then(
            function (compny) {
                $scope.selectedUser.settings.push(compny.data)
            },
            function (error) {
                toastr.error("لا يمكن تسجيل المستخدم لدي الشركة في الوقت الحالي");
            });
    };

    $scope.registration = { // CreateUserInvite
        "email": null,
        "phoneNumber": null,
        "firstName": null,
        "lastName": null,
        "occupation" : null
    };
    $scope.shownewUser = function () {
        $scope.registration = { // CreateUserInvite
            "email": null,
            "phoneNumber": null,
            "firstName": null,
            "lastName": null,
            "occupation": null
        };
        $('#AddUserModal').modal('show');
    };

    $scope.addNewUser = function () {
        console.log('$scope.registration', $scope.registration);
        EasyStoreUserspersistenceService.action.inviteUser(angular.toJson($scope.registration)).then(
            function (uemial) {
                //del="search"
                //"setSearch()
                $scope.search = uemial.data;
                $scope.setSearch();
                $('#AddUserModal').modal('hide');
            },
            function (error) {
                toastr.error("لا يمكن تسجيل المستخدم ");
            });
        //inviteUser


    };

});