(function () {

    'use strict';
    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("BalanceSheetsController", function ($scope, _, BalanceSheetspersistenceService, Offline, $location ,  $routeParams) {

        var BalanceSheetName = $routeParams.name;
        $scope.catName = $routeParams.name;
        console.log(BalanceSheetName);
        $scope.showList = false;
        $scope.BalanceSheets = [];
        $scope.BalanceSheetsNames = [];
        $scope.BalanceSheetpropertyValues = [];
        $scope.PropertyValues = [];
        var getData = function () {

            BalanceSheetspersistenceService.action.getById(BalanceSheetName).then(
                function (BalanceSheets) {
                    $scope.BalanceSheets = BalanceSheets;
                   
                    
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (BalanceSheets.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };
        var lazyGetData = _.debounce(getData, 2);

        Offline.on('confirmed-down', lazyGetData);
        Offline.on('confirmed-up', lazyGetData);

        lazyGetData();

        var getBalanceSheetNameData = function () {

            BalanceSheetspersistenceService.action.getAll().then(
                function (BalanceSheets) {
                    $scope.BalanceSheetsNames = BalanceSheets;

                    console.log(BalanceSheets);
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (BalanceSheets.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };

       
        var lazyGetBalanceSheetNameData = _.debounce(getBalanceSheetNameData, 2);

        Offline.on('confirmed-down', lazyGetBalanceSheetNameData);
        Offline.on('confirmed-up', lazyGetBalanceSheetNameData);

        lazyGetBalanceSheetNameData();
       

        function getValueByKey(key, data) {
            var i, len = data.length;

            for (i = 0; i < len; i++) {
                if (data[i] && data[i].hasOwnProperty(key)) {
                    return data[i][key];
                }
            }

            return -1;
        }

        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;   //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        // $scope.catogeries = BalanceSheetRepository.getcatogery();

        $('#loading').hide();


        // Save 

        $scope.showSuccessMessage = false;
        $scope.showFillOutFormMessage = false;
        $scope.isOnline = true;
        $scope.addBalanceSheet = {};







        var hasBalanceSheetToSave = function () {



            console.log($scope.addBalanceSheet)

            var hasValue = function (value) {

                if (typeof value === 'string') {
                    return value.length > 0;
                }

                return value > 0;
            };

            var returnValue =
                       hasValue($scope.addBalanceSheet.name)
                    && hasValue($scope.addBalanceSheet.price)
                    && hasValue($scope.addBalanceSheet.catogery);

            return returnValue;
        };

        $scope.save = function () {
            if ($scope.addBalanceSheet.id === null || $scope.addBalanceSheet.id === undefined) {
                $scope.addBalanceSheet.id = $scope.addBalanceSheet.name;
            }

            var saveBalanceSheet = hasBalanceSheetToSave();


            var BalanceSheet = $scope.addBalanceSheet;


            //;

            BalanceSheetspersistenceService.action.save(BalanceSheet).then(
                function (result) {
                    $scope.showSuccessMessage = true;
                    $scope.showErrorMessage = false;
                    $scope.addBalanceSheet = {};
                },
                function (error) {
                    $scope.showSuccessMessage = false;
                    $scope.showErrorMessage = true;
                });



        };

        Offline.on('confirmed-down', function () {
            $scope.$apply(function () {
                $scope.isOnline = false;
            });
        });

        Offline.on('confirmed-up', function () {
            $scope.$apply(function () {
                $scope.isOnline = true;
            });
        });

        //Edit and update
        var BalanceSheetToEdit = [];

        $scope.editingBalanceSheet = {};


        $scope.modify = function (BalanceSheet) {
            for (var i = 0, length = $scope.BalanceSheets.length; i < length; i++) {
                $scope.editingBalanceSheet[$scope.BalanceSheets[i].id] = false;
            }
            BalanceSheetToEdit = angular.copy(BalanceSheet);
            $scope.addBalanceSheet = BalanceSheet;
            $scope.editingBalanceSheet[BalanceSheet.id] = true;

        };


        $scope.update = function (BalanceSheet) {
            $scope.editingBalanceSheet[BalanceSheet.id] = false;
        };

        $scope.cancel = function (BalanceSheet) {

            BalanceSheet.name = BalanceSheetToEdit.name;
            BalanceSheet.price = BalanceSheetToEdit.price;
            BalanceSheet.catogery = BalanceSheetToEdit.catogery;

            $scope.editingBalanceSheet[BalanceSheet.id] = false;

        };

        $scope.reset = function () {
            for (var i = 0, length = $scope.BalanceSheets.length; i < length; i++) {
                $scope.editingBalanceSheet[$scope.BalanceSheets[i].id] = false;
            }
            $scope.addBalanceSheet = {};
        };


        //delte
        var delteBalanceSheet = {};
        $scope.BalanceSheetToDelte = function (BalanceSheet) {

            delteBalanceSheet = BalanceSheet;
        };


        $scope.delete = function () {


            BalanceSheetspersistenceService.action.Delete(delteBalanceSheet.id).then(
                function (result) {
                    $scope.BalanceSheets.splice($scope.BalanceSheets.indexOf(delteBalanceSheet), 1);
                    delteBalanceSheet = {};
                },
                function (error) {
                    $scope.error = error;
                });
        };


        BalanceSheetspersistenceService.getById(BalanceSheetName).then(
            function (BalanceSheets) {
                $scope.BalanceSheets = BalanceSheets;
            },
            function (error) {
                $scope.error = error;
            });

    });

}());
