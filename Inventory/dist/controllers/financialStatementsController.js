
(function () {


    'use strict';
    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("financialStatementsController", function ($scope, $timeout, _, AccountspersistenceService, $rootScope, settings, $filter,
                                                           AccountCategoriespersistenceService, BalanceSheetspersistenceService,
                                                           BalanceSheetTypespersistenceService, branchespersistenceService,
                                                           companiespersistenceService, AccountOrderspersistenceService, AccountMovementspersistenceService, Offline, $location, $stateParams, branchesremotePersistenceStrategy, $sce, authService, globalService, $q, $log, DTOptionsBuilder, DTColumnDefBuilder) {



        //$scope.$on('$viewContentLoaded', function () {
        //    // initialize core components
        //    App.initAjax();
        //    Layout.setSidebarMenuActiveLink('set', $('#sidebar_menu_link_profile')); // set profile link active in sidebar menu 

        //    // set default layout mode
        //    // set sidebar closed and body solid layout mode
        //    $rootScope.settings.layout.pageBodySolid = true;
        //    $rootScope.settings.layout.pageSidebarClosed = true;
        //});
        var vm = this;
        vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(2);


        var start = moment().startOf('month');
        var end = moment().endOf('month');

        $scope.startDate = moment().startOf('month').toDate();
        $scope.endDate = moment().endOf('month').toDate();
        $scope.pendDate = start.toDate();
        $scope.pstartDate =end.toDate();

        function cb(start, end) {
            $('#reportrange span').html('من ' + start.format('DD - MM -YYYY') + ' الي ' + end.format('DD - MM - YYYY'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'اليوم': [moment(), moment()],
                'امس': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'اخر 7 ايام': [moment().subtract(6, 'days'), moment()],
                'اخر 30 يوم': [moment().subtract(29, 'days'), moment()],
                'هذا الشهر': [moment().startOf('month'), moment().endOf('month')],
                'الشهر الماضي': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);



        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            $scope.startDate = picker.startDate.toDate();
            $scope.endDate = picker.endDate.toDate();
            $scope.getaccountMoves($scope.selectedAccountId,  $scope.startDate, $scope.endDate);
        });

        $scope.accountsArray = [];

        $scope.branchesGroub = [];

        $scope.branchGroub = {
            id: 0,
            name: 'حدد الشركة - الفرع',
            fullName: 'حدد الشركة - الفرع'
        };

        $scope.bquery = angular.copy($stateParams.bId);
        $scope.aquery = angular.copy($stateParams.acId);

        $scope.loadCompanies = function () {
            AccountspersistenceService.action.Getbranchgroup(1).then(
                function (Companies) {
                    $scope.branchesGroub = Companies;

                    if ($scope.bquery != null) {
                        $scope.branchGroub.id = angular.copy($scope.bquery);
                        $scope.bquery = null;

                    }
                },
                function (error) {
                    $scope.error = error;
                });
        };

        $scope.loadCompanies();

        $scope.totalItems = 0;
        $scope.currentPage = 0;
        $scope.itemsperpage = 1;

        var bId;
        $scope.$watch('branchGroub.id', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                var debitorSelected = $filter('filter')($scope.branchesGroub, { id: $scope.branchGroub.id });
                $scope.branchGroub = debitorSelected.length ? debitorSelected[0] : null;
                if (debitorSelected.length) {

                    var id = debitorSelected[0].id;
                    bId = parseInt(angular.copy(id));
                    getAccountsNames(bId);
                }
            }
        });

        $scope.$watch('startDate', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getaccountMoves($scope.selectedAccountId, $scope.startDate, $scope.endDate);
            }
        });

        $scope.$watch('endDate', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getaccountMoves($scope.selectedAccountId, $scope.startDate, $scope.endDate);
            }

        });



        $scope.debitorSelected = {};
        $scope.crditorSelected = {};

        $scope.accountsArray = [];
        $scope.names = [];
        $scope.debitorSelected = { value: $scope.accountsArray[0] };

        //$scope.debitorSelectedItem = 'undefined';

        var getAccountsNames = function (id) {

            AccountspersistenceService.action.searchfinancialStatements(id).then(
                             function (respone) {
                                 $scope.accountsArray = [];
                                 $scope.debitorSelected = {};


                                 $scope.accountsArray = respone;
                                 $scope.names = respone;
                                 if ($scope.aquery != null) {
                                     $scope.selectedAccountId = $scope.aquery;
                                     $scope.selectedAccount = $scope.accountsArray.filter(function (o) { return o.id == $scope.aquery })[0]
                                     $scope.selectedAccountId = angular.copy($stateParams.acId);
                                     $scope.aquery = null;

                                 }
                                 return;

                             },
                             function (error) {
                                 console.log("error names");
                                 console.log(error);
                             });
        };



        $scope.getItemArr = function () {

            return $scope.accountsArray;
        }





        $scope.OrderTo = {};
        $scope.orders = [];
        $scope.accountMoves = [];
        $scope.selectedAccountId = 0;
        $scope.selectedAccount = {};
        $scope.financiallist = {};
        $scope.checkdetails = false;
        $scope.Math = window.Math;
        $scope.setSelectedAccountId = function (item) {
            $scope.selectedAccountId = item.id
            $scope.selectedAccount = item;

            // $scope.selectedAccountId = id
        };


        $scope.$watch('selectedAccountId', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                if (newVal != 0) {
                    $scope.getaccountMoves(newVal, $scope.startDate, $scope.endDate);

                }
            }
        });


        //$scope.$watch('checkdetails', function (newVal, oldVal) {
        //    if (newVal !== oldVal) {
        //        if (newVal != 0) {
        //            $scope.getaccountMoves($scope.selectedAccountId, $scope.startDate, $scope.endDate);

        //        }
        //    }
        //});
        $scope.allAccounts = false;
        $scope.isAllAccounts = function () {
           
            $scope.getaccountMoves(parseInt($scope.selectedAccountId), $scope.startDate, $scope.endDate);
        };

        $scope.getaccountMoves = function (id, startDate, endDate) {
            //$http.get('/api/AccountOrders/GetAccountOrders/' + id + '/' + pageSize + '/' + pageNumber + '/' + startDate + '/' + endDate)
            $scope.accountsNote.show = false;
            AccountMovementspersistenceService.action.GetfinancialStatements(JSON.stringify({ id: id, startDate: startDate, endDate: endDate , isCompany : $scope.branchGroub.isCompany , allAccounts : $scope.allAccounts })).then(
            function (orders) {
                
                console.log('f statement', orders);
                $scope.financiallist = orders.data;

                $scope.pendDate = orders.data.pendDate;
                $scope.pstartDate = orders.data.pstartDate;
            },
            function (error) {
                $scope.error = error;
            });
        };
        $scope.accountsNote = { show: false };
        $scope.closeNoteShow = function () {
            $scope.accountsNote.show = false;
        };
        $scope.showNote = function (data) {
            console.log("Show Note", data)
            //GetCustomAccountOrdersdCatogery
            console.log($scope.selectedAccount);
            AccountMovementspersistenceService.action.GetCustomAccountOrdersdCatogery(JSON.stringify({ id: parseInt(data.id), startDate: $scope.startDate, endDate: $scope.endDate })).then(
                    function (orders) {
                      
                        $scope.accountsNote = {
                            title: "ايضاح متمم لقائمة " + $scope.selectedAccount.value + " - رقم: " + data.code + " - " + data.name,
                            order: orders.data,
                            show:true
                        };
                        console.log('Accounts Note', $scope.accountsNote)
                    },
                    function (error) {
                        $scope.error = error;
                    });
        };

        $scope.search = "";

        $scope.itemsperpage = 5;

        $scope.getOrders = function (id, orderNo, accountId, isDebit) {
            //$http.get('/api/AccountOrders/GetAccountOrders/' + id + '/' + pageSize + '/' + pageNumber + '/' + startDate + '/' + endDate)

            AccountMovementspersistenceService.action.GetAccountOrder(JSON.stringify({ orderId: id, orderNo: orderNo, branchId: bId, accountId: accountId, isDebit: isDebit })).then(
            function (orders) {

                $scope.OrderTo = orders.data;
            },
            function (error) {
                $scope.error = error;
            });
        };

        

        $scope.accounts = [];


        $scope.addOrder = function () {
            $scope.OrderTo = {};
            $scope.getOrders(0, 0, $scope.selectedAccountId, true);
        };
        $scope.addOrderCrdit = function () {
            $scope.OrderTo = {};
            $scope.getOrders(0, 0, $scope.selectedAccountId, false);
        };

        $scope.editOrder = function (orderToEdit) {

            if ($scope.selectedAccountId != 0) $scope.accountsArray.filter(function (o) { return o.id == $scope.selectedAccountId })[0].status = true;

            $scope.getOrders(orderToEdit.orderId, orderToEdit.orderNo, orderToEdit.accountId, true)
        };

        $scope.closeModel = function () {
            // if ($scope.selectedAccountId != 0) $scope.accountsArray.filter(function (o) { return o.id == $scope.selectedAccountId })[0].status = false;

        };

        $scope.saveOrder = function () {
            if ($scope.OrderTo.orderId === 0) {

                AccountMovementspersistenceService.action.save(JSON.stringify($scope.OrderTo)).then(
                function (Companies) {
                    $scope.accountMoves.push(Companies.data);

                },
                function (error) {
                    $scope.error = error;
                });
            }
            else {
                $scope.updateOrder();
            }



        };

        $scope.updateOrder = function () {
            AccountMovementspersistenceService.action.save(JSON.stringify($scope.OrderTo)).then(
            function (Companies) {

                $scope.accountMoves.filter(function (o) { return o.orderId == Companies.data.orderId })[0].debit = Companies.data.debit;
                $scope.accountMoves.filter(function (o) { return o.orderId == Companies.data.orderId })[0].crdit = Companies.data.crdit;
                $scope.accountMoves.filter(function (o) { return o.orderId == Companies.data.orderId })[0].orderNote = Companies.data.orderNote;
                $scope.accountMoves.filter(function (o) { return o.orderId == Companies.data.orderId })[0].note = Companies.data.note;
                $scope.accountMoves.filter(function (o) { return o.orderId == Companies.data.orderId })[0].summery = Companies.data.summery;
                $scope.accountMoves.filter(function (o) { return o.orderId == Companies.data.orderId })[0] = Companies.data;

                $scope.accountMoves[0].summery = Companies.data.summery;

            },
            function (error) {
                $scope.error = error;
                console.log('order updated error', error)
            });

        };

        $scope.showacc = function () {
            console.log('d account', $scope.debitorAccounts)
        }

        $scope.addNewAccounts = function (account) {
            var lastItem = $scope.OrderTo.secondAccount.length - 1;
            $scope.OrderTo.secondAccount.push({ id: null, account: { id: null, key: null, status: null, value: null }, amount: 0 });

            if (account.id != null) {
                var checkacc = ($scope.accountsArray.indexOf(account), account);

                if (!checkacc.status) {
                    $scope.accountsArray.filter(function (acc) { return acc.id == account.id })[0].status = true;
                }
            }
        };


        $scope.addNewCrditorAccounts = function (account) {
            $scope.crditorAccounts.push({ id: null, account: { id: null, key: null, status: null, value: null }, amount: 0 });
            if (account.id != null) {
                var checkacc = ($scope.accountsArray.indexOf(account), account);
                if (checkacc.status) {
                    $scope.accountsArray.filter(function (acc) { return acc.id == account.id })[0].status = false;
                }
            }

        };

        $scope.addacc = function (account) {
            $scope.accountsArray.filter(function (acc) { return acc.id == $scope.selectedAccount.id })[0].status = true;

            if (account.id != null && account.id != 0) {
                var checkacc = ($scope.accountsArray.indexOf(account), account);
                var debitoracc;
                for (debitoracc of  $scope.OrderTo.secondAccount) {
                    if (debitoracc.account.id != null) {
                        $scope.accountsArray.filter(function (acc) { return acc.id == debitoracc.account.id })[0].status = true;
                    }

                };


                if (checkacc.status) {

                    $scope.accountsArray.filter(function (acc) { return acc.id == account.id })[0].status = false;

                }
                $scope.accountsArray.filter(function (acc) { return acc.id == $scope.selectedAccount.id })[0].status = true;

                if ($scope.selectedAccountId != 0) {
                    $scope.accountsArray.filter(function (acc) { return acc.id == $scope.selectedAccountId })[0].status = true;

                }
            }

        };

        $scope.removeacc = function (account) {
            if (account.id != null && account.id != 0) {

                var checkacc = ($scope.accountsArray.indexOf(account), account);

                if (!checkacc.status) {
                    $scope.accountsArray.filter(function (acc) { return acc.id == account.id })[0].status = true;
                }
            }
        };


        $scope.onOpenClose = function (i) {
            console.log('is open', i);
        };


        $scope.removeAccounts = function (account) {
            var lastItem = $scope.OrderTo.secondAccount.length - 1;
            if (account.id != null) {
                $scope.accountsArray.filter(function (acc) { return acc.id == account.id })[0].status = false;

            };
            if (lastItem > 0) $scope.OrderTo.secondAccount.splice(lastItem);
        };



        $scope.AccountsTotalamount = function () {
            var total = 0;
            var cho = $scope.OrderTo.secondAccount;
            for (var i in cho) {

                if (!isNaN(parseFloat(cho[i].amount)))
                    total += parseFloat(cho[i].amount);
            }

            return total
        };



        $scope.usedAccount = function (item) {

            var deb = 0;
            var acc = 0;
            var bo = 'undefined';

            var debitorf = $filter('filter')($scope.debitorAccounts, { account: item });
            console.log('ddddddebitorAccounts', debitorf);
            for (acc = 0; acc < $scope.debitorAccounts.length; acc++) {
                console.log('$scope.debitorAccounts[acc].account.value', $scope.debitorAccounts[acc].account.value);
                console.log('$item', item.value);
                if ($scope.debitorAccounts[acc].account.value == item.value) {
                    console.log('hi the', $scope.debitorAccounts[acc]);
                    bo = true;
                }
                else {
                    console.log('hi ', $scope.debitorAccounts);
                    bo = false;
                }
            }

            console.log('hi the sss', bo);
            return bo;
            // return item.id == 6 ? true : false;


        };







    });

}());


