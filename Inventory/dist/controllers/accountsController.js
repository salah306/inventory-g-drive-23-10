(function () {

    'use strict';
    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("accountsController", function ($scope, _, AccountspersistenceService, Offline, $location ,  $routeParams) {

        var accountName = $routeParams.name;
        $scope.catName = $routeParams.name;
        console.log(accountName);
        $scope.showList = false;
        $scope.Accounts = [];
        $scope.AccountsNames = [];
        $scope.AccountpropertyValues = [];
        $scope.PropertyValues = [];
        var getData = function () {

            AccountspersistenceService.action.getById(accountName).then(
                function (Accounts) {
                    $scope.Accounts = Accounts;
                   
                    
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (Accounts.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };
        var lazyGetData = _.debounce(getData, 2);

        Offline.on('confirmed-down', lazyGetData);
        Offline.on('confirmed-up', lazyGetData);

        lazyGetData();

        var getAccountNameData = function () {

            AccountspersistenceService.action.getAll().then(
                function (Accounts) {
                    $scope.AccountsNames = Accounts;

                    console.log(Accounts);
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (Accounts.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };

       
        var lazyGetAccountNameData = _.debounce(getAccountNameData, 2);

        Offline.on('confirmed-down', lazyGetAccountNameData);
        Offline.on('confirmed-up', lazyGetAccountNameData);

        lazyGetAccountNameData();
       

        function getValueByKey(key, data) {
            var i, len = data.length;

            for (i = 0; i < len; i++) {
                if (data[i] && data[i].hasOwnProperty(key)) {
                    return data[i][key];
                }
            }

            return -1;
        }

        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;   //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        // $scope.catogeries = AccountRepository.getcatogery();

        $('#loading').hide();


        // Save 

        $scope.showSuccessMessage = false;
        $scope.showFillOutFormMessage = false;
        $scope.isOnline = true;
        $scope.addAccount = {};







        var hasAccountToSave = function () {



            console.log($scope.addAccount)

            var hasValue = function (value) {

                if (typeof value === 'string') {
                    return value.length > 0;
                }

                return value > 0;
            };

            var returnValue =
                       hasValue($scope.addAccount.name)
                    && hasValue($scope.addAccount.price)
                    && hasValue($scope.addAccount.catogery);

            return returnValue;
        };

        $scope.save = function () {
            if ($scope.addAccount.id === null || $scope.addAccount.id === undefined) {
                $scope.addAccount.id = $scope.addAccount.name;
            }

            var saveAccount = hasAccountToSave();


            var Account = $scope.addAccount;


            //;

            AccountspersistenceService.action.save(Account).then(
                function (result) {
                    $scope.showSuccessMessage = true;
                    $scope.showErrorMessage = false;
                    $scope.addAccount = {};
                },
                function (error) {
                    $scope.showSuccessMessage = false;
                    $scope.showErrorMessage = true;
                });



        };

        Offline.on('confirmed-down', function () {
            $scope.$apply(function () {
                $scope.isOnline = false;
            });
        });

        Offline.on('confirmed-up', function () {
            $scope.$apply(function () {
                $scope.isOnline = true;
            });
        });

        //Edit and update
        var AccountToEdit = [];

        $scope.editingAccount = {};


        $scope.modify = function (Account) {
            for (var i = 0, length = $scope.Accounts.length; i < length; i++) {
                $scope.editingAccount[$scope.Accounts[i].id] = false;
            }
            AccountToEdit = angular.copy(Account);
            $scope.addAccount = Account;
            $scope.editingAccount[Account.id] = true;

        };


        $scope.update = function (Account) {
            $scope.editingAccount[Account.id] = false;
        };

        $scope.cancel = function (Account) {

            Account.name = AccountToEdit.name;
            Account.price = AccountToEdit.price;
            Account.catogery = AccountToEdit.catogery;

            $scope.editingAccount[Account.id] = false;

        };

        $scope.reset = function () {
            for (var i = 0, length = $scope.Accounts.length; i < length; i++) {
                $scope.editingAccount[$scope.Accounts[i].id] = false;
            }
            $scope.addAccount = {};
        };


        //delte
        var delteAccount = {};
        $scope.AccountToDelte = function (Account) {

            delteAccount = Account;
        };


        $scope.delete = function () {


            AccountspersistenceService.action.Delete(delteAccount.id).then(
                function (result) {
                    $scope.Accounts.splice($scope.Accounts.indexOf(delteAccount), 1);
                    delteAccount = {};
                },
                function (error) {
                    $scope.error = error;
                });
        };


        AccountspersistenceService.getById(accountName).then(
            function (Accounts) {
                $scope.Accounts = Accounts;
            },
            function (error) {
                $scope.error = error;
            });

    });

}());
