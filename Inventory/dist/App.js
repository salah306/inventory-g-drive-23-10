// **************************** //
// BEGIN: PDSA Collapser Script //
// **************************** //
$(document).ready(function () {
    var pdsaCollapser = $("[data-pdsa-collapser-name]");

    for (var i = 0; i < pdsaCollapser.length; i++) {
        var name = pdsaCollapser[i].id;
        var open = $("#" + name).data("pdsa-collapser-open");
        var close = $("#" + name).data("pdsa-collapser-close");

        // Add 'close' icon to all panels
        $("#" + name + " .pdsa-panel-toggle").addClass(close);

        // Find any panel's that have the class '.in',
        // remove the 'close' glyph and add the 'open' glyph
        var list = $("#" + name + " .in");
        for (var index = 0; index < list.length; index++) {
            $($("a[href='#" + $(list[index]).attr("id") + "']")).next().
              removeClass(close).
              addClass(open);
        }

        // Hook into 'hide' event
        $("#" + name).on('hide.bs.collapse', function (e) {
            var parent = $("#" + e.target.id).parents(".panel-group");

            $("#" + e.target.id).prev().find(".pdsa-panel-toggle").
              removeClass($(parent).data("pdsa-collapser-open")).
              addClass($(parent).data("pdsa-collapser-close"));
        });

        // Hook into 'show' event
        $("#" + name).on('show.bs.collapse', function (e) {
            var parent = $("#" + e.target.id).parents(".panel-group");

            $("#" + e.target.id).prev().find(".pdsa-panel-toggle").
              removeClass($(parent).data("pdsa-collapser-close")).
              addClass($(parent).data("pdsa-collapser-open"));
        });
    }
});
// ************************** //
// END: PDSA Collapser Script //
// ************************** //