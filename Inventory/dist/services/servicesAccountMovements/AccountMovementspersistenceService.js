(function () {

    'use strict';

    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.service('AccountMovementspersistenceService',
        function ($q, Offline, AccountMovementsremotePersistenceStrategy, AccountMovementslocalPersistenceStrategy) {

            var self = this;

            self.persistenceType = 'remote';

            self.action = AccountMovementsremotePersistenceStrategy;

            Offline.on('confirmed-down', function () {
                self.action = AccountMovementslocalPersistenceStrategy;
                self.persistenceType = 'local';
            });

            Offline.on('confirmed-up', function () {
                self.action = AccountMovementsremotePersistenceStrategy;
                self.persistenceType = 'remote';
            });

            self.getRemoteAccountMovement = function (name) {
                return AccountMovementsremotePersistenceStrategy.getById(name);
            };

            self.getLocalAccountMovement = function (name) {
                return AccountMovementslocalPersistenceStrategy.getById(name);
            };

            self.getById = function (name) {

                var deferred = $q.defer();

                if (Offline.state = 'up') {

                    var
                        remoteAccountMovement = {},
                        localAccountMovement = {};

                    self.getRemoteAccountMovement(name).then(function (rAccountMovement) {

                        remoteAccountMovement = rAccountMovement;

                        self.getLocalAccountMovement(name).then(function (lAccountMovement) {

                            localAccountMovement = lAccountMovement;

                            if (localAccountMovement.modifiedDate > (new Date(remoteAccountMovement.modifiedDate))) {
                                deferred.resolve(localAccountMovement);
                            }
                            else {
                                deferred.resolve(remoteAccountMovement);
                            }

                        }, deferred.reject);

                    }, deferred.reject);

                } else {
                    self.getLocalAccountMovement(name).then(deferred.resolve, deferred.reject);
                }

                return deferred.promise;
            };

            return self;

        });

}());