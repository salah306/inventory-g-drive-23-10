(function () {

    'use strict';

    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    };


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.factory('AccountMovementsremotePersistenceStrategy', function ($http, $q) {
        var svc = {

            save: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/AccountMovements', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {

                    return $http.post('/api/AccountMovements', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            GetAccountOrder: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/AccountMovements/GetOrder', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/AccountMovements/GetOrder', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },



            getAll: function () {
                var deferred = $q.defer();

                $http.get('/api/AccountMovements')
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            GetAccountOrdersd: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/AccountMovements/GetAccountMovementsDates', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/AccountMovements/GetAccountMovementsDates', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },


            GetAccountOrdersdCatogery: function (company) {
                //GetAllAccountMovementsCatogeryDates
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/AccountMovements/GetAccountMovementsCatogeryDates', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/AccountMovements/GetAccountMovementsDates', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            GetCustomAccountOrdersdCatogery: function (company) {
                //GetAllAccountMovementsCatogeryDates
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/AccountMovements/GetCustomAccountMovementsCatogeryDates', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/AccountMovements/GetCustomAccountMovementsCatogeryDates', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },

            GetfinancialStatements: function (company) {
                //GetAllAccountMovementsCatogeryDates
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/AccountMovements/GetfinancialStatements', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/AccountMovements/GetfinancialStatements', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            GetAllAccountOrdersdCatogery: function (company) {
                //GetAllAccountMovementsCatogeryDates
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/AccountMovements/GetAllAccountMovementsCatogeryDates', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/AccountMovements/GetAllAccountMovementsCatogeryDates', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },

            getById: function (name) {
                
                var deferred = $q.defer();
                console.log(JSON.stringify(name));
                
                $http.get('/api/AccountMovements/accountMovementById/', { params: { "accountOrderPropertiesName": name.accountOrderPropertiesName, "accountName": name.accountName, "date": name.date } })

                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            exists: function (name) {

                var deferred = $q.defer();

                svc.getById(name).then(function (AccountMovement) {
                    deferred.resolve(AccountMovement.id === name);
                }, deferred.reject);

                return deferred.promise;
            },

            'Delete': function (name) {
                var deferred = $q.defer();

                $http.delete('/api/AccountMovements/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            }
        };
        return svc;
    });

    inventoryModule.factory('AccountMovementslocalPersistenceStrategy',
        function ($q, localDBService, nullAccountMovement, dbModel) {

            var svc = {

                dbModel: dbModel,

                localDBService: localDBService,

                getById: function (name) {
                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getById(svc.dbModel.objectStoreName, name)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                exists: function (name) {

                    var deferred = $q.defer();

                    svc.getById(name).then(function (AccountMovement) {
                        deferred.resolve(AccountMovement.id === name);
                    }, deferred.reject);

                    return deferred.promise;
                },

                save: function (AccountMovement) {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function (e) {

                        var name = AccountMovement.name;

                        if (name === null || name === undefined) {
                            localDBService.insert(svc.dbModel.objectStoreName, AccountMovement, 'id')
                                          .then(deferred.resolve,
                                                deferred.reject);
                        } else {
                            svc.exists(name).then(function (doesExist) {

                                if (doesExist) {
                                    localDBService.update(svc.dbModel.objectStoreName, AccountMovement, name)
                                                  .then(deferred.resolve,
                                                        deferred.reject);;
                                } else {
                                    localDBService.insert(svc.dbModel.objectStoreName, AccountMovement, 'id')
                                                  .then(deferred.resolve,
                                                        deferred.reject);
                                }

                            }, deferred.reject);
                        }

                    }, deferred.reject);

                    return deferred.promise;
                },

                getAll: function () {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getAll(svc.dbModel.objectStoreName)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                'Delete': function (name) {

                    var deferred = $q.defer();

                    localDBService.delete(svc.dbModel.objectStoreName, name)
                                  .then(deferred.resolve,
                                        deferred.reject);

                    return deferred.promise;
                }

            };
           
            return svc;
        });

}());