(function () {

    'use strict';

    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.service('AccountCategoryPropertiespersistenceService',
        function ($q, Offline, AccountCategoryPropertiesremotePersistenceStrategy, AccountCategoryPropertieslocalPersistenceStrategy) {

            var self = this;

            self.persistenceType = 'remote';

            self.action = AccountCategoryPropertiesremotePersistenceStrategy;

            Offline.on('confirmed-down', function () {
                self.action = AccountCategoryPropertieslocalPersistenceStrategy;
                self.persistenceType = 'local';
            });

            Offline.on('confirmed-up', function () {
                self.action = AccountCategoryPropertiesremotePersistenceStrategy;
                self.persistenceType = 'remote';
            });

            self.getRemoteAccountCategoryProperty = function (name) {
                return AccountCategoryPropertiesremotePersistenceStrategy.getById(name);
            };

            self.getLocalAccountCategoryProperty = function (name) {
                return AccountCategoryPropertieslocalPersistenceStrategy.getById(name);
            };

            self.getById = function (name) {

                var deferred = $q.defer();

                if (Offline.state = 'up') {

                    var
                        remoteAccountCategoryProperty = {},
                        localAccountCategoryProperty = {};

                    self.getRemoteAccountCategoryProperty(name).then(function (rAccountCategoryProperty) {

                        remoteAccountCategoryProperty = rAccountCategoryProperty;

                        self.getLocalAccountCategoryProperty(name).then(function (lAccountCategoryProperty) {

                            localAccountCategoryProperty = lAccountCategoryProperty;

                            if (localAccountCategoryProperty.modifiedDate > (new Date(remoteAccountCategoryProperty.modifiedDate))) {
                                deferred.resolve(localAccountCategoryProperty);
                            }
                            else {
                                deferred.resolve(remoteAccountCategoryProperty);
                            }

                        }, deferred.reject);

                    }, deferred.reject);

                } else {
                    self.getLocalAccountCategoryProperty(name).then(deferred.resolve, deferred.reject);
                }

                return deferred.promise;
            };

            return self;

        });

}());