(function () {

    'use strict';

    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.service('AccountCategoriespersistenceService',
        function ($q, Offline, AccountCategoriesremotePersistenceStrategy, AccountCategorieslocalPersistenceStrategy) {

            var self = this;

            self.persistenceType = 'remote';

            self.action = AccountCategoriesremotePersistenceStrategy;

            Offline.on('confirmed-down', function () {
                self.action = AccountCategorieslocalPersistenceStrategy;
                self.persistenceType = 'local';
            });

            Offline.on('confirmed-up', function () {
                self.action = AccountCategoriesremotePersistenceStrategy;
                self.persistenceType = 'remote';
            });

            self.getRemoteAccountCategory = function (name) {
                return AccountCategoriesremotePersistenceStrategy.getById(name);
            };

            self.getLocalAccountCategory = function (name) {
                return AccountCategorieslocalPersistenceStrategy.getById(name);
            };

            self.getById = function (name) {

                var deferred = $q.defer();

                if (Offline.state = 'up') {

                    var
                        remoteAccountCategory = {},
                        localAccountCategory = {};

                    self.getRemoteAccountCategory(name).then(function (rAccountCategory) {

                        remoteAccountCategory = rAccountCategory;

                        self.getLocalAccountCategory(name).then(function (lAccountCategory) {

                            localAccountCategory = lAccountCategory;

                            if (localAccountCategory.modifiedDate > (new Date(remoteAccountCategory.modifiedDate))) {
                                deferred.resolve(localAccountCategory);
                            }
                            else {
                                deferred.resolve(remoteAccountCategory);
                            }

                        }, deferred.reject);

                    }, deferred.reject);

                } else {
                    self.getLocalAccountCategory(name).then(deferred.resolve, deferred.reject);
                }

                return deferred.promise;
            };

            return self;

        });

}());