(function () {

    'use strict';

    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.service('branchespersistenceService',
        function ($q, Offline, branchesremotePersistenceStrategy, brancheslocalPersistenceStrategy) {

            var self = this;

            self.persistenceType = 'remote';

            self.action = branchesremotePersistenceStrategy;

            Offline.on('confirmed-down', function () {
                self.action = brancheslocalPersistenceStrategy;
                self.persistenceType = 'local';
            });

            Offline.on('confirmed-up', function () {
                self.action = branchesremotePersistenceStrategy;
                self.persistenceType = 'remote';
            });

            self.getRemotebranch = function (name) {
                return branchesremotePersistenceStrategy.getById(name);
            };

            self.getLocalbranch = function (name) {
                return brancheslocalPersistenceStrategy.getById(name);
            };

            self.getById = function (name) {

                var deferred = $q.defer();

                if (Offline.state = 'up') {

                    var
                        remotebranch = {},
                        localbranch = {};

                    self.getRemotebranch(name).then(function (rbranch) {

                        remotebranch = rbranch;

                        self.getLocalbranch(name).then(function (lbranch) {

                            localbranch = lbranch;

                            if (localbranch.modifiedDate > (new Date(remotebranch.modifiedDate))) {
                                deferred.resolve(localbranch);
                            }
                            else {
                                deferred.resolve(remotebranch);
                            }

                        }, deferred.reject);

                    }, deferred.reject);

                } else {
                    self.getLocalbranch(name).then(deferred.resolve, deferred.reject);
                }

                return deferred.promise;
            };

            return self;

        });

}());