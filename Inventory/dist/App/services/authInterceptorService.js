'use strict';
inventoryModule.factory('authInterceptorService', ['$q', '$location', 'localStorageService', '$rootScope', function ($q, $location, localStorageService, $rootScope) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {

        config.headers = config.headers || {};

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            config.headers.Authorization = 'Bearer ' + authData.token;
        }

        return config;
    }

    var _responseError = function (rejection) {
        if (rejection.status === 401) {
            
            $rootScope.settings.layout.pageSidebarClosed = true;
            $rootScope.mainUserInfo = null;
            $rootScope.mainshow = false;
            $location.path('/login');
        }
        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);