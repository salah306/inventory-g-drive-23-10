(function () {

    'use strict';

    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    };


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.factory('BalanceSheetTypesremotePersistenceStrategy', function ($http, $q) {
        var svc = {

            save: function (BalanceSheetType) {
                var it = BalanceSheetType;
                var deferred = $q.defer();
                if (isJson(BalanceSheetType))
                {
                    it = JSON.parse(BalanceSheetType);
                    return $q.all(it.map(function (it) {
                        $http.post('/api/BalanceSheetTypes', it).then(deferred.resolve, deferred.reject);
                    }));
                } else {

                    $http.post('/api/BalanceSheetTypes', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },


            getAll: function () {
                var deferred = $q.defer();

                $http.get('/api/BalanceSheetTypes')
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },

            getById: function (name) {
                var deferred = $q.defer();

                $http.get('/api/BalanceSheetTypes/BalanceSheetTypesById/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            exists: function (name) {

                var deferred = $q.defer();

                svc.getById(name).then(function (BalanceSheetType) {
                    deferred.resolve(BalanceSheetType.id === name);
                }, deferred.reject);

                return deferred.promise;
            },

            'Delete': function (name) {
                var deferred = $q.defer();

                $http.delete('/api/BalanceSheetTypes/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            }
        };
        return svc;
    });

    inventoryModule.factory('BalanceSheetTypeslocalPersistenceStrategy',
        function ($q, localDBService, nullBalanceSheetType, dbModel) {

            var svc = {

                dbModel: dbModel,

                localDBService: localDBService,

                getById: function (name) {
                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getById(svc.dbModel.objectStoreName, name)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                exists: function (name) {

                    var deferred = $q.defer();

                    svc.getById(name).then(function (BalanceSheetType) {
                        deferred.resolve(BalanceSheetType.id === name);
                    }, deferred.reject);

                    return deferred.promise;
                },

                save: function (BalanceSheetType) {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function (e) {

                        var name = BalanceSheetType.name;

                        if (name === null || name === undefined) {
                            localDBService.insert(svc.dbModel.objectStoreName, BalanceSheetType, 'id')
                                          .then(deferred.resolve,
                                                deferred.reject);
                        } else {
                            svc.exists(name).then(function (doesExist) {

                                if (doesExist) {
                                    localDBService.update(svc.dbModel.objectStoreName, BalanceSheetType, name)
                                                  .then(deferred.resolve,
                                                        deferred.reject);;
                                } else {
                                    localDBService.insert(svc.dbModel.objectStoreName, BalanceSheetType, 'id')
                                                  .then(deferred.resolve,
                                                        deferred.reject);
                                }

                            }, deferred.reject);
                        }

                    }, deferred.reject);

                    return deferred.promise;
                },

                getAll: function () {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getAll(svc.dbModel.objectStoreName)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                'Delete': function (name) {

                    var deferred = $q.defer();

                    localDBService.delete(svc.dbModel.objectStoreName, name)
                                  .then(deferred.resolve,
                                        deferred.reject);

                    return deferred.promise;
                }

            };
           
            return svc;
        });

}());