(function () {

    'use strict';

    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    };


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.factory('AccountsremotePersistenceStrategy', function ($http, $q) {
        var svc = {

            save: function (Account) {
                var it = Account;
                var deferred = $q.defer();
                if (isJson(Account))
                {
                    it = JSON.parse(Account);
                    return $q.all(it.map(function (it) {
                        $http.post('/api/Accounts', it).then(deferred.resolve, deferred.reject);
                    }));
                } else {

                    $http.post('/api/Accounts', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },


            getAll: function () {
                var deferred = $q.defer();

                $http.get('/api/Accounts')
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },

            getById: function (name) {
                var deferred = $q.defer();

                $http.get('/api/Accounts/AccountsById/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },

            search: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/Accounts/GetAccountsNames', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/Accounts/GetAccountsNames', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },
            searchfinancialStatements: function (id) {
                var deferred = $q.defer();

                $http.get('/api/Accounts/GetfinancialStatements/' + id)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
          
            searchCatogery: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/Accounts/GetAccountsNamesCatogery', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/Accounts/GetAccountsNamesCatogery', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },
            Getbranchgroup: function (id) {
                var deferred = $q.defer();

                $http.get('/api/Accounts/Getbranchgroup/' + id)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            exists: function (name) {

                var deferred = $q.defer();

                svc.getById(name).then(function (Account) {
                    deferred.resolve(Account.id === name);
                }, deferred.reject);

                return deferred.promise;
            },
            Getdashitems: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/dashboard/Getdashitems', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/dashboard/Getdashitems', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            GetaccountCategoryProperties: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/accountProperty/GetaccountCategoryProperties', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/accountProperty/GetaccountCategoryProperties', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            AddaccountCategoryPropery: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/accountProperty/AddaccountCategoryPropery', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/accountProperty/AddaccountCategoryPropery', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            DelaccountCategoryPropery: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/accountProperty/DelaccountCategoryPropery', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/accountProperty/DelaccountCategoryPropery', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            GetAccountInfo: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/accountProperty/GetAccountInfo', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/accountProperty/GetAccountInfo', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            GetAccountOrdersd: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/accountProperty/GetAccountMovementsDates', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/accountProperty/GetAccountMovementsDates', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            addPropertyValue: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/accountProperty/addPropertyValue', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/accountProperty/addPropertyValue', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            delPropertyValue: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/accountProperty/DelPropertyValue', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/accountProperty/DelPropertyValue', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            getacNames: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/accountProperty/getacNames', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/accountProperty/getacNames', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            setOrderRequest: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/accountProperty/setOrderRequest', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/accountProperty/setOrderRequest', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            setOrderMoveRequest: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/accountProperty/setOrderMoveRequest', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/accountProperty/setOrderMoveRequest', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },

            getwaiting: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/accountProperty/getwaiting', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/accountProperty/getwaiting', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            getOrderRequestById: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/accountProperty/getOrderRequestById', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/accountProperty/getOrderRequestById', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            getOrderMoveById: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/accountProperty/getOrderMoveById', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/accountProperty/getOrderMoveById', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            'Delete': function (name) {
                var deferred = $q.defer();

                $http.delete('/api/Accounts/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            }
        };
        return svc;
    });

    inventoryModule.factory('AccountslocalPersistenceStrategy',
        function ($q, localDBService, nullAccount, dbModel) {

            var svc = {

                dbModel: dbModel,

                localDBService: localDBService,

                getById: function (name) {
                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getById(svc.dbModel.objectStoreName, name)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                exists: function (name) {

                    var deferred = $q.defer();

                    svc.getById(name).then(function (Account) {
                        deferred.resolve(Account.id === name);
                    }, deferred.reject);

                    return deferred.promise;
                },

                save: function (Account) {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function (e) {

                        var name = Account.name;

                        if (name === null || name === undefined) {
                            localDBService.insert(svc.dbModel.objectStoreName, Account, 'id')
                                          .then(deferred.resolve,
                                                deferred.reject);
                        } else {
                            svc.exists(name).then(function (doesExist) {

                                if (doesExist) {
                                    localDBService.update(svc.dbModel.objectStoreName, Account, name)
                                                  .then(deferred.resolve,
                                                        deferred.reject);;
                                } else {
                                    localDBService.insert(svc.dbModel.objectStoreName, Account, 'id')
                                                  .then(deferred.resolve,
                                                        deferred.reject);
                                }

                            }, deferred.reject);
                        }

                    }, deferred.reject);

                    return deferred.promise;
                },

                getAll: function () {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getAll(svc.dbModel.objectStoreName)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                'Delete': function (name) {

                    var deferred = $q.defer();

                    localDBService.delete(svc.dbModel.objectStoreName, name)
                                  .then(deferred.resolve,
                                        deferred.reject);

                    return deferred.promise;
                }

            };
           
            return svc;
        });

}());