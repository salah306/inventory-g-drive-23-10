'use strict';
inventoryModule.factory('authService', ['$http', '$q', '$rootScope', 'localStorageService', 'EasyStoreUserspersistenceService', '$location', function ($http, $q, $rootScope, localStorageService, EasyStoreUserspersistenceService, $location) {
 
    var serviceBase = '/';
    var authServiceFactory = {};
 
    var _authentication = {
        isAuth: false,
        userName : "",
        firstName : ""
    };
 
    var _saveRegistration = function (registration) {
 
        _logOut();
 
        return $http.post(serviceBase + 'api/account/create', registration).then(function (response) {
          
            return response;
        });
 
    };


    var _saveInvite = function (registration) {

        

        return $http.post(serviceBase + 'api/account/invite', registration).then(function (response) {

            return response;
        });

    };

    var _getUsers = function () {

        var deferred = $q.defer();

        $http.get('/api/Account/users')
             .success(deferred.resolve)
             .error(deferred.reject);

        return deferred.promise;


    };
 
    var _login = function (loginData) {
 
        var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;
 
        var deferred = $q.defer();
 
        $http.post(serviceBase + 'oauth/token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {
 
            localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName, firstName: response.firstName});
            console.log('respone' , response);
            _authentication.isAuth = true;
            _authentication.userName = loginData.userName;
            deferred.resolve(response);
 
        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });
 
        return deferred.promise;
 
    };
 
    var _logOut = function () {
 
        localStorageService.remove('authorizationData');
 
        _authentication.isAuth = false;
        _authentication.userName = "";
        _authentication.firstName = "";
        $rootScope.mainUserInfo = null;
        $rootScope.mainshow = false;
        $rootScope.settings.layout.pageSidebarClosed = true;
        $location.path('/login');

    };


    var _RegenerateEmailConfirmation = function (userName) {

        return $http.post(serviceBase + 'api/account/regenerateEmailConfirmation/', userName).then(function (response) {
           
            return response;
        });
    };
 
    var _fillAuthData = function () {
 
        var authData = localStorageService.get('authorizationData');
        if (authData)
        {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
            _authentication.firstName = authData.firstName;
            $rootScope.mainshow = true;
            $rootScope.mainUserInfo = {};
            $rootScope.mLogout = _logOut;
            var getuserInfo = function () {
                EasyStoreUserspersistenceService.action.getusersand().then(
            function (result) {
                $rootScope.mainbranchGroub = result.data.branchGroup;
                $rootScope.mainUserInfo = result.data.user;
                $rootScope.mainbranchesGroub = result.data.companies;
            },
            function (error) {

            });
            }();
        }
       
    }
 
    authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.invite = _saveInvite;
    authServiceFactory.getUsers = _getUsers;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;
    authServiceFactory.RegenerateEmailConfirmation = function (email) {

        return $http.post(serviceBase + 'api/account/regenerateEmailConfirmation?email='+ email).then(function (response) {
         
            return response;
        });
    };

    authServiceFactory.GetAllUsers = function () {

        return $http.get(serviceBase + 'api/Account/users').then(function (response) {

            return response;
        });
    };
 
    return authServiceFactory;
}]);