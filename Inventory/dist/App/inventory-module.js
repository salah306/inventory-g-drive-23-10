window.indexedDB = window.indexedDB ||
                    window.mozIndexedDB ||
                    window.webkitIndexedDB ||
                    window.msIndexedDB;

window.IDBTransaction = window.IDBTransaction ||
                        window.webkitIDBTransaction ||
                        window.msIDBTransaction;

window.IDBKeyRange = window.IDBKeyRange ||
                     window.webkitIDBKeyRange ||
                     window.msIDBKeyRange;


var inventoryModule = angular.module("inventoryModule", ['ngRoute', 'ngResource', 'jsTree.directive' ,'angularUtils.directives.dirPagination', 'ui.bootstrap', 'LocalStorageModule', 'angular-loading-bar'])
    .config(function ($routeProvider, $locationProvider, $provide) {

      //  $routeProvider.when('/Inventory/Items', { templateUrl: '/templates/items.html', controller: 'ItemsController' });

        $routeProvider.when("/index/login/", {
            controller: "loginController",
            templateUrl: "/templates/login.html"
        }).when("/index/signup/", {
            controller: "signupController",
            templateUrl: "/templates/signup.html"
        }).when("/index/AccountCategories/", {
            controller: "accountCategoriesController",
            templateUrl: "/templates/accountCategories.html"
        }).when("/index/Accounts/:name/", {
            controller: "accountsController",
            templateUrl: "/templates/accounts.html"
        }).when("/index/Accounts/:catname/AccountDetails/:name/", {
            controller: "accountDetailsController",
            templateUrl: "/templates/accountDetails.html"
        }).when("/index/accountingDepartment/", {
            controller: "treeController",
            templateUrl: "/templates/accountingDepartment.html"
        }).otherwise({ templateUrl: '/templates/accountingDepartment.html', controller: "treeController" });
       
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
       


        $provide.constant('indexedDB', window.indexedDB);

        $provide.constant('_', window._);

        $provide.constant('localStorage', window.localStorage);

        $provide.constant('Offline', window.Offline);

        $provide.value('nullAccountCategory', {
            id: '',
            insertDate: new Date(-8640000000000000),
            modifiedDate: new Date(-8640000000000000)
        });

        $provide.value('nullAccount', {
            id: '',
            insertDate: new Date(-8640000000000000),
            modifiedDate: new Date(-8640000000000000)
        }); 
        $provide.value('nullAccountDetail', {
            id: '',
            insertDate: new Date(-8640000000000000),
            modifiedDate: new Date(-8640000000000000)
        });
        $provide.value('nullAccountOrder', {
            id: '',
            insertDate: new Date(-8640000000000000),
            modifiedDate: new Date(-8640000000000000)
        });
        $provide.value('nullAccountMovement', {
            id: '',
            insertDate: new Date(-8640000000000000),
            modifiedDate: new Date(-8640000000000000)
        });
        $provide.value('nullBalanceSheet', {
            id: '',
            insertDate: new Date(-8640000000000000),
            modifiedDate: new Date(-8640000000000000)
        });

        $provide.value('nullBalanceSheetType', {
            id: '',
            insertDate: new Date(-8640000000000000),
            modifiedDate: new Date(-8640000000000000)
        });
        $provide.value('nullbranch', {
            id: '',
            insertDate: new Date(-8640000000000000),
            modifiedDate: new Date(-8640000000000000)
        });

        $provide.value('nullcompany', {
            id: '',
            insertDate: new Date(-8640000000000000),
            modifiedDate: new Date(-8640000000000000)
        });
       
        $provide.value('dbModel', {
            name: 'inventory',
            version: '1',
            instance: null,
            objectStoreName: 'AccountCategories',
            keyName: 'id',
            upgrade: function (e) {

                var db = e.target.result;
                if (!db.objectStoreNames.contains('AccountCategories')) {
                    db.createObjectStore(('AccountCategories'), {
                        keyPath: 'id'
                    });
                }

            }
        });

    });

inventoryModule.run(['authService', function (authService) {
    authService.fillAuthData();
}]);

inventoryModule.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});

inventoryModule.config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
    cfpLoadingBarProvider.parentSelector = '#loading-bar-container';
    cfpLoadingBarProvider.spinnerTemplate = '<img class="centeringblock" src="/loading-please.gif" width="50" height="50" />';
    
}]);