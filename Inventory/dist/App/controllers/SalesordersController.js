
(function () {


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("SalesordersController", function ($scope, $timeout, _, AccountspersistenceService, $rootScope, settings, $filter,
                                                           AccountCategoriespersistenceService, BalanceSheetspersistenceService,
                                                           BalanceSheetTypespersistenceService, branchespersistenceService,
                                                           companiespersistenceService, $localStorage, AccountOrderspersistenceService, Offline, $location, branchesremotePersistenceStrategy, $sce, authService, globalService, $q, $log, DTOptionsBuilder, DTColumnDefBuilder) {
        var bId;
        $scope.itemsforBill = [];
        $scope.billPaymethods = [];
        $scope.titleAccounts = [];
        $scope.branchesGroub = [];
        $scope.billNames = [];
        $scope.orderGroupNames = [];

        $scope.labalAccount = "العميل";
        $scope.mainType = "مبيعات";
        $scope.branchGroub = $rootScope.mainbranchGroub;

        $scope.$watch('$root.mainbranchGroub.id', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal !== '0') {
                var findCompanybr = angular.copy($filter('filter')($rootScope.mainbranchesGroub, { id: newVal }, true));
                if (findCompanybr) {
                    $scope.branchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    $rootScope.mainbranchGroub = findCompanybr.length ? findCompanybr[0] : null;

                    if (findCompanybr.length) {
                        bId = parseInt(angular.copy(parseInt(findCompanybr[0].id)));
                        $scope.selectedOrderGroupName = { orderGroupId: 0, orderGroupName: 'حدد مجموعة الاذون' };
                        $scope.bill.billMainAccount = { "accountId": 0, "accountName": "--", "value": 0 };

                        $scope.GetOrderGroupNames();
                    }
                }
            }
        });


        $scope.bill = {
            "billId": 1,
            "type": "inventory",
            "billName": { "billName": "الاذن / الفاتورة", "typeName": "-", "typeId": 0 },
            "billNo": 000,
            "billDate": "1/1/2016",
            "billMainAccount": { "accountId": 0, "accountName": "--", "value": 0 },
            "billTitleaccount": { "accountId": 0, "accountName": "حدد الحساب" },
            "billItems": [],
            "billOtherTotalAcounts": [],
            "billTotalAccount": { "accountId": 0, "accountName": "--", "value": 0 },
            "billType": { "typeId": 0, "typeName": "--" }
        }

        //$scope.swtchbill();
        $scope.saveBill = function () {
            if ($scope.vaildBill()) {
                toastr.error("لا يمكن الحفظ نظرا لعدم اكتمال الفاتورة");
                return;
            }
            var billToSave = {};
            billToSave = angular.copy($scope.bill);
            billToSave.billItems = angular.copy($scope.rowColumns);
            billToSave.billTotalAccount.value = angular.copy($scope.orderTotal());
            billToSave.isCompany = $scope.branchGroub.isCompany;
            billToSave.comapnyId = $scope.branchGroub.companyId;
            billToSave.orderGroupId = $scope.selectedOrderGroupName.orderGroupId;
            billToSave.billType = { typeId: 0, typeName: $scope.mainType };
            billToSave.billDate = moment.utc();
            delete billToSave.billTotalAccount.billPaymethods["accounts"];
            console.log("bullToSAve", angular.toJson(billToSave));
            BalanceSheetspersistenceService.action.saveBill(angular.toJson(billToSave)).then(
               function (result) {
                   console.log('OrderGroups', result)
                   //
                   $scope.isNewbill = false;
                   $scope.bill.billNo = result.data;


               },
               function (error) {
                   toastr.error("حدث خطاء - اثناء حفظ البيانات");
               });

        };

        $scope.vaildBill = function () {
            var bill = $scope.bill;
            if ($scope.isNewbill) {
                bill.billDate = moment().format('DD/MM/YYYY');
            }

            if (bill == null) {
                console.log("null bill");
                return true
            };
            if (bill.billName == null) {
                console.log("billName");
                return true
            };
            if (bill.billName.typeId == null || bill.billName.typeId == 0) {
                console.log("bill.billName.typeId");
                return true;
            };
            if (bill.billNo == null || bill.billName <= 0) {
                console.log("billNo");

                return true;
            };
            if (bill.billMainAccount == null || bill.billMainAccount.accountId == 0) {
                console.log("billMainAccount");
                return true;
            }
            if (bill.billTitleaccount == null || bill.billTitleaccount.accountId == 0) {
                console.log("titleAccounts");
                return true;
            }

            //billOtherTotalAcounts checker
            if (bill.billOtherTotalAcounts !== null) {
                for (var i = 0; i < bill.billOtherTotalAcounts.length; i++) {
                    if (!isNaN(parseFloat(bill.billOtherTotalAcounts[i].value))) {
                        if (parseFloat(bill.billOtherTotalAcounts[i].value) < 0) {
                            console.log("billOtherTotalAcounts less 0");
                            return true;
                        }

                    } else {
                        console.log("billOtherTotalAcounts not num");
                        return true;
                    }
                }
            }

            if (bill.billTotalAccount == null) {
                console.log("total account = null");

                return true;

            }
            if (bill.billTotalAccount.accountId == 0 || bill.billTotalAccount.accountName === "حدد الحساب") {
                console.log("total account حدد");

                return true;

            }
            if (bill.billTotalAccount.billPaymethods == null || bill.billTotalAccount.billPaymethods.payId == 0) {
                console.log("pay methed");

                return true;
            }

            if (bill.billTotalAccount.billPaymethods == null || bill.billTotalAccount.billPaymethods.payId == 0) {
                console.log("pay methed payId");
                return true;
            };
            if (bill.billTotalAccount.billPaymethods.billPaymethodProperties !== null) {
                for (var i = 0; i < bill.billTotalAccount.billPaymethods.billPaymethodProperties.length; i++) {
                    var prop = bill.billTotalAccount.billPaymethods.billPaymethodProperties[i];
                    if (prop.isRequied == true) {
                        if (prop.value == null || prop.value == "") {
                            console.log("value == null total account");
                            return true;
                        };
                        if (prop.typeName == "عدد صحيح" || prop.typeName == "عدد عشري") {
                            if (!isNaN(parseFloat(prop.value))) {
                                if (parseFloat(prop.value) <= 0) {
                                    console.log("less than 0 total account");
                                    return true;
                                }

                            } else {
                                console.log("un vald num totalaccount");
                                return true;
                            }
                        };
                        if (prop.type === "تاريخ - اشعار" || prop.type === "تاريخ") {
                            var date = moment(prop);
                            if (!date.isValid()) {
                                console.log("date in total account un vaild");
                                return true;
                            }
                        };

                    }
                }
            }


            //billItems Checker
            if ($scope.rowColumns == null || $scope.rowColumns.length === 0) {
                console.log("no rows rowColumns");
                return false;
            };

            for (var i = 0; i < $scope.rowColumns.length; i++) {
                for (var x = 0; x < $scope.rowColumns[i].length; x++) {
                    var item = $scope.rowColumns[i][x];
                    if (item.type === "item" && item.itemPropertyValueName === "") {
                        console.log("item name missing");
                        return true;
                    }
                    if (item.type === "code" && item.itemPropertyValueName === "") {
                        console.log("item code missing");
                        return true;

                    }
                    if (item.type === "price") {
                        if (!isNaN(parseFloat(item.itemPropertyValueName))) {
                            if (parseFloat(item.itemPropertyValueName) <= 0) {
                                console.log("item price less ");
                                return true;
                            }

                        } else {
                            console.log("price not num");
                            return true;
                        }

                    }
                    if (item.type === "qty") {
                        if (!isNaN(parseFloat(item.itemPropertyValueName))) {
                            if (parseFloat(item.itemPropertyValueName) < 1) {
                                console.log("item qty less 1");
                                return true;
                            }

                        } else {
                            console.log("item qty not num");
                            return true;
                        }

                    }
                    if (item.type === "debit") {
                        if (!isNaN(parseFloat(item.itemPropertyValueName))) {
                            if (parseFloat(item.itemPropertyValueName) < 0) {
                                console.log("item account less");
                                return true;
                            }

                        } else {
                            console.log("item account not num");
                            return true;
                        }

                    }

                    if (item.type === "crdit") {
                        if (!isNaN(parseFloat(item.itemPropertyValueName))) {
                            if (parseFloat(item.itemPropertyValueName) < 0) {
                                console.log("item account less 0");
                                return true;
                            }

                        } else {
                            console.log("item account not num");
                            return true;

                        }

                    }
                }
            }

            //ebd of billItems Checker


            return false;
        };

        $scope.rowColumns = [];

        $scope.re = function () {
            $scope.bill.billItems.forEach(function (column, columnIndex) {
                column.itemPropertyValues.forEach(function (card, rowIndex) {
                    if (!$scope.rowColumns[rowIndex])
                        $scope.rowColumns[rowIndex] = [];
                    $scope.rowColumns[rowIndex][columnIndex] = card;
                });
            });


        };

        $scope.re();
        $scope.addNewitemPropertyValue = function () {
            var newitem = angular.copy($scope.rowColumns[0]);
            console.log("new Items", newitem);
            for (var i = 0; i < newitem.length; i++) {

                newitem[i].itemPropertyValueName = "";
            }
            $scope.rowColumns.push(newitem);
            var id = '#ui' + $scope.rowColumns.length;
            setTimeout(function () { $(id).focus() }, 200);


        }
        $scope.removeItemPropertyValue = function (item) {
            console.log("Item : ", angular.toJson(item));
            $scope.rowColumns.splice(item, 1);
        };
        $scope.calcTotal = 0;

        $scope.orderTotal = function () {
            var rows = [];
            for (var i = 0; i < $scope.rowColumns.length; i++) {
                rows.push($scope.getitemsTotalRow($scope.rowColumns[i]));

            }


            var sum = rows.reduce((a, b) => a + b, 0);
            var accounts = [];
            for (var i = 0; i < $scope.bill.billOtherTotalAcounts.length; i++) {
                if ($scope.bill.billOtherTotalAcounts[i].type === "debit") {
                    if (!isNaN(parseFloat($scope.bill.billOtherTotalAcounts[i].value))) {
                        accounts.push(parseFloat($scope.bill.billOtherTotalAcounts[i].value));
                    }
                }
                else if ($scope.bill.billOtherTotalAcounts[i].type === "crdit") {
                    if (!isNaN(parseFloat($scope.bill.billOtherTotalAcounts[i].value))) {
                        //console.log("crdit", -1 * parseFloat($scope.bill.billOtherTotalAcounts[i].value));
                        accounts.push(-1 * parseFloat($scope.bill.billOtherTotalAcounts[i].value));
                    }
                }
            }

            var sumAccounts = accounts.reduce((a, b) => a + b, 0);
            return sum + sumAccounts;
        };
        $scope.getitemsTotalRow = function (row) {

            var itemObj = angular.toJson(row);

            var parsitem = JSON.parse(itemObj);
            itemObj = parsitem;


            var total = 0;
            var price = 0;
            var qty = 0;
            var totalPrice = 0;
            var accounts = [];

            for (var x = 0; x < itemObj.length; x++) {
                if (itemObj[x].type !== "item" || itemObj[x].type !== "code") {

                    if (itemObj[x].type === "price") {
                        if (!isNaN(parseFloat(itemObj[x].itemPropertyValueName))) {
                            price = parseFloat(itemObj[x].itemPropertyValueName);

                        }

                    }
                    else if (itemObj[x].type === "qty") {
                        if (!isNaN(parseFloat(itemObj[x].itemPropertyValueName))) {
                            qty = parseFloat(itemObj[x].itemPropertyValueName);
                        }

                    }
                    else {
                        if (itemObj[x].type === "crdit") {
                            if (!isNaN(parseFloat(itemObj[x].itemPropertyValueName))) {
                                accounts.push(-1 * parseFloat(itemObj[x].itemPropertyValueName));
                            }

                        }
                        else if (itemObj[x].type === "debit") {
                            if (!isNaN(parseFloat(itemObj[x].itemPropertyValueName))) {
                                accounts.push(parseFloat(itemObj[x].itemPropertyValueName));
                            }
                        }


                    }
                }

            }

            var sum = accounts.reduce((a, b) => a + b, 0);

            totalPrice = (price * qty) + sum;

            return totalPrice;
        };



        $scope.selectedOrderGroupName = { orderGroupId: 0, orderGroupName: 'حدد مجموعة الاذون' };
        $scope.GetOrderGroupNames = function () {
            BalanceSheetspersistenceService.action.GetOrderGroupNames(JSON.stringify({ id: $scope.branchGroub.companyId, isCompany: $scope.branchGroub.isCompany })).then(
                  function (result) {
                      console.log('OrderGroups', result)
                      $scope.orderGroupNames = result.data;
                      if ($localStorage.selectedOrderGroupSales) {
                          $scope.selectedOrderGroupName = $localStorage.selectedOrderGroupSales;
                      }

                  },
                  function (error) {
                      toastr.error("حدث خطاء - اثناء طلب البيانات");
                  });
        };
        $scope.isNewbill = true;
        $scope.confirmNewOrderClicked = function () {
            $scope.isNewbill = true;
            $scope.bill.billNo = 0000;
            var ordername = $filter('filter')($scope.billNames, { billName: $scope.bill.billName.billName }, true);
            $scope.bill.billTitleaccount = { "accountId": 0, "accountName": "حدد الحساب" };
            $scope.bill.billTotalAccount.accountId = 0;
            $scope.bill.billTotalAccount.accountName = "حدد الحساب";
            $scope.bill.billName.typeName = angular.copy(ordername[0].typeName);
            $scope.bill.billName.typeId = angular.copy(ordername[0].typeId);
            $scope.rowColumns = [];

            $scope.re();
        };
        $scope.getBillById = function (billId) {
            BalanceSheetspersistenceService.action.getBillById(JSON.stringify({ billId: billId, CompanyId: $scope.branchGroub.companyId, isCompny: $scope.branchGroub.isCompany, type: $scope.mainType, billTypeName: $scope.bill.billName.typeName, orderGroupId: $scope.selectedOrderGroupName.orderGroupId })).then(
                  function (result) {
                      console.log('bill BY ID', result)
                      //$scope.bill = result.data;
                      $scope.isNewbill = false;
                      $scope.rowColumns = result.data.billItems;
                      $scope.bill.worker = result.data.worker;
                      $scope.bill.billTotalAccount.accountId = result.data.billTotalAccount.accountId;
                      $scope.bill.billTitleaccount.accountId = result.data.billTitleaccount.accountId;
                      $scope.bill.billDate = moment(result.data.billDate).format('DD/MM/YYYY');
                      var acc = {
                          payId: result.data.billTotalAccount.billPaymethods.payId,
                          payName: result.data.billTotalAccount.billPaymethods.payName,
                          accounts: [{ accountId: result.data.billTotalAccount.accountId, accountName: result.data.billTotalAccount.accountName }],
                          billPaymethodProperties: result.data.billTotalAccount.billPaymethods.billPaymethodProperties
                      };
                      $scope.getSelectedPay(acc);
                  },
                  function (error) {
                      toastr.error("لا توجد فاتورة مسجلة بهذا الرقم");
                      $scope.isNewbill = true;
                      $scope.bill.billNo = 0000;
                      var ordername = $filter('filter')($scope.billNames, { billName: $scope.bill.billName.billName }, true);
                      $scope.bill.billTitleaccount = { "accountId": 0, "accountName": "حدد الحساب" };
                      $scope.bill.billTotalAccount.accountId = 0;
                      $scope.bill.billTotalAccount.accountName = "حدد الحساب";
                      $scope.bill.billName.typeName = angular.copy(ordername[0].typeName);
                      $scope.bill.billName.typeId = angular.copy(ordername[0].typeId);
                      $scope.rowColumns = [];

                      $scope.re();
                      return false;
                  });
        };

        //ng-model="selectedPay" ng-change="getSelectedPay()"
        $scope.selectedPay = { accounts: [] };
        $scope.getSelectedPay = function (selected) {
            console.log('selected Pay', selected)


            $scope.selectedPay = selected;
            if ($scope.isNewbill) {
                $scope.bill.billTotalAccount.accountName = "حدد الحساب";
                $scope.bill.billTotalAccount.accountId = 0;
            }
            $scope.bill.billTotalAccount.billPaymethods = $scope.selectedPay;
        }

        $scope.keys = function (event) {

            console.log("$event", event);
            if (event.altKey) {
                switch (event.which) {
                    case 37:
                        //alert("shift + left arrow");
                        break;
                    case 38:
                        //alert("shift + up arrow");
                        break;
                    case 39:
                        //alert("shift + right arrow");
                        break;
                    case 78:
                        $scope.addNewitemPropertyValue();
                        break;
                    default:
                        break;
                }
            }
            //if (event.keyCode == 40 && event.keyCode == 16) {
            //    alert('you pressed SHIFT+A');
            //}

        }
        $scope.getOrderToBill = function () {
            //getOrderToBillById

            BalanceSheetspersistenceService.action.getOrderToBillById(JSON.stringify({ orderGroupId: $scope.selectedOrderGroupName.orderGroupId, InventoryId: $scope.bill.billMainAccount.accountId, Type: $scope.mainType, CompanyId: $scope.branchGroub.companyId, isCompny: bId })).then(
                   function (result) {
                       console.log('BillToOrder', result)
                       $scope.bill.billItems = result.data.billItems;
                       $scope.bill.billOtherTotalAcounts = result.data.billOtherAccounts;
                       $scope.itemsforBill = result.data.billitemsQty
                       $scope.billPaymethods = result.data.billPaymethods;
                       $scope.titleAccounts = result.data.titleAccounts;
                       $scope.billNames = result.data.billNames;
                       $scope.bill.billTitleaccount = { "accountId": 0, "accountName": "حدد الحساب" };
                       $scope.bill.billName.billName = $scope.billNames[0].billName
                       $scope.bill.billName.typeName = $scope.billNames[0].typeName;
                       $scope.bill.billName.typeId = $scope.billNames[0].typeId;
                       $scope.isNewbill = true;
                       $scope.bill.billNo = 0000;
                       $scope.bill.billTotalAccount.accountId = 0;
                       $scope.bill.billTotalAccount.accountName = "حدد الحساب";


                       $scope.rowColumns = [];
                       $scope.re();

                   },
                   function (error) {
                       toastr.error("حدث خطاء - اثناء طلب البيانات");
                   });
        }

        $scope.$watch('selectedOrderGroupName.orderGroupId', function (newVal, oldVal) {
            console.log('selected selectedItemGroup.itemGroupId', newVal + '- old:' + oldVal)
            if (newVal !== oldVal && newVal !== 0) {

                console.log('newVal[0]', newVal)

                BalanceSheetspersistenceService.action.getOrderGroupforBill(newVal).then(
                        function (result) {
                            $scope.bill.billMainAccount = { "accountId": 0, "accountName": "--", "value": 0 };

                            $scope.selectedOrderGroupName = result;
                            $scope.totalAccounts = {};
                            getTotalAccounts();
                            $localStorage.selectedOrderGroupSales = $scope.selectedOrderGroupName;
                            if ($localStorage.billMainAccountSales) {
                                $scope.bill.billMainAccount = $localStorage.billMainAccountSales;
                            }
                        },
                        function (error) {
                            toastr.error("حدث خطاء - اثناء طلب البيانات");
                        });


            };
        });

        $scope.totalAccounts = {};
        var getTotalAccounts = function () {
            //GetTotalaccounts

            BalanceSheetspersistenceService.action.GetTotalaccounts($scope.selectedOrderGroupName.orderGroupId).then(
                    function (result) {
                        $scope.totalAccounts = result;
                    },
                    function (error) {
                        toastr.error("حدث خطاء - اثناء طلب البيانات");
                    });

        };


        $scope.$watch('bill.billTotalAccount.accountId', function (newVal, oldVal) {
            console.log('selected selectedItemGroup.itemGroupId', newVal + '- old:' + oldVal)
            if (newVal !== oldVal && newVal !== 0) {

                console.log('newVal[0]', newVal)
                var ordername = $filter('filter')($scope.selectedPay.accounts, { accountId: newVal }, true);

                $scope.bill.billTotalAccount.accountName = angular.copy(ordername[0].accountName);


            };
        });

        $scope.$watch('bill.billTitleaccount.accountId', function (newVal, oldVal) {
            console.log('selected selectedItemGroup.itemGroupId', newVal + '- old:' + oldVal)
            if (newVal !== oldVal && newVal !== 0) {

                console.log('newVal[0]', newVal)
                var ordername = $filter('filter')($scope.titleAccounts, { accountId: newVal }, true);

                $scope.bill.billTitleaccount.accountName = angular.copy(ordername[0].accountName);


            };
        });
        $scope.selectedOrderName = { ordersNameId: 0, orderName: 'حدد الاذن' };
        $scope.$watch('bill.billId', function (newVal, oldVal) {
            console.log('selected selectedItemGroup.itemGroupId', newVal + '- old:' + oldVal)
            if (newVal !== oldVal) {

                console.log('newVal[0]', newVal)
                var ordername = $filter('filter')($scope.selectedOrderGroupName.ordersNames, { ordersNameId: newVal }, true);

                $scope.bill.billName = angular.copy(ordername[0].orderName);


            };
        });

        //bill.billName.billName
        //$scope.$watch('bill.billName.billName', function (newVal, oldVal) {
        //    console.log('selected selectedItemGroup.itemGroupId', newVal + '- old:' + oldVal)
        //    if (newVal !== oldVal && newVal !== 'الاذن / الفاتورة') {

        //        console.log('newVal[0]', newVal)
        //        var ordername = $filter('filter')($scope.billNames, { billName: newVal }, true);
        //        console.log('sordername', ordername)
        //        $scope.bill.billTitleaccount = { "accountId": 0, "accountName": "حدد الحساب" };
        //        $scope.bill.billName.typeName = angular.copy(ordername[0].typeName);
        //        $scope.bill.billName.typeId = angular.copy(ordername[0].typeId);
        //        $scope.isNewbill = true;
        //        $scope.bill.billNo = 0000;
        //        $scope.bill.billTotalAccount.accountId = 0;
        //        $scope.bill.billTotalAccount.accountName = "حدد الحساب";


        //        $scope.rowColumns = [];

        //        $scope.re();
        //        // $scope.bill.billMainAccount.accountId = angular.copy(ordername[0].accountId);

        //    };
        //});

        $scope.$watch('bill.billMainAccount.accountId', function (newVal, oldVal) {
            console.log('selected selectedItemGroup.itemGroupId', newVal + '- old:' + oldVal)
            if (newVal !== oldVal && newVal !== 0) {
                $scope.bill.billType = { "typeId": 0, "typeName": "--" };
                console.log('newVal[0]', newVal)
                var ordername = $filter('filter')($scope.selectedOrderGroupName.orderMainAccounts, { accountId: newVal }, true);
                console.log('sordername', ordername)

                $scope.bill.billMainAccount.accountName = angular.copy(ordername[0].accountName);
                $scope.bill.billName = { "billName": "الاذن / الفاتورة", "typeName": "-", "typeId": 0 };
                $scope.getOrderToBill();
                // $scope.bill.billMainAccount.accountId = angular.copy(ordername[0].accountId);
                $localStorage.billMainAccountSales = $scope.bill.billMainAccount;

            };
        });
        $scope.itemcodebill = "";
        $scope.getItembyCode = function (item, row, id, key, keyrow) {

            var itemObj = row;
            console.log("item", item);
            console.log("row", row);
            console.log(id);


            for (var i = 0; i < $scope.rowColumns.length; i++) {
                var a;
                for (var x = 0; x < $scope.rowColumns[i].length; x++) {
                    if ($scope.rowColumns[i][x].type === "code" && $scope.rowColumns[i][x].itemPropertyValueName === item.code && $scope.rowColumns[i] !== $scope.rowColumns[keyrow]) {
                        console.log('reapeted', $scope.rowColumns[i][x]);

                        $scope.rowColumns.splice(keyrow, 1);

                        var idindex = '#qty' + i + 3;
                        //$(id).focus();
                        console.log("from repated index", idindex);

                        setTimeout(function () { $(idindex).focus() }, 200);
                        return;
                    }
                }
            };

            for (var x = 0; x < itemObj.length; x++) {

                if (itemObj[x].type === "item") {
                    itemObj[x].itemPropertyValueId = item.itemPropertyValueId;
                    itemObj[x].itemPropertyValueName = item.itemPropertyValueName;

                }
                if (itemObj[x].type === "code") {
                    itemObj[x].itemPropertyValueId = item.itemPropertyValueId;
                    itemObj[x].itemPropertyValueName = item.code;

                }
                if (itemObj[x].type === "price") {
                    itemObj[x].itemPropertyValueName = item.price;

                }
                else if (itemObj[x].type === "qty") {
                    itemObj[x].itemPropertyValueName = 1;

                } else if (itemObj[x].type === "debit" || itemObj[x].type === "crdit") {
                    itemObj[x].itemPropertyValueName = "0";

                }

            }



            //document.getElementsByClassName(id).focus();
            // id = '.' + id;
            var idindex = '#qty' + keyrow + 3;
            //$(id).focus();
            console.log("=index", idindex);
            setTimeout(function () { $(idindex).focus() }, 200);

        };
        $scope.getItembyitem = function () {

        };



        var checkbrGroup = function () {
            if ($scope.branchGroub && $scope.branchGroub.id != '0') {
                bId = parseInt(angular.copy(parseInt($scope.branchGroub.id)));
                $scope.selectedOrderGroupName = { orderGroupId: 0, orderGroupName: 'حدد مجموعة الاذون' };
                $scope.bill.billMainAccount = { "accountId": 0, "accountName": "--", "value": 0 };

                $scope.GetOrderGroupNames();
            }
        }();

        //END
    });

}());


