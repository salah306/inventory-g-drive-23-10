(function () {

    'use strict';
    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("accountingDepartmentController", function ($scope, _, AccountspersistenceService, $rootScope, settings,
                                                           AccountCategoriespersistenceService, BalanceSheetspersistenceService,
                                                           BalanceSheetTypespersistenceService, branchespersistenceService,
                                                           companiespersistenceService, Offline, $location)
    {

        $scope.showList = false;
        $scope.AccountsNames = [];
        $scope.AccountpropertyValues = [];
        $scope.PropertyValues = [];
        $scope.Companies = [];
        $scope.Branches = [];
        $scope.BalanceSheets = [];
        $scope.BalanceSheetTypes = [];
        $scope.AccountCategories = [];
        $scope.Accounts = [];
        $scope.tree = [];

        

        console.log("tree")


        /* Setup general page controller */
        $scope.$on('$viewContentLoaded', function () {
            // initialize core components
            App.initAjax();

            // set default layout mode
            $rootScope.settings.layout.pageContentWhite = true;
            $rootScope.settings.layout.pageBodySolid = false;
            $rootScope.settings.layout.pageSidebarClosed = false;
        });

        //Strating Retrive Data 

        // Adding All company Data
        var Company = function getAccount() {
            companiespersistenceService.action.getAll().then(
                function (Companies) {
                    $scope.Companies = Companies;
                    $scope.tree.Companies = [];
                    $scope.tree.Companies= Companies;
                    //alert(Companies)
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (Companies.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        }();
      
      

        // Adding All Branch Data
       
        var branch = function getAccount() {
            branchespersistenceService.action.getAll().then(
                function (Branches) {
                    $scope.Branches = Branches;

                    angular.forEach(Branches, function (branch, key) {
                        angular.forEach($scope.tree.Companies, function (company, key) {
                            if (branch.companiesId == company.name) {
                                $scope.tree.Companies.Branches = [];
                                $scope.tree.Companies.push(branch)
                            }
                        });
                    });
                    $scope.tree.Companies.Branches = Companies;
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (Branches.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        }();

        // Adding All BalanceSheets Data
        var balanceSheet = function getAccount() {
            BalanceSheetspersistenceService.action.getAll().then(
                function (BalanceSheets) {
                    $scope.BalanceSheets = BalanceSheets;
                    console.log(BalanceSheets);
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (BalanceSheets.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        }();

        // Adding All BalanceSheetTypes Data
        var balanceSheetTypes = function getAccount() {
            BalanceSheetTypespersistenceService.action.getAll().then(
                function (BalanceSheetTypes) {
                    $scope.BalanceSheetTypes =  BalanceSheetTypes;
                    console.log( BalanceSheetTypes);
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (BalanceSheetTypes.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        }();

        console.log($scope.tree.Companies = $scope.Companies);

        console.log($scope.Companies);

        //var getData = function () {

        //    AccountspersistenceService.action.getById(accountName).then(
        //        function (Accounts) {
        //            $scope.Accounts = Accounts;


        //            $scope.showList = true;
        //            $scope.showEmptyListMessage = (Accounts.length === 0);
        //        },
        //        function (error) {
        //            $scope.error = error;
        //        });
        //};
        //var lazyGetData = _.debounce(getData, 2);

        //Offline.on('confirmed-down', lazyGetData);
        //Offline.on('confirmed-up', lazyGetData);

        //lazyGetData();

        var getAccountNameData = function () {

            AccountspersistenceService.action.getAll().then(
                function (Accounts) {
                    $scope.AccountsNames = Accounts;

                    console.log(Accounts);
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (Accounts.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };


        var lazyGetAccountNameData = _.debounce(getAccountNameData, 2);

        Offline.on('confirmed-down', lazyGetAccountNameData);
        Offline.on('confirmed-up', lazyGetAccountNameData);

        lazyGetAccountNameData();


        function getValueByKey(key, data) {
            var i, len = data.length;

            for (i = 0; i < len; i++) {
                if (data[i] && data[i].hasOwnProperty(key)) {
                    return data[i][key];
                }
            }

            return -1;
        }

        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;   //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        // $scope.catogeries = AccountRepository.getcatogery();

        $('#loading').hide();


        // Save 

        $scope.showSuccessMessage = false;
        $scope.showFillOutFormMessage = false;
        $scope.isOnline = true;
        $scope.addAccount = {};







        var hasAccountToSave = function () {



            console.log($scope.addAccount)

            var hasValue = function (value) {

                if (typeof value === 'string') {
                    return value.length > 0;
                }

                return value > 0;
            };

            var returnValue =
                       hasValue($scope.addAccount.name)
                    && hasValue($scope.addAccount.price)
                    && hasValue($scope.addAccount.catogery);

            return returnValue;
        };

        $scope.save = function () {
            if ($scope.addAccount.id === null || $scope.addAccount.id === undefined) {
                $scope.addAccount.id = $scope.addAccount.name;
            }

            var saveAccount = hasAccountToSave();


            var Account = $scope.addAccount;


            //;

            AccountspersistenceService.action.save(Account).then(
                function (result) {
                    $scope.showSuccessMessage = true;
                    $scope.showErrorMessage = false;
                    $scope.addAccount = {};
                },
                function (error) {
                    $scope.showSuccessMessage = false;
                    $scope.showErrorMessage = true;
                });



        };

        Offline.on('confirmed-down', function () {
            $scope.$apply(function () {
                $scope.isOnline = false;
            });
        });

        Offline.on('confirmed-up', function () {
            $scope.$apply(function () {
                $scope.isOnline = true;
            });
        });

        //Edit and update
        var AccountToEdit = [];

        $scope.editingAccount = {};


        $scope.modify = function (Account) {
            for (var i = 0, length = $scope.Accounts.length; i < length; i++) {
                $scope.editingAccount[$scope.Accounts[i].id] = false;
            }
            AccountToEdit = angular.copy(Account);
            $scope.addAccount = Account;
            $scope.editingAccount[Account.id] = true;

        };


        $scope.update = function (Account) {
            $scope.editingAccount[Account.id] = false;
        };

        $scope.cancel = function (Account) {

            Account.name = AccountToEdit.name;
            Account.price = AccountToEdit.price;
            Account.catogery = AccountToEdit.catogery;

            $scope.editingAccount[Account.id] = false;

        };

        $scope.reset = function () {
            for (var i = 0, length = $scope.Accounts.length; i < length; i++) {
                $scope.editingAccount[$scope.Accounts[i].id] = false;
            }
            $scope.addAccount = {};
        };


        //delte
        var delteAccount = {};
        $scope.AccountToDelte = function (Account) {

            delteAccount = Account;
        };


        $scope.delete = function () {


            AccountspersistenceService.action.Delete(delteAccount.id).then(
                function (result) {
                    $scope.Accounts.splice($scope.Accounts.indexOf(delteAccount), 1);
                    delteAccount = {};
                },
                function (error) {
                    $scope.error = error;
                });
        };

        console.log('start tree');

        console.log($scope.tree);
        console.log('end of tree 2');
        //AccountspersistenceService.getById(accountName).then(
        //    function (Accounts) {
        //        $scope.Accounts = Accounts;
        //    },
        //    function (error) {
        //        $scope.error = error;
        //    });

    });

}());
