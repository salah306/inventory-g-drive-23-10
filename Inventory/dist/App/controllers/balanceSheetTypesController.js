(function () {

    'use strict';
    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("balanceSheetTypesController", function ($scope, _, BalanceSheetTypespersistenceService, Offline, $location ,  $routeParams) {

        var BalanceSheetTypeName = $routeParams.name;
        $scope.catName = $routeParams.name;
        console.log(BalanceSheetTypeName);
        $scope.showList = false;
        $scope.BalanceSheetTypes = [];
        $scope.BalanceSheetTypesNames = [];
        $scope.BalanceSheetTypepropertyValues = [];
        $scope.PropertyValues = [];
        var getData = function () {

            BalanceSheetTypespersistenceService.action.getById(BalanceSheetTypeName).then(
                function (BalanceSheetTypes) {
                    $scope.BalanceSheetTypes = BalanceSheetTypes;
                   
                    
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (BalanceSheetTypes.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };
        var lazyGetData = _.debounce(getData, 2);

        Offline.on('confirmed-down', lazyGetData);
        Offline.on('confirmed-up', lazyGetData);

        lazyGetData();

        var getBalanceSheetTypeNameData = function () {

            BalanceSheetTypespersistenceService.action.getAll().then(
                function (BalanceSheetTypes) {
                    $scope.BalanceSheetTypesNames = BalanceSheetTypes;

                    console.log(BalanceSheetTypes);
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (BalanceSheetTypes.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };

       
        var lazyGetBalanceSheetTypeNameData = _.debounce(getBalanceSheetTypeNameData, 2);

        Offline.on('confirmed-down', lazyGetBalanceSheetTypeNameData);
        Offline.on('confirmed-up', lazyGetBalanceSheetTypeNameData);

        lazyGetBalanceSheetTypeNameData();
       

        function getValueByKey(key, data) {
            var i, len = data.length;

            for (i = 0; i < len; i++) {
                if (data[i] && data[i].hasOwnProperty(key)) {
                    return data[i][key];
                }
            }

            return -1;
        }

        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;   //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        // $scope.catogeries = BalanceSheetTypeRepository.getcatogery();

        $('#loading').hide();


        // Save 

        $scope.showSuccessMessage = false;
        $scope.showFillOutFormMessage = false;
        $scope.isOnline = true;
        $scope.addBalanceSheetType = {};







        var hasBalanceSheetTypeToSave = function () {



            console.log($scope.addBalanceSheetType)

            var hasValue = function (value) {

                if (typeof value === 'string') {
                    return value.length > 0;
                }

                return value > 0;
            };

            var returnValue =
                       hasValue($scope.addBalanceSheetType.name)
                    && hasValue($scope.addBalanceSheetType.price)
                    && hasValue($scope.addBalanceSheetType.catogery);

            return returnValue;
        };

        $scope.save = function () {
            if ($scope.addBalanceSheetType.id === null || $scope.addBalanceSheetType.id === undefined) {
                $scope.addBalanceSheetType.id = $scope.addBalanceSheetType.name;
            }

            var saveBalanceSheetType = hasBalanceSheetTypeToSave();


            var BalanceSheetType = $scope.addBalanceSheetType;


            //;

            BalanceSheetTypespersistenceService.action.save(BalanceSheetType).then(
                function (result) {
                    $scope.showSuccessMessage = true;
                    $scope.showErrorMessage = false;
                    $scope.addBalanceSheetType = {};
                },
                function (error) {
                    $scope.showSuccessMessage = false;
                    $scope.showErrorMessage = true;
                });



        };

        Offline.on('confirmed-down', function () {
            $scope.$apply(function () {
                $scope.isOnline = false;
            });
        });

        Offline.on('confirmed-up', function () {
            $scope.$apply(function () {
                $scope.isOnline = true;
            });
        });

        //Edit and update
        var BalanceSheetTypeToEdit = [];

        $scope.editingBalanceSheetType = {};


        $scope.modify = function (BalanceSheetType) {
            for (var i = 0, length = $scope.BalanceSheetTypes.length; i < length; i++) {
                $scope.editingBalanceSheetType[$scope.BalanceSheetTypes[i].id] = false;
            }
            BalanceSheetTypeToEdit = angular.copy(BalanceSheetType);
            $scope.addBalanceSheetType = BalanceSheetType;
            $scope.editingBalanceSheetType[BalanceSheetType.id] = true;

        };


        $scope.update = function (BalanceSheetType) {
            $scope.editingBalanceSheetType[BalanceSheetType.id] = false;
        };

        $scope.cancel = function (BalanceSheetType) {

            BalanceSheetType.name = BalanceSheetTypeToEdit.name;
            BalanceSheetType.price = BalanceSheetTypeToEdit.price;
            BalanceSheetType.catogery = BalanceSheetTypeToEdit.catogery;

            $scope.editingBalanceSheetType[BalanceSheetType.id] = false;

        };

        $scope.reset = function () {
            for (var i = 0, length = $scope.BalanceSheetTypes.length; i < length; i++) {
                $scope.editingBalanceSheetType[$scope.BalanceSheetTypes[i].id] = false;
            }
            $scope.addBalanceSheetType = {};
        };


        //delte
        var delteBalanceSheetType = {};
        $scope.BalanceSheetTypeToDelte = function (BalanceSheetType) {

            delteBalanceSheetType = BalanceSheetType;
        };


        $scope.delete = function () {


            BalanceSheetTypespersistenceService.action.Delete(delteBalanceSheetType.id).then(
                function (result) {
                    $scope.BalanceSheetTypes.splice($scope.BalanceSheetTypes.indexOf(delteBalanceSheetType), 1);
                    delteBalanceSheetType = {};
                },
                function (error) {
                    $scope.error = error;
                });
        };


        BalanceSheetTypespersistenceService.getById(BalanceSheetTypeName).then(
            function (BalanceSheetTypes) {
                $scope.BalanceSheetTypes = BalanceSheetTypes;
            },
            function (error) {
                $scope.error = error;
            });

    });

}());
