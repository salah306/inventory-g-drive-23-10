
(function () {


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("inventoryTransferInControlle", function ($scope, $timeout, _, $localStorage, AccountspersistenceService, $rootScope, settings, $filter,
                                                           AccountOrderspersistenceService, Offline, EasyStoreUserspersistenceService, $location, branchesremotePersistenceStrategy, $sce, globalService, $q, $log) {
        var bId;
        $scope.itemsforBill = [];
        $scope.billPaymethods = [];
        $scope.titleAccounts = [];
        $scope.branchesGroub = [];
        $scope.billNames = [];
        $scope.orderGroupNames = [];

        $scope.branchGroub = $rootScope.mainbranchGroub;

        $scope.$watch('$root.mainbranchGroub.id', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal !== '0') {
                var findCompanybr = angular.copy($filter('filter')($rootScope.mainbranchesGroub, { id: newVal }, true));
                if (findCompanybr) {
                    $scope.branchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    $rootScope.mainbranchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    console.log('$rootScope.mainbranchGroub', $rootScope.mainbranchGroub);
                    if (findCompanybr.length) {
                        $scope.order = {
                            "inventoryTransferId": 0,
                            "orderNo": 0,
                            "orderNoIn": 0,
                            "orderDate": "",
                            "orderDateSecond": "",
                            "userNameFirst": "",
                            "userNameSecond": "",
                            "isDoneFirst": false,
                            "isDoneSecond": false,
                            "cancel": false,
                            "isNewOrder": true,
                            "transferFrom": {
                                "inventoryId": 0,
                                "inventoryName": "",
                                "code": ""
                            },
                            "transferTo": {
                                "inventoryId": 0,
                                "inventoryName": "",
                                "code": ""
                            },
                            "transferItems": [
                              {
                                  "itemId": 0,
                                  "itemName": "",
                                  "itemCode": "",
                                  "qty": 0,
                                  "price": 0,
                                  "note": "",
                                  "noteSecond": null,
                                  "realted": "",
                                  "itemGroupName": "",
                                  "maxQ": 0
                              }
                            ],
                            "companyId": 0,
                            "isCompany": true,
                            "cancelDate": "",
                            "userNameCancel": ""
                        };
                        bId = parseInt(angular.copy(parseInt(findCompanybr[0].id)));

                        EasyStoreUserspersistenceService.action.branchGroupSt(JSON.stringify($scope.branchGroub)).then(function () { }, function () { });
                        $scope.getInventory();
                    }
                }

            }
        });

        $scope.inventoryTo = [];
        $scope.order = {
            "inventoryTransferId": 0,
            "orderNo": 0,
            "orderNoIn": 0,
            "orderDate": "",
            "orderDateSecond": "",
            "userNameFirst": "",
            "userNameSecond": "",
            "isDoneFirst": false,
            "isDoneSecond": false,
            "cancel": false,
            "isNewOrder": true,
            "transferFrom": {
                "inventoryId": 0,
                "inventoryName": "",
                "code": ""
            },
            "transferTo": {
                "inventoryId": 0,
                "inventoryName": "",
                "code": ""
            },
            "transferItems": [
              {
                  "itemId": 0,
                  "itemName": "",
                  "itemCode": "",
                  "qty": 0,
                  "price": 0,
                  "note": "",
                  "noteSecond": null,
                  "realted": "",
                  "itemGroupName": "",
                  "maxQ": 0
              }
            ],
            "companyId": 0,
            "isCompany": true,
            "cancelDate": "",
            "userNameCancel": ""
        }


        $scope.$watch('order.transferFrom.inventoryId', function (newVal, oldVal) {

            if (newVal !== oldVal) {
                var findinventory = angular.copy($filter('filter')($scope.inventory, { inventoryId: newVal }, true));
                $scope.order.transferFrom = findinventory.length ? findinventory[0] : { inventoryId: 0, inventoryName: null };
                if (findinventory.length) {
                    $localStorage.invrntorytransferfromin = $scope.order.transferFrom;
                    $scope.nowDate = moment().format('DD/MM/YYYY');
                }
            }
        });

        $scope.getInventory = function () {
            AccountOrderspersistenceService.action.getInventory(JSON.stringify({ companyId: bId, isComapny: $scope.branchGroub.isCompany })).then(
              function (result) {
                  $scope.inventory = result.data;
                  $scope.nowDate = moment().format('DD/MM/YYYY');
                  if ($localStorage.invrntorytransferfromin) {
                      var accs = $localStorage.invrntorytransferfromin;
                      var findacc = angular.copy($filter('filter')($scope.inventory, { inventoryId: accs.inventoryId }, true));
                      if (findacc.length) {
                          $scope.order.transferFrom.inventoryId = $localStorage.invrntorytransferfromin.inventoryId
                      }
                  };
              },
              function (error) {
                  if (error.data) {
                      if (error.data.message) {
                          toastr.error(error.data.message);
                      }

                  } else {
                      toastr.error("لا يمكن اتمام العملية الان");
                  }

              });
        };

   
        //$scope.getInventoryTo = function () {
        //    if ($scope.order.transferFrom.inventoryName) {
        //        AccountOrderspersistenceService.action.getInventoryTo(JSON.stringify({ inventoryId: $scope.order.transferFrom.inventoryId, inventoryName: $scope.order.transferFrom.inventoryName, companyId: bId, isComapny: $scope.branchGroub.isCompany })).then(
        //            function (result) {
        //                $scope.inventoryTo = result.data.inventoryTransferTo;
        //                if ($localStorage.invrntorytransfertoin) {
        //                    var accs = $localStorage.invrntorytransfertoin;
        //                    var findacc = angular.copy($filter('filter')($scope.inventoryTo, { inventoryId: accs.inventoryId }, true));
        //                    if (findacc.length) {
        //                        $scope.order.transferTo.inventoryId = $localStorage.invrntorytransfertoin.inventoryId;
        //                    }
        //                };
        //            },
        //            function (error) {
        //                console.log(error);
        //                if (error.data) {
        //                    toastr.error(error.data.message);
        //                } else {
        //                    toastr.error("لا يمكن اتمام العملية الان");
        //                } });
        //    }};
      

        $scope.keys = function (event) {

            console.log("$event", event);
            if (event.altKey) {
                switch (event.which) {
                    case 37:
                        //alert("shift + left arrow");
                        break;
                    case 38:
                        //alert("shift + up arrow");
                        break;
                    case 39:
                        //alert("shift + right arrow");
                        break;
                    case 78:
                        //$scope.addNewitemPropertyValue();
                        break;
                    default:
                        break;
                }
            }
        }

        //order.orderNo

        $scope.newOrder = function () {
            console.log($scope.order);
            console.log(angular.toJson($scope.order));
            var neworder = {
                "inventoryTransferId": 0,
                "orderNo": 0,
                "orderDate": moment().format('DD/MM/YYYY'),
                "orderDateSecond": "",
                "userNameFirst": "",
                "userNameSecond": "",
                "isDoneFirst": false,
                "isDoneSecond": false,
                "cancel": false,
                "isNewOrder": true,
                "transferFrom": angular.copy($scope.order.transferFrom),
                "transferTo": angular.copy($scope.order.transferTo),
                "transferItems": [
                  {
                      "itemId": 0,
                      "itemName": "",
                      "itemCode": "",
                      "qty": 0,
                      "price": 0,
                      "note": "",
                      "noteSecond": null,
                      "realted": "",
                      "itemGroupName": "",
                      "maxQ": 0
                  }
                ],
                "companyId": 0,
                "isCompany": true,
                "cancelDate": "",
                "userNameCancel": ""
            };
            $scope.order = neworder;
            $scope.nowDate = moment().format('DD/MM/YYYY');
        };
        $scope.nowDate = moment().format('DD/MM/YYYY');
        $scope.findOrderById = function (data) {
            //GetTransferOrderById
            AccountOrderspersistenceService.action.TransferOrderinById(angular.toJson({ inventoryId: $scope.order.transferFrom.inventoryId, orderNo: parseInt(data) })).then(
                    function (result) {
                        $scope.order = result.data;
                        $scope.order.orderDate = moment(result.data.orderDate).format('DD/MM/YYYY');
                        $scope.order.cancelDate = moment($scope.order.cancelDate).format('DD/MM/YYYY');
                        $scope.order.orderDateSecond = moment($scope.order.orderDateSecond).format('DD/MM/YYYY');
                        $scope.nowDate = moment().format('DD/MM/YYYY');
                    },
                    function (error) {
                        if (error.data) {
                            toastr.error(error.data.message);
                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }

                    });
        };

        $scope.findTransferOrderById = function (data) {
            //GetTransferOrderById
            AccountOrderspersistenceService.action.transferOrderOutforInById(angular.toJson({ inventoryId: $scope.order.transferFrom.inventoryId, orderNo: parseInt(data) })).then(
                    function (result) {
                        $scope.order = result.data;
                        $scope.order.orderDate = moment(result.data.orderDate).format('DD/MM/YYYY');
                        $scope.order.cancelDate = moment($scope.order.cancelDate).format('DD/MM/YYYY');
                        $scope.order.orderDateSecond = moment($scope.order.orderDateSecond).format('DD/MM/YYYY');
                    },
                    function (error) {
                        if (error.data) {
                            toastr.error(error.data.message);
                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }

                    });
        };

        $scope.saveOrder = function () {
            var orderToSave = { orderNo: $scope.order.orderNoIn, orderDate: '', transferFromId: $scope.order.transferFrom.inventoryId, items : $scope.order.transferItems }
            orderToSave.orderDate = moment.utc();
           
            AccountOrderspersistenceService.action.saveTransferOrderin(angular.toJson(orderToSave)).then(
                function (result) {
                    $scope.order = result.data.order;
                    $scope.order.orderDate = moment(result.data.order.orderDate).format('DD/MM/YYYY');
                    $order.cancelDate = moment($order.cancelDate).format('DD/MM/YYYY');
                    $order.orderDateSecond = moment($order.orderDateSecond).format('DD/MM/YYYY');
                    $scope.nowDate = moment().format('DD/MM/YYYY');
                },
                function (error) {
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });


        };

        $scope.waitingorder = [];
        $scope.getwaiting = function () {
            AccountOrderspersistenceService.action.waitingOrderToin().then(
            function (result) {
                $scope.waitingorder = result.data;
                $("#waiting-model").modal("show");
            },
            function (error) {
                if (error.data) {
                    toastr.error(error.data.message);
                } else {
                    toastr.error("لا يمكن اتمام العملية الان");
                }

            });
           
        };

        $scope.waitingorderCount = 0;
        $scope.getwaitinginCount = function () {
            AccountOrderspersistenceService.action.getwaitinginCount().then(
                function (result) {
                    $scope.waitingorderCount = result.data;
                },
                function (error) {});
        };
        $scope.getwaitinginCount();

        $scope.getwaitOrder = function (data) {
            AccountOrderspersistenceService.action.transferOrderOutforInById(angular.toJson({ inventoryId: data.inventoryToId, orderNo: parseInt(data.orderNo) })).then(
                  function (result) {
                      $scope.order = result.data;
                      $scope.order.orderDate = moment(result.data.orderDate).format('DD/MM/YYYY');
                      $scope.order.cancelDate = moment($scope.order.cancelDate).format('DD/MM/YYYY');
                      $scope.order.orderDateSecond = moment($scope.order.orderDateSecond).format('DD/MM/YYYY');
                      $("#waiting-model").modal("hide");
                  },
                  function (error) {
                      if (error.data) {
                          toastr.error(error.data.message);
                      } else {
                          toastr.error("لا يمكن اتمام العملية الان");
                      }

                  });
            
        };

        var checkbrGroup = function () {
            if ($scope.branchGroub && $scope.branchGroub.id != '0') {
                bId = parseInt(angular.copy(parseInt($scope.branchGroub.id)));
                $scope.order.isCompany = $scope.branchGroub.isCompany
                $scope.getInventory();
            }
        }();

    });

}());


