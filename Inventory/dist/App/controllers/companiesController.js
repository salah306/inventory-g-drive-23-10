(function () {

    'use strict';
    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("companiesController", function ($scope, _, companiespersistenceService, Offline, $location ,  $routeParams) {

        var companyName = $routeParams.name;
        $scope.catName = $routeParams.name;
        console.log(companyName);
        $scope.showList = false;
        $scope.companies = [];
        $scope.companiesNames = [];
        $scope.companypropertyValues = [];
        $scope.PropertyValues = [];
        var getData = function () {

            companiespersistenceService.action.getById(companyName).then(
                function (companies) {
                    $scope.companies = companies;
                   
                    
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (companies.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };
        var lazyGetData = _.debounce(getData, 2);

        Offline.on('confirmed-down', lazyGetData);
        Offline.on('confirmed-up', lazyGetData);

        lazyGetData();

        var getcompanyNameData = function () {

            companiespersistenceService.action.getAll().then(
                function (companies) {
                    $scope.companiesNames = companies;

                    console.log(companies);
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (companies.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };

       
        var lazyGetcompanyNameData = _.debounce(getcompanyNameData, 2);

        Offline.on('confirmed-down', lazyGetcompanyNameData);
        Offline.on('confirmed-up', lazyGetcompanyNameData);

        lazyGetcompanyNameData();
       

        function getValueByKey(key, data) {
            var i, len = data.length;

            for (i = 0; i < len; i++) {
                if (data[i] && data[i].hasOwnProperty(key)) {
                    return data[i][key];
                }
            }

            return -1;
        }

        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;   //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        // $scope.catogeries = companyRepository.getcatogery();

        $('#loading').hide();


        // Save 

        $scope.showSuccessMessage = false;
        $scope.showFillOutFormMessage = false;
        $scope.isOnline = true;
        $scope.addcompany = {};







        var hascompanyToSave = function () {



            console.log($scope.addcompany)

            var hasValue = function (value) {

                if (typeof value === 'string') {
                    return value.length > 0;
                }

                return value > 0;
            };

            var returnValue =
                       hasValue($scope.addcompany.name)
                    && hasValue($scope.addcompany.price)
                    && hasValue($scope.addcompany.catogery);

            return returnValue;
        };

        $scope.save = function () {
            if ($scope.addcompany.id === null || $scope.addcompany.id === undefined) {
                $scope.addcompany.id = $scope.addcompany.name;
            }

            var savecompany = hascompanyToSave();


            var company = $scope.addcompany;


            //;

            companiespersistenceService.action.save(company).then(
                function (result) {
                    $scope.showSuccessMessage = true;
                    $scope.showErrorMessage = false;
                    $scope.addcompany = {};
                },
                function (error) {
                    $scope.showSuccessMessage = false;
                    $scope.showErrorMessage = true;
                });



        };

        Offline.on('confirmed-down', function () {
            $scope.$apply(function () {
                $scope.isOnline = false;
            });
        });

        Offline.on('confirmed-up', function () {
            $scope.$apply(function () {
                $scope.isOnline = true;
            });
        });

        //Edit and update
        var companyToEdit = [];

        $scope.editingcompany = {};


        $scope.modify = function (company) {
            for (var i = 0, length = $scope.companies.length; i < length; i++) {
                $scope.editingcompany[$scope.companies[i].id] = false;
            }
            companyToEdit = angular.copy(company);
            $scope.addcompany = company;
            $scope.editingcompany[company.id] = true;

        };


        $scope.update = function (company) {
            $scope.editingcompany[company.id] = false;
        };

        $scope.cancel = function (company) {

            company.name = companyToEdit.name;
            company.price = companyToEdit.price;
            company.catogery = companyToEdit.catogery;

            $scope.editingcompany[company.id] = false;

        };

        $scope.reset = function () {
            for (var i = 0, length = $scope.companies.length; i < length; i++) {
                $scope.editingcompany[$scope.companies[i].id] = false;
            }
            $scope.addcompany = {};
        };


        //delte
        var deltecompany = {};
        $scope.companyToDelte = function (company) {

            deltecompany = company;
        };


        $scope.delete = function () {


            companiespersistenceService.action.Delete(deltecompany.id).then(
                function (result) {
                    $scope.companies.splice($scope.companies.indexOf(deltecompany), 1);
                    deltecompany = {};
                },
                function (error) {
                    $scope.error = error;
                });
        };


        companiespersistenceService.getById(companyName).then(
            function (companies) {
                $scope.companies = companies;
            },
            function (error) {
                $scope.error = error;
            });

    });

}());
