(function () {

    'use strict';
    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("accountCategoriesController", function ($scope, _, AccountCategoriespersistenceService, Offline, $location) {


        $scope.showList = false;
        $scope.AccountCategories = [];
        var getData = function () {

            AccountCategoriespersistenceService.action.getAll().then(
                function (AccountCategories) {
                    $scope.AccountCategories = AccountCategories;
                    $scope.showList = true;
                    $scope.showEmptyListMessage = (AccountCategories.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };

        var lazyGetData = _.debounce(getData, 2);

        Offline.on('confirmed-down', lazyGetData);
        Offline.on('confirmed-up', lazyGetData);

        lazyGetData();

        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;   //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        // $scope.catogeries = AccountCategoryRepository.getcatogery();

        $('#loading').hide();


        // Save 

        $scope.showSuccessMessage = false;
        $scope.showFillOutFormMessage = false;
        $scope.isOnline = true;
        $scope.addAccountCategory = {};







        var hasAccountCategoryToSave = function () {



            console.log($scope.addAccountCategory)

            var hasValue = function (value) {

                if (typeof value === 'string') {
                    return value.length > 0;
                }

                return value > 0;
            };

            var returnValue =
                       hasValue($scope.addAccountCategory.name)
                    && hasValue($scope.addAccountCategory.price)
                    && hasValue($scope.addAccountCategory.catogery);

            return returnValue;
        };

        $scope.save = function () {
            if ($scope.addAccountCategory.id === null || $scope.addAccountCategory.id === undefined) {
                $scope.addAccountCategory.id = $scope.addAccountCategory.name;
            }

            var saveAccountCategory = hasAccountCategoryToSave();


            var AccountCategory = $scope.addAccountCategory;


            //;

            AccountCategoriespersistenceService.action.save(AccountCategory).then(
                function (result) {
                    $scope.showSuccessMessage = true;
                    $scope.showErrorMessage = false;
                    $scope.addAccountCategory = {};
                },
                function (error) {
                    $scope.showSuccessMessage = false;
                    $scope.showErrorMessage = true;
                });



        };

        Offline.on('confirmed-down', function () {
            $scope.$apply(function () {
                $scope.isOnline = false;
            });
        });

        Offline.on('confirmed-up', function () {
            $scope.$apply(function () {
                $scope.isOnline = true;
            });
        });

        //Edit and update
        var AccountCategoryToEdit = [];

        $scope.editingAccountCategory = {};


        $scope.modify = function (AccountCategory) {
            for (var i = 0, length = $scope.AccountCategories.length; i < length; i++) {
                $scope.editingAccountCategory[$scope.AccountCategories[i].id] = false;
            }
            AccountCategoryToEdit = angular.copy(AccountCategory);
            $scope.addAccountCategory = AccountCategory;
            $scope.editingAccountCategory[AccountCategory.id] = true;

        };


        $scope.update = function (AccountCategory) {
            $scope.editingAccountCategory[AccountCategory.id] = false;
        };

        $scope.cancel = function (AccountCategory) {

            AccountCategory.name = AccountCategoryToEdit.name;
            AccountCategory.price = AccountCategoryToEdit.price;
            AccountCategory.catogery = AccountCategoryToEdit.catogery;

            $scope.editingAccountCategory[AccountCategory.id] = false;

        };

        $scope.reset = function () {
            for (var i = 0, length = $scope.AccountCategories.length; i < length; i++) {
                $scope.editingAccountCategory[$scope.AccountCategories[i].id] = false;
            }
            $scope.addAccountCategory = {};
        };


        //delte
        var delteAccountCategory = {};
        $scope.AccountCategoryToDelte = function (AccountCategory) {

            delteAccountCategory = AccountCategory;
        };


        $scope.delete = function () {


            AccountCategoriespersistenceService.action.Delete(delteAccountCategory.id).then(
                function (result) {
                    $scope.AccountCategories.splice($scope.AccountCategories.indexOf(delteAccountCategory), 1);
                    delteAccountCategory = {};
                },
                function (error) {
                    $scope.error = error;
                });
        };


    });

}());
