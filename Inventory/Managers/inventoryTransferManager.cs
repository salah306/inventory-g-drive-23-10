﻿using Inventory.DataLayer;
using Inventory.Models;
using Inventory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Managers
{
    public class inventoryTransferManager
    {
        private ApplicationDbContext db;

        public inventoryTransferManager()
        {
            db = new ApplicationDbContext();
        }
        public inventoryTransferManager(ApplicationDbContext db)
        {
            this.db = db;
        }

        public IEnumerable<InventoryTransfer> getInventoryMatchTransfers(branchGroupSt company, Int32 inventoryId)
        {
           
            var findInvFrom = (from b in db.Accounts
                               where b.AccountId == inventoryId
                               && b.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                               && b.BranchId == (company.isCompany ? (int?)null : company.oId)
                               && b.AccountCategory.AccountsDataTypes.aliasName == "inventory"
                               select b).SingleOrDefault().AccountsforItemGroups;

            if (findInvFrom == null)
            {
                return null;

            }
            var findInvTo = (from a in db.Accounts
                             where a.AccountId != inventoryId
                             && a.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                            && a.AccountCategory.AccountsDataTypes.aliasName == "inventory"
                             select a);
            if (findInvTo == null)
            {
                return null;

            }

            var invintoryGroup = (from g1 in findInvFrom
                                  from g2 in findInvTo
                                  where g2.AccountsforItemGroups.Any(c => c.ItemGroupId == g1.ItemGroupId)
                                  select g2).Distinct().Select(a => new InventoryTransfer()
                                  {
                                      inventoryId = a.AccountId,
                                      inventoryName = a.AccountName,
                                      code = a.Code
                                  });



            if (invintoryGroup == null)
            {
                return null;

            }

            return invintoryGroup;
        }

        public IEnumerable<itemAvailable> getitemAvailableByinventoryId(Int32 inventoryId, branchGroupSt company, bool addCostPrice = false)
        {
       
            var items = (from a in db.SubAccounts
                         where a.AccountId == inventoryId
               && a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
               && a.Account.BranchId == (company.isCompany ? (int?)null : company.oId)
               && a.Account.AccountCategory.AccountsDataTypes.aliasName == "inventory"
                         select new itemAvailable()
                         {
                             itemId = a.SubAccountId,
                             itemName = a.SubAccountName,
                             itemCode = a.ItemInfo.code,
                             Qty = (from q in a.SubAccountMovements where q.SubAccountMovementDate == (from d in a.SubAccountMovements select d.SubAccountMovementDate).Max() select q.ItemStockQuantity).DefaultIfEmpty(0).FirstOrDefault(),
                             GroupName = a.ItemInfo.ItemGroup.ItemGroupName,
                             price = a.ItemInfo.price,
                             costPrice = addCostPrice ? (from q in a.SubAccountMovements where q.SubAccountMovementDate == (from d in a.SubAccountMovements select d.SubAccountMovementDate).Max() select q.ItemStockPrice).DefaultIfEmpty(0).FirstOrDefault() : 0,

                         });



            if (items == null || items.Count() == 0)
            {
                return null;
            }
            return items;
        }


        public InventoryToQty getTransferInfo(Int32 inventoryId, branchGroupSt company, bool addCostPrice = false)
        {
            var items = getitemAvailableByinventoryId(inventoryId, company, addCostPrice);
            if (items == null)
            {
                return null;
            }
            var transferInventory = getInventoryMatchTransfers(company, inventoryId);
            if (transferInventory == null)
            {
                return null;
            }
            InventoryToQty toReturn = new InventoryToQty()
            {
                items = items,
                InventoryTransferTo = transferInventory
            };
            return toReturn;
        }


        public IEnumerable<itemAvailable> getitemAvailableByNotinventoryId(Int32 inventoryId, branchGroupSt company, bool addCostPrice = false)
        {
          
            var items = (from a in db.SubAccounts
                         where a.AccountId == inventoryId
               && a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
               && a.Account.BranchId == (company.isCompany ? (int?)null : company.oId)
               && a.Account.AccountCategory.AccountsDataTypes.aliasName == "goods-in-transit"
                         select new itemAvailable()
                         {
                             itemId = a.SubAccountId,
                             itemName = a.SubAccountName,
                             itemCode = a.ItemInfo.code,
                             Qty = (from q in a.SubAccountMovements where q.SubAccountMovementDate == (from d in a.SubAccountMovements select d.SubAccountMovementDate).Max() select q.ItemStockQuantity).DefaultIfEmpty(0).FirstOrDefault(),
                             GroupName = a.ItemInfo.ItemGroup.ItemGroupName,
                             price = a.ItemInfo.price,
                             costPrice = addCostPrice ? (from q in a.SubAccountMovements where q.SubAccountMovementDate == (from d in a.SubAccountMovements select d.SubAccountMovementDate).Max() select q.ItemStockPrice).DefaultIfEmpty(0).FirstOrDefault() : 0,

                         });



            if (items == null || items.Any())
            {
                return null;
            }
            return items;
        }

        public InventoryTransferOrderVM getTransferbyId(InventoryTransferById order, branchGroupSt company)
        {
        
            #region checkFroOrder
            var o = db.InventoryTransferEntity.SingleOrDefault(a => a.OrderNo == order.OrderNo && a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.MainAccountId == order.inventoryId);
            if (o == null)
            {
                return null;

            }
            var user = o.CurrentWorker;
            var user2 = o.CurrentWorkerSecond;
            var user3 = o.CurrentWorkerCancel;
            var reurnOrder = new InventoryTransferOrderVM()
            {
                CompanyId = company.oId,
                IsCompany = company.isCompany,
                InventoryTransferId = o.InventoryTransferEntityId,
                OrderNo = o.OrderNo,
                OrderDate = o.OrderDate,
                OrderDateSecond = o.OrderDateSecond,
                CancelDate = o.CancelDate.GetValueOrDefault(),
                IsNewOrder = false,
                cancel = o.Cancel,
                isDoneFirst = o.DoneFirst,
                isDoneSecond = o.DoneSceond,
                transferFrom = new inventorytransferAccount() { inventoryId = o.MainAccountId, inventoryName = o.MainAccount.AccountName, code = o.MainAccount.Code },
                transferTo = new inventorytransferAccount() { inventoryId = o.TitleAccountId, inventoryName = o.TitleAccount.AccountName, code = o.TitleAccount.Code },
                userNameFirst = user != null ? user.FirstName + " " + user.LastName : "",
                userNameSecond = user2 != null ? user2.FirstName + " " + user2.LastName : "",
                userNameCancel = user3 != null ? user3.FirstName + " " + user3.LastName : "",
                transferItems = o.Items.Select(a => new InventoryItemsVm()
                {
                    itemId = a.SubAccountFromId,
                    itemCode = a.itemCode,
                    itemName = a.itemName,
                    ItemGroupName = a.ItemGroup.ItemGroupName,
                    note = a.note,
                    qty = a.qty,
                    maxQ = a.qty,
                    noteSecond = a.noteSecond,
                    price = a.price

                }).ToList()

            };

            #endregion
            return reurnOrder;
        }

        public InventoryTransferOrderVM getTransferinbyId(InventoryTransferById order, branchGroupSt company)
        {
           
            #region checkFroOrder
            var o = db.InventoryTransferEntity.SingleOrDefault(a => a.OrderNoIn == order.OrderNo && a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.TitleAccountId == order.inventoryId);
            if (o == null)
            {
                return null;

            }
            var user = o.CurrentWorker;
            var user2 = o.CurrentWorkerSecond;
            var user3 = o.CurrentWorkerCancel;
            var reurnOrder = new InventoryTransferOrderVM()
            {
                CompanyId = company.oId,
                IsCompany = company.isCompany,
                InventoryTransferId = o.InventoryTransferEntityId,
                OrderNo = o.OrderNoIn,
                OrderNoIn = o.OrderNoIn,
                OrderDate = o.OrderDate,
                OrderDateSecond = o.OrderDateSecond,
                CancelDate = o.CancelDate.GetValueOrDefault(),
                IsNewOrder = false,
                cancel = o.Cancel,
                isDoneFirst = o.DoneFirst,
                isDoneSecond = o.DoneSceond,
                transferTo = new inventorytransferAccount() { inventoryId = o.MainAccountId, inventoryName = o.MainAccount.AccountName, code = o.MainAccount.Code },
                transferFrom = new inventorytransferAccount() { inventoryId = o.TitleAccountId, inventoryName = o.TitleAccount.AccountName, code = o.TitleAccount.Code },
                userNameFirst = user != null ? user.FirstName + " " + user.LastName : "",
                userNameSecond = user2 != null ? user2.FirstName + " " + user2.LastName : "",
                userNameCancel = user3 != null ? user3.FirstName + " " + user3.LastName : "",
                transferItems = o.Items.Select(a => new InventoryItemsVm()
                {
                    itemId = a.SubAccountFromId,
                    itemCode = a.itemCode,
                    itemName = a.itemName,
                    ItemGroupName = a.ItemGroup.ItemGroupName,
                    note = a.note,
                    qty = a.qty,
                    maxQ = a.qty,
                    noteSecond = a.noteSecond,
                    price = a.price

                }).ToList()

            };

            #endregion
            return reurnOrder;
        }
        public InventoryTransferOrderVM getTransferforinbyId(InventoryTransferById order, branchGroupSt company)
        {
           
            #region checkFroOrder
            var o = db.InventoryTransferEntity.SingleOrDefault(a => a.OrderNo == order.OrderNo && a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId) && a.TitleAccountId == order.inventoryId);
            if (o == null)
            {
                return null;

            }
            var user = o.CurrentWorker;
            var user2 = o.CurrentWorkerSecond;
            var user3 = o.CurrentWorkerCancel;
            var reurnOrder = new InventoryTransferOrderVM()
            {
                CompanyId = company.oId,
                IsCompany = company.isCompany,
                InventoryTransferId = o.InventoryTransferEntityId,
                OrderNo = o.OrderNoIn,
                OrderNoIn = o.OrderNo,
                OrderDate = o.OrderDate,
                OrderDateSecond = o.OrderDateSecond,
                CancelDate = o.CancelDate.GetValueOrDefault(),
                IsNewOrder = false,
                cancel = o.Cancel,
                isDoneFirst = o.DoneFirst,
                isDoneSecond = o.DoneSceond,
                transferTo = new inventorytransferAccount() { inventoryId = o.MainAccountId, inventoryName = o.MainAccount.AccountName, code = o.MainAccount.Code },
                transferFrom = new inventorytransferAccount() { inventoryId = o.TitleAccountId, inventoryName = o.TitleAccount.AccountName, code = o.TitleAccount.Code },
                userNameFirst = user != null ? user.FirstName + " " + user.LastName : "",
                userNameSecond = user2 != null ? user2.FirstName + " " + user2.LastName : "",
                userNameCancel = user3 != null ? user3.FirstName + " " + user3.LastName : "",
                transferItems = o.Items.Select(a => new InventoryItemsVm()
                {
                    itemId = a.SubAccountFromId,
                    itemCode = a.itemCode,
                    itemName = a.itemName,
                    ItemGroupName = a.ItemGroup.ItemGroupName,
                    note = a.note,
                    qty = a.qty,
                    maxQ = a.qty,
                    noteSecond = a.noteSecond,
                    price = a.price

                }).ToList()

            };

            #endregion
            return reurnOrder;
        }

    }
}