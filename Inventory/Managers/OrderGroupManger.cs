﻿using Inventory.DataLayer;
using Inventory.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Managers
{
    public class OrderGroupManger
    {
        private ApplicationDbContext db;
        public OrderGroupManger(ApplicationDbContext db)
        {
            this.db = db;
        }

        public SalesVm getOrderSalebyId(Int32 id, branchGroupSt company)
        {
            #region sales
            var c = db.Sales.Where(a => a.SalesId == id && a.IsCompany == company.isCompany && a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId)).SingleOrDefault();
            if (c == null)
            {
                return null;
            }
            var sales = new SalesVm()
            {
                salesId = c.SalesId,
                salesName = c.SalesName,
                ordersType = new ordersTypesale()
                {
                    id = c.ordersType.FirstOrDefault().OrdersTypeSalesId,
                    sale = c.ordersType.FirstOrDefault().sale,
                    returnd = c.ordersType.FirstOrDefault().returnd
                },
                payMethod = c.payMethod.Select(pm => new payMethod()
                {
                    payMethodId = pm.OrderGroupPayMethodSalesId,
                    accountId = pm.AccountCategoryId,
                    payName = pm.OrderGroupPayMethodName,
                    accountName = pm.AccountCategory.AccountCategoryName + " - " + pm.AccountCategory.Code,
                    PayMethodProperties = (from pr in pm.OrderPropertiesPaySales
                                           select new PayMethodPropertiesVm()
                                           {
                                               OrderPropertiesPayId = pr.OrderPropertiesPaySalesId,
                                               OrderPropertyTypeId = pr.OrderPropertyTypeId,
                                               OrderPropertiesPayName = pr.OrderPropertiesPaySalesName,
                                               IsRequired = pr.IsRequired,
                                               ToPrint = pr.ToPrint,
                                               OrderPropertyTypeName = pr.OrderPropertyType.OrderPropertyTypeName,
                                           }).ToList()
                }).ToList(),
                orderAccountsforSubAccounts = c.orderAccountsforSubAccounts.Select(pm => new OrderAccountsforSubAccountVm()
                {
                    OrderAccountsforSubAccountId = pm.OrderAccountsforSubAccountSalesId,
                    AccountId = pm.AccountId,
                    AccountName = pm.Account.AccountName + " - " + pm.Account.Code,
                    DebitWhenAdd = pm.DebitWhenAdd,

                }).ToList(),
                orderMainAccounts = c.orderMainAccounts.Select(om => new OrderMainAccountVm()
                {
                    OrderMainAccountId = om.OrderMainAccountId,

                    SalesId = om.SalesId,
                    inventoryAccount = new accountsforSetSalesOrderAccountsMainAccounts() { AccountId = om.InventoryAccountId, AccountName = om.InventoryAccount.AccountName + " - " + om.InventoryAccount.Code },

                    salesAccount = new accountsforSetSalesOrderAccountsMainAccounts() { AccountId = om.SalesAccountId, AccountName = om.SalesAccount.AccountName + " - " + om.SalesAccount.Code },
                    salesCostAccount = new accountsforSetSalesOrderAccountsMainAccounts() { AccountId = om.SalesCostAccountId, AccountName = om.SalesCostAccount.AccountName + " - " + om.SalesCostAccount.Code },
                    salesreturnAccount = new accountsforSetSalesOrderAccountsMainAccounts() { AccountId = om.SalesReturnAccountId, AccountName = om.SalesReturnAccount.AccountName + " - " + om.SalesReturnAccount.Code }

                }).ToList(),

                orderOtherTotalAccounts = c.orderOtherTotalAccounts.Select(ot => new OrderOtherTotalAccountVm()
                {
                    OrderOtherTotalAccountId = ot.OrderOtherTotalAccountSalesId,
                    AccountId = ot.AccountId,
                    AccountName = ot.Account.AccountName + " - " + ot.Account.Code,
                    DebitWhenAdd = ot.DebitWhenAdd,

                }).ToList(),

            };
            #endregion
            return sales;
        }

        public PurchasesVm getOrderPurbyId(Int32 id, branchGroupSt company)
        {
          
            #region pur
            var c = db.Purchases.Where(a => a.PurchasesId == id && a.IsCompany == company.isCompany && a.CompanyId == company.companyId && a.BranchId == (company.isCompany ? (int?)null : company.oId)).SingleOrDefault();
            if (c == null)
            {
                return null;
            }
            var pur = new PurchasesVm()
            {
                purId = c.PurchasesId,
                purName = c.PurchasesName,
                ordersType = new ordersTypepurchaes()
                {
                    id = c.ordersType.FirstOrDefault().OrdersTypePurchasesId,
                    buy = c.ordersType.FirstOrDefault().sale,
                    returnd = c.ordersType.FirstOrDefault().returnd
                },
                payMethod = c.payMethod.Select(pm => new payMethod()
                {
                    payMethodId = pm.OrderGroupPayMethodPurchasesId,
                    accountId = pm.AccountCategoryId,
                    payName = pm.OrderGroupPayMethodPurchasesName,
                    accountName = pm.AccountCategory.AccountCategoryName + " - " + pm.AccountCategory.Code,
                    PayMethodProperties = (from pr in pm.OrderPropertiesPayPurchases
                                           select new PayMethodPropertiesVm()
                                           {
                                               OrderPropertiesPayId = pr.OrderPropertiesPayPurchasesId,
                                               OrderPropertyTypeId = pr.OrderPropertyTypeId,
                                               OrderPropertiesPayName = pr.OrderPropertiesPayPurchasesName,
                                               IsRequired = pr.IsRequired,
                                               ToPrint = pr.ToPrint,
                                               OrderPropertyTypeName = pr.OrderPropertyType.OrderPropertyTypeName,
                                           }).ToList()
                }).ToList(),
                orderAccountsforSubAccounts = c.orderAccountsforSubAccounts.Select(pm => new OrderAccountsforSubAccountVm()
                {
                    OrderAccountsforSubAccountId = pm.OrderAccountsforSubAccountPurchasesId,
                    AccountId = pm.AccountId,
                    AccountName = pm.Account.AccountName + " - " + pm.Account.Code,
                    DebitWhenAdd = pm.DebitWhenAdd,

                }).ToList(),

                orderOtherTotalAccounts = c.orderOtherTotalAccounts.Select(ot => new OrderOtherTotalAccountVm()
                {
                    OrderOtherTotalAccountId = ot.OrderOtherTotalAccountPurchasesId,
                    AccountId = ot.AccountId,
                    AccountName = ot.Account.AccountName + " - " + ot.Account.Code,
                    DebitWhenAdd = ot.DebitWhenAdd


                }).ToList(),

            };
            #endregion
            return pur;
        }

        public OrderGroupVm getOrderbyId(Int32 id)
        {
            

            var group = (from c in db.OrderGroup
                         where c.OrderGroupId == id
                         select new OrderGroupVm()
                         {
                             OrderGroupId = c.OrderGroupId,
                             OrderGroupName = c.OrderGroupName,

                             CompanyBranchId = c.CompanyBranchId,
                             IsCompany = c.IsCompany,
                             sales = new SalesVm()
                             {
                                 id = c.SalesId,
                                 ordersType = new ordersTypesale()
                                 {
                                     id = c.Sales.ordersType.FirstOrDefault().OrdersTypeSalesId,
                                     sale = c.Sales.ordersType.FirstOrDefault().sale,
                                     returnd = c.Sales.ordersType.FirstOrDefault().returnd
                                 },
                                 payMethod = (from pm in db.OrderGroupPayMethodSales
                                              where pm.SalesId == c.SalesId
                                              select new payMethod()
                                              {
                                                  payMethodId = pm.OrderGroupPayMethodSalesId,
                                                  accountId = pm.AccountCategoryId,
                                                  payName = pm.OrderGroupPayMethodName,
                                                  accountName = pm.AccountCategory.AccountCategoryName,
                                                  PayMethodProperties = (from pr in pm.OrderPropertiesPaySales
                                                                         select new PayMethodPropertiesVm()
                                                                         {
                                                                             OrderPropertiesPayId = pr.OrderPropertiesPaySalesId,
                                                                             OrderPropertyTypeId = pr.OrderPropertyTypeId,
                                                                             OrderPropertiesPayName = pr.OrderPropertiesPaySalesName,
                                                                             IsRequired = pr.IsRequired,
                                                                             ToPrint = pr.ToPrint,
                                                                             OrderPropertyTypeName = pr.OrderPropertyType.OrderPropertyTypeName,
                                                                         }).ToList()
                                              }).ToList(),
                                 orderAccountsforSubAccounts = (from pm in db.OrderAccountsforSubAccountSales
                                                                where pm.SalesId == c.SalesId
                                                                select new OrderAccountsforSubAccountVm()
                                                                {
                                                                    OrderAccountsforSubAccountId = pm.OrderAccountsforSubAccountSalesId,
                                                                    AccountId = pm.AccountId,
                                                                    AccountName = pm.Account.AccountName,
                                                                    DebitWhenAdd = pm.DebitWhenAdd,
                                                                    typeWhenAdd = pm.DebitWhenAdd == true ? "مدين" : "دائن",
                                                                    salepurId = pm.SalesId,
                                                                    typename = "sales",
                                                                    DebitforPlus = pm.DebitforPlus,
                                                                    IsNewOrder = pm.IsNewOrder,
                                                                    CostEffect = pm.CostEffect

                                                                }).ToList(),
                                 orderMainAccounts = (from om in db.OrderMainAccounts
                                                      where om.SalesId == c.SalesId
                                                      select new OrderMainAccountVm()
                                                      {
                                                          OrderMainAccountId = om.OrderMainAccountId,

                                                          SalesId = om.SalesId,
                                                          inventoryAccount = new accountsforSetSalesOrderAccountsMainAccounts() { AccountId = om.InventoryAccountId, AccountName = om.InventoryAccount.AccountName },

                                                          salesAccount = new accountsforSetSalesOrderAccountsMainAccounts() { AccountId = om.SalesAccountId, AccountName = om.SalesAccount.AccountName },
                                                          salesCostAccount = new accountsforSetSalesOrderAccountsMainAccounts() { AccountId = om.SalesCostAccountId, AccountName = om.SalesCostAccount.AccountName },
                                                          salesreturnAccount = new accountsforSetSalesOrderAccountsMainAccounts() { AccountId = om.SalesReturnAccountId, AccountName = om.SalesReturnAccount.AccountName }

                                                      }).ToList(),

                                 orderOtherTotalAccounts = (from ot in db.OrderOtherTotalAccountSales
                                                            where ot.SalesId == c.SalesId
                                                            select new OrderOtherTotalAccountVm()
                                                            {
                                                                OrderOtherTotalAccountId = ot.OrderOtherTotalAccountSalesId,
                                                                AccountId = ot.AccountId,
                                                                AccountName = ot.Account.AccountName,
                                                                DebitWhenAdd = ot.DebitWhenAdd,
                                                                typeWhenAdd = ot.DebitWhenAdd == true ? "مدين" : "دائن",
                                                                salepurId = c.SalesId,
                                                                typename = "sales",
                                                                DebitforPlus = ot.DebitforPlus,
                                                                IsNewOrder = ot.IsNewOrder,
                                                                CostEffect = ot.CostEffect
                                                            }).ToList(),

                             },
                             purchases = new PurchasesVm()
                             {
                                 id = c.PurchasesId,
                                 ordersType = new ordersTypepurchaes()
                                 {
                                     id = c.Purchases.ordersType.FirstOrDefault().OrdersTypePurchasesId,
                                     buy = c.Purchases.ordersType.FirstOrDefault().sale,
                                     returnd = c.Purchases.ordersType.FirstOrDefault().returnd
                                 },
                                 payMethod = (from pm in db.OrderGroupPayMethodPurchases
                                              where pm.PurchasesId == c.PurchasesId
                                              select new payMethod()
                                              {
                                                  payMethodId = pm.OrderGroupPayMethodPurchasesId,
                                                  accountId = pm.AccountCategoryId,
                                                  payName = pm.OrderGroupPayMethodPurchasesName,
                                                  accountName = pm.AccountCategory.AccountCategoryName,
                                                  PayMethodProperties = (from pr in pm.OrderPropertiesPayPurchases
                                                                         select new PayMethodPropertiesVm()
                                                                         {
                                                                             OrderPropertiesPayId = pr.OrderPropertiesPayPurchasesId,
                                                                             OrderPropertyTypeId = pr.OrderPropertyTypeId,
                                                                             OrderPropertiesPayName = pr.OrderPropertiesPayPurchasesName,
                                                                             IsRequired = pr.IsRequired,
                                                                             ToPrint = pr.ToPrint,
                                                                             OrderPropertyTypeName = pr.OrderPropertyType.OrderPropertyTypeName,
                                                                         }).ToList()
                                              }).ToList(),
                                 orderAccountsforSubAccounts = (from pm in db.OrderAccountsforSubAccountPurchases
                                                                where pm.PurchasesId == c.PurchasesId
                                                                select new OrderAccountsforSubAccountVm()
                                                                {
                                                                    OrderAccountsforSubAccountId = pm.OrderAccountsforSubAccountPurchasesId,
                                                                    AccountId = pm.AccountId,
                                                                    AccountName = pm.Account.AccountName,
                                                                    DebitWhenAdd = pm.DebitWhenAdd,
                                                                    typeWhenAdd = pm.DebitWhenAdd == true ? "مدين" : "دائن",
                                                                    salepurId = pm.PurchasesId,
                                                                    typename = "purchases",
                                                                    DebitforPlus = pm.DebitforPlus,
                                                                    IsNewOrder = pm.IsNewOrder,
                                                                    CostEffect = pm.CostEffect
                                                                }).ToList(),

                                 orderOtherTotalAccounts = (from ot in db.OrderOtherTotalAccountPurchases
                                                            where ot.PurchasesId == c.PurchasesId
                                                            select new OrderOtherTotalAccountVm()
                                                            {
                                                                OrderOtherTotalAccountId = ot.OrderOtherTotalAccountPurchasesId,
                                                                AccountId = ot.AccountId,
                                                                AccountName = ot.Account.AccountName,
                                                                DebitWhenAdd = ot.DebitWhenAdd,
                                                                typeWhenAdd = ot.DebitWhenAdd == true ? "مدين" : "دائن",
                                                                salepurId = c.PurchasesId,
                                                                typename = "purchases",
                                                                DebitforPlus = ot.DebitforPlus,
                                                                IsNewOrder = ot.IsNewOrder,
                                                                CostEffect = ot.CostEffect

                                                            }).ToList(),

                             }
                         }).SingleOrDefault();

            return group;
        }

        public orderGroupTobill getOrderToBillByType(string typeName, Int32 orderGroupId, Int32 inventoryId, branchGroupSt company)
        {
         
            var orderToreturn = new orderGroupTobill();

            if (typeName == "مبيعات")
            {
                #region مبيعات
                var ordergroup = db.OrderGroup.SingleOrDefault(o => o.OrderGroupId == orderGroupId && o.IsCompany == company.isCompany && o.CompanyBranchId == company.oId);
                if (ordergroup == null)
                {
                    return null;
                }

                var billN = (from o in ordergroup.Sales.ordersType
                             select o).SingleOrDefault();
                var OrderbillName = new List<orderBillNames>()
                        {
                            new orderBillNames {billName = billN.sale , typeId = billN.OrdersTypeSalesId , typeName = "sale" }

                        };
                orderToreturn.billNames = OrderbillName;

                orderToreturn.TitleAccounts = db.Accounts.Where(t => t.AccountCategory.AccountsDataTypes.aliasName == "cust"
                                             && t.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                                             && t.BranchId == (company.isCompany ? (int?)null : company.oId))
                                             .Select(a => new tilteAccounts()
                                             {
                                                 AccountId = a.AccountId,
                                                 AccountName = a.AccountName,
                                                 showInfo = true
                                             }).ToList();

                var billItems = getFixedBillItems();

                var getItems = (from i in ordergroup.Sales.orderAccountsforSubAccounts
                                select new BillItems
                                {
                                    itemPropertyId = i.AccountId,
                                    itemPropertyName = i.Account.AccountName,
                                    type = "account",
                                    itemPropertyValues = new List<itemPropertyValuesbill>() {
                                            new itemPropertyValuesbill() {itemPropertyValueId = i.AccountId , itemPropertyId = 1 ,itemPropertyValueName = "0" , type = i.Account.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? "debit" : "crdit" }
                                        }
                                }).ToList();

                billItems.AddRange(getItems);
                orderToreturn.BillItems = billItems;

                orderToreturn.billitemsQty = getItemsInfoForBills(inventoryId, company);

                //billOtherAccounts
                orderToreturn.billOtherAccounts = (from o in ordergroup.Sales.orderOtherTotalAccounts
                                                   select new billOtherAccounts()
                                                   {
                                                       accountId = o.AccountId,
                                                       accountName = o.Account.AccountName,
                                                       type = o.Account.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? "debit" : "crdit"
                                                   }).ToList();

                //billPaymethods
                orderToreturn.billPaymethods = (from p in ordergroup.Sales.payMethod
                                                select new billPaymethods()
                                                {
                                                    payId = p.OrderGroupPayMethodSalesId,
                                                    payName = p.OrderGroupPayMethodName,
                                                    accounts = (from c in p.AccountCategory.Accounts
                                                                where c.BranchId == (company.isCompany ? (int?)null : company.oId) && c.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                                                                select new billMainAccount()
                                                                {
                                                                    AccountId = c.AccountId,
                                                                    AccountName = c.AccountName
                                                                }).ToList(),
                                                    billPaymethodProperties = (from pr in p.OrderPropertiesPaySales
                                                                               select new billPaymethodProperties()
                                                                               {
                                                                                   Id = pr.OrderPropertiesPaySalesId,
                                                                                   isRequied = pr.IsRequired,
                                                                                   toPrint = pr.ToPrint,
                                                                                   Name = pr.OrderPropertiesPaySalesName,
                                                                                   typeName = pr.OrderPropertyType.OrderPropertyTypeName,

                                                                               }).ToList()
                                                }).ToList();


                #endregion

            }
            if (typeName == "salesReturn")
            {
                #region رد مبيعات
                var ordergroup = db.OrderGroup.SingleOrDefault(o => o.OrderGroupId == orderGroupId && o.IsCompany == company.isCompany && o.CompanyBranchId == company.oId);
                if (ordergroup == null)
                {
                    return null;
                }

                var billN = (from o in ordergroup.Sales.ordersType
                             select o).SingleOrDefault();
                var OrderbillName = new List<orderBillNames>()
                        {
                            new orderBillNames {billName = billN.returnd , typeId = billN.OrdersTypeSalesId , typeName = "returnd" },

                        };
                orderToreturn.billNames = OrderbillName;

                orderToreturn.TitleAccounts = db.Accounts.Where(t => t.AccountCategory.AccountsDataTypes.aliasName == "cust"
                                             && t.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                                             && t.BranchId == (company.isCompany ? (int?)null : company.oId))
                                             .Select(a => new tilteAccounts()
                                             {
                                                 AccountId = a.AccountId,
                                                 AccountName = a.AccountName,
                                                 showInfo = true
                                             }).ToList();

                var billItems = getFixedBillItems();

                var getItems = (from i in ordergroup.Sales.orderAccountsforSubAccounts
                                select new BillItems
                                {
                                    itemPropertyId = i.AccountId,
                                    itemPropertyName = i.Account.AccountName,
                                    type = "account",
                                    itemPropertyValues = new List<itemPropertyValuesbill>() {
                                            new itemPropertyValuesbill() {itemPropertyValueId = i.AccountId , itemPropertyId = 1 ,itemPropertyValueName = "0" , type = i.Account.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? "debit" : "crdit" }
                                        }
                                }).ToList();

                billItems.AddRange(getItems);
                orderToreturn.BillItems = billItems;

                orderToreturn.billitemsQty = getItemsInfoForBills(inventoryId, company);

                //billOtherAccounts
                orderToreturn.billOtherAccounts = (from o in ordergroup.Sales.orderOtherTotalAccounts
                                                   select new billOtherAccounts()
                                                   {
                                                       accountId = o.AccountId,
                                                       accountName = o.Account.AccountName,
                                                       type = o.Account.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? "debit" : "crdit"
                                                   }).ToList();

                //billPaymethods
                orderToreturn.billPaymethods = (from p in ordergroup.Sales.payMethod
                                                select new billPaymethods()
                                                {
                                                    payId = p.OrderGroupPayMethodSalesId,
                                                    payName = p.OrderGroupPayMethodName,
                                                    accounts = (from c in p.AccountCategory.Accounts
                                                                where c.BranchId == (company.isCompany ? (int?)null : company.oId) && c.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                                                                select new billMainAccount()
                                                                {
                                                                    AccountId = c.AccountId,
                                                                    AccountName = c.AccountName
                                                                }).ToList(),
                                                    billPaymethodProperties = (from pr in p.OrderPropertiesPaySales
                                                                               select new billPaymethodProperties()
                                                                               {
                                                                                   Id = pr.OrderPropertiesPaySalesId,
                                                                                   isRequied = pr.IsRequired,
                                                                                   toPrint = pr.ToPrint,
                                                                                   Name = pr.OrderPropertiesPaySalesName,
                                                                                   typeName = pr.OrderPropertyType.OrderPropertyTypeName,

                                                                               }).ToList()
                                                }).ToList();


                #endregion
            }

            if (typeName == "مشتريات")
            {
                #region مشتريات
                var ordergroup = db.OrderGroup.SingleOrDefault(o => o.OrderGroupId == orderGroupId && o.IsCompany == company.isCompany && o.CompanyBranchId == company.oId);
                if (ordergroup == null)
                {
                    return null;
                }
                var billN = (from o in ordergroup.Purchases.ordersType
                             select o).SingleOrDefault();
                var OrderbillName = new List<orderBillNames>()
                        {
                            new orderBillNames {billName = billN.sale , typeId = billN.OrdersTypePurchasesId , typeName = "buy" }

                        };
                orderToreturn.billNames = OrderbillName;

                orderToreturn.TitleAccounts = db.Accounts.Where(t => t.AccountCategory.AccountsDataTypes.aliasName == "pur"
                                        && t.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                                        && t.BranchId == (company.isCompany ? (int?)null : company.oId))
                                         .Select(a => new tilteAccounts()
                                         {
                                             AccountId = a.AccountId,
                                             AccountName = a.AccountName,
                                             showInfo = true
                                         }).ToList();

                var billItems = getFixedBillItems();

                var getItems = (from i in ordergroup.Purchases.orderAccountsforSubAccounts
                                select new BillItems
                                {
                                    itemPropertyId = i.AccountId,
                                    itemPropertyName = i.Account.AccountName,
                                    type = "account",
                                    itemPropertyValues = new List<itemPropertyValuesbill>() {
                                            new itemPropertyValuesbill() {itemPropertyValueId = i.AccountId , itemPropertyId = 1 ,itemPropertyValueName = "0" , type = i.Account.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? "debit" : "crdit"}
                                        }
                                }).ToList();

                billItems.AddRange(getItems);

                orderToreturn.BillItems = billItems;

                //billitemsQty
                orderToreturn.billitemsQty = getItemsInfoForBills(inventoryId, company);


                //billOtherAccounts
                orderToreturn.billOtherAccounts = (from o in ordergroup.Purchases.orderOtherTotalAccounts
                                                   select new billOtherAccounts()
                                                   {
                                                       accountId = o.AccountId,
                                                       accountName = o.Account.AccountName,
                                                       type = o.Account.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? "debit" : "crdit"
                                                   }).ToList();

                //billPaymethods
                orderToreturn.billPaymethods = (from p in ordergroup.Purchases.payMethod
                                                select new billPaymethods()
                                                {
                                                    payId = p.OrderGroupPayMethodPurchasesId,
                                                    payName = p.OrderGroupPayMethodPurchasesName,
                                                    accounts = (from c in p.AccountCategory.Accounts
                                                                where c.BranchId == (company.isCompany ? (int?)null : company.oId) && c.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                                                                select new billMainAccount()
                                                                {
                                                                    AccountId = c.AccountId,
                                                                    AccountName = c.AccountName
                                                                }).ToList(),
                                                    billPaymethodProperties = (from pr in p.OrderPropertiesPayPurchases
                                                                               select new billPaymethodProperties()
                                                                               {
                                                                                   Id = pr.OrderPropertiesPayPurchasesId,
                                                                                   isRequied = pr.IsRequired,
                                                                                   toPrint = pr.ToPrint,
                                                                                   Name = pr.OrderPropertiesPayPurchasesName,
                                                                                   typeName = pr.OrderPropertyType.OrderPropertyTypeName,

                                                                               }).ToList()
                                                }).ToList();
                #endregion
            }
            if (typeName == "purReturns")
            {
                #region رد مشتريات
                var ordergroup = db.OrderGroup.SingleOrDefault(o => o.OrderGroupId == orderGroupId && o.IsCompany == company.isCompany && o.CompanyBranchId == company.oId);
                if (ordergroup == null)
                {
                    return null;
                }
                var billN = (from o in ordergroup.Purchases.ordersType
                             select o).SingleOrDefault();
                var OrderbillName = new List<orderBillNames>()
                            {
                                new orderBillNames {billName = billN.returnd , typeId = billN.OrdersTypePurchasesId , typeName = "returnd" },

                            };
                orderToreturn.billNames = OrderbillName;

                orderToreturn.TitleAccounts = db.Accounts.Where(t => t.AccountCategory.AccountsDataTypes.aliasName == "pur"
                                        && t.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                                        && t.BranchId == (company.isCompany ? (int?)null : company.oId))
                                         .Select(a => new tilteAccounts()
                                         {
                                             AccountId = a.AccountId,
                                             AccountName = a.AccountName,
                                             showInfo = true
                                         }).ToList();

                var billItems = getFixedBillItems();

                var getItems = (from i in ordergroup.Purchases.orderAccountsforSubAccounts
                                select new BillItems
                                {
                                    itemPropertyId = i.AccountId,
                                    itemPropertyName = i.Account.AccountName,
                                    type = "account",
                                    itemPropertyValues = new List<itemPropertyValuesbill>() {
                                                new itemPropertyValuesbill() {itemPropertyValueId = i.AccountId , itemPropertyId = 1 ,itemPropertyValueName = "0" , type = i.Account.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? "debit" : "crdit"}
                                            }
                                }).ToList();

                billItems.AddRange(getItems);

                orderToreturn.BillItems = billItems;

                //billitemsQty
                orderToreturn.billitemsQty = getItemsInfoForBills(inventoryId, company);


                //billOtherAccounts
                orderToreturn.billOtherAccounts = (from o in ordergroup.Purchases.orderOtherTotalAccounts
                                                   select new billOtherAccounts()
                                                   {
                                                       accountId = o.AccountId,
                                                       accountName = o.Account.AccountName,
                                                       type = o.Account.AccountCategory.BalanceSheetType.BalanceSheet.AccountType.AccountTypeName == "مدين" ? "debit" : "crdit"
                                                   }).ToList();

                //billPaymethods
                orderToreturn.billPaymethods = (from p in ordergroup.Purchases.payMethod
                                                select new billPaymethods()
                                                {
                                                    payId = p.OrderGroupPayMethodPurchasesId,
                                                    payName = p.OrderGroupPayMethodPurchasesName,
                                                    accounts = (from c in p.AccountCategory.Accounts
                                                                where c.BranchId == (company.isCompany ? (int?)null : company.oId) && c.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
                                                                select new billMainAccount()
                                                                {
                                                                    AccountId = c.AccountId,
                                                                    AccountName = c.AccountName
                                                                }).ToList(),
                                                    billPaymethodProperties = (from pr in p.OrderPropertiesPayPurchases
                                                                               select new billPaymethodProperties()
                                                                               {
                                                                                   Id = pr.OrderPropertiesPayPurchasesId,
                                                                                   isRequied = pr.IsRequired,
                                                                                   toPrint = pr.ToPrint,
                                                                                   Name = pr.OrderPropertiesPayPurchasesName,
                                                                                   typeName = pr.OrderPropertyType.OrderPropertyTypeName,

                                                                               }).ToList()
                                                }).ToList();
                #endregion
            }

            return orderToreturn;
        }

        public List<billitemsQty> getItemsInfoForBills(Int32 inventoryId, branchGroupSt company, bool addCostPrice = false)
        {
         
            var items = (from a in db.SubAccounts
                         where a.AccountId == inventoryId
               && a.Account.AccountCategory.BalanceSheetType.BalanceSheet.CompanyId == company.companyId
               && a.Account.BranchId == (company.isCompany ? (int?)null : company.oId)
               && a.Account.AccountCategory.AccountsDataTypes.aliasName == "inventory"
                         select new billitemsQty()
                         {
                             itemPropertyValueId = a.SubAccountId,
                             itemPropertyValueName = a.SubAccountName,
                             Code = a.ItemInfo.code,
                             QtyInStock = (from q in a.SubAccountMovements where q.SubAccountMovementDate == (from d in a.SubAccountMovements select d.SubAccountMovementDate).Max() select q.ItemStockQuantity).DefaultIfEmpty(0).FirstOrDefault(),
                             GroupName = a.ItemInfo.ItemGroup.ItemGroupName,
                             price = a.ItemInfo.price.ToString(),
                             costPrice = addCostPrice ? (from q in a.SubAccountMovements where q.SubAccountMovementDate == (from d in a.SubAccountMovements select d.SubAccountMovementDate).Max() select q.ItemStockPrice).DefaultIfEmpty(0).FirstOrDefault() : 0,
                             UnitName = a.ItemInfo.Unit.UnitName,
                             UntitType = a.ItemInfo.Unit.UnitType.UnitTypeName
                         }).ToList();



            if (items == null || items.Count() == 0)
            {
                return null;
            }
            return items;
        }
       

        public OrderGroupforBillVm getOrderforbillbyId(Int32 id)
        {
            var group = (from c in db.OrderGroup
                         where c.OrderGroupId == id
                         select new OrderGroupforBillVm()
                         {
                             OrderGroupId = c.OrderGroupId,
                             OrderGroupName = c.OrderGroupName,
                             CompanyBranchId = c.CompanyBranchId,
                             IsCompany = c.IsCompany,
                             type = new List<OrderGroupTypeForBill>()
                             {
                                 new OrderGroupTypeForBill {typeId = 1 , TypeName = "مبيعات"  },
                                 new OrderGroupTypeForBill {typeId = 2 , TypeName = "مشتريات"},
                                 new OrderGroupTypeForBill {typeId = 3 , TypeName = "تحويلات"}

                             },
                             orderMainAccounts = (from i in c.Sales.orderMainAccounts
                                                  select new orderInventotyAccounts()
                                                  {
                                                      accountId = i.InventoryAccountId,
                                                      accountName = i.InventoryAccount.AccountName
                                                  }).ToList()

                         }).SingleOrDefault();

            return group;
        }

        public List<BillItems> getFixedBillItems()
        {
            var billItems = new List<BillItems>() {

              new BillItems()
                {

                    itemPropertyId = 1,
                    itemPropertyName = "الكود",
                    type = "item",
                    itemPropertyValues = new List<itemPropertyValuesbill>() { new itemPropertyValuesbill() { itemPropertyValueId = 0, itemPropertyId = 1, type = "code" } }
                } ,
                new BillItems()
                {

                    itemPropertyId = 2,
                    itemPropertyName = "الصنف",
                    type = "item",
                    itemPropertyValues = new List<itemPropertyValuesbill>() { new itemPropertyValuesbill() {itemPropertyValueId = 0  , itemPropertyId = 1 , type = "item" }}
                },
                new BillItems()
                {

                    itemPropertyId = 3,
                    itemPropertyName = "السعر",
                    type = "price",
                    itemPropertyValues = new List<itemPropertyValuesbill>() {  new itemPropertyValuesbill() {itemPropertyValueId = 0  , itemPropertyId = 1 , itemPropertyValueName = "0" , type = "price" }}
                },
               new BillItems()
                {

                    itemPropertyId = 4,
                    itemPropertyName = "الكمية",
                    type = "qty",
                    itemPropertyValues = new List<itemPropertyValuesbill>() { new itemPropertyValuesbill() {itemPropertyValueId = 0  , itemPropertyId = 1 ,itemPropertyValueName = "0" , type = "qty"} }
                }
            };

            return billItems;
        }
    }
}