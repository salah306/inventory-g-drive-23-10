﻿using Inventory.DataLayer;
using Inventory.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Managers
{
    public class EasyUserManager : HelperManager
    {
        private ApplicationDbContext db;

        public EasyUserManager()
        {
            db = new ApplicationDbContext();
        }
        public EasyUserManager(ApplicationDbContext db)
        {
            this.db = db;
        }

        public IQueryable<BranchesGroupVm> GetBranchesGroup(string uid, bool IsCompany)
        {
           
            var returndCompany = from s in db.settingRolesForUser
                                 where s.settingPropertiesforUser.settingProperties.name == "Company"
                                 && s.settingPropertiesforUser.ApplicationUserComapny.ApplicationUserId == uid && s.RefrenceType == "Company"
                                 join c in db.Companies on s.RefrenceId equals c.CompanyId
                                 select new BranchesGroupVm()
                                 {
                                     id = c.CompanyId + "C",
                                     name = c.CompanyName,
                                     companyName = "شركات",
                                     fullName = c.CompanyName,
                                     isCompany = true,
                                     companyId = c.CompanyId,
                                     realName = c.CompanyName,
                                     oId = c.CompanyId,
                                 };

            var returndBranch = from s in db.settingRolesForUser
                                where s.settingPropertiesforUser.settingProperties.name == "Branch"
                                && s.settingPropertiesforUser.ApplicationUserComapny.ApplicationUserId == uid && s.RefrenceType == "Branch"
                                join c in db.Branchs on s.RefrenceId equals c.BranchId
                                select new BranchesGroupVm()
                                {
                                    id = c.BranchId + "B",
                                    name = c.BranchName + " - " + c.Company.CompanyName,
                                    companyName = "فروع",
                                    fullName = c.BranchName + " - " + c.Company.CompanyName,
                                    isCompany = false,
                                    companyId = c.CompanyId,
                                    realName = c.BranchName,
                                    oId = c.BranchId
                                };
            returndCompany.ToList().AddRange(returndBranch);
            return returndCompany;

        }
    }
}