﻿using Inventory.DataLayer;
using Inventory.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventory.Managers
{
    public class AccountDTO
    {
        public Int32 AccountId { get; set; }
        public string AccountName { get; set; }
        public string AccountAliasName { get; set; }
        public string Code { get; set; }
        //public decimal? TotalDebit
        //{
        //    get
        //    {
        //        var sumdebit = this.AccountsMovements.Select(a => a.Debit).DefaultIfEmpty(0).Sum();
        //        return sumdebit;
        //    }

        //}
        //public decimal? TotalCrdit
        //{
        //    get
        //    {
        //        var sumCrdit = this.AccountsMovements.Select(a => a.Crdit).DefaultIfEmpty(0).Sum();
        //        return sumCrdit;
        //    }
        //}
        public Int32 AccountCategoryId { get; set; }
        public virtual AccountCategory AccountCategory { get; set; }


        public virtual List<AccountMovement> AccountsMovements { get; set; }
        public Int32? CustomAccountCategoryId { get; set; }
        //public virtual CustomAccountCategory CustomAccountCategory { get; set; }

    
        public byte[] RowVersion { get; set; }
        public virtual ApplicationUser CurrentWorker { get; set; }

        public string CurrentWorkerId { get; set; }
        public Int32? BranchId { get; set; }
        public Branch Branch { get; set; }
        public DateTime CreatedDate { get; set; }
        public virtual List<SubAccount> SubAccounts { get; set; }
        public bool Activated { get; set; }

        public virtual List<AccountsforItemGroup> AccountsforItemGroups { get; set; }
        public virtual List<AccountCategoryPropertiesValue> AccountCategoryPropertiesValues { get; set; }


    }

    //public class SubAccountDTO 
    //{
    //    SubAccount subAccount;
    //    public SubAccountDTO()
    //    {

    //    }
    //    public decimal GetLastQtyIn()
    //    {
    //        var ddd = this.SubAccountMovements.Count();

    //        return this.SubAccountMovements.SingleOrDefault(i => i.SubAccountMovementDate == (this.SubAccountMovements.Select(d => d.SubAccountMovementDate).Max())).ItemPricein;
    //    }
    //    public decimal GetLastQtyOut()
    //    {
    //        var ddd = this.SubAccountMovements.Count();

    //        return this.SubAccountMovements.SingleOrDefault(i => i.SubAccountMovementDate == (this.SubAccountMovements.Select(d => d.SubAccountMovementDate).Max())).ItemPriceOut;
    //    }
    //    public decimal GetStockQty()
    //    {
    //        var ddd = this.SubAccountMovements.Count();

    //        return this.SubAccountMovements.SingleOrDefault(i => i.SubAccountMovementDate == (this.SubAccountMovements.Select(d => d.SubAccountMovementDate).Max())).ItemStockQuantity;

    //    }

    //    public decimal GetStockQtyprop
    //    {
    //        get
    //        {
    //            return this.SubAccountMovements.SingleOrDefault(i => i.SubAccountMovementDate == (this.SubAccountMovements.Select(d => d.SubAccountMovementDate).Max())).ItemStockQuantity;

    //        }
    //    }

    //    public decimal GetPreviousStockQty()
    //    {
    //        var ddd = this.SubAccountMovements.Count();
    //        var lastmove = this.SubAccountMovements.SingleOrDefault(i => i.SubAccountMovementDate == (this.SubAccountMovements.Select(d => d.SubAccountMovementDate).Max()));

    //        return this.GetStockQty() - (lastmove.QuantityIn - lastmove.QuantityOut);
    //    }

    //    public decimal GetStocKBalancePrice()
    //    {
    //        var lastmove = this.SubAccountMovements.SingleOrDefault(i => i.SubAccountMovementDate == (this.SubAccountMovements.Select(d => d.SubAccountMovementDate).Max()));

    //        return this.SubAccountMovements.SingleOrDefault(i => i.SubAccountMovementDate == (this.SubAccountMovements.Select(d => d.SubAccountMovementDate).Max())).ItemTotalStockPrice;
    //    }
    //    public decimal GetPreviousStocBalancePrice()
    //    {
    //        var lastmove = this.SubAccountMovements.SingleOrDefault(i => i.SubAccountMovementDate == (this.SubAccountMovements.Select(d => d.SubAccountMovementDate).Max()));
    //        return GetStocKBalancePrice() - ((lastmove.ItemPricein * lastmove.QuantityIn) - (lastmove.ItemPriceOut * lastmove.QuantityOut));
    //    }
    //    public decimal GetUnitCostPrice()
    //    {
    //        return this.GetStockQty() != 0 ? this.GetStocKBalancePrice() / this.GetStockQty() : 0;
    //    }

    //    public decimal GetPreviousUnitCostPrice()
    //    {
    //        return this.GetPreviousStockQty() != 0 ? this.GetPreviousStocBalancePrice() / this.GetPreviousStockQty() : 0;
    //    }

    //}
    //public class SubAccountManager
    //{
    //    private ApplicationDbContext db;
    //    public SubAccountManager(ApplicationDbContext db)
    //    {
    //        this.db = db;
    //    }



    //}
}