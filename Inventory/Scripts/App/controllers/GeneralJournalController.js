﻿
(function () {


    'use strict';
    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("GeneralJournalController", function ($scope, $timeout, _, AccountspersistenceService, $rootScope, settings, $filter,
                                                           AccountCategoriespersistenceService, BalanceSheetspersistenceService,
                                                           BalanceSheetTypespersistenceService, branchespersistenceService,
                                                           companiespersistenceService, EasyStoreUserspersistenceService, AccountOrderspersistenceService, Offline, $location, branchesremotePersistenceStrategy, $sce, authService, globalService, $q, $log, DTOptionsBuilder, DTColumnDefBuilder) {

        var bId;

        var start = moment().startOf('month');
        var end = moment().endOf('month');

        $scope.startDate = moment().startOf('month');
        $scope.endDate = moment().endOf('month');
        console.log('stsrt date scope', $scope.startDate);
        function cb(start, end) {
            $('#reportrange span').html('من ' + start.format('DD - MM -YYYY') + ' الي ' + end.format('DD - MM - YYYY'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'اليوم': [moment(), moment()],
                'امس': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'اخر 7 ايام': [moment().subtract(6, 'days'), moment()],
                'اخر 30 يوم': [moment().subtract(29, 'days'), moment()],
                'هذا الشهر': [moment().startOf('month'), moment().endOf('month')],
                'الشهر الماضي': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);


      
        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            console.log(picker.startDate.format('YYYY-MM-DD'));
            console.log(picker.endDate.format('YYYY-MM-DD'));
            $scope.startDate = picker.startDate.toDate();
            $scope.endDate = picker.endDate.toDate();
             $scope.getOrders(bId, $scope.itemsperpage, $scope.currentPage, $scope.startDate, $scope.endDate);
            console.log('end date', $scope.endDate);
        });
       
        $scope.accountsArray = [];
        
        $scope.branchesGroub = [];
       
        $scope.branchGroub = {
           id: 0,
           name: 'حدد الشركة - الفرع',
           fullName: 'حدد الشركة - الفرع'
       };

    

       $scope.loadCompanies = function () {
           $scope.branchesGroub = $rootScope.mainbranchesGroub;
       };

       //$scope.loadCompanies();

       $scope.totalItems = 0;
       $scope.currentPage = 0;
       $scope.itemsperpage = 1;

       $scope.branchGroub = $rootScope.mainbranchGroub;


    

       //$scope.$watch('branchGroub.id', function (newVal, oldVal) {
       //    if (newVal !== oldVal) {
       //        var debitorSelected = $filter('filter')($scope.branchesGroub, { id: $scope.branchGroub.id });
       //        $scope.branchGroub = debitorSelected.length ? debitorSelected[0] : null;
       //        console.log("branchGroub", $scope.branchGroub)
       //        if (debitorSelected.length) {
                   
       //            var id = debitorSelected[0].id;
       //            bId = parseInt(angular.copy(id));
       //            console.log('Id', id)
                  
       //            console.log("start datae" , $scope.startDate)
       //             $scope.getOrders(bId, $scope.itemsperpage, $scope.currentPage , $scope.startDate , $scope.endDate);
       //            getAccountsNames(bId);
       //        }
       //    }
       //});

       $scope.$watch('startDate', function (newVal, oldVal) {
           if (newVal !== oldVal) {
                $scope.getOrders(bId, $scope.itemsperpage, $scope.currentPage, $scope.startDate, $scope.endDate);
           }
       });

       $scope.$watch('endDate', function (newVal, oldVal) {
           if (newVal !== oldVal) {
                $scope.getOrders(bId, $scope.itemsperpage, $scope.currentPage, $scope.startDate, $scope.endDate);
           }
          
       });

       $scope.$watch('itemsperpage', function (newVal, oldVal) {

           var newvalint = parseInt(newVal)
           console.log('newval int', newvalint);
           if (isNaN(newvalint)) {
               $scope.itemsperpage = 1;
           }
           if (newvalint !== oldVal && newVal > 1) {
                $scope.getOrders(bId, parseInt($scope.itemsperpage), $scope.currentPage, $scope.startDate, $scope.endDate);
               $scope.itemsperpage = newvalint;
           }
           if (newvalint <= 0) {
               $scope.itemsperpage = 1;
           } else if (isNaN(newvalint)) {
               $scope.itemsperpage = 1;
           }
       });

  
     
       $scope.debitorSelected = {};
       $scope.crditorSelected = {};
       
       $scope.accountsArray = [];
       $scope.debitorSelected = { value: $scope.accountsArray[0] };

        //$scope.debitorSelectedItem = 'undefined';

        var getAccountsNames = function (id) {

            AccountspersistenceService.action.search(JSON.stringify({id: id , isCompany : $scope.branchGroub.isCompany})).then(
                              function (respone) {
                                  $scope.accountsArray = [];
                                  $scope.debitorSelected = {};
                                  console.log("names")
                                  console.log(respone)
                                 

                                  $scope.accountsArray = respone.data;
                                 // $scope.debitorSelected = { value: $scope.accountsArray[0] };
                                  console.log('item array', JSON.stringify(respone.data))
                                  return;

                              },
                              function (error) {
                                  console.log("error names");
                                  console.log(error);
                              });
        };

       
        
        $scope.getItemArr = function () {
            
            return $scope.accountsArray;
        }
      

      


        $scope.OrderTo = {};
        $scope.orders = [];


        $scope.search = "";
       
        $scope.itemsperpage = 5;

          $scope.getOrders = function (id, itemsperpage, currentPage, startDate, endDate) {
            //$http.get('/api/AccountOrders/GetAccountOrders/' + id + '/' + pageSize + '/' + pageNumber + '/' + startDate + '/' + endDate)

              AccountOrderspersistenceService.action.GetAccountOrders(JSON.stringify({ id: id, pageSize: itemsperpage, pageNumber: currentPage, startDate: startDate, endDate: endDate, search: $scope.search ,isCompany: $scope.branchGroub.isCompany})).then(
            function (orders) {
                console.log('order respone', orders)
                $scope.totalItems = orders.data.totalCount
                
                $scope.orders = orders.data.orders;
                console.log('orders', orders)
              
            },
            function (error) {
                $scope.error = error;
            });
          };

          $scope.searchOrder = function () {
              $scope.getOrders(bId, $scope.itemsperpage, $scope.currentPage, $scope.startDate, $scope.endDate);
          };

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
             $scope.getOrders(bId, $scope.itemsperpage, $scope.currentPage, $scope.startDate, $scope.endDate);
           
        };

        $scope.pageChanged = function () {
            $log.log('Page changed to: ' + $scope.currentPage);
             $scope.getOrders(bId, $scope.itemsperpage, $scope.currentPage  , $scope.startDate , $scope.endDate);
        };


        $scope.debitorAccounts = [{id:null, account: {}, amount: 0 }];
        $scope.crditorAccounts = [{id:null, account: {}, amount: 0 }];

        $scope.OrderTo = { orderId: null, orderNo: null, orderNote: '', branchId: null, orderDate: new Date(), debitorAccounts: $scope.debitorAccounts, crditorAccounts: $scope.crditorAccounts };
        $scope.orderModal = false;
        $scope.closeorderModal = function () {
            $scope.orderModal = false;
        };
        $scope.addOrder = function () {
            $scope.debitorAccounts = [];
            $scope.crditorAccounts = [];
            $scope.debitorAccounts = [{ id: null, account: {}, amount: 0 }];
            $scope.crditorAccounts = [{ id: null, account: {}, amount: 0 }];
            getAccountsNames(bId)
           
            $scope.OrderTo.orderId = 0;
            $scope.OrderTo = { orderId: 0, orderNo: $scope.orders.length + 1, orderNote: '', branchId: bId,companyId : bId ,orderDate: new Date(), debitorAccounts: $scope.debitorAccounts, crditorAccounts: $scope.crditorAccounts , isCompany:$scope.branchGroub.isCompany };
            $scope.orderModal = true;
        };

        $scope.editOrder = function (orderToEdit) {
            $scope.debitorAccounts = [];
            $scope.crditorAccounts = [];
            $scope.debitorAccounts =orderToEdit.debitorAccounts;
            $scope.crditorAccounts = orderToEdit.crditorAccounts;
            $scope.OrderTo.orderId = orderToEdit.orderId;
            $scope.OrderTo = { orderId: orderToEdit.orderId, orderNo: orderToEdit.orderNo, orderNote: orderToEdit.orderNote, branchId: bId, companyId: bId, orderDate: orderToEdit.orderDate, debitorAccounts: $scope.debitorAccounts, crditorAccounts: $scope.crditorAccounts, isCompany: $scope.branchGroub.isCompany };
           console.log("order TO Edit", $scope.OrderTo);
        };

       

        $scope.saveOrder = function () {
            if ($scope.OrderTo.orderId === 0) {
                // $scope.OrderTo.orderId = $scope.orderId;
                $scope.OrderTo.debitorAccounts = $scope.debitorAccounts;
                $scope.OrderTo.crditorAccounts = $scope.crditorAccounts;
                console.log('check debit' , $scope.debitorAccounts)
                AccountOrderspersistenceService.action.save(JSON.stringify($scope.OrderTo)).then(
                function (Companies) {
                    console.log('order respone', Companies)
                    $scope.orders.push($scope.OrderTo);
                    $scope.orderModal = false;
                },
                function (error) {
                    $scope.error = error;
                });
            }
            else {
                $scope.updateOrder();
            }
       

         
        };

        $scope.updateOrder = function () {
            AccountOrderspersistenceService.action.save(JSON.stringify($scope.OrderTo)).then(
            function (Companies) {
                console.log('order updated', Companies)
               // $scope.orders.push(Companies);
               // $scope.orders[$scope.orderId] = Companies;
                $scope.orders.filter(function (o) { return o.orderId == $scope.OrderTo.orderId })[0] = Companies.data;
                $scope.orders.filter(function (o) { return o.orderId == $scope.OrderTo.orderId })[0].orderNote = Companies.data.orderNote

            },
            function (error) {
                $scope.error = error;
                console.log('order updated error', error)
            });
            
        };

        $scope.showacc = function () {
            console.log('d account', $scope.debitorAccounts)
        }

        $scope.addNewDebitorAccounts = function (account) {
            var lastItem = $scope.debitorAccounts.length - 1;
            $scope.debitorAccounts.push({ id: null, account: { id:null, key: null, status: null, value: null }, amount: 0 });

            if (account.id != null) {
                console.log('hi you plur on', account);
                var checkacc = ($scope.accountsArray.indexOf(account), account);
                console.log('checkacc focus on', checkacc);

                if (!checkacc.status) {
                    console.log('account to true', $scope.accountsArray.filter(function (acc) { return acc.id == account.id }));
                    $scope.accountsArray.filter(function (acc) { return acc.id == account.id })[0].status = true;
                    console.log('now true', $scope.accountsArray.filter(function (acc) { return acc.id == account.id }));
                    //$scope.accountsArray.account;

                }
            }
        };


        $scope.addNewCrditorAccounts = function (account) {
            $scope.crditorAccounts.push({ id: null, account: { id: null, key: null, status: null, value: null }, amount: 0 });
            if (account.id != null) {
                console.log('hi you focus on', account);
                var checkacc = ($scope.accountsArray.indexOf(account), account);
                console.log('checkacc focus on', checkacc);

                if (checkacc.status) {
                    console.log('account to false', $scope.accountsArray.filter(function (acc) { return acc.id == account.id }));
                    $scope.accountsArray.filter(function (acc) { return acc.id == account.id })[0].status = false;
                    console.log('now false', $scope.accountsArray.filter(function (acc) { return acc.id == account.id }));
                    //$scope.accountsArray.account;

                }
            }

        };

        $scope.addacc = function (account) {
            console.log('acccount when click or focues' , account)
            if (account.id != null) {
            console.log('hi you focus on', account);
            var checkacc = ($scope.accountsArray.indexOf(account), account);
            console.log('checkacc focus on', checkacc);
            var debitoracc;
            console.log('debitorAccounts', $scope.debitorAccounts)
            for (debitoracc of  $scope.debitorAccounts) {
                if (debitoracc.account.id !=null) {
                    console.log('!debitoracc', debitoracc.account.id)
                    $scope.accountsArray.filter(function (acc) { return acc.id == debitoracc.account.id })[0].status = true;
                }
                
            };

            var crditoracc;
            console.log('debitorAccounts', $scope.crditorAccounts)
            for (crditoracc of  $scope.crditorAccounts) {
                if (crditoracc.account.id != null) {
                    console.log('!crditoracc', crditoracc.account.id)
                    $scope.accountsArray.filter(function (acc) { return acc.id == crditoracc.account.id })[0].status = true;
                }

            };

            if (checkacc.status) {
                
                console.log('account to false', $scope.accountsArray.filter(function (acc) { return acc.id == account.id }));
                $scope.accountsArray.filter(function (acc) { return acc.id == account.id })[0].status = false;
                console.log('now false', $scope.accountsArray.filter(function (acc) { return acc.id == account.id }));
                    //$scope.accountsArray.account;

                }
            }
        };

        $scope.removeacc = function (account) {
            if (account.id != null) {
                console.log('hi you plur on', account);
                var checkacc = ($scope.accountsArray.indexOf(account), account);
                console.log('checkacc focus on', checkacc);

                if (!checkacc.status) {
                    console.log('account to true', $scope.accountsArray.filter(function (acc) { return acc.id == account.id }));
                    $scope.accountsArray.filter(function (acc) { return acc.id == account.id })[0].status = true;
                    console.log('now true', $scope.accountsArray.filter(function (acc) { return acc.id == account.id }));
                    //$scope.accountsArray.account;

                }
            }
          
        };


        $scope.onOpenClose = function (i) {
            console.log('is open', i);
        };


        $scope.removeDebitorAccounts = function (account) {
            var lastItem = $scope.debitorAccounts.length - 1;
            if (lastItem > 0) $scope.debitorAccounts.splice($scope.debitorAccounts.indexOf(account));
                $scope.accountsArray.push(account);
        };




        $scope.removeCrditorAccounts = function (account) {
            var lastItem = $scope.crditorAccounts.length - 1;
            if (lastItem > 0) $scope.crditorAccounts.splice($scope.crditorAccounts.indexOf(account));
                $scope.accountsArray.push(account);

        };

        $scope.DebitorAccountsTotalamount = function () {
            var total = 0;
            var cho = $scope.debitorAccounts;
            for (var i in cho) {

                if (!isNaN(parseFloat(cho[i].amount)))
                    total += parseFloat(cho[i].amount);
            }

            return total
        };

        $scope.CrditorAccountsTotalamount = function () {
            var total = 0;
            var cho = $scope.crditorAccounts;
            for (var i in cho) {

                if (!isNaN(parseFloat(cho[i].amount)))
                    total += parseFloat(cho[i].amount);
            }

            return total
        };

        $scope.usedAccount = function (item) {

            var deb= 0;
            var acc = 0;
            var bo = 'undefined';
            
            var debitorf = $filter('filter')($scope.debitorAccounts, { account: item });
            console.log('ddddddebitorAccounts', debitorf);
                for (acc = 0; acc < $scope.debitorAccounts.length; acc++) {
                    console.log('$scope.debitorAccounts[acc].account.value', $scope.debitorAccounts[acc].account.value);
                    console.log('$item', item.value);
                    if ($scope.debitorAccounts[acc].account.value == item.value) {
                        console.log('hi the', $scope.debitorAccounts[acc]);
                        bo =  true;
                    }
                    else {
                        console.log('hi ', $scope.debitorAccounts);
                        bo = false;
                    }
                }
            
            console.log('hi the sss', bo);
            return bo;
           // return item.id == 6 ? true : false;
               

        };

        $scope.$watch('$root.mainbranchGroub.id', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal !== '0') {
                var findCompanybr = angular.copy($filter('filter')($rootScope.mainbranchesGroub, { id: newVal }, true));
                if (findCompanybr) {
                    $scope.branchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    $rootScope.mainbranchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    console.log('$rootScope.mainbranchGroub', $rootScope.mainbranchGroub);
                    if (findCompanybr.length) {

                        bId = parseInt(angular.copy(parseInt(findCompanybr[0].id)));

                        EasyStoreUserspersistenceService.action.branchGroupSt(JSON.stringify($scope.branchGroub)).then(function () { }, function () { });
                        $scope.getOrders(bId, $scope.itemsperpage, $scope.currentPage, $scope.startDate, $scope.endDate);
                        getAccountsNames(bId);
                    }
                }

            }
        });

        var checkbrGroup = function () {
            if ($scope.branchGroub && $scope.branchGroub.id != '0') {
                bId = parseInt(angular.copy(parseInt($scope.branchGroub.id)));

                $scope.getOrders(bId, $scope.itemsperpage, $scope.currentPage, $scope.startDate, $scope.endDate);
                getAccountsNames(bId);
            }
        }();

       

    });

}());


