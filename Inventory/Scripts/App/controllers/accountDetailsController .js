﻿(function () {

    'use strict';
    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("accountDetailsController", function ($scope, _,
        AccountDetailspersistenceService,
        AccountOrderspersistenceService,
        AccountMovementspersistenceService,
        Offline, $location, $routeParams) {

        var AccountDetailName = $routeParams.name;
        $scope.name = $routeParams.name;
        $scope.catname = $routeParams.catname;
        $scope.AccountMovementFrom = [];
        $scope.showList = false;
        $scope.AccountDetails = [];
        $scope.AccountDetailpropertyValues = [];
        $scope.PropertyValues = [];

        $scope.getAccountOrderProperties = [];
        var AccountOrderProperties = null;
        $scope.accountOrderProperties = function (i) {
            $scope.getAccountOrderProperties = i;
           
            AccountOrderProperties = i;
           $scope.AccountMovementFrom.accountOrderProperties = i;

        };

        var AccountMovementName = {
            "accountOrderPropertiesName": "",
            "accountName": "",
            "date": ""
        };

        $scope.AccountMovements = function getAccount(AccountMovementName) {
            AccountMovementspersistenceService.action.getById(AccountMovementName).then(
                function (AccountMovements) {
                    $scope.AccountMovements = AccountMovements;


                    $scope.showList = true;
                    $scope.showEmptyListMessage = (AccountMovements.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        }

        



        $scope.AccountMovementFrom = {
            

                'id': 'Order',
                'name': '',
                'mdate': function () {
                    return new Date()
                }(),
                'accountOrderProperties': AccountOrderProperties,
                'amount': '',
               
                'AccountMovementsTo': [{

                    'id': 'Order',
                    'name': '',
                    'mdate': function () {
                        return new Date()
                    }(),
                    'accountOrderProperties': AccountOrderProperties,
                    'amount': ''

                }]

        };
      

        $scope.addNewaccountMovementsTo = function () {
            var newItemNo = $scope.AccountMovementFrom.AccountMovementsTo.length + 1;
            console.log($scope.AccountMovementFrom)
            $scope.AccountMovementFrom.AccountMovementsTo.push({});
        };

        $scope.Totalamount = function () {
            var total = 0;
            var cho = $scope.AccountMovementFrom.AccountMovementsTo;
            for (var i in cho) {

                if (!isNaN(parseFloat(cho[i].amount)))
                total += parseFloat(cho[i].amount);
            }

            $scope.AccountMovementFrom.name = $scope.AccountDetails.name
            $scope.AccountMovementFrom.amount = total
            return total
        };
        

        $scope.removeAccountMovementsTo = function () {
            var lastItem = $scope.AccountMovementFrom.AccountMovementsTo.length - 1;
            if(lastItem > 0)
                $scope.AccountMovementFrom.AccountMovementsTo.splice(lastItem);
        };

        var getData = function () {

            AccountDetailspersistenceService.action.getById(AccountDetailName).then(
                function (AccountDetails) {
                    $scope.AccountDetails = AccountDetails;


                    $scope.showList = true;
                    $scope.showEmptyListMessage = (AccountDetails.length === 0);
                },
                function (error) {
                    $scope.error = error;
                });
        };

        var lazyGetData = _.debounce(getData, 2);

        Offline.on('confirmed-down', lazyGetData);
        Offline.on('confirmed-up', lazyGetData);

        lazyGetData();

        function getValueByKey(key, data) {
            var i, len = data.length;

            for (i = 0; i < len; i++) {
                if (data[i] && data[i].hasOwnProperty(key)) {
                    return data[i][key];
                }
            }

            return -1;
        }

        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;   //set the sortKey to the param passed
            $scope.reverse = !$scope.reverse; //if true make it false and vice versa
        }


        // $scope.catogeries = AccountDetailRepository.getcatogery();

        $('#loading').hide();


        // Save 

        $scope.showSuccessMessage = false;
        $scope.showFillOutFormMessage = false;
        $scope.isOnline = true;
        $scope.addAccountDetail = {};







        var hasAccountDetailToSave = function () {


            var hasValue = function (value) {

                if (typeof value === 'string') {
                    return value.length > 0;
                }

                return value > 0;
            };



            var returnValue =
                       hasValue($scope.addAccountDetail.name)
                    && hasValue($scope.addAccountDetail.price)
                    && hasValue($scope.addAccountDetail.catogery);

            return returnValue;
        };


 
       
        $scope.save = function () {
            if ($scope.addAccountDetail.id === null || $scope.addAccountDetail.id === undefined) {
                $scope.addAccountDetail.id = $scope.addAccountDetail.name;
            }

            var saveAccountDetail = hasAccountDetailToSave();


            var AccountDetail = $scope.addAccountDetail;


            //;

            AccountDetailspersistenceService.action.save(AccountDetail).then(
                function (result) {
                    $scope.showSuccessMessage = true;
                    $scope.showErrorMessage = false;
                    $scope.addAccountDetail = {};
                },
                function (error) {
                    $scope.showSuccessMessage = false;
                    $scope.showErrorMessage = true;
                });



        };

        $scope.Orders = function () {
            var Order = {};
            Order.id = $scope.AccountMovementFrom.id,
            Order.name = $scope.AccountMovementFrom.mdate,
            Order.date = $scope.AccountMovementFrom.mdate,
            Order.AccountOrderProperties = function(){
                var prop = {};
                prop.id = $scope.AccountMovementFrom.accountOrderProperties,
                prop.name = $scope.AccountMovementFrom.accountOrderProperties
                return prop;
            }(), 
            Order.AccountMovementFrom = function () {
                var accfr = {};
                accfr.id = $scope.AccountMovementFrom.name,
                accfr.name = $scope.AccountMovementFrom.name,
                accfr.mdate = $scope.AccountMovementFrom.mdate,
                accfr.Debit = function () {
                    if ($scope.AccountMovementFrom.accountOrderProperties == 'سحب') {
                        return 0;
                    }
                    if ($scope.AccountMovementFrom.accountOrderProperties == 'ايداع') {
                        return $scope.AccountMovementFrom.amount;
                    }
                }(),
               accfr.Crdit = function () {
                   if ($scope.AccountMovementFrom.accountOrderProperties == 'سحب') {
                       return $scope.AccountMovementFrom.amount;
                    }
                   if ($scope.AccountMovementFrom.accountOrderProperties == 'ايداع') {
                       return 0;
                    }
                }(),
                
                accfr.BranchName = 'فرع المعادي - ابراج المهندسين',
                accfr.AccountOrderId = '',
                accfr.AccountOrderPropertiesName = $scope.AccountMovementFrom.accountOrderProperties
                accfr.Note = ''
                return accfr;
            }(),

            Order.AccountMovementsTo = function () {
               
                var ListOrderAccountMovementsTo = [];
                for (var i = 0; i < $scope.AccountMovementFrom.AccountMovementsTo.length; i++) {
                    var acc = {};
                    acc.id = $scope.AccountMovementFrom.AccountMovementsTo[i].name,
                    acc.name = $scope.AccountMovementFrom.AccountMovementsTo[i].name,
                    acc.mdate = $scope.AccountMovementFrom.AccountMovementsTo[i].mdate,
                    acc.Crdit = function () {
                        if ($scope.AccountMovementFrom.accountOrderProperties == 'ايداع') {
                            return $scope.AccountMovementFrom.AccountMovementsTo[i].amount;
                        }
                        if ($scope.AccountMovementFrom.accountOrderProperties == 'سحب') {
                            return 0;
                        }
                    }(),
                    acc.Debit = function () {
                        if ($scope.AccountMovementFrom.accountOrderProperties == 'ايداع') {
                            return 0;
                        }
                        if ($scope.AccountMovementFrom.accountOrderProperties == 'سحب') {
                            return $scope.AccountMovementFrom.AccountMovementsTo[i].amount;
                        }
                    }(),
                    acc.BranchName = 'فرع المعادي - ابراج المهندسين',
                    acc.AccountOrderId = '',
                    acc.AccountOrderPropertiesName = $scope.AccountMovementFrom.accountOrderProperties
                    acc.Note = ''
                    ListOrderAccountMovementsTo.push(acc);
                }

               
             

                return ListOrderAccountMovementsTo;

            }()

            return Order;

        };


        $scope.saveOrder = function () {

      

            //if ($scope.Orders.id === null || $scope.Orders.id === undefined) {
            //    $scope.newOrder.id = $scope.newOrder.name;
            //}

           // var saveAccountOrder = hasAccountOrderToSave();


            var AccountOrder = $scope.Orders();

          
            console.log(AccountOrder);

            //;

            AccountOrderspersistenceService.action.save(AccountOrder).then(
                function (result) {
                    $scope.showSuccessMessage = true;
                    $scope.showErrorMessage = false;
                    $scope.addAccountDetail = {};
                },
                function (error) {
                    $scope.showSuccessMessage = false;
                    $scope.showErrorMessage = true;
                });

        };

        Offline.on('confirmed-down', function () {
            $scope.$apply(function () {
                $scope.isOnline = false;
            });
        });

        Offline.on('confirmed-up', function () {
            $scope.$apply(function () {
                $scope.isOnline = true;
            });
        });

        //Edit and update
        var AccountDetailToEdit = [];

        $scope.editingAccountDetail = {};


        $scope.modify = function (AccountDetail) {
            for (var i = 0, length = $scope.AccountDetails.length; i < length; i++) {
                $scope.editingAccountDetail[$scope.AccountDetails[i].id] = false;
            }
            AccountDetailToEdit = angular.copy(AccountDetail);
            $scope.addAccountDetail = AccountDetail;
            $scope.editingAccountDetail[AccountDetail.id] = true;

        };


        $scope.update = function (AccountDetail) {
            $scope.editingAccountDetail[AccountDetail.id] = false;
        };

        $scope.cancel = function (AccountDetail) {

            AccountDetail.name = AccountDetailToEdit.name;
            AccountDetail.price = AccountDetailToEdit.price;
            AccountDetail.catogery = AccountDetailToEdit.catogery;

            $scope.editingAccountDetail[AccountDetail.id] = false;

        };

        $scope.reset = function () {
            for (var i = 0, length = $scope.AccountDetails.length; i < length; i++) {
                $scope.editingAccountDetail[$scope.AccountDetails[i].id] = false;
            }
            $scope.addAccountDetail = {};
        };


        //delte
        var delteAccountDetail = {};
        $scope.AccountDetailToDelte = function (AccountDetail) {

            delteAccountDetail = AccountDetail;
        };


        $scope.delete = function () {


            AccountDetailspersistenceService.action.Delete(delteAccountDetail.id).then(
                function (result) {
                    $scope.AccountDetails.splice($scope.AccountDetails.indexOf(delteAccountDetail), 1);
                    delteAccountDetail = {};
                },
                function (error) {
                    $scope.error = error;
                });
        };


        AccountDetailspersistenceService.getById(AccountDetailName).then(
            function (AccountDetails) {
                $scope.AccountDetails = AccountDetails;
            },
            function (error) {
                $scope.error = error;
            });

    });

}());
