﻿
(function () {
  

    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("ItemController", function ($scope, $timeout, _, $localStorage, AccountspersistenceService, $rootScope, settings, $filter,
        AccountOrderspersistenceService, Offline, EasyStoreUserspersistenceService, $location, branchesremotePersistenceStrategy, $sce, globalService, $q, $log) {


        
        var bId;
        $scope.selectedinventories = [];
        $scope.branchesGroub = [];
        $scope.itemGroups = [];
        $scope.inventories =[];
        $scope.items =[];
        $scope.totalPages = 0;
        $scope.totalItems = 0;
        $scope.units = [];
        $scope.types = [];
        $scope.groupItems = [];
        $scope.newObjects = {};
        $scope.unitTypes = [];
        $scope.itemGroupsOrginal = [];
        $scope.showUnitWarning = false;
        $scope.showGroupWarning = false;
        $scope.showCancelGroupEdit = false;
        $scope.fromGetGrouingo = false;
        $scope.selectedGroup = { selected: {}};
        $scope.iteminfo = { pageNumber: 1, search: null, orderby: "qty", inventoryId: 0, groupId: 0, orderType: true, pageSize: 10 };
        $scope.edititem = {};
        $scope.edititemObj = {
            "itemId": 0,
            "itemCode": null,
            "itemName": null,
            "itemPrice": 0,
            "reorderPoint": "0",
            "groupId": 0,
            "properties": [
                {
                    "propertyNameId": 0,
                    "propertyName": null,
                    "type": {
                        "typeId": 0,
                        "typeName": null,
                        "isNew": true
                    },
                    "isNew": true,
                    "propertiesValues": {
                        "isNew": true,
                        "propertyId": 0,
                        "propertyValueId": 0,
                        "propertyValueName" :null
                    }
                },
                
            ],
            "isNew": true,
            "isToEdit": false
        };
        $scope.selectConfig = {
            requiredMin: 0
        };
        $scope.branchGroub = $rootScope.mainbranchGroub;

        $scope.$watch('$root.mainbranchGroub.id', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal !== '0') {
                var findCompanybr = angular.copy($filter('filter')($rootScope.mainbranchesGroub, { id: newVal }, true));
                if (findCompanybr) {
                    $scope.branchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    $rootScope.mainbranchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    console.log('$rootScope.mainbranchGroub', $rootScope.mainbranchGroub);
                    if (findCompanybr.length) {
                  
                        bId = parseInt(angular.copy(parseInt(findCompanybr[0].id)));

                        EasyStoreUserspersistenceService.action.branchGroupSt(JSON.stringify($scope.branchGroub)).then(function () { }, function () { });
                        $scope.loadItems();
                    }
                }

            }
        });

   

        $scope.loadItems = function () {
            EasyStoreUserspersistenceService.action.loaditemsInfoVM(JSON.stringify($scope.branchGroub)).then(
                function (result) {
                    $scope.itemGroups = result.data.itemGroups;
                    $scope.itemGroupsOrginal =angular.copy( result.data.itemGroups);
                    $scope.inventories = result.data.inventories;
                    $scope.inventoriesforMulti = angular.copy(result.data.inventories);
                    $scope.selectedGroup = { selected: angular.copy(result.data.itemGroups[0]) };
                    $scope.edititemObj.groupId = angular.copy($scope.selectedGroup.selected.groupId);
                    $scope.edititemObj.properties = angular.copy($scope.selectedGroup.selected.properties);
                    $scope.edititem = angular.copy($scope.edititemObj);
                    $scope.items = result.data.items;
                    $scope.totalItems = result.data.count
                    $scope.totalPages = result.data.totalPages;
                    $scope.iteminfo.pageSize = result.data.pageSize;
                    $scope.units = result.data.units;
                    $scope.types = result.data.types;
                    $scope.newObjects = result.data.newObjctes;
                    $scope.unitTypes = result.data.unitTypes;
                },
                function (error) {
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });
        };
        $scope.getItemInfo = function () {
            if (!$scope.iteminfo.inventoryId) {
                $scope.iteminfo.inventoryId = 0;
            }
            if (!$scope.iteminfo.groupId) {
                $scope.iteminfo.groupId = 0;
            }
            EasyStoreUserspersistenceService.action.getitemsInfoVM(JSON.stringify($scope.iteminfo)).then(
                function (result) {
                   
                    $scope.items = result.data.items;
                    $scope.totalItems = result.data.count;
                    $scope.totalPages = result.data.totalPages;
                    $scope.iteminfo.pageSize = result.data.pageSize;
                },
                function (error) {
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });
        };
        $scope.getgroupInfo = function (data) {
            $scope.fromGetGrouingo = true;
           
            $scope.inventoriesforMulti = angular.copy($scope.inventories);
        };


        $scope.addGroup = function (name) {
            var newGroup = angular.copy($scope.newObjects.itemsGroupDTO);
            $scope.oldGroup = angular.copy($scope.selectedGroup.selected);
            newGroup.groupName = name;
            newGroup.properties = [];
            newGroup.unit = angular.copy($scope.units[0]);
            $scope.selectedGroup.selected = newGroup;
            $scope.selectedGroup.selected.inventoryies = [];
            $scope.inventoriesforMulti = angular.copy($scope.inventories);
            $scope.showCancelGroupEdit = true;

        };
        $scope.addUnit = function (name) {
            var newunit = angular.copy($scope.newObjects.unitDTO);
            $scope.oldUnit = angular.copy($scope.selectedGroup.selected.unit);
            newunit.unitName = name;
            if (!$scope.selectedGroup.selected.isNew) {
                $scope.selectedGroup.selected.isToEdit = true;
                $scope.showCancelGroupEdit = true;
            }
            $scope.selectedGroup.selected.unit = newunit;
           

        };
        $scope.editGroup = function () {
            if (!$scope.selectedGroup.selected.isNew)
            {
                $scope.selectedGroup.selected.isToEdit = true;
                $scope.showCancelGroupEdit = true;
            }
    
        };

        $scope.editUnit = function () {
            if (!$scope.selectedGroup.selected.isNew) {
                $scope.selectedGroup.selected.isToEdit = true;
                $scope.showCancelGroupEdit = true;
                $scope.selectedGroup.selected.unit.isToEdit = true;
                return; 

            }
            $scope.selectedGroup.selected.unit.isToEdit = true;
            $scope.showCancelGroupEdit = true;

        };
        $scope.getUnitnfo = function (data) {
            console.log(data);
            if (!$scope.selectedGroup.selected.isNew) {
                var find = angular.copy($filter('filter')($scope.itemGroupsOrginal, { groupId: $scope.selectedGroup.selected.groupId }, true)[0]);

                if (data.unitId !== find.unit.unitId) {
                    if (!$scope.selectedGroup.selected.isNew) {
                        $scope.selectedGroup.selected.isToEdit = true;
                        $scope.showCancelGroupEdit = true;
                    }
                }
            }
        
        };
       
        $scope.$watch('selectedGroup.selected.groupId', function (newVal, oldVal) {
            if (!$scope.selectedGroup || !$scope.selectedGroup.selected) return;
            if (newVal === oldVal || newVal === 0) return;

            $scope.getItemsbyGroupId(newVal);
        });
        $scope.$watch('selectedGroup.selected.inventoryies', function (newVal, oldVal) {
            
            if ($scope.selectedGroup.selected.inventoryies && $scope.selectedGroup.selected.inventoryies.length && $scope.selectedGroup.selected.groupId !== 0 && $scope.itemGroupsOrginal && $scope.itemGroupsOrginal.length) {
                if ($scope.fromGetGrouingo = true)
                {
                    $scope.fromGetGrouingo = false;
                    return;
                }

                var findgroup = angular.copy($filter('filter')($scope.itemGroupsOrginal, { groupId: $scope.selectedGroup.selected.groupId }, true)[0]);
                if ($scope.selectedGroup.selected.groupId !== findgroup.groupId) return;
                    var groupsDiff = _.isEqual(findgroup.inventoryies.sort(), angular.copy($scope.selectedGroup.selected.inventoryies).sort());
                if (!groupsDiff) {
                    if (!$scope.selectedGroup.selected.isNew) {
                        $scope.selectedGroup.selected.isToEdit = true;
                        $scope.showCancelGroupEdit = true;
                    }
                }

            }
        });

        $scope.dismissGroup = function () {
            if ($scope.selectedGroup.selected.groupId === 0) {
                $scope.selectedGroup.selected = angular.copy($scope.oldGroup);
            }
           
            $scope.itemGroups.forEach(function (group, i) {
              
                if (group.groupId === $scope.selectedGroup.selected.groupId) {
                    var find = angular.copy($filter('filter')($scope.itemGroupsOrginal, { groupId: $scope.selectedGroup.selected.groupId }, true)[0]);
                    $scope.itemGroups[i] = angular.copy(find);
                    $scope.selectedGroup = angular.copy(find);
                    $scope.selectedGroup.selected = angular.copy(find);
                } 
            });
            $scope.showCancelGroupEdit = false;
        };
      
        $scope.checkUnitName = function (name)
        {
            var findunit = angular.copy($filter('filter')($scope.units, { unitName: name }, true));
            if (findunit.length) {
                for (var i = 0; i < findunit.length; i++) {
                    if (findunit[i].unitName === $scope.selectedGroup.selected.unit.unitName && findunit[i].unitId !== $scope.selectedGroup.selected.unit.unitId) {
                        $scope.showUnitWarning = true;
                        return;
                    }
                }
                $scope.showUnitWarning = false;
            }

        };

        $scope.checkGroupName = function (name) {
            var findGroup = angular.copy($filter('filter')($scope.itemGroups, { groupName: name }, true));
            if (findGroup.length) {
                for (var i = 0; i < findGroup.length; i++) {
                    if (findGroup[i].groupName === $scope.selectedGroup.selected.groupName && findGroup[i].groupId !== $scope.selectedGroup.selected.groupId) {
                        $scope.showGroupWarning = true;
                        return;
                    } 
                }
                $scope.showGroupWarning = false;
            }

        };

        $scope.addGroupProperty = function () {
            if (!$scope.selectedGroup.selected.isNew) {
                $scope.selectedGroup.selected.isToEdit = true;
                $scope.showCancelGroupEdit = true;

            };

            if (!$scope.selectedGroup.selected.properties) {
                $scope.selectedGroup.selected.properties = angular.copy($scope.newObjects.itemsGroupDTO.properties);
                return;

            }
            $scope.selectedGroup.selected.properties.push(angular.copy($scope.newObjects.itemsGroupDTO.properties[0]));

        };
        $scope.removeGroupProperty = function (index) {
            if (!$scope.selectedGroup.selected.isNew) {
                $scope.selectedGroup.selected.isToEdit = true;
                $scope.showCancelGroupEdit = true;
            }
            if (index > -1) {
                $scope.selectedGroup.selected.properties.splice(index, 1);
            }
        };
        $scope.getbarCode = function (data, index) {
            if (data) {
                $('#barcode' + index).JsBarcode(data, {
                    height: 25
                });
            } };

        $scope.getItemsbyGroupId = function (groupId)
        {
            if (groupId === 0) return;

            EasyStoreUserspersistenceService.action.getItemsbyGroupId(groupId).then(
                function (result) {
                    console.log('group items', result);
                    $scope.groupItems = result;
                    $scope.edititemObj.groupId =angular.copy($scope.selectedGroup.selected.groupId);
                    $scope.edititemObj.properties =angular.copy( $scope.selectedGroup.selected.properties);
                    for (var i = 0; i < $scope.edititemObj.properties.length; i++) {
                        if (!$scope.edititemObj.properties[i].propertiesValues) $scope.edititemObj.properties[i].propertiesValues = {};

                        $scope.edititemObj.properties[i].propertiesValues =
                            {
                             "isNew": true,
                             "propertyId": $scope.edititemObj.properties[i].propertyNameId,
                             "propertyValueId": 0,
                             "propertyValueName": null,
                             "propertyName": $scope.edititemObj.properties[i].propertyName
                            }
                    }

                    $scope.edititem = angular.copy($scope.edititemObj);
                },
                function (error) {
                    toastr.clear();
                    if (error.data) toastr.error(error.data.message); else toastr.error("لا يمكن اتمام العملية الان");
                });
        };
        $scope.addItemToedit = function (data)
        {
            var item = angular.copy(data);
            for (var i = 0; i < item.properties.length; i++) {
               
                if (!item.properties[i].propertyValues)
                {
                    item.properties[i].propertyValues = { propertyValueId: 0, propertyValueName: "", propertyId: item.properties[i].propertyNameId, isNew: true };
                   
                } else
                {
                    if (!item.properties[i].propertyValues.propertyValueName) item.properties[i].propertyValues.propertyValueName = "";
                    if (!item.properties[i].propertyValues.propertyId || item.properties[i].propertyValues.propertyId === 0) item.properties[i].propertyNameId;
                    if (!item.properties[i].propertyValues.propertyValueId) item.properties[i].propertyValues.propertyValueId = 0;
                   
                }
            }
            item.isNew = false;
            item.isToEdit = true;
            $scope.edititem =angular.copy(item);
        }
        $scope.saveItem = function () {
            console.log($scope.edititem);
            if (!$scope.groupItems) $scope.groupItems = [];
            if ($scope.edititem.isNew) {
                $scope.edititem.isNew = false;
                $scope.edititem.isToEdit = false; 
                console.log($scope.groupItems);

                $scope.groupItems.unshift(angular.copy($scope.edititem));
                console.log($scope.groupItems);

                $scope.edititem = angular.copy($scope.edititemObj);

                return;
            }
            $scope.edititem.isNew = false;
            $scope.edititem.isToEdit = false; 
            for (var i = 0; i < $scope.groupItems.length; i++) {
                if ($scope.groupItems[i].itemId === $scope.edititem.itemId) $scope.groupItems[i] = angular.copy($scope.edititem);

            }
            $scope.edititem = angular.copy($scope.edititemObj);
        };

        $scope.save = function () {
            EasyStoreUserspersistenceService.action.saveGroupVM(angular.toJson($scope.selectedGroup.selected)).then(
                function (result) {
                  
                    if (!$scope.itemGroupsOrginal) {
                        $scope.itemGroupsOrginal = [];
                        $scope.itemGroups = [];
                    };
                    if (!$scope.units) $scope.units = [];
                    if ($scope.selectedGroup.selected.unit.unitId === 0) $scope.units.push(angular.copy(result.data.unit));
                    if ($scope.selectedGroup.selected.groupId === 0) {
                        $scope.itemGroupsOrginal.push(angular.copy(result.data));
                        $scope.itemGroups.push(angular.copy(result.data));
                    };

                    $scope.itemGroupsOrginal.forEach(function (group, i) {
                        if (group.groupId === result.data.groupId) $scope.itemGroupsOrginal[i] = angular.copy(result.data);
                        if (group.unit.unitId === result.data.unit.unitId) $scope.itemGroupsOrginal[i].unit = angular.copy(result.data.unit);
                    });
                    $scope.itemGroups.forEach(function (group, i) {
                        if (group.groupId === result.data.groupId) $scope.itemGroups[i] = angular.copy(result.data);
                        if (group.unit.unitId === result.data.unit.unitId) $scope.itemGroupsOrginal[i].unit = angular.copy(result.data.unit);

                    });
                    $scope.units.forEach(function (unit, i) {
                        if (unit.unitId === result.data.unit.unitId) $scope.units[i] = angular.copy(result.data.unit);
                    });
                    $scope.oldGroup = angular.copy(result.data);
                    $scope.selectedGroup = angular.copy(result.data);
                    $scope.selectedGroup.selected = angular.copy(result.data);
                    $scope.showCancelGroupEdit = false;;


                },
                function (error) {
                    console.log(error);
                    if (error.data) {
                        toastr.clear();
                        if (error.data.message) toastr.error(error.data.message);
                        if (error.data.err) toastr.error(error.data.err);

                        if (error.data.group) {
                            var result = angular.copy(error.data.group);
                            console.log('dismiss save', result);
                            if (!$scope.itemGroupsOrginal) {
                                $scope.itemGroupsOrginal = [];
                                $scope.itemGroups = [];
                            };
                            if (!$scope.units) $scope.units = [];
                            if ($scope.selectedGroup.selected.unit.unitId === 0) $scope.units.push(angular.copy(result.unit));
                            if ($scope.selectedGroup.selected.groupId === 0) {
                                $scope.itemGroupsOrginal.push(angular.copy( result.data));
                                $scope.itemGroups.push(angular.copy( result.data));
                            };
                            $scope.itemGroupsOrginal.forEach(function (group, i) {
                                if (group.groupId === result.groupId) $scope.itemGroupsOrginal[i] = angular.copy(result);
                                if (group.unit.unitId === result.unit.unitId) $scope.itemGroupsOrginal[i].unit = angular.copy( result.unit);
                            });
                            $scope.itemGroups.forEach(function (group, i) {
                                if (group.groupId === result.groupId) $scope.itemGroups[i] = angular.copy(result);
                                if (group.unit.unitId === result.unit.unitId) $scope.itemGroupsOrginal[i].unit = angular.copy(result.unit);

                            });
                            $scope.units.forEach(function (unit, i) {
                                if (unit.unitId === result.unit.unitId) $scope.units[i] = angular.copy(result.unit);
                            });
                            $scope.oldGroup =angular.copy(result);
                            $scope.selectedGroup = angular.copy( result);
                            $scope.selectedGroup.selected = angular.copy( result);
                            $scope.showCancelGroupEdit = false;;
                        } else  {

                            $scope.dismissGroup();
                            console.log('dismiss');
                        }
                    
                   
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                        $scope.dismissGroup();
                    }

                });
        };
       
        var checkbrGroup = function () {
            if ($scope.branchGroub && $scope.branchGroub.id !== '0') {
                bId = parseInt(angular.copy(parseInt($scope.branchGroub.id)));
                $scope.loadItems();
            }
        }();

    });

}());
