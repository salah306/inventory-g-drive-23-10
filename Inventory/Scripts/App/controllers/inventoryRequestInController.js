﻿
(function () {


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("inventoryRequestInController", function ($scope, $timeout, _, $localStorage, AccountspersistenceService, $rootScope, settings, $filter,
        AccountOrderspersistenceService, Offline, EasyStoreUserspersistenceService, $location, branchesremotePersistenceStrategy, $sce, globalService, $q, $log, DTOptionsBuilder, DTColumnDefBuilder) {
        var bId;
        $scope.orderType = "in"
        $scope.tilteName = "اذن اضافة"
        $scope.itemsforBill = [];
        $scope.billPaymethods = [];
        $scope.titleAccounts = [];
        $scope.branchesGroub = [];
        $scope.billNames = [];
        $scope.orderGroupNames = [];
        $scope.waiting = 0;
        $scope.workinprogress = 0;
        jQuery('input[name="notfydate"]').daterangepicker({
            "singleDatePicker": true,
            "startDate": moment().format('YYYY-MM-DD'),
            "minDate": moment().format('YYYY-MM-DD'),
            "locale": {
                "format": "YYYY-MM-DD"
            }
        });

        var vm = this;
        vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(3);
        var start = moment().startOf('year');
        var end = moment().endOf('year');
        $scope.nowDate = moment().format('YYYY-MM-DD');
        $scope.startDate = moment().startOf('year').toDate();
        $scope.endDate = moment().endOf('year').toDate();
        function cb(start, end) {
            $('#reportrange span').html('عن الفترة من ' + start.format('YYYY-MM-DD') + ' الي ' + end.format('YYYY-MM-DD'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'اليوم': [moment(), moment()],
                'امس': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'اخر 7 ايام': [moment().subtract(6, 'days'), moment()],
                'اخر 30 يوم': [moment().subtract(29, 'days'), moment()],
                'هذا الشهر': [moment().startOf('month'), moment().endOf('month')],
                'الشهر الماضي': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'السنة الحالية': [moment().startOf('year'), moment().endOf('year')]
            }
        }, cb);

        cb(start, end);



        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            $scope.startDate = picker.startDate.toDate();
            $scope.endDate = picker.endDate.toDate();
        });

        $scope.branchGroub = $rootScope.mainbranchGroub;

        $scope.$watch('$root.mainbranchGroub.id', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal !== '0') {
                var findCompanybr = angular.copy($filter('filter')($rootScope.mainbranchesGroub, { id: newVal }, true));
                if (findCompanybr) {
                    $scope.branchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    $rootScope.mainbranchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    console.log('$rootScope.mainbranchGroub', $rootScope.mainbranchGroub);
                    if (findCompanybr.length) {
                        $scope.order = {
                            "purchaseOrderId": 0,
                            "orderNo": 0,
                            "orderONo": 0,
                            "type": $scope.orderType,
                            "orderDate": moment().format('YYYY-MM-DD'),
                            "orderDateSecond": "",
                            "orderDateThird": "",
                            "recivedDate": moment().format('YYYY-MM-DD'),
                            "userName": "",
                            "userNameSecond": "",
                            "userNameThird": "",
                            "isDoneFirst": false,
                            "isDoneSecond": false,
                            "isDoneThird": false,
                            "cancel": false,
                            "isNewOrder": true,
                            "transferTo": {
                                "inventoryId": 0,
                                "inventoryName": "",
                                "code": ""
                            },
                            "itemsRequest": [
                                {
                                    "itemId": 0,
                                    "itemName": "",
                                    "itemCode": "",
                                    "qty": 0,
                                    "price": 0,
                                    "note": "",
                                    "realted": ""
                                }
                            ],
                            "companyId": 0,
                            "isCompany": true,
                            "cancelDate": "",
                            "userNameCancel": ""
                        };
                        bId = parseInt(angular.copy(parseInt(findCompanybr[0].id)));

                        EasyStoreUserspersistenceService.action.branchGroupSt(JSON.stringify($scope.branchGroub)).then(function () { }, function () { });
                        $scope.getInventory();
                    }
                }

            }
        });


        $scope.order = {
            "purchaseOrderId": 0,
            "orderNo": 0,
            "OrderONo": 0,
            "orderDate": moment().format('YYYY-MM-DD'),
            "orderDateSecond": "",
            "orderDateThird": "",
            "recivedDate": moment().format('YYYY-MM-DD'),
            "userName": "",
            "userNameSecond": "",
            "userNameThird": "",
            "isDoneFirst": false,
            "isDoneSecond": false,
            "isDoneThird": false,
            "cancel": false,
            "isNewOrder": true,
            "transferTo": {
                "inventoryId": 0,
                "inventoryName": "",
                "code": ""
            },
            "itemsRequest": [
                {
                    "itemId": 0,
                    "itemName": "",
                    "itemCode": "",
                    "qty": 0,
                    "price": 0,
                    "note": "",
                    "realted": ""
                }
            ],
            "companyId": 0,
            "isCompany": true,
            "cancelDate": "",
            "userNameCancel": ""
        };
        $scope.items = [];
        $scope.$watch('order.transferTo.inventoryId', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                var findinventory = angular.copy($filter('filter')($scope.inventory, { inventoryId: newVal }, true));
                $scope.order.transferTo = findinventory.length ? findinventory[0] : { inventoryId: 0, inventoryName: null };

                if (findinventory.length) {
                    $localStorage.invrntorytransfertoPurRequest = $scope.order.transferTo;
                 
                }
            }
        });

        $scope.getInventory = function () {
            console.log("OrderType",$scope.orderType);
            AccountOrderspersistenceService.action.getInventoryOrder(JSON.stringify({ orderType: $scope.orderType, inventoryId: 0,type: null,orderNo: 0})).then(
                function (result) {
                    $scope.inventory = result.data.inventory;
                    $scope.waiting = result.data.waitingcount;
                },
                function (error) {
                    if (error.data) {
                        if (error.data.message) {
                            toastr.error(error.data.message);
                        }

                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });
        };


        //order.orderNo

        $scope.newOrder = function () {

            var neworder = {
                "purchaseOrderId": 0,
                "orderNo": 0,
                "OrderONo": 0,
                "orderDate": moment().format('YYYY-MM-DD'),
                "orderDateSecond": "",
                "orderDateThird": "",
                "recivedDate": moment().format('YYYY-MM-DD'),
                "userName": "",
                "userNameSecond": "",
                "userNameThird": "",
                "isDoneFirst": false,
                "isDoneSecond": false,
                "isDoneThird": false,
                "cancel": false,
                "isNewOrder": true,
                "transferTo": angular.copy($scope.order.transferTo),
                "itemsRequest": [
                    {
                        "itemId": 0,
                        "itemName": "",
                        "itemCode": "",
                        "qty": 0,
                        "price": 0,
                        "note": "",
                        "realted": ""
                    }
                ],
                "companyId": 0,
                "isCompany": true,
                "cancelDate": "",
                "userNameCancel": ""
            };
            $scope.order = neworder;
        };
        $scope.findOrderById = function (data , orderId) {
            //GetTransferOrderById
            AccountOrderspersistenceService.action.getOrderRequestByIdw(angular.toJson({ orderNo: parseInt(data), orderType: $scope.orderType, orderId: orderId })).then(
                function (result) {
                    $scope.inventory = [];
                    $scope.inventory.push(result.data.transferTo);
                    $scope.order = result.data;
                    $scope.order.orderDate = moment($scope.order.orderDate).format('YYYY-MM-DD');
                    $scope.order.cancelDate = moment($scope.order.cancelDate).format('YYYY-MM-DD');
                    $scope.order.recivedDate = moment($scope.order.recivedDate).format('YYYY-MM-DD');
                    $scope.order.orderDateSecond = moment($scope.order.orderDateSecond).format('YYYY-MM-DD');
                },
                function (error) {
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });
        };
        $scope.findOrderByIdModal = function (data) {
            //GetTransferOrderById
            AccountOrderspersistenceService.action.getOrderRequestByIdw(angular.toJson({ orderNo: parseInt(data.orderNo), orderType: $scope.orderType  ,orderId: data.orderId  })).then(
                function (result) {
                    $scope.inventory = [];
                    $scope.inventory.push(result.data.transferTo);
                    $scope.order = result.data;
                    $scope.order.orderDate = moment($scope.order.orderDate).format('YYYY-MM-DD');
                    $scope.order.cancelDate = moment($scope.order.cancelDate).format('YYYY-MM-DD');
                    $scope.order.recivedDate = moment($scope.order.recivedDate).format('YYYY-MM-DD');
                    $scope.order.orderDateSecond = moment($scope.order.orderDateSecond).format('YYYY-MM-DD');
                    $("#waiting-model").modal("hide");
                },
                function (error) {
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });
        };

        $scope.saveOrder = function () {

            var orderToSave = angular.copy($scope.order);

            orderToSave.orderDate = moment.utc();
            
            AccountOrderspersistenceService.action.saveinventoryrequest(angular.toJson(orderToSave)).then(
                function (result) {
                    $scope.order.isDoneSecond = true;
                },
                function (error) {
                    toastr.clear();
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });


        };

     


        $scope.getWaitingCounts = function () {
            //GetTransferOrderById
            AccountOrderspersistenceService.action.getwaitingPurOrderCounts().then(
                function (result) {
                    $scope.waiting = result.data.waitingcount;
                    $scope.workinprogress = result.data.workinprogress;
                },
                function (error) {
                    toastr.clear();
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }

                });
        }

        $scope.currentPage = 1;
        $scope.ordertoQuery = { orderNo: 0, isDoneSecond: false, cancel: false, all: true, isDoneFirst: false, minDate: '', maxDate: '', pageSize: 15, pageNumber: 0 }
        $scope.ordertoQueryallChenge = function () {
            if ($scope.ordertoQuery.all) {
                $scope.ordertoQuery.isDoneSecond = false;
                $scope.ordertoQuery.isDoneFirst = false;
                $scope.ordertoQuery.cancel = false;
            }


        };
        $scope.ordertoQueryallChengeFirst = function () {
            if ($scope.ordertoQuery.isDoneFirst) {
                $scope.ordertoQuery.isDoneSecond = false;
                $scope.ordertoQuery.all = false;
                $scope.ordertoQuery.cancel = false;
            }


        };

        $scope.ordertoQueryallChengeSecond = function () {
            if ($scope.ordertoQuery.isDoneSecond) {
                $scope.ordertoQuery.isDoneFirst = false;
                $scope.ordertoQuery.all = false;
                $scope.ordertoQuery.cancel = false;
            }

        };
        $scope.resetQuery = function () {
            $scope.ordertoQuery.isDoneFirst = false;
            $scope.ordertoQuery.all = true;
            $scope.ordertoQuery.cancel = false;
            $scope.ordertoQuery.isDoneSecond = false;
        };

        $scope.ordertoQueryallChengecancel = function () {
            if ($scope.ordertoQuery.cancel) {
                $scope.ordertoQuery.isDoneFirst = false;
                $scope.ordertoQuery.all = false;
                $scope.ordertoQuery.isDoneSecond = false;
            }


        };

        $scope.totalItems = 0;
        $scope.waitingorder = [];
        $scope.getwaitingDonefirst = function () {
            $scope.ordertoQuery.isDoneFirst = true;
            $scope.ordertoQueryallChengeFirst();
            var qu = {
                minDate: $scope.startDate, maxDate: $scope.endDate, pageSize: 15, orderType:$scope.orderType,
                pageNumber: $scope.currentPage, orderNo: parseInt($scope.ordertoQuery.orderNo), isDoneSecond: $scope.ordertoQuery.isDoneSecond, cancel: $scope.ordertoQuery.cancel, all: $scope.ordertoQuery.all, isDoneFirst: $scope.ordertoQuery.isDoneFirst
            }


            AccountOrderspersistenceService.action.getwaitingOrderInventory(JSON.stringify(qu)).then(
                function (result) {
                    $scope.waitingorder = result.data;
                    $("#waiting-model").modal("show");
                },
                function (error) {
                    toastr.clear();
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }
                    $scope.resetQuery();

                });
        }

        $scope.getwaitingDonesecond = function () {
            $scope.ordertoQuery.isDoneSecond = true;
            $scope.ordertoQueryallChengeSecond();
            var qu = {
                minDate: $scope.startDate, maxDate: $scope.endDate, pageSize: 15, orderType: $scope.orderType,
                pageNumber: $scope.currentPage, orderNo: parseInt($scope.ordertoQuery.orderNo), isDoneSecond: $scope.ordertoQuery.isDoneSecond, cancel: $scope.ordertoQuery.cancel, all: $scope.ordertoQuery.all, isDoneFirst: $scope.ordertoQuery.isDoneFirst
            }


            AccountOrderspersistenceService.action.getwaitingOrderInventory(JSON.stringify(qu)).then(
                function (result) {
                    $scope.waitingorder = result.data;
                    $("#waiting-model").modal("show");
                },
                function (error) {
                    toastr.clear();
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }
                    $scope.resetQuery();
                });
        }

        $scope.getQueryRequest = function () {

            var qu = {
                minDate: $scope.startDate, maxDate: $scope.endDate, pageSize: 15, orderType: $scope.orderType
            }


            AccountOrderspersistenceService.action.getwaitingOrderInventory(JSON.stringify(qu)).then(
                function (result) {
                    $scope.waitingorder = result.data;
                    $("#waiting-model").modal("show");
                },
                function (error) {
                    toastr.clear()
                    if (error.data) {
                        toastr.error(error.data.message);
                    } else {
                        toastr.error("لا يمكن اتمام العملية الان");
                    }
                    $scope.resetQuery();
                });
        }

        $scope.vaildOrder = function () {
            if ($scope.branchGroub && $scope.branchGroub.id != '0') {
                if (!$scope.order.transferTo || $scope.order.transferTo.inventoryId == 0 || !$scope.order.transferTo.inventoryName) {
                    console.log("$scope.order.transferTo")
                    return false;
                }
                if ($scope.order.itemsRequest.length < 1) {
                    console.log("itemsRequest.length")
                    return false;
                }
                for (var i = 0; i < $scope.order.itemsRequest.length; i++) {

                    if (!$scope.order.itemsRequest[i].itemCode || !$scope.order.itemsRequest[i].itemName) {
                        console.log("itemsRequest[i].itemCode ")
                        return false;
                    }

                    if (!isNaN(parseFloat($scope.order.itemsRequest[i].qty)) && isFinite($scope.order.itemsRequest[i].qty)) {
                        if ($scope.order.itemsRequest[i].qty <= 0) {
                            console.log("itemsRequest[i].qty <= 0")
                            return false;
                        }


                    } else {
                        console.log("itemsRequest[i].qty NAN")
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        };
        var checkbrGroup = function () {
            if ($scope.branchGroub && $scope.branchGroub.id != '0') {
                bId = parseInt(angular.copy(parseInt($scope.branchGroub.id)));
                $scope.order.isCompany = $scope.branchGroub.isCompany
                $scope.getInventory();
            }
        }();

    });

}());


