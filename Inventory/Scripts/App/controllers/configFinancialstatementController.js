﻿(function () {
    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("configFinancialstatementController", function ($scope, $timeout, _, AccountspersistenceService, OrdersConfigurationpersistenceService, $rootScope, settings, $filter, NgTableParams,
        AccountCategoriespersistenceService, BalanceSheetspersistenceService,
        BalanceSheetTypespersistenceService, branchespersistenceService,
        companiespersistenceService, companiesremotePersistenceStrategy, Offline, $location,
        branchesremotePersistenceStrategy, EasyStoreUserspersistenceService,$sce, authService, globalService, $q, $log) {



        var bId;

        $scope.branchesGroub = [];
        $scope.branchGroub = $rootScope.mainbranchGroub;




        $scope.loadCompanies = function () {
            $scope.branchesGroub = $rootScope.mainbranchesGroub;
        };

        $scope.selectedList = {  };

        $scope.$watch('$root.mainbranchGroub.id', function (newVal, oldVal) {
            if (newVal !== oldVal && newVal !== '0') {
                var findCompanybr = angular.copy($filter('filter')($rootScope.mainbranchesGroub, { id: newVal }, true));
                if (findCompanybr) {
                    $scope.branchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    $rootScope.mainbranchGroub = findCompanybr.length ? findCompanybr[0] : null;
                    console.log('$rootScope.mainbranchGroub', $rootScope.mainbranchGroub);
                    if (findCompanybr.length) {

                        bId = parseInt(angular.copy(parseInt(findCompanybr[0].id)));

                        EasyStoreUserspersistenceService.action.branchGroupSt(JSON.stringify($scope.branchGroub)).then(function () { }, function () { });
                        if ($scope.branchGroub.id !== '0') {
                            $scope.loadListes();
                        }
                    }
                }

            }
        });

        $scope.toggoletablerows = function (keyf, open) {

            //var toggleKey = angular.copy(key);
            var showHideList = $scope.selectedList;
            console.log("List", showHideList.listes);
            if (open) {
                var key = $scope.selectedList.listes.length
                while (key > 0) {
                    key--;
                    $scope.selectedList.listes[key].show = true;
                    $("i#td" + keyf + '' + key).removeClass("arrowup2t");
                    $("i#td" + keyf + '' + key).removeClass("arrow2t");
                    $("i#td" + keyf + '' + key).addClass("arrowup2t");

                }
            }
            else {
                var key = $scope.selectedList.listes.length
                while (key > 0) {
                    key--;
                    if ($scope.selectedList.listes[key].level != 2) {
                        $scope.selectedList.listes[key].show = false;
                        $("i#td" + keyf + '' + key).removeClass("arrowup2t");
                        $("i#td" + keyf + '' + key).removeClass("arrow2t");
                        $("i#td" + keyf + '' + key).addClass("arrow2t");
                    }
                    else {
                        $("i#td" + keyf + '' + key).removeClass("arrowup2t");
                        $("i#td" + keyf + '' + key).removeClass("arrow2t");
                        $("i#td" + keyf + '' + key).addClass("arrow2t");
                    }

                }

            }

            $(".level4t > i").removeClass("arrowup2t");
            $(".level4t > i").removeClass("arrow2t");

        };

        $scope.toggoletablerow = function (row, keyf, key) {
            var openClose = false;
            var toggleKey = angular.copy(key);
            if (key != 0) {
                openClose = !$scope.selectedList.listes[key - 1].show;
            }
            if (openClose) {
                while (key > 0) {
                    key--;
                    if ($scope.selectedList.listes[key].parentId == row.id) {
                        $scope.selectedList.listes[key].show = true;

                        $("i#td" + keyf + '' + key).addClass("arrow2t");

                    }
                }
                $("i#td" + keyf + '' + toggleKey).removeClass("arrowup2t");
                $("i#td" + keyf + '' + toggleKey).removeClass("arrow2t");

                $("i#td" + keyf + '' + toggleKey).addClass("arrowup2t");
            }
            else {
                while (key > 0) {
                    key--;
                    if ($scope.selectedList.listes[key].level > row.level) {
                        $("i#td" + keyf + '' + key).removeClass("arrowup2t");
                        $("i#td" + keyf + '' + key).removeClass("arrow2t");
                        $scope.selectedList.listes[key].show = false;

                    }
                    else {


                        key = 0;
                    }

                }

                $("i#td" + keyf + '' + toggleKey).removeClass("arrow2t");
                $("i#td" + keyf + '' + toggleKey).addClass("arrow2t");

            };

            $(".level4t > i").removeClass("arrowup2t");
            $(".level4t > i").removeClass("arrow2t");
        };

        $scope.getListInfo = function (data) {
            console.log('selectedList',data);
            $scope.selectedList = data;
        };
        $scope.listesName = [];
        $scope.loadListes = function () {
           
            OrdersConfigurationpersistenceService.action.GetAllFinancialList().then(
                function (result) {

                    $scope.financialLists = result.data;
                    console.log('f Lists', result)

                },
                function (error) {

                    toastr.error("حدث خطاء - اثناء انشاء قائمة للحسابات");

                });
        };

        $scope.addNewacc = function (data) {
            toastr.clear();
            if (data) {
                AccountspersistenceService.action.addNewMainacc(JSON.stringify({ listName: data,isNew: true, listId: null})).then(
                    function (result) {
                        if (!$scope.financialLists) {
                            $scope.financialLists = [];
                        }
                        $scope.financialLists.push(result.data);
                        toastr.info("تم الحفظ");
                        toastr.clear();
                    },
                    function (error) {
                        if (error.data) {
                            if (error.data.message) {
                                toastr.error(error.data.message);
                            }

                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }
                    });

            }
            if (!data) {

                toastr.warning("يجب ادخال اسم للقائمة ");
            }
        };

        $scope.istoEdit = false;
        $scope.selectedListToEdit = {};
        $scope.selectedListToEdiOrginal = {};
        $scope.seteditaccName = function (data) {
            if (data) {
                $scope.istoEdit = true;
                $scope.selectedListToEdit = angular.copy(data);
                $scope.selectedListToEdiOrginal = data;
            }
        };
        $scope.editaccName = function (val) {
            if (val === false) {
                $scope.selectedListToEdit = {};
                $scope.istoEdit = false;
                return;
            };

            toastr.clear();
            if ($scope.selectedListToEdit) {
                console.log('acc To Edit', $scope.selectedListToEdit);
                AccountspersistenceService.action.addNewMainacc(JSON.stringify({ listName: $scope.selectedListToEdit.listName, isNew: false, listId: $scope.selectedListToEdit.listId})).then(
                    function (result) {
                        $scope.selectedListToEdiOrginal.listName = $scope.selectedListToEdit.listName;
                        var findacc = $filter('filter')($scope.financialLists, { listId: $scope.selectedListToEdit.listId }, true);
                        if (findacc.length) {
                            findacc[0].listName = $scope.selectedListToEdit.listName;
                            $localStorage.selectedListToEdit = $scope.selectedListToEdiOrginal;
                        };
                        toastr.info("تم التحديث");
                        $scope.selectedListToEdit = {};
                        $scope.istoEdit = false;

                        toastr.clear();
                    },
                    function (error) {
                        if (error.data) {
                            if (error.data.message) {
                                toastr.error(error.data.message);
                            }

                        } else {
                            toastr.error("لا يمكن اتمام العملية الان");
                        }
                    });
            }
        };

        
        //End Of js
        var checkbrGroup = function () {
            if ($scope.branchGroub && $scope.branchGroub.id != '0') {
                bId = parseInt(angular.copy(parseInt($scope.branchGroub.id)));
                if ($scope.branchGroub.id !== '0') {
                    $scope.loadListes();
                }
            }
        }();

    });
}());