﻿
(function () {


    'use strict';
    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.controller("trialBalanceController", function ($scope, $timeout, _, AccountspersistenceService, $rootScope, settings, $filter,
                                                           AccountCategoriespersistenceService, BalanceSheetspersistenceService,
                                                           BalanceSheetTypespersistenceService, branchespersistenceService,
                                                           companiespersistenceService, AccountOrderspersistenceService, AccountMovementspersistenceService, Offline, $location, $stateParams, branchesremotePersistenceStrategy, $sce, authService, globalService, $q, $log, DTOptionsBuilder, DTColumnDefBuilder) {



        //$scope.$on('$viewContentLoaded', function () {
        //    // initialize core components
        //    App.initAjax();
        //    Layout.setSidebarMenuActiveLink('set', $('#sidebar_menu_link_profile')); // set profile link active in sidebar menu 

        //    // set default layout mode
        //    // set sidebar closed and body solid layout mode
        //    $rootScope.settings.layout.pageBodySolid = true;
        //    $rootScope.settings.layout.pageSidebarClosed = true;
        //});
        var vm = this;
        vm.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers').withDisplayLength(2);


        var start = moment().startOf('month');
        var end = moment().endOf('month');

        $scope.startDate = moment().startOf('month');
        $scope.endDate = moment().endOf('month');
        function cb(start, end) {
            $('#reportrange span').html('من ' + start.format('DD - MM -YYYY') + ' الي ' + end.format('DD - MM - YYYY'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'اليوم': [moment(), moment()],
                'امس': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'اخر 7 ايام': [moment().subtract(6, 'days'), moment()],
                'اخر 30 يوم': [moment().subtract(29, 'days'), moment()],
                'هذا الشهر': [moment().startOf('month'), moment().endOf('month')],
                'الشهر الماضي': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);



        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            $scope.startDate = picker.startDate.toDate();
            $scope.endDate = picker.endDate.toDate();

            $scope.getaccountMoves($scope.selectedAccountId, $scope.itemsperpage, $scope.currentPage, $scope.startDate, $scope.endDate);
        });

        $scope.accountsArray = [];

        $scope.branchesGroub = [];

        $scope.branchGroub = {
            id: 0,
            name: 'حدد الشركة - الفرع',
            fullName: 'حدد الشركة - الفرع'
        };

        $scope.bquery = angular.copy($stateParams.bId);
        $scope.aquery = angular.copy($stateParams.acId);

        $scope.loadCompanies = function () {
            AccountspersistenceService.action.Getbranchgroup(1).then(
                function (Companies) {
                    $scope.branchesGroub = Companies;

                    if ($scope.bquery != null) {
                        $scope.branchGroub.id = angular.copy($scope.bquery);
                        $scope.bquery = null;

                    }
                },
                function (error) {
                    $scope.error = error;
                });
        };

        $scope.loadCompanies();

        $scope.totalItems = 0;
        $scope.currentPage = 0;
        $scope.itemsperpage = 1;

        var bId;
        $scope.$watch('branchGroub.id', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                console.log('old - new', oldVal + ' - ' + newVal);
                var debitorSelected = $filter('filter')($scope.branchesGroub, { id: $scope.branchGroub.id });
                $scope.branchGroub = debitorSelected.length ? debitorSelected[0] : null;
                if (debitorSelected.length) {
                    $scope.selectedAccountId = newVal;
                    //$scope.getaccountMoves(newVal, $scope.itemsperpage, $scope.currentPage, $scope.startDate, $scope.endDate);
                    console.log('load', $scope.branchGroub)
                    var id = debitorSelected[0].id;
                    bId = parseInt(angular.copy(id));
                   
                }
            }
        });

        $scope.$watch('startDate', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getaccountMoves(parseInt($scope.selectedAccountId), $scope.itemsperpage, $scope.currentPage, $scope.startDate, $scope.endDate);
            }
        });

        $scope.$watch('endDate', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getaccountMoves(parseInt($scope.selectedAccountId), $scope.itemsperpage, $scope.currentPage, $scope.startDate, $scope.endDate);
            }

        });

        $scope.$watch('itemsperpage', function (newVal, oldVal) {

            var newvalint = parseInt(newVal)
            if (isNaN(newvalint)) {
                $scope.itemsperpage = 1;
            }
            if (newvalint !== oldVal && newVal > 1) {
                $scope.getaccountMoves(parseInt($scope.selectedAccountId), parseInt($scope.itemsperpage), $scope.currentPage, $scope.startDate, $scope.endDate);
                $scope.itemsperpage = newvalint;
            }
            if (newvalint <= 0) {
                $scope.itemsperpage = 1;
            } else if (isNaN(newvalint)) {
                $scope.itemsperpage = 1;
            }
        });

        $scope.sortType = 'code'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order

        $scope.$watch('sortType', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getaccountMoves(parseInt($scope.selectedAccountId), $scope.itemsperpage, $scope.currentPage, $scope.startDate, $scope.endDate);
            }

        });
        $scope.$watch('sortReverse', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                $scope.getaccountMoves(parseInt($scope.selectedAccountId), $scope.itemsperpage, $scope.currentPage, $scope.startDate, $scope.endDate);
            }

        });

        $scope.debitorSelected = {};
        $scope.crditorSelected = {};

        $scope.accountsArray = [];
        $scope.names = [];
        $scope.debitorSelected = { value: $scope.accountsArray[0] };




        $scope.getItemArr = function () {

            return $scope.accountsArray;
        }





       
        $scope.setSelectedAccountId = function (item) {
            $scope.selectedAccountId = item.id
            $scope.selectedAccount = item;

            // $scope.selectedAccountId = id
        };

        $scope.allAccounts = false;


        $scope.$watch('selectedAccountId', function (newVal, oldVal) {
            if (newVal !== oldVal) {
                if (newVal != 0) {

                    $scope.getaccountMoves(parseInt(newVal), $scope.itemsperpage, $scope.currentPage, $scope.startDate, $scope.endDate);

                }
            }
        });
        $scope.isAllAccounts = function () {
            $scope.getaccountMoves(parseInt($scope.selectedAccountId), $scope.itemsperpage, $scope.currentPage, $scope.startDate, $scope.endDate);
        };
        $scope.getaccountMoves = function (id, itemsperpage, currentPage, startDate, endDate) {
            //$http.get('/api/AccountOrders/GetAccountOrders/' + id + '/' + pageSize + '/' + pageNumber + '/' + startDate + '/' + endDate)

            AccountMovementspersistenceService.action.GetAllAccountOrdersdCatogery(JSON.stringify({ id: id, pageSize: itemsperpage, pageNumber: currentPage, startDate: startDate, endDate: endDate, search: $scope.search, orderby: $scope.sortType, orderType: $scope.sortReverse , isCompany : $scope.branchGroub.isCompany , allAccounts : $scope.allAccounts })).then(
            function (orders) {
                $scope.totalItems = orders.data.totalCount

                $scope.accountMoves = orders.data.orders;
                //$scope.accountMoves.sumDebit = $scope.getTotal($scope.accountMoves, 'debit');
                //$scope.accountMoves.sumCrdit = $scope.getTotal($scope.accountMoves, 'crdit');

            },
            function (error) {
                $scope.error = error;
            });
        };

        $scope.getTotal = function (type) {
            var total = 0;
            if ($scope.accountMoves!= null)
            {
                for (var i = 0; i < $scope.accountMoves.length; i++) {
                    if (type == 'debit') {
                        total += $scope.accountMoves[i].debit
                    }
                    else {
                        total += $scope.accountMoves[i].crdit
                    }

                }
            }
       
            return total;
        }
        $scope.search = "";

        $scope.itemsperpage = 5;

        $scope.getOrders = function (id, orderNo, accountId, isDebit) {
            //$http.get('/api/AccountOrders/GetAccountOrders/' + id + '/' + pageSize + '/' + pageNumber + '/' + startDate + '/' + endDate)

            AccountMovementspersistenceService.action.GetAccountOrder(JSON.stringify({ orderId: id, orderNo: orderNo, branchId: bId, accountId: accountId, isDebit: isDebit })).then(
            function (orders) {

                $scope.OrderTo = orders.data;
            },
            function (error) {
                $scope.error = error;
            });
        };

        $scope.searchOrder = function () {

            $scope.getaccountMoves(parseInt($scope.selectedAccountId), $scope.itemsperpage, $scope.currentPage, $scope.startDate, $scope.endDate);
        };

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
            $scope.getaccountMoves(parseInt($scope.selectedAccountId), $scope.itemsperpage, $scope.currentPage, $scope.startDate, $scope.endDate);

        };

        $scope.pageChanged = function () {
            $log.log('Page changed to: ' + $scope.currentPage);
            $scope.getaccountMoves(parseInt($scope.selectedAccountId), $scope.itemsperpage, $scope.currentPage, $scope.startDate, $scope.endDate);
        };


        $scope.accounts = [];


        $scope.addOrder = function () {
            $scope.OrderTo = {};
            $scope.getOrders(0, 0, parseInt($scope.selectedAccountId), true);
        };
        $scope.addOrderCrdit = function () {
            $scope.OrderTo = {};
            $scope.getOrders(0, 0, parseInt($scope.selectedAccountId), false);
        };

        $scope.editOrder = function (orderToEdit) {

            if (parseInt($scope.selectedAccountId) != 0) $scope.accountsArray.filter(function (o) { return o.id == parseInt($scope.selectedAccountId) })[0].status = true;

            $scope.getOrders(orderToEdit.orderId, orderToEdit.orderNo, orderToEdit.accountId, true)
        };

        $scope.closeModel = function () {
            // if ($scope.selectedAccountId != 0) $scope.accountsArray.filter(function (o) { return o.id == $scope.selectedAccountId })[0].status = false;

        };

        $scope.saveOrder = function () {
            if ($scope.OrderTo.orderId === 0) {

                AccountMovementspersistenceService.action.save(JSON.stringify($scope.OrderTo)).then(
                function (Companies) {
                    $scope.accountMoves.push(Companies.data);

                },
                function (error) {
                    $scope.error = error;
                });
            }
            else {
                $scope.updateOrder();
            }



        };

        $scope.updateOrder = function () {
            AccountMovementspersistenceService.action.save(JSON.stringify($scope.OrderTo)).then(
            function (Companies) {

                $scope.accountMoves.filter(function (o) { return o.orderId == Companies.data.orderId })[0].debit = Companies.data.debit;
                $scope.accountMoves.filter(function (o) { return o.orderId == Companies.data.orderId })[0].crdit = Companies.data.crdit;
                $scope.accountMoves.filter(function (o) { return o.orderId == Companies.data.orderId })[0].orderNote = Companies.data.orderNote;
                $scope.accountMoves.filter(function (o) { return o.orderId == Companies.data.orderId })[0].note = Companies.data.note;
                $scope.accountMoves.filter(function (o) { return o.orderId == Companies.data.orderId })[0].summery = Companies.data.summery;
                $scope.accountMoves.filter(function (o) { return o.orderId == Companies.data.orderId })[0] = Companies.data;

                $scope.accountMoves[0].summery = Companies.data.summery;

            },
            function (error) {
                $scope.error = error;
                console.log('order updated error', error)
            });

        };

        $scope.showacc = function () {
            console.log('d account', $scope.debitorAccounts)
        }

        $scope.addNewAccounts = function (account) {
            var lastItem = $scope.OrderTo.secondAccount.length - 1;
            $scope.OrderTo.secondAccount.push({ id: null, account: { id: null, key: null, status: null, value: null }, amount: 0 });

            if (account.id != null) {
                var checkacc = ($scope.accountsArray.indexOf(account), account);

                if (!checkacc.status) {
                    $scope.accountsArray.filter(function (acc) { return acc.id == account.id })[0].status = true;
                }
            }
        };


        $scope.addNewCrditorAccounts = function (account) {
            $scope.crditorAccounts.push({ id: null, account: { id: null, key: null, status: null, value: null }, amount: 0 });
            if (account.id != null) {
                var checkacc = ($scope.accountsArray.indexOf(account), account);
                if (checkacc.status) {
                    $scope.accountsArray.filter(function (acc) { return acc.id == account.id })[0].status = false;
                }
            }

        };

        $scope.addacc = function (account) {
            $scope.accountsArray.filter(function (acc) { return acc.id == $scope.selectedAccount.id })[0].status = true;

            if (account.id != null && account.id != 0) {
                var checkacc = ($scope.accountsArray.indexOf(account), account);
                var debitoracc;
                for (debitoracc of  $scope.OrderTo.secondAccount) {
                    if (debitoracc.account.id != null) {
                        $scope.accountsArray.filter(function (acc) { return acc.id == debitoracc.account.id })[0].status = true;
                    }

                };


                if (checkacc.status) {

                    $scope.accountsArray.filter(function (acc) { return acc.id == account.id })[0].status = false;

                }
                $scope.accountsArray.filter(function (acc) { return acc.id == $scope.selectedAccount.id })[0].status = true;

                if (parseInt($scope.selectedAccountId )!= 0) {
                    $scope.accountsArray.filter(function (acc) { return acc.id == parseInt($scope.selectedAccountId )})[0].status = true;

                }
            }

        };

        $scope.removeacc = function (account) {
            if (account.id != null && account.id != 0) {

                var checkacc = ($scope.accountsArray.indexOf(account), account);

                if (!checkacc.status) {
                    $scope.accountsArray.filter(function (acc) { return acc.id == account.id })[0].status = true;
                }
            }
        };


        $scope.onOpenClose = function (i) {
            console.log('is open', i);
        };


        $scope.removeAccounts = function (account) {
            var lastItem = $scope.OrderTo.secondAccount.length - 1;
            if (account.id != null) {
                $scope.accountsArray.filter(function (acc) { return acc.id == account.id })[0].status = false;

            };
            if (lastItem > 0) $scope.OrderTo.secondAccount.splice(lastItem);
        };



        $scope.AccountsTotalamount = function () {
            var total = 0;
            var cho = $scope.OrderTo.secondAccount;
            for (var i in cho) {

                if (!isNaN(parseFloat(cho[i].amount)))
                    total += parseFloat(cho[i].amount);
            }

            return total
        };



        $scope.usedAccount = function (item) {

            var deb = 0;
            var acc = 0;
            var bo = 'undefined';

            var debitorf = $filter('filter')($scope.debitorAccounts, { account: item });
            console.log('ddddddebitorAccounts', debitorf);
            for (acc = 0; acc < $scope.debitorAccounts.length; acc++) {
                console.log('$scope.debitorAccounts[acc].account.value', $scope.debitorAccounts[acc].account.value);
                console.log('$item', item.value);
                if ($scope.debitorAccounts[acc].account.value == item.value) {
                    console.log('hi the', $scope.debitorAccounts[acc]);
                    bo = true;
                }
                else {
                    console.log('hi ', $scope.debitorAccounts);
                    bo = false;
                }
            }

            console.log('hi the sss', bo);
            return bo;
            // return item.id == 6 ? true : false;


        };







    });

}());


