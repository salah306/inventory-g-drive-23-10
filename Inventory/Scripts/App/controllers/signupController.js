﻿'use strict';
(function () {

var inventoryModule = angular.module('inventoryModule');

inventoryModule.controller('signupController', ['$scope', '$location', '$timeout', 'authService', function ($scope, $location, $timeout, authService) {

    $scope.savedSuccessfully = false;
    $scope.message = "";

    $scope.registration = {
        firstName: "",
        lastName: "",
        occupation: "",
        userName: "",
        email: "",
        phoneNumber: "",
        password: "",
        confirmPassword: ""
    };
    $scope.signUp = function () {

        console.log($scope.registration);

        authService.saveRegistration($scope.registration).then(function (response) {

            $scope.savedSuccessfully = true;
            $scope.message = "تم التسجيل بتجاح, سوف يتم ارسال رساله الي بريدك الالكتروني لتأكيد البريد اتبع الرابط في الرسالة المرسلة.";
            //startTimer();

        },
         function (response) {
             var errors = [];
             for (var key in response.data.modelState) {
                 for (var i = 0; i < response.data.modelState[key].length; i++) {
                     errors.push(response.data.modelState[key][i]);
                 }
             }
             $scope.message = "فشل في التسجيل:" + errors.join(' ');
         });
    };

    var startTimer = function () {
        var timer = $timeout(function () {
            //$timeout.cancel(timer);
            //$location.path('/login');
        }, 2000);
    }

}]);

}());