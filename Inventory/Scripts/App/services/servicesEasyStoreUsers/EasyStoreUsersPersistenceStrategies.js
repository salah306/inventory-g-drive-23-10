﻿(function () {

    'use strict';

    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    };


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.factory('EasyStoreUsersremotePersistenceStrategy', function ($http, $q) {
        var svc = {

            getAllUsers: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/easystoreusers/getAllUsers', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else
                {

                    $http.post('/api/easystoreusers/getAllUsers', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },
         
            getUserRole: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/easystoreusers/getUserRole', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/easystoreusers/getUserRole', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },
            getitemsInfoVM: function (o) {
               
                var deferred = $q.defer();
                  
                return $http.post('/api/items/getitemInfo', o)
                        .success(deferred.resolve)
                        .error(deferred.reject);
               

                return deferred.promise;
            },

            saveGroupVM: function (o) {

                var deferred = $q.defer();

                return $http.post('/api/items/save', o)
                    .success(deferred.resolve)
                    .error(deferred.reject);


                return deferred.promise;
            },
            getItemsbyGroupId: function (groupId) {
                var deferred = $q.defer();

                $http.get('/api/items/getItemsbyGroupId/' + groupId)
                    .success(deferred.resolve)
                    .error(deferred.reject);

                return deferred.promise;
            },
            loaditemsInfoVM: function (o) {

                var deferred = $q.defer();

                return $http.post('/api/items/getUnits', o)
                    .success(deferred.resolve)
                    .error(deferred.reject);


                return deferred.promise;
            },
            getuers: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/easystoreusers/getuers')
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/easystoreusers/getuers')
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },

            getusersand: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                    return $http.post('/api/easystoreusers/getusersand')
                    .success(deferred.resolve)
                   .error(deferred.reject);
                return deferred.promise;
            },
            setUserRole: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/easystoreusers/setUserRole', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/easystoreusers/setUserRole', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },

            branchGroupSt: function (stro) {
                var deferred = $q.defer();
                return $http.post('/api/AppStorge/branchGroupSt', stro)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                return deferred.promise;
            },

            getCompanyByName: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/easystoreusers/getCompanyByName', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/easystoreusers/getCompanyByName', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },
            addUserToCompany: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/easystoreusers/addUserToCompany', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/easystoreusers/addUserToCompany', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },
            inviteUser: function (BalanceSheet) {
                var it = BalanceSheet;
                var deferred = $q.defer();
                if (isJson(BalanceSheet)) {
                    it = JSON.parse(BalanceSheet);
                    return $http.post('/api/easystoreusers/inviteUser', it)
                    .success(deferred.resolve)
                   .error(deferred.reject);
                } else {

                    $http.post('/api/easystoreusers/inviteUser', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },

            getUserSettings: function (name) {
                var deferred = $q.defer();

                $http.get('/api/easystoreusers/getUserSettings/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
    ////sssss

        };
        return svc;
    });

    inventoryModule.factory('EasyStoreUserslocalPersistenceStrategy',
        function ($q, localDBService, nullBalanceSheet, dbModel) {

            var svc = {

                dbModel: dbModel,

                localDBService: localDBService,

                getById: function (name) {
                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getById(svc.dbModel.objectStoreName, name)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                exists: function (name) {

                    var deferred = $q.defer();

                    svc.getById(name).then(function (BalanceSheet) {
                        deferred.resolve(BalanceSheet.id === name);
                    }, deferred.reject);

                    return deferred.promise;
                },

                save: function (BalanceSheet) {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function (e) {

                        var name = BalanceSheet.name;

                        if (name === null || name === undefined) {
                            localDBService.insert(svc.dbModel.objectStoreName, BalanceSheet, 'id')
                                          .then(deferred.resolve,
                                                deferred.reject);
                        } else {
                            svc.exists(name).then(function (doesExist) {

                                if (doesExist) {
                                    localDBService.update(svc.dbModel.objectStoreName, BalanceSheet, name)
                                                  .then(deferred.resolve,
                                                        deferred.reject);;
                                } else {
                                    localDBService.insert(svc.dbModel.objectStoreName, BalanceSheet, 'id')
                                                  .then(deferred.resolve,
                                                        deferred.reject);
                                }

                            }, deferred.reject);
                        }

                    }, deferred.reject);

                    return deferred.promise;
                },

                getAll: function () {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getAll(svc.dbModel.objectStoreName)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                'Delete': function (name) {

                    var deferred = $q.defer();

                    localDBService.delete(svc.dbModel.objectStoreName, name)
                                  .then(deferred.resolve,
                                        deferred.reject);

                    return deferred.promise;
                }

            };
           
            return svc;
        });

}());