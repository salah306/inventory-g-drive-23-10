﻿(function () {

    'use strict';

    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.service('AccountspersistenceService',
        function ($q, Offline, AccountsremotePersistenceStrategy, AccountslocalPersistenceStrategy) {

            var self = this;

            self.persistenceType = 'remote';

            self.action = AccountsremotePersistenceStrategy;

            Offline.on('confirmed-down', function () {
                self.action = AccountslocalPersistenceStrategy;
                self.persistenceType = 'local';
            });

            Offline.on('confirmed-up', function () {
                self.action = AccountsremotePersistenceStrategy;
                self.persistenceType = 'remote';
            });

            self.getRemoteAccount = function (name) {
                return AccountsremotePersistenceStrategy.getById(name);
            };

            self.getLocalAccount = function (name) {
                return AccountslocalPersistenceStrategy.getById(name);
            };

            self.getById = function (name) {

                var deferred = $q.defer();

                if (Offline.state = 'up') {

                    var
                        remoteAccount = {},
                        localAccount = {};

                    self.getRemoteAccount(name).then(function (rAccount) {

                        remoteAccount = rAccount;

                        self.getLocalAccount(name).then(function (lAccount) {

                            localAccount = lAccount;

                            if (localAccount.modifiedDate > (new Date(remoteAccount.modifiedDate))) {
                                deferred.resolve(localAccount);
                            }
                            else {
                                deferred.resolve(remoteAccount);
                            }

                        }, deferred.reject);

                    }, deferred.reject);

                } else {
                    self.getLocalAccount(name).then(deferred.resolve, deferred.reject);
                }

                return deferred.promise;
            };

            return self;

        });

}());