﻿(function () {

    'use strict';

    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.service('OrdersConfigurationpersistenceService',
        function ($q, Offline, OrdersConfigurationremotePersistenceStrategy, OrdersConfigurationlocalPersistenceStrategy) {

            var self = this;

            self.persistenceType = 'remote';

            self.action = OrdersConfigurationremotePersistenceStrategy;

            Offline.on('confirmed-down', function () {
                self.action = OrdersConfigurationlocalPersistenceStrategy;
                self.persistenceType = 'local';
            });

            Offline.on('confirmed-up', function () {
                self.action = OrdersConfigurationremotePersistenceStrategy;
                self.persistenceType = 'remote';
            });

            self.getRemoteOrdersConfiguration = function (name) {
                return OrdersConfigurationremotePersistenceStrategy.getById(name);
            };

            self.getLocalOrdersConfiguration = function (name) {
                return OrdersConfigurationlocalPersistenceStrategy.getById(name);
            };

            self.getById = function (name) {

                var deferred = $q.defer();

                if (Offline.state = 'up') {

                    var
                        remoteOrdersConfiguration = {},
                        localOrdersConfiguration = {};

                    self.getRemoteOrdersConfiguration(name).then(function (rOrdersConfiguration) {

                        remoteOrdersConfiguration = rOrdersConfiguration;

                        self.getLocalOrdersConfiguration(name).then(function (lOrdersConfiguration) {

                            localOrdersConfiguration = lOrdersConfiguration;

                            if (localOrdersConfiguration.modifiedDate > (new Date(remoteOrdersConfiguration.modifiedDate))) {
                                deferred.resolve(localOrdersConfiguration);
                            }
                            else {
                                deferred.resolve(remoteOrdersConfiguration);
                            }

                        }, deferred.reject);

                    }, deferred.reject);

                } else {
                    self.getLocalOrdersConfiguration(name).then(deferred.resolve, deferred.reject);
                }

                return deferred.promise;
            };

            return self;

        });

}());