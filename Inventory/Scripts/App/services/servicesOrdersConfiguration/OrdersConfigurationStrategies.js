﻿(function () {

    'use strict';

    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    };


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.factory('OrdersConfigurationremotePersistenceStrategy', function ($http, $q) {
        var svc = {


     saveSalesOrderconfig: function (or) {
           
            var deferred = $q.defer();
         
            return $http.post('/api/ordersconfiguration/saveSalesOrderconfig', or)
                 .success(deferred.resolve)
                 .error(deferred.reject);
                return deferred.promise;
     },
     savePurchaseOrderconfig: function (or) {

         var deferred = $q.defer();

         return $http.post('/api/ordersconfiguration/savePurchaseOrderconfig', or)
              .success(deferred.resolve)
              .error(deferred.reject);
         return deferred.promise;
     },
     saveinventory: function (or) {

         var deferred = $q.defer();

         return $http.post('/api/ordersconfiguration/saveinventory', or)
             .success(deferred.resolve)
             .error(deferred.reject);
         return deferred.promise;
     },
     GetOrderGroupNames: function (company) {
       
         var deferred = $q.defer();
         return $http.post('/api/POrder/getOrderGroupNames', company)
                 .success(deferred.resolve)
                 .error(deferred.reject);
         return deferred.promise;
     },
     saveOrder: function (company) {

         var deferred = $q.defer();
         return $http.post('/api/POrder/saveOrder', company)
             .success(deferred.resolve)
             .error(deferred.reject);
         return deferred.promise;
     },
     getOrderObyId: function (company) {

         var deferred = $q.defer();
         return $http.post('/api/POrder/getOrderbyId', company)
             .success(deferred.resolve)
             .error(deferred.reject);
         return deferred.promise;
     },
     GetOrderTemplatebyId: function (o) {

         var deferred = $q.defer();
         return $http.post('/api/POrder/GetOrderTemplatebyId', o)
             .success(deferred.resolve)
             .error(deferred.reject);
         return deferred.promise;
     },
     GetAllFinancialList: function () {

         var deferred = $q.defer();
         return $http.post('/api/financialStatementsconfig/GetAllFinancialList')
             .success(deferred.resolve)
             .error(deferred.reject);
         return deferred.promise;
     },
     getAllOrderwaiting: function (o) {

         var deferred = $q.defer();
         return $http.post('/api/POrder/getAllOrderwaiting', o)
             .success(deferred.resolve)
             .error(deferred.reject);
         return deferred.promise;
     },
     getOrderGroupforBill: function (name) {
         var deferred = $q.defer();

         $http.get('/api/POrder/getOrderGroupforBill/' + name)
             .success(deferred.resolve)
             .error(deferred.reject);

         return deferred.promise;
     },
     GetOrderbyId: function (or) {

         var deferred = $q.defer();

         return $http.post('/api/ordersconfiguration/GetOrderbyId', or)
              .success(deferred.resolve)
              .error(deferred.reject);
         return deferred.promise;
     },
     getOrderConfigureName: function () {
           
            var deferred = $q.defer();
         
            return $http.post('/api/ordersconfiguration/getOrdersName')
                    .success(deferred.resolve)
                    .error(deferred.reject);
            return deferred.promise;
        }};
        return svc;
    });


    inventoryModule.factory('OrdersConfigurationlocalPersistenceStrategy',
        function ($q, localDBService, nullBalanceSheet, dbModel) {

            var svc = {

                dbModel: dbModel,

                localDBService: localDBService,

                getById: function (name) {
                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getById(svc.dbModel.objectStoreName, name)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                exists: function (name) {

                    var deferred = $q.defer();

                    svc.getById(name).then(function (BalanceSheet) {
                        deferred.resolve(BalanceSheet.id === name);
                    }, deferred.reject);

                    return deferred.promise;
                },

                save: function (BalanceSheet) {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function (e) {

                        var name = BalanceSheet.name;

                        if (name === null || name === undefined) {
                            localDBService.insert(svc.dbModel.objectStoreName, BalanceSheet, 'id')
                                          .then(deferred.resolve,
                                                deferred.reject);
                        } else {
                            svc.exists(name).then(function (doesExist) {

                                if (doesExist) {
                                    localDBService.update(svc.dbModel.objectStoreName, BalanceSheet, name)
                                                  .then(deferred.resolve,
                                                        deferred.reject);;
                                } else {
                                    localDBService.insert(svc.dbModel.objectStoreName, BalanceSheet, 'id')
                                                  .then(deferred.resolve,
                                                        deferred.reject);
                                }

                            }, deferred.reject);
                        }

                    }, deferred.reject);

                    return deferred.promise;
                },

                getAll: function () {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getAll(svc.dbModel.objectStoreName)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                'Delete': function (name) {

                    var deferred = $q.defer();

                    localDBService.delete(svc.dbModel.objectStoreName, name)
                                  .then(deferred.resolve,
                                        deferred.reject);

                    return deferred.promise;
                }

            };
           
            return svc;
        });

}());