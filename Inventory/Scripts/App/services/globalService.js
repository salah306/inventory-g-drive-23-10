﻿'use strict';
inventoryModule.factory('globalService', ['$http', '$q', 'localStorageService', function ($http, $q, localStorageService) {

    var serviceBase = '/';
    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        userName: "",
        firstName: ""
    };

    var _saveRegistration = function (registration) {

        _logOut();

        return $http.post(serviceBase + 'api/account/create', registration).then(function (response) {

            return response;
        });

    };


    var _saveInvite = function (registration) {



        return $http.post(serviceBase + 'api/account/invite', registration).then(function (response) {

            return response;
        });

    };

    var _getUserCompanyRole = function (values) {



        return $http.post(serviceBase + 'api/companies/getUserCompanyRole', values).then(function (response) {

            return response;
        });

    };

    var _getUsers = function () {

        var deferred = $q.defer();

        $http.get('/api/Account/users')
             .success(deferred.resolve)
             .error(deferred.reject);

        return deferred.promise;


    };

    var _login = function (loginData) {

        var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

        var deferred = $q.defer();

        $http.post(serviceBase + 'oauth/token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

            localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.userName, firstName: response.firstName });

            _authentication.isAuth = true;
            _authentication.userName = loginData.userName;

            console.log(_authentication)

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _logOut = function () {

        localStorageService.remove('authorizationData');

        _authentication.isAuth = false;
        _authentication.userName = "";
        _authentication.firstName = "";

    };


    var _RegenerateEmailConfirmation = function (userName) {

        return $http.post(serviceBase + 'api/account/regenerateEmailConfirmation/', userName).then(function (response) {

            return response;
        });
    };

    var _fillAuthData = function () {

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
            _authentication.firstName = authData.firstName;
            console.log('here we are')
            console.log(_authentication.firstName)
        }

    }


    authServiceFactory.invite = _saveInvite;
    authServiceFactory.getUserCompanyRole = _getUserCompanyRole;
    authServiceFactory.getUsers = _getUsers;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;
   

    authServiceFactory.GetAllUsers = function () {

        return $http.get(serviceBase + 'api/Account/users').then(function (response) {

            return response;
        });
    };

    return authServiceFactory;
}]);