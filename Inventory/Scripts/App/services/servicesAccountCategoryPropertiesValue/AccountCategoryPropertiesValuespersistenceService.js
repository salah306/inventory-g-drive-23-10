﻿(function () {

    'use strict';

    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.service('AccountCategoryPropertiesValuespersistenceService',
        function ($q, Offline, AccountCategoryPropertiesValuesremotePersistenceStrategy, AccountCategoryPropertiesValueslocalPersistenceStrategy) {

            var self = this;

            self.persistenceType = 'remote';

            self.action = AccountCategoryPropertiesValuesremotePersistenceStrategy;

            Offline.on('confirmed-down', function () {
                self.action = AccountCategoryPropertiesValueslocalPersistenceStrategy;
                self.persistenceType = 'local';
            });

            Offline.on('confirmed-up', function () {
                self.action = AccountCategoryPropertiesValuesremotePersistenceStrategy;
                self.persistenceType = 'remote';
            });

            self.getRemoteAccountCategoryPropertiesValue = function (name) {
                return AccountCategoryPropertiesValuesremotePersistenceStrategy.getById(name);
            };

            self.getLocalAccountCategoryPropertiesValue = function (name) {
                return AccountCategoryPropertiesValueslocalPersistenceStrategy.getById(name);
            };

            self.getById = function (name) {

                var deferred = $q.defer();

                if (Offline.state = 'up') {

                    var
                        remoteAccountCategoryPropertiesValue = {},
                        localAccountCategoryPropertiesValue = {};

                    self.getRemoteAccountCategoryPropertiesValue(name).then(function (rAccountCategoryPropertiesValue) {

                        remoteAccountCategoryPropertiesValue = rAccountCategoryPropertiesValue;

                        self.getLocalAccountCategoryPropertiesValue(name).then(function (lAccountCategoryPropertiesValue) {

                            localAccountCategoryPropertiesValue = lAccountCategoryPropertiesValue;

                            if (localAccountCategoryPropertiesValue.modifiedDate > (new Date(remoteAccountCategoryPropertiesValue.modifiedDate))) {
                                deferred.resolve(localAccountCategoryPropertiesValue);
                            }
                            else {
                                deferred.resolve(remoteAccountCategoryPropertiesValue);
                            }

                        }, deferred.reject);

                    }, deferred.reject);

                } else {
                    self.getLocalAccountCategoryPropertiesValue(name).then(deferred.resolve, deferred.reject);
                }

                return deferred.promise;
            };

            return self;

        });

}());