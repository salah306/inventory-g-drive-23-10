﻿(function () {

    'use strict';

    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    };


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.factory('companiesremotePersistenceStrategy', function ($http, $q) {
        var svc = {

            save: function () {
               
                var deferred = $q.defer();
                return $http.post('/api/companies')
                    .success(deferred.resolve)
                    .error(deferred.reject);

                return deferred.promise;
            },

            getTypesInfo: function () {

                var deferred = $q.defer();
                return $http.post('/api/companies/getTypesInfo')
                    .success(deferred.resolve)
                    .error(deferred.reject);

                return deferred.promise;
            },
            update: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company))
                {
                    it = JSON.parse(company);
                    return $http.post('/api/companies/updateCompany', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {

                  

                    return $http.post('/api/companies/updateCompany', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },
            getCompanyInfo: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/companies/getCompanyInfo', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/companies/getCompanyInfo', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },

            addCompanyBranchProperty: function (company) {
                var deferred = $q.defer();
                return $http.post('/api/companies/addCompanyBranchProperty', company)
                   .success(deferred.resolve)
                   .error(deferred.reject);

                return deferred.promise;
            },
            updateBranch: function (company) {
                var deferred = $q.defer();
                return $http.post('/api/companies/updateBranch', company)
                   .success(deferred.resolve)
                   .error(deferred.reject);

                return deferred.promise;
            },
            saveBranch: function (company) {
                var deferred = $q.defer();
                return $http.post('/api/companies/saveBranch', company)
                   .success(deferred.resolve)
                   .error(deferred.reject);

                return deferred.promise;
            },
            updateCompanyBranchProperty: function (company) {
                var deferred = $q.defer();
                return $http.post('/api/companies/updateCompanyBranchProperty', company)
                   .success(deferred.resolve)
                   .error(deferred.reject);

                return deferred.promise;
            },
            delCompanyBranchProperty: function (company) {
                var deferred = $q.defer();
                return $http.post('/api/companies/delCompanyBranchProperty', company)
                   .success(deferred.resolve)
                   .error(deferred.reject);

                return deferred.promise;
            },
            updateCompanyBranchPropertyValue: function (company) {
                var deferred = $q.defer();
                return $http.post('/api/companies/updateCompanyBranchPropertyValue', company)
                   .success(deferred.resolve)
                   .error(deferred.reject);

                return deferred.promise;
            },
            addCompanyBranchPropertyValue: function (company) {
                var deferred = $q.defer();
                return $http.post('/api/companies/addCompanyBranchPropertyValue', company)
                   .success(deferred.resolve)
                   .error(deferred.reject);

                return deferred.promise;
            },
            //getTypesInfo: function () {
            //    var deferred = $q.defer();
            //    return $http.post('/api/companies/getTypesInfo')
            //       .success(deferred.resolve)
            //       .error(deferred.reject);

            //    return deferred.promise;
            //},
            delCompanyBranchPropertyValue: function (company) {
                var deferred = $q.defer();
                return $http.post('/api/companies/delCompanyBranchPropertyValue', company)
                   .success(deferred.resolve)
                   .error(deferred.reject);

                return deferred.promise;
            },

            getallCompany: function () {
                var deferred = $q.defer();
                console.log("het all company ")
                $http.get('/api/companies/Companies')
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },

            getById: function (name) {
                var deferred = $q.defer();

                $http.get('/api/companies/companiesById/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            exists: function (name) {

                var deferred = $q.defer();

                svc.getById(name).then(function (company) {
                    deferred.resolve(company.id === name);
                }, deferred.reject);

                return deferred.promise;
            },
  
            chickuser: function (company) {
                var it = company;
                var deferred = $q.defer();
                if (isJson(company)) {
                    it = JSON.parse(company);
                    return $http.post('/api/companies/chickuser', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                } else {



                    return $http.post('/api/companies/chickuser', it)
                      .success(deferred.resolve)
                      .error(deferred.reject);
                }

                return deferred.promise;
            },

            'Delete': function (name) {
                var deferred = $q.defer();

                $http.delete('/api/companies/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            }
        };
        return svc;
    });

    inventoryModule.factory('companieslocalPersistenceStrategy',
        function ($q, localDBService, nullcompany, dbModel) {

            var svc = {

                dbModel: dbModel,

                localDBService: localDBService,

                getById: function (name) {
                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getById(svc.dbModel.objectStoreName, name)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                exists: function (name) {

                    var deferred = $q.defer();

                    svc.getById(name).then(function (company) {
                        deferred.resolve(company.id === name);
                    }, deferred.reject);

                    return deferred.promise;
                },

                save: function (company) {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function (e) {

                        var name = company.name;

                        if (name === null || name === undefined) {
                            localDBService.insert(svc.dbModel.objectStoreName, company, 'id')
                                          .then(deferred.resolve,
                                                deferred.reject);
                        } else {
                            svc.exists(name).then(function (doesExist) {

                                if (doesExist) {
                                    localDBService.update(svc.dbModel.objectStoreName, company, name)
                                                  .then(deferred.resolve,
                                                        deferred.reject);;
                                } else {
                                    localDBService.insert(svc.dbModel.objectStoreName, company, 'id')
                                                  .then(deferred.resolve,
                                                        deferred.reject);
                                }

                            }, deferred.reject);
                        }

                    }, deferred.reject);

                    return deferred.promise;
                },

                getAll: function () {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getAll(svc.dbModel.objectStoreName)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                'Delete': function (name) {

                    var deferred = $q.defer();

                    localDBService.delete(svc.dbModel.objectStoreName, name)
                                  .then(deferred.resolve,
                                        deferred.reject);

                    return deferred.promise;
                }

            };
           
            return svc;
        });

}());