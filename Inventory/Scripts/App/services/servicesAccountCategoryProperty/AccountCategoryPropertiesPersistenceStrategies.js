﻿(function () {

    'use strict';

    function isJson(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    };


    var inventoryModule = angular.module('inventoryModule');

    inventoryModule.factory('AccountCategoryPropertiesremotePersistenceStrategy', function ($http, $q) {
        var svc = {

            save: function (AccountCategoryProperty) {
                var it = AccountCategoryProperty;
                var deferred = $q.defer();
                if (isJson(AccountCategoryProperty))
                {
                    it = JSON.parse(AccountCategoryProperty);
                    return $q.all(it.map(function (it) {
                        $http.post('/api/AccountCategoryProperties', it).then(deferred.resolve, deferred.reject);
                    }));
                } else {

                    $http.post('/api/AccountCategoryProperties', it)
                     .success(deferred.resolve)
                     .error(deferred.reject);
                }

                return deferred.promise;
            },


            getAll: function () {
                var deferred = $q.defer();

                $http.get('/api/AccountCategoryProperties')
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },

            getById: function (name) {
                var deferred = $q.defer();

                $http.get('/api/AccountCategoryProperties/AccountCategoryPropertiesById/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            },
            exists: function (name) {

                var deferred = $q.defer();

                svc.getById(name).then(function (AccountCategoryProperty) {
                    deferred.resolve(AccountCategoryProperty.id === name);
                }, deferred.reject);

                return deferred.promise;
            },

            'Delete': function (name) {
                var deferred = $q.defer();

                $http.delete('/api/AccountCategoryProperties/' + name)
                     .success(deferred.resolve)
                     .error(deferred.reject);

                return deferred.promise;
            }
        };
        return svc;
    });

    inventoryModule.factory('AccountCategoryPropertieslocalPersistenceStrategy',
        function ($q, localDBService, nullAccountCategoryProperty, dbModel) {

            var svc = {

                dbModel: dbModel,

                localDBService: localDBService,

                getById: function (name) {
                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getById(svc.dbModel.objectStoreName, name)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                exists: function (name) {

                    var deferred = $q.defer();

                    svc.getById(name).then(function (AccountCategoryProperty) {
                        deferred.resolve(AccountCategoryProperty.id === name);
                    }, deferred.reject);

                    return deferred.promise;
                },

                save: function (AccountCategoryProperty) {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function (e) {

                        var name = AccountCategoryProperty.name;

                        if (name === null || name === undefined) {
                            localDBService.insert(svc.dbModel.objectStoreName, AccountCategoryProperty, 'id')
                                          .then(deferred.resolve,
                                                deferred.reject);
                        } else {
                            svc.exists(name).then(function (doesExist) {

                                if (doesExist) {
                                    localDBService.update(svc.dbModel.objectStoreName, AccountCategoryProperty, name)
                                                  .then(deferred.resolve,
                                                        deferred.reject);;
                                } else {
                                    localDBService.insert(svc.dbModel.objectStoreName, AccountCategoryProperty, 'id')
                                                  .then(deferred.resolve,
                                                        deferred.reject);
                                }

                            }, deferred.reject);
                        }

                    }, deferred.reject);

                    return deferred.promise;
                },

                getAll: function () {

                    var deferred = $q.defer();

                    localDBService.open(svc.dbModel).then(function () {
                        localDBService.getAll(svc.dbModel.objectStoreName)
                                      .then(deferred.resolve,
                                            deferred.reject);
                    }, deferred.reject);

                    return deferred.promise;
                },

                'Delete': function (name) {

                    var deferred = $q.defer();

                    localDBService.delete(svc.dbModel.objectStoreName, name)
                                  .then(deferred.resolve,
                                        deferred.reject);

                    return deferred.promise;
                }

            };
           
            return svc;
        });

}());